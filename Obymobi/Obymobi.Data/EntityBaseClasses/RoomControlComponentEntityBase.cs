﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'RoomControlComponent'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RoomControlComponentEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RoomControlComponentEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection	_roomControlComponentLanguageCollection;
		private bool	_alwaysFetchRoomControlComponentLanguageCollection, _alreadyFetchedRoomControlComponentLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection;
		private bool	_alwaysFetchRoomControlWidgetCollection, _alreadyFetchedRoomControlWidgetCollection;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection_;
		private bool	_alwaysFetchRoomControlWidgetCollection_, _alreadyFetchedRoomControlWidgetCollection_;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection__;
		private bool	_alwaysFetchRoomControlWidgetCollection__, _alreadyFetchedRoomControlWidgetCollection__;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection___;
		private bool	_alwaysFetchRoomControlWidgetCollection___, _alreadyFetchedRoomControlWidgetCollection___;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection____;
		private bool	_alwaysFetchRoomControlWidgetCollection____, _alreadyFetchedRoomControlWidgetCollection____;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection_____;
		private bool	_alwaysFetchRoomControlWidgetCollection_____, _alreadyFetchedRoomControlWidgetCollection_____;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection______;
		private bool	_alwaysFetchRoomControlWidgetCollection______, _alreadyFetchedRoomControlWidgetCollection______;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection_______;
		private bool	_alwaysFetchRoomControlWidgetCollection_______, _alreadyFetchedRoomControlWidgetCollection_______;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection________;
		private bool	_alwaysFetchRoomControlWidgetCollection________, _alreadyFetchedRoomControlWidgetCollection________;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetCollection	_roomControlWidgetCollection_________;
		private bool	_alwaysFetchRoomControlWidgetCollection_________, _alreadyFetchedRoomControlWidgetCollection_________;
		private InfraredConfigurationEntity _infraredConfigurationEntity;
		private bool	_alwaysFetchInfraredConfigurationEntity, _alreadyFetchedInfraredConfigurationEntity, _infraredConfigurationEntityReturnsNewIfNotFound;
		private RoomControlSectionEntity _roomControlSectionEntity;
		private bool	_alwaysFetchRoomControlSectionEntity, _alreadyFetchedRoomControlSectionEntity, _roomControlSectionEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name InfraredConfigurationEntity</summary>
			public static readonly string InfraredConfigurationEntity = "InfraredConfigurationEntity";
			/// <summary>Member name RoomControlSectionEntity</summary>
			public static readonly string RoomControlSectionEntity = "RoomControlSectionEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name RoomControlComponentLanguageCollection</summary>
			public static readonly string RoomControlComponentLanguageCollection = "RoomControlComponentLanguageCollection";
			/// <summary>Member name RoomControlWidgetCollection</summary>
			public static readonly string RoomControlWidgetCollection = "RoomControlWidgetCollection";
			/// <summary>Member name RoomControlWidgetCollection_</summary>
			public static readonly string RoomControlWidgetCollection_ = "RoomControlWidgetCollection_";
			/// <summary>Member name RoomControlWidgetCollection__</summary>
			public static readonly string RoomControlWidgetCollection__ = "RoomControlWidgetCollection__";
			/// <summary>Member name RoomControlWidgetCollection___</summary>
			public static readonly string RoomControlWidgetCollection___ = "RoomControlWidgetCollection___";
			/// <summary>Member name RoomControlWidgetCollection____</summary>
			public static readonly string RoomControlWidgetCollection____ = "RoomControlWidgetCollection____";
			/// <summary>Member name RoomControlWidgetCollection_____</summary>
			public static readonly string RoomControlWidgetCollection_____ = "RoomControlWidgetCollection_____";
			/// <summary>Member name RoomControlWidgetCollection______</summary>
			public static readonly string RoomControlWidgetCollection______ = "RoomControlWidgetCollection______";
			/// <summary>Member name RoomControlWidgetCollection_______</summary>
			public static readonly string RoomControlWidgetCollection_______ = "RoomControlWidgetCollection_______";
			/// <summary>Member name RoomControlWidgetCollection________</summary>
			public static readonly string RoomControlWidgetCollection________ = "RoomControlWidgetCollection________";
			/// <summary>Member name RoomControlWidgetCollection_________</summary>
			public static readonly string RoomControlWidgetCollection_________ = "RoomControlWidgetCollection_________";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RoomControlComponentEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RoomControlComponentEntityBase() :base("RoomControlComponentEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		protected RoomControlComponentEntityBase(System.Int32 roomControlComponentId):base("RoomControlComponentEntity")
		{
			InitClassFetch(roomControlComponentId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RoomControlComponentEntityBase(System.Int32 roomControlComponentId, IPrefetchPath prefetchPathToUse): base("RoomControlComponentEntity")
		{
			InitClassFetch(roomControlComponentId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="validator">The custom validator object for this RoomControlComponentEntity</param>
		protected RoomControlComponentEntityBase(System.Int32 roomControlComponentId, IValidator validator):base("RoomControlComponentEntity")
		{
			InitClassFetch(roomControlComponentId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlComponentEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_roomControlComponentLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection)info.GetValue("_roomControlComponentLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection));
			_alwaysFetchRoomControlComponentLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlComponentLanguageCollection");
			_alreadyFetchedRoomControlComponentLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlComponentLanguageCollection");

			_roomControlWidgetCollection = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection");
			_alreadyFetchedRoomControlWidgetCollection = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection");

			_roomControlWidgetCollection_ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection_", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection_ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection_");
			_alreadyFetchedRoomControlWidgetCollection_ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection_");

			_roomControlWidgetCollection__ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection__", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection__ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection__");
			_alreadyFetchedRoomControlWidgetCollection__ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection__");

			_roomControlWidgetCollection___ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection___", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection___ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection___");
			_alreadyFetchedRoomControlWidgetCollection___ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection___");

			_roomControlWidgetCollection____ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection____", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection____ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection____");
			_alreadyFetchedRoomControlWidgetCollection____ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection____");

			_roomControlWidgetCollection_____ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection_____", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection_____ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection_____");
			_alreadyFetchedRoomControlWidgetCollection_____ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection_____");

			_roomControlWidgetCollection______ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection______", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection______ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection______");
			_alreadyFetchedRoomControlWidgetCollection______ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection______");

			_roomControlWidgetCollection_______ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection_______", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection_______ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection_______");
			_alreadyFetchedRoomControlWidgetCollection_______ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection_______");

			_roomControlWidgetCollection________ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection________", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection________ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection________");
			_alreadyFetchedRoomControlWidgetCollection________ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection________");

			_roomControlWidgetCollection_________ = (Obymobi.Data.CollectionClasses.RoomControlWidgetCollection)info.GetValue("_roomControlWidgetCollection_________", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetCollection));
			_alwaysFetchRoomControlWidgetCollection_________ = info.GetBoolean("_alwaysFetchRoomControlWidgetCollection_________");
			_alreadyFetchedRoomControlWidgetCollection_________ = info.GetBoolean("_alreadyFetchedRoomControlWidgetCollection_________");
			_infraredConfigurationEntity = (InfraredConfigurationEntity)info.GetValue("_infraredConfigurationEntity", typeof(InfraredConfigurationEntity));
			if(_infraredConfigurationEntity!=null)
			{
				_infraredConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_infraredConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_infraredConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchInfraredConfigurationEntity = info.GetBoolean("_alwaysFetchInfraredConfigurationEntity");
			_alreadyFetchedInfraredConfigurationEntity = info.GetBoolean("_alreadyFetchedInfraredConfigurationEntity");

			_roomControlSectionEntity = (RoomControlSectionEntity)info.GetValue("_roomControlSectionEntity", typeof(RoomControlSectionEntity));
			if(_roomControlSectionEntity!=null)
			{
				_roomControlSectionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionEntity = info.GetBoolean("_alwaysFetchRoomControlSectionEntity");
			_alreadyFetchedRoomControlSectionEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RoomControlComponentFieldIndex)fieldIndex)
			{
				case RoomControlComponentFieldIndex.RoomControlSectionId:
					DesetupSyncRoomControlSectionEntity(true, false);
					_alreadyFetchedRoomControlSectionEntity = false;
					break;
				case RoomControlComponentFieldIndex.InfraredConfigurationId:
					DesetupSyncInfraredConfigurationEntity(true, false);
					_alreadyFetchedInfraredConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedRoomControlComponentLanguageCollection = (_roomControlComponentLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection = (_roomControlWidgetCollection.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection_ = (_roomControlWidgetCollection_.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection__ = (_roomControlWidgetCollection__.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection___ = (_roomControlWidgetCollection___.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection____ = (_roomControlWidgetCollection____.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection_____ = (_roomControlWidgetCollection_____.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection______ = (_roomControlWidgetCollection______.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection_______ = (_roomControlWidgetCollection_______.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection________ = (_roomControlWidgetCollection________.Count > 0);
			_alreadyFetchedRoomControlWidgetCollection_________ = (_roomControlWidgetCollection_________.Count > 0);
			_alreadyFetchedInfraredConfigurationEntity = (_infraredConfigurationEntity != null);
			_alreadyFetchedRoomControlSectionEntity = (_roomControlSectionEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					toReturn.Add(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
					break;
				case "RoomControlSectionEntity":
					toReturn.Add(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingRoomControlComponentId);
					break;
				case "RoomControlComponentLanguageCollection":
					toReturn.Add(Relations.RoomControlComponentLanguageEntityUsingRoomControlComponentId);
					break;
				case "RoomControlWidgetCollection":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId1);
					break;
				case "RoomControlWidgetCollection_":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId10);
					break;
				case "RoomControlWidgetCollection__":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId2);
					break;
				case "RoomControlWidgetCollection___":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId3);
					break;
				case "RoomControlWidgetCollection____":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId4);
					break;
				case "RoomControlWidgetCollection_____":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId5);
					break;
				case "RoomControlWidgetCollection______":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId6);
					break;
				case "RoomControlWidgetCollection_______":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId7);
					break;
				case "RoomControlWidgetCollection________":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId8);
					break;
				case "RoomControlWidgetCollection_________":
					toReturn.Add(Relations.RoomControlWidgetEntityUsingRoomControlComponentId9);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_roomControlComponentLanguageCollection", (!this.MarkedForDeletion?_roomControlComponentLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlComponentLanguageCollection", _alwaysFetchRoomControlComponentLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlComponentLanguageCollection", _alreadyFetchedRoomControlComponentLanguageCollection);
			info.AddValue("_roomControlWidgetCollection", (!this.MarkedForDeletion?_roomControlWidgetCollection:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection", _alwaysFetchRoomControlWidgetCollection);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection", _alreadyFetchedRoomControlWidgetCollection);
			info.AddValue("_roomControlWidgetCollection_", (!this.MarkedForDeletion?_roomControlWidgetCollection_:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection_", _alwaysFetchRoomControlWidgetCollection_);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection_", _alreadyFetchedRoomControlWidgetCollection_);
			info.AddValue("_roomControlWidgetCollection__", (!this.MarkedForDeletion?_roomControlWidgetCollection__:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection__", _alwaysFetchRoomControlWidgetCollection__);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection__", _alreadyFetchedRoomControlWidgetCollection__);
			info.AddValue("_roomControlWidgetCollection___", (!this.MarkedForDeletion?_roomControlWidgetCollection___:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection___", _alwaysFetchRoomControlWidgetCollection___);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection___", _alreadyFetchedRoomControlWidgetCollection___);
			info.AddValue("_roomControlWidgetCollection____", (!this.MarkedForDeletion?_roomControlWidgetCollection____:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection____", _alwaysFetchRoomControlWidgetCollection____);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection____", _alreadyFetchedRoomControlWidgetCollection____);
			info.AddValue("_roomControlWidgetCollection_____", (!this.MarkedForDeletion?_roomControlWidgetCollection_____:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection_____", _alwaysFetchRoomControlWidgetCollection_____);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection_____", _alreadyFetchedRoomControlWidgetCollection_____);
			info.AddValue("_roomControlWidgetCollection______", (!this.MarkedForDeletion?_roomControlWidgetCollection______:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection______", _alwaysFetchRoomControlWidgetCollection______);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection______", _alreadyFetchedRoomControlWidgetCollection______);
			info.AddValue("_roomControlWidgetCollection_______", (!this.MarkedForDeletion?_roomControlWidgetCollection_______:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection_______", _alwaysFetchRoomControlWidgetCollection_______);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection_______", _alreadyFetchedRoomControlWidgetCollection_______);
			info.AddValue("_roomControlWidgetCollection________", (!this.MarkedForDeletion?_roomControlWidgetCollection________:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection________", _alwaysFetchRoomControlWidgetCollection________);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection________", _alreadyFetchedRoomControlWidgetCollection________);
			info.AddValue("_roomControlWidgetCollection_________", (!this.MarkedForDeletion?_roomControlWidgetCollection_________:null));
			info.AddValue("_alwaysFetchRoomControlWidgetCollection_________", _alwaysFetchRoomControlWidgetCollection_________);
			info.AddValue("_alreadyFetchedRoomControlWidgetCollection_________", _alreadyFetchedRoomControlWidgetCollection_________);
			info.AddValue("_infraredConfigurationEntity", (!this.MarkedForDeletion?_infraredConfigurationEntity:null));
			info.AddValue("_infraredConfigurationEntityReturnsNewIfNotFound", _infraredConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInfraredConfigurationEntity", _alwaysFetchInfraredConfigurationEntity);
			info.AddValue("_alreadyFetchedInfraredConfigurationEntity", _alreadyFetchedInfraredConfigurationEntity);
			info.AddValue("_roomControlSectionEntity", (!this.MarkedForDeletion?_roomControlSectionEntity:null));
			info.AddValue("_roomControlSectionEntityReturnsNewIfNotFound", _roomControlSectionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionEntity", _alwaysFetchRoomControlSectionEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionEntity", _alreadyFetchedRoomControlSectionEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "InfraredConfigurationEntity":
					_alreadyFetchedInfraredConfigurationEntity = true;
					this.InfraredConfigurationEntity = (InfraredConfigurationEntity)entity;
					break;
				case "RoomControlSectionEntity":
					_alreadyFetchedRoomControlSectionEntity = true;
					this.RoomControlSectionEntity = (RoomControlSectionEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "RoomControlComponentLanguageCollection":
					_alreadyFetchedRoomControlComponentLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlComponentLanguageCollection.Add((RoomControlComponentLanguageEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection":
					_alreadyFetchedRoomControlWidgetCollection = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection_":
					_alreadyFetchedRoomControlWidgetCollection_ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection_.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection__":
					_alreadyFetchedRoomControlWidgetCollection__ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection__.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection___":
					_alreadyFetchedRoomControlWidgetCollection___ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection___.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection____":
					_alreadyFetchedRoomControlWidgetCollection____ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection____.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection_____":
					_alreadyFetchedRoomControlWidgetCollection_____ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection_____.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection______":
					_alreadyFetchedRoomControlWidgetCollection______ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection______.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection_______":
					_alreadyFetchedRoomControlWidgetCollection_______ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection_______.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection________":
					_alreadyFetchedRoomControlWidgetCollection________ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection________.Add((RoomControlWidgetEntity)entity);
					}
					break;
				case "RoomControlWidgetCollection_________":
					_alreadyFetchedRoomControlWidgetCollection_________ = true;
					if(entity!=null)
					{
						this.RoomControlWidgetCollection_________.Add((RoomControlWidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					SetupSyncInfraredConfigurationEntity(relatedEntity);
					break;
				case "RoomControlSectionEntity":
					SetupSyncRoomControlSectionEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "RoomControlComponentLanguageCollection":
					_roomControlComponentLanguageCollection.Add((RoomControlComponentLanguageEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection":
					_roomControlWidgetCollection.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection_":
					_roomControlWidgetCollection_.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection__":
					_roomControlWidgetCollection__.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection___":
					_roomControlWidgetCollection___.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection____":
					_roomControlWidgetCollection____.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection_____":
					_roomControlWidgetCollection_____.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection______":
					_roomControlWidgetCollection______.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection_______":
					_roomControlWidgetCollection_______.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection________":
					_roomControlWidgetCollection________.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				case "RoomControlWidgetCollection_________":
					_roomControlWidgetCollection_________.Add((RoomControlWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					DesetupSyncInfraredConfigurationEntity(false, true);
					break;
				case "RoomControlSectionEntity":
					DesetupSyncRoomControlSectionEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlComponentLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlComponentLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection_":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection__":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection__, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection___":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection___, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection____":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection____, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection_____":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection_____, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection______":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection______, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection_______":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection_______, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection________":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection________, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetCollection_________":
					this.PerformRelatedEntityRemoval(_roomControlWidgetCollection_________, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_infraredConfigurationEntity!=null)
			{
				toReturn.Add(_infraredConfigurationEntity);
			}
			if(_roomControlSectionEntity!=null)
			{
				toReturn.Add(_roomControlSectionEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_roomControlComponentLanguageCollection);
			toReturn.Add(_roomControlWidgetCollection);
			toReturn.Add(_roomControlWidgetCollection_);
			toReturn.Add(_roomControlWidgetCollection__);
			toReturn.Add(_roomControlWidgetCollection___);
			toReturn.Add(_roomControlWidgetCollection____);
			toReturn.Add(_roomControlWidgetCollection_____);
			toReturn.Add(_roomControlWidgetCollection______);
			toReturn.Add(_roomControlWidgetCollection_______);
			toReturn.Add(_roomControlWidgetCollection________);
			toReturn.Add(_roomControlWidgetCollection_________);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlComponentId)
		{
			return FetchUsingPK(roomControlComponentId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlComponentId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(roomControlComponentId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlComponentId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(roomControlComponentId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlComponentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(roomControlComponentId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RoomControlComponentId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RoomControlComponentRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlComponentLanguageCollection(forceFetch, _roomControlComponentLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlComponentLanguageCollection(forceFetch, _roomControlComponentLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlComponentLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlComponentLanguageCollection || forceFetch || _alwaysFetchRoomControlComponentLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlComponentLanguageCollection);
				_roomControlComponentLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlComponentLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlComponentLanguageCollection.GetMultiManyToOne(null, this, filter);
				_roomControlComponentLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlComponentLanguageCollection = true;
			}
			return _roomControlComponentLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlComponentLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlComponentLanguageCollection is requested or GetMultiRoomControlComponentLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlComponentLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlComponentLanguageCollection.SortClauses=sortClauses;
			_roomControlComponentLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, _roomControlWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection || forceFetch || _alwaysFetchRoomControlWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection);
				_roomControlWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_roomControlWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection = true;
			}
			return _roomControlWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection is requested or GetMultiRoomControlWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection.SortClauses=sortClauses;
			_roomControlWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection_(forceFetch, _roomControlWidgetCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection_(forceFetch, _roomControlWidgetCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection_ || forceFetch || _alwaysFetchRoomControlWidgetCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection_);
				_roomControlWidgetCollection_.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection_.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection_.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, null, filter);
				_roomControlWidgetCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection_ = true;
			}
			return _roomControlWidgetCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection_'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection_ is requested or GetMultiRoomControlWidgetCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection_.SortClauses=sortClauses;
			_roomControlWidgetCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection__(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection__(forceFetch, _roomControlWidgetCollection__.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection__(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection__(forceFetch, _roomControlWidgetCollection__.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection__(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection__(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection__ || forceFetch || _alwaysFetchRoomControlWidgetCollection__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection__);
				_roomControlWidgetCollection__.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection__.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection__.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_roomControlWidgetCollection__.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection__ = true;
			}
			return _roomControlWidgetCollection__;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection__'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection__ is requested or GetMultiRoomControlWidgetCollection__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection__.SortClauses=sortClauses;
			_roomControlWidgetCollection__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection___(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection___(forceFetch, _roomControlWidgetCollection___.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection___(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection___(forceFetch, _roomControlWidgetCollection___.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection___(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection___(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection___ || forceFetch || _alwaysFetchRoomControlWidgetCollection___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection___);
				_roomControlWidgetCollection___.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection___.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection___.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, null, filter);
				_roomControlWidgetCollection___.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection___ = true;
			}
			return _roomControlWidgetCollection___;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection___'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection___ is requested or GetMultiRoomControlWidgetCollection___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection___.SortClauses=sortClauses;
			_roomControlWidgetCollection___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection____(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection____(forceFetch, _roomControlWidgetCollection____.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection____(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection____(forceFetch, _roomControlWidgetCollection____.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection____(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection____(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection____ || forceFetch || _alwaysFetchRoomControlWidgetCollection____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection____);
				_roomControlWidgetCollection____.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection____.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection____.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_roomControlWidgetCollection____.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection____ = true;
			}
			return _roomControlWidgetCollection____;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection____'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection____ is requested or GetMultiRoomControlWidgetCollection____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection____.SortClauses=sortClauses;
			_roomControlWidgetCollection____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_____(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection_____(forceFetch, _roomControlWidgetCollection_____.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_____(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection_____(forceFetch, _roomControlWidgetCollection_____.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection_____(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_____(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection_____ || forceFetch || _alwaysFetchRoomControlWidgetCollection_____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection_____);
				_roomControlWidgetCollection_____.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection_____.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection_____.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_roomControlWidgetCollection_____.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection_____ = true;
			}
			return _roomControlWidgetCollection_____;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection_____'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection_____ is requested or GetMultiRoomControlWidgetCollection_____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection_____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection_____.SortClauses=sortClauses;
			_roomControlWidgetCollection_____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection______(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection______(forceFetch, _roomControlWidgetCollection______.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection______(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection______(forceFetch, _roomControlWidgetCollection______.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection______(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection______(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection______(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection______ || forceFetch || _alwaysFetchRoomControlWidgetCollection______) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection______);
				_roomControlWidgetCollection______.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection______.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection______.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, null, null, filter);
				_roomControlWidgetCollection______.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection______ = true;
			}
			return _roomControlWidgetCollection______;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection______'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection______ is requested or GetMultiRoomControlWidgetCollection______ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection______(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection______.SortClauses=sortClauses;
			_roomControlWidgetCollection______.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_______(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection_______(forceFetch, _roomControlWidgetCollection_______.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_______(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection_______(forceFetch, _roomControlWidgetCollection_______.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_______(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection_______(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_______(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection_______ || forceFetch || _alwaysFetchRoomControlWidgetCollection_______) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection_______);
				_roomControlWidgetCollection_______.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection_______.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection_______.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, null, null, filter);
				_roomControlWidgetCollection_______.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection_______ = true;
			}
			return _roomControlWidgetCollection_______;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection_______'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection_______ is requested or GetMultiRoomControlWidgetCollection_______ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection_______(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection_______.SortClauses=sortClauses;
			_roomControlWidgetCollection_______.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection________(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection________(forceFetch, _roomControlWidgetCollection________.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection________(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection________(forceFetch, _roomControlWidgetCollection________.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection________(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection________(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection________(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection________ || forceFetch || _alwaysFetchRoomControlWidgetCollection________) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection________);
				_roomControlWidgetCollection________.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection________.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection________.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_roomControlWidgetCollection________.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection________ = true;
			}
			return _roomControlWidgetCollection________;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection________'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection________ is requested or GetMultiRoomControlWidgetCollection________ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection________(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection________.SortClauses=sortClauses;
			_roomControlWidgetCollection________.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_________(bool forceFetch)
		{
			return GetMultiRoomControlWidgetCollection_________(forceFetch, _roomControlWidgetCollection_________.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_________(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetCollection_________(forceFetch, _roomControlWidgetCollection_________.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_________(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetCollection_________(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection GetMultiRoomControlWidgetCollection_________(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetCollection_________ || forceFetch || _alwaysFetchRoomControlWidgetCollection_________) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetCollection_________);
				_roomControlWidgetCollection_________.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetCollection_________.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetCollection_________.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, null, filter);
				_roomControlWidgetCollection_________.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetCollection_________ = true;
			}
			return _roomControlWidgetCollection_________;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetCollection_________'. These settings will be taken into account
		/// when the property RoomControlWidgetCollection_________ is requested or GetMultiRoomControlWidgetCollection_________ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetCollection_________(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetCollection_________.SortClauses=sortClauses;
			_roomControlWidgetCollection_________.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public InfraredConfigurationEntity GetSingleInfraredConfigurationEntity()
		{
			return GetSingleInfraredConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public virtual InfraredConfigurationEntity GetSingleInfraredConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedInfraredConfigurationEntity || forceFetch || _alwaysFetchInfraredConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
				InfraredConfigurationEntity newEntity = new InfraredConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InfraredConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InfraredConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_infraredConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InfraredConfigurationEntity = newEntity;
				_alreadyFetchedInfraredConfigurationEntity = fetchResult;
			}
			return _infraredConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public RoomControlSectionEntity GetSingleRoomControlSectionEntity()
		{
			return GetSingleRoomControlSectionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionEntity GetSingleRoomControlSectionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionEntity || forceFetch || _alwaysFetchRoomControlSectionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
				RoomControlSectionEntity newEntity = new RoomControlSectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionEntity = newEntity;
				_alreadyFetchedRoomControlSectionEntity = fetchResult;
			}
			return _roomControlSectionEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("InfraredConfigurationEntity", _infraredConfigurationEntity);
			toReturn.Add("RoomControlSectionEntity", _roomControlSectionEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("RoomControlComponentLanguageCollection", _roomControlComponentLanguageCollection);
			toReturn.Add("RoomControlWidgetCollection", _roomControlWidgetCollection);
			toReturn.Add("RoomControlWidgetCollection_", _roomControlWidgetCollection_);
			toReturn.Add("RoomControlWidgetCollection__", _roomControlWidgetCollection__);
			toReturn.Add("RoomControlWidgetCollection___", _roomControlWidgetCollection___);
			toReturn.Add("RoomControlWidgetCollection____", _roomControlWidgetCollection____);
			toReturn.Add("RoomControlWidgetCollection_____", _roomControlWidgetCollection_____);
			toReturn.Add("RoomControlWidgetCollection______", _roomControlWidgetCollection______);
			toReturn.Add("RoomControlWidgetCollection_______", _roomControlWidgetCollection_______);
			toReturn.Add("RoomControlWidgetCollection________", _roomControlWidgetCollection________);
			toReturn.Add("RoomControlWidgetCollection_________", _roomControlWidgetCollection_________);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="validator">The validator object for this RoomControlComponentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 roomControlComponentId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(roomControlComponentId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "RoomControlComponentEntity");

			_roomControlComponentLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection();
			_roomControlComponentLanguageCollection.SetContainingEntityInfo(this, "RoomControlComponentEntity");

			_roomControlWidgetCollection = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection.SetContainingEntityInfo(this, "RoomControlComponentEntity");

			_roomControlWidgetCollection_ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection_.SetContainingEntityInfo(this, "RoomControlComponentEntity_");

			_roomControlWidgetCollection__ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection__.SetContainingEntityInfo(this, "RoomControlComponentEntity__");

			_roomControlWidgetCollection___ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection___.SetContainingEntityInfo(this, "RoomControlComponentEntity___");

			_roomControlWidgetCollection____ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection____.SetContainingEntityInfo(this, "RoomControlComponentEntity____");

			_roomControlWidgetCollection_____ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection_____.SetContainingEntityInfo(this, "RoomControlComponentEntity_____");

			_roomControlWidgetCollection______ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection______.SetContainingEntityInfo(this, "RoomControlComponentEntity______");

			_roomControlWidgetCollection_______ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection_______.SetContainingEntityInfo(this, "RoomControlComponentEntity_______");

			_roomControlWidgetCollection________ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection________.SetContainingEntityInfo(this, "RoomControlComponentEntity________");

			_roomControlWidgetCollection_________ = new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection();
			_roomControlWidgetCollection_________.SetContainingEntityInfo(this, "RoomControlComponentEntity_________");
			_infraredConfigurationEntityReturnsNewIfNotFound = true;
			_roomControlSectionEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NameSystem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfraredConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ControllerId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInfraredConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlComponentRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, signalRelatedEntity, "RoomControlComponentCollection", resetFKFields, new int[] { (int)RoomControlComponentFieldIndex.InfraredConfigurationId } );		
			_infraredConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInfraredConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_infraredConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncInfraredConfigurationEntity(true, true);
				_infraredConfigurationEntity = (InfraredConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlComponentRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, ref _alreadyFetchedInfraredConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInfraredConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticRoomControlComponentRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, signalRelatedEntity, "RoomControlComponentCollection", resetFKFields, new int[] { (int)RoomControlComponentFieldIndex.RoomControlSectionId } );		
			_roomControlSectionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionEntity(true, true);
				_roomControlSectionEntity = (RoomControlSectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticRoomControlComponentRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, ref _alreadyFetchedRoomControlSectionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 roomControlComponentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RoomControlComponentFieldIndex.RoomControlComponentId].ForcedCurrentValueWrite(roomControlComponentId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoomControlComponentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RoomControlComponentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RoomControlComponentRelations Relations
		{
			get	{ return new RoomControlComponentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponentLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentLanguageCollection")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlComponentLanguageEntity, 0, null, null, null, "RoomControlComponentLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection_")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection__
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection__")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection___
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection___")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection____
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection____")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection_____
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection_____")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection_____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection______
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection______")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection______", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection_______
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection_______")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection_______", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection________
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection________")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection________", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetCollection_________
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetCollection_________")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, 0, null, null, null, "RoomControlWidgetCollection_________", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InfraredConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInfraredConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.InfraredConfigurationCollection(), (IEntityRelation)GetRelationsForField("InfraredConfigurationEntity")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, 0, null, null, null, "InfraredConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionEntity")[0], (int)Obymobi.Data.EntityType.RoomControlComponentEntity, (int)Obymobi.Data.EntityType.RoomControlSectionEntity, 0, null, null, null, "RoomControlSectionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RoomControlComponentId property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."RoomControlComponentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoomControlComponentId
		{
			get { return (System.Int32)GetValue((int)RoomControlComponentFieldIndex.RoomControlComponentId, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.RoomControlComponentId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlComponentFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The RoomControlSectionId property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."RoomControlSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlComponentFieldIndex.RoomControlSectionId, false); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.RoomControlSectionId, value, true); }
		}

		/// <summary> The Name property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.Name, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.Name, value, true); }
		}

		/// <summary> The NameSystem property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."NameSystem"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NameSystem
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.NameSystem, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.NameSystem, value, true); }
		}

		/// <summary> The Type property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.RoomControlComponentType Type
		{
			get { return (Obymobi.Enums.RoomControlComponentType)GetValue((int)RoomControlComponentFieldIndex.Type, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.Type, value, true); }
		}

		/// <summary> The SortOrder property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)RoomControlComponentFieldIndex.SortOrder, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The Visible property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)RoomControlComponentFieldIndex.Visible, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.Visible, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlComponentFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlComponentFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlComponentFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlComponentFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The InfraredConfigurationId property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."InfraredConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InfraredConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlComponentFieldIndex.InfraredConfigurationId, false); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.InfraredConfigurationId, value, true); }
		}

		/// <summary> The ControllerId property of the Entity RoomControlComponent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlComponent"."ControllerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ControllerId
		{
			get { return (System.String)GetValue((int)RoomControlComponentFieldIndex.ControllerId, true); }
			set	{ SetValue((int)RoomControlComponentFieldIndex.ControllerId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlComponentLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection RoomControlComponentLanguageCollection
		{
			get	{ return GetMultiRoomControlComponentLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentLanguageCollection. When set to true, RoomControlComponentLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlComponentLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlComponentLanguageCollection; }
			set	{ _alwaysFetchRoomControlComponentLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentLanguageCollection already has been fetched. Setting this property to false when RoomControlComponentLanguageCollection has been fetched
		/// will clear the RoomControlComponentLanguageCollection collection well. Setting this property to true while RoomControlComponentLanguageCollection hasn't been fetched disables lazy loading for RoomControlComponentLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentLanguageCollection
		{
			get { return _alreadyFetchedRoomControlComponentLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentLanguageCollection && !value && (_roomControlComponentLanguageCollection != null))
				{
					_roomControlComponentLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlComponentLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection
		{
			get	{ return GetMultiRoomControlWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection. When set to true, RoomControlWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection; }
			set	{ _alwaysFetchRoomControlWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection already has been fetched. Setting this property to false when RoomControlWidgetCollection has been fetched
		/// will clear the RoomControlWidgetCollection collection well. Setting this property to true while RoomControlWidgetCollection hasn't been fetched disables lazy loading for RoomControlWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection
		{
			get { return _alreadyFetchedRoomControlWidgetCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection && !value && (_roomControlWidgetCollection != null))
				{
					_roomControlWidgetCollection.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection_
		{
			get	{ return GetMultiRoomControlWidgetCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection_. When set to true, RoomControlWidgetCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection_
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection_; }
			set	{ _alwaysFetchRoomControlWidgetCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection_ already has been fetched. Setting this property to false when RoomControlWidgetCollection_ has been fetched
		/// will clear the RoomControlWidgetCollection_ collection well. Setting this property to true while RoomControlWidgetCollection_ hasn't been fetched disables lazy loading for RoomControlWidgetCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection_
		{
			get { return _alreadyFetchedRoomControlWidgetCollection_;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection_ && !value && (_roomControlWidgetCollection_ != null))
				{
					_roomControlWidgetCollection_.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection__
		{
			get	{ return GetMultiRoomControlWidgetCollection__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection__. When set to true, RoomControlWidgetCollection__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection__ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection__
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection__; }
			set	{ _alwaysFetchRoomControlWidgetCollection__ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection__ already has been fetched. Setting this property to false when RoomControlWidgetCollection__ has been fetched
		/// will clear the RoomControlWidgetCollection__ collection well. Setting this property to true while RoomControlWidgetCollection__ hasn't been fetched disables lazy loading for RoomControlWidgetCollection__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection__
		{
			get { return _alreadyFetchedRoomControlWidgetCollection__;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection__ && !value && (_roomControlWidgetCollection__ != null))
				{
					_roomControlWidgetCollection__.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection__ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection___
		{
			get	{ return GetMultiRoomControlWidgetCollection___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection___. When set to true, RoomControlWidgetCollection___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection___ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection___
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection___; }
			set	{ _alwaysFetchRoomControlWidgetCollection___ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection___ already has been fetched. Setting this property to false when RoomControlWidgetCollection___ has been fetched
		/// will clear the RoomControlWidgetCollection___ collection well. Setting this property to true while RoomControlWidgetCollection___ hasn't been fetched disables lazy loading for RoomControlWidgetCollection___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection___
		{
			get { return _alreadyFetchedRoomControlWidgetCollection___;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection___ && !value && (_roomControlWidgetCollection___ != null))
				{
					_roomControlWidgetCollection___.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection___ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection____
		{
			get	{ return GetMultiRoomControlWidgetCollection____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection____. When set to true, RoomControlWidgetCollection____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection____ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection____
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection____; }
			set	{ _alwaysFetchRoomControlWidgetCollection____ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection____ already has been fetched. Setting this property to false when RoomControlWidgetCollection____ has been fetched
		/// will clear the RoomControlWidgetCollection____ collection well. Setting this property to true while RoomControlWidgetCollection____ hasn't been fetched disables lazy loading for RoomControlWidgetCollection____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection____
		{
			get { return _alreadyFetchedRoomControlWidgetCollection____;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection____ && !value && (_roomControlWidgetCollection____ != null))
				{
					_roomControlWidgetCollection____.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection____ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection_____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection_____
		{
			get	{ return GetMultiRoomControlWidgetCollection_____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection_____. When set to true, RoomControlWidgetCollection_____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection_____ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection_____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection_____
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection_____; }
			set	{ _alwaysFetchRoomControlWidgetCollection_____ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection_____ already has been fetched. Setting this property to false when RoomControlWidgetCollection_____ has been fetched
		/// will clear the RoomControlWidgetCollection_____ collection well. Setting this property to true while RoomControlWidgetCollection_____ hasn't been fetched disables lazy loading for RoomControlWidgetCollection_____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection_____
		{
			get { return _alreadyFetchedRoomControlWidgetCollection_____;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection_____ && !value && (_roomControlWidgetCollection_____ != null))
				{
					_roomControlWidgetCollection_____.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection_____ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection______()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection______
		{
			get	{ return GetMultiRoomControlWidgetCollection______(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection______. When set to true, RoomControlWidgetCollection______ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection______ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection______(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection______
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection______; }
			set	{ _alwaysFetchRoomControlWidgetCollection______ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection______ already has been fetched. Setting this property to false when RoomControlWidgetCollection______ has been fetched
		/// will clear the RoomControlWidgetCollection______ collection well. Setting this property to true while RoomControlWidgetCollection______ hasn't been fetched disables lazy loading for RoomControlWidgetCollection______</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection______
		{
			get { return _alreadyFetchedRoomControlWidgetCollection______;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection______ && !value && (_roomControlWidgetCollection______ != null))
				{
					_roomControlWidgetCollection______.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection______ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection_______()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection_______
		{
			get	{ return GetMultiRoomControlWidgetCollection_______(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection_______. When set to true, RoomControlWidgetCollection_______ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection_______ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection_______(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection_______
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection_______; }
			set	{ _alwaysFetchRoomControlWidgetCollection_______ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection_______ already has been fetched. Setting this property to false when RoomControlWidgetCollection_______ has been fetched
		/// will clear the RoomControlWidgetCollection_______ collection well. Setting this property to true while RoomControlWidgetCollection_______ hasn't been fetched disables lazy loading for RoomControlWidgetCollection_______</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection_______
		{
			get { return _alreadyFetchedRoomControlWidgetCollection_______;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection_______ && !value && (_roomControlWidgetCollection_______ != null))
				{
					_roomControlWidgetCollection_______.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection_______ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection________()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection________
		{
			get	{ return GetMultiRoomControlWidgetCollection________(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection________. When set to true, RoomControlWidgetCollection________ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection________ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection________(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection________
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection________; }
			set	{ _alwaysFetchRoomControlWidgetCollection________ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection________ already has been fetched. Setting this property to false when RoomControlWidgetCollection________ has been fetched
		/// will clear the RoomControlWidgetCollection________ collection well. Setting this property to true while RoomControlWidgetCollection________ hasn't been fetched disables lazy loading for RoomControlWidgetCollection________</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection________
		{
			get { return _alreadyFetchedRoomControlWidgetCollection________;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection________ && !value && (_roomControlWidgetCollection________ != null))
				{
					_roomControlWidgetCollection________.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection________ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetCollection_________()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetCollection RoomControlWidgetCollection_________
		{
			get	{ return GetMultiRoomControlWidgetCollection_________(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetCollection_________. When set to true, RoomControlWidgetCollection_________ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetCollection_________ is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetCollection_________(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetCollection_________
		{
			get	{ return _alwaysFetchRoomControlWidgetCollection_________; }
			set	{ _alwaysFetchRoomControlWidgetCollection_________ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetCollection_________ already has been fetched. Setting this property to false when RoomControlWidgetCollection_________ has been fetched
		/// will clear the RoomControlWidgetCollection_________ collection well. Setting this property to true while RoomControlWidgetCollection_________ hasn't been fetched disables lazy loading for RoomControlWidgetCollection_________</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetCollection_________
		{
			get { return _alreadyFetchedRoomControlWidgetCollection_________;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetCollection_________ && !value && (_roomControlWidgetCollection_________ != null))
				{
					_roomControlWidgetCollection_________.Clear();
				}
				_alreadyFetchedRoomControlWidgetCollection_________ = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'InfraredConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInfraredConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual InfraredConfigurationEntity InfraredConfigurationEntity
		{
			get	{ return GetSingleInfraredConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInfraredConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlComponentCollection", "InfraredConfigurationEntity", _infraredConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InfraredConfigurationEntity. When set to true, InfraredConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InfraredConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleInfraredConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInfraredConfigurationEntity
		{
			get	{ return _alwaysFetchInfraredConfigurationEntity; }
			set	{ _alwaysFetchInfraredConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InfraredConfigurationEntity already has been fetched. Setting this property to false when InfraredConfigurationEntity has been fetched
		/// will set InfraredConfigurationEntity to null as well. Setting this property to true while InfraredConfigurationEntity hasn't been fetched disables lazy loading for InfraredConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInfraredConfigurationEntity
		{
			get { return _alreadyFetchedInfraredConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedInfraredConfigurationEntity && !value)
				{
					this.InfraredConfigurationEntity = null;
				}
				_alreadyFetchedInfraredConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InfraredConfigurationEntity is not found
		/// in the database. When set to true, InfraredConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool InfraredConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _infraredConfigurationEntityReturnsNewIfNotFound; }
			set { _infraredConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionEntity RoomControlSectionEntity
		{
			get	{ return GetSingleRoomControlSectionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlComponentCollection", "RoomControlSectionEntity", _roomControlSectionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionEntity. When set to true, RoomControlSectionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionEntity
		{
			get	{ return _alwaysFetchRoomControlSectionEntity; }
			set	{ _alwaysFetchRoomControlSectionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionEntity already has been fetched. Setting this property to false when RoomControlSectionEntity has been fetched
		/// will set RoomControlSectionEntity to null as well. Setting this property to true while RoomControlSectionEntity hasn't been fetched disables lazy loading for RoomControlSectionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionEntity
		{
			get { return _alreadyFetchedRoomControlSectionEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionEntity && !value)
				{
					this.RoomControlSectionEntity = null;
				}
				_alreadyFetchedRoomControlSectionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionEntity is not found
		/// in the database. When set to true, RoomControlSectionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionEntityReturnsNewIfNotFound; }
			set { _roomControlSectionEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RoomControlComponentEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
