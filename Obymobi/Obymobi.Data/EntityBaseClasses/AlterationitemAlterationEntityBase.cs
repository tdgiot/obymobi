﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'AlterationitemAlteration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AlterationitemAlterationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AlterationitemAlterationEntity"; }
		}
	
		#region Class Member Declarations
		private AlterationEntity _alterationEntity;
		private bool	_alwaysFetchAlterationEntity, _alreadyFetchedAlterationEntity, _alterationEntityReturnsNewIfNotFound;
		private AlterationitemEntity _alterationitemEntity;
		private bool	_alwaysFetchAlterationitemEntity, _alreadyFetchedAlterationitemEntity, _alterationitemEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationEntity</summary>
			public static readonly string AlterationEntity = "AlterationEntity";
			/// <summary>Member name AlterationitemEntity</summary>
			public static readonly string AlterationitemEntity = "AlterationitemEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AlterationitemAlterationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AlterationitemAlterationEntityBase() :base("AlterationitemAlterationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		protected AlterationitemAlterationEntityBase(System.Int32 alterationitemAlterationId):base("AlterationitemAlterationEntity")
		{
			InitClassFetch(alterationitemAlterationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AlterationitemAlterationEntityBase(System.Int32 alterationitemAlterationId, IPrefetchPath prefetchPathToUse): base("AlterationitemAlterationEntity")
		{
			InitClassFetch(alterationitemAlterationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="validator">The custom validator object for this AlterationitemAlterationEntity</param>
		protected AlterationitemAlterationEntityBase(System.Int32 alterationitemAlterationId, IValidator validator):base("AlterationitemAlterationEntity")
		{
			InitClassFetch(alterationitemAlterationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationitemAlterationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationEntity = (AlterationEntity)info.GetValue("_alterationEntity", typeof(AlterationEntity));
			if(_alterationEntity!=null)
			{
				_alterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationEntityReturnsNewIfNotFound = info.GetBoolean("_alterationEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationEntity = info.GetBoolean("_alwaysFetchAlterationEntity");
			_alreadyFetchedAlterationEntity = info.GetBoolean("_alreadyFetchedAlterationEntity");

			_alterationitemEntity = (AlterationitemEntity)info.GetValue("_alterationitemEntity", typeof(AlterationitemEntity));
			if(_alterationitemEntity!=null)
			{
				_alterationitemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationitemEntityReturnsNewIfNotFound = info.GetBoolean("_alterationitemEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationitemEntity = info.GetBoolean("_alwaysFetchAlterationitemEntity");
			_alreadyFetchedAlterationitemEntity = info.GetBoolean("_alreadyFetchedAlterationitemEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AlterationitemAlterationFieldIndex)fieldIndex)
			{
				case AlterationitemAlterationFieldIndex.AlterationitemId:
					DesetupSyncAlterationitemEntity(true, false);
					_alreadyFetchedAlterationitemEntity = false;
					break;
				case AlterationitemAlterationFieldIndex.AlterationId:
					DesetupSyncAlterationEntity(true, false);
					_alreadyFetchedAlterationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationEntity = (_alterationEntity != null);
			_alreadyFetchedAlterationitemEntity = (_alterationitemEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationId);
					break;
				case "AlterationitemEntity":
					toReturn.Add(Relations.AlterationitemEntityUsingAlterationitemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationEntity", (!this.MarkedForDeletion?_alterationEntity:null));
			info.AddValue("_alterationEntityReturnsNewIfNotFound", _alterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationEntity", _alwaysFetchAlterationEntity);
			info.AddValue("_alreadyFetchedAlterationEntity", _alreadyFetchedAlterationEntity);
			info.AddValue("_alterationitemEntity", (!this.MarkedForDeletion?_alterationitemEntity:null));
			info.AddValue("_alterationitemEntityReturnsNewIfNotFound", _alterationitemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationitemEntity", _alwaysFetchAlterationitemEntity);
			info.AddValue("_alreadyFetchedAlterationitemEntity", _alreadyFetchedAlterationitemEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationEntity":
					_alreadyFetchedAlterationEntity = true;
					this.AlterationEntity = (AlterationEntity)entity;
					break;
				case "AlterationitemEntity":
					_alreadyFetchedAlterationitemEntity = true;
					this.AlterationitemEntity = (AlterationitemEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					SetupSyncAlterationEntity(relatedEntity);
					break;
				case "AlterationitemEntity":
					SetupSyncAlterationitemEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					DesetupSyncAlterationEntity(false, true);
					break;
				case "AlterationitemEntity":
					DesetupSyncAlterationitemEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_alterationEntity!=null)
			{
				toReturn.Add(_alterationEntity);
			}
			if(_alterationitemEntity!=null)
			{
				toReturn.Add(_alterationitemEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alterationitemId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="alterationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlterationitemIdAlterationId(System.Int32 alterationitemId, System.Int32 alterationId)
		{
			return FetchUsingUCAlterationitemIdAlterationId( alterationitemId,  alterationId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alterationitemId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="alterationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlterationitemIdAlterationId(System.Int32 alterationitemId, System.Int32 alterationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAlterationitemIdAlterationId( alterationitemId,  alterationId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alterationitemId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="alterationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlterationitemIdAlterationId(System.Int32 alterationitemId, System.Int32 alterationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAlterationitemIdAlterationId( alterationitemId,  alterationId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alterationitemId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="alterationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlterationitemIdAlterationId(System.Int32 alterationitemId, System.Int32 alterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((AlterationitemAlterationDAO)CreateDAOInstance()).FetchAlterationitemAlterationUsingUCAlterationitemIdAlterationId(this, this.Transaction, alterationitemId, alterationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemAlterationId)
		{
			return FetchUsingPK(alterationitemAlterationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemAlterationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(alterationitemAlterationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemAlterationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(alterationitemAlterationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationitemAlterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(alterationitemAlterationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AlterationitemAlterationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AlterationitemAlterationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleAlterationEntity()
		{
			return GetSingleAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationEntity || forceFetch || _alwaysFetchAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationId);
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationEntity = newEntity;
				_alreadyFetchedAlterationEntity = fetchResult;
			}
			return _alterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationitemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationitemEntity' which is related to this entity.</returns>
		public AlterationitemEntity GetSingleAlterationitemEntity()
		{
			return GetSingleAlterationitemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationitemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationitemEntity' which is related to this entity.</returns>
		public virtual AlterationitemEntity GetSingleAlterationitemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationitemEntity || forceFetch || _alwaysFetchAlterationitemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationitemEntityUsingAlterationitemId);
				AlterationitemEntity newEntity = new AlterationitemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationitemId);
				}
				if(fetchResult)
				{
					newEntity = (AlterationitemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationitemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationitemEntity = newEntity;
				_alreadyFetchedAlterationitemEntity = fetchResult;
			}
			return _alterationitemEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationEntity", _alterationEntity);
			toReturn.Add("AlterationitemEntity", _alterationitemEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="validator">The validator object for this AlterationitemAlterationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 alterationitemAlterationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(alterationitemAlterationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_alterationEntityReturnsNewIfNotFound = true;
			_alterationitemEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationitemAlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _alterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationitemAlterationRelations.AlterationEntityUsingAlterationIdStatic, true, signalRelatedEntity, "AlterationitemAlterationCollection", resetFKFields, new int[] { (int)AlterationitemAlterationFieldIndex.AlterationId } );		
			_alterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationEntity(IEntityCore relatedEntity)
		{
			if(_alterationEntity!=relatedEntity)
			{		
				DesetupSyncAlterationEntity(true, true);
				_alterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationitemAlterationRelations.AlterationEntityUsingAlterationIdStatic, true, ref _alreadyFetchedAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationitemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationitemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationitemEntity, new PropertyChangedEventHandler( OnAlterationitemEntityPropertyChanged ), "AlterationitemEntity", Obymobi.Data.RelationClasses.StaticAlterationitemAlterationRelations.AlterationitemEntityUsingAlterationitemIdStatic, true, signalRelatedEntity, "AlterationitemAlterationCollection", resetFKFields, new int[] { (int)AlterationitemAlterationFieldIndex.AlterationitemId } );		
			_alterationitemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationitemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationitemEntity(IEntityCore relatedEntity)
		{
			if(_alterationitemEntity!=relatedEntity)
			{		
				DesetupSyncAlterationitemEntity(true, true);
				_alterationitemEntity = (AlterationitemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationitemEntity, new PropertyChangedEventHandler( OnAlterationitemEntityPropertyChanged ), "AlterationitemEntity", Obymobi.Data.RelationClasses.StaticAlterationitemAlterationRelations.AlterationitemEntityUsingAlterationitemIdStatic, true, ref _alreadyFetchedAlterationitemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationitemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="alterationitemAlterationId">PK value for AlterationitemAlteration which data should be fetched into this AlterationitemAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 alterationitemAlterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AlterationitemAlterationFieldIndex.AlterationitemAlterationId].ForcedCurrentValueWrite(alterationitemAlterationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAlterationitemAlterationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AlterationitemAlterationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AlterationitemAlterationRelations Relations
		{
			get	{ return new AlterationitemAlterationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationEntity")[0], (int)Obymobi.Data.EntityType.AlterationitemAlterationEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationitem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationitemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationitemCollection(), (IEntityRelation)GetRelationsForField("AlterationitemEntity")[0], (int)Obymobi.Data.EntityType.AlterationitemAlterationEntity, (int)Obymobi.Data.EntityType.AlterationitemEntity, 0, null, null, null, "AlterationitemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlterationitemAlterationId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."AlterationitemAlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationitemAlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.AlterationitemAlterationId, true); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.AlterationitemAlterationId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemAlterationFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The AlterationitemId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."AlterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationitemId
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.AlterationitemId, true); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.AlterationitemId, value, true); }
		}

		/// <summary> The AlterationId property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.AlterationId, true); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AlterationitemAlterationFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemAlterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemAlterationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationitemAlterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity AlterationitemAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationitemAlteration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationitemAlterationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AlterationitemAlterationFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity AlterationEntity
		{
			get	{ return GetSingleAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationitemAlterationCollection", "AlterationEntity", _alterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationEntity. When set to true, AlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationEntity
		{
			get	{ return _alwaysFetchAlterationEntity; }
			set	{ _alwaysFetchAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationEntity already has been fetched. Setting this property to false when AlterationEntity has been fetched
		/// will set AlterationEntity to null as well. Setting this property to true while AlterationEntity hasn't been fetched disables lazy loading for AlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationEntity
		{
			get { return _alreadyFetchedAlterationEntity;}
			set 
			{
				if(_alreadyFetchedAlterationEntity && !value)
				{
					this.AlterationEntity = null;
				}
				_alreadyFetchedAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationEntity is not found
		/// in the database. When set to true, AlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationEntityReturnsNewIfNotFound
		{
			get	{ return _alterationEntityReturnsNewIfNotFound; }
			set { _alterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationitemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationitemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationitemEntity AlterationitemEntity
		{
			get	{ return GetSingleAlterationitemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationitemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationitemAlterationCollection", "AlterationitemEntity", _alterationitemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationitemEntity. When set to true, AlterationitemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationitemEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationitemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationitemEntity
		{
			get	{ return _alwaysFetchAlterationitemEntity; }
			set	{ _alwaysFetchAlterationitemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationitemEntity already has been fetched. Setting this property to false when AlterationitemEntity has been fetched
		/// will set AlterationitemEntity to null as well. Setting this property to true while AlterationitemEntity hasn't been fetched disables lazy loading for AlterationitemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationitemEntity
		{
			get { return _alreadyFetchedAlterationitemEntity;}
			set 
			{
				if(_alreadyFetchedAlterationitemEntity && !value)
				{
					this.AlterationitemEntity = null;
				}
				_alreadyFetchedAlterationitemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationitemEntity is not found
		/// in the database. When set to true, AlterationitemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationitemEntityReturnsNewIfNotFound
		{
			get	{ return _alterationitemEntityReturnsNewIfNotFound; }
			set { _alterationitemEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AlterationitemAlterationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
