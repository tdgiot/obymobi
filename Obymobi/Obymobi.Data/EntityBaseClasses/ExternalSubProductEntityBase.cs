﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ExternalSubProduct'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ExternalSubProductEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ExternalSubProductEntity"; }
		}
	
		#region Class Member Declarations
		private ExternalProductEntity _externalProductEntity;
		private bool	_alwaysFetchExternalProductEntity, _alreadyFetchedExternalProductEntity, _externalProductEntityReturnsNewIfNotFound;
		private ExternalProductEntity _externalProductEntity1;
		private bool	_alwaysFetchExternalProductEntity1, _alreadyFetchedExternalProductEntity1, _externalProductEntity1ReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalProductEntity</summary>
			public static readonly string ExternalProductEntity = "ExternalProductEntity";
			/// <summary>Member name ExternalProductEntity1</summary>
			public static readonly string ExternalProductEntity1 = "ExternalProductEntity1";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ExternalSubProductEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ExternalSubProductEntityBase() :base("ExternalSubProductEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		protected ExternalSubProductEntityBase(System.Int32 externalProductExternalProductId):base("ExternalSubProductEntity")
		{
			InitClassFetch(externalProductExternalProductId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ExternalSubProductEntityBase(System.Int32 externalProductExternalProductId, IPrefetchPath prefetchPathToUse): base("ExternalSubProductEntity")
		{
			InitClassFetch(externalProductExternalProductId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="validator">The custom validator object for this ExternalSubProductEntity</param>
		protected ExternalSubProductEntityBase(System.Int32 externalProductExternalProductId, IValidator validator):base("ExternalSubProductEntity")
		{
			InitClassFetch(externalProductExternalProductId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalSubProductEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_externalProductEntity = (ExternalProductEntity)info.GetValue("_externalProductEntity", typeof(ExternalProductEntity));
			if(_externalProductEntity!=null)
			{
				_externalProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalProductEntityReturnsNewIfNotFound = info.GetBoolean("_externalProductEntityReturnsNewIfNotFound");
			_alwaysFetchExternalProductEntity = info.GetBoolean("_alwaysFetchExternalProductEntity");
			_alreadyFetchedExternalProductEntity = info.GetBoolean("_alreadyFetchedExternalProductEntity");

			_externalProductEntity1 = (ExternalProductEntity)info.GetValue("_externalProductEntity1", typeof(ExternalProductEntity));
			if(_externalProductEntity1!=null)
			{
				_externalProductEntity1.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalProductEntity1ReturnsNewIfNotFound = info.GetBoolean("_externalProductEntity1ReturnsNewIfNotFound");
			_alwaysFetchExternalProductEntity1 = info.GetBoolean("_alwaysFetchExternalProductEntity1");
			_alreadyFetchedExternalProductEntity1 = info.GetBoolean("_alreadyFetchedExternalProductEntity1");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ExternalSubProductFieldIndex)fieldIndex)
			{
				case ExternalSubProductFieldIndex.ExternalProductId:
					DesetupSyncExternalProductEntity(true, false);
					_alreadyFetchedExternalProductEntity = false;
					break;
				case ExternalSubProductFieldIndex.ExternalSubProductId:
					DesetupSyncExternalProductEntity1(true, false);
					_alreadyFetchedExternalProductEntity1 = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedExternalProductEntity = (_externalProductEntity != null);
			_alreadyFetchedExternalProductEntity1 = (_externalProductEntity1 != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ExternalProductEntity":
					toReturn.Add(Relations.ExternalProductEntityUsingExternalProductId);
					break;
				case "ExternalProductEntity1":
					toReturn.Add(Relations.ExternalProductEntityUsingExternalSubProductId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_externalProductEntity", (!this.MarkedForDeletion?_externalProductEntity:null));
			info.AddValue("_externalProductEntityReturnsNewIfNotFound", _externalProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalProductEntity", _alwaysFetchExternalProductEntity);
			info.AddValue("_alreadyFetchedExternalProductEntity", _alreadyFetchedExternalProductEntity);
			info.AddValue("_externalProductEntity1", (!this.MarkedForDeletion?_externalProductEntity1:null));
			info.AddValue("_externalProductEntity1ReturnsNewIfNotFound", _externalProductEntity1ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalProductEntity1", _alwaysFetchExternalProductEntity1);
			info.AddValue("_alreadyFetchedExternalProductEntity1", _alreadyFetchedExternalProductEntity1);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ExternalProductEntity":
					_alreadyFetchedExternalProductEntity = true;
					this.ExternalProductEntity = (ExternalProductEntity)entity;
					break;
				case "ExternalProductEntity1":
					_alreadyFetchedExternalProductEntity1 = true;
					this.ExternalProductEntity1 = (ExternalProductEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ExternalProductEntity":
					SetupSyncExternalProductEntity(relatedEntity);
					break;
				case "ExternalProductEntity1":
					SetupSyncExternalProductEntity1(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ExternalProductEntity":
					DesetupSyncExternalProductEntity(false, true);
					break;
				case "ExternalProductEntity1":
					DesetupSyncExternalProductEntity1(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_externalProductEntity!=null)
			{
				toReturn.Add(_externalProductEntity);
			}
			if(_externalProductEntity1!=null)
			{
				toReturn.Add(_externalProductEntity1);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductExternalProductId)
		{
			return FetchUsingPK(externalProductExternalProductId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductExternalProductId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(externalProductExternalProductId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductExternalProductId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(externalProductExternalProductId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalProductExternalProductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(externalProductExternalProductId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ExternalProductExternalProductId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ExternalSubProductRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public ExternalProductEntity GetSingleExternalProductEntity()
		{
			return GetSingleExternalProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public virtual ExternalProductEntity GetSingleExternalProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalProductEntity || forceFetch || _alwaysFetchExternalProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalProductEntityUsingExternalProductId);
				ExternalProductEntity newEntity = new ExternalProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalProductId);
				}
				if(fetchResult)
				{
					newEntity = (ExternalProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalProductEntity = newEntity;
				_alreadyFetchedExternalProductEntity = fetchResult;
			}
			return _externalProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public ExternalProductEntity GetSingleExternalProductEntity1()
		{
			return GetSingleExternalProductEntity1(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public virtual ExternalProductEntity GetSingleExternalProductEntity1(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalProductEntity1 || forceFetch || _alwaysFetchExternalProductEntity1) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalProductEntityUsingExternalSubProductId);
				ExternalProductEntity newEntity = new ExternalProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalSubProductId);
				}
				if(fetchResult)
				{
					newEntity = (ExternalProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalProductEntity1ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalProductEntity1 = newEntity;
				_alreadyFetchedExternalProductEntity1 = fetchResult;
			}
			return _externalProductEntity1;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ExternalProductEntity", _externalProductEntity);
			toReturn.Add("ExternalProductEntity1", _externalProductEntity1);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="validator">The validator object for this ExternalSubProductEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 externalProductExternalProductId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(externalProductExternalProductId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_externalProductEntityReturnsNewIfNotFound = true;
			_externalProductEntity1ReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalProductExternalProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalSubProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _externalProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalProductEntity, new PropertyChangedEventHandler( OnExternalProductEntityPropertyChanged ), "ExternalProductEntity", Obymobi.Data.RelationClasses.StaticExternalSubProductRelations.ExternalProductEntityUsingExternalProductIdStatic, true, signalRelatedEntity, "ExternalSubProductCollection", resetFKFields, new int[] { (int)ExternalSubProductFieldIndex.ExternalProductId } );		
			_externalProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalProductEntity(IEntityCore relatedEntity)
		{
			if(_externalProductEntity!=relatedEntity)
			{		
				DesetupSyncExternalProductEntity(true, true);
				_externalProductEntity = (ExternalProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalProductEntity, new PropertyChangedEventHandler( OnExternalProductEntityPropertyChanged ), "ExternalProductEntity", Obymobi.Data.RelationClasses.StaticExternalSubProductRelations.ExternalProductEntityUsingExternalProductIdStatic, true, ref _alreadyFetchedExternalProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalProductEntity1</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalProductEntity1(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalProductEntity1, new PropertyChangedEventHandler( OnExternalProductEntity1PropertyChanged ), "ExternalProductEntity1", Obymobi.Data.RelationClasses.StaticExternalSubProductRelations.ExternalProductEntityUsingExternalSubProductIdStatic, true, signalRelatedEntity, "ExternalSubProductCollection1", resetFKFields, new int[] { (int)ExternalSubProductFieldIndex.ExternalSubProductId } );		
			_externalProductEntity1 = null;
		}
		
		/// <summary> setups the sync logic for member _externalProductEntity1</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalProductEntity1(IEntityCore relatedEntity)
		{
			if(_externalProductEntity1!=relatedEntity)
			{		
				DesetupSyncExternalProductEntity1(true, true);
				_externalProductEntity1 = (ExternalProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalProductEntity1, new PropertyChangedEventHandler( OnExternalProductEntity1PropertyChanged ), "ExternalProductEntity1", Obymobi.Data.RelationClasses.StaticExternalSubProductRelations.ExternalProductEntityUsingExternalSubProductIdStatic, true, ref _alreadyFetchedExternalProductEntity1, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalProductEntity1PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="externalProductExternalProductId">PK value for ExternalSubProduct which data should be fetched into this ExternalSubProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 externalProductExternalProductId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ExternalSubProductFieldIndex.ExternalProductExternalProductId].ForcedCurrentValueWrite(externalProductExternalProductId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateExternalSubProductDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ExternalSubProductEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ExternalSubProductRelations Relations
		{
			get	{ return new ExternalSubProductRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalProduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalProductCollection(), (IEntityRelation)GetRelationsForField("ExternalProductEntity")[0], (int)Obymobi.Data.EntityType.ExternalSubProductEntity, (int)Obymobi.Data.EntityType.ExternalProductEntity, 0, null, null, null, "ExternalProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalProduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalProductEntity1
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalProductCollection(), (IEntityRelation)GetRelationsForField("ExternalProductEntity1")[0], (int)Obymobi.Data.EntityType.ExternalSubProductEntity, (int)Obymobi.Data.EntityType.ExternalProductEntity, 0, null, null, null, "ExternalProductEntity1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ExternalProductExternalProductId property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."ExternalProductExternalProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalProductExternalProductId
		{
			get { return (System.Int32)GetValue((int)ExternalSubProductFieldIndex.ExternalProductExternalProductId, true); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.ExternalProductExternalProductId, value, true); }
		}

		/// <summary> The ExternalProductId property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."ExternalProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalProductId
		{
			get { return (System.Int32)GetValue((int)ExternalSubProductFieldIndex.ExternalProductId, true); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.ExternalProductId, value, true); }
		}

		/// <summary> The ExternalSubProductId property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."ExternalSubProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSubProductId
		{
			get { return (System.Int32)GetValue((int)ExternalSubProductFieldIndex.ExternalSubProductId, true); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.ExternalSubProductId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ExternalSubProductFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ExternalSubProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalSubProduct"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalSubProductFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ExternalSubProductFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ExternalProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalProductEntity ExternalProductEntity
		{
			get	{ return GetSingleExternalProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalSubProductCollection", "ExternalProductEntity", _externalProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalProductEntity. When set to true, ExternalProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalProductEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalProductEntity
		{
			get	{ return _alwaysFetchExternalProductEntity; }
			set	{ _alwaysFetchExternalProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalProductEntity already has been fetched. Setting this property to false when ExternalProductEntity has been fetched
		/// will set ExternalProductEntity to null as well. Setting this property to true while ExternalProductEntity hasn't been fetched disables lazy loading for ExternalProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalProductEntity
		{
			get { return _alreadyFetchedExternalProductEntity;}
			set 
			{
				if(_alreadyFetchedExternalProductEntity && !value)
				{
					this.ExternalProductEntity = null;
				}
				_alreadyFetchedExternalProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalProductEntity is not found
		/// in the database. When set to true, ExternalProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalProductEntityReturnsNewIfNotFound
		{
			get	{ return _externalProductEntityReturnsNewIfNotFound; }
			set { _externalProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalProductEntity1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalProductEntity ExternalProductEntity1
		{
			get	{ return GetSingleExternalProductEntity1(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalProductEntity1(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalSubProductCollection1", "ExternalProductEntity1", _externalProductEntity1, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalProductEntity1. When set to true, ExternalProductEntity1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalProductEntity1 is accessed. You can always execute a forced fetch by calling GetSingleExternalProductEntity1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalProductEntity1
		{
			get	{ return _alwaysFetchExternalProductEntity1; }
			set	{ _alwaysFetchExternalProductEntity1 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalProductEntity1 already has been fetched. Setting this property to false when ExternalProductEntity1 has been fetched
		/// will set ExternalProductEntity1 to null as well. Setting this property to true while ExternalProductEntity1 hasn't been fetched disables lazy loading for ExternalProductEntity1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalProductEntity1
		{
			get { return _alreadyFetchedExternalProductEntity1;}
			set 
			{
				if(_alreadyFetchedExternalProductEntity1 && !value)
				{
					this.ExternalProductEntity1 = null;
				}
				_alreadyFetchedExternalProductEntity1 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalProductEntity1 is not found
		/// in the database. When set to true, ExternalProductEntity1 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalProductEntity1ReturnsNewIfNotFound
		{
			get	{ return _externalProductEntity1ReturnsNewIfNotFound; }
			set { _externalProductEntity1ReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ExternalSubProductEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
