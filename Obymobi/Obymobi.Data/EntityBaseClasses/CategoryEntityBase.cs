﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Category'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CategoryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CategoryEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection	_advertisementTagCategoryCollection;
		private bool	_alwaysFetchAdvertisementTagCategoryCollection, _alreadyFetchedAdvertisementTagCategoryCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection	_announcementCollection_;
		private bool	_alwaysFetchAnnouncementCollection_, _alreadyFetchedAnnouncementCollection_;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection	_announcementCollection;
		private bool	_alwaysFetchAnnouncementCollection, _alreadyFetchedAnnouncementCollection;
		private Obymobi.Data.CollectionClasses.ActionCollection	_actionCollection;
		private bool	_alwaysFetchActionCollection, _alreadyFetchedActionCollection;
		private Obymobi.Data.CollectionClasses.AvailabilityCollection	_availabilityCollection;
		private bool	_alwaysFetchAvailabilityCollection, _alreadyFetchedAvailabilityCollection;
		private Obymobi.Data.CollectionClasses.CategoryCollection	_childCategoryCollection;
		private bool	_alwaysFetchChildCategoryCollection, _alreadyFetchedChildCategoryCollection;
		private Obymobi.Data.CollectionClasses.CategoryAlterationCollection	_categoryAlterationCollection;
		private bool	_alwaysFetchCategoryAlterationCollection, _alreadyFetchedCategoryAlterationCollection;
		private Obymobi.Data.CollectionClasses.CategoryLanguageCollection	_categoryLanguageCollection;
		private bool	_alwaysFetchCategoryLanguageCollection, _alreadyFetchedCategoryLanguageCollection;
		private Obymobi.Data.CollectionClasses.CategorySuggestionCollection	_categorySuggestionCollection;
		private bool	_alwaysFetchCategorySuggestionCollection, _alreadyFetchedCategorySuggestionCollection;
		private Obymobi.Data.CollectionClasses.CategoryTagCollection	_categoryTagCollection;
		private bool	_alwaysFetchCategoryTagCollection, _alreadyFetchedCategoryTagCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_actionMediaCollection;
		private bool	_alwaysFetchActionMediaCollection, _alreadyFetchedActionMediaCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessageRecipientCollection	_messageRecipientCollection;
		private bool	_alwaysFetchMessageRecipientCollection, _alreadyFetchedMessageRecipientCollection;
		private Obymobi.Data.CollectionClasses.MessageTemplateCollection	_messageTemplateCollection;
		private bool	_alwaysFetchMessageTemplateCollection, _alreadyFetchedMessageTemplateCollection;
		private Obymobi.Data.CollectionClasses.OrderHourCollection	_orderHourCollection;
		private bool	_alwaysFetchOrderHourCollection, _alreadyFetchedOrderHourCollection;
		private Obymobi.Data.CollectionClasses.OrderitemCollection	_orderitemCollection;
		private bool	_alwaysFetchOrderitemCollection, _alreadyFetchedOrderitemCollection;
		private Obymobi.Data.CollectionClasses.ProductCategoryCollection	_productCategoryCollection;
		private bool	_alwaysFetchProductCategoryCollection, _alreadyFetchedProductCategoryCollection;
		private Obymobi.Data.CollectionClasses.ProductCategoryTagCollection	_productCategoryTagCollection;
		private bool	_alwaysFetchProductCategoryTagCollection, _alreadyFetchedProductCategoryTagCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageCollection	_scheduledMessageCollection;
		private bool	_alwaysFetchScheduledMessageCollection, _alreadyFetchedScheduledMessageCollection;
		private Obymobi.Data.CollectionClasses.UITabCollection	_uITabCollection;
		private bool	_alwaysFetchUITabCollection, _alreadyFetchedUITabCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementCollection _advertisementCollectionViaMedium;
		private bool	_alwaysFetchAdvertisementCollectionViaMedium, _alreadyFetchedAdvertisementCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationCollection _alterationCollectionViaMedium;
		private bool	_alwaysFetchAlterationCollectionViaMedium, _alreadyFetchedAlterationCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationCollection _alterationCollectionViaCategoryAlteration;
		private bool	_alwaysFetchAlterationCollectionViaCategoryAlteration, _alreadyFetchedAlterationCollectionViaCategoryAlteration;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium_;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium_, _alreadyFetchedAlterationoptionCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium_;
		private bool	_alwaysFetchCategoryCollectionViaMedium_, _alreadyFetchedCategoryCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.ClientCollection _clientCollectionViaMessage;
		private bool	_alwaysFetchClientCollectionViaMessage, _alreadyFetchedClientCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMedium;
		private bool	_alwaysFetchCompanyCollectionViaMedium, _alreadyFetchedCompanyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMessage;
		private bool	_alwaysFetchCompanyCollectionViaMessage, _alreadyFetchedCompanyCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMessageTemplate;
		private bool	_alwaysFetchCompanyCollectionViaMessageTemplate, _alreadyFetchedCompanyCollectionViaMessageTemplate;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaAdvertisement;
		private bool	_alwaysFetchCompanyCollectionViaAdvertisement, _alreadyFetchedCompanyCollectionViaAdvertisement;

		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaMessage;
		private bool	_alwaysFetchCustomerCollectionViaMessage, _alreadyFetchedCustomerCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaMessage;
		private bool	_alwaysFetchDeliverypointCollectionViaMessage, _alreadyFetchedDeliverypointCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAdvertisement;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAdvertisement, _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAnnouncement;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAnnouncement, _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAnnouncement_;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_, _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaMedium;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaMedium, _alreadyFetchedDeliverypointgroupCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAnnouncement;
		private bool	_alwaysFetchEntertainmentCollectionViaAnnouncement, _alreadyFetchedEntertainmentCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAnnouncement_;
		private bool	_alwaysFetchEntertainmentCollectionViaAnnouncement_, _alreadyFetchedEntertainmentCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium_;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium_, _alreadyFetchedEntertainmentCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium__;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium__, _alreadyFetchedEntertainmentCollectionViaMedium__;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessage;
		private bool	_alwaysFetchEntertainmentCollectionViaMessage, _alreadyFetchedEntertainmentCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessageTemplate;
		private bool	_alwaysFetchEntertainmentCollectionViaMessageTemplate, _alreadyFetchedEntertainmentCollectionViaMessageTemplate;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaUITab;
		private bool	_alwaysFetchEntertainmentCollectionViaUITab, _alreadyFetchedEntertainmentCollectionViaUITab;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement, _alreadyFetchedEntertainmentCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement_;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement_, _alreadyFetchedEntertainmentCollectionViaAdvertisement_;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement, _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium_;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium_, _alreadyFetchedEntertainmentcategoryCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection _genericcategoryCollectionViaMedium;
		private bool	_alwaysFetchGenericcategoryCollectionViaMedium, _alreadyFetchedGenericcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaMedium;
		private bool	_alwaysFetchGenericproductCollectionViaMedium, _alreadyFetchedGenericproductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaAdvertisement;
		private bool	_alwaysFetchGenericproductCollectionViaAdvertisement, _alreadyFetchedGenericproductCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaAnnouncement;
		private bool	_alwaysFetchMediaCollectionViaAnnouncement, _alreadyFetchedMediaCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaAnnouncement_;
		private bool	_alwaysFetchMediaCollectionViaAnnouncement_, _alreadyFetchedMediaCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaMessage;
		private bool	_alwaysFetchMediaCollectionViaMessage, _alreadyFetchedMediaCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaMessageTemplate;
		private bool	_alwaysFetchMediaCollectionViaMessageTemplate, _alreadyFetchedMediaCollectionViaMessageTemplate;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaCategory;
		private bool	_alwaysFetchMenuCollectionViaCategory, _alreadyFetchedMenuCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaMessage;
		private bool	_alwaysFetchOrderCollectionViaMessage, _alreadyFetchedOrderCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrderitem;
		private bool	_alwaysFetchOrderCollectionViaOrderitem, _alreadyFetchedOrderCollectionViaOrderitem;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium_;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium_, _alreadyFetchedPointOfInterestCollectionViaMedium_;

		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAnnouncement;
		private bool	_alwaysFetchProductCollectionViaAnnouncement, _alreadyFetchedProductCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAnnouncement_;
		private bool	_alwaysFetchProductCollectionViaAnnouncement_, _alreadyFetchedProductCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaCategorySuggestion;
		private bool	_alwaysFetchProductCollectionViaCategorySuggestion, _alreadyFetchedProductCollectionViaCategorySuggestion;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium_;
		private bool	_alwaysFetchProductCollectionViaMedium_, _alreadyFetchedProductCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium__;
		private bool	_alwaysFetchProductCollectionViaMedium__, _alreadyFetchedProductCollectionViaMedium__;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMessageTemplate;
		private bool	_alwaysFetchProductCollectionViaMessageTemplate, _alreadyFetchedProductCollectionViaMessageTemplate;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaOrderitem;
		private bool	_alwaysFetchProductCollectionViaOrderitem, _alreadyFetchedProductCollectionViaOrderitem;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaProductCategory;
		private bool	_alwaysFetchProductCollectionViaProductCategory, _alreadyFetchedProductCollectionViaProductCategory;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAdvertisement;
		private bool	_alwaysFetchProductCollectionViaAdvertisement, _alreadyFetchedProductCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCategory;
		private bool	_alwaysFetchRouteCollectionViaCategory, _alreadyFetchedRouteCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.SupplierCollection _supplierCollectionViaAdvertisement;
		private bool	_alwaysFetchSupplierCollectionViaAdvertisement, _alreadyFetchedSupplierCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.SurveyCollection _surveyCollectionViaMedium;
		private bool	_alwaysFetchSurveyCollectionViaMedium, _alreadyFetchedSurveyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium_;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium_, _alreadyFetchedSurveyPageCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaUITab;
		private bool	_alwaysFetchUIModeCollectionViaUITab, _alreadyFetchedUIModeCollectionViaUITab;
		private CategoryEntity _parentCategoryEntity;
		private bool	_alwaysFetchParentCategoryEntity, _alreadyFetchedParentCategoryEntity, _parentCategoryEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private GenericcategoryEntity _genericcategoryEntity;
		private bool	_alwaysFetchGenericcategoryEntity, _alreadyFetchedGenericcategoryEntity, _genericcategoryEntityReturnsNewIfNotFound;
		private MenuEntity _menuEntity;
		private bool	_alwaysFetchMenuEntity, _alreadyFetchedMenuEntity, _menuEntityReturnsNewIfNotFound;
		private PoscategoryEntity _poscategoryEntity;
		private bool	_alwaysFetchPoscategoryEntity, _alreadyFetchedPoscategoryEntity, _poscategoryEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private RouteEntity _routeEntity;
		private bool	_alwaysFetchRouteEntity, _alreadyFetchedRouteEntity, _routeEntityReturnsNewIfNotFound;
		private ScheduleEntity _scheduleEntity;
		private bool	_alwaysFetchScheduleEntity, _alreadyFetchedScheduleEntity, _scheduleEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentCategoryEntity</summary>
			public static readonly string ParentCategoryEntity = "ParentCategoryEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name GenericcategoryEntity</summary>
			public static readonly string GenericcategoryEntity = "GenericcategoryEntity";
			/// <summary>Member name MenuEntity</summary>
			public static readonly string MenuEntity = "MenuEntity";
			/// <summary>Member name PoscategoryEntity</summary>
			public static readonly string PoscategoryEntity = "PoscategoryEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name RouteEntity</summary>
			public static readonly string RouteEntity = "RouteEntity";
			/// <summary>Member name ScheduleEntity</summary>
			public static readonly string ScheduleEntity = "ScheduleEntity";
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name AdvertisementTagCategoryCollection</summary>
			public static readonly string AdvertisementTagCategoryCollection = "AdvertisementTagCategoryCollection";
			/// <summary>Member name AnnouncementCollection_</summary>
			public static readonly string AnnouncementCollection_ = "AnnouncementCollection_";
			/// <summary>Member name AnnouncementCollection</summary>
			public static readonly string AnnouncementCollection = "AnnouncementCollection";
			/// <summary>Member name ActionCollection</summary>
			public static readonly string ActionCollection = "ActionCollection";
			/// <summary>Member name AvailabilityCollection</summary>
			public static readonly string AvailabilityCollection = "AvailabilityCollection";
			/// <summary>Member name ChildCategoryCollection</summary>
			public static readonly string ChildCategoryCollection = "ChildCategoryCollection";
			/// <summary>Member name CategoryAlterationCollection</summary>
			public static readonly string CategoryAlterationCollection = "CategoryAlterationCollection";
			/// <summary>Member name CategoryLanguageCollection</summary>
			public static readonly string CategoryLanguageCollection = "CategoryLanguageCollection";
			/// <summary>Member name CategorySuggestionCollection</summary>
			public static readonly string CategorySuggestionCollection = "CategorySuggestionCollection";
			/// <summary>Member name CategoryTagCollection</summary>
			public static readonly string CategoryTagCollection = "CategoryTagCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name ActionMediaCollection</summary>
			public static readonly string ActionMediaCollection = "ActionMediaCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessageRecipientCollection</summary>
			public static readonly string MessageRecipientCollection = "MessageRecipientCollection";
			/// <summary>Member name MessageTemplateCollection</summary>
			public static readonly string MessageTemplateCollection = "MessageTemplateCollection";
			/// <summary>Member name OrderHourCollection</summary>
			public static readonly string OrderHourCollection = "OrderHourCollection";
			/// <summary>Member name OrderitemCollection</summary>
			public static readonly string OrderitemCollection = "OrderitemCollection";
			/// <summary>Member name ProductCategoryCollection</summary>
			public static readonly string ProductCategoryCollection = "ProductCategoryCollection";
			/// <summary>Member name ProductCategoryTagCollection</summary>
			public static readonly string ProductCategoryTagCollection = "ProductCategoryTagCollection";
			/// <summary>Member name ScheduledMessageCollection</summary>
			public static readonly string ScheduledMessageCollection = "ScheduledMessageCollection";
			/// <summary>Member name UITabCollection</summary>
			public static readonly string UITabCollection = "UITabCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
			/// <summary>Member name AdvertisementCollectionViaMedium</summary>
			public static readonly string AdvertisementCollectionViaMedium = "AdvertisementCollectionViaMedium";
			/// <summary>Member name AlterationCollectionViaMedium</summary>
			public static readonly string AlterationCollectionViaMedium = "AlterationCollectionViaMedium";
			/// <summary>Member name AlterationCollectionViaCategoryAlteration</summary>
			public static readonly string AlterationCollectionViaCategoryAlteration = "AlterationCollectionViaCategoryAlteration";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name AlterationoptionCollectionViaMedium_</summary>
			public static readonly string AlterationoptionCollectionViaMedium_ = "AlterationoptionCollectionViaMedium_";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium_</summary>
			public static readonly string CategoryCollectionViaMedium_ = "CategoryCollectionViaMedium_";
			/// <summary>Member name ClientCollectionViaMessage</summary>
			public static readonly string ClientCollectionViaMessage = "ClientCollectionViaMessage";
			/// <summary>Member name CompanyCollectionViaMedium</summary>
			public static readonly string CompanyCollectionViaMedium = "CompanyCollectionViaMedium";
			/// <summary>Member name CompanyCollectionViaMessage</summary>
			public static readonly string CompanyCollectionViaMessage = "CompanyCollectionViaMessage";
			/// <summary>Member name CompanyCollectionViaMessageTemplate</summary>
			public static readonly string CompanyCollectionViaMessageTemplate = "CompanyCollectionViaMessageTemplate";
			/// <summary>Member name CompanyCollectionViaAdvertisement</summary>
			public static readonly string CompanyCollectionViaAdvertisement = "CompanyCollectionViaAdvertisement";

			/// <summary>Member name CustomerCollectionViaMessage</summary>
			public static readonly string CustomerCollectionViaMessage = "CustomerCollectionViaMessage";
			/// <summary>Member name DeliverypointCollectionViaMessage</summary>
			public static readonly string DeliverypointCollectionViaMessage = "DeliverypointCollectionViaMessage";
			/// <summary>Member name DeliverypointgroupCollectionViaAdvertisement</summary>
			public static readonly string DeliverypointgroupCollectionViaAdvertisement = "DeliverypointgroupCollectionViaAdvertisement";
			/// <summary>Member name DeliverypointgroupCollectionViaAnnouncement</summary>
			public static readonly string DeliverypointgroupCollectionViaAnnouncement = "DeliverypointgroupCollectionViaAnnouncement";
			/// <summary>Member name DeliverypointgroupCollectionViaAnnouncement_</summary>
			public static readonly string DeliverypointgroupCollectionViaAnnouncement_ = "DeliverypointgroupCollectionViaAnnouncement_";
			/// <summary>Member name DeliverypointgroupCollectionViaMedium</summary>
			public static readonly string DeliverypointgroupCollectionViaMedium = "DeliverypointgroupCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaAnnouncement</summary>
			public static readonly string EntertainmentCollectionViaAnnouncement = "EntertainmentCollectionViaAnnouncement";
			/// <summary>Member name EntertainmentCollectionViaAnnouncement_</summary>
			public static readonly string EntertainmentCollectionViaAnnouncement_ = "EntertainmentCollectionViaAnnouncement_";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium_</summary>
			public static readonly string EntertainmentCollectionViaMedium_ = "EntertainmentCollectionViaMedium_";
			/// <summary>Member name EntertainmentCollectionViaMedium__</summary>
			public static readonly string EntertainmentCollectionViaMedium__ = "EntertainmentCollectionViaMedium__";
			/// <summary>Member name EntertainmentCollectionViaMessage</summary>
			public static readonly string EntertainmentCollectionViaMessage = "EntertainmentCollectionViaMessage";
			/// <summary>Member name EntertainmentCollectionViaMessageTemplate</summary>
			public static readonly string EntertainmentCollectionViaMessageTemplate = "EntertainmentCollectionViaMessageTemplate";
			/// <summary>Member name EntertainmentCollectionViaUITab</summary>
			public static readonly string EntertainmentCollectionViaUITab = "EntertainmentCollectionViaUITab";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement = "EntertainmentCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement_</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement_ = "EntertainmentCollectionViaAdvertisement_";
			/// <summary>Member name EntertainmentcategoryCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentcategoryCollectionViaAdvertisement = "EntertainmentcategoryCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium_</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium_ = "EntertainmentcategoryCollectionViaMedium_";
			/// <summary>Member name GenericcategoryCollectionViaMedium</summary>
			public static readonly string GenericcategoryCollectionViaMedium = "GenericcategoryCollectionViaMedium";
			/// <summary>Member name GenericproductCollectionViaMedium</summary>
			public static readonly string GenericproductCollectionViaMedium = "GenericproductCollectionViaMedium";
			/// <summary>Member name GenericproductCollectionViaAdvertisement</summary>
			public static readonly string GenericproductCollectionViaAdvertisement = "GenericproductCollectionViaAdvertisement";
			/// <summary>Member name MediaCollectionViaAnnouncement</summary>
			public static readonly string MediaCollectionViaAnnouncement = "MediaCollectionViaAnnouncement";
			/// <summary>Member name MediaCollectionViaAnnouncement_</summary>
			public static readonly string MediaCollectionViaAnnouncement_ = "MediaCollectionViaAnnouncement_";
			/// <summary>Member name MediaCollectionViaMessage</summary>
			public static readonly string MediaCollectionViaMessage = "MediaCollectionViaMessage";
			/// <summary>Member name MediaCollectionViaMessageTemplate</summary>
			public static readonly string MediaCollectionViaMessageTemplate = "MediaCollectionViaMessageTemplate";
			/// <summary>Member name MenuCollectionViaCategory</summary>
			public static readonly string MenuCollectionViaCategory = "MenuCollectionViaCategory";
			/// <summary>Member name OrderCollectionViaMessage</summary>
			public static readonly string OrderCollectionViaMessage = "OrderCollectionViaMessage";
			/// <summary>Member name OrderCollectionViaOrderitem</summary>
			public static readonly string OrderCollectionViaOrderitem = "OrderCollectionViaOrderitem";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name PointOfInterestCollectionViaMedium_</summary>
			public static readonly string PointOfInterestCollectionViaMedium_ = "PointOfInterestCollectionViaMedium_";

			/// <summary>Member name ProductCollectionViaAnnouncement</summary>
			public static readonly string ProductCollectionViaAnnouncement = "ProductCollectionViaAnnouncement";
			/// <summary>Member name ProductCollectionViaAnnouncement_</summary>
			public static readonly string ProductCollectionViaAnnouncement_ = "ProductCollectionViaAnnouncement_";
			/// <summary>Member name ProductCollectionViaCategorySuggestion</summary>
			public static readonly string ProductCollectionViaCategorySuggestion = "ProductCollectionViaCategorySuggestion";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium_</summary>
			public static readonly string ProductCollectionViaMedium_ = "ProductCollectionViaMedium_";
			/// <summary>Member name ProductCollectionViaMedium__</summary>
			public static readonly string ProductCollectionViaMedium__ = "ProductCollectionViaMedium__";
			/// <summary>Member name ProductCollectionViaMessageTemplate</summary>
			public static readonly string ProductCollectionViaMessageTemplate = "ProductCollectionViaMessageTemplate";
			/// <summary>Member name ProductCollectionViaOrderitem</summary>
			public static readonly string ProductCollectionViaOrderitem = "ProductCollectionViaOrderitem";
			/// <summary>Member name ProductCollectionViaProductCategory</summary>
			public static readonly string ProductCollectionViaProductCategory = "ProductCollectionViaProductCategory";
			/// <summary>Member name ProductCollectionViaAdvertisement</summary>
			public static readonly string ProductCollectionViaAdvertisement = "ProductCollectionViaAdvertisement";
			/// <summary>Member name RouteCollectionViaCategory</summary>
			public static readonly string RouteCollectionViaCategory = "RouteCollectionViaCategory";
			/// <summary>Member name SupplierCollectionViaAdvertisement</summary>
			public static readonly string SupplierCollectionViaAdvertisement = "SupplierCollectionViaAdvertisement";
			/// <summary>Member name SurveyCollectionViaMedium</summary>
			public static readonly string SurveyCollectionViaMedium = "SurveyCollectionViaMedium";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
			/// <summary>Member name SurveyPageCollectionViaMedium_</summary>
			public static readonly string SurveyPageCollectionViaMedium_ = "SurveyPageCollectionViaMedium_";
			/// <summary>Member name UIModeCollectionViaUITab</summary>
			public static readonly string UIModeCollectionViaUITab = "UIModeCollectionViaUITab";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CategoryEntityBase() :base("CategoryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		protected CategoryEntityBase(System.Int32 categoryId):base("CategoryEntity")
		{
			InitClassFetch(categoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CategoryEntityBase(System.Int32 categoryId, IPrefetchPath prefetchPathToUse): base("CategoryEntity")
		{
			InitClassFetch(categoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="validator">The custom validator object for this CategoryEntity</param>
		protected CategoryEntityBase(System.Int32 categoryId, IValidator validator):base("CategoryEntity")
		{
			InitClassFetch(categoryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_advertisementTagCategoryCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection)info.GetValue("_advertisementTagCategoryCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection));
			_alwaysFetchAdvertisementTagCategoryCollection = info.GetBoolean("_alwaysFetchAdvertisementTagCategoryCollection");
			_alreadyFetchedAdvertisementTagCategoryCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagCategoryCollection");

			_announcementCollection_ = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollection_", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollection_ = info.GetBoolean("_alwaysFetchAnnouncementCollection_");
			_alreadyFetchedAnnouncementCollection_ = info.GetBoolean("_alreadyFetchedAnnouncementCollection_");

			_announcementCollection = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollection", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollection = info.GetBoolean("_alwaysFetchAnnouncementCollection");
			_alreadyFetchedAnnouncementCollection = info.GetBoolean("_alreadyFetchedAnnouncementCollection");

			_actionCollection = (Obymobi.Data.CollectionClasses.ActionCollection)info.GetValue("_actionCollection", typeof(Obymobi.Data.CollectionClasses.ActionCollection));
			_alwaysFetchActionCollection = info.GetBoolean("_alwaysFetchActionCollection");
			_alreadyFetchedActionCollection = info.GetBoolean("_alreadyFetchedActionCollection");

			_availabilityCollection = (Obymobi.Data.CollectionClasses.AvailabilityCollection)info.GetValue("_availabilityCollection", typeof(Obymobi.Data.CollectionClasses.AvailabilityCollection));
			_alwaysFetchAvailabilityCollection = info.GetBoolean("_alwaysFetchAvailabilityCollection");
			_alreadyFetchedAvailabilityCollection = info.GetBoolean("_alreadyFetchedAvailabilityCollection");

			_childCategoryCollection = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_childCategoryCollection", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchChildCategoryCollection = info.GetBoolean("_alwaysFetchChildCategoryCollection");
			_alreadyFetchedChildCategoryCollection = info.GetBoolean("_alreadyFetchedChildCategoryCollection");

			_categoryAlterationCollection = (Obymobi.Data.CollectionClasses.CategoryAlterationCollection)info.GetValue("_categoryAlterationCollection", typeof(Obymobi.Data.CollectionClasses.CategoryAlterationCollection));
			_alwaysFetchCategoryAlterationCollection = info.GetBoolean("_alwaysFetchCategoryAlterationCollection");
			_alreadyFetchedCategoryAlterationCollection = info.GetBoolean("_alreadyFetchedCategoryAlterationCollection");

			_categoryLanguageCollection = (Obymobi.Data.CollectionClasses.CategoryLanguageCollection)info.GetValue("_categoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.CategoryLanguageCollection));
			_alwaysFetchCategoryLanguageCollection = info.GetBoolean("_alwaysFetchCategoryLanguageCollection");
			_alreadyFetchedCategoryLanguageCollection = info.GetBoolean("_alreadyFetchedCategoryLanguageCollection");

			_categorySuggestionCollection = (Obymobi.Data.CollectionClasses.CategorySuggestionCollection)info.GetValue("_categorySuggestionCollection", typeof(Obymobi.Data.CollectionClasses.CategorySuggestionCollection));
			_alwaysFetchCategorySuggestionCollection = info.GetBoolean("_alwaysFetchCategorySuggestionCollection");
			_alreadyFetchedCategorySuggestionCollection = info.GetBoolean("_alreadyFetchedCategorySuggestionCollection");

			_categoryTagCollection = (Obymobi.Data.CollectionClasses.CategoryTagCollection)info.GetValue("_categoryTagCollection", typeof(Obymobi.Data.CollectionClasses.CategoryTagCollection));
			_alwaysFetchCategoryTagCollection = info.GetBoolean("_alwaysFetchCategoryTagCollection");
			_alreadyFetchedCategoryTagCollection = info.GetBoolean("_alreadyFetchedCategoryTagCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_actionMediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_actionMediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchActionMediaCollection = info.GetBoolean("_alwaysFetchActionMediaCollection");
			_alreadyFetchedActionMediaCollection = info.GetBoolean("_alreadyFetchedActionMediaCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messageRecipientCollection = (Obymobi.Data.CollectionClasses.MessageRecipientCollection)info.GetValue("_messageRecipientCollection", typeof(Obymobi.Data.CollectionClasses.MessageRecipientCollection));
			_alwaysFetchMessageRecipientCollection = info.GetBoolean("_alwaysFetchMessageRecipientCollection");
			_alreadyFetchedMessageRecipientCollection = info.GetBoolean("_alreadyFetchedMessageRecipientCollection");

			_messageTemplateCollection = (Obymobi.Data.CollectionClasses.MessageTemplateCollection)info.GetValue("_messageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.MessageTemplateCollection));
			_alwaysFetchMessageTemplateCollection = info.GetBoolean("_alwaysFetchMessageTemplateCollection");
			_alreadyFetchedMessageTemplateCollection = info.GetBoolean("_alreadyFetchedMessageTemplateCollection");

			_orderHourCollection = (Obymobi.Data.CollectionClasses.OrderHourCollection)info.GetValue("_orderHourCollection", typeof(Obymobi.Data.CollectionClasses.OrderHourCollection));
			_alwaysFetchOrderHourCollection = info.GetBoolean("_alwaysFetchOrderHourCollection");
			_alreadyFetchedOrderHourCollection = info.GetBoolean("_alreadyFetchedOrderHourCollection");

			_orderitemCollection = (Obymobi.Data.CollectionClasses.OrderitemCollection)info.GetValue("_orderitemCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemCollection));
			_alwaysFetchOrderitemCollection = info.GetBoolean("_alwaysFetchOrderitemCollection");
			_alreadyFetchedOrderitemCollection = info.GetBoolean("_alreadyFetchedOrderitemCollection");

			_productCategoryCollection = (Obymobi.Data.CollectionClasses.ProductCategoryCollection)info.GetValue("_productCategoryCollection", typeof(Obymobi.Data.CollectionClasses.ProductCategoryCollection));
			_alwaysFetchProductCategoryCollection = info.GetBoolean("_alwaysFetchProductCategoryCollection");
			_alreadyFetchedProductCategoryCollection = info.GetBoolean("_alreadyFetchedProductCategoryCollection");

			_productCategoryTagCollection = (Obymobi.Data.CollectionClasses.ProductCategoryTagCollection)info.GetValue("_productCategoryTagCollection", typeof(Obymobi.Data.CollectionClasses.ProductCategoryTagCollection));
			_alwaysFetchProductCategoryTagCollection = info.GetBoolean("_alwaysFetchProductCategoryTagCollection");
			_alreadyFetchedProductCategoryTagCollection = info.GetBoolean("_alreadyFetchedProductCategoryTagCollection");

			_scheduledMessageCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageCollection)info.GetValue("_scheduledMessageCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageCollection));
			_alwaysFetchScheduledMessageCollection = info.GetBoolean("_alwaysFetchScheduledMessageCollection");
			_alreadyFetchedScheduledMessageCollection = info.GetBoolean("_alreadyFetchedScheduledMessageCollection");

			_uITabCollection = (Obymobi.Data.CollectionClasses.UITabCollection)info.GetValue("_uITabCollection", typeof(Obymobi.Data.CollectionClasses.UITabCollection));
			_alwaysFetchUITabCollection = info.GetBoolean("_alwaysFetchUITabCollection");
			_alreadyFetchedUITabCollection = info.GetBoolean("_alreadyFetchedUITabCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_advertisementCollectionViaMedium = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollectionViaMedium = info.GetBoolean("_alwaysFetchAdvertisementCollectionViaMedium");
			_alreadyFetchedAdvertisementCollectionViaMedium = info.GetBoolean("_alreadyFetchedAdvertisementCollectionViaMedium");

			_alterationCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationCollectionViaMedium");
			_alreadyFetchedAlterationCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationCollectionViaMedium");

			_alterationCollectionViaCategoryAlteration = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollectionViaCategoryAlteration", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollectionViaCategoryAlteration = info.GetBoolean("_alwaysFetchAlterationCollectionViaCategoryAlteration");
			_alreadyFetchedAlterationCollectionViaCategoryAlteration = info.GetBoolean("_alreadyFetchedAlterationCollectionViaCategoryAlteration");

			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_alterationoptionCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium_ = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium_");
			_alreadyFetchedAlterationoptionCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium_");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_categoryCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium_ = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium_");
			_alreadyFetchedCategoryCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium_");

			_clientCollectionViaMessage = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaMessage = info.GetBoolean("_alwaysFetchClientCollectionViaMessage");
			_alreadyFetchedClientCollectionViaMessage = info.GetBoolean("_alreadyFetchedClientCollectionViaMessage");

			_companyCollectionViaMedium = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMedium = info.GetBoolean("_alwaysFetchCompanyCollectionViaMedium");
			_alreadyFetchedCompanyCollectionViaMedium = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMedium");

			_companyCollectionViaMessage = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMessage = info.GetBoolean("_alwaysFetchCompanyCollectionViaMessage");
			_alreadyFetchedCompanyCollectionViaMessage = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMessage");

			_companyCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchCompanyCollectionViaMessageTemplate");
			_alreadyFetchedCompanyCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMessageTemplate");

			_companyCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchCompanyCollectionViaAdvertisement");
			_alreadyFetchedCompanyCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedCompanyCollectionViaAdvertisement");


			_customerCollectionViaMessage = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaMessage = info.GetBoolean("_alwaysFetchCustomerCollectionViaMessage");
			_alreadyFetchedCustomerCollectionViaMessage = info.GetBoolean("_alreadyFetchedCustomerCollectionViaMessage");

			_deliverypointCollectionViaMessage = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaMessage = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaMessage");
			_alreadyFetchedDeliverypointCollectionViaMessage = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaMessage");

			_deliverypointgroupCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement");
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement");

			_deliverypointgroupCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement");
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement");

			_deliverypointgroupCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_");
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_");

			_deliverypointgroupCollectionViaMedium = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaMedium");
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaMedium");

			_entertainmentCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAnnouncement");
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAnnouncement");

			_entertainmentCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAnnouncement_");
			_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAnnouncement_");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium_");
			_alreadyFetchedEntertainmentCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium_");

			_entertainmentCollectionViaMedium__ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium__", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium__ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium__");
			_alreadyFetchedEntertainmentCollectionViaMedium__ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium__");

			_entertainmentCollectionViaMessage = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessage = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessage");
			_alreadyFetchedEntertainmentCollectionViaMessage = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessage");

			_entertainmentCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessageTemplate");
			_alreadyFetchedEntertainmentCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessageTemplate");

			_entertainmentCollectionViaUITab = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaUITab", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaUITab = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaUITab");
			_alreadyFetchedEntertainmentCollectionViaUITab = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaUITab");

			_entertainmentCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement");

			_entertainmentCollectionViaAdvertisement_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement_");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement_");

			_entertainmentcategoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_entertainmentcategoryCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium_ = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium_");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium_");

			_genericcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericcategoryCollectionViaMedium");
			_alreadyFetchedGenericcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericcategoryCollectionViaMedium");

			_genericproductCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericproductCollectionViaMedium");
			_alreadyFetchedGenericproductCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaMedium");

			_genericproductCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchGenericproductCollectionViaAdvertisement");
			_alreadyFetchedGenericproductCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaAdvertisement");

			_mediaCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchMediaCollectionViaAnnouncement");
			_alreadyFetchedMediaCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedMediaCollectionViaAnnouncement");

			_mediaCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchMediaCollectionViaAnnouncement_");
			_alreadyFetchedMediaCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedMediaCollectionViaAnnouncement_");

			_mediaCollectionViaMessage = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaMessage = info.GetBoolean("_alwaysFetchMediaCollectionViaMessage");
			_alreadyFetchedMediaCollectionViaMessage = info.GetBoolean("_alreadyFetchedMediaCollectionViaMessage");

			_mediaCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchMediaCollectionViaMessageTemplate");
			_alreadyFetchedMediaCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedMediaCollectionViaMessageTemplate");

			_menuCollectionViaCategory = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaCategory = info.GetBoolean("_alwaysFetchMenuCollectionViaCategory");
			_alreadyFetchedMenuCollectionViaCategory = info.GetBoolean("_alreadyFetchedMenuCollectionViaCategory");

			_orderCollectionViaMessage = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaMessage = info.GetBoolean("_alwaysFetchOrderCollectionViaMessage");
			_alreadyFetchedOrderCollectionViaMessage = info.GetBoolean("_alreadyFetchedOrderCollectionViaMessage");

			_orderCollectionViaOrderitem = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrderitem", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrderitem = info.GetBoolean("_alwaysFetchOrderCollectionViaOrderitem");
			_alreadyFetchedOrderCollectionViaOrderitem = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrderitem");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_pointOfInterestCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium_ = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium_");
			_alreadyFetchedPointOfInterestCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium_");


			_productCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchProductCollectionViaAnnouncement");
			_alreadyFetchedProductCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedProductCollectionViaAnnouncement");

			_productCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchProductCollectionViaAnnouncement_");
			_alreadyFetchedProductCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedProductCollectionViaAnnouncement_");

			_productCollectionViaCategorySuggestion = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaCategorySuggestion", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaCategorySuggestion = info.GetBoolean("_alwaysFetchProductCollectionViaCategorySuggestion");
			_alreadyFetchedProductCollectionViaCategorySuggestion = info.GetBoolean("_alreadyFetchedProductCollectionViaCategorySuggestion");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_productCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium_ = info.GetBoolean("_alwaysFetchProductCollectionViaMedium_");
			_alreadyFetchedProductCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium_");

			_productCollectionViaMedium__ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium__", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium__ = info.GetBoolean("_alwaysFetchProductCollectionViaMedium__");
			_alreadyFetchedProductCollectionViaMedium__ = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium__");

			_productCollectionViaMessageTemplate = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMessageTemplate", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMessageTemplate = info.GetBoolean("_alwaysFetchProductCollectionViaMessageTemplate");
			_alreadyFetchedProductCollectionViaMessageTemplate = info.GetBoolean("_alreadyFetchedProductCollectionViaMessageTemplate");

			_productCollectionViaOrderitem = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaOrderitem", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaOrderitem = info.GetBoolean("_alwaysFetchProductCollectionViaOrderitem");
			_alreadyFetchedProductCollectionViaOrderitem = info.GetBoolean("_alreadyFetchedProductCollectionViaOrderitem");

			_productCollectionViaProductCategory = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaProductCategory", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaProductCategory = info.GetBoolean("_alwaysFetchProductCollectionViaProductCategory");
			_alreadyFetchedProductCollectionViaProductCategory = info.GetBoolean("_alreadyFetchedProductCollectionViaProductCategory");

			_productCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchProductCollectionViaAdvertisement");
			_alreadyFetchedProductCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedProductCollectionViaAdvertisement");

			_routeCollectionViaCategory = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCategory = info.GetBoolean("_alwaysFetchRouteCollectionViaCategory");
			_alreadyFetchedRouteCollectionViaCategory = info.GetBoolean("_alreadyFetchedRouteCollectionViaCategory");

			_supplierCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.SupplierCollection)info.GetValue("_supplierCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.SupplierCollection));
			_alwaysFetchSupplierCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchSupplierCollectionViaAdvertisement");
			_alreadyFetchedSupplierCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedSupplierCollectionViaAdvertisement");

			_surveyCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyCollection)info.GetValue("_surveyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyCollection));
			_alwaysFetchSurveyCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyCollectionViaMedium");
			_alreadyFetchedSurveyCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyCollectionViaMedium");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");

			_surveyPageCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium_ = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium_");
			_alreadyFetchedSurveyPageCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium_");

			_uIModeCollectionViaUITab = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaUITab", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaUITab = info.GetBoolean("_alwaysFetchUIModeCollectionViaUITab");
			_alreadyFetchedUIModeCollectionViaUITab = info.GetBoolean("_alreadyFetchedUIModeCollectionViaUITab");
			_parentCategoryEntity = (CategoryEntity)info.GetValue("_parentCategoryEntity", typeof(CategoryEntity));
			if(_parentCategoryEntity!=null)
			{
				_parentCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_parentCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchParentCategoryEntity = info.GetBoolean("_alwaysFetchParentCategoryEntity");
			_alreadyFetchedParentCategoryEntity = info.GetBoolean("_alreadyFetchedParentCategoryEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_genericcategoryEntity = (GenericcategoryEntity)info.GetValue("_genericcategoryEntity", typeof(GenericcategoryEntity));
			if(_genericcategoryEntity!=null)
			{
				_genericcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_genericcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchGenericcategoryEntity = info.GetBoolean("_alwaysFetchGenericcategoryEntity");
			_alreadyFetchedGenericcategoryEntity = info.GetBoolean("_alreadyFetchedGenericcategoryEntity");

			_menuEntity = (MenuEntity)info.GetValue("_menuEntity", typeof(MenuEntity));
			if(_menuEntity!=null)
			{
				_menuEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_menuEntityReturnsNewIfNotFound = info.GetBoolean("_menuEntityReturnsNewIfNotFound");
			_alwaysFetchMenuEntity = info.GetBoolean("_alwaysFetchMenuEntity");
			_alreadyFetchedMenuEntity = info.GetBoolean("_alreadyFetchedMenuEntity");

			_poscategoryEntity = (PoscategoryEntity)info.GetValue("_poscategoryEntity", typeof(PoscategoryEntity));
			if(_poscategoryEntity!=null)
			{
				_poscategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_poscategoryEntityReturnsNewIfNotFound = info.GetBoolean("_poscategoryEntityReturnsNewIfNotFound");
			_alwaysFetchPoscategoryEntity = info.GetBoolean("_alwaysFetchPoscategoryEntity");
			_alreadyFetchedPoscategoryEntity = info.GetBoolean("_alreadyFetchedPoscategoryEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_routeEntity = (RouteEntity)info.GetValue("_routeEntity", typeof(RouteEntity));
			if(_routeEntity!=null)
			{
				_routeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routeEntityReturnsNewIfNotFound = info.GetBoolean("_routeEntityReturnsNewIfNotFound");
			_alwaysFetchRouteEntity = info.GetBoolean("_alwaysFetchRouteEntity");
			_alreadyFetchedRouteEntity = info.GetBoolean("_alreadyFetchedRouteEntity");

			_scheduleEntity = (ScheduleEntity)info.GetValue("_scheduleEntity", typeof(ScheduleEntity));
			if(_scheduleEntity!=null)
			{
				_scheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_scheduleEntityReturnsNewIfNotFound = info.GetBoolean("_scheduleEntityReturnsNewIfNotFound");
			_alwaysFetchScheduleEntity = info.GetBoolean("_alwaysFetchScheduleEntity");
			_alreadyFetchedScheduleEntity = info.GetBoolean("_alreadyFetchedScheduleEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CategoryFieldIndex)fieldIndex)
			{
				case CategoryFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case CategoryFieldIndex.ParentCategoryId:
					DesetupSyncParentCategoryEntity(true, false);
					_alreadyFetchedParentCategoryEntity = false;
					break;
				case CategoryFieldIndex.GenericcategoryId:
					DesetupSyncGenericcategoryEntity(true, false);
					_alreadyFetchedGenericcategoryEntity = false;
					break;
				case CategoryFieldIndex.PoscategoryId:
					DesetupSyncPoscategoryEntity(true, false);
					_alreadyFetchedPoscategoryEntity = false;
					break;
				case CategoryFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case CategoryFieldIndex.RouteId:
					DesetupSyncRouteEntity(true, false);
					_alreadyFetchedRouteEntity = false;
					break;
				case CategoryFieldIndex.MenuId:
					DesetupSyncMenuEntity(true, false);
					_alreadyFetchedMenuEntity = false;
					break;
				case CategoryFieldIndex.ScheduleId:
					DesetupSyncScheduleEntity(true, false);
					_alreadyFetchedScheduleEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedAdvertisementTagCategoryCollection = (_advertisementTagCategoryCollection.Count > 0);
			_alreadyFetchedAnnouncementCollection_ = (_announcementCollection_.Count > 0);
			_alreadyFetchedAnnouncementCollection = (_announcementCollection.Count > 0);
			_alreadyFetchedActionCollection = (_actionCollection.Count > 0);
			_alreadyFetchedAvailabilityCollection = (_availabilityCollection.Count > 0);
			_alreadyFetchedChildCategoryCollection = (_childCategoryCollection.Count > 0);
			_alreadyFetchedCategoryAlterationCollection = (_categoryAlterationCollection.Count > 0);
			_alreadyFetchedCategoryLanguageCollection = (_categoryLanguageCollection.Count > 0);
			_alreadyFetchedCategorySuggestionCollection = (_categorySuggestionCollection.Count > 0);
			_alreadyFetchedCategoryTagCollection = (_categoryTagCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedActionMediaCollection = (_actionMediaCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessageRecipientCollection = (_messageRecipientCollection.Count > 0);
			_alreadyFetchedMessageTemplateCollection = (_messageTemplateCollection.Count > 0);
			_alreadyFetchedOrderHourCollection = (_orderHourCollection.Count > 0);
			_alreadyFetchedOrderitemCollection = (_orderitemCollection.Count > 0);
			_alreadyFetchedProductCategoryCollection = (_productCategoryCollection.Count > 0);
			_alreadyFetchedProductCategoryTagCollection = (_productCategoryTagCollection.Count > 0);
			_alreadyFetchedScheduledMessageCollection = (_scheduledMessageCollection.Count > 0);
			_alreadyFetchedUITabCollection = (_uITabCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedAdvertisementCollectionViaMedium = (_advertisementCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationCollectionViaMedium = (_alterationCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationCollectionViaCategoryAlteration = (_alterationCollectionViaCategoryAlteration.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium_ = (_alterationoptionCollectionViaMedium_.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium_ = (_categoryCollectionViaMedium_.Count > 0);
			_alreadyFetchedClientCollectionViaMessage = (_clientCollectionViaMessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaMedium = (_companyCollectionViaMedium.Count > 0);
			_alreadyFetchedCompanyCollectionViaMessage = (_companyCollectionViaMessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaMessageTemplate = (_companyCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedCompanyCollectionViaAdvertisement = (_companyCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedCustomerCollectionViaMessage = (_customerCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaMessage = (_deliverypointCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = (_deliverypointgroupCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = (_deliverypointgroupCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = (_deliverypointgroupCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = (_deliverypointgroupCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = (_entertainmentCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = (_entertainmentCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium_ = (_entertainmentCollectionViaMedium_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium__ = (_entertainmentCollectionViaMedium__.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessage = (_entertainmentCollectionViaMessage.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessageTemplate = (_entertainmentCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaUITab = (_entertainmentCollectionViaUITab.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = (_entertainmentCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = (_entertainmentCollectionViaAdvertisement_.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = (_entertainmentcategoryCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium_ = (_entertainmentcategoryCollectionViaMedium_.Count > 0);
			_alreadyFetchedGenericcategoryCollectionViaMedium = (_genericcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericproductCollectionViaMedium = (_genericproductCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericproductCollectionViaAdvertisement = (_genericproductCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedMediaCollectionViaAnnouncement = (_mediaCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedMediaCollectionViaAnnouncement_ = (_mediaCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedMediaCollectionViaMessage = (_mediaCollectionViaMessage.Count > 0);
			_alreadyFetchedMediaCollectionViaMessageTemplate = (_mediaCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedMenuCollectionViaCategory = (_menuCollectionViaCategory.Count > 0);
			_alreadyFetchedOrderCollectionViaMessage = (_orderCollectionViaMessage.Count > 0);
			_alreadyFetchedOrderCollectionViaOrderitem = (_orderCollectionViaOrderitem.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium_ = (_pointOfInterestCollectionViaMedium_.Count > 0);
			_alreadyFetchedProductCollectionViaAnnouncement = (_productCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedProductCollectionViaAnnouncement_ = (_productCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedProductCollectionViaCategorySuggestion = (_productCollectionViaCategorySuggestion.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium_ = (_productCollectionViaMedium_.Count > 0);
			_alreadyFetchedProductCollectionViaMedium__ = (_productCollectionViaMedium__.Count > 0);
			_alreadyFetchedProductCollectionViaMessageTemplate = (_productCollectionViaMessageTemplate.Count > 0);
			_alreadyFetchedProductCollectionViaOrderitem = (_productCollectionViaOrderitem.Count > 0);
			_alreadyFetchedProductCollectionViaProductCategory = (_productCollectionViaProductCategory.Count > 0);
			_alreadyFetchedProductCollectionViaAdvertisement = (_productCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedRouteCollectionViaCategory = (_routeCollectionViaCategory.Count > 0);
			_alreadyFetchedSupplierCollectionViaAdvertisement = (_supplierCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedSurveyCollectionViaMedium = (_surveyCollectionViaMedium.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium_ = (_surveyPageCollectionViaMedium_.Count > 0);
			_alreadyFetchedUIModeCollectionViaUITab = (_uIModeCollectionViaUITab.Count > 0);
			_alreadyFetchedParentCategoryEntity = (_parentCategoryEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedGenericcategoryEntity = (_genericcategoryEntity != null);
			_alreadyFetchedMenuEntity = (_menuEntity != null);
			_alreadyFetchedPoscategoryEntity = (_poscategoryEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedRouteEntity = (_routeEntity != null);
			_alreadyFetchedScheduleEntity = (_scheduleEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParentCategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryIdParentCategoryId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "GenericcategoryEntity":
					toReturn.Add(Relations.GenericcategoryEntityUsingGenericcategoryId);
					break;
				case "MenuEntity":
					toReturn.Add(Relations.MenuEntityUsingMenuId);
					break;
				case "PoscategoryEntity":
					toReturn.Add(Relations.PoscategoryEntityUsingPoscategoryId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "RouteEntity":
					toReturn.Add(Relations.RouteEntityUsingRouteId);
					break;
				case "ScheduleEntity":
					toReturn.Add(Relations.ScheduleEntityUsingScheduleId);
					break;
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId);
					break;
				case "AdvertisementTagCategoryCollection":
					toReturn.Add(Relations.AdvertisementTagCategoryEntityUsingCategoryId);
					break;
				case "AnnouncementCollection_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoCategory);
					break;
				case "AnnouncementCollection":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesCategory);
					break;
				case "ActionCollection":
					toReturn.Add(Relations.ActionEntityUsingCategoryId);
					break;
				case "AvailabilityCollection":
					toReturn.Add(Relations.AvailabilityEntityUsingActionCategoryId);
					break;
				case "ChildCategoryCollection":
					toReturn.Add(Relations.CategoryEntityUsingParentCategoryId);
					break;
				case "CategoryAlterationCollection":
					toReturn.Add(Relations.CategoryAlterationEntityUsingCategoryId);
					break;
				case "CategoryLanguageCollection":
					toReturn.Add(Relations.CategoryLanguageEntityUsingCategoryId);
					break;
				case "CategorySuggestionCollection":
					toReturn.Add(Relations.CategorySuggestionEntityUsingCategoryId);
					break;
				case "CategoryTagCollection":
					toReturn.Add(Relations.CategoryTagEntityUsingCategoryId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingCategoryId);
					break;
				case "ActionMediaCollection":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingCategoryId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingCategoryId);
					break;
				case "MessageRecipientCollection":
					toReturn.Add(Relations.MessageRecipientEntityUsingCategoryId);
					break;
				case "MessageTemplateCollection":
					toReturn.Add(Relations.MessageTemplateEntityUsingCategoryId);
					break;
				case "OrderHourCollection":
					toReturn.Add(Relations.OrderHourEntityUsingCategoryId);
					break;
				case "OrderitemCollection":
					toReturn.Add(Relations.OrderitemEntityUsingCategoryId);
					break;
				case "ProductCategoryCollection":
					toReturn.Add(Relations.ProductCategoryEntityUsingCategoryId);
					break;
				case "ProductCategoryTagCollection":
					toReturn.Add(Relations.ProductCategoryTagEntityUsingCategoryId);
					break;
				case "ScheduledMessageCollection":
					toReturn.Add(Relations.ScheduledMessageEntityUsingCategoryId);
					break;
				case "UITabCollection":
					toReturn.Add(Relations.UITabEntityUsingCategoryId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingCategoryId);
					break;
				case "AdvertisementCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationCollectionViaCategoryAlteration":
					toReturn.Add(Relations.CategoryAlterationEntityUsingCategoryId, "CategoryEntity__", "CategoryAlteration_", JoinHint.None);
					toReturn.Add(CategoryAlterationEntity.Relations.AlterationEntityUsingAlterationId, "CategoryAlteration_", string.Empty, JoinHint.None);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationoptionCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ClientCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCategoryId, "CategoryEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.ClientEntityUsingClientId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CompanyEntityUsingCompanyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCategoryId, "CategoryEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CompanyEntityUsingCompanyId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingCategoryId, "CategoryEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.CompanyEntityUsingCompanyId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.CompanyEntityUsingCompanyId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCategoryId, "CategoryEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CustomerEntityUsingCustomerId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCategoryId, "CategoryEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentEntityUsingOnYesEntertainment, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentEntityUsingOnYesEntertainment, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium__":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCategoryId, "CategoryEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Message_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingCategoryId, "CategoryEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.EntertainmentEntityUsingEntertainmentId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaUITab":
					toReturn.Add(Relations.UITabEntityUsingCategoryId, "CategoryEntity__", "UITab_", JoinHint.None);
					toReturn.Add(UITabEntity.Relations.EntertainmentEntityUsingEntertainmentId, "UITab_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement_":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericcategoryEntityUsingGenericcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericproductEntityUsingGenericproductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.GenericproductEntityUsingGenericproductId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.MediaEntityUsingMediaId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.MediaEntityUsingMediaId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCategoryId, "CategoryEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.MediaEntityUsingMediaId, "Message_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingCategoryId, "CategoryEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.MediaEntityUsingMediaId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingParentCategoryId, "CategoryEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.MenuEntityUsingMenuId, "Category_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCategoryId, "CategoryEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.OrderEntityUsingOrderId, "Message_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrderitem":
					toReturn.Add(Relations.OrderitemEntityUsingCategoryId, "CategoryEntity__", "Orderitem_", JoinHint.None);
					toReturn.Add(OrderitemEntity.Relations.OrderEntityUsingOrderId, "Orderitem_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.ProductEntityUsingOnYesProduct, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesCategory, "CategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.ProductEntityUsingOnYesProduct, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaCategorySuggestion":
					toReturn.Add(Relations.CategorySuggestionEntityUsingCategoryId, "CategoryEntity__", "CategorySuggestion_", JoinHint.None);
					toReturn.Add(CategorySuggestionEntity.Relations.ProductEntityUsingProductId, "CategorySuggestion_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium__":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMessageTemplate":
					toReturn.Add(Relations.MessageTemplateEntityUsingCategoryId, "CategoryEntity__", "MessageTemplate_", JoinHint.None);
					toReturn.Add(MessageTemplateEntity.Relations.ProductEntityUsingProductId, "MessageTemplate_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaOrderitem":
					toReturn.Add(Relations.OrderitemEntityUsingCategoryId, "CategoryEntity__", "Orderitem_", JoinHint.None);
					toReturn.Add(OrderitemEntity.Relations.ProductEntityUsingProductId, "Orderitem_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaProductCategory":
					toReturn.Add(Relations.ProductCategoryEntityUsingCategoryId, "CategoryEntity__", "ProductCategory_", JoinHint.None);
					toReturn.Add(ProductCategoryEntity.Relations.ProductEntityUsingProductId, "ProductCategory_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.ProductEntityUsingProductId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingParentCategoryId, "CategoryEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.RouteEntityUsingRouteId, "Category_", string.Empty, JoinHint.None);
					break;
				case "SupplierCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionCategoryId, "CategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.SupplierEntityUsingSupplierId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "SurveyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingCategoryId, "CategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaUITab":
					toReturn.Add(Relations.UITabEntityUsingCategoryId, "CategoryEntity__", "UITab_", JoinHint.None);
					toReturn.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId, "UITab_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_advertisementTagCategoryCollection", (!this.MarkedForDeletion?_advertisementTagCategoryCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagCategoryCollection", _alwaysFetchAdvertisementTagCategoryCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagCategoryCollection", _alreadyFetchedAdvertisementTagCategoryCollection);
			info.AddValue("_announcementCollection_", (!this.MarkedForDeletion?_announcementCollection_:null));
			info.AddValue("_alwaysFetchAnnouncementCollection_", _alwaysFetchAnnouncementCollection_);
			info.AddValue("_alreadyFetchedAnnouncementCollection_", _alreadyFetchedAnnouncementCollection_);
			info.AddValue("_announcementCollection", (!this.MarkedForDeletion?_announcementCollection:null));
			info.AddValue("_alwaysFetchAnnouncementCollection", _alwaysFetchAnnouncementCollection);
			info.AddValue("_alreadyFetchedAnnouncementCollection", _alreadyFetchedAnnouncementCollection);
			info.AddValue("_actionCollection", (!this.MarkedForDeletion?_actionCollection:null));
			info.AddValue("_alwaysFetchActionCollection", _alwaysFetchActionCollection);
			info.AddValue("_alreadyFetchedActionCollection", _alreadyFetchedActionCollection);
			info.AddValue("_availabilityCollection", (!this.MarkedForDeletion?_availabilityCollection:null));
			info.AddValue("_alwaysFetchAvailabilityCollection", _alwaysFetchAvailabilityCollection);
			info.AddValue("_alreadyFetchedAvailabilityCollection", _alreadyFetchedAvailabilityCollection);
			info.AddValue("_childCategoryCollection", (!this.MarkedForDeletion?_childCategoryCollection:null));
			info.AddValue("_alwaysFetchChildCategoryCollection", _alwaysFetchChildCategoryCollection);
			info.AddValue("_alreadyFetchedChildCategoryCollection", _alreadyFetchedChildCategoryCollection);
			info.AddValue("_categoryAlterationCollection", (!this.MarkedForDeletion?_categoryAlterationCollection:null));
			info.AddValue("_alwaysFetchCategoryAlterationCollection", _alwaysFetchCategoryAlterationCollection);
			info.AddValue("_alreadyFetchedCategoryAlterationCollection", _alreadyFetchedCategoryAlterationCollection);
			info.AddValue("_categoryLanguageCollection", (!this.MarkedForDeletion?_categoryLanguageCollection:null));
			info.AddValue("_alwaysFetchCategoryLanguageCollection", _alwaysFetchCategoryLanguageCollection);
			info.AddValue("_alreadyFetchedCategoryLanguageCollection", _alreadyFetchedCategoryLanguageCollection);
			info.AddValue("_categorySuggestionCollection", (!this.MarkedForDeletion?_categorySuggestionCollection:null));
			info.AddValue("_alwaysFetchCategorySuggestionCollection", _alwaysFetchCategorySuggestionCollection);
			info.AddValue("_alreadyFetchedCategorySuggestionCollection", _alreadyFetchedCategorySuggestionCollection);
			info.AddValue("_categoryTagCollection", (!this.MarkedForDeletion?_categoryTagCollection:null));
			info.AddValue("_alwaysFetchCategoryTagCollection", _alwaysFetchCategoryTagCollection);
			info.AddValue("_alreadyFetchedCategoryTagCollection", _alreadyFetchedCategoryTagCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_actionMediaCollection", (!this.MarkedForDeletion?_actionMediaCollection:null));
			info.AddValue("_alwaysFetchActionMediaCollection", _alwaysFetchActionMediaCollection);
			info.AddValue("_alreadyFetchedActionMediaCollection", _alreadyFetchedActionMediaCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messageRecipientCollection", (!this.MarkedForDeletion?_messageRecipientCollection:null));
			info.AddValue("_alwaysFetchMessageRecipientCollection", _alwaysFetchMessageRecipientCollection);
			info.AddValue("_alreadyFetchedMessageRecipientCollection", _alreadyFetchedMessageRecipientCollection);
			info.AddValue("_messageTemplateCollection", (!this.MarkedForDeletion?_messageTemplateCollection:null));
			info.AddValue("_alwaysFetchMessageTemplateCollection", _alwaysFetchMessageTemplateCollection);
			info.AddValue("_alreadyFetchedMessageTemplateCollection", _alreadyFetchedMessageTemplateCollection);
			info.AddValue("_orderHourCollection", (!this.MarkedForDeletion?_orderHourCollection:null));
			info.AddValue("_alwaysFetchOrderHourCollection", _alwaysFetchOrderHourCollection);
			info.AddValue("_alreadyFetchedOrderHourCollection", _alreadyFetchedOrderHourCollection);
			info.AddValue("_orderitemCollection", (!this.MarkedForDeletion?_orderitemCollection:null));
			info.AddValue("_alwaysFetchOrderitemCollection", _alwaysFetchOrderitemCollection);
			info.AddValue("_alreadyFetchedOrderitemCollection", _alreadyFetchedOrderitemCollection);
			info.AddValue("_productCategoryCollection", (!this.MarkedForDeletion?_productCategoryCollection:null));
			info.AddValue("_alwaysFetchProductCategoryCollection", _alwaysFetchProductCategoryCollection);
			info.AddValue("_alreadyFetchedProductCategoryCollection", _alreadyFetchedProductCategoryCollection);
			info.AddValue("_productCategoryTagCollection", (!this.MarkedForDeletion?_productCategoryTagCollection:null));
			info.AddValue("_alwaysFetchProductCategoryTagCollection", _alwaysFetchProductCategoryTagCollection);
			info.AddValue("_alreadyFetchedProductCategoryTagCollection", _alreadyFetchedProductCategoryTagCollection);
			info.AddValue("_scheduledMessageCollection", (!this.MarkedForDeletion?_scheduledMessageCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageCollection", _alwaysFetchScheduledMessageCollection);
			info.AddValue("_alreadyFetchedScheduledMessageCollection", _alreadyFetchedScheduledMessageCollection);
			info.AddValue("_uITabCollection", (!this.MarkedForDeletion?_uITabCollection:null));
			info.AddValue("_alwaysFetchUITabCollection", _alwaysFetchUITabCollection);
			info.AddValue("_alreadyFetchedUITabCollection", _alreadyFetchedUITabCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_advertisementCollectionViaMedium", (!this.MarkedForDeletion?_advertisementCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAdvertisementCollectionViaMedium", _alwaysFetchAdvertisementCollectionViaMedium);
			info.AddValue("_alreadyFetchedAdvertisementCollectionViaMedium", _alreadyFetchedAdvertisementCollectionViaMedium);
			info.AddValue("_alterationCollectionViaMedium", (!this.MarkedForDeletion?_alterationCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationCollectionViaMedium", _alwaysFetchAlterationCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationCollectionViaMedium", _alreadyFetchedAlterationCollectionViaMedium);
			info.AddValue("_alterationCollectionViaCategoryAlteration", (!this.MarkedForDeletion?_alterationCollectionViaCategoryAlteration:null));
			info.AddValue("_alwaysFetchAlterationCollectionViaCategoryAlteration", _alwaysFetchAlterationCollectionViaCategoryAlteration);
			info.AddValue("_alreadyFetchedAlterationCollectionViaCategoryAlteration", _alreadyFetchedAlterationCollectionViaCategoryAlteration);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_alterationoptionCollectionViaMedium_", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium_", _alwaysFetchAlterationoptionCollectionViaMedium_);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium_", _alreadyFetchedAlterationoptionCollectionViaMedium_);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium_", (!this.MarkedForDeletion?_categoryCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium_", _alwaysFetchCategoryCollectionViaMedium_);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium_", _alreadyFetchedCategoryCollectionViaMedium_);
			info.AddValue("_clientCollectionViaMessage", (!this.MarkedForDeletion?_clientCollectionViaMessage:null));
			info.AddValue("_alwaysFetchClientCollectionViaMessage", _alwaysFetchClientCollectionViaMessage);
			info.AddValue("_alreadyFetchedClientCollectionViaMessage", _alreadyFetchedClientCollectionViaMessage);
			info.AddValue("_companyCollectionViaMedium", (!this.MarkedForDeletion?_companyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMedium", _alwaysFetchCompanyCollectionViaMedium);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMedium", _alreadyFetchedCompanyCollectionViaMedium);
			info.AddValue("_companyCollectionViaMessage", (!this.MarkedForDeletion?_companyCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMessage", _alwaysFetchCompanyCollectionViaMessage);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMessage", _alreadyFetchedCompanyCollectionViaMessage);
			info.AddValue("_companyCollectionViaMessageTemplate", (!this.MarkedForDeletion?_companyCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMessageTemplate", _alwaysFetchCompanyCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMessageTemplate", _alreadyFetchedCompanyCollectionViaMessageTemplate);
			info.AddValue("_companyCollectionViaAdvertisement", (!this.MarkedForDeletion?_companyCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaAdvertisement", _alwaysFetchCompanyCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedCompanyCollectionViaAdvertisement", _alreadyFetchedCompanyCollectionViaAdvertisement);
			info.AddValue("_customerCollectionViaMessage", (!this.MarkedForDeletion?_customerCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaMessage", _alwaysFetchCustomerCollectionViaMessage);
			info.AddValue("_alreadyFetchedCustomerCollectionViaMessage", _alreadyFetchedCustomerCollectionViaMessage);
			info.AddValue("_deliverypointCollectionViaMessage", (!this.MarkedForDeletion?_deliverypointCollectionViaMessage:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaMessage", _alwaysFetchDeliverypointCollectionViaMessage);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaMessage", _alreadyFetchedDeliverypointCollectionViaMessage);
			info.AddValue("_deliverypointgroupCollectionViaAdvertisement", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement", _alwaysFetchDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement", _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_deliverypointgroupCollectionViaAnnouncement", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement", _alwaysFetchDeliverypointgroupCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement", _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement);
			info.AddValue("_deliverypointgroupCollectionViaAnnouncement_", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_", _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_", _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_);
			info.AddValue("_deliverypointgroupCollectionViaMedium", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaMedium:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaMedium", _alwaysFetchDeliverypointgroupCollectionViaMedium);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaMedium", _alreadyFetchedDeliverypointgroupCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaAnnouncement", (!this.MarkedForDeletion?_entertainmentCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAnnouncement", _alwaysFetchEntertainmentCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAnnouncement", _alreadyFetchedEntertainmentCollectionViaAnnouncement);
			info.AddValue("_entertainmentCollectionViaAnnouncement_", (!this.MarkedForDeletion?_entertainmentCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAnnouncement_", _alwaysFetchEntertainmentCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAnnouncement_", _alreadyFetchedEntertainmentCollectionViaAnnouncement_);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium_", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium_", _alwaysFetchEntertainmentCollectionViaMedium_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium_", _alreadyFetchedEntertainmentCollectionViaMedium_);
			info.AddValue("_entertainmentCollectionViaMedium__", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium__:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium__", _alwaysFetchEntertainmentCollectionViaMedium__);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium__", _alreadyFetchedEntertainmentCollectionViaMedium__);
			info.AddValue("_entertainmentCollectionViaMessage", (!this.MarkedForDeletion?_entertainmentCollectionViaMessage:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessage", _alwaysFetchEntertainmentCollectionViaMessage);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessage", _alreadyFetchedEntertainmentCollectionViaMessage);
			info.AddValue("_entertainmentCollectionViaMessageTemplate", (!this.MarkedForDeletion?_entertainmentCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessageTemplate", _alwaysFetchEntertainmentCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessageTemplate", _alreadyFetchedEntertainmentCollectionViaMessageTemplate);
			info.AddValue("_entertainmentCollectionViaUITab", (!this.MarkedForDeletion?_entertainmentCollectionViaUITab:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaUITab", _alwaysFetchEntertainmentCollectionViaUITab);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaUITab", _alreadyFetchedEntertainmentCollectionViaUITab);
			info.AddValue("_entertainmentCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement", _alwaysFetchEntertainmentCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement", _alreadyFetchedEntertainmentCollectionViaAdvertisement);
			info.AddValue("_entertainmentCollectionViaAdvertisement_", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement_", _alwaysFetchEntertainmentCollectionViaAdvertisement_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement_", _alreadyFetchedEntertainmentCollectionViaAdvertisement_);
			info.AddValue("_entertainmentcategoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement", _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement", _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_entertainmentcategoryCollectionViaMedium_", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium_", _alwaysFetchEntertainmentcategoryCollectionViaMedium_);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium_", _alreadyFetchedEntertainmentcategoryCollectionViaMedium_);
			info.AddValue("_genericcategoryCollectionViaMedium", (!this.MarkedForDeletion?_genericcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericcategoryCollectionViaMedium", _alwaysFetchGenericcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericcategoryCollectionViaMedium", _alreadyFetchedGenericcategoryCollectionViaMedium);
			info.AddValue("_genericproductCollectionViaMedium", (!this.MarkedForDeletion?_genericproductCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaMedium", _alwaysFetchGenericproductCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaMedium", _alreadyFetchedGenericproductCollectionViaMedium);
			info.AddValue("_genericproductCollectionViaAdvertisement", (!this.MarkedForDeletion?_genericproductCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaAdvertisement", _alwaysFetchGenericproductCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaAdvertisement", _alreadyFetchedGenericproductCollectionViaAdvertisement);
			info.AddValue("_mediaCollectionViaAnnouncement", (!this.MarkedForDeletion?_mediaCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchMediaCollectionViaAnnouncement", _alwaysFetchMediaCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedMediaCollectionViaAnnouncement", _alreadyFetchedMediaCollectionViaAnnouncement);
			info.AddValue("_mediaCollectionViaAnnouncement_", (!this.MarkedForDeletion?_mediaCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchMediaCollectionViaAnnouncement_", _alwaysFetchMediaCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedMediaCollectionViaAnnouncement_", _alreadyFetchedMediaCollectionViaAnnouncement_);
			info.AddValue("_mediaCollectionViaMessage", (!this.MarkedForDeletion?_mediaCollectionViaMessage:null));
			info.AddValue("_alwaysFetchMediaCollectionViaMessage", _alwaysFetchMediaCollectionViaMessage);
			info.AddValue("_alreadyFetchedMediaCollectionViaMessage", _alreadyFetchedMediaCollectionViaMessage);
			info.AddValue("_mediaCollectionViaMessageTemplate", (!this.MarkedForDeletion?_mediaCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchMediaCollectionViaMessageTemplate", _alwaysFetchMediaCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedMediaCollectionViaMessageTemplate", _alreadyFetchedMediaCollectionViaMessageTemplate);
			info.AddValue("_menuCollectionViaCategory", (!this.MarkedForDeletion?_menuCollectionViaCategory:null));
			info.AddValue("_alwaysFetchMenuCollectionViaCategory", _alwaysFetchMenuCollectionViaCategory);
			info.AddValue("_alreadyFetchedMenuCollectionViaCategory", _alreadyFetchedMenuCollectionViaCategory);
			info.AddValue("_orderCollectionViaMessage", (!this.MarkedForDeletion?_orderCollectionViaMessage:null));
			info.AddValue("_alwaysFetchOrderCollectionViaMessage", _alwaysFetchOrderCollectionViaMessage);
			info.AddValue("_alreadyFetchedOrderCollectionViaMessage", _alreadyFetchedOrderCollectionViaMessage);
			info.AddValue("_orderCollectionViaOrderitem", (!this.MarkedForDeletion?_orderCollectionViaOrderitem:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrderitem", _alwaysFetchOrderCollectionViaOrderitem);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrderitem", _alreadyFetchedOrderCollectionViaOrderitem);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_pointOfInterestCollectionViaMedium_", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium_", _alwaysFetchPointOfInterestCollectionViaMedium_);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium_", _alreadyFetchedPointOfInterestCollectionViaMedium_);
			info.AddValue("_productCollectionViaAnnouncement", (!this.MarkedForDeletion?_productCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchProductCollectionViaAnnouncement", _alwaysFetchProductCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedProductCollectionViaAnnouncement", _alreadyFetchedProductCollectionViaAnnouncement);
			info.AddValue("_productCollectionViaAnnouncement_", (!this.MarkedForDeletion?_productCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchProductCollectionViaAnnouncement_", _alwaysFetchProductCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedProductCollectionViaAnnouncement_", _alreadyFetchedProductCollectionViaAnnouncement_);
			info.AddValue("_productCollectionViaCategorySuggestion", (!this.MarkedForDeletion?_productCollectionViaCategorySuggestion:null));
			info.AddValue("_alwaysFetchProductCollectionViaCategorySuggestion", _alwaysFetchProductCollectionViaCategorySuggestion);
			info.AddValue("_alreadyFetchedProductCollectionViaCategorySuggestion", _alreadyFetchedProductCollectionViaCategorySuggestion);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium_", (!this.MarkedForDeletion?_productCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium_", _alwaysFetchProductCollectionViaMedium_);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium_", _alreadyFetchedProductCollectionViaMedium_);
			info.AddValue("_productCollectionViaMedium__", (!this.MarkedForDeletion?_productCollectionViaMedium__:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium__", _alwaysFetchProductCollectionViaMedium__);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium__", _alreadyFetchedProductCollectionViaMedium__);
			info.AddValue("_productCollectionViaMessageTemplate", (!this.MarkedForDeletion?_productCollectionViaMessageTemplate:null));
			info.AddValue("_alwaysFetchProductCollectionViaMessageTemplate", _alwaysFetchProductCollectionViaMessageTemplate);
			info.AddValue("_alreadyFetchedProductCollectionViaMessageTemplate", _alreadyFetchedProductCollectionViaMessageTemplate);
			info.AddValue("_productCollectionViaOrderitem", (!this.MarkedForDeletion?_productCollectionViaOrderitem:null));
			info.AddValue("_alwaysFetchProductCollectionViaOrderitem", _alwaysFetchProductCollectionViaOrderitem);
			info.AddValue("_alreadyFetchedProductCollectionViaOrderitem", _alreadyFetchedProductCollectionViaOrderitem);
			info.AddValue("_productCollectionViaProductCategory", (!this.MarkedForDeletion?_productCollectionViaProductCategory:null));
			info.AddValue("_alwaysFetchProductCollectionViaProductCategory", _alwaysFetchProductCollectionViaProductCategory);
			info.AddValue("_alreadyFetchedProductCollectionViaProductCategory", _alreadyFetchedProductCollectionViaProductCategory);
			info.AddValue("_productCollectionViaAdvertisement", (!this.MarkedForDeletion?_productCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchProductCollectionViaAdvertisement", _alwaysFetchProductCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedProductCollectionViaAdvertisement", _alreadyFetchedProductCollectionViaAdvertisement);
			info.AddValue("_routeCollectionViaCategory", (!this.MarkedForDeletion?_routeCollectionViaCategory:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCategory", _alwaysFetchRouteCollectionViaCategory);
			info.AddValue("_alreadyFetchedRouteCollectionViaCategory", _alreadyFetchedRouteCollectionViaCategory);
			info.AddValue("_supplierCollectionViaAdvertisement", (!this.MarkedForDeletion?_supplierCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchSupplierCollectionViaAdvertisement", _alwaysFetchSupplierCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedSupplierCollectionViaAdvertisement", _alreadyFetchedSupplierCollectionViaAdvertisement);
			info.AddValue("_surveyCollectionViaMedium", (!this.MarkedForDeletion?_surveyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyCollectionViaMedium", _alwaysFetchSurveyCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyCollectionViaMedium", _alreadyFetchedSurveyCollectionViaMedium);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);
			info.AddValue("_surveyPageCollectionViaMedium_", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium_", _alwaysFetchSurveyPageCollectionViaMedium_);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium_", _alreadyFetchedSurveyPageCollectionViaMedium_);
			info.AddValue("_uIModeCollectionViaUITab", (!this.MarkedForDeletion?_uIModeCollectionViaUITab:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaUITab", _alwaysFetchUIModeCollectionViaUITab);
			info.AddValue("_alreadyFetchedUIModeCollectionViaUITab", _alreadyFetchedUIModeCollectionViaUITab);
			info.AddValue("_parentCategoryEntity", (!this.MarkedForDeletion?_parentCategoryEntity:null));
			info.AddValue("_parentCategoryEntityReturnsNewIfNotFound", _parentCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentCategoryEntity", _alwaysFetchParentCategoryEntity);
			info.AddValue("_alreadyFetchedParentCategoryEntity", _alreadyFetchedParentCategoryEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_genericcategoryEntity", (!this.MarkedForDeletion?_genericcategoryEntity:null));
			info.AddValue("_genericcategoryEntityReturnsNewIfNotFound", _genericcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericcategoryEntity", _alwaysFetchGenericcategoryEntity);
			info.AddValue("_alreadyFetchedGenericcategoryEntity", _alreadyFetchedGenericcategoryEntity);
			info.AddValue("_menuEntity", (!this.MarkedForDeletion?_menuEntity:null));
			info.AddValue("_menuEntityReturnsNewIfNotFound", _menuEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMenuEntity", _alwaysFetchMenuEntity);
			info.AddValue("_alreadyFetchedMenuEntity", _alreadyFetchedMenuEntity);
			info.AddValue("_poscategoryEntity", (!this.MarkedForDeletion?_poscategoryEntity:null));
			info.AddValue("_poscategoryEntityReturnsNewIfNotFound", _poscategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPoscategoryEntity", _alwaysFetchPoscategoryEntity);
			info.AddValue("_alreadyFetchedPoscategoryEntity", _alreadyFetchedPoscategoryEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_routeEntity", (!this.MarkedForDeletion?_routeEntity:null));
			info.AddValue("_routeEntityReturnsNewIfNotFound", _routeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRouteEntity", _alwaysFetchRouteEntity);
			info.AddValue("_alreadyFetchedRouteEntity", _alreadyFetchedRouteEntity);
			info.AddValue("_scheduleEntity", (!this.MarkedForDeletion?_scheduleEntity:null));
			info.AddValue("_scheduleEntityReturnsNewIfNotFound", _scheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchScheduleEntity", _alwaysFetchScheduleEntity);
			info.AddValue("_alreadyFetchedScheduleEntity", _alreadyFetchedScheduleEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParentCategoryEntity":
					_alreadyFetchedParentCategoryEntity = true;
					this.ParentCategoryEntity = (CategoryEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "GenericcategoryEntity":
					_alreadyFetchedGenericcategoryEntity = true;
					this.GenericcategoryEntity = (GenericcategoryEntity)entity;
					break;
				case "MenuEntity":
					_alreadyFetchedMenuEntity = true;
					this.MenuEntity = (MenuEntity)entity;
					break;
				case "PoscategoryEntity":
					_alreadyFetchedPoscategoryEntity = true;
					this.PoscategoryEntity = (PoscategoryEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "RouteEntity":
					_alreadyFetchedRouteEntity = true;
					this.RouteEntity = (RouteEntity)entity;
					break;
				case "ScheduleEntity":
					_alreadyFetchedScheduleEntity = true;
					this.ScheduleEntity = (ScheduleEntity)entity;
					break;
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "AdvertisementTagCategoryCollection":
					_alreadyFetchedAdvertisementTagCategoryCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagCategoryCollection.Add((AdvertisementTagCategoryEntity)entity);
					}
					break;
				case "AnnouncementCollection_":
					_alreadyFetchedAnnouncementCollection_ = true;
					if(entity!=null)
					{
						this.AnnouncementCollection_.Add((AnnouncementEntity)entity);
					}
					break;
				case "AnnouncementCollection":
					_alreadyFetchedAnnouncementCollection = true;
					if(entity!=null)
					{
						this.AnnouncementCollection.Add((AnnouncementEntity)entity);
					}
					break;
				case "ActionCollection":
					_alreadyFetchedActionCollection = true;
					if(entity!=null)
					{
						this.ActionCollection.Add((ActionEntity)entity);
					}
					break;
				case "AvailabilityCollection":
					_alreadyFetchedAvailabilityCollection = true;
					if(entity!=null)
					{
						this.AvailabilityCollection.Add((AvailabilityEntity)entity);
					}
					break;
				case "ChildCategoryCollection":
					_alreadyFetchedChildCategoryCollection = true;
					if(entity!=null)
					{
						this.ChildCategoryCollection.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryAlterationCollection":
					_alreadyFetchedCategoryAlterationCollection = true;
					if(entity!=null)
					{
						this.CategoryAlterationCollection.Add((CategoryAlterationEntity)entity);
					}
					break;
				case "CategoryLanguageCollection":
					_alreadyFetchedCategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.CategoryLanguageCollection.Add((CategoryLanguageEntity)entity);
					}
					break;
				case "CategorySuggestionCollection":
					_alreadyFetchedCategorySuggestionCollection = true;
					if(entity!=null)
					{
						this.CategorySuggestionCollection.Add((CategorySuggestionEntity)entity);
					}
					break;
				case "CategoryTagCollection":
					_alreadyFetchedCategoryTagCollection = true;
					if(entity!=null)
					{
						this.CategoryTagCollection.Add((CategoryTagEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "ActionMediaCollection":
					_alreadyFetchedActionMediaCollection = true;
					if(entity!=null)
					{
						this.ActionMediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessageRecipientCollection":
					_alreadyFetchedMessageRecipientCollection = true;
					if(entity!=null)
					{
						this.MessageRecipientCollection.Add((MessageRecipientEntity)entity);
					}
					break;
				case "MessageTemplateCollection":
					_alreadyFetchedMessageTemplateCollection = true;
					if(entity!=null)
					{
						this.MessageTemplateCollection.Add((MessageTemplateEntity)entity);
					}
					break;
				case "OrderHourCollection":
					_alreadyFetchedOrderHourCollection = true;
					if(entity!=null)
					{
						this.OrderHourCollection.Add((OrderHourEntity)entity);
					}
					break;
				case "OrderitemCollection":
					_alreadyFetchedOrderitemCollection = true;
					if(entity!=null)
					{
						this.OrderitemCollection.Add((OrderitemEntity)entity);
					}
					break;
				case "ProductCategoryCollection":
					_alreadyFetchedProductCategoryCollection = true;
					if(entity!=null)
					{
						this.ProductCategoryCollection.Add((ProductCategoryEntity)entity);
					}
					break;
				case "ProductCategoryTagCollection":
					_alreadyFetchedProductCategoryTagCollection = true;
					if(entity!=null)
					{
						this.ProductCategoryTagCollection.Add((ProductCategoryTagEntity)entity);
					}
					break;
				case "ScheduledMessageCollection":
					_alreadyFetchedScheduledMessageCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageCollection.Add((ScheduledMessageEntity)entity);
					}
					break;
				case "UITabCollection":
					_alreadyFetchedUITabCollection = true;
					if(entity!=null)
					{
						this.UITabCollection.Add((UITabEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				case "AdvertisementCollectionViaMedium":
					_alreadyFetchedAdvertisementCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AdvertisementCollectionViaMedium.Add((AdvertisementEntity)entity);
					}
					break;
				case "AlterationCollectionViaMedium":
					_alreadyFetchedAlterationCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationCollectionViaMedium.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationCollectionViaCategoryAlteration":
					_alreadyFetchedAlterationCollectionViaCategoryAlteration = true;
					if(entity!=null)
					{
						this.AlterationCollectionViaCategoryAlteration.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium_":
					_alreadyFetchedAlterationoptionCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium_.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium_":
					_alreadyFetchedCategoryCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium_.Add((CategoryEntity)entity);
					}
					break;
				case "ClientCollectionViaMessage":
					_alreadyFetchedClientCollectionViaMessage = true;
					if(entity!=null)
					{
						this.ClientCollectionViaMessage.Add((ClientEntity)entity);
					}
					break;
				case "CompanyCollectionViaMedium":
					_alreadyFetchedCompanyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMedium.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaMessage":
					_alreadyFetchedCompanyCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMessage.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaMessageTemplate":
					_alreadyFetchedCompanyCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMessageTemplate.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaAdvertisement":
					_alreadyFetchedCompanyCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaAdvertisement.Add((CompanyEntity)entity);
					}
					break;
				case "CustomerCollectionViaMessage":
					_alreadyFetchedCustomerCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaMessage.Add((CustomerEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaMessage":
					_alreadyFetchedDeliverypointCollectionViaMessage = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaMessage.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAdvertisement.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAnnouncement":
					_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAnnouncement.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAnnouncement_":
					_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAnnouncement_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaMedium":
					_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaMedium.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAnnouncement":
					_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAnnouncement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAnnouncement_":
					_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAnnouncement_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium_":
					_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium__":
					_alreadyFetchedEntertainmentCollectionViaMedium__ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium__.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessage":
					_alreadyFetchedEntertainmentCollectionViaMessage = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessage.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessageTemplate":
					_alreadyFetchedEntertainmentCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessageTemplate.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaUITab":
					_alreadyFetchedEntertainmentCollectionViaUITab = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaUITab.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement_":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAdvertisement.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium_":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium_.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "GenericcategoryCollectionViaMedium":
					_alreadyFetchedGenericcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericcategoryCollectionViaMedium.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericproductCollectionViaMedium":
					_alreadyFetchedGenericproductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaMedium.Add((GenericproductEntity)entity);
					}
					break;
				case "GenericproductCollectionViaAdvertisement":
					_alreadyFetchedGenericproductCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaAdvertisement.Add((GenericproductEntity)entity);
					}
					break;
				case "MediaCollectionViaAnnouncement":
					_alreadyFetchedMediaCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.MediaCollectionViaAnnouncement.Add((MediaEntity)entity);
					}
					break;
				case "MediaCollectionViaAnnouncement_":
					_alreadyFetchedMediaCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.MediaCollectionViaAnnouncement_.Add((MediaEntity)entity);
					}
					break;
				case "MediaCollectionViaMessage":
					_alreadyFetchedMediaCollectionViaMessage = true;
					if(entity!=null)
					{
						this.MediaCollectionViaMessage.Add((MediaEntity)entity);
					}
					break;
				case "MediaCollectionViaMessageTemplate":
					_alreadyFetchedMediaCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.MediaCollectionViaMessageTemplate.Add((MediaEntity)entity);
					}
					break;
				case "MenuCollectionViaCategory":
					_alreadyFetchedMenuCollectionViaCategory = true;
					if(entity!=null)
					{
						this.MenuCollectionViaCategory.Add((MenuEntity)entity);
					}
					break;
				case "OrderCollectionViaMessage":
					_alreadyFetchedOrderCollectionViaMessage = true;
					if(entity!=null)
					{
						this.OrderCollectionViaMessage.Add((OrderEntity)entity);
					}
					break;
				case "OrderCollectionViaOrderitem":
					_alreadyFetchedOrderCollectionViaOrderitem = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrderitem.Add((OrderEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium_":
					_alreadyFetchedPointOfInterestCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium_.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaAnnouncement":
					_alreadyFetchedProductCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAnnouncement.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaAnnouncement_":
					_alreadyFetchedProductCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAnnouncement_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaCategorySuggestion":
					_alreadyFetchedProductCollectionViaCategorySuggestion = true;
					if(entity!=null)
					{
						this.ProductCollectionViaCategorySuggestion.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium_":
					_alreadyFetchedProductCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium__":
					_alreadyFetchedProductCollectionViaMedium__ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium__.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMessageTemplate":
					_alreadyFetchedProductCollectionViaMessageTemplate = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMessageTemplate.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaOrderitem":
					_alreadyFetchedProductCollectionViaOrderitem = true;
					if(entity!=null)
					{
						this.ProductCollectionViaOrderitem.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaProductCategory":
					_alreadyFetchedProductCollectionViaProductCategory = true;
					if(entity!=null)
					{
						this.ProductCollectionViaProductCategory.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaAdvertisement":
					_alreadyFetchedProductCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAdvertisement.Add((ProductEntity)entity);
					}
					break;
				case "RouteCollectionViaCategory":
					_alreadyFetchedRouteCollectionViaCategory = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCategory.Add((RouteEntity)entity);
					}
					break;
				case "SupplierCollectionViaAdvertisement":
					_alreadyFetchedSupplierCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.SupplierCollectionViaAdvertisement.Add((SupplierEntity)entity);
					}
					break;
				case "SurveyCollectionViaMedium":
					_alreadyFetchedSurveyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyCollectionViaMedium.Add((SurveyEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium_":
					_alreadyFetchedSurveyPageCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium_.Add((SurveyPageEntity)entity);
					}
					break;
				case "UIModeCollectionViaUITab":
					_alreadyFetchedUIModeCollectionViaUITab = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaUITab.Add((UIModeEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParentCategoryEntity":
					SetupSyncParentCategoryEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "GenericcategoryEntity":
					SetupSyncGenericcategoryEntity(relatedEntity);
					break;
				case "MenuEntity":
					SetupSyncMenuEntity(relatedEntity);
					break;
				case "PoscategoryEntity":
					SetupSyncPoscategoryEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "RouteEntity":
					SetupSyncRouteEntity(relatedEntity);
					break;
				case "ScheduleEntity":
					SetupSyncScheduleEntity(relatedEntity);
					break;
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "AdvertisementTagCategoryCollection":
					_advertisementTagCategoryCollection.Add((AdvertisementTagCategoryEntity)relatedEntity);
					break;
				case "AnnouncementCollection_":
					_announcementCollection_.Add((AnnouncementEntity)relatedEntity);
					break;
				case "AnnouncementCollection":
					_announcementCollection.Add((AnnouncementEntity)relatedEntity);
					break;
				case "ActionCollection":
					_actionCollection.Add((ActionEntity)relatedEntity);
					break;
				case "AvailabilityCollection":
					_availabilityCollection.Add((AvailabilityEntity)relatedEntity);
					break;
				case "ChildCategoryCollection":
					_childCategoryCollection.Add((CategoryEntity)relatedEntity);
					break;
				case "CategoryAlterationCollection":
					_categoryAlterationCollection.Add((CategoryAlterationEntity)relatedEntity);
					break;
				case "CategoryLanguageCollection":
					_categoryLanguageCollection.Add((CategoryLanguageEntity)relatedEntity);
					break;
				case "CategorySuggestionCollection":
					_categorySuggestionCollection.Add((CategorySuggestionEntity)relatedEntity);
					break;
				case "CategoryTagCollection":
					_categoryTagCollection.Add((CategoryTagEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "ActionMediaCollection":
					_actionMediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessageRecipientCollection":
					_messageRecipientCollection.Add((MessageRecipientEntity)relatedEntity);
					break;
				case "MessageTemplateCollection":
					_messageTemplateCollection.Add((MessageTemplateEntity)relatedEntity);
					break;
				case "OrderHourCollection":
					_orderHourCollection.Add((OrderHourEntity)relatedEntity);
					break;
				case "OrderitemCollection":
					_orderitemCollection.Add((OrderitemEntity)relatedEntity);
					break;
				case "ProductCategoryCollection":
					_productCategoryCollection.Add((ProductCategoryEntity)relatedEntity);
					break;
				case "ProductCategoryTagCollection":
					_productCategoryTagCollection.Add((ProductCategoryTagEntity)relatedEntity);
					break;
				case "ScheduledMessageCollection":
					_scheduledMessageCollection.Add((ScheduledMessageEntity)relatedEntity);
					break;
				case "UITabCollection":
					_uITabCollection.Add((UITabEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParentCategoryEntity":
					DesetupSyncParentCategoryEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "GenericcategoryEntity":
					DesetupSyncGenericcategoryEntity(false, true);
					break;
				case "MenuEntity":
					DesetupSyncMenuEntity(false, true);
					break;
				case "PoscategoryEntity":
					DesetupSyncPoscategoryEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "RouteEntity":
					DesetupSyncRouteEntity(false, true);
					break;
				case "ScheduleEntity":
					DesetupSyncScheduleEntity(false, true);
					break;
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagCategoryCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AnnouncementCollection_":
					this.PerformRelatedEntityRemoval(_announcementCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AnnouncementCollection":
					this.PerformRelatedEntityRemoval(_announcementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ActionCollection":
					this.PerformRelatedEntityRemoval(_actionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AvailabilityCollection":
					this.PerformRelatedEntityRemoval(_availabilityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ChildCategoryCollection":
					this.PerformRelatedEntityRemoval(_childCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategoryAlterationCollection":
					this.PerformRelatedEntityRemoval(_categoryAlterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_categoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategorySuggestionCollection":
					this.PerformRelatedEntityRemoval(_categorySuggestionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategoryTagCollection":
					this.PerformRelatedEntityRemoval(_categoryTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ActionMediaCollection":
					this.PerformRelatedEntityRemoval(_actionMediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageRecipientCollection":
					this.PerformRelatedEntityRemoval(_messageRecipientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageTemplateCollection":
					this.PerformRelatedEntityRemoval(_messageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderHourCollection":
					this.PerformRelatedEntityRemoval(_orderHourCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemCollection":
					this.PerformRelatedEntityRemoval(_orderitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCategoryCollection":
					this.PerformRelatedEntityRemoval(_productCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCategoryTagCollection":
					this.PerformRelatedEntityRemoval(_productCategoryTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UITabCollection":
					this.PerformRelatedEntityRemoval(_uITabCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_parentCategoryEntity!=null)
			{
				toReturn.Add(_parentCategoryEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_genericcategoryEntity!=null)
			{
				toReturn.Add(_genericcategoryEntity);
			}
			if(_menuEntity!=null)
			{
				toReturn.Add(_menuEntity);
			}
			if(_poscategoryEntity!=null)
			{
				toReturn.Add(_poscategoryEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_routeEntity!=null)
			{
				toReturn.Add(_routeEntity);
			}
			if(_scheduleEntity!=null)
			{
				toReturn.Add(_scheduleEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_advertisementTagCategoryCollection);
			toReturn.Add(_announcementCollection_);
			toReturn.Add(_announcementCollection);
			toReturn.Add(_actionCollection);
			toReturn.Add(_availabilityCollection);
			toReturn.Add(_childCategoryCollection);
			toReturn.Add(_categoryAlterationCollection);
			toReturn.Add(_categoryLanguageCollection);
			toReturn.Add(_categorySuggestionCollection);
			toReturn.Add(_categoryTagCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_actionMediaCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messageRecipientCollection);
			toReturn.Add(_messageTemplateCollection);
			toReturn.Add(_orderHourCollection);
			toReturn.Add(_orderitemCollection);
			toReturn.Add(_productCategoryCollection);
			toReturn.Add(_productCategoryTagCollection);
			toReturn.Add(_scheduledMessageCollection);
			toReturn.Add(_uITabCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryId)
		{
			return FetchUsingPK(categoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(categoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(categoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(categoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CategoryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagCategoryCollection(forceFetch, _advertisementTagCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagCategoryCollection(forceFetch, _advertisementTagCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection GetMultiAdvertisementTagCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagCategoryCollection || forceFetch || _alwaysFetchAdvertisementTagCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagCategoryCollection);
				_advertisementTagCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagCategoryCollection.GetMultiManyToOne(null, this, filter);
				_advertisementTagCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagCategoryCollection = true;
			}
			return _advertisementTagCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagCategoryCollection'. These settings will be taken into account
		/// when the property AdvertisementTagCategoryCollection is requested or GetMultiAdvertisementTagCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagCategoryCollection.SortClauses=sortClauses;
			_advertisementTagCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch)
		{
			return GetMultiAnnouncementCollection_(forceFetch, _announcementCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementCollection_(forceFetch, _announcementCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementCollection_ || forceFetch || _alwaysFetchAnnouncementCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollection_);
				_announcementCollection_.SuppressClearInGetMulti=!forceFetch;
				_announcementCollection_.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollection_.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, filter);
				_announcementCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollection_ = true;
			}
			return _announcementCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollection_'. These settings will be taken into account
		/// when the property AnnouncementCollection_ is requested or GetMultiAnnouncementCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollection_.SortClauses=sortClauses;
			_announcementCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementCollection || forceFetch || _alwaysFetchAnnouncementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollection);
				_announcementCollection.SuppressClearInGetMulti=!forceFetch;
				_announcementCollection.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollection.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, filter);
				_announcementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollection = true;
			}
			return _announcementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollection'. These settings will be taken into account
		/// when the property AnnouncementCollection is requested or GetMultiAnnouncementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollection.SortClauses=sortClauses;
			_announcementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ActionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch)
		{
			return GetMultiActionCollection(forceFetch, _actionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ActionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiActionCollection(forceFetch, _actionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiActionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedActionCollection || forceFetch || _alwaysFetchActionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_actionCollection);
				_actionCollection.SuppressClearInGetMulti=!forceFetch;
				_actionCollection.EntityFactoryToUse = entityFactoryToUse;
				_actionCollection.GetMultiManyToOne(null, null, this, null, null, filter);
				_actionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedActionCollection = true;
			}
			return _actionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ActionCollection'. These settings will be taken into account
		/// when the property ActionCollection is requested or GetMultiActionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersActionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_actionCollection.SortClauses=sortClauses;
			_actionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAvailabilityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAvailabilityCollection || forceFetch || _alwaysFetchAvailabilityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_availabilityCollection);
				_availabilityCollection.SuppressClearInGetMulti=!forceFetch;
				_availabilityCollection.EntityFactoryToUse = entityFactoryToUse;
				_availabilityCollection.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_availabilityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAvailabilityCollection = true;
			}
			return _availabilityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AvailabilityCollection'. These settings will be taken into account
		/// when the property AvailabilityCollection is requested or GetMultiAvailabilityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAvailabilityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_availabilityCollection.SortClauses=sortClauses;
			_availabilityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiChildCategoryCollection(bool forceFetch)
		{
			return GetMultiChildCategoryCollection(forceFetch, _childCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiChildCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChildCategoryCollection(forceFetch, _childCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiChildCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChildCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection GetMultiChildCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChildCategoryCollection || forceFetch || _alwaysFetchChildCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_childCategoryCollection);
				_childCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_childCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_childCategoryCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_childCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedChildCategoryCollection = true;
			}
			return _childCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ChildCategoryCollection'. These settings will be taken into account
		/// when the property ChildCategoryCollection is requested or GetMultiChildCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChildCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_childCategoryCollection.SortClauses=sortClauses;
			_childCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch)
		{
			return GetMultiCategoryAlterationCollection(forceFetch, _categoryAlterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryAlterationCollection(forceFetch, _categoryAlterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryAlterationCollection || forceFetch || _alwaysFetchCategoryAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryAlterationCollection);
				_categoryAlterationCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryAlterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryAlterationCollection.GetMultiManyToOne(null, this, filter);
				_categoryAlterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryAlterationCollection = true;
			}
			return _categoryAlterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryAlterationCollection'. These settings will be taken into account
		/// when the property CategoryAlterationCollection is requested or GetMultiCategoryAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryAlterationCollection.SortClauses=sortClauses;
			_categoryAlterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiCategoryLanguageCollection(forceFetch, _categoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryLanguageCollection(forceFetch, _categoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryLanguageCollection || forceFetch || _alwaysFetchCategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryLanguageCollection);
				_categoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryLanguageCollection.GetMultiManyToOne(this, null, filter);
				_categoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryLanguageCollection = true;
			}
			return _categoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryLanguageCollection'. These settings will be taken into account
		/// when the property CategoryLanguageCollection is requested or GetMultiCategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryLanguageCollection.SortClauses=sortClauses;
			_categoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch)
		{
			return GetMultiCategorySuggestionCollection(forceFetch, _categorySuggestionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategorySuggestionCollection(forceFetch, _categorySuggestionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategorySuggestionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategorySuggestionCollection || forceFetch || _alwaysFetchCategorySuggestionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categorySuggestionCollection);
				_categorySuggestionCollection.SuppressClearInGetMulti=!forceFetch;
				_categorySuggestionCollection.EntityFactoryToUse = entityFactoryToUse;
				_categorySuggestionCollection.GetMultiManyToOne(this, null, null, filter);
				_categorySuggestionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategorySuggestionCollection = true;
			}
			return _categorySuggestionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategorySuggestionCollection'. These settings will be taken into account
		/// when the property CategorySuggestionCollection is requested or GetMultiCategorySuggestionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategorySuggestionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categorySuggestionCollection.SortClauses=sortClauses;
			_categorySuggestionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch)
		{
			return GetMultiCategoryTagCollection(forceFetch, _categoryTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryTagCollection(forceFetch, _categoryTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryTagCollection GetMultiCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryTagCollection || forceFetch || _alwaysFetchCategoryTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryTagCollection);
				_categoryTagCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryTagCollection.GetMultiManyToOne(this, null, filter);
				_categoryTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryTagCollection = true;
			}
			return _categoryTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryTagCollection'. These settings will be taken into account
		/// when the property CategoryTagCollection is requested or GetMultiCategoryTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryTagCollection.SortClauses=sortClauses;
			_categoryTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch)
		{
			return GetMultiActionMediaCollection(forceFetch, _actionMediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiActionMediaCollection(forceFetch, _actionMediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiActionMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedActionMediaCollection || forceFetch || _alwaysFetchActionMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_actionMediaCollection);
				_actionMediaCollection.SuppressClearInGetMulti=!forceFetch;
				_actionMediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_actionMediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_actionMediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedActionMediaCollection = true;
			}
			return _actionMediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ActionMediaCollection'. These settings will be taken into account
		/// when the property ActionMediaCollection is requested or GetMultiActionMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersActionMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_actionMediaCollection.SortClauses=sortClauses;
			_actionMediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageRecipientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageRecipientCollection || forceFetch || _alwaysFetchMessageRecipientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageRecipientCollection);
				_messageRecipientCollection.SuppressClearInGetMulti=!forceFetch;
				_messageRecipientCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageRecipientCollection.GetMultiManyToOne(this, null, null, null, null, null, null, filter);
				_messageRecipientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageRecipientCollection = true;
			}
			return _messageRecipientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageRecipientCollection'. These settings will be taken into account
		/// when the property MessageRecipientCollection is requested or GetMultiMessageRecipientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageRecipientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageRecipientCollection.SortClauses=sortClauses;
			_messageRecipientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageTemplateCollection || forceFetch || _alwaysFetchMessageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageTemplateCollection);
				_messageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_messageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageTemplateCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_messageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageTemplateCollection = true;
			}
			return _messageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageTemplateCollection'. These settings will be taken into account
		/// when the property MessageTemplateCollection is requested or GetMultiMessageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageTemplateCollection.SortClauses=sortClauses;
			_messageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderHourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderHourEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderHourCollection GetMultiOrderHourCollection(bool forceFetch)
		{
			return GetMultiOrderHourCollection(forceFetch, _orderHourCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderHourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderHourEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderHourCollection GetMultiOrderHourCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderHourCollection(forceFetch, _orderHourCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderHourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderHourCollection GetMultiOrderHourCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderHourCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderHourEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderHourCollection GetMultiOrderHourCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderHourCollection || forceFetch || _alwaysFetchOrderHourCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderHourCollection);
				_orderHourCollection.SuppressClearInGetMulti=!forceFetch;
				_orderHourCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderHourCollection.GetMultiManyToOne(this, null, filter);
				_orderHourCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderHourCollection = true;
			}
			return _orderHourCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderHourCollection'. These settings will be taken into account
		/// when the property OrderHourCollection is requested or GetMultiOrderHourCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderHourCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderHourCollection.SortClauses=sortClauses;
			_orderHourCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch)
		{
			return GetMultiOrderitemCollection(forceFetch, _orderitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemCollection(forceFetch, _orderitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemCollection GetMultiOrderitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemCollection || forceFetch || _alwaysFetchOrderitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemCollection);
				_orderitemCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemCollection.GetMultiManyToOne(this, null, null, null, null, filter);
				_orderitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemCollection = true;
			}
			return _orderitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemCollection'. These settings will be taken into account
		/// when the property OrderitemCollection is requested or GetMultiOrderitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemCollection.SortClauses=sortClauses;
			_orderitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryCollection GetMultiProductCategoryCollection(bool forceFetch)
		{
			return GetMultiProductCategoryCollection(forceFetch, _productCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryCollection GetMultiProductCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCategoryCollection(forceFetch, _productCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryCollection GetMultiProductCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryCollection GetMultiProductCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCategoryCollection || forceFetch || _alwaysFetchProductCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCategoryCollection);
				_productCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_productCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCategoryCollection.GetMultiManyToOne(this, null, filter);
				_productCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCategoryCollection = true;
			}
			return _productCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCategoryCollection'. These settings will be taken into account
		/// when the property ProductCategoryCollection is requested or GetMultiProductCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCategoryCollection.SortClauses=sortClauses;
			_productCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, _productCategoryTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, _productCategoryTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCategoryTagCollection || forceFetch || _alwaysFetchProductCategoryTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCategoryTagCollection);
				_productCategoryTagCollection.SuppressClearInGetMulti=!forceFetch;
				_productCategoryTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCategoryTagCollection.GetMultiManyToOne(this, null, null, filter);
				_productCategoryTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCategoryTagCollection = true;
			}
			return _productCategoryTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCategoryTagCollection'. These settings will be taken into account
		/// when the property ProductCategoryTagCollection is requested or GetMultiProductCategoryTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCategoryTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCategoryTagCollection.SortClauses=sortClauses;
			_productCategoryTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageCollection || forceFetch || _alwaysFetchScheduledMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageCollection);
				_scheduledMessageCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_scheduledMessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageCollection = true;
			}
			return _scheduledMessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageCollection'. These settings will be taken into account
		/// when the property ScheduledMessageCollection is requested or GetMultiScheduledMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageCollection.SortClauses=sortClauses;
			_scheduledMessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UITabEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch)
		{
			return GetMultiUITabCollection(forceFetch, _uITabCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UITabEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUITabCollection(forceFetch, _uITabCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUITabCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUITabCollection || forceFetch || _alwaysFetchUITabCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uITabCollection);
				_uITabCollection.SuppressClearInGetMulti=!forceFetch;
				_uITabCollection.EntityFactoryToUse = entityFactoryToUse;
				_uITabCollection.GetMultiManyToOne(this, null, null, null, null, filter);
				_uITabCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUITabCollection = true;
			}
			return _uITabCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UITabCollection'. These settings will be taken into account
		/// when the property UITabCollection is requested or GetMultiUITabCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUITabCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uITabCollection.SortClauses=sortClauses;
			_uITabCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAdvertisementCollectionViaMedium(forceFetch, _advertisementCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAdvertisementCollectionViaMedium || forceFetch || _alwaysFetchAdvertisementCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollectionViaMedium.GetMulti(filter, GetRelationsForField("AdvertisementCollectionViaMedium"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollectionViaMedium = true;
			}
			return _advertisementCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollectionViaMedium'. These settings will be taken into account
		/// when the property AdvertisementCollectionViaMedium is requested or GetMultiAdvertisementCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollectionViaMedium.SortClauses=sortClauses;
			_advertisementCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationCollectionViaMedium(forceFetch, _alterationCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationCollectionViaMedium || forceFetch || _alwaysFetchAlterationCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationCollectionViaMedium"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollectionViaMedium = true;
			}
			return _alterationCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationCollectionViaMedium is requested or GetMultiAlterationCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollectionViaMedium.SortClauses=sortClauses;
			_alterationCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaCategoryAlteration(bool forceFetch)
		{
			return GetMultiAlterationCollectionViaCategoryAlteration(forceFetch, _alterationCollectionViaCategoryAlteration.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaCategoryAlteration(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationCollectionViaCategoryAlteration || forceFetch || _alwaysFetchAlterationCollectionViaCategoryAlteration) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollectionViaCategoryAlteration);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_alterationCollectionViaCategoryAlteration.SuppressClearInGetMulti=!forceFetch;
				_alterationCollectionViaCategoryAlteration.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollectionViaCategoryAlteration.GetMulti(filter, GetRelationsForField("AlterationCollectionViaCategoryAlteration"));
				_alterationCollectionViaCategoryAlteration.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollectionViaCategoryAlteration = true;
			}
			return _alterationCollectionViaCategoryAlteration;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollectionViaCategoryAlteration'. These settings will be taken into account
		/// when the property AlterationCollectionViaCategoryAlteration is requested or GetMultiAlterationCollectionViaCategoryAlteration is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollectionViaCategoryAlteration(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollectionViaCategoryAlteration.SortClauses=sortClauses;
			_alterationCollectionViaCategoryAlteration.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium_(forceFetch, _alterationoptionCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium_ || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_alterationoptionCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium_.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium_"));
				_alterationoptionCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium_ = true;
			}
			return _alterationoptionCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium_'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium_ is requested or GetMultiAlterationoptionCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium_.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium_(forceFetch, _categoryCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium_ || forceFetch || _alwaysFetchCategoryCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium_.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium_"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium_ = true;
			}
			return _categoryCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium_'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium_ is requested or GetMultiCategoryCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium_.SortClauses=sortClauses;
			_categoryCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaMessage(bool forceFetch)
		{
			return GetMultiClientCollectionViaMessage(forceFetch, _clientCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaMessage || forceFetch || _alwaysFetchClientCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_clientCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaMessage.GetMulti(filter, GetRelationsForField("ClientCollectionViaMessage"));
				_clientCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaMessage = true;
			}
			return _clientCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaMessage'. These settings will be taken into account
		/// when the property ClientCollectionViaMessage is requested or GetMultiClientCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaMessage.SortClauses=sortClauses;
			_clientCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMedium(forceFetch, _companyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMedium || forceFetch || _alwaysFetchCompanyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMedium.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMedium"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMedium = true;
			}
			return _companyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMedium'. These settings will be taken into account
		/// when the property CompanyCollectionViaMedium is requested or GetMultiCompanyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMedium.SortClauses=sortClauses;
			_companyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMessage(forceFetch, _companyCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMessage || forceFetch || _alwaysFetchCompanyCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_companyCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMessage.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMessage"));
				_companyCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMessage = true;
			}
			return _companyCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMessage'. These settings will be taken into account
		/// when the property CompanyCollectionViaMessage is requested or GetMultiCompanyCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMessage.SortClauses=sortClauses;
			_companyCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMessageTemplate(forceFetch, _companyCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMessageTemplate || forceFetch || _alwaysFetchCompanyCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_companyCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMessageTemplate"));
				_companyCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMessageTemplate = true;
			}
			return _companyCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property CompanyCollectionViaMessageTemplate is requested or GetMultiCompanyCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMessageTemplate.SortClauses=sortClauses;
			_companyCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaAdvertisement(forceFetch, _companyCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaAdvertisement || forceFetch || _alwaysFetchCompanyCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_companyCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("CompanyCollectionViaAdvertisement"));
				_companyCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaAdvertisement = true;
			}
			return _companyCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property CompanyCollectionViaAdvertisement is requested or GetMultiCompanyCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaAdvertisement.SortClauses=sortClauses;
			_companyCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaMessage(forceFetch, _customerCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaMessage || forceFetch || _alwaysFetchCustomerCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_customerCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaMessage.GetMulti(filter, GetRelationsForField("CustomerCollectionViaMessage"));
				_customerCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaMessage = true;
			}
			return _customerCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaMessage'. These settings will be taken into account
		/// when the property CustomerCollectionViaMessage is requested or GetMultiCustomerCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaMessage.SortClauses=sortClauses;
			_customerCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaMessage(forceFetch, _deliverypointCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaMessage || forceFetch || _alwaysFetchDeliverypointCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaMessage.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaMessage"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaMessage = true;
			}
			return _deliverypointCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaMessage'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaMessage is requested or GetMultiDeliverypointCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaMessage.SortClauses=sortClauses;
			_deliverypointCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAdvertisement(forceFetch, _deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
			}
			return _deliverypointgroupCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAdvertisement is requested or GetMultiDeliverypointgroupCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAdvertisement.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAnnouncement(forceFetch, _deliverypointgroupCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_deliverypointgroupCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement"));
				_deliverypointgroupCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = true;
			}
			return _deliverypointgroupCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAnnouncement is requested or GetMultiDeliverypointgroupCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAnnouncement.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAnnouncement_(forceFetch, _deliverypointgroupCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_deliverypointgroupCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement_"));
				_deliverypointgroupCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = true;
			}
			return _deliverypointgroupCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAnnouncement_ is requested or GetMultiDeliverypointgroupCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAnnouncement_.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaMedium(forceFetch, _deliverypointgroupCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaMedium || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaMedium.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaMedium"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
			}
			return _deliverypointgroupCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaMedium'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaMedium is requested or GetMultiDeliverypointgroupCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaMedium.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAnnouncement(forceFetch, _entertainmentCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAnnouncement || forceFetch || _alwaysFetchEntertainmentCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAnnouncement"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
			}
			return _entertainmentCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAnnouncement is requested or GetMultiEntertainmentCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAnnouncement.SortClauses=sortClauses;
			_entertainmentCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAnnouncement_(forceFetch, _entertainmentCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAnnouncement_ || forceFetch || _alwaysFetchEntertainmentCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAnnouncement_"));
				_entertainmentCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = true;
			}
			return _entertainmentCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAnnouncement_ is requested or GetMultiEntertainmentCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAnnouncement_.SortClauses=sortClauses;
			_entertainmentCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium_(forceFetch, _entertainmentCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium_ || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium_"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
			}
			return _entertainmentCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium_ is requested or GetMultiEntertainmentCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium_.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium__(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium__(forceFetch, _entertainmentCollectionViaMedium__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium__ || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaMedium__.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium__.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium__.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium__"));
				_entertainmentCollectionViaMedium__.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium__ = true;
			}
			return _entertainmentCollectionViaMedium__;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium__'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium__ is requested or GetMultiEntertainmentCollectionViaMedium__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium__.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessage(forceFetch, _entertainmentCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessage || forceFetch || _alwaysFetchEntertainmentCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessage.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessage"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessage = true;
			}
			return _entertainmentCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessage'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessage is requested or GetMultiEntertainmentCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessage.SortClauses=sortClauses;
			_entertainmentCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessageTemplate(forceFetch, _entertainmentCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessageTemplate || forceFetch || _alwaysFetchEntertainmentCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessageTemplate"));
				_entertainmentCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessageTemplate = true;
			}
			return _entertainmentCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessageTemplate is requested or GetMultiEntertainmentCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessageTemplate.SortClauses=sortClauses;
			_entertainmentCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaUITab(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaUITab(forceFetch, _entertainmentCollectionViaUITab.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaUITab(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaUITab || forceFetch || _alwaysFetchEntertainmentCollectionViaUITab) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaUITab);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaUITab.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaUITab.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaUITab.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaUITab"));
				_entertainmentCollectionViaUITab.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaUITab = true;
			}
			return _entertainmentCollectionViaUITab;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaUITab'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaUITab is requested or GetMultiEntertainmentCollectionViaUITab is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaUITab(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaUITab.SortClauses=sortClauses;
			_entertainmentCollectionViaUITab.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement(forceFetch, _entertainmentCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
			}
			return _entertainmentCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement is requested or GetMultiEntertainmentCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement_(forceFetch, _entertainmentCollectionViaAdvertisement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement_ || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentCollectionViaAdvertisement_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement_"));
				_entertainmentCollectionViaAdvertisement_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = true;
			}
			return _entertainmentCollectionViaAdvertisement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement_ is requested or GetMultiEntertainmentCollectionViaAdvertisement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement_.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAdvertisement(forceFetch, _entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
			}
			return _entertainmentcategoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAdvertisement is requested or GetMultiEntertainmentcategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium_(forceFetch, _entertainmentcategoryCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium_ || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_entertainmentcategoryCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium_.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium_"));
				_entertainmentcategoryCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium_ = true;
			}
			return _entertainmentcategoryCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium_'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium_ is requested or GetMultiEntertainmentcategoryCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium_.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericcategoryCollectionViaMedium(forceFetch, _genericcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollectionViaMedium || forceFetch || _alwaysFetchGenericcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericcategoryCollectionViaMedium"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollectionViaMedium = true;
			}
			return _genericcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericcategoryCollectionViaMedium is requested or GetMultiGenericcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollectionViaMedium.SortClauses=sortClauses;
			_genericcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaMedium(forceFetch, _genericproductCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaMedium || forceFetch || _alwaysFetchGenericproductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaMedium"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaMedium = true;
			}
			return _genericproductCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericproductCollectionViaMedium is requested or GetMultiGenericproductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaMedium.SortClauses=sortClauses;
			_genericproductCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaAdvertisement(forceFetch, _genericproductCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaAdvertisement || forceFetch || _alwaysFetchGenericproductCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_genericproductCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaAdvertisement"));
				_genericproductCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaAdvertisement = true;
			}
			return _genericproductCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property GenericproductCollectionViaAdvertisement is requested or GetMultiGenericproductCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaAdvertisement.SortClauses=sortClauses;
			_genericproductCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiMediaCollectionViaAnnouncement(forceFetch, _mediaCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaAnnouncement || forceFetch || _alwaysFetchMediaCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_mediaCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("MediaCollectionViaAnnouncement"));
				_mediaCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaAnnouncement = true;
			}
			return _mediaCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property MediaCollectionViaAnnouncement is requested or GetMultiMediaCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaAnnouncement.SortClauses=sortClauses;
			_mediaCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiMediaCollectionViaAnnouncement_(forceFetch, _mediaCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaAnnouncement_ || forceFetch || _alwaysFetchMediaCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_mediaCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("MediaCollectionViaAnnouncement_"));
				_mediaCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaAnnouncement_ = true;
			}
			return _mediaCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property MediaCollectionViaAnnouncement_ is requested or GetMultiMediaCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaAnnouncement_.SortClauses=sortClauses;
			_mediaCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch)
		{
			return GetMultiMediaCollectionViaMessage(forceFetch, _mediaCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaMessage || forceFetch || _alwaysFetchMediaCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaMessage.GetMulti(filter, GetRelationsForField("MediaCollectionViaMessage"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaMessage = true;
			}
			return _mediaCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaMessage'. These settings will be taken into account
		/// when the property MediaCollectionViaMessage is requested or GetMultiMediaCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaMessage.SortClauses=sortClauses;
			_mediaCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiMediaCollectionViaMessageTemplate(forceFetch, _mediaCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaMessageTemplate || forceFetch || _alwaysFetchMediaCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_mediaCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("MediaCollectionViaMessageTemplate"));
				_mediaCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaMessageTemplate = true;
			}
			return _mediaCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property MediaCollectionViaMessageTemplate is requested or GetMultiMediaCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaMessageTemplate.SortClauses=sortClauses;
			_mediaCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch)
		{
			return GetMultiMenuCollectionViaCategory(forceFetch, _menuCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaCategory || forceFetch || _alwaysFetchMenuCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaCategory.GetMulti(filter, GetRelationsForField("MenuCollectionViaCategory"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaCategory = true;
			}
			return _menuCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaCategory'. These settings will be taken into account
		/// when the property MenuCollectionViaCategory is requested or GetMultiMenuCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaCategory.SortClauses=sortClauses;
			_menuCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaMessage(bool forceFetch)
		{
			return GetMultiOrderCollectionViaMessage(forceFetch, _orderCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaMessage || forceFetch || _alwaysFetchOrderCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_orderCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaMessage.GetMulti(filter, GetRelationsForField("OrderCollectionViaMessage"));
				_orderCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaMessage = true;
			}
			return _orderCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaMessage'. These settings will be taken into account
		/// when the property OrderCollectionViaMessage is requested or GetMultiOrderCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaMessage.SortClauses=sortClauses;
			_orderCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderitem(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrderitem(forceFetch, _orderCollectionViaOrderitem.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderitem(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrderitem || forceFetch || _alwaysFetchOrderCollectionViaOrderitem) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrderitem);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_orderCollectionViaOrderitem.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrderitem.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrderitem.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrderitem"));
				_orderCollectionViaOrderitem.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrderitem = true;
			}
			return _orderCollectionViaOrderitem;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrderitem'. These settings will be taken into account
		/// when the property OrderCollectionViaOrderitem is requested or GetMultiOrderCollectionViaOrderitem is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrderitem(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrderitem.SortClauses=sortClauses;
			_orderCollectionViaOrderitem.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium_(forceFetch, _pointOfInterestCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium_ || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_pointOfInterestCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium_.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium_"));
				_pointOfInterestCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium_ = true;
			}
			return _pointOfInterestCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium_'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium_ is requested or GetMultiPointOfInterestCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium_.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiProductCollectionViaAnnouncement(forceFetch, _productCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAnnouncement || forceFetch || _alwaysFetchProductCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("ProductCollectionViaAnnouncement"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAnnouncement = true;
			}
			return _productCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property ProductCollectionViaAnnouncement is requested or GetMultiProductCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAnnouncement.SortClauses=sortClauses;
			_productCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiProductCollectionViaAnnouncement_(forceFetch, _productCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAnnouncement_ || forceFetch || _alwaysFetchProductCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("ProductCollectionViaAnnouncement_"));
				_productCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAnnouncement_ = true;
			}
			return _productCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property ProductCollectionViaAnnouncement_ is requested or GetMultiProductCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAnnouncement_.SortClauses=sortClauses;
			_productCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaCategorySuggestion(bool forceFetch)
		{
			return GetMultiProductCollectionViaCategorySuggestion(forceFetch, _productCollectionViaCategorySuggestion.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaCategorySuggestion(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaCategorySuggestion || forceFetch || _alwaysFetchProductCollectionViaCategorySuggestion) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaCategorySuggestion);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaCategorySuggestion.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaCategorySuggestion.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaCategorySuggestion.GetMulti(filter, GetRelationsForField("ProductCollectionViaCategorySuggestion"));
				_productCollectionViaCategorySuggestion.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaCategorySuggestion = true;
			}
			return _productCollectionViaCategorySuggestion;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaCategorySuggestion'. These settings will be taken into account
		/// when the property ProductCollectionViaCategorySuggestion is requested or GetMultiProductCollectionViaCategorySuggestion is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaCategorySuggestion(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaCategorySuggestion.SortClauses=sortClauses;
			_productCollectionViaCategorySuggestion.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium_(forceFetch, _productCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium_ || forceFetch || _alwaysFetchProductCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium_.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium_"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium_ = true;
			}
			return _productCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium_'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium_ is requested or GetMultiProductCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium_.SortClauses=sortClauses;
			_productCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium__(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium__(forceFetch, _productCollectionViaMedium__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium__ || forceFetch || _alwaysFetchProductCollectionViaMedium__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaMedium__.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium__.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium__.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium__"));
				_productCollectionViaMedium__.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium__ = true;
			}
			return _productCollectionViaMedium__;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium__'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium__ is requested or GetMultiProductCollectionViaMedium__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium__.SortClauses=sortClauses;
			_productCollectionViaMedium__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMessageTemplate(bool forceFetch)
		{
			return GetMultiProductCollectionViaMessageTemplate(forceFetch, _productCollectionViaMessageTemplate.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMessageTemplate(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMessageTemplate || forceFetch || _alwaysFetchProductCollectionViaMessageTemplate) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMessageTemplate);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaMessageTemplate.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMessageTemplate.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMessageTemplate.GetMulti(filter, GetRelationsForField("ProductCollectionViaMessageTemplate"));
				_productCollectionViaMessageTemplate.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMessageTemplate = true;
			}
			return _productCollectionViaMessageTemplate;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMessageTemplate'. These settings will be taken into account
		/// when the property ProductCollectionViaMessageTemplate is requested or GetMultiProductCollectionViaMessageTemplate is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMessageTemplate(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMessageTemplate.SortClauses=sortClauses;
			_productCollectionViaMessageTemplate.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaOrderitem(bool forceFetch)
		{
			return GetMultiProductCollectionViaOrderitem(forceFetch, _productCollectionViaOrderitem.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaOrderitem(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaOrderitem || forceFetch || _alwaysFetchProductCollectionViaOrderitem) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaOrderitem);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaOrderitem.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaOrderitem.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaOrderitem.GetMulti(filter, GetRelationsForField("ProductCollectionViaOrderitem"));
				_productCollectionViaOrderitem.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaOrderitem = true;
			}
			return _productCollectionViaOrderitem;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaOrderitem'. These settings will be taken into account
		/// when the property ProductCollectionViaOrderitem is requested or GetMultiProductCollectionViaOrderitem is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaOrderitem(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaOrderitem.SortClauses=sortClauses;
			_productCollectionViaOrderitem.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaProductCategory(bool forceFetch)
		{
			return GetMultiProductCollectionViaProductCategory(forceFetch, _productCollectionViaProductCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaProductCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaProductCategory || forceFetch || _alwaysFetchProductCollectionViaProductCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaProductCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaProductCategory.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaProductCategory.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaProductCategory.GetMulti(filter, GetRelationsForField("ProductCollectionViaProductCategory"));
				_productCollectionViaProductCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaProductCategory = true;
			}
			return _productCollectionViaProductCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaProductCategory'. These settings will be taken into account
		/// when the property ProductCollectionViaProductCategory is requested or GetMultiProductCollectionViaProductCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaProductCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaProductCategory.SortClauses=sortClauses;
			_productCollectionViaProductCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiProductCollectionViaAdvertisement(forceFetch, _productCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAdvertisement || forceFetch || _alwaysFetchProductCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_productCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("ProductCollectionViaAdvertisement"));
				_productCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAdvertisement = true;
			}
			return _productCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property ProductCollectionViaAdvertisement is requested or GetMultiProductCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAdvertisement.SortClauses=sortClauses;
			_productCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCategory(forceFetch, _routeCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCategory || forceFetch || _alwaysFetchRouteCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCategory.GetMulti(filter, GetRelationsForField("RouteCollectionViaCategory"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCategory = true;
			}
			return _routeCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCategory'. These settings will be taken into account
		/// when the property RouteCollectionViaCategory is requested or GetMultiRouteCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCategory.SortClauses=sortClauses;
			_routeCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupplierEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupplierCollection GetMultiSupplierCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiSupplierCollectionViaAdvertisement(forceFetch, _supplierCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupplierCollection GetMultiSupplierCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupplierCollectionViaAdvertisement || forceFetch || _alwaysFetchSupplierCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supplierCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_supplierCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_supplierCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_supplierCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("SupplierCollectionViaAdvertisement"));
				_supplierCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedSupplierCollectionViaAdvertisement = true;
			}
			return _supplierCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupplierCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property SupplierCollectionViaAdvertisement is requested or GetMultiSupplierCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupplierCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supplierCollectionViaAdvertisement.SortClauses=sortClauses;
			_supplierCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyCollectionViaMedium(forceFetch, _surveyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyCollectionViaMedium || forceFetch || _alwaysFetchSurveyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyCollectionViaMedium"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyCollectionViaMedium = true;
			}
			return _surveyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyCollectionViaMedium is requested or GetMultiSurveyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyCollectionViaMedium.SortClauses=sortClauses;
			_surveyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium_(forceFetch, _surveyPageCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium_ || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_surveyPageCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium_.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium_"));
				_surveyPageCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium_ = true;
			}
			return _surveyPageCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium_'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium_ is requested or GetMultiSurveyPageCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium_.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaUITab(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaUITab(forceFetch, _uIModeCollectionViaUITab.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaUITab(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaUITab || forceFetch || _alwaysFetchUIModeCollectionViaUITab) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaUITab);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, this.CategoryId, "CategoryEntity__"));
				_uIModeCollectionViaUITab.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaUITab.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaUITab.GetMulti(filter, GetRelationsForField("UIModeCollectionViaUITab"));
				_uIModeCollectionViaUITab.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaUITab = true;
			}
			return _uIModeCollectionViaUITab;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaUITab'. These settings will be taken into account
		/// when the property UIModeCollectionViaUITab is requested or GetMultiUIModeCollectionViaUITab is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaUITab(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaUITab.SortClauses=sortClauses;
			_uIModeCollectionViaUITab.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleParentCategoryEntity()
		{
			return GetSingleParentCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleParentCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentCategoryEntity || forceFetch || _alwaysFetchParentCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryIdParentCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentCategoryEntity = newEntity;
				_alreadyFetchedParentCategoryEntity = fetchResult;
			}
			return _parentCategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public GenericcategoryEntity GetSingleGenericcategoryEntity()
		{
			return GetSingleGenericcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public virtual GenericcategoryEntity GetSingleGenericcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericcategoryEntity || forceFetch || _alwaysFetchGenericcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericcategoryEntityUsingGenericcategoryId);
				GenericcategoryEntity newEntity = new GenericcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericcategoryEntity = newEntity;
				_alreadyFetchedGenericcategoryEntity = fetchResult;
			}
			return _genericcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public MenuEntity GetSingleMenuEntity()
		{
			return GetSingleMenuEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public virtual MenuEntity GetSingleMenuEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMenuEntity || forceFetch || _alwaysFetchMenuEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MenuEntityUsingMenuId);
				MenuEntity newEntity = new MenuEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MenuId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MenuEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_menuEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MenuEntity = newEntity;
				_alreadyFetchedMenuEntity = fetchResult;
			}
			return _menuEntity;
		}


		/// <summary> Retrieves the related entity of type 'PoscategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PoscategoryEntity' which is related to this entity.</returns>
		public PoscategoryEntity GetSinglePoscategoryEntity()
		{
			return GetSinglePoscategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PoscategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PoscategoryEntity' which is related to this entity.</returns>
		public virtual PoscategoryEntity GetSinglePoscategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPoscategoryEntity || forceFetch || _alwaysFetchPoscategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PoscategoryEntityUsingPoscategoryId);
				PoscategoryEntity newEntity = new PoscategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PoscategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PoscategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_poscategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PoscategoryEntity = newEntity;
				_alreadyFetchedPoscategoryEntity = fetchResult;
			}
			return _poscategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleRouteEntity()
		{
			return GetSingleRouteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleRouteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRouteEntity || forceFetch || _alwaysFetchRouteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RouteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RouteEntity = newEntity;
				_alreadyFetchedRouteEntity = fetchResult;
			}
			return _routeEntity;
		}


		/// <summary> Retrieves the related entity of type 'ScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ScheduleEntity' which is related to this entity.</returns>
		public ScheduleEntity GetSingleScheduleEntity()
		{
			return GetSingleScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ScheduleEntity' which is related to this entity.</returns>
		public virtual ScheduleEntity GetSingleScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedScheduleEntity || forceFetch || _alwaysFetchScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ScheduleEntityUsingScheduleId);
				ScheduleEntity newEntity = new ScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_scheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ScheduleEntity = newEntity;
				_alreadyFetchedScheduleEntity = fetchResult;
			}
			return _scheduleEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParentCategoryEntity", _parentCategoryEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("GenericcategoryEntity", _genericcategoryEntity);
			toReturn.Add("MenuEntity", _menuEntity);
			toReturn.Add("PoscategoryEntity", _poscategoryEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("RouteEntity", _routeEntity);
			toReturn.Add("ScheduleEntity", _scheduleEntity);
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("AdvertisementTagCategoryCollection", _advertisementTagCategoryCollection);
			toReturn.Add("AnnouncementCollection_", _announcementCollection_);
			toReturn.Add("AnnouncementCollection", _announcementCollection);
			toReturn.Add("ActionCollection", _actionCollection);
			toReturn.Add("AvailabilityCollection", _availabilityCollection);
			toReturn.Add("ChildCategoryCollection", _childCategoryCollection);
			toReturn.Add("CategoryAlterationCollection", _categoryAlterationCollection);
			toReturn.Add("CategoryLanguageCollection", _categoryLanguageCollection);
			toReturn.Add("CategorySuggestionCollection", _categorySuggestionCollection);
			toReturn.Add("CategoryTagCollection", _categoryTagCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("ActionMediaCollection", _actionMediaCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessageRecipientCollection", _messageRecipientCollection);
			toReturn.Add("MessageTemplateCollection", _messageTemplateCollection);
			toReturn.Add("OrderHourCollection", _orderHourCollection);
			toReturn.Add("OrderitemCollection", _orderitemCollection);
			toReturn.Add("ProductCategoryCollection", _productCategoryCollection);
			toReturn.Add("ProductCategoryTagCollection", _productCategoryTagCollection);
			toReturn.Add("ScheduledMessageCollection", _scheduledMessageCollection);
			toReturn.Add("UITabCollection", _uITabCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			toReturn.Add("AdvertisementCollectionViaMedium", _advertisementCollectionViaMedium);
			toReturn.Add("AlterationCollectionViaMedium", _alterationCollectionViaMedium);
			toReturn.Add("AlterationCollectionViaCategoryAlteration", _alterationCollectionViaCategoryAlteration);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("AlterationoptionCollectionViaMedium_", _alterationoptionCollectionViaMedium_);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium_", _categoryCollectionViaMedium_);
			toReturn.Add("ClientCollectionViaMessage", _clientCollectionViaMessage);
			toReturn.Add("CompanyCollectionViaMedium", _companyCollectionViaMedium);
			toReturn.Add("CompanyCollectionViaMessage", _companyCollectionViaMessage);
			toReturn.Add("CompanyCollectionViaMessageTemplate", _companyCollectionViaMessageTemplate);
			toReturn.Add("CompanyCollectionViaAdvertisement", _companyCollectionViaAdvertisement);
			toReturn.Add("CustomerCollectionViaMessage", _customerCollectionViaMessage);
			toReturn.Add("DeliverypointCollectionViaMessage", _deliverypointCollectionViaMessage);
			toReturn.Add("DeliverypointgroupCollectionViaAdvertisement", _deliverypointgroupCollectionViaAdvertisement);
			toReturn.Add("DeliverypointgroupCollectionViaAnnouncement", _deliverypointgroupCollectionViaAnnouncement);
			toReturn.Add("DeliverypointgroupCollectionViaAnnouncement_", _deliverypointgroupCollectionViaAnnouncement_);
			toReturn.Add("DeliverypointgroupCollectionViaMedium", _deliverypointgroupCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaAnnouncement", _entertainmentCollectionViaAnnouncement);
			toReturn.Add("EntertainmentCollectionViaAnnouncement_", _entertainmentCollectionViaAnnouncement_);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium_", _entertainmentCollectionViaMedium_);
			toReturn.Add("EntertainmentCollectionViaMedium__", _entertainmentCollectionViaMedium__);
			toReturn.Add("EntertainmentCollectionViaMessage", _entertainmentCollectionViaMessage);
			toReturn.Add("EntertainmentCollectionViaMessageTemplate", _entertainmentCollectionViaMessageTemplate);
			toReturn.Add("EntertainmentCollectionViaUITab", _entertainmentCollectionViaUITab);
			toReturn.Add("EntertainmentCollectionViaAdvertisement", _entertainmentCollectionViaAdvertisement);
			toReturn.Add("EntertainmentCollectionViaAdvertisement_", _entertainmentCollectionViaAdvertisement_);
			toReturn.Add("EntertainmentcategoryCollectionViaAdvertisement", _entertainmentcategoryCollectionViaAdvertisement);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium_", _entertainmentcategoryCollectionViaMedium_);
			toReturn.Add("GenericcategoryCollectionViaMedium", _genericcategoryCollectionViaMedium);
			toReturn.Add("GenericproductCollectionViaMedium", _genericproductCollectionViaMedium);
			toReturn.Add("GenericproductCollectionViaAdvertisement", _genericproductCollectionViaAdvertisement);
			toReturn.Add("MediaCollectionViaAnnouncement", _mediaCollectionViaAnnouncement);
			toReturn.Add("MediaCollectionViaAnnouncement_", _mediaCollectionViaAnnouncement_);
			toReturn.Add("MediaCollectionViaMessage", _mediaCollectionViaMessage);
			toReturn.Add("MediaCollectionViaMessageTemplate", _mediaCollectionViaMessageTemplate);
			toReturn.Add("MenuCollectionViaCategory", _menuCollectionViaCategory);
			toReturn.Add("OrderCollectionViaMessage", _orderCollectionViaMessage);
			toReturn.Add("OrderCollectionViaOrderitem", _orderCollectionViaOrderitem);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("PointOfInterestCollectionViaMedium_", _pointOfInterestCollectionViaMedium_);
			toReturn.Add("ProductCollectionViaAnnouncement", _productCollectionViaAnnouncement);
			toReturn.Add("ProductCollectionViaAnnouncement_", _productCollectionViaAnnouncement_);
			toReturn.Add("ProductCollectionViaCategorySuggestion", _productCollectionViaCategorySuggestion);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium_", _productCollectionViaMedium_);
			toReturn.Add("ProductCollectionViaMedium__", _productCollectionViaMedium__);
			toReturn.Add("ProductCollectionViaMessageTemplate", _productCollectionViaMessageTemplate);
			toReturn.Add("ProductCollectionViaOrderitem", _productCollectionViaOrderitem);
			toReturn.Add("ProductCollectionViaProductCategory", _productCollectionViaProductCategory);
			toReturn.Add("ProductCollectionViaAdvertisement", _productCollectionViaAdvertisement);
			toReturn.Add("RouteCollectionViaCategory", _routeCollectionViaCategory);
			toReturn.Add("SupplierCollectionViaAdvertisement", _supplierCollectionViaAdvertisement);
			toReturn.Add("SurveyCollectionViaMedium", _surveyCollectionViaMedium);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			toReturn.Add("SurveyPageCollectionViaMedium_", _surveyPageCollectionViaMedium_);
			toReturn.Add("UIModeCollectionViaUITab", _uIModeCollectionViaUITab);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="validator">The validator object for this CategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 categoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(categoryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "ActionCategoryEntity");

			_advertisementTagCategoryCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection();
			_advertisementTagCategoryCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_announcementCollection_ = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollection_.SetContainingEntityInfo(this, "CategoryEntity");

			_announcementCollection = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollection.SetContainingEntityInfo(this, "CategoryEntity_");

			_actionCollection = new Obymobi.Data.CollectionClasses.ActionCollection();
			_actionCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_availabilityCollection = new Obymobi.Data.CollectionClasses.AvailabilityCollection();
			_availabilityCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_childCategoryCollection = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_childCategoryCollection.SetContainingEntityInfo(this, "ParentCategoryEntity");

			_categoryAlterationCollection = new Obymobi.Data.CollectionClasses.CategoryAlterationCollection();
			_categoryAlterationCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_categoryLanguageCollection = new Obymobi.Data.CollectionClasses.CategoryLanguageCollection();
			_categoryLanguageCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_categorySuggestionCollection = new Obymobi.Data.CollectionClasses.CategorySuggestionCollection();
			_categorySuggestionCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_categoryTagCollection = new Obymobi.Data.CollectionClasses.CategoryTagCollection();
			_categoryTagCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_actionMediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_actionMediaCollection.SetContainingEntityInfo(this, "ActionCategoryEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_messageRecipientCollection = new Obymobi.Data.CollectionClasses.MessageRecipientCollection();
			_messageRecipientCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_messageTemplateCollection = new Obymobi.Data.CollectionClasses.MessageTemplateCollection();
			_messageTemplateCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_orderHourCollection = new Obymobi.Data.CollectionClasses.OrderHourCollection();
			_orderHourCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_orderitemCollection = new Obymobi.Data.CollectionClasses.OrderitemCollection();
			_orderitemCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_productCategoryCollection = new Obymobi.Data.CollectionClasses.ProductCategoryCollection();
			_productCategoryCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_productCategoryTagCollection = new Obymobi.Data.CollectionClasses.ProductCategoryTagCollection();
			_productCategoryTagCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_scheduledMessageCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageCollection();
			_scheduledMessageCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_uITabCollection = new Obymobi.Data.CollectionClasses.UITabCollection();
			_uITabCollection.SetContainingEntityInfo(this, "CategoryEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "CategoryEntity");
			_advertisementCollectionViaMedium = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_alterationCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationCollectionViaCategoryAlteration = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_alterationoptionCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_clientCollectionViaMessage = new Obymobi.Data.CollectionClasses.ClientCollection();
			_companyCollectionViaMedium = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaMessage = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_customerCollectionViaMessage = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_deliverypointCollectionViaMessage = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaMedium = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium__ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMessage = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaUITab = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAdvertisement_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_entertainmentcategoryCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_genericcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericproductCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_genericproductCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_mediaCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollectionViaMessage = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.MediaCollection();
			_menuCollectionViaCategory = new Obymobi.Data.CollectionClasses.MenuCollection();
			_orderCollectionViaMessage = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollectionViaOrderitem = new Obymobi.Data.CollectionClasses.OrderCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_pointOfInterestCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaCategorySuggestion = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium__ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMessageTemplate = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaOrderitem = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaProductCategory = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.ProductCollection();
			_routeCollectionViaCategory = new Obymobi.Data.CollectionClasses.RouteCollection();
			_supplierCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.SupplierCollection();
			_surveyCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_surveyPageCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_uIModeCollectionViaUITab = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_parentCategoryEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_genericcategoryEntityReturnsNewIfNotFound = true;
			_menuEntityReturnsNewIfNotFound = true;
			_poscategoryEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_routeEntityReturnsNewIfNotFound = true;
			_scheduleEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PoscategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailableOnOtoucho", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailableOnObymobi", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rateable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HidePrices", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnnouncementAction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Color", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Geofencing", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowFreeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViewLayoutType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliveryLocationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupportNotificationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisibilityType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomizeButtonText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViewType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuItemsMustBeLinkedToExternalProduct", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CoversType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _parentCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentCategoryEntity, new PropertyChangedEventHandler( OnParentCategoryEntityPropertyChanged ), "ParentCategoryEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.CategoryEntityUsingCategoryIdParentCategoryIdStatic, true, signalRelatedEntity, "ChildCategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.ParentCategoryId } );		
			_parentCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _parentCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentCategoryEntity(IEntityCore relatedEntity)
		{
			if(_parentCategoryEntity!=relatedEntity)
			{		
				DesetupSyncParentCategoryEntity(true, true);
				_parentCategoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentCategoryEntity, new PropertyChangedEventHandler( OnParentCategoryEntityPropertyChanged ), "ParentCategoryEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.CategoryEntityUsingCategoryIdParentCategoryIdStatic, true, ref _alreadyFetchedParentCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "CategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, signalRelatedEntity, "CategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.GenericcategoryId } );		
			_genericcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericcategoryEntity(IEntityCore relatedEntity)
		{
			if(_genericcategoryEntity!=relatedEntity)
			{		
				DesetupSyncGenericcategoryEntity(true, true);
				_genericcategoryEntity = (GenericcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.GenericcategoryEntityUsingGenericcategoryIdStatic, true, ref _alreadyFetchedGenericcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _menuEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMenuEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.MenuEntityUsingMenuIdStatic, true, signalRelatedEntity, "CategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.MenuId } );		
			_menuEntity = null;
		}
		
		/// <summary> setups the sync logic for member _menuEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMenuEntity(IEntityCore relatedEntity)
		{
			if(_menuEntity!=relatedEntity)
			{		
				DesetupSyncMenuEntity(true, true);
				_menuEntity = (MenuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.MenuEntityUsingMenuIdStatic, true, ref _alreadyFetchedMenuEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMenuEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _poscategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPoscategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _poscategoryEntity, new PropertyChangedEventHandler( OnPoscategoryEntityPropertyChanged ), "PoscategoryEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.PoscategoryEntityUsingPoscategoryIdStatic, true, signalRelatedEntity, "CategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.PoscategoryId } );		
			_poscategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _poscategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPoscategoryEntity(IEntityCore relatedEntity)
		{
			if(_poscategoryEntity!=relatedEntity)
			{		
				DesetupSyncPoscategoryEntity(true, true);
				_poscategoryEntity = (PoscategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _poscategoryEntity, new PropertyChangedEventHandler( OnPoscategoryEntityPropertyChanged ), "PoscategoryEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.PoscategoryEntityUsingPoscategoryIdStatic, true, ref _alreadyFetchedPoscategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPoscategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "CategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRouteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.RouteEntityUsingRouteIdStatic, true, signalRelatedEntity, "CategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.RouteId } );		
			_routeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _routeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRouteEntity(IEntityCore relatedEntity)
		{
			if(_routeEntity!=relatedEntity)
			{		
				DesetupSyncRouteEntity(true, true);
				_routeEntity = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.RouteEntityUsingRouteIdStatic, true, ref _alreadyFetchedRouteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRouteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _scheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _scheduleEntity, new PropertyChangedEventHandler( OnScheduleEntityPropertyChanged ), "ScheduleEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.ScheduleEntityUsingScheduleIdStatic, true, signalRelatedEntity, "CategoryCollection", resetFKFields, new int[] { (int)CategoryFieldIndex.ScheduleId } );		
			_scheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _scheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncScheduleEntity(IEntityCore relatedEntity)
		{
			if(_scheduleEntity!=relatedEntity)
			{		
				DesetupSyncScheduleEntity(true, true);
				_scheduleEntity = (ScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _scheduleEntity, new PropertyChangedEventHandler( OnScheduleEntityPropertyChanged ), "ScheduleEntity", Obymobi.Data.RelationClasses.StaticCategoryRelations.ScheduleEntityUsingScheduleIdStatic, true, ref _alreadyFetchedScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 categoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CategoryFieldIndex.CategoryId].ForcedCurrentValueWrite(categoryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CategoryRelations Relations
		{
			get	{ return new CategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagCategoryCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AdvertisementTagCategoryEntity, 0, null, null, null, "AdvertisementTagCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementCollection_")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Action' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionCollection(), (IEntityRelation)GetRelationsForField("ActionCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ActionEntity, 0, null, null, null, "ActionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Availability' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAvailabilityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AvailabilityCollection(), (IEntityRelation)GetRelationsForField("AvailabilityCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AvailabilityEntity, 0, null, null, null, "AvailabilityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChildCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("ChildCategoryCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "ChildCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategoryAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryAlterationCollection(), (IEntityRelation)GetRelationsForField("CategoryAlterationCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategoryAlterationEntity, 0, null, null, null, "CategoryAlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("CategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategoryLanguageEntity, 0, null, null, null, "CategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategorySuggestion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategorySuggestionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategorySuggestionCollection(), (IEntityRelation)GetRelationsForField("CategorySuggestionCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategorySuggestionEntity, 0, null, null, null, "CategorySuggestionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryTagCollection(), (IEntityRelation)GetRelationsForField("CategoryTagCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategoryTagEntity, 0, null, null, null, "CategoryTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("ActionMediaCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "ActionMediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageRecipient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageRecipientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageRecipientCollection(), (IEntityRelation)GetRelationsForField("MessageRecipientCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MessageRecipientEntity, 0, null, null, null, "MessageRecipientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageTemplateCollection(), (IEntityRelation)GetRelationsForField("MessageTemplateCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MessageTemplateEntity, 0, null, null, null, "MessageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderHour' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderHourCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderHourCollection(), (IEntityRelation)GetRelationsForField("OrderHourCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.OrderHourEntity, 0, null, null, null, "OrderHourCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Orderitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.OrderitemEntity, 0, null, null, null, "OrderitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductCategoryEntity, 0, null, null, null, "ProductCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryTagCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryTagCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductCategoryTagEntity, 0, null, null, null, "ProductCategoryTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITab' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabCollection(), (IEntityRelation)GetRelationsForField("UITabCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.UITabEntity, 0, null, null, null, "UITabCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, GetRelationsForField("AdvertisementCollectionViaMedium"), "AdvertisementCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, GetRelationsForField("AlterationCollectionViaMedium"), "AlterationCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollectionViaCategoryAlteration
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryAlterationEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "CategoryAlteration_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, GetRelationsForField("AlterationCollectionViaCategoryAlteration"), "AlterationCollectionViaCategoryAlteration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium_"), "AlterationoptionCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium_"), "CategoryCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaMessage"), "ClientCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMedium"), "CompanyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMessage"), "CompanyCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMessageTemplate"), "CompanyCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaAdvertisement"), "CompanyCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaMessage"), "CustomerCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaMessage"), "DeliverypointCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"), "DeliverypointgroupCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement"), "DeliverypointgroupCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement_"), "DeliverypointgroupCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaMedium"), "DeliverypointgroupCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAnnouncement"), "EntertainmentCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAnnouncement_"), "EntertainmentCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium_"), "EntertainmentCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium__"), "EntertainmentCollectionViaMedium__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessage"), "EntertainmentCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessageTemplate"), "EntertainmentCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaUITab
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UITabEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "UITab_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaUITab"), "EntertainmentCollectionViaUITab", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement"), "EntertainmentCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement_"), "EntertainmentCollectionViaAdvertisement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"), "EntertainmentcategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium_"), "EntertainmentcategoryCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, GetRelationsForField("GenericcategoryCollectionViaMedium"), "GenericcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaMedium"), "GenericproductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaAdvertisement"), "GenericproductCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaAnnouncement"), "MediaCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaAnnouncement_"), "MediaCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaMessage"), "MediaCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaMessageTemplate"), "MediaCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingParentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaCategory"), "MenuCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaMessage"), "OrderCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrderitem
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderitemEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Orderitem_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrderitem"), "OrderCollectionViaOrderitem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium_"), "PointOfInterestCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAnnouncement"), "ProductCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAnnouncement_"), "ProductCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaCategorySuggestion
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategorySuggestionEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "CategorySuggestion_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaCategorySuggestion"), "ProductCollectionViaCategorySuggestion", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium_"), "ProductCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium__"), "ProductCollectionViaMedium__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMessageTemplate
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageTemplateEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "MessageTemplate_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMessageTemplate"), "ProductCollectionViaMessageTemplate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaOrderitem
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderitemEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Orderitem_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaOrderitem"), "ProductCollectionViaOrderitem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaProductCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductCategoryEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "ProductCategory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaProductCategory"), "ProductCollectionViaProductCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAdvertisement"), "ProductCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingParentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCategory"), "RouteCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supplier'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupplierCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupplierCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.SupplierEntity, 0, null, null, GetRelationsForField("SupplierCollectionViaAdvertisement"), "SupplierCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.SurveyEntity, 0, null, null, GetRelationsForField("SurveyCollectionViaMedium"), "SurveyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium_"), "SurveyPageCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaUITab
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UITabEntityUsingCategoryId;
				intermediateRelation.SetAliases(string.Empty, "UITab_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaUITab"), "UIModeCollectionViaUITab", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("ParentCategoryEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "ParentCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, null, "GenericcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), (IEntityRelation)GetRelationsForField("MenuEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, null, "MenuEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Poscategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPoscategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PoscategoryCollection(), (IEntityRelation)GetRelationsForField("PoscategoryEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.PoscategoryEntity, 0, null, null, null, "PoscategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RouteEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "RouteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Schedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduleCollection(), (IEntityRelation)GetRelationsForField("ScheduleEntity")[0], (int)Obymobi.Data.EntityType.CategoryEntity, (int)Obymobi.Data.EntityType.ScheduleEntity, 0, null, null, null, "ScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CategoryId
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.CategoryId, true); }
			set	{ SetValue((int)CategoryFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.CompanyId, true); }
			set	{ SetValue((int)CategoryFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The ParentCategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ParentCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.ParentCategoryId, false); }
			set	{ SetValue((int)CategoryFieldIndex.ParentCategoryId, value, true); }
		}

		/// <summary> The GenericcategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."GenericcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.GenericcategoryId, false); }
			set	{ SetValue((int)CategoryFieldIndex.GenericcategoryId, value, true); }
		}

		/// <summary> The Name property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.Name, true); }
			set	{ SetValue((int)CategoryFieldIndex.Name, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.SortOrder, true); }
			set	{ SetValue((int)CategoryFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The PoscategoryId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."PoscategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PoscategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.PoscategoryId, false); }
			set	{ SetValue((int)CategoryFieldIndex.PoscategoryId, value, true); }
		}

		/// <summary> The ProductId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.ProductId, false); }
			set	{ SetValue((int)CategoryFieldIndex.ProductId, value, true); }
		}

		/// <summary> The AvailableOnOtoucho property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AvailableOnOtoucho"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnOtoucho
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.AvailableOnOtoucho, true); }
			set	{ SetValue((int)CategoryFieldIndex.AvailableOnOtoucho, value, true); }
		}

		/// <summary> The AvailableOnObymobi property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AvailableOnObymobi"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)CategoryFieldIndex.AvailableOnObymobi, value, true); }
		}

		/// <summary> The Rateable property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Rateable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Rateable
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CategoryFieldIndex.Rateable, false); }
			set	{ SetValue((int)CategoryFieldIndex.Rateable, value, true); }
		}

		/// <summary> The Visible property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.Visible, true); }
			set	{ SetValue((int)CategoryFieldIndex.Visible, value, true); }
		}

		/// <summary> The Type property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.Type, true); }
			set	{ SetValue((int)CategoryFieldIndex.Type, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CategoryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CategoryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The RouteId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."RouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.RouteId, false); }
			set	{ SetValue((int)CategoryFieldIndex.RouteId, value, true); }
		}

		/// <summary> The MenuId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."MenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.MenuId, false); }
			set	{ SetValue((int)CategoryFieldIndex.MenuId, value, true); }
		}

		/// <summary> The HidePrices property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."HidePrices"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrices
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.HidePrices, true); }
			set	{ SetValue((int)CategoryFieldIndex.HidePrices, value, true); }
		}

		/// <summary> The AnnouncementAction property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AnnouncementAction"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnnouncementAction
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.AnnouncementAction, true); }
			set	{ SetValue((int)CategoryFieldIndex.AnnouncementAction, value, true); }
		}

		/// <summary> The Color property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Color"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Color
		{
			get { return (System.Int32)GetValue((int)CategoryFieldIndex.Color, true); }
			set	{ SetValue((int)CategoryFieldIndex.Color, value, true); }
		}

		/// <summary> The Description property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.Description, true); }
			set	{ SetValue((int)CategoryFieldIndex.Description, value, true); }
		}

		/// <summary> The Geofencing property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."Geofencing"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Geofencing
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CategoryFieldIndex.Geofencing, false); }
			set	{ SetValue((int)CategoryFieldIndex.Geofencing, value, true); }
		}

		/// <summary> The AllowFreeText property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."AllowFreeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AllowFreeText
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CategoryFieldIndex.AllowFreeText, false); }
			set	{ SetValue((int)CategoryFieldIndex.AllowFreeText, value, true); }
		}

		/// <summary> The ScheduleId property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CategoryFieldIndex.ScheduleId, false); }
			set	{ SetValue((int)CategoryFieldIndex.ScheduleId, value, true); }
		}

		/// <summary> The ViewLayoutType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ViewLayoutType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ViewLayoutType ViewLayoutType
		{
			get { return (Obymobi.Enums.ViewLayoutType)GetValue((int)CategoryFieldIndex.ViewLayoutType, true); }
			set	{ SetValue((int)CategoryFieldIndex.ViewLayoutType, value, true); }
		}

		/// <summary> The DeliveryLocationType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."DeliveryLocationType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.DeliveryLocationType DeliveryLocationType
		{
			get { return (Obymobi.Enums.DeliveryLocationType)GetValue((int)CategoryFieldIndex.DeliveryLocationType, true); }
			set	{ SetValue((int)CategoryFieldIndex.DeliveryLocationType, value, true); }
		}

		/// <summary> The SupportNotificationType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."SupportNotificationType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.SupportNotificationType SupportNotificationType
		{
			get { return (Obymobi.Enums.SupportNotificationType)GetValue((int)CategoryFieldIndex.SupportNotificationType, true); }
			set	{ SetValue((int)CategoryFieldIndex.SupportNotificationType, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CategoryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CategoryFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The VisibilityType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."VisibilityType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.VisibilityType VisibilityType
		{
			get { return (Obymobi.Enums.VisibilityType)GetValue((int)CategoryFieldIndex.VisibilityType, true); }
			set	{ SetValue((int)CategoryFieldIndex.VisibilityType, value, true); }
		}

		/// <summary> The ButtonText property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ButtonText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ButtonText
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.ButtonText, true); }
			set	{ SetValue((int)CategoryFieldIndex.ButtonText, value, true); }
		}

		/// <summary> The CustomizeButtonText property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CustomizeButtonText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CustomizeButtonText
		{
			get { return (System.String)GetValue((int)CategoryFieldIndex.CustomizeButtonText, true); }
			set	{ SetValue((int)CategoryFieldIndex.CustomizeButtonText, value, true); }
		}

		/// <summary> The ViewType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ViewType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.CategoryViewType ViewType
		{
			get { return (Obymobi.Enums.CategoryViewType)GetValue((int)CategoryFieldIndex.ViewType, true); }
			set	{ SetValue((int)CategoryFieldIndex.ViewType, value, true); }
		}

		/// <summary> The MenuItemsMustBeLinkedToExternalProduct property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."MenuItemsMustBeLinkedToExternalProduct"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MenuItemsMustBeLinkedToExternalProduct
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.MenuItemsMustBeLinkedToExternalProduct, true); }
			set	{ SetValue((int)CategoryFieldIndex.MenuItemsMustBeLinkedToExternalProduct, value, true); }
		}

		/// <summary> The ShowName property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."ShowName"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowName
		{
			get { return (System.Boolean)GetValue((int)CategoryFieldIndex.ShowName, true); }
			set	{ SetValue((int)CategoryFieldIndex.ShowName, value, true); }
		}

		/// <summary> The CoversType property of the Entity Category<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Category"."CoversType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.CoversType CoversType
		{
			get { return (Obymobi.Enums.CoversType)GetValue((int)CategoryFieldIndex.CoversType, true); }
			set	{ SetValue((int)CategoryFieldIndex.CoversType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagCategoryCollection AdvertisementTagCategoryCollection
		{
			get	{ return GetMultiAdvertisementTagCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagCategoryCollection. When set to true, AdvertisementTagCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagCategoryCollection
		{
			get	{ return _alwaysFetchAdvertisementTagCategoryCollection; }
			set	{ _alwaysFetchAdvertisementTagCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagCategoryCollection already has been fetched. Setting this property to false when AdvertisementTagCategoryCollection has been fetched
		/// will clear the AdvertisementTagCategoryCollection collection well. Setting this property to true while AdvertisementTagCategoryCollection hasn't been fetched disables lazy loading for AdvertisementTagCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagCategoryCollection
		{
			get { return _alreadyFetchedAdvertisementTagCategoryCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagCategoryCollection && !value && (_advertisementTagCategoryCollection != null))
				{
					_advertisementTagCategoryCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollection_
		{
			get	{ return GetMultiAnnouncementCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollection_. When set to true, AnnouncementCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollection_
		{
			get	{ return _alwaysFetchAnnouncementCollection_; }
			set	{ _alwaysFetchAnnouncementCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollection_ already has been fetched. Setting this property to false when AnnouncementCollection_ has been fetched
		/// will clear the AnnouncementCollection_ collection well. Setting this property to true while AnnouncementCollection_ hasn't been fetched disables lazy loading for AnnouncementCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollection_
		{
			get { return _alreadyFetchedAnnouncementCollection_;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollection_ && !value && (_announcementCollection_ != null))
				{
					_announcementCollection_.Clear();
				}
				_alreadyFetchedAnnouncementCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollection
		{
			get	{ return GetMultiAnnouncementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollection. When set to true, AnnouncementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollection
		{
			get	{ return _alwaysFetchAnnouncementCollection; }
			set	{ _alwaysFetchAnnouncementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollection already has been fetched. Setting this property to false when AnnouncementCollection has been fetched
		/// will clear the AnnouncementCollection collection well. Setting this property to true while AnnouncementCollection hasn't been fetched disables lazy loading for AnnouncementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollection
		{
			get { return _alreadyFetchedAnnouncementCollection;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollection && !value && (_announcementCollection != null))
				{
					_announcementCollection.Clear();
				}
				_alreadyFetchedAnnouncementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiActionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ActionCollection ActionCollection
		{
			get	{ return GetMultiActionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ActionCollection. When set to true, ActionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiActionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionCollection
		{
			get	{ return _alwaysFetchActionCollection; }
			set	{ _alwaysFetchActionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionCollection already has been fetched. Setting this property to false when ActionCollection has been fetched
		/// will clear the ActionCollection collection well. Setting this property to true while ActionCollection hasn't been fetched disables lazy loading for ActionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionCollection
		{
			get { return _alreadyFetchedActionCollection;}
			set 
			{
				if(_alreadyFetchedActionCollection && !value && (_actionCollection != null))
				{
					_actionCollection.Clear();
				}
				_alreadyFetchedActionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAvailabilityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection AvailabilityCollection
		{
			get	{ return GetMultiAvailabilityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AvailabilityCollection. When set to true, AvailabilityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AvailabilityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAvailabilityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAvailabilityCollection
		{
			get	{ return _alwaysFetchAvailabilityCollection; }
			set	{ _alwaysFetchAvailabilityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AvailabilityCollection already has been fetched. Setting this property to false when AvailabilityCollection has been fetched
		/// will clear the AvailabilityCollection collection well. Setting this property to true while AvailabilityCollection hasn't been fetched disables lazy loading for AvailabilityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAvailabilityCollection
		{
			get { return _alreadyFetchedAvailabilityCollection;}
			set 
			{
				if(_alreadyFetchedAvailabilityCollection && !value && (_availabilityCollection != null))
				{
					_availabilityCollection.Clear();
				}
				_alreadyFetchedAvailabilityCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChildCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection ChildCategoryCollection
		{
			get	{ return GetMultiChildCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ChildCategoryCollection. When set to true, ChildCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ChildCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiChildCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChildCategoryCollection
		{
			get	{ return _alwaysFetchChildCategoryCollection; }
			set	{ _alwaysFetchChildCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ChildCategoryCollection already has been fetched. Setting this property to false when ChildCategoryCollection has been fetched
		/// will clear the ChildCategoryCollection collection well. Setting this property to true while ChildCategoryCollection hasn't been fetched disables lazy loading for ChildCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChildCategoryCollection
		{
			get { return _alreadyFetchedChildCategoryCollection;}
			set 
			{
				if(_alreadyFetchedChildCategoryCollection && !value && (_childCategoryCollection != null))
				{
					_childCategoryCollection.Clear();
				}
				_alreadyFetchedChildCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryAlterationCollection CategoryAlterationCollection
		{
			get	{ return GetMultiCategoryAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryAlterationCollection. When set to true, CategoryAlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryAlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryAlterationCollection
		{
			get	{ return _alwaysFetchCategoryAlterationCollection; }
			set	{ _alwaysFetchCategoryAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryAlterationCollection already has been fetched. Setting this property to false when CategoryAlterationCollection has been fetched
		/// will clear the CategoryAlterationCollection collection well. Setting this property to true while CategoryAlterationCollection hasn't been fetched disables lazy loading for CategoryAlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryAlterationCollection
		{
			get { return _alreadyFetchedCategoryAlterationCollection;}
			set 
			{
				if(_alreadyFetchedCategoryAlterationCollection && !value && (_categoryAlterationCollection != null))
				{
					_categoryAlterationCollection.Clear();
				}
				_alreadyFetchedCategoryAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryLanguageCollection CategoryLanguageCollection
		{
			get	{ return GetMultiCategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryLanguageCollection. When set to true, CategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryLanguageCollection
		{
			get	{ return _alwaysFetchCategoryLanguageCollection; }
			set	{ _alwaysFetchCategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryLanguageCollection already has been fetched. Setting this property to false when CategoryLanguageCollection has been fetched
		/// will clear the CategoryLanguageCollection collection well. Setting this property to true while CategoryLanguageCollection hasn't been fetched disables lazy loading for CategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryLanguageCollection
		{
			get { return _alreadyFetchedCategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedCategoryLanguageCollection && !value && (_categoryLanguageCollection != null))
				{
					_categoryLanguageCollection.Clear();
				}
				_alreadyFetchedCategoryLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategorySuggestionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategorySuggestionCollection CategorySuggestionCollection
		{
			get	{ return GetMultiCategorySuggestionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategorySuggestionCollection. When set to true, CategorySuggestionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategorySuggestionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategorySuggestionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategorySuggestionCollection
		{
			get	{ return _alwaysFetchCategorySuggestionCollection; }
			set	{ _alwaysFetchCategorySuggestionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategorySuggestionCollection already has been fetched. Setting this property to false when CategorySuggestionCollection has been fetched
		/// will clear the CategorySuggestionCollection collection well. Setting this property to true while CategorySuggestionCollection hasn't been fetched disables lazy loading for CategorySuggestionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategorySuggestionCollection
		{
			get { return _alreadyFetchedCategorySuggestionCollection;}
			set 
			{
				if(_alreadyFetchedCategorySuggestionCollection && !value && (_categorySuggestionCollection != null))
				{
					_categorySuggestionCollection.Clear();
				}
				_alreadyFetchedCategorySuggestionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategoryTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryTagCollection CategoryTagCollection
		{
			get	{ return GetMultiCategoryTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryTagCollection. When set to true, CategoryTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryTagCollection
		{
			get	{ return _alwaysFetchCategoryTagCollection; }
			set	{ _alwaysFetchCategoryTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryTagCollection already has been fetched. Setting this property to false when CategoryTagCollection has been fetched
		/// will clear the CategoryTagCollection collection well. Setting this property to true while CategoryTagCollection hasn't been fetched disables lazy loading for CategoryTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryTagCollection
		{
			get { return _alreadyFetchedCategoryTagCollection;}
			set 
			{
				if(_alreadyFetchedCategoryTagCollection && !value && (_categoryTagCollection != null))
				{
					_categoryTagCollection.Clear();
				}
				_alreadyFetchedCategoryTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiActionMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection ActionMediaCollection
		{
			get	{ return GetMultiActionMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ActionMediaCollection. When set to true, ActionMediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionMediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiActionMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionMediaCollection
		{
			get	{ return _alwaysFetchActionMediaCollection; }
			set	{ _alwaysFetchActionMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionMediaCollection already has been fetched. Setting this property to false when ActionMediaCollection has been fetched
		/// will clear the ActionMediaCollection collection well. Setting this property to true while ActionMediaCollection hasn't been fetched disables lazy loading for ActionMediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionMediaCollection
		{
			get { return _alreadyFetchedActionMediaCollection;}
			set 
			{
				if(_alreadyFetchedActionMediaCollection && !value && (_actionMediaCollection != null))
				{
					_actionMediaCollection.Clear();
				}
				_alreadyFetchedActionMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageRecipientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection MessageRecipientCollection
		{
			get	{ return GetMultiMessageRecipientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageRecipientCollection. When set to true, MessageRecipientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageRecipientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageRecipientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageRecipientCollection
		{
			get	{ return _alwaysFetchMessageRecipientCollection; }
			set	{ _alwaysFetchMessageRecipientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageRecipientCollection already has been fetched. Setting this property to false when MessageRecipientCollection has been fetched
		/// will clear the MessageRecipientCollection collection well. Setting this property to true while MessageRecipientCollection hasn't been fetched disables lazy loading for MessageRecipientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageRecipientCollection
		{
			get { return _alreadyFetchedMessageRecipientCollection;}
			set 
			{
				if(_alreadyFetchedMessageRecipientCollection && !value && (_messageRecipientCollection != null))
				{
					_messageRecipientCollection.Clear();
				}
				_alreadyFetchedMessageRecipientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection MessageTemplateCollection
		{
			get	{ return GetMultiMessageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageTemplateCollection. When set to true, MessageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageTemplateCollection
		{
			get	{ return _alwaysFetchMessageTemplateCollection; }
			set	{ _alwaysFetchMessageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageTemplateCollection already has been fetched. Setting this property to false when MessageTemplateCollection has been fetched
		/// will clear the MessageTemplateCollection collection well. Setting this property to true while MessageTemplateCollection hasn't been fetched disables lazy loading for MessageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageTemplateCollection
		{
			get { return _alreadyFetchedMessageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedMessageTemplateCollection && !value && (_messageTemplateCollection != null))
				{
					_messageTemplateCollection.Clear();
				}
				_alreadyFetchedMessageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderHourEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderHourCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderHourCollection OrderHourCollection
		{
			get	{ return GetMultiOrderHourCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderHourCollection. When set to true, OrderHourCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderHourCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderHourCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderHourCollection
		{
			get	{ return _alwaysFetchOrderHourCollection; }
			set	{ _alwaysFetchOrderHourCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderHourCollection already has been fetched. Setting this property to false when OrderHourCollection has been fetched
		/// will clear the OrderHourCollection collection well. Setting this property to true while OrderHourCollection hasn't been fetched disables lazy loading for OrderHourCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderHourCollection
		{
			get { return _alreadyFetchedOrderHourCollection;}
			set 
			{
				if(_alreadyFetchedOrderHourCollection && !value && (_orderHourCollection != null))
				{
					_orderHourCollection.Clear();
				}
				_alreadyFetchedOrderHourCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemCollection OrderitemCollection
		{
			get	{ return GetMultiOrderitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemCollection. When set to true, OrderitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemCollection
		{
			get	{ return _alwaysFetchOrderitemCollection; }
			set	{ _alwaysFetchOrderitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemCollection already has been fetched. Setting this property to false when OrderitemCollection has been fetched
		/// will clear the OrderitemCollection collection well. Setting this property to true while OrderitemCollection hasn't been fetched disables lazy loading for OrderitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemCollection
		{
			get { return _alreadyFetchedOrderitemCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemCollection && !value && (_orderitemCollection != null))
				{
					_orderitemCollection.Clear();
				}
				_alreadyFetchedOrderitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryCollection ProductCategoryCollection
		{
			get	{ return GetMultiProductCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryCollection. When set to true, ProductCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryCollection
		{
			get	{ return _alwaysFetchProductCategoryCollection; }
			set	{ _alwaysFetchProductCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryCollection already has been fetched. Setting this property to false when ProductCategoryCollection has been fetched
		/// will clear the ProductCategoryCollection collection well. Setting this property to true while ProductCategoryCollection hasn't been fetched disables lazy loading for ProductCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryCollection
		{
			get { return _alreadyFetchedProductCategoryCollection;}
			set 
			{
				if(_alreadyFetchedProductCategoryCollection && !value && (_productCategoryCollection != null))
				{
					_productCategoryCollection.Clear();
				}
				_alreadyFetchedProductCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCategoryTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryTagCollection ProductCategoryTagCollection
		{
			get	{ return GetMultiProductCategoryTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryTagCollection. When set to true, ProductCategoryTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCategoryTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryTagCollection
		{
			get	{ return _alwaysFetchProductCategoryTagCollection; }
			set	{ _alwaysFetchProductCategoryTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryTagCollection already has been fetched. Setting this property to false when ProductCategoryTagCollection has been fetched
		/// will clear the ProductCategoryTagCollection collection well. Setting this property to true while ProductCategoryTagCollection hasn't been fetched disables lazy loading for ProductCategoryTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryTagCollection
		{
			get { return _alreadyFetchedProductCategoryTagCollection;}
			set 
			{
				if(_alreadyFetchedProductCategoryTagCollection && !value && (_productCategoryTagCollection != null))
				{
					_productCategoryTagCollection.Clear();
				}
				_alreadyFetchedProductCategoryTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection ScheduledMessageCollection
		{
			get	{ return GetMultiScheduledMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageCollection. When set to true, ScheduledMessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageCollection
		{
			get	{ return _alwaysFetchScheduledMessageCollection; }
			set	{ _alwaysFetchScheduledMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageCollection already has been fetched. Setting this property to false when ScheduledMessageCollection has been fetched
		/// will clear the ScheduledMessageCollection collection well. Setting this property to true while ScheduledMessageCollection hasn't been fetched disables lazy loading for ScheduledMessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageCollection
		{
			get { return _alreadyFetchedScheduledMessageCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageCollection && !value && (_scheduledMessageCollection != null))
				{
					_scheduledMessageCollection.Clear();
				}
				_alreadyFetchedScheduledMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUITabCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UITabCollection UITabCollection
		{
			get	{ return GetMultiUITabCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UITabCollection. When set to true, UITabCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUITabCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabCollection
		{
			get	{ return _alwaysFetchUITabCollection; }
			set	{ _alwaysFetchUITabCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabCollection already has been fetched. Setting this property to false when UITabCollection has been fetched
		/// will clear the UITabCollection collection well. Setting this property to true while UITabCollection hasn't been fetched disables lazy loading for UITabCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabCollection
		{
			get { return _alreadyFetchedUITabCollection;}
			set 
			{
				if(_alreadyFetchedUITabCollection && !value && (_uITabCollection != null))
				{
					_uITabCollection.Clear();
				}
				_alreadyFetchedUITabCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollectionViaMedium
		{
			get { return GetMultiAdvertisementCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollectionViaMedium. When set to true, AdvertisementCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAdvertisementCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollectionViaMedium
		{
			get	{ return _alwaysFetchAdvertisementCollectionViaMedium; }
			set	{ _alwaysFetchAdvertisementCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollectionViaMedium already has been fetched. Setting this property to false when AdvertisementCollectionViaMedium has been fetched
		/// will clear the AdvertisementCollectionViaMedium collection well. Setting this property to true while AdvertisementCollectionViaMedium hasn't been fetched disables lazy loading for AdvertisementCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollectionViaMedium
		{
			get { return _alreadyFetchedAdvertisementCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollectionViaMedium && !value && (_advertisementCollectionViaMedium != null))
				{
					_advertisementCollectionViaMedium.Clear();
				}
				_alreadyFetchedAdvertisementCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollectionViaMedium
		{
			get { return GetMultiAlterationCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollectionViaMedium. When set to true, AlterationCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationCollectionViaMedium; }
			set	{ _alwaysFetchAlterationCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollectionViaMedium already has been fetched. Setting this property to false when AlterationCollectionViaMedium has been fetched
		/// will clear the AlterationCollectionViaMedium collection well. Setting this property to true while AlterationCollectionViaMedium hasn't been fetched disables lazy loading for AlterationCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationCollectionViaMedium && !value && (_alterationCollectionViaMedium != null))
				{
					_alterationCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollectionViaCategoryAlteration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollectionViaCategoryAlteration
		{
			get { return GetMultiAlterationCollectionViaCategoryAlteration(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollectionViaCategoryAlteration. When set to true, AlterationCollectionViaCategoryAlteration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollectionViaCategoryAlteration is accessed. You can always execute a forced fetch by calling GetMultiAlterationCollectionViaCategoryAlteration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollectionViaCategoryAlteration
		{
			get	{ return _alwaysFetchAlterationCollectionViaCategoryAlteration; }
			set	{ _alwaysFetchAlterationCollectionViaCategoryAlteration = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollectionViaCategoryAlteration already has been fetched. Setting this property to false when AlterationCollectionViaCategoryAlteration has been fetched
		/// will clear the AlterationCollectionViaCategoryAlteration collection well. Setting this property to true while AlterationCollectionViaCategoryAlteration hasn't been fetched disables lazy loading for AlterationCollectionViaCategoryAlteration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollectionViaCategoryAlteration
		{
			get { return _alreadyFetchedAlterationCollectionViaCategoryAlteration;}
			set 
			{
				if(_alreadyFetchedAlterationCollectionViaCategoryAlteration && !value && (_alterationCollectionViaCategoryAlteration != null))
				{
					_alterationCollectionViaCategoryAlteration.Clear();
				}
				_alreadyFetchedAlterationCollectionViaCategoryAlteration = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium_
		{
			get { return GetMultiAlterationoptionCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium_. When set to true, AlterationoptionCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium_
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium_; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium_ already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium_ has been fetched
		/// will clear the AlterationoptionCollectionViaMedium_ collection well. Setting this property to true while AlterationoptionCollectionViaMedium_ hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium_
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium_ && !value && (_alterationoptionCollectionViaMedium_ != null))
				{
					_alterationoptionCollectionViaMedium_.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium_
		{
			get { return GetMultiCategoryCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium_. When set to true, CategoryCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium_
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium_; }
			set	{ _alwaysFetchCategoryCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium_ already has been fetched. Setting this property to false when CategoryCollectionViaMedium_ has been fetched
		/// will clear the CategoryCollectionViaMedium_ collection well. Setting this property to true while CategoryCollectionViaMedium_ hasn't been fetched disables lazy loading for CategoryCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium_
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium_ && !value && (_categoryCollectionViaMedium_ != null))
				{
					_categoryCollectionViaMedium_.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollectionViaMessage
		{
			get { return GetMultiClientCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaMessage. When set to true, ClientCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaMessage
		{
			get	{ return _alwaysFetchClientCollectionViaMessage; }
			set	{ _alwaysFetchClientCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaMessage already has been fetched. Setting this property to false when ClientCollectionViaMessage has been fetched
		/// will clear the ClientCollectionViaMessage collection well. Setting this property to true while ClientCollectionViaMessage hasn't been fetched disables lazy loading for ClientCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaMessage
		{
			get { return _alreadyFetchedClientCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaMessage && !value && (_clientCollectionViaMessage != null))
				{
					_clientCollectionViaMessage.Clear();
				}
				_alreadyFetchedClientCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMedium
		{
			get { return GetMultiCompanyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMedium. When set to true, CompanyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMedium
		{
			get	{ return _alwaysFetchCompanyCollectionViaMedium; }
			set	{ _alwaysFetchCompanyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMedium already has been fetched. Setting this property to false when CompanyCollectionViaMedium has been fetched
		/// will clear the CompanyCollectionViaMedium collection well. Setting this property to true while CompanyCollectionViaMedium hasn't been fetched disables lazy loading for CompanyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMedium
		{
			get { return _alreadyFetchedCompanyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMedium && !value && (_companyCollectionViaMedium != null))
				{
					_companyCollectionViaMedium.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMessage
		{
			get { return GetMultiCompanyCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMessage. When set to true, CompanyCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMessage
		{
			get	{ return _alwaysFetchCompanyCollectionViaMessage; }
			set	{ _alwaysFetchCompanyCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMessage already has been fetched. Setting this property to false when CompanyCollectionViaMessage has been fetched
		/// will clear the CompanyCollectionViaMessage collection well. Setting this property to true while CompanyCollectionViaMessage hasn't been fetched disables lazy loading for CompanyCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMessage
		{
			get { return _alreadyFetchedCompanyCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMessage && !value && (_companyCollectionViaMessage != null))
				{
					_companyCollectionViaMessage.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMessageTemplate
		{
			get { return GetMultiCompanyCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMessageTemplate. When set to true, CompanyCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchCompanyCollectionViaMessageTemplate; }
			set	{ _alwaysFetchCompanyCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMessageTemplate already has been fetched. Setting this property to false when CompanyCollectionViaMessageTemplate has been fetched
		/// will clear the CompanyCollectionViaMessageTemplate collection well. Setting this property to true while CompanyCollectionViaMessageTemplate hasn't been fetched disables lazy loading for CompanyCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedCompanyCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMessageTemplate && !value && (_companyCollectionViaMessageTemplate != null))
				{
					_companyCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaAdvertisement
		{
			get { return GetMultiCompanyCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaAdvertisement. When set to true, CompanyCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchCompanyCollectionViaAdvertisement; }
			set	{ _alwaysFetchCompanyCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaAdvertisement already has been fetched. Setting this property to false when CompanyCollectionViaAdvertisement has been fetched
		/// will clear the CompanyCollectionViaAdvertisement collection well. Setting this property to true while CompanyCollectionViaAdvertisement hasn't been fetched disables lazy loading for CompanyCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaAdvertisement
		{
			get { return _alreadyFetchedCompanyCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaAdvertisement && !value && (_companyCollectionViaAdvertisement != null))
				{
					_companyCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedCompanyCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaMessage
		{
			get { return GetMultiCustomerCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaMessage. When set to true, CustomerCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaMessage
		{
			get	{ return _alwaysFetchCustomerCollectionViaMessage; }
			set	{ _alwaysFetchCustomerCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaMessage already has been fetched. Setting this property to false when CustomerCollectionViaMessage has been fetched
		/// will clear the CustomerCollectionViaMessage collection well. Setting this property to true while CustomerCollectionViaMessage hasn't been fetched disables lazy loading for CustomerCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaMessage
		{
			get { return _alreadyFetchedCustomerCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaMessage && !value && (_customerCollectionViaMessage != null))
				{
					_customerCollectionViaMessage.Clear();
				}
				_alreadyFetchedCustomerCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaMessage
		{
			get { return GetMultiDeliverypointCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaMessage. When set to true, DeliverypointCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaMessage
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaMessage; }
			set	{ _alwaysFetchDeliverypointCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaMessage already has been fetched. Setting this property to false when DeliverypointCollectionViaMessage has been fetched
		/// will clear the DeliverypointCollectionViaMessage collection well. Setting this property to true while DeliverypointCollectionViaMessage hasn't been fetched disables lazy loading for DeliverypointCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaMessage
		{
			get { return _alreadyFetchedDeliverypointCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaMessage && !value && (_deliverypointCollectionViaMessage != null))
				{
					_deliverypointCollectionViaMessage.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAdvertisement
		{
			get { return GetMultiDeliverypointgroupCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAdvertisement. When set to true, DeliverypointgroupCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAdvertisement; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAdvertisement already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAdvertisement has been fetched
		/// will clear the DeliverypointgroupCollectionViaAdvertisement collection well. Setting this property to true while DeliverypointgroupCollectionViaAdvertisement hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAdvertisement
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement && !value && (_deliverypointgroupCollectionViaAdvertisement != null))
				{
					_deliverypointgroupCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAnnouncement
		{
			get { return GetMultiDeliverypointgroupCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAnnouncement. When set to true, DeliverypointgroupCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAnnouncement; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAnnouncement already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAnnouncement has been fetched
		/// will clear the DeliverypointgroupCollectionViaAnnouncement collection well. Setting this property to true while DeliverypointgroupCollectionViaAnnouncement hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAnnouncement
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement && !value && (_deliverypointgroupCollectionViaAnnouncement != null))
				{
					_deliverypointgroupCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAnnouncement_
		{
			get { return GetMultiDeliverypointgroupCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAnnouncement_. When set to true, DeliverypointgroupCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAnnouncement_ already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAnnouncement_ has been fetched
		/// will clear the DeliverypointgroupCollectionViaAnnouncement_ collection well. Setting this property to true while DeliverypointgroupCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ && !value && (_deliverypointgroupCollectionViaAnnouncement_ != null))
				{
					_deliverypointgroupCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaMedium
		{
			get { return GetMultiDeliverypointgroupCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaMedium. When set to true, DeliverypointgroupCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaMedium
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaMedium; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaMedium already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaMedium has been fetched
		/// will clear the DeliverypointgroupCollectionViaMedium collection well. Setting this property to true while DeliverypointgroupCollectionViaMedium hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaMedium
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaMedium && !value && (_deliverypointgroupCollectionViaMedium != null))
				{
					_deliverypointgroupCollectionViaMedium.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAnnouncement
		{
			get { return GetMultiEntertainmentCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAnnouncement. When set to true, EntertainmentCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAnnouncement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAnnouncement already has been fetched. Setting this property to false when EntertainmentCollectionViaAnnouncement has been fetched
		/// will clear the EntertainmentCollectionViaAnnouncement collection well. Setting this property to true while EntertainmentCollectionViaAnnouncement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAnnouncement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAnnouncement && !value && (_entertainmentCollectionViaAnnouncement != null))
				{
					_entertainmentCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAnnouncement_
		{
			get { return GetMultiEntertainmentCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAnnouncement_. When set to true, EntertainmentCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAnnouncement_; }
			set	{ _alwaysFetchEntertainmentCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAnnouncement_ already has been fetched. Setting this property to false when EntertainmentCollectionViaAnnouncement_ has been fetched
		/// will clear the EntertainmentCollectionViaAnnouncement_ collection well. Setting this property to true while EntertainmentCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAnnouncement_ && !value && (_entertainmentCollectionViaAnnouncement_ != null))
				{
					_entertainmentCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium_
		{
			get { return GetMultiEntertainmentCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium_. When set to true, EntertainmentCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium_; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium_ already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium_ has been fetched
		/// will clear the EntertainmentCollectionViaMedium_ collection well. Setting this property to true while EntertainmentCollectionViaMedium_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium_ && !value && (_entertainmentCollectionViaMedium_ != null))
				{
					_entertainmentCollectionViaMedium_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium__
		{
			get { return GetMultiEntertainmentCollectionViaMedium__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium__. When set to true, EntertainmentCollectionViaMedium__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium__ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium__
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium__; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium__ already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium__ has been fetched
		/// will clear the EntertainmentCollectionViaMedium__ collection well. Setting this property to true while EntertainmentCollectionViaMedium__ hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium__
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium__;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium__ && !value && (_entertainmentCollectionViaMedium__ != null))
				{
					_entertainmentCollectionViaMedium__.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessage
		{
			get { return GetMultiEntertainmentCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessage. When set to true, EntertainmentCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessage
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessage; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessage already has been fetched. Setting this property to false when EntertainmentCollectionViaMessage has been fetched
		/// will clear the EntertainmentCollectionViaMessage collection well. Setting this property to true while EntertainmentCollectionViaMessage hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessage
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessage && !value && (_entertainmentCollectionViaMessage != null))
				{
					_entertainmentCollectionViaMessage.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessageTemplate
		{
			get { return GetMultiEntertainmentCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessageTemplate. When set to true, EntertainmentCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessageTemplate; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessageTemplate already has been fetched. Setting this property to false when EntertainmentCollectionViaMessageTemplate has been fetched
		/// will clear the EntertainmentCollectionViaMessageTemplate collection well. Setting this property to true while EntertainmentCollectionViaMessageTemplate hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessageTemplate && !value && (_entertainmentCollectionViaMessageTemplate != null))
				{
					_entertainmentCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaUITab()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaUITab
		{
			get { return GetMultiEntertainmentCollectionViaUITab(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaUITab. When set to true, EntertainmentCollectionViaUITab is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaUITab is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaUITab(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaUITab
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaUITab; }
			set	{ _alwaysFetchEntertainmentCollectionViaUITab = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaUITab already has been fetched. Setting this property to false when EntertainmentCollectionViaUITab has been fetched
		/// will clear the EntertainmentCollectionViaUITab collection well. Setting this property to true while EntertainmentCollectionViaUITab hasn't been fetched disables lazy loading for EntertainmentCollectionViaUITab</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaUITab
		{
			get { return _alreadyFetchedEntertainmentCollectionViaUITab;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaUITab && !value && (_entertainmentCollectionViaUITab != null))
				{
					_entertainmentCollectionViaUITab.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaUITab = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement. When set to true, EntertainmentCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement && !value && (_entertainmentCollectionViaAdvertisement != null))
				{
					_entertainmentCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement_
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement_. When set to true, EntertainmentCollectionViaAdvertisement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement_; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement_ already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement_ has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement_ collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement_ && !value && (_entertainmentCollectionViaAdvertisement_ != null))
				{
					_entertainmentCollectionViaAdvertisement_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAdvertisement. When set to true, EntertainmentcategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentcategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement && !value && (_entertainmentcategoryCollectionViaAdvertisement != null))
				{
					_entertainmentcategoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium_
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium_. When set to true, EntertainmentcategoryCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium_
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium_; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium_ already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium_ has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium_ collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium_ hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium_
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium_ && !value && (_entertainmentcategoryCollectionViaMedium_ != null))
				{
					_entertainmentcategoryCollectionViaMedium_.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollectionViaMedium
		{
			get { return GetMultiGenericcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollectionViaMedium. When set to true, GenericcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericcategoryCollectionViaMedium; }
			set	{ _alwaysFetchGenericcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollectionViaMedium already has been fetched. Setting this property to false when GenericcategoryCollectionViaMedium has been fetched
		/// will clear the GenericcategoryCollectionViaMedium collection well. Setting this property to true while GenericcategoryCollectionViaMedium hasn't been fetched disables lazy loading for GenericcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedGenericcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollectionViaMedium && !value && (_genericcategoryCollectionViaMedium != null))
				{
					_genericcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaMedium
		{
			get { return GetMultiGenericproductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaMedium. When set to true, GenericproductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericproductCollectionViaMedium; }
			set	{ _alwaysFetchGenericproductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaMedium already has been fetched. Setting this property to false when GenericproductCollectionViaMedium has been fetched
		/// will clear the GenericproductCollectionViaMedium collection well. Setting this property to true while GenericproductCollectionViaMedium hasn't been fetched disables lazy loading for GenericproductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaMedium
		{
			get { return _alreadyFetchedGenericproductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaMedium && !value && (_genericproductCollectionViaMedium != null))
				{
					_genericproductCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaAdvertisement
		{
			get { return GetMultiGenericproductCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaAdvertisement. When set to true, GenericproductCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchGenericproductCollectionViaAdvertisement; }
			set	{ _alwaysFetchGenericproductCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaAdvertisement already has been fetched. Setting this property to false when GenericproductCollectionViaAdvertisement has been fetched
		/// will clear the GenericproductCollectionViaAdvertisement collection well. Setting this property to true while GenericproductCollectionViaAdvertisement hasn't been fetched disables lazy loading for GenericproductCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaAdvertisement
		{
			get { return _alreadyFetchedGenericproductCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaAdvertisement && !value && (_genericproductCollectionViaAdvertisement != null))
				{
					_genericproductCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaAnnouncement
		{
			get { return GetMultiMediaCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaAnnouncement. When set to true, MediaCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchMediaCollectionViaAnnouncement; }
			set	{ _alwaysFetchMediaCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaAnnouncement already has been fetched. Setting this property to false when MediaCollectionViaAnnouncement has been fetched
		/// will clear the MediaCollectionViaAnnouncement collection well. Setting this property to true while MediaCollectionViaAnnouncement hasn't been fetched disables lazy loading for MediaCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaAnnouncement
		{
			get { return _alreadyFetchedMediaCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaAnnouncement && !value && (_mediaCollectionViaAnnouncement != null))
				{
					_mediaCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedMediaCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaAnnouncement_
		{
			get { return GetMultiMediaCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaAnnouncement_. When set to true, MediaCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchMediaCollectionViaAnnouncement_; }
			set	{ _alwaysFetchMediaCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaAnnouncement_ already has been fetched. Setting this property to false when MediaCollectionViaAnnouncement_ has been fetched
		/// will clear the MediaCollectionViaAnnouncement_ collection well. Setting this property to true while MediaCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for MediaCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedMediaCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaAnnouncement_ && !value && (_mediaCollectionViaAnnouncement_ != null))
				{
					_mediaCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedMediaCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaMessage
		{
			get { return GetMultiMediaCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaMessage. When set to true, MediaCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaMessage
		{
			get	{ return _alwaysFetchMediaCollectionViaMessage; }
			set	{ _alwaysFetchMediaCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaMessage already has been fetched. Setting this property to false when MediaCollectionViaMessage has been fetched
		/// will clear the MediaCollectionViaMessage collection well. Setting this property to true while MediaCollectionViaMessage hasn't been fetched disables lazy loading for MediaCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaMessage
		{
			get { return _alreadyFetchedMediaCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaMessage && !value && (_mediaCollectionViaMessage != null))
				{
					_mediaCollectionViaMessage.Clear();
				}
				_alreadyFetchedMediaCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaMessageTemplate
		{
			get { return GetMultiMediaCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaMessageTemplate. When set to true, MediaCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchMediaCollectionViaMessageTemplate; }
			set	{ _alwaysFetchMediaCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaMessageTemplate already has been fetched. Setting this property to false when MediaCollectionViaMessageTemplate has been fetched
		/// will clear the MediaCollectionViaMessageTemplate collection well. Setting this property to true while MediaCollectionViaMessageTemplate hasn't been fetched disables lazy loading for MediaCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedMediaCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaMessageTemplate && !value && (_mediaCollectionViaMessageTemplate != null))
				{
					_mediaCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedMediaCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaCategory
		{
			get { return GetMultiMenuCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaCategory. When set to true, MenuCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaCategory
		{
			get	{ return _alwaysFetchMenuCollectionViaCategory; }
			set	{ _alwaysFetchMenuCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaCategory already has been fetched. Setting this property to false when MenuCollectionViaCategory has been fetched
		/// will clear the MenuCollectionViaCategory collection well. Setting this property to true while MenuCollectionViaCategory hasn't been fetched disables lazy loading for MenuCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaCategory
		{
			get { return _alreadyFetchedMenuCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaCategory && !value && (_menuCollectionViaCategory != null))
				{
					_menuCollectionViaCategory.Clear();
				}
				_alreadyFetchedMenuCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaMessage
		{
			get { return GetMultiOrderCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaMessage. When set to true, OrderCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaMessage
		{
			get	{ return _alwaysFetchOrderCollectionViaMessage; }
			set	{ _alwaysFetchOrderCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaMessage already has been fetched. Setting this property to false when OrderCollectionViaMessage has been fetched
		/// will clear the OrderCollectionViaMessage collection well. Setting this property to true while OrderCollectionViaMessage hasn't been fetched disables lazy loading for OrderCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaMessage
		{
			get { return _alreadyFetchedOrderCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaMessage && !value && (_orderCollectionViaMessage != null))
				{
					_orderCollectionViaMessage.Clear();
				}
				_alreadyFetchedOrderCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrderitem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrderitem
		{
			get { return GetMultiOrderCollectionViaOrderitem(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrderitem. When set to true, OrderCollectionViaOrderitem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrderitem is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrderitem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrderitem
		{
			get	{ return _alwaysFetchOrderCollectionViaOrderitem; }
			set	{ _alwaysFetchOrderCollectionViaOrderitem = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrderitem already has been fetched. Setting this property to false when OrderCollectionViaOrderitem has been fetched
		/// will clear the OrderCollectionViaOrderitem collection well. Setting this property to true while OrderCollectionViaOrderitem hasn't been fetched disables lazy loading for OrderCollectionViaOrderitem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrderitem
		{
			get { return _alreadyFetchedOrderCollectionViaOrderitem;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrderitem && !value && (_orderCollectionViaOrderitem != null))
				{
					_orderCollectionViaOrderitem.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrderitem = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium_
		{
			get { return GetMultiPointOfInterestCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium_. When set to true, PointOfInterestCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium_
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium_; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium_ already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium_ has been fetched
		/// will clear the PointOfInterestCollectionViaMedium_ collection well. Setting this property to true while PointOfInterestCollectionViaMedium_ hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium_
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium_ && !value && (_pointOfInterestCollectionViaMedium_ != null))
				{
					_pointOfInterestCollectionViaMedium_.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAnnouncement
		{
			get { return GetMultiProductCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAnnouncement. When set to true, ProductCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchProductCollectionViaAnnouncement; }
			set	{ _alwaysFetchProductCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAnnouncement already has been fetched. Setting this property to false when ProductCollectionViaAnnouncement has been fetched
		/// will clear the ProductCollectionViaAnnouncement collection well. Setting this property to true while ProductCollectionViaAnnouncement hasn't been fetched disables lazy loading for ProductCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAnnouncement
		{
			get { return _alreadyFetchedProductCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAnnouncement && !value && (_productCollectionViaAnnouncement != null))
				{
					_productCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedProductCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAnnouncement_
		{
			get { return GetMultiProductCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAnnouncement_. When set to true, ProductCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchProductCollectionViaAnnouncement_; }
			set	{ _alwaysFetchProductCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAnnouncement_ already has been fetched. Setting this property to false when ProductCollectionViaAnnouncement_ has been fetched
		/// will clear the ProductCollectionViaAnnouncement_ collection well. Setting this property to true while ProductCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for ProductCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedProductCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAnnouncement_ && !value && (_productCollectionViaAnnouncement_ != null))
				{
					_productCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedProductCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaCategorySuggestion()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaCategorySuggestion
		{
			get { return GetMultiProductCollectionViaCategorySuggestion(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaCategorySuggestion. When set to true, ProductCollectionViaCategorySuggestion is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaCategorySuggestion is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaCategorySuggestion(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaCategorySuggestion
		{
			get	{ return _alwaysFetchProductCollectionViaCategorySuggestion; }
			set	{ _alwaysFetchProductCollectionViaCategorySuggestion = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaCategorySuggestion already has been fetched. Setting this property to false when ProductCollectionViaCategorySuggestion has been fetched
		/// will clear the ProductCollectionViaCategorySuggestion collection well. Setting this property to true while ProductCollectionViaCategorySuggestion hasn't been fetched disables lazy loading for ProductCollectionViaCategorySuggestion</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaCategorySuggestion
		{
			get { return _alreadyFetchedProductCollectionViaCategorySuggestion;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaCategorySuggestion && !value && (_productCollectionViaCategorySuggestion != null))
				{
					_productCollectionViaCategorySuggestion.Clear();
				}
				_alreadyFetchedProductCollectionViaCategorySuggestion = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium_
		{
			get { return GetMultiProductCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium_. When set to true, ProductCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium_
		{
			get	{ return _alwaysFetchProductCollectionViaMedium_; }
			set	{ _alwaysFetchProductCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium_ already has been fetched. Setting this property to false when ProductCollectionViaMedium_ has been fetched
		/// will clear the ProductCollectionViaMedium_ collection well. Setting this property to true while ProductCollectionViaMedium_ hasn't been fetched disables lazy loading for ProductCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium_
		{
			get { return _alreadyFetchedProductCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium_ && !value && (_productCollectionViaMedium_ != null))
				{
					_productCollectionViaMedium_.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium__
		{
			get { return GetMultiProductCollectionViaMedium__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium__. When set to true, ProductCollectionViaMedium__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium__ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium__
		{
			get	{ return _alwaysFetchProductCollectionViaMedium__; }
			set	{ _alwaysFetchProductCollectionViaMedium__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium__ already has been fetched. Setting this property to false when ProductCollectionViaMedium__ has been fetched
		/// will clear the ProductCollectionViaMedium__ collection well. Setting this property to true while ProductCollectionViaMedium__ hasn't been fetched disables lazy loading for ProductCollectionViaMedium__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium__
		{
			get { return _alreadyFetchedProductCollectionViaMedium__;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium__ && !value && (_productCollectionViaMedium__ != null))
				{
					_productCollectionViaMedium__.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMessageTemplate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMessageTemplate
		{
			get { return GetMultiProductCollectionViaMessageTemplate(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMessageTemplate. When set to true, ProductCollectionViaMessageTemplate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMessageTemplate is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMessageTemplate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMessageTemplate
		{
			get	{ return _alwaysFetchProductCollectionViaMessageTemplate; }
			set	{ _alwaysFetchProductCollectionViaMessageTemplate = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMessageTemplate already has been fetched. Setting this property to false when ProductCollectionViaMessageTemplate has been fetched
		/// will clear the ProductCollectionViaMessageTemplate collection well. Setting this property to true while ProductCollectionViaMessageTemplate hasn't been fetched disables lazy loading for ProductCollectionViaMessageTemplate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMessageTemplate
		{
			get { return _alreadyFetchedProductCollectionViaMessageTemplate;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMessageTemplate && !value && (_productCollectionViaMessageTemplate != null))
				{
					_productCollectionViaMessageTemplate.Clear();
				}
				_alreadyFetchedProductCollectionViaMessageTemplate = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaOrderitem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaOrderitem
		{
			get { return GetMultiProductCollectionViaOrderitem(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaOrderitem. When set to true, ProductCollectionViaOrderitem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaOrderitem is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaOrderitem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaOrderitem
		{
			get	{ return _alwaysFetchProductCollectionViaOrderitem; }
			set	{ _alwaysFetchProductCollectionViaOrderitem = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaOrderitem already has been fetched. Setting this property to false when ProductCollectionViaOrderitem has been fetched
		/// will clear the ProductCollectionViaOrderitem collection well. Setting this property to true while ProductCollectionViaOrderitem hasn't been fetched disables lazy loading for ProductCollectionViaOrderitem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaOrderitem
		{
			get { return _alreadyFetchedProductCollectionViaOrderitem;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaOrderitem && !value && (_productCollectionViaOrderitem != null))
				{
					_productCollectionViaOrderitem.Clear();
				}
				_alreadyFetchedProductCollectionViaOrderitem = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaProductCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaProductCategory
		{
			get { return GetMultiProductCollectionViaProductCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaProductCategory. When set to true, ProductCollectionViaProductCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaProductCategory is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaProductCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaProductCategory
		{
			get	{ return _alwaysFetchProductCollectionViaProductCategory; }
			set	{ _alwaysFetchProductCollectionViaProductCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaProductCategory already has been fetched. Setting this property to false when ProductCollectionViaProductCategory has been fetched
		/// will clear the ProductCollectionViaProductCategory collection well. Setting this property to true while ProductCollectionViaProductCategory hasn't been fetched disables lazy loading for ProductCollectionViaProductCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaProductCategory
		{
			get { return _alreadyFetchedProductCollectionViaProductCategory;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaProductCategory && !value && (_productCollectionViaProductCategory != null))
				{
					_productCollectionViaProductCategory.Clear();
				}
				_alreadyFetchedProductCollectionViaProductCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAdvertisement
		{
			get { return GetMultiProductCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAdvertisement. When set to true, ProductCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchProductCollectionViaAdvertisement; }
			set	{ _alwaysFetchProductCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAdvertisement already has been fetched. Setting this property to false when ProductCollectionViaAdvertisement has been fetched
		/// will clear the ProductCollectionViaAdvertisement collection well. Setting this property to true while ProductCollectionViaAdvertisement hasn't been fetched disables lazy loading for ProductCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAdvertisement
		{
			get { return _alreadyFetchedProductCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAdvertisement && !value && (_productCollectionViaAdvertisement != null))
				{
					_productCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedProductCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCategory
		{
			get { return GetMultiRouteCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCategory. When set to true, RouteCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCategory
		{
			get	{ return _alwaysFetchRouteCollectionViaCategory; }
			set	{ _alwaysFetchRouteCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCategory already has been fetched. Setting this property to false when RouteCollectionViaCategory has been fetched
		/// will clear the RouteCollectionViaCategory collection well. Setting this property to true while RouteCollectionViaCategory hasn't been fetched disables lazy loading for RouteCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCategory
		{
			get { return _alreadyFetchedRouteCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCategory && !value && (_routeCollectionViaCategory != null))
				{
					_routeCollectionViaCategory.Clear();
				}
				_alreadyFetchedRouteCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupplierCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupplierCollection SupplierCollectionViaAdvertisement
		{
			get { return GetMultiSupplierCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupplierCollectionViaAdvertisement. When set to true, SupplierCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupplierCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiSupplierCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupplierCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchSupplierCollectionViaAdvertisement; }
			set	{ _alwaysFetchSupplierCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupplierCollectionViaAdvertisement already has been fetched. Setting this property to false when SupplierCollectionViaAdvertisement has been fetched
		/// will clear the SupplierCollectionViaAdvertisement collection well. Setting this property to true while SupplierCollectionViaAdvertisement hasn't been fetched disables lazy loading for SupplierCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupplierCollectionViaAdvertisement
		{
			get { return _alreadyFetchedSupplierCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedSupplierCollectionViaAdvertisement && !value && (_supplierCollectionViaAdvertisement != null))
				{
					_supplierCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedSupplierCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyCollection SurveyCollectionViaMedium
		{
			get { return GetMultiSurveyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyCollectionViaMedium. When set to true, SurveyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyCollectionViaMedium; }
			set	{ _alwaysFetchSurveyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyCollectionViaMedium already has been fetched. Setting this property to false when SurveyCollectionViaMedium has been fetched
		/// will clear the SurveyCollectionViaMedium collection well. Setting this property to true while SurveyCollectionViaMedium hasn't been fetched disables lazy loading for SurveyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyCollectionViaMedium && !value && (_surveyCollectionViaMedium != null))
				{
					_surveyCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium_
		{
			get { return GetMultiSurveyPageCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium_. When set to true, SurveyPageCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium_
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium_; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium_ already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium_ has been fetched
		/// will clear the SurveyPageCollectionViaMedium_ collection well. Setting this property to true while SurveyPageCollectionViaMedium_ hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium_
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium_ && !value && (_surveyPageCollectionViaMedium_ != null))
				{
					_surveyPageCollectionViaMedium_.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaUITab()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaUITab
		{
			get { return GetMultiUIModeCollectionViaUITab(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaUITab. When set to true, UIModeCollectionViaUITab is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaUITab is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaUITab(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaUITab
		{
			get	{ return _alwaysFetchUIModeCollectionViaUITab; }
			set	{ _alwaysFetchUIModeCollectionViaUITab = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaUITab already has been fetched. Setting this property to false when UIModeCollectionViaUITab has been fetched
		/// will clear the UIModeCollectionViaUITab collection well. Setting this property to true while UIModeCollectionViaUITab hasn't been fetched disables lazy loading for UIModeCollectionViaUITab</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaUITab
		{
			get { return _alreadyFetchedUIModeCollectionViaUITab;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaUITab && !value && (_uIModeCollectionViaUITab != null))
				{
					_uIModeCollectionViaUITab.Clear();
				}
				_alreadyFetchedUIModeCollectionViaUITab = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity ParentCategoryEntity
		{
			get	{ return GetSingleParentCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildCategoryCollection", "ParentCategoryEntity", _parentCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentCategoryEntity. When set to true, ParentCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleParentCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentCategoryEntity
		{
			get	{ return _alwaysFetchParentCategoryEntity; }
			set	{ _alwaysFetchParentCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentCategoryEntity already has been fetched. Setting this property to false when ParentCategoryEntity has been fetched
		/// will set ParentCategoryEntity to null as well. Setting this property to true while ParentCategoryEntity hasn't been fetched disables lazy loading for ParentCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentCategoryEntity
		{
			get { return _alreadyFetchedParentCategoryEntity;}
			set 
			{
				if(_alreadyFetchedParentCategoryEntity && !value)
				{
					this.ParentCategoryEntity = null;
				}
				_alreadyFetchedParentCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentCategoryEntity is not found
		/// in the database. When set to true, ParentCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ParentCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _parentCategoryEntityReturnsNewIfNotFound; }
			set { _parentCategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CategoryCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericcategoryEntity GenericcategoryEntity
		{
			get	{ return GetSingleGenericcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CategoryCollection", "GenericcategoryEntity", _genericcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryEntity. When set to true, GenericcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryEntity
		{
			get	{ return _alwaysFetchGenericcategoryEntity; }
			set	{ _alwaysFetchGenericcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryEntity already has been fetched. Setting this property to false when GenericcategoryEntity has been fetched
		/// will set GenericcategoryEntity to null as well. Setting this property to true while GenericcategoryEntity hasn't been fetched disables lazy loading for GenericcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryEntity
		{
			get { return _alreadyFetchedGenericcategoryEntity;}
			set 
			{
				if(_alreadyFetchedGenericcategoryEntity && !value)
				{
					this.GenericcategoryEntity = null;
				}
				_alreadyFetchedGenericcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericcategoryEntity is not found
		/// in the database. When set to true, GenericcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _genericcategoryEntityReturnsNewIfNotFound; }
			set { _genericcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MenuEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMenuEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MenuEntity MenuEntity
		{
			get	{ return GetSingleMenuEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMenuEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CategoryCollection", "MenuEntity", _menuEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MenuEntity. When set to true, MenuEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuEntity is accessed. You can always execute a forced fetch by calling GetSingleMenuEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuEntity
		{
			get	{ return _alwaysFetchMenuEntity; }
			set	{ _alwaysFetchMenuEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuEntity already has been fetched. Setting this property to false when MenuEntity has been fetched
		/// will set MenuEntity to null as well. Setting this property to true while MenuEntity hasn't been fetched disables lazy loading for MenuEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuEntity
		{
			get { return _alreadyFetchedMenuEntity;}
			set 
			{
				if(_alreadyFetchedMenuEntity && !value)
				{
					this.MenuEntity = null;
				}
				_alreadyFetchedMenuEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MenuEntity is not found
		/// in the database. When set to true, MenuEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MenuEntityReturnsNewIfNotFound
		{
			get	{ return _menuEntityReturnsNewIfNotFound; }
			set { _menuEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PoscategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePoscategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PoscategoryEntity PoscategoryEntity
		{
			get	{ return GetSinglePoscategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPoscategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CategoryCollection", "PoscategoryEntity", _poscategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PoscategoryEntity. When set to true, PoscategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PoscategoryEntity is accessed. You can always execute a forced fetch by calling GetSinglePoscategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPoscategoryEntity
		{
			get	{ return _alwaysFetchPoscategoryEntity; }
			set	{ _alwaysFetchPoscategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PoscategoryEntity already has been fetched. Setting this property to false when PoscategoryEntity has been fetched
		/// will set PoscategoryEntity to null as well. Setting this property to true while PoscategoryEntity hasn't been fetched disables lazy loading for PoscategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPoscategoryEntity
		{
			get { return _alreadyFetchedPoscategoryEntity;}
			set 
			{
				if(_alreadyFetchedPoscategoryEntity && !value)
				{
					this.PoscategoryEntity = null;
				}
				_alreadyFetchedPoscategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PoscategoryEntity is not found
		/// in the database. When set to true, PoscategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PoscategoryEntityReturnsNewIfNotFound
		{
			get	{ return _poscategoryEntityReturnsNewIfNotFound; }
			set { _poscategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CategoryCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRouteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity RouteEntity
		{
			get	{ return GetSingleRouteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRouteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CategoryCollection", "RouteEntity", _routeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RouteEntity. When set to true, RouteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteEntity is accessed. You can always execute a forced fetch by calling GetSingleRouteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteEntity
		{
			get	{ return _alwaysFetchRouteEntity; }
			set	{ _alwaysFetchRouteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteEntity already has been fetched. Setting this property to false when RouteEntity has been fetched
		/// will set RouteEntity to null as well. Setting this property to true while RouteEntity hasn't been fetched disables lazy loading for RouteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteEntity
		{
			get { return _alreadyFetchedRouteEntity;}
			set 
			{
				if(_alreadyFetchedRouteEntity && !value)
				{
					this.RouteEntity = null;
				}
				_alreadyFetchedRouteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RouteEntity is not found
		/// in the database. When set to true, RouteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RouteEntityReturnsNewIfNotFound
		{
			get	{ return _routeEntityReturnsNewIfNotFound; }
			set { _routeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ScheduleEntity ScheduleEntity
		{
			get	{ return GetSingleScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CategoryCollection", "ScheduleEntity", _scheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduleEntity. When set to true, ScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduleEntity is accessed. You can always execute a forced fetch by calling GetSingleScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduleEntity
		{
			get	{ return _alwaysFetchScheduleEntity; }
			set	{ _alwaysFetchScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduleEntity already has been fetched. Setting this property to false when ScheduleEntity has been fetched
		/// will set ScheduleEntity to null as well. Setting this property to true while ScheduleEntity hasn't been fetched disables lazy loading for ScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduleEntity
		{
			get { return _alreadyFetchedScheduleEntity;}
			set 
			{
				if(_alreadyFetchedScheduleEntity && !value)
				{
					this.ScheduleEntity = null;
				}
				_alreadyFetchedScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ScheduleEntity is not found
		/// in the database. When set to true, ScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _scheduleEntityReturnsNewIfNotFound; }
			set { _scheduleEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CategoryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
