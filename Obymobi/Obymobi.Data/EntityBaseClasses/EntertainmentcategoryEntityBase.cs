﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Entertainmentcategory'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class EntertainmentcategoryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "EntertainmentcategoryEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection	_announcementCollection;
		private bool	_alwaysFetchAnnouncementCollection, _alreadyFetchedAnnouncementCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection	_announcementCollection_;
		private bool	_alwaysFetchAnnouncementCollection_, _alreadyFetchedAnnouncementCollection_;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection	_entertainmentCollection;
		private bool	_alwaysFetchEntertainmentCollection, _alreadyFetchedEntertainmentCollection;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection	_entertainmentcategoryLanguageCollection;
		private bool	_alwaysFetchEntertainmentcategoryLanguageCollection, _alreadyFetchedEntertainmentcategoryLanguageCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_actionMediaCollection;
		private bool	_alwaysFetchActionMediaCollection, _alreadyFetchedActionMediaCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementCollection _advertisementCollectionViaMedium;
		private bool	_alwaysFetchAdvertisementCollectionViaMedium, _alreadyFetchedAdvertisementCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationCollection _alterationCollectionViaMedium;
		private bool	_alwaysFetchAlterationCollectionViaMedium, _alreadyFetchedAlterationCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAdvertisement;
		private bool	_alwaysFetchCategoryCollectionViaAdvertisement, _alreadyFetchedCategoryCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium_;
		private bool	_alwaysFetchCategoryCollectionViaMedium_, _alreadyFetchedCategoryCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaAdvertisement;
		private bool	_alwaysFetchCompanyCollectionViaAdvertisement, _alreadyFetchedCompanyCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMedium;
		private bool	_alwaysFetchCompanyCollectionViaMedium, _alreadyFetchedCompanyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaMedium;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaMedium, _alreadyFetchedDeliverypointgroupCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAdvertisement;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAdvertisement, _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAnnouncement;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAnnouncement, _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAnnouncement_;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_, _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement, _alreadyFetchedEntertainmentCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement_;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement_, _alreadyFetchedEntertainmentCollectionViaAdvertisement_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium_;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium_, _alreadyFetchedEntertainmentCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAnnouncement;
		private bool	_alwaysFetchEntertainmentCollectionViaAnnouncement, _alreadyFetchedEntertainmentCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAnnouncement_;
		private bool	_alwaysFetchEntertainmentCollectionViaAnnouncement_, _alreadyFetchedEntertainmentCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection _genericcategoryCollectionViaMedium;
		private bool	_alwaysFetchGenericcategoryCollectionViaMedium, _alreadyFetchedGenericcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaMedium;
		private bool	_alwaysFetchGenericproductCollectionViaMedium, _alreadyFetchedGenericproductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaAdvertisement;
		private bool	_alwaysFetchGenericproductCollectionViaAdvertisement, _alreadyFetchedGenericproductCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.LanguageCollection _languageCollectionViaEntertainmentcategoryLanguage;
		private bool	_alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage, _alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaAnnouncement;
		private bool	_alwaysFetchMediaCollectionViaAnnouncement, _alreadyFetchedMediaCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaAnnouncement_;
		private bool	_alwaysFetchMediaCollectionViaAnnouncement_, _alreadyFetchedMediaCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAnnouncement;
		private bool	_alwaysFetchProductCollectionViaAnnouncement, _alreadyFetchedProductCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAnnouncement_;
		private bool	_alwaysFetchProductCollectionViaAnnouncement_, _alreadyFetchedProductCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium_;
		private bool	_alwaysFetchProductCollectionViaMedium_, _alreadyFetchedProductCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAdvertisement;
		private bool	_alwaysFetchProductCollectionViaAdvertisement, _alreadyFetchedProductCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.SupplierCollection _supplierCollectionViaAdvertisement;
		private bool	_alwaysFetchSupplierCollectionViaAdvertisement, _alreadyFetchedSupplierCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.SurveyCollection _surveyCollectionViaMedium;
		private bool	_alwaysFetchSurveyCollectionViaMedium, _alreadyFetchedSurveyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name AnnouncementCollection</summary>
			public static readonly string AnnouncementCollection = "AnnouncementCollection";
			/// <summary>Member name AnnouncementCollection_</summary>
			public static readonly string AnnouncementCollection_ = "AnnouncementCollection_";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name EntertainmentCollection</summary>
			public static readonly string EntertainmentCollection = "EntertainmentCollection";
			/// <summary>Member name EntertainmentcategoryLanguageCollection</summary>
			public static readonly string EntertainmentcategoryLanguageCollection = "EntertainmentcategoryLanguageCollection";
			/// <summary>Member name ActionMediaCollection</summary>
			public static readonly string ActionMediaCollection = "ActionMediaCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
			/// <summary>Member name AdvertisementCollectionViaMedium</summary>
			public static readonly string AdvertisementCollectionViaMedium = "AdvertisementCollectionViaMedium";
			/// <summary>Member name AlterationCollectionViaMedium</summary>
			public static readonly string AlterationCollectionViaMedium = "AlterationCollectionViaMedium";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaAdvertisement</summary>
			public static readonly string CategoryCollectionViaAdvertisement = "CategoryCollectionViaAdvertisement";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium_</summary>
			public static readonly string CategoryCollectionViaMedium_ = "CategoryCollectionViaMedium_";
			/// <summary>Member name CompanyCollectionViaAdvertisement</summary>
			public static readonly string CompanyCollectionViaAdvertisement = "CompanyCollectionViaAdvertisement";
			/// <summary>Member name CompanyCollectionViaMedium</summary>
			public static readonly string CompanyCollectionViaMedium = "CompanyCollectionViaMedium";
			/// <summary>Member name DeliverypointgroupCollectionViaMedium</summary>
			public static readonly string DeliverypointgroupCollectionViaMedium = "DeliverypointgroupCollectionViaMedium";
			/// <summary>Member name DeliverypointgroupCollectionViaAdvertisement</summary>
			public static readonly string DeliverypointgroupCollectionViaAdvertisement = "DeliverypointgroupCollectionViaAdvertisement";
			/// <summary>Member name DeliverypointgroupCollectionViaAnnouncement</summary>
			public static readonly string DeliverypointgroupCollectionViaAnnouncement = "DeliverypointgroupCollectionViaAnnouncement";
			/// <summary>Member name DeliverypointgroupCollectionViaAnnouncement_</summary>
			public static readonly string DeliverypointgroupCollectionViaAnnouncement_ = "DeliverypointgroupCollectionViaAnnouncement_";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement = "EntertainmentCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement_</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement_ = "EntertainmentCollectionViaAdvertisement_";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium_</summary>
			public static readonly string EntertainmentCollectionViaMedium_ = "EntertainmentCollectionViaMedium_";
			/// <summary>Member name EntertainmentCollectionViaAnnouncement</summary>
			public static readonly string EntertainmentCollectionViaAnnouncement = "EntertainmentCollectionViaAnnouncement";
			/// <summary>Member name EntertainmentCollectionViaAnnouncement_</summary>
			public static readonly string EntertainmentCollectionViaAnnouncement_ = "EntertainmentCollectionViaAnnouncement_";
			/// <summary>Member name GenericcategoryCollectionViaMedium</summary>
			public static readonly string GenericcategoryCollectionViaMedium = "GenericcategoryCollectionViaMedium";
			/// <summary>Member name GenericproductCollectionViaMedium</summary>
			public static readonly string GenericproductCollectionViaMedium = "GenericproductCollectionViaMedium";
			/// <summary>Member name GenericproductCollectionViaAdvertisement</summary>
			public static readonly string GenericproductCollectionViaAdvertisement = "GenericproductCollectionViaAdvertisement";
			/// <summary>Member name LanguageCollectionViaEntertainmentcategoryLanguage</summary>
			public static readonly string LanguageCollectionViaEntertainmentcategoryLanguage = "LanguageCollectionViaEntertainmentcategoryLanguage";
			/// <summary>Member name MediaCollectionViaAnnouncement</summary>
			public static readonly string MediaCollectionViaAnnouncement = "MediaCollectionViaAnnouncement";
			/// <summary>Member name MediaCollectionViaAnnouncement_</summary>
			public static readonly string MediaCollectionViaAnnouncement_ = "MediaCollectionViaAnnouncement_";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaAnnouncement</summary>
			public static readonly string ProductCollectionViaAnnouncement = "ProductCollectionViaAnnouncement";
			/// <summary>Member name ProductCollectionViaAnnouncement_</summary>
			public static readonly string ProductCollectionViaAnnouncement_ = "ProductCollectionViaAnnouncement_";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium_</summary>
			public static readonly string ProductCollectionViaMedium_ = "ProductCollectionViaMedium_";
			/// <summary>Member name ProductCollectionViaAdvertisement</summary>
			public static readonly string ProductCollectionViaAdvertisement = "ProductCollectionViaAdvertisement";
			/// <summary>Member name SupplierCollectionViaAdvertisement</summary>
			public static readonly string SupplierCollectionViaAdvertisement = "SupplierCollectionViaAdvertisement";
			/// <summary>Member name SurveyCollectionViaMedium</summary>
			public static readonly string SurveyCollectionViaMedium = "SurveyCollectionViaMedium";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EntertainmentcategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected EntertainmentcategoryEntityBase() :base("EntertainmentcategoryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		protected EntertainmentcategoryEntityBase(System.Int32 entertainmentcategoryId):base("EntertainmentcategoryEntity")
		{
			InitClassFetch(entertainmentcategoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected EntertainmentcategoryEntityBase(System.Int32 entertainmentcategoryId, IPrefetchPath prefetchPathToUse): base("EntertainmentcategoryEntity")
		{
			InitClassFetch(entertainmentcategoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <param name="validator">The custom validator object for this EntertainmentcategoryEntity</param>
		protected EntertainmentcategoryEntityBase(System.Int32 entertainmentcategoryId, IValidator validator):base("EntertainmentcategoryEntity")
		{
			InitClassFetch(entertainmentcategoryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntertainmentcategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_announcementCollection = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollection", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollection = info.GetBoolean("_alwaysFetchAnnouncementCollection");
			_alreadyFetchedAnnouncementCollection = info.GetBoolean("_alreadyFetchedAnnouncementCollection");

			_announcementCollection_ = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollection_", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollection_ = info.GetBoolean("_alwaysFetchAnnouncementCollection_");
			_alreadyFetchedAnnouncementCollection_ = info.GetBoolean("_alreadyFetchedAnnouncementCollection_");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_entertainmentCollection = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollection", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollection = info.GetBoolean("_alwaysFetchEntertainmentCollection");
			_alreadyFetchedEntertainmentCollection = info.GetBoolean("_alreadyFetchedEntertainmentCollection");

			_entertainmentcategoryLanguageCollection = (Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection)info.GetValue("_entertainmentcategoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection));
			_alwaysFetchEntertainmentcategoryLanguageCollection = info.GetBoolean("_alwaysFetchEntertainmentcategoryLanguageCollection");
			_alreadyFetchedEntertainmentcategoryLanguageCollection = info.GetBoolean("_alreadyFetchedEntertainmentcategoryLanguageCollection");

			_actionMediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_actionMediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchActionMediaCollection = info.GetBoolean("_alwaysFetchActionMediaCollection");
			_alreadyFetchedActionMediaCollection = info.GetBoolean("_alreadyFetchedActionMediaCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_advertisementCollectionViaMedium = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollectionViaMedium = info.GetBoolean("_alwaysFetchAdvertisementCollectionViaMedium");
			_alreadyFetchedAdvertisementCollectionViaMedium = info.GetBoolean("_alreadyFetchedAdvertisementCollectionViaMedium");

			_alterationCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationCollectionViaMedium");
			_alreadyFetchedAlterationCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationCollectionViaMedium");

			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_categoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchCategoryCollectionViaAdvertisement");
			_alreadyFetchedCategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAdvertisement");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_categoryCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium_ = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium_");
			_alreadyFetchedCategoryCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium_");

			_companyCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchCompanyCollectionViaAdvertisement");
			_alreadyFetchedCompanyCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedCompanyCollectionViaAdvertisement");

			_companyCollectionViaMedium = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMedium = info.GetBoolean("_alwaysFetchCompanyCollectionViaMedium");
			_alreadyFetchedCompanyCollectionViaMedium = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMedium");

			_deliverypointgroupCollectionViaMedium = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaMedium");
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaMedium");

			_deliverypointgroupCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement");
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement");

			_deliverypointgroupCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement");
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement");

			_deliverypointgroupCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_");
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_");

			_entertainmentCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement");

			_entertainmentCollectionViaAdvertisement_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement_");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement_");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium_");
			_alreadyFetchedEntertainmentCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium_");

			_entertainmentCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAnnouncement");
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAnnouncement");

			_entertainmentCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAnnouncement_");
			_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAnnouncement_");

			_genericcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericcategoryCollectionViaMedium");
			_alreadyFetchedGenericcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericcategoryCollectionViaMedium");

			_genericproductCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericproductCollectionViaMedium");
			_alreadyFetchedGenericproductCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaMedium");

			_genericproductCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchGenericproductCollectionViaAdvertisement");
			_alreadyFetchedGenericproductCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaAdvertisement");

			_languageCollectionViaEntertainmentcategoryLanguage = (Obymobi.Data.CollectionClasses.LanguageCollection)info.GetValue("_languageCollectionViaEntertainmentcategoryLanguage", typeof(Obymobi.Data.CollectionClasses.LanguageCollection));
			_alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage = info.GetBoolean("_alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage");
			_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage = info.GetBoolean("_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage");

			_mediaCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchMediaCollectionViaAnnouncement");
			_alreadyFetchedMediaCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedMediaCollectionViaAnnouncement");

			_mediaCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchMediaCollectionViaAnnouncement_");
			_alreadyFetchedMediaCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedMediaCollectionViaAnnouncement_");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_productCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchProductCollectionViaAnnouncement");
			_alreadyFetchedProductCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedProductCollectionViaAnnouncement");

			_productCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchProductCollectionViaAnnouncement_");
			_alreadyFetchedProductCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedProductCollectionViaAnnouncement_");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_productCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium_ = info.GetBoolean("_alwaysFetchProductCollectionViaMedium_");
			_alreadyFetchedProductCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium_");

			_productCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchProductCollectionViaAdvertisement");
			_alreadyFetchedProductCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedProductCollectionViaAdvertisement");

			_supplierCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.SupplierCollection)info.GetValue("_supplierCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.SupplierCollection));
			_alwaysFetchSupplierCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchSupplierCollectionViaAdvertisement");
			_alreadyFetchedSupplierCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedSupplierCollectionViaAdvertisement");

			_surveyCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyCollection)info.GetValue("_surveyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyCollection));
			_alwaysFetchSurveyCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyCollectionViaMedium");
			_alreadyFetchedSurveyCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyCollectionViaMedium");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedAnnouncementCollection = (_announcementCollection.Count > 0);
			_alreadyFetchedAnnouncementCollection_ = (_announcementCollection_.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedEntertainmentCollection = (_entertainmentCollection.Count > 0);
			_alreadyFetchedEntertainmentcategoryLanguageCollection = (_entertainmentcategoryLanguageCollection.Count > 0);
			_alreadyFetchedActionMediaCollection = (_actionMediaCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedAdvertisementCollectionViaMedium = (_advertisementCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationCollectionViaMedium = (_alterationCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaAdvertisement = (_categoryCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium_ = (_categoryCollectionViaMedium_.Count > 0);
			_alreadyFetchedCompanyCollectionViaAdvertisement = (_companyCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedCompanyCollectionViaMedium = (_companyCollectionViaMedium.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = (_deliverypointgroupCollectionViaMedium.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = (_deliverypointgroupCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = (_deliverypointgroupCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = (_deliverypointgroupCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = (_entertainmentCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = (_entertainmentCollectionViaAdvertisement_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium_ = (_entertainmentCollectionViaMedium_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = (_entertainmentCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = (_entertainmentCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedGenericcategoryCollectionViaMedium = (_genericcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericproductCollectionViaMedium = (_genericproductCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericproductCollectionViaAdvertisement = (_genericproductCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage = (_languageCollectionViaEntertainmentcategoryLanguage.Count > 0);
			_alreadyFetchedMediaCollectionViaAnnouncement = (_mediaCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedMediaCollectionViaAnnouncement_ = (_mediaCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaAnnouncement = (_productCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedProductCollectionViaAnnouncement_ = (_productCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium_ = (_productCollectionViaMedium_.Count > 0);
			_alreadyFetchedProductCollectionViaAdvertisement = (_productCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedSupplierCollectionViaAdvertisement = (_supplierCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedSurveyCollectionViaMedium = (_surveyCollectionViaMedium.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId);
					break;
				case "AnnouncementCollection":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesEntertainmentCategory);
					break;
				case "AnnouncementCollection_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoEntertainmentCategory);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingEntertainmentcategoryId);
					break;
				case "EntertainmentCollection":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentcategoryId);
					break;
				case "EntertainmentcategoryLanguageCollection":
					toReturn.Add(Relations.EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryId);
					break;
				case "ActionMediaCollection":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingEntertainmentcategoryId);
					break;
				case "AdvertisementCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.CategoryEntityUsingActionCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.CompanyEntityUsingCompanyId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CompanyEntityUsingCompanyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Media_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement_":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentEntityUsingOnYesEntertainment, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentEntityUsingOnYesEntertainment, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "GenericcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericcategoryEntityUsingGenericcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericproductEntityUsingGenericproductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.GenericproductEntityUsingGenericproductId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "LanguageCollectionViaEntertainmentcategoryLanguage":
					toReturn.Add(Relations.EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryId, "EntertainmentcategoryEntity__", "EntertainmentcategoryLanguage_", JoinHint.None);
					toReturn.Add(EntertainmentcategoryLanguageEntity.Relations.LanguageEntityUsingLanguageId, "EntertainmentcategoryLanguage_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.MediaEntityUsingMediaId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.MediaEntityUsingMediaId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingOnYesEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.ProductEntityUsingOnYesProduct, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingOnNoEntertainmentCategory, "EntertainmentcategoryEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.ProductEntityUsingOnYesProduct, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.ProductEntityUsingProductId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "SupplierCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingActionEntertainmentCategoryId, "EntertainmentcategoryEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.SupplierEntityUsingSupplierId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "SurveyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingActionEntertainmentcategoryId, "EntertainmentcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_announcementCollection", (!this.MarkedForDeletion?_announcementCollection:null));
			info.AddValue("_alwaysFetchAnnouncementCollection", _alwaysFetchAnnouncementCollection);
			info.AddValue("_alreadyFetchedAnnouncementCollection", _alreadyFetchedAnnouncementCollection);
			info.AddValue("_announcementCollection_", (!this.MarkedForDeletion?_announcementCollection_:null));
			info.AddValue("_alwaysFetchAnnouncementCollection_", _alwaysFetchAnnouncementCollection_);
			info.AddValue("_alreadyFetchedAnnouncementCollection_", _alreadyFetchedAnnouncementCollection_);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_entertainmentCollection", (!this.MarkedForDeletion?_entertainmentCollection:null));
			info.AddValue("_alwaysFetchEntertainmentCollection", _alwaysFetchEntertainmentCollection);
			info.AddValue("_alreadyFetchedEntertainmentCollection", _alreadyFetchedEntertainmentCollection);
			info.AddValue("_entertainmentcategoryLanguageCollection", (!this.MarkedForDeletion?_entertainmentcategoryLanguageCollection:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryLanguageCollection", _alwaysFetchEntertainmentcategoryLanguageCollection);
			info.AddValue("_alreadyFetchedEntertainmentcategoryLanguageCollection", _alreadyFetchedEntertainmentcategoryLanguageCollection);
			info.AddValue("_actionMediaCollection", (!this.MarkedForDeletion?_actionMediaCollection:null));
			info.AddValue("_alwaysFetchActionMediaCollection", _alwaysFetchActionMediaCollection);
			info.AddValue("_alreadyFetchedActionMediaCollection", _alreadyFetchedActionMediaCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_advertisementCollectionViaMedium", (!this.MarkedForDeletion?_advertisementCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAdvertisementCollectionViaMedium", _alwaysFetchAdvertisementCollectionViaMedium);
			info.AddValue("_alreadyFetchedAdvertisementCollectionViaMedium", _alreadyFetchedAdvertisementCollectionViaMedium);
			info.AddValue("_alterationCollectionViaMedium", (!this.MarkedForDeletion?_alterationCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationCollectionViaMedium", _alwaysFetchAlterationCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationCollectionViaMedium", _alreadyFetchedAlterationCollectionViaMedium);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_categoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_categoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAdvertisement", _alwaysFetchCategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAdvertisement", _alreadyFetchedCategoryCollectionViaAdvertisement);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium_", (!this.MarkedForDeletion?_categoryCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium_", _alwaysFetchCategoryCollectionViaMedium_);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium_", _alreadyFetchedCategoryCollectionViaMedium_);
			info.AddValue("_companyCollectionViaAdvertisement", (!this.MarkedForDeletion?_companyCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaAdvertisement", _alwaysFetchCompanyCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedCompanyCollectionViaAdvertisement", _alreadyFetchedCompanyCollectionViaAdvertisement);
			info.AddValue("_companyCollectionViaMedium", (!this.MarkedForDeletion?_companyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMedium", _alwaysFetchCompanyCollectionViaMedium);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMedium", _alreadyFetchedCompanyCollectionViaMedium);
			info.AddValue("_deliverypointgroupCollectionViaMedium", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaMedium:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaMedium", _alwaysFetchDeliverypointgroupCollectionViaMedium);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaMedium", _alreadyFetchedDeliverypointgroupCollectionViaMedium);
			info.AddValue("_deliverypointgroupCollectionViaAdvertisement", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement", _alwaysFetchDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement", _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_deliverypointgroupCollectionViaAnnouncement", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement", _alwaysFetchDeliverypointgroupCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement", _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement);
			info.AddValue("_deliverypointgroupCollectionViaAnnouncement_", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAnnouncement_", _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_", _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_);
			info.AddValue("_entertainmentCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement", _alwaysFetchEntertainmentCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement", _alreadyFetchedEntertainmentCollectionViaAdvertisement);
			info.AddValue("_entertainmentCollectionViaAdvertisement_", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement_", _alwaysFetchEntertainmentCollectionViaAdvertisement_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement_", _alreadyFetchedEntertainmentCollectionViaAdvertisement_);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium_", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium_", _alwaysFetchEntertainmentCollectionViaMedium_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium_", _alreadyFetchedEntertainmentCollectionViaMedium_);
			info.AddValue("_entertainmentCollectionViaAnnouncement", (!this.MarkedForDeletion?_entertainmentCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAnnouncement", _alwaysFetchEntertainmentCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAnnouncement", _alreadyFetchedEntertainmentCollectionViaAnnouncement);
			info.AddValue("_entertainmentCollectionViaAnnouncement_", (!this.MarkedForDeletion?_entertainmentCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAnnouncement_", _alwaysFetchEntertainmentCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAnnouncement_", _alreadyFetchedEntertainmentCollectionViaAnnouncement_);
			info.AddValue("_genericcategoryCollectionViaMedium", (!this.MarkedForDeletion?_genericcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericcategoryCollectionViaMedium", _alwaysFetchGenericcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericcategoryCollectionViaMedium", _alreadyFetchedGenericcategoryCollectionViaMedium);
			info.AddValue("_genericproductCollectionViaMedium", (!this.MarkedForDeletion?_genericproductCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaMedium", _alwaysFetchGenericproductCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaMedium", _alreadyFetchedGenericproductCollectionViaMedium);
			info.AddValue("_genericproductCollectionViaAdvertisement", (!this.MarkedForDeletion?_genericproductCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaAdvertisement", _alwaysFetchGenericproductCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaAdvertisement", _alreadyFetchedGenericproductCollectionViaAdvertisement);
			info.AddValue("_languageCollectionViaEntertainmentcategoryLanguage", (!this.MarkedForDeletion?_languageCollectionViaEntertainmentcategoryLanguage:null));
			info.AddValue("_alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage", _alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage);
			info.AddValue("_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage", _alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage);
			info.AddValue("_mediaCollectionViaAnnouncement", (!this.MarkedForDeletion?_mediaCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchMediaCollectionViaAnnouncement", _alwaysFetchMediaCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedMediaCollectionViaAnnouncement", _alreadyFetchedMediaCollectionViaAnnouncement);
			info.AddValue("_mediaCollectionViaAnnouncement_", (!this.MarkedForDeletion?_mediaCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchMediaCollectionViaAnnouncement_", _alwaysFetchMediaCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedMediaCollectionViaAnnouncement_", _alreadyFetchedMediaCollectionViaAnnouncement_);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_productCollectionViaAnnouncement", (!this.MarkedForDeletion?_productCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchProductCollectionViaAnnouncement", _alwaysFetchProductCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedProductCollectionViaAnnouncement", _alreadyFetchedProductCollectionViaAnnouncement);
			info.AddValue("_productCollectionViaAnnouncement_", (!this.MarkedForDeletion?_productCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchProductCollectionViaAnnouncement_", _alwaysFetchProductCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedProductCollectionViaAnnouncement_", _alreadyFetchedProductCollectionViaAnnouncement_);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium_", (!this.MarkedForDeletion?_productCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium_", _alwaysFetchProductCollectionViaMedium_);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium_", _alreadyFetchedProductCollectionViaMedium_);
			info.AddValue("_productCollectionViaAdvertisement", (!this.MarkedForDeletion?_productCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchProductCollectionViaAdvertisement", _alwaysFetchProductCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedProductCollectionViaAdvertisement", _alreadyFetchedProductCollectionViaAdvertisement);
			info.AddValue("_supplierCollectionViaAdvertisement", (!this.MarkedForDeletion?_supplierCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchSupplierCollectionViaAdvertisement", _alwaysFetchSupplierCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedSupplierCollectionViaAdvertisement", _alreadyFetchedSupplierCollectionViaAdvertisement);
			info.AddValue("_surveyCollectionViaMedium", (!this.MarkedForDeletion?_surveyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyCollectionViaMedium", _alwaysFetchSurveyCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyCollectionViaMedium", _alreadyFetchedSurveyCollectionViaMedium);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "AnnouncementCollection":
					_alreadyFetchedAnnouncementCollection = true;
					if(entity!=null)
					{
						this.AnnouncementCollection.Add((AnnouncementEntity)entity);
					}
					break;
				case "AnnouncementCollection_":
					_alreadyFetchedAnnouncementCollection_ = true;
					if(entity!=null)
					{
						this.AnnouncementCollection_.Add((AnnouncementEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "EntertainmentCollection":
					_alreadyFetchedEntertainmentCollection = true;
					if(entity!=null)
					{
						this.EntertainmentCollection.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryLanguageCollection":
					_alreadyFetchedEntertainmentcategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryLanguageCollection.Add((EntertainmentcategoryLanguageEntity)entity);
					}
					break;
				case "ActionMediaCollection":
					_alreadyFetchedActionMediaCollection = true;
					if(entity!=null)
					{
						this.ActionMediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				case "AdvertisementCollectionViaMedium":
					_alreadyFetchedAdvertisementCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AdvertisementCollectionViaMedium.Add((AdvertisementEntity)entity);
					}
					break;
				case "AlterationCollectionViaMedium":
					_alreadyFetchedAlterationCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationCollectionViaMedium.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaAdvertisement":
					_alreadyFetchedCategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAdvertisement.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium_":
					_alreadyFetchedCategoryCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium_.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaAdvertisement":
					_alreadyFetchedCompanyCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaAdvertisement.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaMedium":
					_alreadyFetchedCompanyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMedium.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaMedium":
					_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaMedium.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAdvertisement.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAnnouncement":
					_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAnnouncement.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAnnouncement_":
					_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAnnouncement_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement_":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium_":
					_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAnnouncement":
					_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAnnouncement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAnnouncement_":
					_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAnnouncement_.Add((EntertainmentEntity)entity);
					}
					break;
				case "GenericcategoryCollectionViaMedium":
					_alreadyFetchedGenericcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericcategoryCollectionViaMedium.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericproductCollectionViaMedium":
					_alreadyFetchedGenericproductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaMedium.Add((GenericproductEntity)entity);
					}
					break;
				case "GenericproductCollectionViaAdvertisement":
					_alreadyFetchedGenericproductCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaAdvertisement.Add((GenericproductEntity)entity);
					}
					break;
				case "LanguageCollectionViaEntertainmentcategoryLanguage":
					_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage = true;
					if(entity!=null)
					{
						this.LanguageCollectionViaEntertainmentcategoryLanguage.Add((LanguageEntity)entity);
					}
					break;
				case "MediaCollectionViaAnnouncement":
					_alreadyFetchedMediaCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.MediaCollectionViaAnnouncement.Add((MediaEntity)entity);
					}
					break;
				case "MediaCollectionViaAnnouncement_":
					_alreadyFetchedMediaCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.MediaCollectionViaAnnouncement_.Add((MediaEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaAnnouncement":
					_alreadyFetchedProductCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAnnouncement.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaAnnouncement_":
					_alreadyFetchedProductCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAnnouncement_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium_":
					_alreadyFetchedProductCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaAdvertisement":
					_alreadyFetchedProductCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAdvertisement.Add((ProductEntity)entity);
					}
					break;
				case "SupplierCollectionViaAdvertisement":
					_alreadyFetchedSupplierCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.SupplierCollectionViaAdvertisement.Add((SupplierEntity)entity);
					}
					break;
				case "SurveyCollectionViaMedium":
					_alreadyFetchedSurveyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyCollectionViaMedium.Add((SurveyEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "AnnouncementCollection":
					_announcementCollection.Add((AnnouncementEntity)relatedEntity);
					break;
				case "AnnouncementCollection_":
					_announcementCollection_.Add((AnnouncementEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "EntertainmentCollection":
					_entertainmentCollection.Add((EntertainmentEntity)relatedEntity);
					break;
				case "EntertainmentcategoryLanguageCollection":
					_entertainmentcategoryLanguageCollection.Add((EntertainmentcategoryLanguageEntity)relatedEntity);
					break;
				case "ActionMediaCollection":
					_actionMediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AnnouncementCollection":
					this.PerformRelatedEntityRemoval(_announcementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AnnouncementCollection_":
					this.PerformRelatedEntityRemoval(_announcementCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EntertainmentCollection":
					this.PerformRelatedEntityRemoval(_entertainmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EntertainmentcategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_entertainmentcategoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ActionMediaCollection":
					this.PerformRelatedEntityRemoval(_actionMediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_announcementCollection);
			toReturn.Add(_announcementCollection_);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_entertainmentCollection);
			toReturn.Add(_entertainmentcategoryLanguageCollection);
			toReturn.Add(_actionMediaCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryId)
		{
			return FetchUsingPK(entertainmentcategoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entertainmentcategoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entertainmentcategoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entertainmentcategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntertainmentcategoryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EntertainmentcategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementCollection || forceFetch || _alwaysFetchAnnouncementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollection);
				_announcementCollection.SuppressClearInGetMulti=!forceFetch;
				_announcementCollection.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, filter);
				_announcementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollection = true;
			}
			return _announcementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollection'. These settings will be taken into account
		/// when the property AnnouncementCollection is requested or GetMultiAnnouncementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollection.SortClauses=sortClauses;
			_announcementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch)
		{
			return GetMultiAnnouncementCollection_(forceFetch, _announcementCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementCollection_(forceFetch, _announcementCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementCollection_ || forceFetch || _alwaysFetchAnnouncementCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollection_);
				_announcementCollection_.SuppressClearInGetMulti=!forceFetch;
				_announcementCollection_.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollection_.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, filter);
				_announcementCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollection_ = true;
			}
			return _announcementCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollection_'. These settings will be taken into account
		/// when the property AnnouncementCollection_ is requested or GetMultiAnnouncementCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollection_.SortClauses=sortClauses;
			_announcementCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch)
		{
			return GetMultiEntertainmentCollection(forceFetch, _entertainmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntertainmentCollection(forceFetch, _entertainmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntertainmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntertainmentCollection || forceFetch || _alwaysFetchEntertainmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollection);
				_entertainmentCollection.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollection.GetMultiManyToOne(null, this, null, null, null, filter);
				_entertainmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollection = true;
			}
			return _entertainmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollection'. These settings will be taken into account
		/// when the property EntertainmentCollection is requested or GetMultiEntertainmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollection.SortClauses=sortClauses;
			_entertainmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryLanguageCollection(forceFetch, _entertainmentcategoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntertainmentcategoryLanguageCollection(forceFetch, _entertainmentcategoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntertainmentcategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryLanguageCollection || forceFetch || _alwaysFetchEntertainmentcategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryLanguageCollection);
				_entertainmentcategoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryLanguageCollection.GetMultiManyToOne(this, null, filter);
				_entertainmentcategoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryLanguageCollection = true;
			}
			return _entertainmentcategoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryLanguageCollection'. These settings will be taken into account
		/// when the property EntertainmentcategoryLanguageCollection is requested or GetMultiEntertainmentcategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryLanguageCollection.SortClauses=sortClauses;
			_entertainmentcategoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch)
		{
			return GetMultiActionMediaCollection(forceFetch, _actionMediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiActionMediaCollection(forceFetch, _actionMediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiActionMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiActionMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedActionMediaCollection || forceFetch || _alwaysFetchActionMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_actionMediaCollection);
				_actionMediaCollection.SuppressClearInGetMulti=!forceFetch;
				_actionMediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_actionMediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_actionMediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedActionMediaCollection = true;
			}
			return _actionMediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ActionMediaCollection'. These settings will be taken into account
		/// when the property ActionMediaCollection is requested or GetMultiActionMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersActionMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_actionMediaCollection.SortClauses=sortClauses;
			_actionMediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAdvertisementCollectionViaMedium(forceFetch, _advertisementCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAdvertisementCollectionViaMedium || forceFetch || _alwaysFetchAdvertisementCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollectionViaMedium.GetMulti(filter, GetRelationsForField("AdvertisementCollectionViaMedium"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollectionViaMedium = true;
			}
			return _advertisementCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollectionViaMedium'. These settings will be taken into account
		/// when the property AdvertisementCollectionViaMedium is requested or GetMultiAdvertisementCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollectionViaMedium.SortClauses=sortClauses;
			_advertisementCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationCollectionViaMedium(forceFetch, _alterationCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationCollectionViaMedium || forceFetch || _alwaysFetchAlterationCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationCollectionViaMedium"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollectionViaMedium = true;
			}
			return _alterationCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationCollectionViaMedium is requested or GetMultiAlterationCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollectionViaMedium.SortClauses=sortClauses;
			_alterationCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAdvertisement(forceFetch, _categoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchCategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAdvertisement"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAdvertisement = true;
			}
			return _categoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property CategoryCollectionViaAdvertisement is requested or GetMultiCategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_categoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium_(forceFetch, _categoryCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium_ || forceFetch || _alwaysFetchCategoryCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium_.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium_"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium_ = true;
			}
			return _categoryCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium_'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium_ is requested or GetMultiCategoryCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium_.SortClauses=sortClauses;
			_categoryCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaAdvertisement(forceFetch, _companyCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaAdvertisement || forceFetch || _alwaysFetchCompanyCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_companyCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("CompanyCollectionViaAdvertisement"));
				_companyCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaAdvertisement = true;
			}
			return _companyCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property CompanyCollectionViaAdvertisement is requested or GetMultiCompanyCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaAdvertisement.SortClauses=sortClauses;
			_companyCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMedium(forceFetch, _companyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMedium || forceFetch || _alwaysFetchCompanyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMedium.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMedium"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMedium = true;
			}
			return _companyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMedium'. These settings will be taken into account
		/// when the property CompanyCollectionViaMedium is requested or GetMultiCompanyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMedium.SortClauses=sortClauses;
			_companyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaMedium(forceFetch, _deliverypointgroupCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaMedium || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaMedium.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaMedium"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
			}
			return _deliverypointgroupCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaMedium'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaMedium is requested or GetMultiDeliverypointgroupCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaMedium.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAdvertisement(forceFetch, _deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
			}
			return _deliverypointgroupCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAdvertisement is requested or GetMultiDeliverypointgroupCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAdvertisement.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAnnouncement(forceFetch, _deliverypointgroupCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_deliverypointgroupCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement"));
				_deliverypointgroupCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = true;
			}
			return _deliverypointgroupCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAnnouncement is requested or GetMultiDeliverypointgroupCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAnnouncement.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAnnouncement_(forceFetch, _deliverypointgroupCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_deliverypointgroupCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement_"));
				_deliverypointgroupCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = true;
			}
			return _deliverypointgroupCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAnnouncement_ is requested or GetMultiDeliverypointgroupCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAnnouncement_.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement(forceFetch, _entertainmentCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
			}
			return _entertainmentCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement is requested or GetMultiEntertainmentCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement_(forceFetch, _entertainmentCollectionViaAdvertisement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement_ || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_entertainmentCollectionViaAdvertisement_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement_"));
				_entertainmentCollectionViaAdvertisement_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = true;
			}
			return _entertainmentCollectionViaAdvertisement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement_ is requested or GetMultiEntertainmentCollectionViaAdvertisement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement_.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium_(forceFetch, _entertainmentCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium_ || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium_"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
			}
			return _entertainmentCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium_ is requested or GetMultiEntertainmentCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium_.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAnnouncement(forceFetch, _entertainmentCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAnnouncement || forceFetch || _alwaysFetchEntertainmentCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAnnouncement"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
			}
			return _entertainmentCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAnnouncement is requested or GetMultiEntertainmentCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAnnouncement.SortClauses=sortClauses;
			_entertainmentCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAnnouncement_(forceFetch, _entertainmentCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAnnouncement_ || forceFetch || _alwaysFetchEntertainmentCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_entertainmentCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAnnouncement_"));
				_entertainmentCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = true;
			}
			return _entertainmentCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAnnouncement_ is requested or GetMultiEntertainmentCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAnnouncement_.SortClauses=sortClauses;
			_entertainmentCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericcategoryCollectionViaMedium(forceFetch, _genericcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollectionViaMedium || forceFetch || _alwaysFetchGenericcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericcategoryCollectionViaMedium"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollectionViaMedium = true;
			}
			return _genericcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericcategoryCollectionViaMedium is requested or GetMultiGenericcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollectionViaMedium.SortClauses=sortClauses;
			_genericcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaMedium(forceFetch, _genericproductCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaMedium || forceFetch || _alwaysFetchGenericproductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaMedium"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaMedium = true;
			}
			return _genericproductCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericproductCollectionViaMedium is requested or GetMultiGenericproductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaMedium.SortClauses=sortClauses;
			_genericproductCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaAdvertisement(forceFetch, _genericproductCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaAdvertisement || forceFetch || _alwaysFetchGenericproductCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_genericproductCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaAdvertisement"));
				_genericproductCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaAdvertisement = true;
			}
			return _genericproductCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property GenericproductCollectionViaAdvertisement is requested or GetMultiGenericproductCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaAdvertisement.SortClauses=sortClauses;
			_genericproductCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaEntertainmentcategoryLanguage(bool forceFetch)
		{
			return GetMultiLanguageCollectionViaEntertainmentcategoryLanguage(forceFetch, _languageCollectionViaEntertainmentcategoryLanguage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaEntertainmentcategoryLanguage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage || forceFetch || _alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_languageCollectionViaEntertainmentcategoryLanguage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_languageCollectionViaEntertainmentcategoryLanguage.SuppressClearInGetMulti=!forceFetch;
				_languageCollectionViaEntertainmentcategoryLanguage.EntityFactoryToUse = entityFactoryToUse;
				_languageCollectionViaEntertainmentcategoryLanguage.GetMulti(filter, GetRelationsForField("LanguageCollectionViaEntertainmentcategoryLanguage"));
				_languageCollectionViaEntertainmentcategoryLanguage.SuppressClearInGetMulti=false;
				_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage = true;
			}
			return _languageCollectionViaEntertainmentcategoryLanguage;
		}

		/// <summary> Sets the collection parameters for the collection for 'LanguageCollectionViaEntertainmentcategoryLanguage'. These settings will be taken into account
		/// when the property LanguageCollectionViaEntertainmentcategoryLanguage is requested or GetMultiLanguageCollectionViaEntertainmentcategoryLanguage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLanguageCollectionViaEntertainmentcategoryLanguage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_languageCollectionViaEntertainmentcategoryLanguage.SortClauses=sortClauses;
			_languageCollectionViaEntertainmentcategoryLanguage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiMediaCollectionViaAnnouncement(forceFetch, _mediaCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaAnnouncement || forceFetch || _alwaysFetchMediaCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_mediaCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("MediaCollectionViaAnnouncement"));
				_mediaCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaAnnouncement = true;
			}
			return _mediaCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property MediaCollectionViaAnnouncement is requested or GetMultiMediaCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaAnnouncement.SortClauses=sortClauses;
			_mediaCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiMediaCollectionViaAnnouncement_(forceFetch, _mediaCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaAnnouncement_ || forceFetch || _alwaysFetchMediaCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_mediaCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("MediaCollectionViaAnnouncement_"));
				_mediaCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaAnnouncement_ = true;
			}
			return _mediaCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property MediaCollectionViaAnnouncement_ is requested or GetMultiMediaCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaAnnouncement_.SortClauses=sortClauses;
			_mediaCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiProductCollectionViaAnnouncement(forceFetch, _productCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAnnouncement || forceFetch || _alwaysFetchProductCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("ProductCollectionViaAnnouncement"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAnnouncement = true;
			}
			return _productCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property ProductCollectionViaAnnouncement is requested or GetMultiProductCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAnnouncement.SortClauses=sortClauses;
			_productCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiProductCollectionViaAnnouncement_(forceFetch, _productCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAnnouncement_ || forceFetch || _alwaysFetchProductCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_productCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("ProductCollectionViaAnnouncement_"));
				_productCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAnnouncement_ = true;
			}
			return _productCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property ProductCollectionViaAnnouncement_ is requested or GetMultiProductCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAnnouncement_.SortClauses=sortClauses;
			_productCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium_(forceFetch, _productCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium_ || forceFetch || _alwaysFetchProductCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium_.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium_"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium_ = true;
			}
			return _productCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium_'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium_ is requested or GetMultiProductCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium_.SortClauses=sortClauses;
			_productCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiProductCollectionViaAdvertisement(forceFetch, _productCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAdvertisement || forceFetch || _alwaysFetchProductCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_productCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("ProductCollectionViaAdvertisement"));
				_productCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAdvertisement = true;
			}
			return _productCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property ProductCollectionViaAdvertisement is requested or GetMultiProductCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAdvertisement.SortClauses=sortClauses;
			_productCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupplierEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupplierCollection GetMultiSupplierCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiSupplierCollectionViaAdvertisement(forceFetch, _supplierCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupplierCollection GetMultiSupplierCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupplierCollectionViaAdvertisement || forceFetch || _alwaysFetchSupplierCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supplierCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_supplierCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_supplierCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_supplierCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("SupplierCollectionViaAdvertisement"));
				_supplierCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedSupplierCollectionViaAdvertisement = true;
			}
			return _supplierCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupplierCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property SupplierCollectionViaAdvertisement is requested or GetMultiSupplierCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupplierCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supplierCollectionViaAdvertisement.SortClauses=sortClauses;
			_supplierCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyCollectionViaMedium(forceFetch, _surveyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyCollectionViaMedium || forceFetch || _alwaysFetchSurveyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyCollectionViaMedium"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyCollectionViaMedium = true;
			}
			return _surveyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyCollectionViaMedium is requested or GetMultiSurveyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyCollectionViaMedium.SortClauses=sortClauses;
			_surveyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(EntertainmentcategoryFields.EntertainmentcategoryId, ComparisonOperator.Equal, this.EntertainmentcategoryId, "EntertainmentcategoryEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("AnnouncementCollection", _announcementCollection);
			toReturn.Add("AnnouncementCollection_", _announcementCollection_);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("EntertainmentCollection", _entertainmentCollection);
			toReturn.Add("EntertainmentcategoryLanguageCollection", _entertainmentcategoryLanguageCollection);
			toReturn.Add("ActionMediaCollection", _actionMediaCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			toReturn.Add("AdvertisementCollectionViaMedium", _advertisementCollectionViaMedium);
			toReturn.Add("AlterationCollectionViaMedium", _alterationCollectionViaMedium);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaAdvertisement", _categoryCollectionViaAdvertisement);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium_", _categoryCollectionViaMedium_);
			toReturn.Add("CompanyCollectionViaAdvertisement", _companyCollectionViaAdvertisement);
			toReturn.Add("CompanyCollectionViaMedium", _companyCollectionViaMedium);
			toReturn.Add("DeliverypointgroupCollectionViaMedium", _deliverypointgroupCollectionViaMedium);
			toReturn.Add("DeliverypointgroupCollectionViaAdvertisement", _deliverypointgroupCollectionViaAdvertisement);
			toReturn.Add("DeliverypointgroupCollectionViaAnnouncement", _deliverypointgroupCollectionViaAnnouncement);
			toReturn.Add("DeliverypointgroupCollectionViaAnnouncement_", _deliverypointgroupCollectionViaAnnouncement_);
			toReturn.Add("EntertainmentCollectionViaAdvertisement", _entertainmentCollectionViaAdvertisement);
			toReturn.Add("EntertainmentCollectionViaAdvertisement_", _entertainmentCollectionViaAdvertisement_);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium_", _entertainmentCollectionViaMedium_);
			toReturn.Add("EntertainmentCollectionViaAnnouncement", _entertainmentCollectionViaAnnouncement);
			toReturn.Add("EntertainmentCollectionViaAnnouncement_", _entertainmentCollectionViaAnnouncement_);
			toReturn.Add("GenericcategoryCollectionViaMedium", _genericcategoryCollectionViaMedium);
			toReturn.Add("GenericproductCollectionViaMedium", _genericproductCollectionViaMedium);
			toReturn.Add("GenericproductCollectionViaAdvertisement", _genericproductCollectionViaAdvertisement);
			toReturn.Add("LanguageCollectionViaEntertainmentcategoryLanguage", _languageCollectionViaEntertainmentcategoryLanguage);
			toReturn.Add("MediaCollectionViaAnnouncement", _mediaCollectionViaAnnouncement);
			toReturn.Add("MediaCollectionViaAnnouncement_", _mediaCollectionViaAnnouncement_);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("ProductCollectionViaAnnouncement", _productCollectionViaAnnouncement);
			toReturn.Add("ProductCollectionViaAnnouncement_", _productCollectionViaAnnouncement_);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium_", _productCollectionViaMedium_);
			toReturn.Add("ProductCollectionViaAdvertisement", _productCollectionViaAdvertisement);
			toReturn.Add("SupplierCollectionViaAdvertisement", _supplierCollectionViaAdvertisement);
			toReturn.Add("SurveyCollectionViaMedium", _surveyCollectionViaMedium);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <param name="validator">The validator object for this EntertainmentcategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 entertainmentcategoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entertainmentcategoryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "ActionEntertainmentcategoryEntity");

			_announcementCollection = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollection.SetContainingEntityInfo(this, "EntertainmentcategoryEntity");

			_announcementCollection_ = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollection_.SetContainingEntityInfo(this, "EntertainmentcategoryEntity_");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "EntertainmentcategoryEntity");

			_entertainmentCollection = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollection.SetContainingEntityInfo(this, "EntertainmentcategoryEntity");

			_entertainmentcategoryLanguageCollection = new Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection();
			_entertainmentcategoryLanguageCollection.SetContainingEntityInfo(this, "EntertainmentcategoryEntity");

			_actionMediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_actionMediaCollection.SetContainingEntityInfo(this, "ActionEntertainmentcategoryEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "EntertainmentcategoryEntity");
			_advertisementCollectionViaMedium = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_alterationCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaMedium = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointgroupCollectionViaMedium = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAdvertisement_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_genericcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericproductCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_genericproductCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_languageCollectionViaEntertainmentcategoryLanguage = new Obymobi.Data.CollectionClasses.LanguageCollection();
			_mediaCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.MediaCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.ProductCollection();
			_supplierCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.SupplierCollection();
			_surveyCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entertainmentcategoryId">PK value for Entertainmentcategory which data should be fetched into this Entertainmentcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 entertainmentcategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EntertainmentcategoryFieldIndex.EntertainmentcategoryId].ForcedCurrentValueWrite(entertainmentcategoryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEntertainmentcategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EntertainmentcategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EntertainmentcategoryRelations Relations
		{
			get	{ return new EntertainmentcategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementCollection_")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntertainmentcategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("EntertainmentcategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity, 0, null, null, null, "EntertainmentcategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("ActionMediaCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "ActionMediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, GetRelationsForField("AdvertisementCollectionViaMedium"), "AdvertisementCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, GetRelationsForField("AlterationCollectionViaMedium"), "AlterationCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAdvertisement"), "CategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium_"), "CategoryCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaAdvertisement"), "CompanyCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMedium"), "CompanyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaMedium"), "DeliverypointgroupCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"), "DeliverypointgroupCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement"), "DeliverypointgroupCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAnnouncement_"), "DeliverypointgroupCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement"), "EntertainmentCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement_"), "EntertainmentCollectionViaAdvertisement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium_"), "EntertainmentCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAnnouncement"), "EntertainmentCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAnnouncement_"), "EntertainmentCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, GetRelationsForField("GenericcategoryCollectionViaMedium"), "GenericcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaMedium"), "GenericproductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaAdvertisement"), "GenericproductCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageCollectionViaEntertainmentcategoryLanguage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "EntertainmentcategoryLanguage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, GetRelationsForField("LanguageCollectionViaEntertainmentcategoryLanguage"), "LanguageCollectionViaEntertainmentcategoryLanguage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaAnnouncement"), "MediaCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaAnnouncement_"), "MediaCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnYesEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAnnouncement"), "ProductCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingOnNoEntertainmentCategory;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAnnouncement_"), "ProductCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium_"), "ProductCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAdvertisement"), "ProductCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supplier'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupplierCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingActionEntertainmentCategoryId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupplierCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.SupplierEntity, 0, null, null, GetRelationsForField("SupplierCollectionViaAdvertisement"), "SupplierCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.SurveyEntity, 0, null, null, GetRelationsForField("SurveyCollectionViaMedium"), "SurveyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingActionEntertainmentcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EntertainmentcategoryId property of the Entity Entertainmentcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Entertainmentcategory"."EntertainmentcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 EntertainmentcategoryId
		{
			get { return (System.Int32)GetValue((int)EntertainmentcategoryFieldIndex.EntertainmentcategoryId, true); }
			set	{ SetValue((int)EntertainmentcategoryFieldIndex.EntertainmentcategoryId, value, true); }
		}

		/// <summary> The Name property of the Entity Entertainmentcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Entertainmentcategory"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)EntertainmentcategoryFieldIndex.Name, true); }
			set	{ SetValue((int)EntertainmentcategoryFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Entertainmentcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Entertainmentcategory"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentcategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)EntertainmentcategoryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Entertainmentcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Entertainmentcategory"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentcategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)EntertainmentcategoryFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Entertainmentcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Entertainmentcategory"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)EntertainmentcategoryFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)EntertainmentcategoryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Entertainmentcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Entertainmentcategory"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)EntertainmentcategoryFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)EntertainmentcategoryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollection
		{
			get	{ return GetMultiAnnouncementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollection. When set to true, AnnouncementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollection
		{
			get	{ return _alwaysFetchAnnouncementCollection; }
			set	{ _alwaysFetchAnnouncementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollection already has been fetched. Setting this property to false when AnnouncementCollection has been fetched
		/// will clear the AnnouncementCollection collection well. Setting this property to true while AnnouncementCollection hasn't been fetched disables lazy loading for AnnouncementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollection
		{
			get { return _alreadyFetchedAnnouncementCollection;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollection && !value && (_announcementCollection != null))
				{
					_announcementCollection.Clear();
				}
				_alreadyFetchedAnnouncementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollection_
		{
			get	{ return GetMultiAnnouncementCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollection_. When set to true, AnnouncementCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollection_
		{
			get	{ return _alwaysFetchAnnouncementCollection_; }
			set	{ _alwaysFetchAnnouncementCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollection_ already has been fetched. Setting this property to false when AnnouncementCollection_ has been fetched
		/// will clear the AnnouncementCollection_ collection well. Setting this property to true while AnnouncementCollection_ hasn't been fetched disables lazy loading for AnnouncementCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollection_
		{
			get { return _alreadyFetchedAnnouncementCollection_;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollection_ && !value && (_announcementCollection_ != null))
				{
					_announcementCollection_.Clear();
				}
				_alreadyFetchedAnnouncementCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollection
		{
			get	{ return GetMultiEntertainmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollection. When set to true, EntertainmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiEntertainmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollection
		{
			get	{ return _alwaysFetchEntertainmentCollection; }
			set	{ _alwaysFetchEntertainmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollection already has been fetched. Setting this property to false when EntertainmentCollection has been fetched
		/// will clear the EntertainmentCollection collection well. Setting this property to true while EntertainmentCollection hasn't been fetched disables lazy loading for EntertainmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollection
		{
			get { return _alreadyFetchedEntertainmentCollection;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollection && !value && (_entertainmentCollection != null))
				{
					_entertainmentCollection.Clear();
				}
				_alreadyFetchedEntertainmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection EntertainmentcategoryLanguageCollection
		{
			get	{ return GetMultiEntertainmentcategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryLanguageCollection. When set to true, EntertainmentcategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiEntertainmentcategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryLanguageCollection
		{
			get	{ return _alwaysFetchEntertainmentcategoryLanguageCollection; }
			set	{ _alwaysFetchEntertainmentcategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryLanguageCollection already has been fetched. Setting this property to false when EntertainmentcategoryLanguageCollection has been fetched
		/// will clear the EntertainmentcategoryLanguageCollection collection well. Setting this property to true while EntertainmentcategoryLanguageCollection hasn't been fetched disables lazy loading for EntertainmentcategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryLanguageCollection
		{
			get { return _alreadyFetchedEntertainmentcategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryLanguageCollection && !value && (_entertainmentcategoryLanguageCollection != null))
				{
					_entertainmentcategoryLanguageCollection.Clear();
				}
				_alreadyFetchedEntertainmentcategoryLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiActionMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection ActionMediaCollection
		{
			get	{ return GetMultiActionMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ActionMediaCollection. When set to true, ActionMediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionMediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiActionMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionMediaCollection
		{
			get	{ return _alwaysFetchActionMediaCollection; }
			set	{ _alwaysFetchActionMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionMediaCollection already has been fetched. Setting this property to false when ActionMediaCollection has been fetched
		/// will clear the ActionMediaCollection collection well. Setting this property to true while ActionMediaCollection hasn't been fetched disables lazy loading for ActionMediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionMediaCollection
		{
			get { return _alreadyFetchedActionMediaCollection;}
			set 
			{
				if(_alreadyFetchedActionMediaCollection && !value && (_actionMediaCollection != null))
				{
					_actionMediaCollection.Clear();
				}
				_alreadyFetchedActionMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollectionViaMedium
		{
			get { return GetMultiAdvertisementCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollectionViaMedium. When set to true, AdvertisementCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAdvertisementCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollectionViaMedium
		{
			get	{ return _alwaysFetchAdvertisementCollectionViaMedium; }
			set	{ _alwaysFetchAdvertisementCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollectionViaMedium already has been fetched. Setting this property to false when AdvertisementCollectionViaMedium has been fetched
		/// will clear the AdvertisementCollectionViaMedium collection well. Setting this property to true while AdvertisementCollectionViaMedium hasn't been fetched disables lazy loading for AdvertisementCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollectionViaMedium
		{
			get { return _alreadyFetchedAdvertisementCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollectionViaMedium && !value && (_advertisementCollectionViaMedium != null))
				{
					_advertisementCollectionViaMedium.Clear();
				}
				_alreadyFetchedAdvertisementCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollectionViaMedium
		{
			get { return GetMultiAlterationCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollectionViaMedium. When set to true, AlterationCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationCollectionViaMedium; }
			set	{ _alwaysFetchAlterationCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollectionViaMedium already has been fetched. Setting this property to false when AlterationCollectionViaMedium has been fetched
		/// will clear the AlterationCollectionViaMedium collection well. Setting this property to true while AlterationCollectionViaMedium hasn't been fetched disables lazy loading for AlterationCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationCollectionViaMedium && !value && (_alterationCollectionViaMedium != null))
				{
					_alterationCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAdvertisement
		{
			get { return GetMultiCategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAdvertisement. When set to true, CategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchCategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchCategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when CategoryCollectionViaAdvertisement has been fetched
		/// will clear the CategoryCollectionViaAdvertisement collection well. Setting this property to true while CategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for CategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedCategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAdvertisement && !value && (_categoryCollectionViaAdvertisement != null))
				{
					_categoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium_
		{
			get { return GetMultiCategoryCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium_. When set to true, CategoryCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium_
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium_; }
			set	{ _alwaysFetchCategoryCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium_ already has been fetched. Setting this property to false when CategoryCollectionViaMedium_ has been fetched
		/// will clear the CategoryCollectionViaMedium_ collection well. Setting this property to true while CategoryCollectionViaMedium_ hasn't been fetched disables lazy loading for CategoryCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium_
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium_ && !value && (_categoryCollectionViaMedium_ != null))
				{
					_categoryCollectionViaMedium_.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaAdvertisement
		{
			get { return GetMultiCompanyCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaAdvertisement. When set to true, CompanyCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchCompanyCollectionViaAdvertisement; }
			set	{ _alwaysFetchCompanyCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaAdvertisement already has been fetched. Setting this property to false when CompanyCollectionViaAdvertisement has been fetched
		/// will clear the CompanyCollectionViaAdvertisement collection well. Setting this property to true while CompanyCollectionViaAdvertisement hasn't been fetched disables lazy loading for CompanyCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaAdvertisement
		{
			get { return _alreadyFetchedCompanyCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaAdvertisement && !value && (_companyCollectionViaAdvertisement != null))
				{
					_companyCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedCompanyCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMedium
		{
			get { return GetMultiCompanyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMedium. When set to true, CompanyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMedium
		{
			get	{ return _alwaysFetchCompanyCollectionViaMedium; }
			set	{ _alwaysFetchCompanyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMedium already has been fetched. Setting this property to false when CompanyCollectionViaMedium has been fetched
		/// will clear the CompanyCollectionViaMedium collection well. Setting this property to true while CompanyCollectionViaMedium hasn't been fetched disables lazy loading for CompanyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMedium
		{
			get { return _alreadyFetchedCompanyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMedium && !value && (_companyCollectionViaMedium != null))
				{
					_companyCollectionViaMedium.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaMedium
		{
			get { return GetMultiDeliverypointgroupCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaMedium. When set to true, DeliverypointgroupCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaMedium
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaMedium; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaMedium already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaMedium has been fetched
		/// will clear the DeliverypointgroupCollectionViaMedium collection well. Setting this property to true while DeliverypointgroupCollectionViaMedium hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaMedium
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaMedium && !value && (_deliverypointgroupCollectionViaMedium != null))
				{
					_deliverypointgroupCollectionViaMedium.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAdvertisement
		{
			get { return GetMultiDeliverypointgroupCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAdvertisement. When set to true, DeliverypointgroupCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAdvertisement; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAdvertisement already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAdvertisement has been fetched
		/// will clear the DeliverypointgroupCollectionViaAdvertisement collection well. Setting this property to true while DeliverypointgroupCollectionViaAdvertisement hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAdvertisement
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement && !value && (_deliverypointgroupCollectionViaAdvertisement != null))
				{
					_deliverypointgroupCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAnnouncement
		{
			get { return GetMultiDeliverypointgroupCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAnnouncement. When set to true, DeliverypointgroupCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAnnouncement; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAnnouncement already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAnnouncement has been fetched
		/// will clear the DeliverypointgroupCollectionViaAnnouncement collection well. Setting this property to true while DeliverypointgroupCollectionViaAnnouncement hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAnnouncement
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement && !value && (_deliverypointgroupCollectionViaAnnouncement != null))
				{
					_deliverypointgroupCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAnnouncement_
		{
			get { return GetMultiDeliverypointgroupCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAnnouncement_. When set to true, DeliverypointgroupCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAnnouncement_ already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAnnouncement_ has been fetched
		/// will clear the DeliverypointgroupCollectionViaAnnouncement_ collection well. Setting this property to true while DeliverypointgroupCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ && !value && (_deliverypointgroupCollectionViaAnnouncement_ != null))
				{
					_deliverypointgroupCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement. When set to true, EntertainmentCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement && !value && (_entertainmentCollectionViaAdvertisement != null))
				{
					_entertainmentCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement_
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement_. When set to true, EntertainmentCollectionViaAdvertisement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement_; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement_ already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement_ has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement_ collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement_ && !value && (_entertainmentCollectionViaAdvertisement_ != null))
				{
					_entertainmentCollectionViaAdvertisement_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium_
		{
			get { return GetMultiEntertainmentCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium_. When set to true, EntertainmentCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium_; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium_ already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium_ has been fetched
		/// will clear the EntertainmentCollectionViaMedium_ collection well. Setting this property to true while EntertainmentCollectionViaMedium_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium_ && !value && (_entertainmentCollectionViaMedium_ != null))
				{
					_entertainmentCollectionViaMedium_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAnnouncement
		{
			get { return GetMultiEntertainmentCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAnnouncement. When set to true, EntertainmentCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAnnouncement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAnnouncement already has been fetched. Setting this property to false when EntertainmentCollectionViaAnnouncement has been fetched
		/// will clear the EntertainmentCollectionViaAnnouncement collection well. Setting this property to true while EntertainmentCollectionViaAnnouncement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAnnouncement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAnnouncement && !value && (_entertainmentCollectionViaAnnouncement != null))
				{
					_entertainmentCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAnnouncement_
		{
			get { return GetMultiEntertainmentCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAnnouncement_. When set to true, EntertainmentCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAnnouncement_; }
			set	{ _alwaysFetchEntertainmentCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAnnouncement_ already has been fetched. Setting this property to false when EntertainmentCollectionViaAnnouncement_ has been fetched
		/// will clear the EntertainmentCollectionViaAnnouncement_ collection well. Setting this property to true while EntertainmentCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAnnouncement_ && !value && (_entertainmentCollectionViaAnnouncement_ != null))
				{
					_entertainmentCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollectionViaMedium
		{
			get { return GetMultiGenericcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollectionViaMedium. When set to true, GenericcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericcategoryCollectionViaMedium; }
			set	{ _alwaysFetchGenericcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollectionViaMedium already has been fetched. Setting this property to false when GenericcategoryCollectionViaMedium has been fetched
		/// will clear the GenericcategoryCollectionViaMedium collection well. Setting this property to true while GenericcategoryCollectionViaMedium hasn't been fetched disables lazy loading for GenericcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedGenericcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollectionViaMedium && !value && (_genericcategoryCollectionViaMedium != null))
				{
					_genericcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaMedium
		{
			get { return GetMultiGenericproductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaMedium. When set to true, GenericproductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericproductCollectionViaMedium; }
			set	{ _alwaysFetchGenericproductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaMedium already has been fetched. Setting this property to false when GenericproductCollectionViaMedium has been fetched
		/// will clear the GenericproductCollectionViaMedium collection well. Setting this property to true while GenericproductCollectionViaMedium hasn't been fetched disables lazy loading for GenericproductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaMedium
		{
			get { return _alreadyFetchedGenericproductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaMedium && !value && (_genericproductCollectionViaMedium != null))
				{
					_genericproductCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaAdvertisement
		{
			get { return GetMultiGenericproductCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaAdvertisement. When set to true, GenericproductCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchGenericproductCollectionViaAdvertisement; }
			set	{ _alwaysFetchGenericproductCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaAdvertisement already has been fetched. Setting this property to false when GenericproductCollectionViaAdvertisement has been fetched
		/// will clear the GenericproductCollectionViaAdvertisement collection well. Setting this property to true while GenericproductCollectionViaAdvertisement hasn't been fetched disables lazy loading for GenericproductCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaAdvertisement
		{
			get { return _alreadyFetchedGenericproductCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaAdvertisement && !value && (_genericproductCollectionViaAdvertisement != null))
				{
					_genericproductCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLanguageCollectionViaEntertainmentcategoryLanguage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LanguageCollection LanguageCollectionViaEntertainmentcategoryLanguage
		{
			get { return GetMultiLanguageCollectionViaEntertainmentcategoryLanguage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageCollectionViaEntertainmentcategoryLanguage. When set to true, LanguageCollectionViaEntertainmentcategoryLanguage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageCollectionViaEntertainmentcategoryLanguage is accessed. You can always execute a forced fetch by calling GetMultiLanguageCollectionViaEntertainmentcategoryLanguage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage
		{
			get	{ return _alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage; }
			set	{ _alwaysFetchLanguageCollectionViaEntertainmentcategoryLanguage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageCollectionViaEntertainmentcategoryLanguage already has been fetched. Setting this property to false when LanguageCollectionViaEntertainmentcategoryLanguage has been fetched
		/// will clear the LanguageCollectionViaEntertainmentcategoryLanguage collection well. Setting this property to true while LanguageCollectionViaEntertainmentcategoryLanguage hasn't been fetched disables lazy loading for LanguageCollectionViaEntertainmentcategoryLanguage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage
		{
			get { return _alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage;}
			set 
			{
				if(_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage && !value && (_languageCollectionViaEntertainmentcategoryLanguage != null))
				{
					_languageCollectionViaEntertainmentcategoryLanguage.Clear();
				}
				_alreadyFetchedLanguageCollectionViaEntertainmentcategoryLanguage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaAnnouncement
		{
			get { return GetMultiMediaCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaAnnouncement. When set to true, MediaCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchMediaCollectionViaAnnouncement; }
			set	{ _alwaysFetchMediaCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaAnnouncement already has been fetched. Setting this property to false when MediaCollectionViaAnnouncement has been fetched
		/// will clear the MediaCollectionViaAnnouncement collection well. Setting this property to true while MediaCollectionViaAnnouncement hasn't been fetched disables lazy loading for MediaCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaAnnouncement
		{
			get { return _alreadyFetchedMediaCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaAnnouncement && !value && (_mediaCollectionViaAnnouncement != null))
				{
					_mediaCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedMediaCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaAnnouncement_
		{
			get { return GetMultiMediaCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaAnnouncement_. When set to true, MediaCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchMediaCollectionViaAnnouncement_; }
			set	{ _alwaysFetchMediaCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaAnnouncement_ already has been fetched. Setting this property to false when MediaCollectionViaAnnouncement_ has been fetched
		/// will clear the MediaCollectionViaAnnouncement_ collection well. Setting this property to true while MediaCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for MediaCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedMediaCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaAnnouncement_ && !value && (_mediaCollectionViaAnnouncement_ != null))
				{
					_mediaCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedMediaCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAnnouncement
		{
			get { return GetMultiProductCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAnnouncement. When set to true, ProductCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchProductCollectionViaAnnouncement; }
			set	{ _alwaysFetchProductCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAnnouncement already has been fetched. Setting this property to false when ProductCollectionViaAnnouncement has been fetched
		/// will clear the ProductCollectionViaAnnouncement collection well. Setting this property to true while ProductCollectionViaAnnouncement hasn't been fetched disables lazy loading for ProductCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAnnouncement
		{
			get { return _alreadyFetchedProductCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAnnouncement && !value && (_productCollectionViaAnnouncement != null))
				{
					_productCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedProductCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAnnouncement_
		{
			get { return GetMultiProductCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAnnouncement_. When set to true, ProductCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchProductCollectionViaAnnouncement_; }
			set	{ _alwaysFetchProductCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAnnouncement_ already has been fetched. Setting this property to false when ProductCollectionViaAnnouncement_ has been fetched
		/// will clear the ProductCollectionViaAnnouncement_ collection well. Setting this property to true while ProductCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for ProductCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedProductCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAnnouncement_ && !value && (_productCollectionViaAnnouncement_ != null))
				{
					_productCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedProductCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium_
		{
			get { return GetMultiProductCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium_. When set to true, ProductCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium_
		{
			get	{ return _alwaysFetchProductCollectionViaMedium_; }
			set	{ _alwaysFetchProductCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium_ already has been fetched. Setting this property to false when ProductCollectionViaMedium_ has been fetched
		/// will clear the ProductCollectionViaMedium_ collection well. Setting this property to true while ProductCollectionViaMedium_ hasn't been fetched disables lazy loading for ProductCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium_
		{
			get { return _alreadyFetchedProductCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium_ && !value && (_productCollectionViaMedium_ != null))
				{
					_productCollectionViaMedium_.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAdvertisement
		{
			get { return GetMultiProductCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAdvertisement. When set to true, ProductCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchProductCollectionViaAdvertisement; }
			set	{ _alwaysFetchProductCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAdvertisement already has been fetched. Setting this property to false when ProductCollectionViaAdvertisement has been fetched
		/// will clear the ProductCollectionViaAdvertisement collection well. Setting this property to true while ProductCollectionViaAdvertisement hasn't been fetched disables lazy loading for ProductCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAdvertisement
		{
			get { return _alreadyFetchedProductCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAdvertisement && !value && (_productCollectionViaAdvertisement != null))
				{
					_productCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedProductCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupplierCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupplierCollection SupplierCollectionViaAdvertisement
		{
			get { return GetMultiSupplierCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupplierCollectionViaAdvertisement. When set to true, SupplierCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupplierCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiSupplierCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupplierCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchSupplierCollectionViaAdvertisement; }
			set	{ _alwaysFetchSupplierCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupplierCollectionViaAdvertisement already has been fetched. Setting this property to false when SupplierCollectionViaAdvertisement has been fetched
		/// will clear the SupplierCollectionViaAdvertisement collection well. Setting this property to true while SupplierCollectionViaAdvertisement hasn't been fetched disables lazy loading for SupplierCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupplierCollectionViaAdvertisement
		{
			get { return _alreadyFetchedSupplierCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedSupplierCollectionViaAdvertisement && !value && (_supplierCollectionViaAdvertisement != null))
				{
					_supplierCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedSupplierCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyCollection SurveyCollectionViaMedium
		{
			get { return GetMultiSurveyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyCollectionViaMedium. When set to true, SurveyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyCollectionViaMedium; }
			set	{ _alwaysFetchSurveyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyCollectionViaMedium already has been fetched. Setting this property to false when SurveyCollectionViaMedium has been fetched
		/// will clear the SurveyCollectionViaMedium collection well. Setting this property to true while SurveyCollectionViaMedium hasn't been fetched disables lazy loading for SurveyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyCollectionViaMedium && !value && (_surveyCollectionViaMedium != null))
				{
					_surveyCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
