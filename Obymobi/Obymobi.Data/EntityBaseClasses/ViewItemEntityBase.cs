﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ViewItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ViewItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ViewItemEntity"; }
		}
	
		#region Class Member Declarations
		private ViewEntity _viewEntity;
		private bool	_alwaysFetchViewEntity, _alreadyFetchedViewEntity, _viewEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ViewEntity</summary>
			public static readonly string ViewEntity = "ViewEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ViewItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ViewItemEntityBase() :base("ViewItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		protected ViewItemEntityBase(System.Int32 viewItemId):base("ViewItemEntity")
		{
			InitClassFetch(viewItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ViewItemEntityBase(System.Int32 viewItemId, IPrefetchPath prefetchPathToUse): base("ViewItemEntity")
		{
			InitClassFetch(viewItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <param name="validator">The custom validator object for this ViewItemEntity</param>
		protected ViewItemEntityBase(System.Int32 viewItemId, IValidator validator):base("ViewItemEntity")
		{
			InitClassFetch(viewItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ViewItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_viewEntity = (ViewEntity)info.GetValue("_viewEntity", typeof(ViewEntity));
			if(_viewEntity!=null)
			{
				_viewEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_viewEntityReturnsNewIfNotFound = info.GetBoolean("_viewEntityReturnsNewIfNotFound");
			_alwaysFetchViewEntity = info.GetBoolean("_alwaysFetchViewEntity");
			_alreadyFetchedViewEntity = info.GetBoolean("_alreadyFetchedViewEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ViewItemFieldIndex)fieldIndex)
			{
				case ViewItemFieldIndex.ViewId:
					DesetupSyncViewEntity(true, false);
					_alreadyFetchedViewEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedViewEntity = (_viewEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ViewEntity":
					toReturn.Add(Relations.ViewEntityUsingViewId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_viewEntity", (!this.MarkedForDeletion?_viewEntity:null));
			info.AddValue("_viewEntityReturnsNewIfNotFound", _viewEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchViewEntity", _alwaysFetchViewEntity);
			info.AddValue("_alreadyFetchedViewEntity", _alreadyFetchedViewEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ViewEntity":
					_alreadyFetchedViewEntity = true;
					this.ViewEntity = (ViewEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ViewEntity":
					SetupSyncViewEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ViewEntity":
					DesetupSyncViewEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_viewEntity!=null)
			{
				toReturn.Add(_viewEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewItemId)
		{
			return FetchUsingPK(viewItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(viewItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(viewItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(viewItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ViewItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ViewItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ViewEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ViewEntity' which is related to this entity.</returns>
		public ViewEntity GetSingleViewEntity()
		{
			return GetSingleViewEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ViewEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ViewEntity' which is related to this entity.</returns>
		public virtual ViewEntity GetSingleViewEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedViewEntity || forceFetch || _alwaysFetchViewEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ViewEntityUsingViewId);
				ViewEntity newEntity = new ViewEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ViewId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ViewEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_viewEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ViewEntity = newEntity;
				_alreadyFetchedViewEntity = fetchResult;
			}
			return _viewEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ViewEntity", _viewEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <param name="validator">The validator object for this ViewItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 viewItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(viewItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_viewEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViewItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViewId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowOnGridView", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayPosition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortPosition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOperator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayFormatString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Width", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UiElementTypeNameFull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ForLocalUseWhileSyncingIsUpdated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Updated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _viewEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncViewEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _viewEntity, new PropertyChangedEventHandler( OnViewEntityPropertyChanged ), "ViewEntity", Obymobi.Data.RelationClasses.StaticViewItemRelations.ViewEntityUsingViewIdStatic, true, signalRelatedEntity, "ViewItemCollection", resetFKFields, new int[] { (int)ViewItemFieldIndex.ViewId } );		
			_viewEntity = null;
		}
		
		/// <summary> setups the sync logic for member _viewEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncViewEntity(IEntityCore relatedEntity)
		{
			if(_viewEntity!=relatedEntity)
			{		
				DesetupSyncViewEntity(true, true);
				_viewEntity = (ViewEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _viewEntity, new PropertyChangedEventHandler( OnViewEntityPropertyChanged ), "ViewEntity", Obymobi.Data.RelationClasses.StaticViewItemRelations.ViewEntityUsingViewIdStatic, true, ref _alreadyFetchedViewEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnViewEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="viewItemId">PK value for ViewItem which data should be fetched into this ViewItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 viewItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ViewItemFieldIndex.ViewItemId].ForcedCurrentValueWrite(viewItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateViewItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ViewItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ViewItemRelations Relations
		{
			get	{ return new ViewItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'View'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathViewEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ViewCollection(), (IEntityRelation)GetRelationsForField("ViewEntity")[0], (int)Obymobi.Data.EntityType.ViewItemEntity, (int)Obymobi.Data.EntityType.ViewEntity, 0, null, null, null, "ViewEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ViewItemId property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."ViewItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ViewItemId
		{
			get { return (System.Int32)GetValue((int)ViewItemFieldIndex.ViewItemId, true); }
			set	{ SetValue((int)ViewItemFieldIndex.ViewItemId, value, true); }
		}

		/// <summary> The ViewId property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."ViewId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ViewId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ViewItemFieldIndex.ViewId, false); }
			set	{ SetValue((int)ViewItemFieldIndex.ViewId, value, true); }
		}

		/// <summary> The EntityName property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."EntityName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EntityName
		{
			get { return (System.String)GetValue((int)ViewItemFieldIndex.EntityName, true); }
			set	{ SetValue((int)ViewItemFieldIndex.EntityName, value, true); }
		}

		/// <summary> The FieldName property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."FieldName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldName
		{
			get { return (System.String)GetValue((int)ViewItemFieldIndex.FieldName, true); }
			set	{ SetValue((int)ViewItemFieldIndex.FieldName, value, true); }
		}

		/// <summary> The ShowOnGridView property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."ShowOnGridView"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowOnGridView
		{
			get { return (System.Boolean)GetValue((int)ViewItemFieldIndex.ShowOnGridView, true); }
			set	{ SetValue((int)ViewItemFieldIndex.ShowOnGridView, value, true); }
		}

		/// <summary> The DisplayPosition property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."DisplayPosition"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DisplayPosition
		{
			get { return (System.Int32)GetValue((int)ViewItemFieldIndex.DisplayPosition, true); }
			set	{ SetValue((int)ViewItemFieldIndex.DisplayPosition, value, true); }
		}

		/// <summary> The SortPosition property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."SortPosition"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortPosition
		{
			get { return (System.Int32)GetValue((int)ViewItemFieldIndex.SortPosition, true); }
			set	{ SetValue((int)ViewItemFieldIndex.SortPosition, value, true); }
		}

		/// <summary> The SortOperator property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."SortOperator"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOperator
		{
			get { return (System.Int32)GetValue((int)ViewItemFieldIndex.SortOperator, true); }
			set	{ SetValue((int)ViewItemFieldIndex.SortOperator, value, true); }
		}

		/// <summary> The DisplayFormatString property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."DisplayFormatString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DisplayFormatString
		{
			get { return (System.String)GetValue((int)ViewItemFieldIndex.DisplayFormatString, true); }
			set	{ SetValue((int)ViewItemFieldIndex.DisplayFormatString, value, true); }
		}

		/// <summary> The Width property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."Width"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Width
		{
			get { return (System.String)GetValue((int)ViewItemFieldIndex.Width, true); }
			set	{ SetValue((int)ViewItemFieldIndex.Width, value, true); }
		}

		/// <summary> The Type property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Type
		{
			get { return (System.String)GetValue((int)ViewItemFieldIndex.Type, true); }
			set	{ SetValue((int)ViewItemFieldIndex.Type, value, true); }
		}

		/// <summary> The UiElementTypeNameFull property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."UiElementTypeNameFull"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UiElementTypeNameFull
		{
			get { return (System.String)GetValue((int)ViewItemFieldIndex.UiElementTypeNameFull, true); }
			set	{ SetValue((int)ViewItemFieldIndex.UiElementTypeNameFull, value, true); }
		}

		/// <summary> The ForLocalUseWhileSyncingIsUpdated property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."ForLocalUseWhileSyncingIsUpdated"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ForLocalUseWhileSyncingIsUpdated
		{
			get { return (System.Boolean)GetValue((int)ViewItemFieldIndex.ForLocalUseWhileSyncingIsUpdated, true); }
			set	{ SetValue((int)ViewItemFieldIndex.ForLocalUseWhileSyncingIsUpdated, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ViewItemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ViewItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The Updated property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."Updated"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ViewItemFieldIndex.Updated, false); }
			set	{ SetValue((int)ViewItemFieldIndex.Updated, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ViewItemFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ViewItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ViewItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ViewItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ViewItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ViewItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ViewItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ViewItemFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ViewEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleViewEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ViewEntity ViewEntity
		{
			get	{ return GetSingleViewEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncViewEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ViewItemCollection", "ViewEntity", _viewEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ViewEntity. When set to true, ViewEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ViewEntity is accessed. You can always execute a forced fetch by calling GetSingleViewEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchViewEntity
		{
			get	{ return _alwaysFetchViewEntity; }
			set	{ _alwaysFetchViewEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ViewEntity already has been fetched. Setting this property to false when ViewEntity has been fetched
		/// will set ViewEntity to null as well. Setting this property to true while ViewEntity hasn't been fetched disables lazy loading for ViewEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedViewEntity
		{
			get { return _alreadyFetchedViewEntity;}
			set 
			{
				if(_alreadyFetchedViewEntity && !value)
				{
					this.ViewEntity = null;
				}
				_alreadyFetchedViewEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ViewEntity is not found
		/// in the database. When set to true, ViewEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ViewEntityReturnsNewIfNotFound
		{
			get	{ return _viewEntityReturnsNewIfNotFound; }
			set { _viewEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ViewItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
