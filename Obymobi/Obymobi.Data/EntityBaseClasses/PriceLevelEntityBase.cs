﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PriceLevel'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PriceLevelEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PriceLevelEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.PriceLevelItemCollection	_priceLevelItemCollection;
		private bool	_alwaysFetchPriceLevelItemCollection, _alreadyFetchedPriceLevelItemCollection;
		private Obymobi.Data.CollectionClasses.PriceScheduleItemCollection	_priceScheduleItemCollection;
		private bool	_alwaysFetchPriceScheduleItemCollection, _alreadyFetchedPriceScheduleItemCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name PriceLevelItemCollection</summary>
			public static readonly string PriceLevelItemCollection = "PriceLevelItemCollection";
			/// <summary>Member name PriceScheduleItemCollection</summary>
			public static readonly string PriceScheduleItemCollection = "PriceScheduleItemCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PriceLevelEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PriceLevelEntityBase() :base("PriceLevelEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		protected PriceLevelEntityBase(System.Int32 priceLevelId):base("PriceLevelEntity")
		{
			InitClassFetch(priceLevelId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PriceLevelEntityBase(System.Int32 priceLevelId, IPrefetchPath prefetchPathToUse): base("PriceLevelEntity")
		{
			InitClassFetch(priceLevelId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="validator">The custom validator object for this PriceLevelEntity</param>
		protected PriceLevelEntityBase(System.Int32 priceLevelId, IValidator validator):base("PriceLevelEntity")
		{
			InitClassFetch(priceLevelId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceLevelEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_priceLevelItemCollection = (Obymobi.Data.CollectionClasses.PriceLevelItemCollection)info.GetValue("_priceLevelItemCollection", typeof(Obymobi.Data.CollectionClasses.PriceLevelItemCollection));
			_alwaysFetchPriceLevelItemCollection = info.GetBoolean("_alwaysFetchPriceLevelItemCollection");
			_alreadyFetchedPriceLevelItemCollection = info.GetBoolean("_alreadyFetchedPriceLevelItemCollection");

			_priceScheduleItemCollection = (Obymobi.Data.CollectionClasses.PriceScheduleItemCollection)info.GetValue("_priceScheduleItemCollection", typeof(Obymobi.Data.CollectionClasses.PriceScheduleItemCollection));
			_alwaysFetchPriceScheduleItemCollection = info.GetBoolean("_alwaysFetchPriceScheduleItemCollection");
			_alreadyFetchedPriceScheduleItemCollection = info.GetBoolean("_alreadyFetchedPriceScheduleItemCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PriceLevelFieldIndex)fieldIndex)
			{
				case PriceLevelFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPriceLevelItemCollection = (_priceLevelItemCollection.Count > 0);
			_alreadyFetchedPriceScheduleItemCollection = (_priceScheduleItemCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "PriceLevelItemCollection":
					toReturn.Add(Relations.PriceLevelItemEntityUsingPriceLevelId);
					break;
				case "PriceScheduleItemCollection":
					toReturn.Add(Relations.PriceScheduleItemEntityUsingPriceLevelId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_priceLevelItemCollection", (!this.MarkedForDeletion?_priceLevelItemCollection:null));
			info.AddValue("_alwaysFetchPriceLevelItemCollection", _alwaysFetchPriceLevelItemCollection);
			info.AddValue("_alreadyFetchedPriceLevelItemCollection", _alreadyFetchedPriceLevelItemCollection);
			info.AddValue("_priceScheduleItemCollection", (!this.MarkedForDeletion?_priceScheduleItemCollection:null));
			info.AddValue("_alwaysFetchPriceScheduleItemCollection", _alwaysFetchPriceScheduleItemCollection);
			info.AddValue("_alreadyFetchedPriceScheduleItemCollection", _alreadyFetchedPriceScheduleItemCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "PriceLevelItemCollection":
					_alreadyFetchedPriceLevelItemCollection = true;
					if(entity!=null)
					{
						this.PriceLevelItemCollection.Add((PriceLevelItemEntity)entity);
					}
					break;
				case "PriceScheduleItemCollection":
					_alreadyFetchedPriceScheduleItemCollection = true;
					if(entity!=null)
					{
						this.PriceScheduleItemCollection.Add((PriceScheduleItemEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "PriceLevelItemCollection":
					_priceLevelItemCollection.Add((PriceLevelItemEntity)relatedEntity);
					break;
				case "PriceScheduleItemCollection":
					_priceScheduleItemCollection.Add((PriceScheduleItemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "PriceLevelItemCollection":
					this.PerformRelatedEntityRemoval(_priceLevelItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PriceScheduleItemCollection":
					this.PerformRelatedEntityRemoval(_priceScheduleItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_priceLevelItemCollection);
			toReturn.Add(_priceScheduleItemCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelId)
		{
			return FetchUsingPK(priceLevelId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(priceLevelId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(priceLevelId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceLevelId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(priceLevelId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PriceLevelId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PriceLevelRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PriceLevelItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, _priceLevelItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PriceLevelItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, _priceLevelItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPriceLevelItemCollection || forceFetch || _alwaysFetchPriceLevelItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_priceLevelItemCollection);
				_priceLevelItemCollection.SuppressClearInGetMulti=!forceFetch;
				_priceLevelItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_priceLevelItemCollection.GetMultiManyToOne(null, null, this, null, filter);
				_priceLevelItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPriceLevelItemCollection = true;
			}
			return _priceLevelItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PriceLevelItemCollection'. These settings will be taken into account
		/// when the property PriceLevelItemCollection is requested or GetMultiPriceLevelItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPriceLevelItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_priceLevelItemCollection.SortClauses=sortClauses;
			_priceLevelItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PriceScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceScheduleItemCollection GetMultiPriceScheduleItemCollection(bool forceFetch)
		{
			return GetMultiPriceScheduleItemCollection(forceFetch, _priceScheduleItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PriceScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceScheduleItemCollection GetMultiPriceScheduleItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPriceScheduleItemCollection(forceFetch, _priceScheduleItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PriceScheduleItemCollection GetMultiPriceScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPriceScheduleItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PriceScheduleItemCollection GetMultiPriceScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPriceScheduleItemCollection || forceFetch || _alwaysFetchPriceScheduleItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_priceScheduleItemCollection);
				_priceScheduleItemCollection.SuppressClearInGetMulti=!forceFetch;
				_priceScheduleItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_priceScheduleItemCollection.GetMultiManyToOne(this, null, filter);
				_priceScheduleItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPriceScheduleItemCollection = true;
			}
			return _priceScheduleItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PriceScheduleItemCollection'. These settings will be taken into account
		/// when the property PriceScheduleItemCollection is requested or GetMultiPriceScheduleItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPriceScheduleItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_priceScheduleItemCollection.SortClauses=sortClauses;
			_priceScheduleItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("PriceLevelItemCollection", _priceLevelItemCollection);
			toReturn.Add("PriceScheduleItemCollection", _priceScheduleItemCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="validator">The validator object for this PriceLevelEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 priceLevelId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(priceLevelId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_priceLevelItemCollection = new Obymobi.Data.CollectionClasses.PriceLevelItemCollection();
			_priceLevelItemCollection.SetContainingEntityInfo(this, "PriceLevelEntity");

			_priceScheduleItemCollection = new Obymobi.Data.CollectionClasses.PriceScheduleItemCollection();
			_priceScheduleItemCollection.SetContainingEntityInfo(this, "PriceLevelEntity");
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPriceLevelRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "PriceLevelCollection", resetFKFields, new int[] { (int)PriceLevelFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPriceLevelRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="priceLevelId">PK value for PriceLevel which data should be fetched into this PriceLevel object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 priceLevelId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PriceLevelFieldIndex.PriceLevelId].ForcedCurrentValueWrite(priceLevelId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePriceLevelDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PriceLevelEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PriceLevelRelations Relations
		{
			get	{ return new PriceLevelRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceLevelItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceLevelItemCollection(), (IEntityRelation)GetRelationsForField("PriceLevelItemCollection")[0], (int)Obymobi.Data.EntityType.PriceLevelEntity, (int)Obymobi.Data.EntityType.PriceLevelItemEntity, 0, null, null, null, "PriceLevelItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceScheduleItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceScheduleItemCollection(), (IEntityRelation)GetRelationsForField("PriceScheduleItemCollection")[0], (int)Obymobi.Data.EntityType.PriceLevelEntity, (int)Obymobi.Data.EntityType.PriceScheduleItemEntity, 0, null, null, null, "PriceScheduleItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.PriceLevelEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PriceLevelId property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."PriceLevelId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceLevelId
		{
			get { return (System.Int32)GetValue((int)PriceLevelFieldIndex.PriceLevelId, true); }
			set	{ SetValue((int)PriceLevelFieldIndex.PriceLevelId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PriceLevelFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PriceLevelFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PriceLevelFieldIndex.Name, true); }
			set	{ SetValue((int)PriceLevelFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceLevelFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PriceLevel<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceLevel"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceLevelFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceLevelFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPriceLevelItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PriceLevelItemCollection PriceLevelItemCollection
		{
			get	{ return GetMultiPriceLevelItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PriceLevelItemCollection. When set to true, PriceLevelItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceLevelItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPriceLevelItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceLevelItemCollection
		{
			get	{ return _alwaysFetchPriceLevelItemCollection; }
			set	{ _alwaysFetchPriceLevelItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceLevelItemCollection already has been fetched. Setting this property to false when PriceLevelItemCollection has been fetched
		/// will clear the PriceLevelItemCollection collection well. Setting this property to true while PriceLevelItemCollection hasn't been fetched disables lazy loading for PriceLevelItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceLevelItemCollection
		{
			get { return _alreadyFetchedPriceLevelItemCollection;}
			set 
			{
				if(_alreadyFetchedPriceLevelItemCollection && !value && (_priceLevelItemCollection != null))
				{
					_priceLevelItemCollection.Clear();
				}
				_alreadyFetchedPriceLevelItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PriceScheduleItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPriceScheduleItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PriceScheduleItemCollection PriceScheduleItemCollection
		{
			get	{ return GetMultiPriceScheduleItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PriceScheduleItemCollection. When set to true, PriceScheduleItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceScheduleItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPriceScheduleItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceScheduleItemCollection
		{
			get	{ return _alwaysFetchPriceScheduleItemCollection; }
			set	{ _alwaysFetchPriceScheduleItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceScheduleItemCollection already has been fetched. Setting this property to false when PriceScheduleItemCollection has been fetched
		/// will clear the PriceScheduleItemCollection collection well. Setting this property to true while PriceScheduleItemCollection hasn't been fetched disables lazy loading for PriceScheduleItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceScheduleItemCollection
		{
			get { return _alreadyFetchedPriceScheduleItemCollection;}
			set 
			{
				if(_alreadyFetchedPriceScheduleItemCollection && !value && (_priceScheduleItemCollection != null))
				{
					_priceScheduleItemCollection.Clear();
				}
				_alreadyFetchedPriceScheduleItemCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceLevelCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PriceLevelEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
