﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'User'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UserEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UserEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CompanyOwnerCollection	_companyOwnerCollection;
		private bool	_alwaysFetchCompanyOwnerCollection, _alreadyFetchedCompanyOwnerCollection;
		private Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection	_deviceTokenHistoryCollection;
		private bool	_alwaysFetchDeviceTokenHistoryCollection, _alreadyFetchedDeviceTokenHistoryCollection;
		private Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection	_deviceTokenTaskCollection;
		private bool	_alwaysFetchDeviceTokenTaskCollection, _alreadyFetchedDeviceTokenTaskCollection;
		private Obymobi.Data.CollectionClasses.PublishingCollection	_publishingCollection;
		private bool	_alwaysFetchPublishingCollection, _alreadyFetchedPublishingCollection;
		private Obymobi.Data.CollectionClasses.TerminalCollection	_terminalCollection;
		private bool	_alwaysFetchTerminalCollection, _alreadyFetchedTerminalCollection;
		private Obymobi.Data.CollectionClasses.UserBrandCollection	_userBrandCollection;
		private bool	_alwaysFetchUserBrandCollection, _alreadyFetchedUserBrandCollection;
		private Obymobi.Data.CollectionClasses.UserLogonCollection	_userLogonCollection;
		private bool	_alwaysFetchUserLogonCollection, _alreadyFetchedUserLogonCollection;
		private Obymobi.Data.CollectionClasses.UserRoleCollection	_userRoleCollection;
		private bool	_alwaysFetchUserRoleCollection, _alreadyFetchedUserRoleCollection;
		private Obymobi.Data.CollectionClasses.ViewCustomCollection	_viewCustomCollection;
		private bool	_alwaysFetchViewCustomCollection, _alreadyFetchedViewCustomCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaTerminal;
		private bool	_alwaysFetchCompanyCollectionViaTerminal, _alreadyFetchedCompanyCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal, _alreadyFetchedDeliverypointCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal_;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal_, _alreadyFetchedDeliverypointCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaTerminal, _alreadyFetchedDeliverypointgroupCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaTerminal;
		private bool	_alwaysFetchDeviceCollectionViaTerminal, _alreadyFetchedDeviceCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal, _alreadyFetchedEntertainmentCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal_;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal_, _alreadyFetchedEntertainmentCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal__;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal__, _alreadyFetchedEntertainmentCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection _icrtouchprintermappingCollectionViaTerminal;
		private bool	_alwaysFetchIcrtouchprintermappingCollectionViaTerminal, _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal;
		private bool	_alwaysFetchProductCollectionViaTerminal, _alreadyFetchedProductCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal_;
		private bool	_alwaysFetchProductCollectionViaTerminal_, _alreadyFetchedProductCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal__;
		private bool	_alwaysFetchProductCollectionViaTerminal__, _alreadyFetchedProductCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal___;
		private bool	_alwaysFetchProductCollectionViaTerminal___, _alreadyFetchedProductCollectionViaTerminal___;

		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaTerminal;
		private bool	_alwaysFetchTerminalCollectionViaTerminal, _alreadyFetchedTerminalCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaTerminal;
		private bool	_alwaysFetchUIModeCollectionViaTerminal, _alreadyFetchedUIModeCollectionViaTerminal;
		private AccountEntity _accountEntity;
		private bool	_alwaysFetchAccountEntity, _alreadyFetchedAccountEntity, _accountEntityReturnsNewIfNotFound;
		private TimeZoneEntity _timeZoneEntity;
		private bool	_alwaysFetchTimeZoneEntity, _alreadyFetchedTimeZoneEntity, _timeZoneEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AccountEntity</summary>
			public static readonly string AccountEntity = "AccountEntity";
			/// <summary>Member name TimeZoneEntity</summary>
			public static readonly string TimeZoneEntity = "TimeZoneEntity";
			/// <summary>Member name CompanyOwnerCollection</summary>
			public static readonly string CompanyOwnerCollection = "CompanyOwnerCollection";
			/// <summary>Member name DeviceTokenHistoryCollection</summary>
			public static readonly string DeviceTokenHistoryCollection = "DeviceTokenHistoryCollection";
			/// <summary>Member name DeviceTokenTaskCollection</summary>
			public static readonly string DeviceTokenTaskCollection = "DeviceTokenTaskCollection";
			/// <summary>Member name PublishingCollection</summary>
			public static readonly string PublishingCollection = "PublishingCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name UserBrandCollection</summary>
			public static readonly string UserBrandCollection = "UserBrandCollection";
			/// <summary>Member name UserLogonCollection</summary>
			public static readonly string UserLogonCollection = "UserLogonCollection";
			/// <summary>Member name UserRoleCollection</summary>
			public static readonly string UserRoleCollection = "UserRoleCollection";
			/// <summary>Member name ViewCustomCollection</summary>
			public static readonly string ViewCustomCollection = "ViewCustomCollection";
			/// <summary>Member name CompanyCollectionViaTerminal</summary>
			public static readonly string CompanyCollectionViaTerminal = "CompanyCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaTerminal</summary>
			public static readonly string DeliverypointCollectionViaTerminal = "DeliverypointCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaTerminal_</summary>
			public static readonly string DeliverypointCollectionViaTerminal_ = "DeliverypointCollectionViaTerminal_";
			/// <summary>Member name DeliverypointgroupCollectionViaTerminal</summary>
			public static readonly string DeliverypointgroupCollectionViaTerminal = "DeliverypointgroupCollectionViaTerminal";
			/// <summary>Member name DeviceCollectionViaTerminal</summary>
			public static readonly string DeviceCollectionViaTerminal = "DeviceCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal</summary>
			public static readonly string EntertainmentCollectionViaTerminal = "EntertainmentCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal_</summary>
			public static readonly string EntertainmentCollectionViaTerminal_ = "EntertainmentCollectionViaTerminal_";
			/// <summary>Member name EntertainmentCollectionViaTerminal__</summary>
			public static readonly string EntertainmentCollectionViaTerminal__ = "EntertainmentCollectionViaTerminal__";
			/// <summary>Member name IcrtouchprintermappingCollectionViaTerminal</summary>
			public static readonly string IcrtouchprintermappingCollectionViaTerminal = "IcrtouchprintermappingCollectionViaTerminal";
			/// <summary>Member name ProductCollectionViaTerminal</summary>
			public static readonly string ProductCollectionViaTerminal = "ProductCollectionViaTerminal";
			/// <summary>Member name ProductCollectionViaTerminal_</summary>
			public static readonly string ProductCollectionViaTerminal_ = "ProductCollectionViaTerminal_";
			/// <summary>Member name ProductCollectionViaTerminal__</summary>
			public static readonly string ProductCollectionViaTerminal__ = "ProductCollectionViaTerminal__";
			/// <summary>Member name ProductCollectionViaTerminal___</summary>
			public static readonly string ProductCollectionViaTerminal___ = "ProductCollectionViaTerminal___";

			/// <summary>Member name TerminalCollectionViaTerminal</summary>
			public static readonly string TerminalCollectionViaTerminal = "TerminalCollectionViaTerminal";
			/// <summary>Member name UIModeCollectionViaTerminal</summary>
			public static readonly string UIModeCollectionViaTerminal = "UIModeCollectionViaTerminal";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UserEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UserEntityBase() :base("UserEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		protected UserEntityBase(System.Int32 userId):base("UserEntity")
		{
			InitClassFetch(userId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UserEntityBase(System.Int32 userId, IPrefetchPath prefetchPathToUse): base("UserEntity")
		{
			InitClassFetch(userId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="validator">The custom validator object for this UserEntity</param>
		protected UserEntityBase(System.Int32 userId, IValidator validator):base("UserEntity")
		{
			InitClassFetch(userId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_companyOwnerCollection = (Obymobi.Data.CollectionClasses.CompanyOwnerCollection)info.GetValue("_companyOwnerCollection", typeof(Obymobi.Data.CollectionClasses.CompanyOwnerCollection));
			_alwaysFetchCompanyOwnerCollection = info.GetBoolean("_alwaysFetchCompanyOwnerCollection");
			_alreadyFetchedCompanyOwnerCollection = info.GetBoolean("_alreadyFetchedCompanyOwnerCollection");

			_deviceTokenHistoryCollection = (Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection)info.GetValue("_deviceTokenHistoryCollection", typeof(Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection));
			_alwaysFetchDeviceTokenHistoryCollection = info.GetBoolean("_alwaysFetchDeviceTokenHistoryCollection");
			_alreadyFetchedDeviceTokenHistoryCollection = info.GetBoolean("_alreadyFetchedDeviceTokenHistoryCollection");

			_deviceTokenTaskCollection = (Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection)info.GetValue("_deviceTokenTaskCollection", typeof(Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection));
			_alwaysFetchDeviceTokenTaskCollection = info.GetBoolean("_alwaysFetchDeviceTokenTaskCollection");
			_alreadyFetchedDeviceTokenTaskCollection = info.GetBoolean("_alreadyFetchedDeviceTokenTaskCollection");

			_publishingCollection = (Obymobi.Data.CollectionClasses.PublishingCollection)info.GetValue("_publishingCollection", typeof(Obymobi.Data.CollectionClasses.PublishingCollection));
			_alwaysFetchPublishingCollection = info.GetBoolean("_alwaysFetchPublishingCollection");
			_alreadyFetchedPublishingCollection = info.GetBoolean("_alreadyFetchedPublishingCollection");

			_terminalCollection = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollection", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollection = info.GetBoolean("_alwaysFetchTerminalCollection");
			_alreadyFetchedTerminalCollection = info.GetBoolean("_alreadyFetchedTerminalCollection");

			_userBrandCollection = (Obymobi.Data.CollectionClasses.UserBrandCollection)info.GetValue("_userBrandCollection", typeof(Obymobi.Data.CollectionClasses.UserBrandCollection));
			_alwaysFetchUserBrandCollection = info.GetBoolean("_alwaysFetchUserBrandCollection");
			_alreadyFetchedUserBrandCollection = info.GetBoolean("_alreadyFetchedUserBrandCollection");

			_userLogonCollection = (Obymobi.Data.CollectionClasses.UserLogonCollection)info.GetValue("_userLogonCollection", typeof(Obymobi.Data.CollectionClasses.UserLogonCollection));
			_alwaysFetchUserLogonCollection = info.GetBoolean("_alwaysFetchUserLogonCollection");
			_alreadyFetchedUserLogonCollection = info.GetBoolean("_alreadyFetchedUserLogonCollection");

			_userRoleCollection = (Obymobi.Data.CollectionClasses.UserRoleCollection)info.GetValue("_userRoleCollection", typeof(Obymobi.Data.CollectionClasses.UserRoleCollection));
			_alwaysFetchUserRoleCollection = info.GetBoolean("_alwaysFetchUserRoleCollection");
			_alreadyFetchedUserRoleCollection = info.GetBoolean("_alreadyFetchedUserRoleCollection");

			_viewCustomCollection = (Obymobi.Data.CollectionClasses.ViewCustomCollection)info.GetValue("_viewCustomCollection", typeof(Obymobi.Data.CollectionClasses.ViewCustomCollection));
			_alwaysFetchViewCustomCollection = info.GetBoolean("_alwaysFetchViewCustomCollection");
			_alreadyFetchedViewCustomCollection = info.GetBoolean("_alreadyFetchedViewCustomCollection");
			_companyCollectionViaTerminal = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaTerminal = info.GetBoolean("_alwaysFetchCompanyCollectionViaTerminal");
			_alreadyFetchedCompanyCollectionViaTerminal = info.GetBoolean("_alreadyFetchedCompanyCollectionViaTerminal");

			_deliverypointCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal");
			_alreadyFetchedDeliverypointCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal");

			_deliverypointCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal_");
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal_");

			_deliverypointgroupCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaTerminal");
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaTerminal");

			_deviceCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeviceCollectionViaTerminal");
			_alreadyFetchedDeviceCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeviceCollectionViaTerminal");

			_entertainmentCollectionViaTerminal = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal");
			_alreadyFetchedEntertainmentCollectionViaTerminal = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal");

			_entertainmentCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal_");
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal_");

			_entertainmentCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal__");
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal__");

			_icrtouchprintermappingCollectionViaTerminal = (Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection)info.GetValue("_icrtouchprintermappingCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection));
			_alwaysFetchIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal");
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal");

			_productCollectionViaTerminal = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal");
			_alreadyFetchedProductCollectionViaTerminal = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal");

			_productCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal_");
			_alreadyFetchedProductCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal_");

			_productCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal__");
			_alreadyFetchedProductCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal__");

			_productCollectionViaTerminal___ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal___", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal___ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal___");
			_alreadyFetchedProductCollectionViaTerminal___ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal___");


			_terminalCollectionViaTerminal = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaTerminal = info.GetBoolean("_alwaysFetchTerminalCollectionViaTerminal");
			_alreadyFetchedTerminalCollectionViaTerminal = info.GetBoolean("_alreadyFetchedTerminalCollectionViaTerminal");

			_uIModeCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaTerminal = info.GetBoolean("_alwaysFetchUIModeCollectionViaTerminal");
			_alreadyFetchedUIModeCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUIModeCollectionViaTerminal");
			_accountEntity = (AccountEntity)info.GetValue("_accountEntity", typeof(AccountEntity));
			if(_accountEntity!=null)
			{
				_accountEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_accountEntityReturnsNewIfNotFound = info.GetBoolean("_accountEntityReturnsNewIfNotFound");
			_alwaysFetchAccountEntity = info.GetBoolean("_alwaysFetchAccountEntity");
			_alreadyFetchedAccountEntity = info.GetBoolean("_alreadyFetchedAccountEntity");

			_timeZoneEntity = (TimeZoneEntity)info.GetValue("_timeZoneEntity", typeof(TimeZoneEntity));
			if(_timeZoneEntity!=null)
			{
				_timeZoneEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timeZoneEntityReturnsNewIfNotFound = info.GetBoolean("_timeZoneEntityReturnsNewIfNotFound");
			_alwaysFetchTimeZoneEntity = info.GetBoolean("_alwaysFetchTimeZoneEntity");
			_alreadyFetchedTimeZoneEntity = info.GetBoolean("_alreadyFetchedTimeZoneEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UserFieldIndex)fieldIndex)
			{
				case UserFieldIndex.AccountId:
					DesetupSyncAccountEntity(true, false);
					_alreadyFetchedAccountEntity = false;
					break;
				case UserFieldIndex.TimeZoneId:
					DesetupSyncTimeZoneEntity(true, false);
					_alreadyFetchedTimeZoneEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCompanyOwnerCollection = (_companyOwnerCollection.Count > 0);
			_alreadyFetchedDeviceTokenHistoryCollection = (_deviceTokenHistoryCollection.Count > 0);
			_alreadyFetchedDeviceTokenTaskCollection = (_deviceTokenTaskCollection.Count > 0);
			_alreadyFetchedPublishingCollection = (_publishingCollection.Count > 0);
			_alreadyFetchedTerminalCollection = (_terminalCollection.Count > 0);
			_alreadyFetchedUserBrandCollection = (_userBrandCollection.Count > 0);
			_alreadyFetchedUserLogonCollection = (_userLogonCollection.Count > 0);
			_alreadyFetchedUserRoleCollection = (_userRoleCollection.Count > 0);
			_alreadyFetchedViewCustomCollection = (_viewCustomCollection.Count > 0);
			_alreadyFetchedCompanyCollectionViaTerminal = (_companyCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal = (_deliverypointCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = (_deliverypointCollectionViaTerminal_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = (_deliverypointgroupCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeviceCollectionViaTerminal = (_deviceCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal = (_entertainmentCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = (_entertainmentCollectionViaTerminal_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = (_entertainmentCollectionViaTerminal__.Count > 0);
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = (_icrtouchprintermappingCollectionViaTerminal.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal = (_productCollectionViaTerminal.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal_ = (_productCollectionViaTerminal_.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal__ = (_productCollectionViaTerminal__.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal___ = (_productCollectionViaTerminal___.Count > 0);
			_alreadyFetchedTerminalCollectionViaTerminal = (_terminalCollectionViaTerminal.Count > 0);
			_alreadyFetchedUIModeCollectionViaTerminal = (_uIModeCollectionViaTerminal.Count > 0);
			_alreadyFetchedAccountEntity = (_accountEntity != null);
			_alreadyFetchedTimeZoneEntity = (_timeZoneEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AccountEntity":
					toReturn.Add(Relations.AccountEntityUsingAccountId);
					break;
				case "TimeZoneEntity":
					toReturn.Add(Relations.TimeZoneEntityUsingTimeZoneId);
					break;
				case "CompanyOwnerCollection":
					toReturn.Add(Relations.CompanyOwnerEntityUsingUserId);
					break;
				case "DeviceTokenHistoryCollection":
					toReturn.Add(Relations.DeviceTokenHistoryEntityUsingUserId);
					break;
				case "DeviceTokenTaskCollection":
					toReturn.Add(Relations.DeviceTokenTaskEntityUsingUserId);
					break;
				case "PublishingCollection":
					toReturn.Add(Relations.PublishingEntityUsingUserId);
					break;
				case "TerminalCollection":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId);
					break;
				case "UserBrandCollection":
					toReturn.Add(Relations.UserBrandEntityUsingUserId);
					break;
				case "UserLogonCollection":
					toReturn.Add(Relations.UserLogonEntityUsingUserId);
					break;
				case "UserRoleCollection":
					toReturn.Add(Relations.UserRoleEntityUsingUserId);
					break;
				case "ViewCustomCollection":
					toReturn.Add(Relations.ViewCustomEntityUsingUserId);
					break;
				case "CompanyCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingAltSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser1, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser2, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingCmsPage, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingBatteryLowProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingClientDisconnectedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingOrderFailedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal___":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingUnlockDeliverypointProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.TerminalEntityUsingForwardToTerminalId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingAutomaticSignOnUserId, "UserEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UIModeEntityUsingUIModeId, "Terminal_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_companyOwnerCollection", (!this.MarkedForDeletion?_companyOwnerCollection:null));
			info.AddValue("_alwaysFetchCompanyOwnerCollection", _alwaysFetchCompanyOwnerCollection);
			info.AddValue("_alreadyFetchedCompanyOwnerCollection", _alreadyFetchedCompanyOwnerCollection);
			info.AddValue("_deviceTokenHistoryCollection", (!this.MarkedForDeletion?_deviceTokenHistoryCollection:null));
			info.AddValue("_alwaysFetchDeviceTokenHistoryCollection", _alwaysFetchDeviceTokenHistoryCollection);
			info.AddValue("_alreadyFetchedDeviceTokenHistoryCollection", _alreadyFetchedDeviceTokenHistoryCollection);
			info.AddValue("_deviceTokenTaskCollection", (!this.MarkedForDeletion?_deviceTokenTaskCollection:null));
			info.AddValue("_alwaysFetchDeviceTokenTaskCollection", _alwaysFetchDeviceTokenTaskCollection);
			info.AddValue("_alreadyFetchedDeviceTokenTaskCollection", _alreadyFetchedDeviceTokenTaskCollection);
			info.AddValue("_publishingCollection", (!this.MarkedForDeletion?_publishingCollection:null));
			info.AddValue("_alwaysFetchPublishingCollection", _alwaysFetchPublishingCollection);
			info.AddValue("_alreadyFetchedPublishingCollection", _alreadyFetchedPublishingCollection);
			info.AddValue("_terminalCollection", (!this.MarkedForDeletion?_terminalCollection:null));
			info.AddValue("_alwaysFetchTerminalCollection", _alwaysFetchTerminalCollection);
			info.AddValue("_alreadyFetchedTerminalCollection", _alreadyFetchedTerminalCollection);
			info.AddValue("_userBrandCollection", (!this.MarkedForDeletion?_userBrandCollection:null));
			info.AddValue("_alwaysFetchUserBrandCollection", _alwaysFetchUserBrandCollection);
			info.AddValue("_alreadyFetchedUserBrandCollection", _alreadyFetchedUserBrandCollection);
			info.AddValue("_userLogonCollection", (!this.MarkedForDeletion?_userLogonCollection:null));
			info.AddValue("_alwaysFetchUserLogonCollection", _alwaysFetchUserLogonCollection);
			info.AddValue("_alreadyFetchedUserLogonCollection", _alreadyFetchedUserLogonCollection);
			info.AddValue("_userRoleCollection", (!this.MarkedForDeletion?_userRoleCollection:null));
			info.AddValue("_alwaysFetchUserRoleCollection", _alwaysFetchUserRoleCollection);
			info.AddValue("_alreadyFetchedUserRoleCollection", _alreadyFetchedUserRoleCollection);
			info.AddValue("_viewCustomCollection", (!this.MarkedForDeletion?_viewCustomCollection:null));
			info.AddValue("_alwaysFetchViewCustomCollection", _alwaysFetchViewCustomCollection);
			info.AddValue("_alreadyFetchedViewCustomCollection", _alreadyFetchedViewCustomCollection);
			info.AddValue("_companyCollectionViaTerminal", (!this.MarkedForDeletion?_companyCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaTerminal", _alwaysFetchCompanyCollectionViaTerminal);
			info.AddValue("_alreadyFetchedCompanyCollectionViaTerminal", _alreadyFetchedCompanyCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal", _alwaysFetchDeliverypointCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal", _alreadyFetchedDeliverypointCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaTerminal_", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal_", _alwaysFetchDeliverypointCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal_", _alreadyFetchedDeliverypointCollectionViaTerminal_);
			info.AddValue("_deliverypointgroupCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaTerminal", _alwaysFetchDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaTerminal", _alreadyFetchedDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_deviceCollectionViaTerminal", (!this.MarkedForDeletion?_deviceCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaTerminal", _alwaysFetchDeviceCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeviceCollectionViaTerminal", _alreadyFetchedDeviceCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal", _alwaysFetchEntertainmentCollectionViaTerminal);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal", _alreadyFetchedEntertainmentCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal_", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal_", _alwaysFetchEntertainmentCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal_", _alreadyFetchedEntertainmentCollectionViaTerminal_);
			info.AddValue("_entertainmentCollectionViaTerminal__", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal__", _alwaysFetchEntertainmentCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal__", _alreadyFetchedEntertainmentCollectionViaTerminal__);
			info.AddValue("_icrtouchprintermappingCollectionViaTerminal", (!this.MarkedForDeletion?_icrtouchprintermappingCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal", _alwaysFetchIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal", _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_productCollectionViaTerminal", (!this.MarkedForDeletion?_productCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal", _alwaysFetchProductCollectionViaTerminal);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal", _alreadyFetchedProductCollectionViaTerminal);
			info.AddValue("_productCollectionViaTerminal_", (!this.MarkedForDeletion?_productCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal_", _alwaysFetchProductCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal_", _alreadyFetchedProductCollectionViaTerminal_);
			info.AddValue("_productCollectionViaTerminal__", (!this.MarkedForDeletion?_productCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal__", _alwaysFetchProductCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal__", _alreadyFetchedProductCollectionViaTerminal__);
			info.AddValue("_productCollectionViaTerminal___", (!this.MarkedForDeletion?_productCollectionViaTerminal___:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal___", _alwaysFetchProductCollectionViaTerminal___);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal___", _alreadyFetchedProductCollectionViaTerminal___);
			info.AddValue("_terminalCollectionViaTerminal", (!this.MarkedForDeletion?_terminalCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaTerminal", _alwaysFetchTerminalCollectionViaTerminal);
			info.AddValue("_alreadyFetchedTerminalCollectionViaTerminal", _alreadyFetchedTerminalCollectionViaTerminal);
			info.AddValue("_uIModeCollectionViaTerminal", (!this.MarkedForDeletion?_uIModeCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaTerminal", _alwaysFetchUIModeCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUIModeCollectionViaTerminal", _alreadyFetchedUIModeCollectionViaTerminal);
			info.AddValue("_accountEntity", (!this.MarkedForDeletion?_accountEntity:null));
			info.AddValue("_accountEntityReturnsNewIfNotFound", _accountEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAccountEntity", _alwaysFetchAccountEntity);
			info.AddValue("_alreadyFetchedAccountEntity", _alreadyFetchedAccountEntity);
			info.AddValue("_timeZoneEntity", (!this.MarkedForDeletion?_timeZoneEntity:null));
			info.AddValue("_timeZoneEntityReturnsNewIfNotFound", _timeZoneEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimeZoneEntity", _alwaysFetchTimeZoneEntity);
			info.AddValue("_alreadyFetchedTimeZoneEntity", _alreadyFetchedTimeZoneEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AccountEntity":
					_alreadyFetchedAccountEntity = true;
					this.AccountEntity = (AccountEntity)entity;
					break;
				case "TimeZoneEntity":
					_alreadyFetchedTimeZoneEntity = true;
					this.TimeZoneEntity = (TimeZoneEntity)entity;
					break;
				case "CompanyOwnerCollection":
					_alreadyFetchedCompanyOwnerCollection = true;
					if(entity!=null)
					{
						this.CompanyOwnerCollection.Add((CompanyOwnerEntity)entity);
					}
					break;
				case "DeviceTokenHistoryCollection":
					_alreadyFetchedDeviceTokenHistoryCollection = true;
					if(entity!=null)
					{
						this.DeviceTokenHistoryCollection.Add((DeviceTokenHistoryEntity)entity);
					}
					break;
				case "DeviceTokenTaskCollection":
					_alreadyFetchedDeviceTokenTaskCollection = true;
					if(entity!=null)
					{
						this.DeviceTokenTaskCollection.Add((DeviceTokenTaskEntity)entity);
					}
					break;
				case "PublishingCollection":
					_alreadyFetchedPublishingCollection = true;
					if(entity!=null)
					{
						this.PublishingCollection.Add((PublishingEntity)entity);
					}
					break;
				case "TerminalCollection":
					_alreadyFetchedTerminalCollection = true;
					if(entity!=null)
					{
						this.TerminalCollection.Add((TerminalEntity)entity);
					}
					break;
				case "UserBrandCollection":
					_alreadyFetchedUserBrandCollection = true;
					if(entity!=null)
					{
						this.UserBrandCollection.Add((UserBrandEntity)entity);
					}
					break;
				case "UserLogonCollection":
					_alreadyFetchedUserLogonCollection = true;
					if(entity!=null)
					{
						this.UserLogonCollection.Add((UserLogonEntity)entity);
					}
					break;
				case "UserRoleCollection":
					_alreadyFetchedUserRoleCollection = true;
					if(entity!=null)
					{
						this.UserRoleCollection.Add((UserRoleEntity)entity);
					}
					break;
				case "ViewCustomCollection":
					_alreadyFetchedViewCustomCollection = true;
					if(entity!=null)
					{
						this.ViewCustomCollection.Add((ViewCustomEntity)entity);
					}
					break;
				case "CompanyCollectionViaTerminal":
					_alreadyFetchedCompanyCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaTerminal.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal":
					_alreadyFetchedDeliverypointCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal_":
					_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaTerminal.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeviceCollectionViaTerminal":
					_alreadyFetchedDeviceCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaTerminal.Add((DeviceEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal":
					_alreadyFetchedEntertainmentCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal_":
					_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal__":
					_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal__.Add((EntertainmentEntity)entity);
					}
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.IcrtouchprintermappingCollectionViaTerminal.Add((IcrtouchprintermappingEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal":
					_alreadyFetchedProductCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal_":
					_alreadyFetchedProductCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal__":
					_alreadyFetchedProductCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal__.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal___":
					_alreadyFetchedProductCollectionViaTerminal___ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal___.Add((ProductEntity)entity);
					}
					break;
				case "TerminalCollectionViaTerminal":
					_alreadyFetchedTerminalCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaTerminal.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaTerminal":
					_alreadyFetchedUIModeCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaTerminal.Add((UIModeEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AccountEntity":
					SetupSyncAccountEntity(relatedEntity);
					break;
				case "TimeZoneEntity":
					SetupSyncTimeZoneEntity(relatedEntity);
					break;
				case "CompanyOwnerCollection":
					_companyOwnerCollection.Add((CompanyOwnerEntity)relatedEntity);
					break;
				case "DeviceTokenHistoryCollection":
					_deviceTokenHistoryCollection.Add((DeviceTokenHistoryEntity)relatedEntity);
					break;
				case "DeviceTokenTaskCollection":
					_deviceTokenTaskCollection.Add((DeviceTokenTaskEntity)relatedEntity);
					break;
				case "PublishingCollection":
					_publishingCollection.Add((PublishingEntity)relatedEntity);
					break;
				case "TerminalCollection":
					_terminalCollection.Add((TerminalEntity)relatedEntity);
					break;
				case "UserBrandCollection":
					_userBrandCollection.Add((UserBrandEntity)relatedEntity);
					break;
				case "UserLogonCollection":
					_userLogonCollection.Add((UserLogonEntity)relatedEntity);
					break;
				case "UserRoleCollection":
					_userRoleCollection.Add((UserRoleEntity)relatedEntity);
					break;
				case "ViewCustomCollection":
					_viewCustomCollection.Add((ViewCustomEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AccountEntity":
					DesetupSyncAccountEntity(false, true);
					break;
				case "TimeZoneEntity":
					DesetupSyncTimeZoneEntity(false, true);
					break;
				case "CompanyOwnerCollection":
					this.PerformRelatedEntityRemoval(_companyOwnerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceTokenHistoryCollection":
					this.PerformRelatedEntityRemoval(_deviceTokenHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceTokenTaskCollection":
					this.PerformRelatedEntityRemoval(_deviceTokenTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PublishingCollection":
					this.PerformRelatedEntityRemoval(_publishingCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalCollection":
					this.PerformRelatedEntityRemoval(_terminalCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserBrandCollection":
					this.PerformRelatedEntityRemoval(_userBrandCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserLogonCollection":
					this.PerformRelatedEntityRemoval(_userLogonCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserRoleCollection":
					this.PerformRelatedEntityRemoval(_userRoleCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ViewCustomCollection":
					this.PerformRelatedEntityRemoval(_viewCustomCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_accountEntity!=null)
			{
				toReturn.Add(_accountEntity);
			}
			if(_timeZoneEntity!=null)
			{
				toReturn.Add(_timeZoneEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_companyOwnerCollection);
			toReturn.Add(_deviceTokenHistoryCollection);
			toReturn.Add(_deviceTokenTaskCollection);
			toReturn.Add(_publishingCollection);
			toReturn.Add(_terminalCollection);
			toReturn.Add(_userBrandCollection);
			toReturn.Add(_userLogonCollection);
			toReturn.Add(_userRoleCollection);
			toReturn.Add(_viewCustomCollection);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username)
		{
			return FetchUsingUCUsername( username, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCUsername( username, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCUsername( username, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="username">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCUsername(System.String username, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((UserDAO)CreateDAOInstance()).FetchUserUsingUCUsername(this, this.Transaction, username, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userId)
		{
			return FetchUsingPK(userId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(userId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(userId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 userId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(userId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UserId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UserRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyOwnerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollection(bool forceFetch)
		{
			return GetMultiCompanyOwnerCollection(forceFetch, _companyOwnerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyOwnerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyOwnerCollection(forceFetch, _companyOwnerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyOwnerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyOwnerCollection || forceFetch || _alwaysFetchCompanyOwnerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyOwnerCollection);
				_companyOwnerCollection.SuppressClearInGetMulti=!forceFetch;
				_companyOwnerCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyOwnerCollection.GetMultiManyToOne(this, filter);
				_companyOwnerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyOwnerCollection = true;
			}
			return _companyOwnerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyOwnerCollection'. These settings will be taken into account
		/// when the property CompanyOwnerCollection is requested or GetMultiCompanyOwnerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyOwnerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyOwnerCollection.SortClauses=sortClauses;
			_companyOwnerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch)
		{
			return GetMultiDeviceTokenHistoryCollection(forceFetch, _deviceTokenHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceTokenHistoryCollection(forceFetch, _deviceTokenHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceTokenHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceTokenHistoryCollection || forceFetch || _alwaysFetchDeviceTokenHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceTokenHistoryCollection);
				_deviceTokenHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_deviceTokenHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_deviceTokenHistoryCollection.GetMultiManyToOne(null, this, filter);
				_deviceTokenHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceTokenHistoryCollection = true;
			}
			return _deviceTokenHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceTokenHistoryCollection'. These settings will be taken into account
		/// when the property DeviceTokenHistoryCollection is requested or GetMultiDeviceTokenHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceTokenHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceTokenHistoryCollection.SortClauses=sortClauses;
			_deviceTokenHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch)
		{
			return GetMultiDeviceTokenTaskCollection(forceFetch, _deviceTokenTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceTokenTaskCollection(forceFetch, _deviceTokenTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceTokenTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceTokenTaskCollection || forceFetch || _alwaysFetchDeviceTokenTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceTokenTaskCollection);
				_deviceTokenTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_deviceTokenTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_deviceTokenTaskCollection.GetMultiManyToOne(null, this, filter);
				_deviceTokenTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceTokenTaskCollection = true;
			}
			return _deviceTokenTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceTokenTaskCollection'. These settings will be taken into account
		/// when the property DeviceTokenTaskCollection is requested or GetMultiDeviceTokenTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceTokenTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceTokenTaskCollection.SortClauses=sortClauses;
			_deviceTokenTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PublishingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PublishingEntity'</returns>
		public Obymobi.Data.CollectionClasses.PublishingCollection GetMultiPublishingCollection(bool forceFetch)
		{
			return GetMultiPublishingCollection(forceFetch, _publishingCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PublishingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PublishingEntity'</returns>
		public Obymobi.Data.CollectionClasses.PublishingCollection GetMultiPublishingCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPublishingCollection(forceFetch, _publishingCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PublishingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PublishingCollection GetMultiPublishingCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPublishingCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PublishingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PublishingCollection GetMultiPublishingCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPublishingCollection || forceFetch || _alwaysFetchPublishingCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_publishingCollection);
				_publishingCollection.SuppressClearInGetMulti=!forceFetch;
				_publishingCollection.EntityFactoryToUse = entityFactoryToUse;
				_publishingCollection.GetMultiManyToOne(this, filter);
				_publishingCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPublishingCollection = true;
			}
			return _publishingCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PublishingCollection'. These settings will be taken into account
		/// when the property PublishingCollection is requested or GetMultiPublishingCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPublishingCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_publishingCollection.SortClauses=sortClauses;
			_publishingCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalCollection || forceFetch || _alwaysFetchTerminalCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollection);
				_terminalCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_terminalCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollection = true;
			}
			return _terminalCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollection'. These settings will be taken into account
		/// when the property TerminalCollection is requested or GetMultiTerminalCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollection.SortClauses=sortClauses;
			_terminalCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch)
		{
			return GetMultiUserBrandCollection(forceFetch, _userBrandCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserBrandCollection(forceFetch, _userBrandCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserBrandCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UserBrandCollection GetMultiUserBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserBrandCollection || forceFetch || _alwaysFetchUserBrandCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userBrandCollection);
				_userBrandCollection.SuppressClearInGetMulti=!forceFetch;
				_userBrandCollection.EntityFactoryToUse = entityFactoryToUse;
				_userBrandCollection.GetMultiManyToOne(null, this, filter);
				_userBrandCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUserBrandCollection = true;
			}
			return _userBrandCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserBrandCollection'. These settings will be taken into account
		/// when the property UserBrandCollection is requested or GetMultiUserBrandCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserBrandCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userBrandCollection.SortClauses=sortClauses;
			_userBrandCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserLogonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserLogonEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserLogonCollection GetMultiUserLogonCollection(bool forceFetch)
		{
			return GetMultiUserLogonCollection(forceFetch, _userLogonCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserLogonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserLogonEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserLogonCollection GetMultiUserLogonCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserLogonCollection(forceFetch, _userLogonCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserLogonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserLogonCollection GetMultiUserLogonCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserLogonCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserLogonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UserLogonCollection GetMultiUserLogonCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserLogonCollection || forceFetch || _alwaysFetchUserLogonCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userLogonCollection);
				_userLogonCollection.SuppressClearInGetMulti=!forceFetch;
				_userLogonCollection.EntityFactoryToUse = entityFactoryToUse;
				_userLogonCollection.GetMultiManyToOne(this, filter);
				_userLogonCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUserLogonCollection = true;
			}
			return _userLogonCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserLogonCollection'. These settings will be taken into account
		/// when the property UserLogonCollection is requested or GetMultiUserLogonCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserLogonCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userLogonCollection.SortClauses=sortClauses;
			_userLogonCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserRoleEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch)
		{
			return GetMultiUserRoleCollection(forceFetch, _userRoleCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserRoleEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserRoleCollection(forceFetch, _userRoleCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserRoleCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UserRoleCollection GetMultiUserRoleCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserRoleCollection || forceFetch || _alwaysFetchUserRoleCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userRoleCollection);
				_userRoleCollection.SuppressClearInGetMulti=!forceFetch;
				_userRoleCollection.EntityFactoryToUse = entityFactoryToUse;
				_userRoleCollection.GetMultiManyToOne(null, this, filter);
				_userRoleCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUserRoleCollection = true;
			}
			return _userRoleCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserRoleCollection'. These settings will be taken into account
		/// when the property UserRoleCollection is requested or GetMultiUserRoleCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserRoleCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userRoleCollection.SortClauses=sortClauses;
			_userRoleCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ViewCustomEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ViewCustomEntity'</returns>
		public Obymobi.Data.CollectionClasses.ViewCustomCollection GetMultiViewCustomCollection(bool forceFetch)
		{
			return GetMultiViewCustomCollection(forceFetch, _viewCustomCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ViewCustomEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ViewCustomEntity'</returns>
		public Obymobi.Data.CollectionClasses.ViewCustomCollection GetMultiViewCustomCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiViewCustomCollection(forceFetch, _viewCustomCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ViewCustomEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ViewCustomCollection GetMultiViewCustomCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiViewCustomCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ViewCustomEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ViewCustomCollection GetMultiViewCustomCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedViewCustomCollection || forceFetch || _alwaysFetchViewCustomCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_viewCustomCollection);
				_viewCustomCollection.SuppressClearInGetMulti=!forceFetch;
				_viewCustomCollection.EntityFactoryToUse = entityFactoryToUse;
				_viewCustomCollection.GetMultiManyToOne(this, filter);
				_viewCustomCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedViewCustomCollection = true;
			}
			return _viewCustomCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ViewCustomCollection'. These settings will be taken into account
		/// when the property ViewCustomCollection is requested or GetMultiViewCustomCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersViewCustomCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_viewCustomCollection.SortClauses=sortClauses;
			_viewCustomCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaTerminal(forceFetch, _companyCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaTerminal || forceFetch || _alwaysFetchCompanyCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaTerminal.GetMulti(filter, GetRelationsForField("CompanyCollectionViaTerminal"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaTerminal = true;
			}
			return _companyCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaTerminal'. These settings will be taken into account
		/// when the property CompanyCollectionViaTerminal is requested or GetMultiCompanyCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaTerminal.SortClauses=sortClauses;
			_companyCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal(forceFetch, _deliverypointCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal = true;
			}
			return _deliverypointCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal is requested or GetMultiDeliverypointCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal_(forceFetch, _deliverypointCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal_ || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal_"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
			}
			return _deliverypointCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal_ is requested or GetMultiDeliverypointCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal_.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaTerminal(forceFetch, _deliverypointgroupCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
			}
			return _deliverypointgroupCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaTerminal is requested or GetMultiDeliverypointgroupCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaTerminal(forceFetch, _deviceCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaTerminal || forceFetch || _alwaysFetchDeviceCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeviceCollectionViaTerminal"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaTerminal = true;
			}
			return _deviceCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeviceCollectionViaTerminal is requested or GetMultiDeviceCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaTerminal.SortClauses=sortClauses;
			_deviceCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal(forceFetch, _entertainmentCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal = true;
			}
			return _entertainmentCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal is requested or GetMultiEntertainmentCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal_(forceFetch, _entertainmentCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal_ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal_"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
			}
			return _entertainmentCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal_ is requested or GetMultiEntertainmentCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal_.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal__(forceFetch, _entertainmentCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal__ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
			}
			return _entertainmentCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal__'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal__ is requested or GetMultiEntertainmentCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal__.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiIcrtouchprintermappingCollectionViaTerminal(forceFetch, _icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal || forceFetch || _alwaysFetchIcrtouchprintermappingCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_icrtouchprintermappingCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_icrtouchprintermappingCollectionViaTerminal.GetMulti(filter, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
			}
			return _icrtouchprintermappingCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'IcrtouchprintermappingCollectionViaTerminal'. These settings will be taken into account
		/// when the property IcrtouchprintermappingCollectionViaTerminal is requested or GetMultiIcrtouchprintermappingCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIcrtouchprintermappingCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_icrtouchprintermappingCollectionViaTerminal.SortClauses=sortClauses;
			_icrtouchprintermappingCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal(forceFetch, _productCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal || forceFetch || _alwaysFetchProductCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal = true;
			}
			return _productCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal is requested or GetMultiProductCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal.SortClauses=sortClauses;
			_productCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal_(forceFetch, _productCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal_ || forceFetch || _alwaysFetchProductCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal_"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal_ = true;
			}
			return _productCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal_'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal_ is requested or GetMultiProductCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal_.SortClauses=sortClauses;
			_productCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal__(forceFetch, _productCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal__ || forceFetch || _alwaysFetchProductCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal__ = true;
			}
			return _productCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal__'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal__ is requested or GetMultiProductCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal__.SortClauses=sortClauses;
			_productCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal___(forceFetch, _productCollectionViaTerminal___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal___ || forceFetch || _alwaysFetchProductCollectionViaTerminal___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal___.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal___.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal___"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal___ = true;
			}
			return _productCollectionViaTerminal___;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal___'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal___ is requested or GetMultiProductCollectionViaTerminal___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal___.SortClauses=sortClauses;
			_productCollectionViaTerminal___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaTerminal(forceFetch, _terminalCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaTerminal || forceFetch || _alwaysFetchTerminalCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaTerminal.GetMulti(filter, GetRelationsForField("TerminalCollectionViaTerminal"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaTerminal = true;
			}
			return _terminalCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaTerminal'. These settings will be taken into account
		/// when the property TerminalCollectionViaTerminal is requested or GetMultiTerminalCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaTerminal.SortClauses=sortClauses;
			_terminalCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaTerminal(forceFetch, _uIModeCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaTerminal || forceFetch || _alwaysFetchUIModeCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UserFields.UserId, ComparisonOperator.Equal, this.UserId, "UserEntity__"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UIModeCollectionViaTerminal"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaTerminal = true;
			}
			return _uIModeCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaTerminal'. These settings will be taken into account
		/// when the property UIModeCollectionViaTerminal is requested or GetMultiUIModeCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaTerminal.SortClauses=sortClauses;
			_uIModeCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AccountEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AccountEntity' which is related to this entity.</returns>
		public AccountEntity GetSingleAccountEntity()
		{
			return GetSingleAccountEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AccountEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AccountEntity' which is related to this entity.</returns>
		public virtual AccountEntity GetSingleAccountEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAccountEntity || forceFetch || _alwaysFetchAccountEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AccountEntityUsingAccountId);
				AccountEntity newEntity = new AccountEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AccountId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AccountEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_accountEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AccountEntity = newEntity;
				_alreadyFetchedAccountEntity = fetchResult;
			}
			return _accountEntity;
		}


		/// <summary> Retrieves the related entity of type 'TimeZoneEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TimeZoneEntity' which is related to this entity.</returns>
		public TimeZoneEntity GetSingleTimeZoneEntity()
		{
			return GetSingleTimeZoneEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TimeZoneEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimeZoneEntity' which is related to this entity.</returns>
		public virtual TimeZoneEntity GetSingleTimeZoneEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimeZoneEntity || forceFetch || _alwaysFetchTimeZoneEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimeZoneEntityUsingTimeZoneId);
				TimeZoneEntity newEntity = new TimeZoneEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TimeZoneId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TimeZoneEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timeZoneEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimeZoneEntity = newEntity;
				_alreadyFetchedTimeZoneEntity = fetchResult;
			}
			return _timeZoneEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AccountEntity", _accountEntity);
			toReturn.Add("TimeZoneEntity", _timeZoneEntity);
			toReturn.Add("CompanyOwnerCollection", _companyOwnerCollection);
			toReturn.Add("DeviceTokenHistoryCollection", _deviceTokenHistoryCollection);
			toReturn.Add("DeviceTokenTaskCollection", _deviceTokenTaskCollection);
			toReturn.Add("PublishingCollection", _publishingCollection);
			toReturn.Add("TerminalCollection", _terminalCollection);
			toReturn.Add("UserBrandCollection", _userBrandCollection);
			toReturn.Add("UserLogonCollection", _userLogonCollection);
			toReturn.Add("UserRoleCollection", _userRoleCollection);
			toReturn.Add("ViewCustomCollection", _viewCustomCollection);
			toReturn.Add("CompanyCollectionViaTerminal", _companyCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaTerminal", _deliverypointCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaTerminal_", _deliverypointCollectionViaTerminal_);
			toReturn.Add("DeliverypointgroupCollectionViaTerminal", _deliverypointgroupCollectionViaTerminal);
			toReturn.Add("DeviceCollectionViaTerminal", _deviceCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal", _entertainmentCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal_", _entertainmentCollectionViaTerminal_);
			toReturn.Add("EntertainmentCollectionViaTerminal__", _entertainmentCollectionViaTerminal__);
			toReturn.Add("IcrtouchprintermappingCollectionViaTerminal", _icrtouchprintermappingCollectionViaTerminal);
			toReturn.Add("ProductCollectionViaTerminal", _productCollectionViaTerminal);
			toReturn.Add("ProductCollectionViaTerminal_", _productCollectionViaTerminal_);
			toReturn.Add("ProductCollectionViaTerminal__", _productCollectionViaTerminal__);
			toReturn.Add("ProductCollectionViaTerminal___", _productCollectionViaTerminal___);
			toReturn.Add("TerminalCollectionViaTerminal", _terminalCollectionViaTerminal);
			toReturn.Add("UIModeCollectionViaTerminal", _uIModeCollectionViaTerminal);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="validator">The validator object for this UserEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 userId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(userId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_companyOwnerCollection = new Obymobi.Data.CollectionClasses.CompanyOwnerCollection();
			_companyOwnerCollection.SetContainingEntityInfo(this, "UserEntity");

			_deviceTokenHistoryCollection = new Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection();
			_deviceTokenHistoryCollection.SetContainingEntityInfo(this, "UserEntity");

			_deviceTokenTaskCollection = new Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection();
			_deviceTokenTaskCollection.SetContainingEntityInfo(this, "UserEntity");

			_publishingCollection = new Obymobi.Data.CollectionClasses.PublishingCollection();
			_publishingCollection.SetContainingEntityInfo(this, "UserEntity");

			_terminalCollection = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollection.SetContainingEntityInfo(this, "AutomaticSignOnUserEntity");

			_userBrandCollection = new Obymobi.Data.CollectionClasses.UserBrandCollection();
			_userBrandCollection.SetContainingEntityInfo(this, "UserEntity");

			_userLogonCollection = new Obymobi.Data.CollectionClasses.UserLogonCollection();
			_userLogonCollection.SetContainingEntityInfo(this, "UserEntity");

			_userRoleCollection = new Obymobi.Data.CollectionClasses.UserRoleCollection();
			_userRoleCollection.SetContainingEntityInfo(this, "UserEntity");

			_viewCustomCollection = new Obymobi.Data.CollectionClasses.ViewCustomCollection();
			_viewCustomCollection.SetContainingEntityInfo(this, "UserEntity");
			_companyCollectionViaTerminal = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deviceCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_entertainmentCollectionViaTerminal = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_icrtouchprintermappingCollectionViaTerminal = new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection();
			_productCollectionViaTerminal = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal___ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_terminalCollectionViaTerminal = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_accountEntityReturnsNewIfNotFound = true;
			_timeZoneEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Username", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Firstname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Lastname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastnamePrefix", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsApproved", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsOnline", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsLockedOut", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FailedPasswordAttemptCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordResetLinkIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogingLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RandomValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deleted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Archived", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageSize", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Role", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastActivityDateUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastLoginDateUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastPasswordChangedDateUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastLockedOutDateUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FailedPasswordAttemptWindowStartUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FailedPasswordAttemptLastDateUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordResetLinkGeneratedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneOlsonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowPublishing", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTester", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDeveloper", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasCboAccount", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _accountEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAccountEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _accountEntity, new PropertyChangedEventHandler( OnAccountEntityPropertyChanged ), "AccountEntity", Obymobi.Data.RelationClasses.StaticUserRelations.AccountEntityUsingAccountIdStatic, true, signalRelatedEntity, "UserCollection", resetFKFields, new int[] { (int)UserFieldIndex.AccountId } );		
			_accountEntity = null;
		}
		
		/// <summary> setups the sync logic for member _accountEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAccountEntity(IEntityCore relatedEntity)
		{
			if(_accountEntity!=relatedEntity)
			{		
				DesetupSyncAccountEntity(true, true);
				_accountEntity = (AccountEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _accountEntity, new PropertyChangedEventHandler( OnAccountEntityPropertyChanged ), "AccountEntity", Obymobi.Data.RelationClasses.StaticUserRelations.AccountEntityUsingAccountIdStatic, true, ref _alreadyFetchedAccountEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAccountEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timeZoneEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimeZoneEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timeZoneEntity, new PropertyChangedEventHandler( OnTimeZoneEntityPropertyChanged ), "TimeZoneEntity", Obymobi.Data.RelationClasses.StaticUserRelations.TimeZoneEntityUsingTimeZoneIdStatic, true, signalRelatedEntity, "UserCollection", resetFKFields, new int[] { (int)UserFieldIndex.TimeZoneId } );		
			_timeZoneEntity = null;
		}
		
		/// <summary> setups the sync logic for member _timeZoneEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimeZoneEntity(IEntityCore relatedEntity)
		{
			if(_timeZoneEntity!=relatedEntity)
			{		
				DesetupSyncTimeZoneEntity(true, true);
				_timeZoneEntity = (TimeZoneEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timeZoneEntity, new PropertyChangedEventHandler( OnTimeZoneEntityPropertyChanged ), "TimeZoneEntity", Obymobi.Data.RelationClasses.StaticUserRelations.TimeZoneEntityUsingTimeZoneIdStatic, true, ref _alreadyFetchedTimeZoneEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimeZoneEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 userId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UserFieldIndex.UserId].ForcedCurrentValueWrite(userId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUserDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UserEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UserRelations Relations
		{
			get	{ return new UserRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyOwner' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyOwnerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyOwnerCollection(), (IEntityRelation)GetRelationsForField("CompanyOwnerCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.CompanyOwnerEntity, 0, null, null, null, "CompanyOwnerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceTokenHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceTokenHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection(), (IEntityRelation)GetRelationsForField("DeviceTokenHistoryCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.DeviceTokenHistoryEntity, 0, null, null, null, "DeviceTokenHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceTokenTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceTokenTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection(), (IEntityRelation)GetRelationsForField("DeviceTokenTaskCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.DeviceTokenTaskEntity, 0, null, null, null, "DeviceTokenTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Publishing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPublishingCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PublishingCollection(), (IEntityRelation)GetRelationsForField("PublishingCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.PublishingEntity, 0, null, null, null, "PublishingCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserBrand' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserBrandCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserBrandCollection(), (IEntityRelation)GetRelationsForField("UserBrandCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.UserBrandEntity, 0, null, null, null, "UserBrandCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserLogon' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserLogonCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserLogonCollection(), (IEntityRelation)GetRelationsForField("UserLogonCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.UserLogonEntity, 0, null, null, null, "UserLogonCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserRole' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserRoleCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserRoleCollection(), (IEntityRelation)GetRelationsForField("UserRoleCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.UserRoleEntity, 0, null, null, null, "UserRoleCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ViewCustom' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathViewCustomCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ViewCustomCollection(), (IEntityRelation)GetRelationsForField("ViewCustomCollection")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.ViewCustomEntity, 0, null, null, null, "ViewCustomCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaTerminal"), "CompanyCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal"), "DeliverypointCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal_"), "DeliverypointCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"), "DeliverypointgroupCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaTerminal"), "DeviceCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal"), "EntertainmentCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal_"), "EntertainmentCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal__"), "EntertainmentCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"), "IcrtouchprintermappingCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal"), "ProductCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal_"), "ProductCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal__"), "ProductCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal___"), "ProductCollectionViaTerminal___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaTerminal"), "TerminalCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingAutomaticSignOnUserId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaTerminal"), "UIModeCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Account'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AccountCollection(), (IEntityRelation)GetRelationsForField("AccountEntity")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.AccountEntity, 0, null, null, null, "AccountEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TimeZone'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimeZoneEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimeZoneCollection(), (IEntityRelation)GetRelationsForField("TimeZoneEntity")[0], (int)Obymobi.Data.EntityType.UserEntity, (int)Obymobi.Data.EntityType.TimeZoneEntity, 0, null, null, null, "TimeZoneEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UserId property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."UserId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UserId
		{
			get { return (System.Int32)GetValue((int)UserFieldIndex.UserId, true); }
			set	{ SetValue((int)UserFieldIndex.UserId, value, true); }
		}

		/// <summary> The Username property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Username"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Username
		{
			get { return (System.String)GetValue((int)UserFieldIndex.Username, true); }
			set	{ SetValue((int)UserFieldIndex.Username, value, true); }
		}

		/// <summary> The Firstname property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Firstname"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Firstname
		{
			get { return (System.String)GetValue((int)UserFieldIndex.Firstname, true); }
			set	{ SetValue((int)UserFieldIndex.Firstname, value, true); }
		}

		/// <summary> The Lastname property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Lastname"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Lastname
		{
			get { return (System.String)GetValue((int)UserFieldIndex.Lastname, true); }
			set	{ SetValue((int)UserFieldIndex.Lastname, value, true); }
		}

		/// <summary> The LastnamePrefix property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."LastnamePrefix"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastnamePrefix
		{
			get { return (System.String)GetValue((int)UserFieldIndex.LastnamePrefix, true); }
			set	{ SetValue((int)UserFieldIndex.LastnamePrefix, value, true); }
		}

		/// <summary> The Email property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)UserFieldIndex.Email, true); }
			set	{ SetValue((int)UserFieldIndex.Email, value, true); }
		}

		/// <summary> The Password property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Password"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)UserFieldIndex.Password, true); }
			set	{ SetValue((int)UserFieldIndex.Password, value, true); }
		}

		/// <summary> The IsApproved property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."IsApproved"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsApproved
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.IsApproved, true); }
			set	{ SetValue((int)UserFieldIndex.IsApproved, value, true); }
		}

		/// <summary> The IsOnline property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."IsOnline"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsOnline
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.IsOnline, true); }
			set	{ SetValue((int)UserFieldIndex.IsOnline, value, true); }
		}

		/// <summary> The IsLockedOut property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."IsLockedOut"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsLockedOut
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.IsLockedOut, true); }
			set	{ SetValue((int)UserFieldIndex.IsLockedOut, value, true); }
		}

		/// <summary> The FailedPasswordAttemptCount property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."FailedPasswordAttemptCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FailedPasswordAttemptCount
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserFieldIndex.FailedPasswordAttemptCount, false); }
			set	{ SetValue((int)UserFieldIndex.FailedPasswordAttemptCount, value, true); }
		}

		/// <summary> The PasswordResetLinkIdentifier property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."PasswordResetLinkIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PasswordResetLinkIdentifier
		{
			get { return (System.String)GetValue((int)UserFieldIndex.PasswordResetLinkIdentifier, true); }
			set	{ SetValue((int)UserFieldIndex.PasswordResetLinkIdentifier, value, true); }
		}

		/// <summary> The LogingLevel property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."LogingLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LogingLevel
		{
			get { return (System.Int32)GetValue((int)UserFieldIndex.LogingLevel, true); }
			set	{ SetValue((int)UserFieldIndex.LogingLevel, value, true); }
		}

		/// <summary> The RandomValue property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."RandomValue"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RandomValue
		{
			get { return (System.String)GetValue((int)UserFieldIndex.RandomValue, true); }
			set	{ SetValue((int)UserFieldIndex.RandomValue, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UserFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UserFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Deleted property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Deleted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Deleted
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.Deleted, true); }
			set	{ SetValue((int)UserFieldIndex.Deleted, value, true); }
		}

		/// <summary> The Archived property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Archived"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Archived
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.Archived, true); }
			set	{ SetValue((int)UserFieldIndex.Archived, value, true); }
		}

		/// <summary> The PageSize property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."PageSize"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageSize
		{
			get { return (System.Int32)GetValue((int)UserFieldIndex.PageSize, true); }
			set	{ SetValue((int)UserFieldIndex.PageSize, value, true); }
		}

		/// <summary> The AccountId property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."AccountId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AccountId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserFieldIndex.AccountId, false); }
			set	{ SetValue((int)UserFieldIndex.AccountId, value, true); }
		}

		/// <summary> The Role property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."Role"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.Role Role
		{
			get { return (Obymobi.Enums.Role)GetValue((int)UserFieldIndex.Role, true); }
			set	{ SetValue((int)UserFieldIndex.Role, value, true); }
		}

		/// <summary> The TimeZoneId property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."TimeZoneId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TimeZoneId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UserFieldIndex.TimeZoneId, false); }
			set	{ SetValue((int)UserFieldIndex.TimeZoneId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UserFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UserFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LastActivityDateUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."LastActivityDateUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastActivityDateUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.LastActivityDateUTC, false); }
			set	{ SetValue((int)UserFieldIndex.LastActivityDateUTC, value, true); }
		}

		/// <summary> The LastLoginDateUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."LastLoginDateUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastLoginDateUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.LastLoginDateUTC, false); }
			set	{ SetValue((int)UserFieldIndex.LastLoginDateUTC, value, true); }
		}

		/// <summary> The LastPasswordChangedDateUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."LastPasswordChangedDateUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastPasswordChangedDateUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.LastPasswordChangedDateUTC, false); }
			set	{ SetValue((int)UserFieldIndex.LastPasswordChangedDateUTC, value, true); }
		}

		/// <summary> The LastLockedOutDateUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."LastLockedOutDateUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastLockedOutDateUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.LastLockedOutDateUTC, false); }
			set	{ SetValue((int)UserFieldIndex.LastLockedOutDateUTC, value, true); }
		}

		/// <summary> The FailedPasswordAttemptWindowStartUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."FailedPasswordAttemptWindowStartUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FailedPasswordAttemptWindowStartUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.FailedPasswordAttemptWindowStartUTC, false); }
			set	{ SetValue((int)UserFieldIndex.FailedPasswordAttemptWindowStartUTC, value, true); }
		}

		/// <summary> The FailedPasswordAttemptLastDateUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."FailedPasswordAttemptLastDateUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FailedPasswordAttemptLastDateUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.FailedPasswordAttemptLastDateUTC, false); }
			set	{ SetValue((int)UserFieldIndex.FailedPasswordAttemptLastDateUTC, value, true); }
		}

		/// <summary> The PasswordResetLinkGeneratedUTC property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."PasswordResetLinkGeneratedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PasswordResetLinkGeneratedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UserFieldIndex.PasswordResetLinkGeneratedUTC, false); }
			set	{ SetValue((int)UserFieldIndex.PasswordResetLinkGeneratedUTC, value, true); }
		}

		/// <summary> The TimeZoneOlsonId property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."TimeZoneOlsonId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeZoneOlsonId
		{
			get { return (System.String)GetValue((int)UserFieldIndex.TimeZoneOlsonId, true); }
			set	{ SetValue((int)UserFieldIndex.TimeZoneOlsonId, value, true); }
		}

		/// <summary> The CultureCode property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."CultureCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)UserFieldIndex.CultureCode, true); }
			set	{ SetValue((int)UserFieldIndex.CultureCode, value, true); }
		}

		/// <summary> The AllowPublishing property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."AllowPublishing"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowPublishing
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.AllowPublishing, true); }
			set	{ SetValue((int)UserFieldIndex.AllowPublishing, value, true); }
		}

		/// <summary> The IsTester property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."IsTester"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTester
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.IsTester, true); }
			set	{ SetValue((int)UserFieldIndex.IsTester, value, true); }
		}

		/// <summary> The IsDeveloper property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."IsDeveloper"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsDeveloper
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.IsDeveloper, true); }
			set	{ SetValue((int)UserFieldIndex.IsDeveloper, value, true); }
		}

		/// <summary> The HasCboAccount property of the Entity User<br/><br/></summary>
		/// <remarks>Mapped on  table field: "User"."hasCboAccount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasCboAccount
		{
			get { return (System.Boolean)GetValue((int)UserFieldIndex.HasCboAccount, true); }
			set	{ SetValue((int)UserFieldIndex.HasCboAccount, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyOwnerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyOwnerCollection CompanyOwnerCollection
		{
			get	{ return GetMultiCompanyOwnerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyOwnerCollection. When set to true, CompanyOwnerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyOwnerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyOwnerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyOwnerCollection
		{
			get	{ return _alwaysFetchCompanyOwnerCollection; }
			set	{ _alwaysFetchCompanyOwnerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyOwnerCollection already has been fetched. Setting this property to false when CompanyOwnerCollection has been fetched
		/// will clear the CompanyOwnerCollection collection well. Setting this property to true while CompanyOwnerCollection hasn't been fetched disables lazy loading for CompanyOwnerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyOwnerCollection
		{
			get { return _alreadyFetchedCompanyOwnerCollection;}
			set 
			{
				if(_alreadyFetchedCompanyOwnerCollection && !value && (_companyOwnerCollection != null))
				{
					_companyOwnerCollection.Clear();
				}
				_alreadyFetchedCompanyOwnerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceTokenHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection DeviceTokenHistoryCollection
		{
			get	{ return GetMultiDeviceTokenHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceTokenHistoryCollection. When set to true, DeviceTokenHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceTokenHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceTokenHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceTokenHistoryCollection
		{
			get	{ return _alwaysFetchDeviceTokenHistoryCollection; }
			set	{ _alwaysFetchDeviceTokenHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceTokenHistoryCollection already has been fetched. Setting this property to false when DeviceTokenHistoryCollection has been fetched
		/// will clear the DeviceTokenHistoryCollection collection well. Setting this property to true while DeviceTokenHistoryCollection hasn't been fetched disables lazy loading for DeviceTokenHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceTokenHistoryCollection
		{
			get { return _alreadyFetchedDeviceTokenHistoryCollection;}
			set 
			{
				if(_alreadyFetchedDeviceTokenHistoryCollection && !value && (_deviceTokenHistoryCollection != null))
				{
					_deviceTokenHistoryCollection.Clear();
				}
				_alreadyFetchedDeviceTokenHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceTokenTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection DeviceTokenTaskCollection
		{
			get	{ return GetMultiDeviceTokenTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceTokenTaskCollection. When set to true, DeviceTokenTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceTokenTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceTokenTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceTokenTaskCollection
		{
			get	{ return _alwaysFetchDeviceTokenTaskCollection; }
			set	{ _alwaysFetchDeviceTokenTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceTokenTaskCollection already has been fetched. Setting this property to false when DeviceTokenTaskCollection has been fetched
		/// will clear the DeviceTokenTaskCollection collection well. Setting this property to true while DeviceTokenTaskCollection hasn't been fetched disables lazy loading for DeviceTokenTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceTokenTaskCollection
		{
			get { return _alreadyFetchedDeviceTokenTaskCollection;}
			set 
			{
				if(_alreadyFetchedDeviceTokenTaskCollection && !value && (_deviceTokenTaskCollection != null))
				{
					_deviceTokenTaskCollection.Clear();
				}
				_alreadyFetchedDeviceTokenTaskCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PublishingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPublishingCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PublishingCollection PublishingCollection
		{
			get	{ return GetMultiPublishingCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PublishingCollection. When set to true, PublishingCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PublishingCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPublishingCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPublishingCollection
		{
			get	{ return _alwaysFetchPublishingCollection; }
			set	{ _alwaysFetchPublishingCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PublishingCollection already has been fetched. Setting this property to false when PublishingCollection has been fetched
		/// will clear the PublishingCollection collection well. Setting this property to true while PublishingCollection hasn't been fetched disables lazy loading for PublishingCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPublishingCollection
		{
			get { return _alreadyFetchedPublishingCollection;}
			set 
			{
				if(_alreadyFetchedPublishingCollection && !value && (_publishingCollection != null))
				{
					_publishingCollection.Clear();
				}
				_alreadyFetchedPublishingCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollection
		{
			get	{ return GetMultiTerminalCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollection. When set to true, TerminalCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollection
		{
			get	{ return _alwaysFetchTerminalCollection; }
			set	{ _alwaysFetchTerminalCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollection already has been fetched. Setting this property to false when TerminalCollection has been fetched
		/// will clear the TerminalCollection collection well. Setting this property to true while TerminalCollection hasn't been fetched disables lazy loading for TerminalCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollection
		{
			get { return _alreadyFetchedTerminalCollection;}
			set 
			{
				if(_alreadyFetchedTerminalCollection && !value && (_terminalCollection != null))
				{
					_terminalCollection.Clear();
				}
				_alreadyFetchedTerminalCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserBrandEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserBrandCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserBrandCollection UserBrandCollection
		{
			get	{ return GetMultiUserBrandCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserBrandCollection. When set to true, UserBrandCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserBrandCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUserBrandCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserBrandCollection
		{
			get	{ return _alwaysFetchUserBrandCollection; }
			set	{ _alwaysFetchUserBrandCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserBrandCollection already has been fetched. Setting this property to false when UserBrandCollection has been fetched
		/// will clear the UserBrandCollection collection well. Setting this property to true while UserBrandCollection hasn't been fetched disables lazy loading for UserBrandCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserBrandCollection
		{
			get { return _alreadyFetchedUserBrandCollection;}
			set 
			{
				if(_alreadyFetchedUserBrandCollection && !value && (_userBrandCollection != null))
				{
					_userBrandCollection.Clear();
				}
				_alreadyFetchedUserBrandCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserLogonEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserLogonCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserLogonCollection UserLogonCollection
		{
			get	{ return GetMultiUserLogonCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserLogonCollection. When set to true, UserLogonCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserLogonCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUserLogonCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserLogonCollection
		{
			get	{ return _alwaysFetchUserLogonCollection; }
			set	{ _alwaysFetchUserLogonCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserLogonCollection already has been fetched. Setting this property to false when UserLogonCollection has been fetched
		/// will clear the UserLogonCollection collection well. Setting this property to true while UserLogonCollection hasn't been fetched disables lazy loading for UserLogonCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserLogonCollection
		{
			get { return _alreadyFetchedUserLogonCollection;}
			set 
			{
				if(_alreadyFetchedUserLogonCollection && !value && (_userLogonCollection != null))
				{
					_userLogonCollection.Clear();
				}
				_alreadyFetchedUserLogonCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserRoleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserRoleCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserRoleCollection UserRoleCollection
		{
			get	{ return GetMultiUserRoleCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserRoleCollection. When set to true, UserRoleCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserRoleCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUserRoleCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserRoleCollection
		{
			get	{ return _alwaysFetchUserRoleCollection; }
			set	{ _alwaysFetchUserRoleCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserRoleCollection already has been fetched. Setting this property to false when UserRoleCollection has been fetched
		/// will clear the UserRoleCollection collection well. Setting this property to true while UserRoleCollection hasn't been fetched disables lazy loading for UserRoleCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserRoleCollection
		{
			get { return _alreadyFetchedUserRoleCollection;}
			set 
			{
				if(_alreadyFetchedUserRoleCollection && !value && (_userRoleCollection != null))
				{
					_userRoleCollection.Clear();
				}
				_alreadyFetchedUserRoleCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ViewCustomEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiViewCustomCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ViewCustomCollection ViewCustomCollection
		{
			get	{ return GetMultiViewCustomCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ViewCustomCollection. When set to true, ViewCustomCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ViewCustomCollection is accessed. You can always execute/ a forced fetch by calling GetMultiViewCustomCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchViewCustomCollection
		{
			get	{ return _alwaysFetchViewCustomCollection; }
			set	{ _alwaysFetchViewCustomCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ViewCustomCollection already has been fetched. Setting this property to false when ViewCustomCollection has been fetched
		/// will clear the ViewCustomCollection collection well. Setting this property to true while ViewCustomCollection hasn't been fetched disables lazy loading for ViewCustomCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedViewCustomCollection
		{
			get { return _alreadyFetchedViewCustomCollection;}
			set 
			{
				if(_alreadyFetchedViewCustomCollection && !value && (_viewCustomCollection != null))
				{
					_viewCustomCollection.Clear();
				}
				_alreadyFetchedViewCustomCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaTerminal
		{
			get { return GetMultiCompanyCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaTerminal. When set to true, CompanyCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaTerminal
		{
			get	{ return _alwaysFetchCompanyCollectionViaTerminal; }
			set	{ _alwaysFetchCompanyCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaTerminal already has been fetched. Setting this property to false when CompanyCollectionViaTerminal has been fetched
		/// will clear the CompanyCollectionViaTerminal collection well. Setting this property to true while CompanyCollectionViaTerminal hasn't been fetched disables lazy loading for CompanyCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaTerminal
		{
			get { return _alreadyFetchedCompanyCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaTerminal && !value && (_companyCollectionViaTerminal != null))
				{
					_companyCollectionViaTerminal.Clear();
				}
				_alreadyFetchedCompanyCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal
		{
			get { return GetMultiDeliverypointCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal. When set to true, DeliverypointCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal has been fetched
		/// will clear the DeliverypointCollectionViaTerminal collection well. Setting this property to true while DeliverypointCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal && !value && (_deliverypointCollectionViaTerminal != null))
				{
					_deliverypointCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal_
		{
			get { return GetMultiDeliverypointCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal_. When set to true, DeliverypointCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal_; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal_ already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal_ has been fetched
		/// will clear the DeliverypointCollectionViaTerminal_ collection well. Setting this property to true while DeliverypointCollectionViaTerminal_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal_ && !value && (_deliverypointCollectionViaTerminal_ != null))
				{
					_deliverypointCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaTerminal
		{
			get { return GetMultiDeliverypointgroupCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaTerminal. When set to true, DeliverypointgroupCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaTerminal has been fetched
		/// will clear the DeliverypointgroupCollectionViaTerminal collection well. Setting this property to true while DeliverypointgroupCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaTerminal && !value && (_deliverypointgroupCollectionViaTerminal != null))
				{
					_deliverypointgroupCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaTerminal
		{
			get { return GetMultiDeviceCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaTerminal. When set to true, DeviceCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeviceCollectionViaTerminal; }
			set	{ _alwaysFetchDeviceCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaTerminal already has been fetched. Setting this property to false when DeviceCollectionViaTerminal has been fetched
		/// will clear the DeviceCollectionViaTerminal collection well. Setting this property to true while DeviceCollectionViaTerminal hasn't been fetched disables lazy loading for DeviceCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaTerminal
		{
			get { return _alreadyFetchedDeviceCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaTerminal && !value && (_deviceCollectionViaTerminal != null))
				{
					_deviceCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeviceCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal
		{
			get { return GetMultiEntertainmentCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal. When set to true, EntertainmentCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal has been fetched
		/// will clear the EntertainmentCollectionViaTerminal collection well. Setting this property to true while EntertainmentCollectionViaTerminal hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal && !value && (_entertainmentCollectionViaTerminal != null))
				{
					_entertainmentCollectionViaTerminal.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal_
		{
			get { return GetMultiEntertainmentCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal_. When set to true, EntertainmentCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal_; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal_ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal_ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal_ collection well. Setting this property to true while EntertainmentCollectionViaTerminal_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal_ && !value && (_entertainmentCollectionViaTerminal_ != null))
				{
					_entertainmentCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal__
		{
			get { return GetMultiEntertainmentCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal__. When set to true, EntertainmentCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal__
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal__; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal__ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal__ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal__ collection well. Setting this property to true while EntertainmentCollectionViaTerminal__ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal__
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal__ && !value && (_entertainmentCollectionViaTerminal__ != null))
				{
					_entertainmentCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIcrtouchprintermappingCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection IcrtouchprintermappingCollectionViaTerminal
		{
			get { return GetMultiIcrtouchprintermappingCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingCollectionViaTerminal. When set to true, IcrtouchprintermappingCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiIcrtouchprintermappingCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingCollectionViaTerminal
		{
			get	{ return _alwaysFetchIcrtouchprintermappingCollectionViaTerminal; }
			set	{ _alwaysFetchIcrtouchprintermappingCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingCollectionViaTerminal already has been fetched. Setting this property to false when IcrtouchprintermappingCollectionViaTerminal has been fetched
		/// will clear the IcrtouchprintermappingCollectionViaTerminal collection well. Setting this property to true while IcrtouchprintermappingCollectionViaTerminal hasn't been fetched disables lazy loading for IcrtouchprintermappingCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingCollectionViaTerminal
		{
			get { return _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal && !value && (_icrtouchprintermappingCollectionViaTerminal != null))
				{
					_icrtouchprintermappingCollectionViaTerminal.Clear();
				}
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal
		{
			get { return GetMultiProductCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal. When set to true, ProductCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal; }
			set	{ _alwaysFetchProductCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal already has been fetched. Setting this property to false when ProductCollectionViaTerminal has been fetched
		/// will clear the ProductCollectionViaTerminal collection well. Setting this property to true while ProductCollectionViaTerminal hasn't been fetched disables lazy loading for ProductCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal
		{
			get { return _alreadyFetchedProductCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal && !value && (_productCollectionViaTerminal != null))
				{
					_productCollectionViaTerminal.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal_
		{
			get { return GetMultiProductCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal_. When set to true, ProductCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal_
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal_; }
			set	{ _alwaysFetchProductCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal_ already has been fetched. Setting this property to false when ProductCollectionViaTerminal_ has been fetched
		/// will clear the ProductCollectionViaTerminal_ collection well. Setting this property to true while ProductCollectionViaTerminal_ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal_
		{
			get { return _alreadyFetchedProductCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal_ && !value && (_productCollectionViaTerminal_ != null))
				{
					_productCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal__
		{
			get { return GetMultiProductCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal__. When set to true, ProductCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal__
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal__; }
			set	{ _alwaysFetchProductCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal__ already has been fetched. Setting this property to false when ProductCollectionViaTerminal__ has been fetched
		/// will clear the ProductCollectionViaTerminal__ collection well. Setting this property to true while ProductCollectionViaTerminal__ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal__
		{
			get { return _alreadyFetchedProductCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal__ && !value && (_productCollectionViaTerminal__ != null))
				{
					_productCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal___
		{
			get { return GetMultiProductCollectionViaTerminal___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal___. When set to true, ProductCollectionViaTerminal___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal___ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal___
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal___; }
			set	{ _alwaysFetchProductCollectionViaTerminal___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal___ already has been fetched. Setting this property to false when ProductCollectionViaTerminal___ has been fetched
		/// will clear the ProductCollectionViaTerminal___ collection well. Setting this property to true while ProductCollectionViaTerminal___ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal___
		{
			get { return _alreadyFetchedProductCollectionViaTerminal___;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal___ && !value && (_productCollectionViaTerminal___ != null))
				{
					_productCollectionViaTerminal___.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaTerminal
		{
			get { return GetMultiTerminalCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaTerminal. When set to true, TerminalCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaTerminal
		{
			get	{ return _alwaysFetchTerminalCollectionViaTerminal; }
			set	{ _alwaysFetchTerminalCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaTerminal already has been fetched. Setting this property to false when TerminalCollectionViaTerminal has been fetched
		/// will clear the TerminalCollectionViaTerminal collection well. Setting this property to true while TerminalCollectionViaTerminal hasn't been fetched disables lazy loading for TerminalCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaTerminal
		{
			get { return _alreadyFetchedTerminalCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaTerminal && !value && (_terminalCollectionViaTerminal != null))
				{
					_terminalCollectionViaTerminal.Clear();
				}
				_alreadyFetchedTerminalCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaTerminal
		{
			get { return GetMultiUIModeCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaTerminal. When set to true, UIModeCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaTerminal
		{
			get	{ return _alwaysFetchUIModeCollectionViaTerminal; }
			set	{ _alwaysFetchUIModeCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaTerminal already has been fetched. Setting this property to false when UIModeCollectionViaTerminal has been fetched
		/// will clear the UIModeCollectionViaTerminal collection well. Setting this property to true while UIModeCollectionViaTerminal hasn't been fetched disables lazy loading for UIModeCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaTerminal
		{
			get { return _alreadyFetchedUIModeCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaTerminal && !value && (_uIModeCollectionViaTerminal != null))
				{
					_uIModeCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUIModeCollectionViaTerminal = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AccountEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAccountEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AccountEntity AccountEntity
		{
			get	{ return GetSingleAccountEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAccountEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserCollection", "AccountEntity", _accountEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AccountEntity. When set to true, AccountEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountEntity is accessed. You can always execute a forced fetch by calling GetSingleAccountEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountEntity
		{
			get	{ return _alwaysFetchAccountEntity; }
			set	{ _alwaysFetchAccountEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountEntity already has been fetched. Setting this property to false when AccountEntity has been fetched
		/// will set AccountEntity to null as well. Setting this property to true while AccountEntity hasn't been fetched disables lazy loading for AccountEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountEntity
		{
			get { return _alreadyFetchedAccountEntity;}
			set 
			{
				if(_alreadyFetchedAccountEntity && !value)
				{
					this.AccountEntity = null;
				}
				_alreadyFetchedAccountEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AccountEntity is not found
		/// in the database. When set to true, AccountEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AccountEntityReturnsNewIfNotFound
		{
			get	{ return _accountEntityReturnsNewIfNotFound; }
			set { _accountEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimeZoneEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimeZoneEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimeZoneEntity TimeZoneEntity
		{
			get	{ return GetSingleTimeZoneEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTimeZoneEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UserCollection", "TimeZoneEntity", _timeZoneEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimeZoneEntity. When set to true, TimeZoneEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimeZoneEntity is accessed. You can always execute a forced fetch by calling GetSingleTimeZoneEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimeZoneEntity
		{
			get	{ return _alwaysFetchTimeZoneEntity; }
			set	{ _alwaysFetchTimeZoneEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TimeZoneEntity already has been fetched. Setting this property to false when TimeZoneEntity has been fetched
		/// will set TimeZoneEntity to null as well. Setting this property to true while TimeZoneEntity hasn't been fetched disables lazy loading for TimeZoneEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimeZoneEntity
		{
			get { return _alreadyFetchedTimeZoneEntity;}
			set 
			{
				if(_alreadyFetchedTimeZoneEntity && !value)
				{
					this.TimeZoneEntity = null;
				}
				_alreadyFetchedTimeZoneEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimeZoneEntity is not found
		/// in the database. When set to true, TimeZoneEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimeZoneEntityReturnsNewIfNotFound
		{
			get	{ return _timeZoneEntityReturnsNewIfNotFound; }
			set { _timeZoneEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UserEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
