﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ClientConfiguration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ClientConfigurationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ClientConfigurationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection	_clientConfigurationRouteCollection;
		private bool	_alwaysFetchClientConfigurationRouteCollection, _alreadyFetchedClientConfigurationRouteCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection	_deliverypointCollection;
		private bool	_alwaysFetchDeliverypointCollection, _alreadyFetchedDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.PmsActionRuleCollection	_pmsActionRuleCollection;
		private bool	_alwaysFetchPmsActionRuleCollection, _alreadyFetchedPmsActionRuleCollection;
		private AdvertisementConfigurationEntity _advertisementConfigurationEntity;
		private bool	_alwaysFetchAdvertisementConfigurationEntity, _alreadyFetchedAdvertisementConfigurationEntity, _advertisementConfigurationEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private EntertainmentConfigurationEntity _entertainmentConfigurationEntity;
		private bool	_alwaysFetchEntertainmentConfigurationEntity, _alreadyFetchedEntertainmentConfigurationEntity, _entertainmentConfigurationEntityReturnsNewIfNotFound;
		private MenuEntity _menuEntity;
		private bool	_alwaysFetchMenuEntity, _alreadyFetchedMenuEntity, _menuEntityReturnsNewIfNotFound;
		private PriceScheduleEntity _priceScheduleEntity;
		private bool	_alwaysFetchPriceScheduleEntity, _alreadyFetchedPriceScheduleEntity, _priceScheduleEntityReturnsNewIfNotFound;
		private RoomControlConfigurationEntity _roomControlConfigurationEntity;
		private bool	_alwaysFetchRoomControlConfigurationEntity, _alreadyFetchedRoomControlConfigurationEntity, _roomControlConfigurationEntityReturnsNewIfNotFound;
		private UIModeEntity _uIModeEntity;
		private bool	_alwaysFetchUIModeEntity, _alreadyFetchedUIModeEntity, _uIModeEntityReturnsNewIfNotFound;
		private UIScheduleEntity _uIScheduleEntity;
		private bool	_alwaysFetchUIScheduleEntity, _alreadyFetchedUIScheduleEntity, _uIScheduleEntityReturnsNewIfNotFound;
		private UIThemeEntity _uIThemeEntity;
		private bool	_alwaysFetchUIThemeEntity, _alreadyFetchedUIThemeEntity, _uIThemeEntityReturnsNewIfNotFound;
		private WifiConfigurationEntity _wifiConfigurationEntity;
		private bool	_alwaysFetchWifiConfigurationEntity, _alreadyFetchedWifiConfigurationEntity, _wifiConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementConfigurationEntity</summary>
			public static readonly string AdvertisementConfigurationEntity = "AdvertisementConfigurationEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name EntertainmentConfigurationEntity</summary>
			public static readonly string EntertainmentConfigurationEntity = "EntertainmentConfigurationEntity";
			/// <summary>Member name MenuEntity</summary>
			public static readonly string MenuEntity = "MenuEntity";
			/// <summary>Member name PriceScheduleEntity</summary>
			public static readonly string PriceScheduleEntity = "PriceScheduleEntity";
			/// <summary>Member name RoomControlConfigurationEntity</summary>
			public static readonly string RoomControlConfigurationEntity = "RoomControlConfigurationEntity";
			/// <summary>Member name UIModeEntity</summary>
			public static readonly string UIModeEntity = "UIModeEntity";
			/// <summary>Member name UIScheduleEntity</summary>
			public static readonly string UIScheduleEntity = "UIScheduleEntity";
			/// <summary>Member name UIThemeEntity</summary>
			public static readonly string UIThemeEntity = "UIThemeEntity";
			/// <summary>Member name WifiConfigurationEntity</summary>
			public static readonly string WifiConfigurationEntity = "WifiConfigurationEntity";
			/// <summary>Member name ClientConfigurationRouteCollection</summary>
			public static readonly string ClientConfigurationRouteCollection = "ClientConfigurationRouteCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name PmsActionRuleCollection</summary>
			public static readonly string PmsActionRuleCollection = "PmsActionRuleCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClientConfigurationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ClientConfigurationEntityBase() :base("ClientConfigurationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		protected ClientConfigurationEntityBase(System.Int32 clientConfigurationId):base("ClientConfigurationEntity")
		{
			InitClassFetch(clientConfigurationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ClientConfigurationEntityBase(System.Int32 clientConfigurationId, IPrefetchPath prefetchPathToUse): base("ClientConfigurationEntity")
		{
			InitClassFetch(clientConfigurationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="validator">The custom validator object for this ClientConfigurationEntity</param>
		protected ClientConfigurationEntityBase(System.Int32 clientConfigurationId, IValidator validator):base("ClientConfigurationEntity")
		{
			InitClassFetch(clientConfigurationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientConfigurationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientConfigurationRouteCollection = (Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection)info.GetValue("_clientConfigurationRouteCollection", typeof(Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection));
			_alwaysFetchClientConfigurationRouteCollection = info.GetBoolean("_alwaysFetchClientConfigurationRouteCollection");
			_alreadyFetchedClientConfigurationRouteCollection = info.GetBoolean("_alreadyFetchedClientConfigurationRouteCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_deliverypointCollection = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollection = info.GetBoolean("_alwaysFetchDeliverypointCollection");
			_alreadyFetchedDeliverypointCollection = info.GetBoolean("_alreadyFetchedDeliverypointCollection");

			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_pmsActionRuleCollection = (Obymobi.Data.CollectionClasses.PmsActionRuleCollection)info.GetValue("_pmsActionRuleCollection", typeof(Obymobi.Data.CollectionClasses.PmsActionRuleCollection));
			_alwaysFetchPmsActionRuleCollection = info.GetBoolean("_alwaysFetchPmsActionRuleCollection");
			_alreadyFetchedPmsActionRuleCollection = info.GetBoolean("_alreadyFetchedPmsActionRuleCollection");
			_advertisementConfigurationEntity = (AdvertisementConfigurationEntity)info.GetValue("_advertisementConfigurationEntity", typeof(AdvertisementConfigurationEntity));
			if(_advertisementConfigurationEntity!=null)
			{
				_advertisementConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementConfigurationEntity = info.GetBoolean("_alwaysFetchAdvertisementConfigurationEntity");
			_alreadyFetchedAdvertisementConfigurationEntity = info.GetBoolean("_alreadyFetchedAdvertisementConfigurationEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_entertainmentConfigurationEntity = (EntertainmentConfigurationEntity)info.GetValue("_entertainmentConfigurationEntity", typeof(EntertainmentConfigurationEntity));
			if(_entertainmentConfigurationEntity!=null)
			{
				_entertainmentConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentConfigurationEntity = info.GetBoolean("_alwaysFetchEntertainmentConfigurationEntity");
			_alreadyFetchedEntertainmentConfigurationEntity = info.GetBoolean("_alreadyFetchedEntertainmentConfigurationEntity");

			_menuEntity = (MenuEntity)info.GetValue("_menuEntity", typeof(MenuEntity));
			if(_menuEntity!=null)
			{
				_menuEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_menuEntityReturnsNewIfNotFound = info.GetBoolean("_menuEntityReturnsNewIfNotFound");
			_alwaysFetchMenuEntity = info.GetBoolean("_alwaysFetchMenuEntity");
			_alreadyFetchedMenuEntity = info.GetBoolean("_alreadyFetchedMenuEntity");

			_priceScheduleEntity = (PriceScheduleEntity)info.GetValue("_priceScheduleEntity", typeof(PriceScheduleEntity));
			if(_priceScheduleEntity!=null)
			{
				_priceScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_priceScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchPriceScheduleEntity = info.GetBoolean("_alwaysFetchPriceScheduleEntity");
			_alreadyFetchedPriceScheduleEntity = info.GetBoolean("_alreadyFetchedPriceScheduleEntity");

			_roomControlConfigurationEntity = (RoomControlConfigurationEntity)info.GetValue("_roomControlConfigurationEntity", typeof(RoomControlConfigurationEntity));
			if(_roomControlConfigurationEntity!=null)
			{
				_roomControlConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlConfigurationEntity = info.GetBoolean("_alwaysFetchRoomControlConfigurationEntity");
			_alreadyFetchedRoomControlConfigurationEntity = info.GetBoolean("_alreadyFetchedRoomControlConfigurationEntity");

			_uIModeEntity = (UIModeEntity)info.GetValue("_uIModeEntity", typeof(UIModeEntity));
			if(_uIModeEntity!=null)
			{
				_uIModeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIModeEntityReturnsNewIfNotFound = info.GetBoolean("_uIModeEntityReturnsNewIfNotFound");
			_alwaysFetchUIModeEntity = info.GetBoolean("_alwaysFetchUIModeEntity");
			_alreadyFetchedUIModeEntity = info.GetBoolean("_alreadyFetchedUIModeEntity");

			_uIScheduleEntity = (UIScheduleEntity)info.GetValue("_uIScheduleEntity", typeof(UIScheduleEntity));
			if(_uIScheduleEntity!=null)
			{
				_uIScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_uIScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchUIScheduleEntity = info.GetBoolean("_alwaysFetchUIScheduleEntity");
			_alreadyFetchedUIScheduleEntity = info.GetBoolean("_alreadyFetchedUIScheduleEntity");

			_uIThemeEntity = (UIThemeEntity)info.GetValue("_uIThemeEntity", typeof(UIThemeEntity));
			if(_uIThemeEntity!=null)
			{
				_uIThemeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIThemeEntityReturnsNewIfNotFound = info.GetBoolean("_uIThemeEntityReturnsNewIfNotFound");
			_alwaysFetchUIThemeEntity = info.GetBoolean("_alwaysFetchUIThemeEntity");
			_alreadyFetchedUIThemeEntity = info.GetBoolean("_alreadyFetchedUIThemeEntity");

			_wifiConfigurationEntity = (WifiConfigurationEntity)info.GetValue("_wifiConfigurationEntity", typeof(WifiConfigurationEntity));
			if(_wifiConfigurationEntity!=null)
			{
				_wifiConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_wifiConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_wifiConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchWifiConfigurationEntity = info.GetBoolean("_alwaysFetchWifiConfigurationEntity");
			_alreadyFetchedWifiConfigurationEntity = info.GetBoolean("_alreadyFetchedWifiConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ClientConfigurationFieldIndex)fieldIndex)
			{
				case ClientConfigurationFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case ClientConfigurationFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case ClientConfigurationFieldIndex.MenuId:
					DesetupSyncMenuEntity(true, false);
					_alreadyFetchedMenuEntity = false;
					break;
				case ClientConfigurationFieldIndex.UIModeId:
					DesetupSyncUIModeEntity(true, false);
					_alreadyFetchedUIModeEntity = false;
					break;
				case ClientConfigurationFieldIndex.UIThemeId:
					DesetupSyncUIThemeEntity(true, false);
					_alreadyFetchedUIThemeEntity = false;
					break;
				case ClientConfigurationFieldIndex.UIScheduleId:
					DesetupSyncUIScheduleEntity(true, false);
					_alreadyFetchedUIScheduleEntity = false;
					break;
				case ClientConfigurationFieldIndex.RoomControlConfigurationId:
					DesetupSyncRoomControlConfigurationEntity(true, false);
					_alreadyFetchedRoomControlConfigurationEntity = false;
					break;
				case ClientConfigurationFieldIndex.WifiConfigurationId:
					DesetupSyncWifiConfigurationEntity(true, false);
					_alreadyFetchedWifiConfigurationEntity = false;
					break;
				case ClientConfigurationFieldIndex.PriceScheduleId:
					DesetupSyncPriceScheduleEntity(true, false);
					_alreadyFetchedPriceScheduleEntity = false;
					break;
				case ClientConfigurationFieldIndex.AdvertisementConfigurationId:
					DesetupSyncAdvertisementConfigurationEntity(true, false);
					_alreadyFetchedAdvertisementConfigurationEntity = false;
					break;
				case ClientConfigurationFieldIndex.EntertainmentConfigurationId:
					DesetupSyncEntertainmentConfigurationEntity(true, false);
					_alreadyFetchedEntertainmentConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientConfigurationRouteCollection = (_clientConfigurationRouteCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedDeliverypointCollection = (_deliverypointCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedPmsActionRuleCollection = (_pmsActionRuleCollection.Count > 0);
			_alreadyFetchedAdvertisementConfigurationEntity = (_advertisementConfigurationEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedEntertainmentConfigurationEntity = (_entertainmentConfigurationEntity != null);
			_alreadyFetchedMenuEntity = (_menuEntity != null);
			_alreadyFetchedPriceScheduleEntity = (_priceScheduleEntity != null);
			_alreadyFetchedRoomControlConfigurationEntity = (_roomControlConfigurationEntity != null);
			_alreadyFetchedUIModeEntity = (_uIModeEntity != null);
			_alreadyFetchedUIScheduleEntity = (_uIScheduleEntity != null);
			_alreadyFetchedUIThemeEntity = (_uIThemeEntity != null);
			_alreadyFetchedWifiConfigurationEntity = (_wifiConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementConfigurationEntity":
					toReturn.Add(Relations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "EntertainmentConfigurationEntity":
					toReturn.Add(Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
					break;
				case "MenuEntity":
					toReturn.Add(Relations.MenuEntityUsingMenuId);
					break;
				case "PriceScheduleEntity":
					toReturn.Add(Relations.PriceScheduleEntityUsingPriceScheduleId);
					break;
				case "RoomControlConfigurationEntity":
					toReturn.Add(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
					break;
				case "UIModeEntity":
					toReturn.Add(Relations.UIModeEntityUsingUIModeId);
					break;
				case "UIScheduleEntity":
					toReturn.Add(Relations.UIScheduleEntityUsingUIScheduleId);
					break;
				case "UIThemeEntity":
					toReturn.Add(Relations.UIThemeEntityUsingUIThemeId);
					break;
				case "WifiConfigurationEntity":
					toReturn.Add(Relations.WifiConfigurationEntityUsingWifiConfigurationId);
					break;
				case "ClientConfigurationRouteCollection":
					toReturn.Add(Relations.ClientConfigurationRouteEntityUsingClientConfigurationId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingClientConfigurationId);
					break;
				case "DeliverypointCollection":
					toReturn.Add(Relations.DeliverypointEntityUsingClientConfigurationId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingClientConfigurationId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingClientConfigurationId);
					break;
				case "PmsActionRuleCollection":
					toReturn.Add(Relations.PmsActionRuleEntityUsingClientConfigurationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientConfigurationRouteCollection", (!this.MarkedForDeletion?_clientConfigurationRouteCollection:null));
			info.AddValue("_alwaysFetchClientConfigurationRouteCollection", _alwaysFetchClientConfigurationRouteCollection);
			info.AddValue("_alreadyFetchedClientConfigurationRouteCollection", _alreadyFetchedClientConfigurationRouteCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_deliverypointCollection", (!this.MarkedForDeletion?_deliverypointCollection:null));
			info.AddValue("_alwaysFetchDeliverypointCollection", _alwaysFetchDeliverypointCollection);
			info.AddValue("_alreadyFetchedDeliverypointCollection", _alreadyFetchedDeliverypointCollection);
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_pmsActionRuleCollection", (!this.MarkedForDeletion?_pmsActionRuleCollection:null));
			info.AddValue("_alwaysFetchPmsActionRuleCollection", _alwaysFetchPmsActionRuleCollection);
			info.AddValue("_alreadyFetchedPmsActionRuleCollection", _alreadyFetchedPmsActionRuleCollection);
			info.AddValue("_advertisementConfigurationEntity", (!this.MarkedForDeletion?_advertisementConfigurationEntity:null));
			info.AddValue("_advertisementConfigurationEntityReturnsNewIfNotFound", _advertisementConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementConfigurationEntity", _alwaysFetchAdvertisementConfigurationEntity);
			info.AddValue("_alreadyFetchedAdvertisementConfigurationEntity", _alreadyFetchedAdvertisementConfigurationEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_entertainmentConfigurationEntity", (!this.MarkedForDeletion?_entertainmentConfigurationEntity:null));
			info.AddValue("_entertainmentConfigurationEntityReturnsNewIfNotFound", _entertainmentConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentConfigurationEntity", _alwaysFetchEntertainmentConfigurationEntity);
			info.AddValue("_alreadyFetchedEntertainmentConfigurationEntity", _alreadyFetchedEntertainmentConfigurationEntity);
			info.AddValue("_menuEntity", (!this.MarkedForDeletion?_menuEntity:null));
			info.AddValue("_menuEntityReturnsNewIfNotFound", _menuEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMenuEntity", _alwaysFetchMenuEntity);
			info.AddValue("_alreadyFetchedMenuEntity", _alreadyFetchedMenuEntity);
			info.AddValue("_priceScheduleEntity", (!this.MarkedForDeletion?_priceScheduleEntity:null));
			info.AddValue("_priceScheduleEntityReturnsNewIfNotFound", _priceScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceScheduleEntity", _alwaysFetchPriceScheduleEntity);
			info.AddValue("_alreadyFetchedPriceScheduleEntity", _alreadyFetchedPriceScheduleEntity);
			info.AddValue("_roomControlConfigurationEntity", (!this.MarkedForDeletion?_roomControlConfigurationEntity:null));
			info.AddValue("_roomControlConfigurationEntityReturnsNewIfNotFound", _roomControlConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlConfigurationEntity", _alwaysFetchRoomControlConfigurationEntity);
			info.AddValue("_alreadyFetchedRoomControlConfigurationEntity", _alreadyFetchedRoomControlConfigurationEntity);
			info.AddValue("_uIModeEntity", (!this.MarkedForDeletion?_uIModeEntity:null));
			info.AddValue("_uIModeEntityReturnsNewIfNotFound", _uIModeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIModeEntity", _alwaysFetchUIModeEntity);
			info.AddValue("_alreadyFetchedUIModeEntity", _alreadyFetchedUIModeEntity);
			info.AddValue("_uIScheduleEntity", (!this.MarkedForDeletion?_uIScheduleEntity:null));
			info.AddValue("_uIScheduleEntityReturnsNewIfNotFound", _uIScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIScheduleEntity", _alwaysFetchUIScheduleEntity);
			info.AddValue("_alreadyFetchedUIScheduleEntity", _alreadyFetchedUIScheduleEntity);
			info.AddValue("_uIThemeEntity", (!this.MarkedForDeletion?_uIThemeEntity:null));
			info.AddValue("_uIThemeEntityReturnsNewIfNotFound", _uIThemeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIThemeEntity", _alwaysFetchUIThemeEntity);
			info.AddValue("_alreadyFetchedUIThemeEntity", _alreadyFetchedUIThemeEntity);
			info.AddValue("_wifiConfigurationEntity", (!this.MarkedForDeletion?_wifiConfigurationEntity:null));
			info.AddValue("_wifiConfigurationEntityReturnsNewIfNotFound", _wifiConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWifiConfigurationEntity", _alwaysFetchWifiConfigurationEntity);
			info.AddValue("_alreadyFetchedWifiConfigurationEntity", _alreadyFetchedWifiConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementConfigurationEntity":
					_alreadyFetchedAdvertisementConfigurationEntity = true;
					this.AdvertisementConfigurationEntity = (AdvertisementConfigurationEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "EntertainmentConfigurationEntity":
					_alreadyFetchedEntertainmentConfigurationEntity = true;
					this.EntertainmentConfigurationEntity = (EntertainmentConfigurationEntity)entity;
					break;
				case "MenuEntity":
					_alreadyFetchedMenuEntity = true;
					this.MenuEntity = (MenuEntity)entity;
					break;
				case "PriceScheduleEntity":
					_alreadyFetchedPriceScheduleEntity = true;
					this.PriceScheduleEntity = (PriceScheduleEntity)entity;
					break;
				case "RoomControlConfigurationEntity":
					_alreadyFetchedRoomControlConfigurationEntity = true;
					this.RoomControlConfigurationEntity = (RoomControlConfigurationEntity)entity;
					break;
				case "UIModeEntity":
					_alreadyFetchedUIModeEntity = true;
					this.UIModeEntity = (UIModeEntity)entity;
					break;
				case "UIScheduleEntity":
					_alreadyFetchedUIScheduleEntity = true;
					this.UIScheduleEntity = (UIScheduleEntity)entity;
					break;
				case "UIThemeEntity":
					_alreadyFetchedUIThemeEntity = true;
					this.UIThemeEntity = (UIThemeEntity)entity;
					break;
				case "WifiConfigurationEntity":
					_alreadyFetchedWifiConfigurationEntity = true;
					this.WifiConfigurationEntity = (WifiConfigurationEntity)entity;
					break;
				case "ClientConfigurationRouteCollection":
					_alreadyFetchedClientConfigurationRouteCollection = true;
					if(entity!=null)
					{
						this.ClientConfigurationRouteCollection.Add((ClientConfigurationRouteEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "DeliverypointCollection":
					_alreadyFetchedDeliverypointCollection = true;
					if(entity!=null)
					{
						this.DeliverypointCollection.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "PmsActionRuleCollection":
					_alreadyFetchedPmsActionRuleCollection = true;
					if(entity!=null)
					{
						this.PmsActionRuleCollection.Add((PmsActionRuleEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementConfigurationEntity":
					SetupSyncAdvertisementConfigurationEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "EntertainmentConfigurationEntity":
					SetupSyncEntertainmentConfigurationEntity(relatedEntity);
					break;
				case "MenuEntity":
					SetupSyncMenuEntity(relatedEntity);
					break;
				case "PriceScheduleEntity":
					SetupSyncPriceScheduleEntity(relatedEntity);
					break;
				case "RoomControlConfigurationEntity":
					SetupSyncRoomControlConfigurationEntity(relatedEntity);
					break;
				case "UIModeEntity":
					SetupSyncUIModeEntity(relatedEntity);
					break;
				case "UIScheduleEntity":
					SetupSyncUIScheduleEntity(relatedEntity);
					break;
				case "UIThemeEntity":
					SetupSyncUIThemeEntity(relatedEntity);
					break;
				case "WifiConfigurationEntity":
					SetupSyncWifiConfigurationEntity(relatedEntity);
					break;
				case "ClientConfigurationRouteCollection":
					_clientConfigurationRouteCollection.Add((ClientConfigurationRouteEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "DeliverypointCollection":
					_deliverypointCollection.Add((DeliverypointEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "PmsActionRuleCollection":
					_pmsActionRuleCollection.Add((PmsActionRuleEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementConfigurationEntity":
					DesetupSyncAdvertisementConfigurationEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "EntertainmentConfigurationEntity":
					DesetupSyncEntertainmentConfigurationEntity(false, true);
					break;
				case "MenuEntity":
					DesetupSyncMenuEntity(false, true);
					break;
				case "PriceScheduleEntity":
					DesetupSyncPriceScheduleEntity(false, true);
					break;
				case "RoomControlConfigurationEntity":
					DesetupSyncRoomControlConfigurationEntity(false, true);
					break;
				case "UIModeEntity":
					DesetupSyncUIModeEntity(false, true);
					break;
				case "UIScheduleEntity":
					DesetupSyncUIScheduleEntity(false, true);
					break;
				case "UIThemeEntity":
					DesetupSyncUIThemeEntity(false, true);
					break;
				case "WifiConfigurationEntity":
					DesetupSyncWifiConfigurationEntity(false, true);
					break;
				case "ClientConfigurationRouteCollection":
					this.PerformRelatedEntityRemoval(_clientConfigurationRouteCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointCollection":
					this.PerformRelatedEntityRemoval(_deliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PmsActionRuleCollection":
					this.PerformRelatedEntityRemoval(_pmsActionRuleCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_advertisementConfigurationEntity!=null)
			{
				toReturn.Add(_advertisementConfigurationEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_entertainmentConfigurationEntity!=null)
			{
				toReturn.Add(_entertainmentConfigurationEntity);
			}
			if(_menuEntity!=null)
			{
				toReturn.Add(_menuEntity);
			}
			if(_priceScheduleEntity!=null)
			{
				toReturn.Add(_priceScheduleEntity);
			}
			if(_roomControlConfigurationEntity!=null)
			{
				toReturn.Add(_roomControlConfigurationEntity);
			}
			if(_uIModeEntity!=null)
			{
				toReturn.Add(_uIModeEntity);
			}
			if(_uIScheduleEntity!=null)
			{
				toReturn.Add(_uIScheduleEntity);
			}
			if(_uIThemeEntity!=null)
			{
				toReturn.Add(_uIThemeEntity);
			}
			if(_wifiConfigurationEntity!=null)
			{
				toReturn.Add(_wifiConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientConfigurationRouteCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_deliverypointCollection);
			toReturn.Add(_deliverypointgroupCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_pmsActionRuleCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationId)
		{
			return FetchUsingPK(clientConfigurationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(clientConfigurationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(clientConfigurationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(clientConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ClientConfigurationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClientConfigurationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationRouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch)
		{
			return GetMultiClientConfigurationRouteCollection(forceFetch, _clientConfigurationRouteCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationRouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientConfigurationRouteCollection(forceFetch, _clientConfigurationRouteCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientConfigurationRouteCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientConfigurationRouteCollection || forceFetch || _alwaysFetchClientConfigurationRouteCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientConfigurationRouteCollection);
				_clientConfigurationRouteCollection.SuppressClearInGetMulti=!forceFetch;
				_clientConfigurationRouteCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientConfigurationRouteCollection.GetMultiManyToOne(this, null, filter);
				_clientConfigurationRouteCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientConfigurationRouteCollection = true;
			}
			return _clientConfigurationRouteCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientConfigurationRouteCollection'. These settings will be taken into account
		/// when the property ClientConfigurationRouteCollection is requested or GetMultiClientConfigurationRouteCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientConfigurationRouteCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientConfigurationRouteCollection.SortClauses=sortClauses;
			_clientConfigurationRouteCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointCollection || forceFetch || _alwaysFetchDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollection);
				_deliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollection.GetMultiManyToOne(this, null, null, null, null, null, null, filter);
				_deliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollection = true;
			}
			return _deliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollection'. These settings will be taken into account
		/// when the property DeliverypointCollection is requested or GetMultiDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollection.SortClauses=sortClauses;
			_deliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PmsActionRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch)
		{
			return GetMultiPmsActionRuleCollection(forceFetch, _pmsActionRuleCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PmsActionRuleEntity'</returns>
		public Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPmsActionRuleCollection(forceFetch, _pmsActionRuleCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPmsActionRuleCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PmsActionRuleCollection GetMultiPmsActionRuleCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPmsActionRuleCollection || forceFetch || _alwaysFetchPmsActionRuleCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pmsActionRuleCollection);
				_pmsActionRuleCollection.SuppressClearInGetMulti=!forceFetch;
				_pmsActionRuleCollection.EntityFactoryToUse = entityFactoryToUse;
				_pmsActionRuleCollection.GetMultiManyToOne(this, null, null, null, filter);
				_pmsActionRuleCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPmsActionRuleCollection = true;
			}
			return _pmsActionRuleCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PmsActionRuleCollection'. These settings will be taken into account
		/// when the property PmsActionRuleCollection is requested or GetMultiPmsActionRuleCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPmsActionRuleCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pmsActionRuleCollection.SortClauses=sortClauses;
			_pmsActionRuleCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementConfigurationEntity' which is related to this entity.</returns>
		public AdvertisementConfigurationEntity GetSingleAdvertisementConfigurationEntity()
		{
			return GetSingleAdvertisementConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementConfigurationEntity' which is related to this entity.</returns>
		public virtual AdvertisementConfigurationEntity GetSingleAdvertisementConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementConfigurationEntity || forceFetch || _alwaysFetchAdvertisementConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
				AdvertisementConfigurationEntity newEntity = new AdvertisementConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementConfigurationEntity = newEntity;
				_alreadyFetchedAdvertisementConfigurationEntity = fetchResult;
			}
			return _advertisementConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentConfigurationEntity' which is related to this entity.</returns>
		public EntertainmentConfigurationEntity GetSingleEntertainmentConfigurationEntity()
		{
			return GetSingleEntertainmentConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentConfigurationEntity' which is related to this entity.</returns>
		public virtual EntertainmentConfigurationEntity GetSingleEntertainmentConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentConfigurationEntity || forceFetch || _alwaysFetchEntertainmentConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
				EntertainmentConfigurationEntity newEntity = new EntertainmentConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentConfigurationEntity = newEntity;
				_alreadyFetchedEntertainmentConfigurationEntity = fetchResult;
			}
			return _entertainmentConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public MenuEntity GetSingleMenuEntity()
		{
			return GetSingleMenuEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public virtual MenuEntity GetSingleMenuEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMenuEntity || forceFetch || _alwaysFetchMenuEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MenuEntityUsingMenuId);
				MenuEntity newEntity = new MenuEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MenuId);
				}
				if(fetchResult)
				{
					newEntity = (MenuEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_menuEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MenuEntity = newEntity;
				_alreadyFetchedMenuEntity = fetchResult;
			}
			return _menuEntity;
		}


		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public PriceScheduleEntity GetSinglePriceScheduleEntity()
		{
			return GetSinglePriceScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public virtual PriceScheduleEntity GetSinglePriceScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceScheduleEntity || forceFetch || _alwaysFetchPriceScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceScheduleEntityUsingPriceScheduleId);
				PriceScheduleEntity newEntity = new PriceScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PriceScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceScheduleEntity = newEntity;
				_alreadyFetchedPriceScheduleEntity = fetchResult;
			}
			return _priceScheduleEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity()
		{
			return GetSingleRoomControlConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public virtual RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlConfigurationEntity || forceFetch || _alwaysFetchRoomControlConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
				RoomControlConfigurationEntity newEntity = new RoomControlConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlConfigurationEntity = newEntity;
				_alreadyFetchedRoomControlConfigurationEntity = fetchResult;
			}
			return _roomControlConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public UIModeEntity GetSingleUIModeEntity()
		{
			return GetSingleUIModeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public virtual UIModeEntity GetSingleUIModeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIModeEntity || forceFetch || _alwaysFetchUIModeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIModeEntityUsingUIModeId);
				UIModeEntity newEntity = new UIModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIModeId);
				}
				if(fetchResult)
				{
					newEntity = (UIModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIModeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIModeEntity = newEntity;
				_alreadyFetchedUIModeEntity = fetchResult;
			}
			return _uIModeEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public UIScheduleEntity GetSingleUIScheduleEntity()
		{
			return GetSingleUIScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public virtual UIScheduleEntity GetSingleUIScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIScheduleEntity || forceFetch || _alwaysFetchUIScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIScheduleEntityUsingUIScheduleId);
				UIScheduleEntity newEntity = new UIScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIScheduleEntity = newEntity;
				_alreadyFetchedUIScheduleEntity = fetchResult;
			}
			return _uIScheduleEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public UIThemeEntity GetSingleUIThemeEntity()
		{
			return GetSingleUIThemeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public virtual UIThemeEntity GetSingleUIThemeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIThemeEntity || forceFetch || _alwaysFetchUIThemeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIThemeEntityUsingUIThemeId);
				UIThemeEntity newEntity = new UIThemeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIThemeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIThemeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIThemeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIThemeEntity = newEntity;
				_alreadyFetchedUIThemeEntity = fetchResult;
			}
			return _uIThemeEntity;
		}


		/// <summary> Retrieves the related entity of type 'WifiConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WifiConfigurationEntity' which is related to this entity.</returns>
		public WifiConfigurationEntity GetSingleWifiConfigurationEntity()
		{
			return GetSingleWifiConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WifiConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WifiConfigurationEntity' which is related to this entity.</returns>
		public virtual WifiConfigurationEntity GetSingleWifiConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWifiConfigurationEntity || forceFetch || _alwaysFetchWifiConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WifiConfigurationEntityUsingWifiConfigurationId);
				WifiConfigurationEntity newEntity = new WifiConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.WifiConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (WifiConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_wifiConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WifiConfigurationEntity = newEntity;
				_alreadyFetchedWifiConfigurationEntity = fetchResult;
			}
			return _wifiConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementConfigurationEntity", _advertisementConfigurationEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("EntertainmentConfigurationEntity", _entertainmentConfigurationEntity);
			toReturn.Add("MenuEntity", _menuEntity);
			toReturn.Add("PriceScheduleEntity", _priceScheduleEntity);
			toReturn.Add("RoomControlConfigurationEntity", _roomControlConfigurationEntity);
			toReturn.Add("UIModeEntity", _uIModeEntity);
			toReturn.Add("UIScheduleEntity", _uIScheduleEntity);
			toReturn.Add("UIThemeEntity", _uIThemeEntity);
			toReturn.Add("WifiConfigurationEntity", _wifiConfigurationEntity);
			toReturn.Add("ClientConfigurationRouteCollection", _clientConfigurationRouteCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("DeliverypointCollection", _deliverypointCollection);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("PmsActionRuleCollection", _pmsActionRuleCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="validator">The validator object for this ClientConfigurationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 clientConfigurationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(clientConfigurationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientConfigurationRouteCollection = new Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection();
			_clientConfigurationRouteCollection.SetContainingEntityInfo(this, "ClientConfigurationEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "ClientConfigurationEntity");

			_deliverypointCollection = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollection.SetContainingEntityInfo(this, "ClientConfigurationEntity");

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "ClientConfigurationEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "ClientConfigurationEntity");

			_pmsActionRuleCollection = new Obymobi.Data.CollectionClasses.PmsActionRuleCollection();
			_pmsActionRuleCollection.SetContainingEntityInfo(this, "ClientConfigurationEntity");
			_advertisementConfigurationEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_entertainmentConfigurationEntityReturnsNewIfNotFound = true;
			_menuEntityReturnsNewIfNotFound = true;
			_priceScheduleEntityReturnsNewIfNotFound = true;
			_roomControlConfigurationEntityReturnsNewIfNotFound = true;
			_uIModeEntityReturnsNewIfNotFound = true;
			_uIScheduleEntityReturnsNewIfNotFound = true;
			_uIThemeEntityReturnsNewIfNotFound = true;
			_wifiConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIThemeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Pincode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PincodeSU", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PincodeGM", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HidePrices", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowCurrencySymbol", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearSessionOnTimeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResetTimeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreenTimeoutInterval", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DimLevelDull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DimLevelMedium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DimLevelBright", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DockedDimLevelDull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DockedDimLevelMedium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DockedDimLevelBright", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerSaveTimeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerSaveLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DailyOrderReset", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SleepTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WakeUpTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HomepageSlideshowInterval", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsChargerRemovedDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsChargerRemovedReminderDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerButtonHardBehaviour", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerButtonSoftBehaviour", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreenOffMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreensaverMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceRebootMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomserviceCharge", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowFreeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrowserAgeVerificationEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GooglePrinterId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrowserAgeVerificationLayout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RestartApplicationDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RebootDeviceDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RestartTimeoutSeconds", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsIntegration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsAllowShowBill", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsAllowExpressCheckout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsAllowShowGuestName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsLockClientWhenNotCheckedIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsWelcomeTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutCompleteTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsOrderitemAddedDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompletedNotificationEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsClearBasketDialogEnabled", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _advertisementConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementConfigurationEntity, new PropertyChangedEventHandler( OnAdvertisementConfigurationEntityPropertyChanged ), "AdvertisementConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.AdvertisementConfigurationId } );		
			_advertisementConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _advertisementConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_advertisementConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncAdvertisementConfigurationEntity(true, true);
				_advertisementConfigurationEntity = (AdvertisementConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementConfigurationEntity, new PropertyChangedEventHandler( OnAdvertisementConfigurationEntityPropertyChanged ), "AdvertisementConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic, true, ref _alreadyFetchedAdvertisementConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentConfigurationEntity, new PropertyChangedEventHandler( OnEntertainmentConfigurationEntityPropertyChanged ), "EntertainmentConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.EntertainmentConfigurationId } );		
			_entertainmentConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentConfigurationEntity(true, true);
				_entertainmentConfigurationEntity = (EntertainmentConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentConfigurationEntity, new PropertyChangedEventHandler( OnEntertainmentConfigurationEntityPropertyChanged ), "EntertainmentConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic, true, ref _alreadyFetchedEntertainmentConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _menuEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMenuEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.MenuEntityUsingMenuIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.MenuId } );		
			_menuEntity = null;
		}
		
		/// <summary> setups the sync logic for member _menuEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMenuEntity(IEntityCore relatedEntity)
		{
			if(_menuEntity!=relatedEntity)
			{		
				DesetupSyncMenuEntity(true, true);
				_menuEntity = (MenuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.MenuEntityUsingMenuIdStatic, true, ref _alreadyFetchedMenuEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMenuEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.PriceScheduleId } );		
			_priceScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceScheduleEntity(IEntityCore relatedEntity)
		{
			if(_priceScheduleEntity!=relatedEntity)
			{		
				DesetupSyncPriceScheduleEntity(true, true);
				_priceScheduleEntity = (PriceScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, ref _alreadyFetchedPriceScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.RoomControlConfigurationId } );		
			_roomControlConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_roomControlConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlConfigurationEntity(true, true);
				_roomControlConfigurationEntity = (RoomControlConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, ref _alreadyFetchedRoomControlConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIModeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIModeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.UIModeEntityUsingUIModeIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.UIModeId } );		
			_uIModeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIModeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIModeEntity(IEntityCore relatedEntity)
		{
			if(_uIModeEntity!=relatedEntity)
			{		
				DesetupSyncUIModeEntity(true, true);
				_uIModeEntity = (UIModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.UIModeEntityUsingUIModeIdStatic, true, ref _alreadyFetchedUIModeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIModeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.UIScheduleId } );		
			_uIScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIScheduleEntity(IEntityCore relatedEntity)
		{
			if(_uIScheduleEntity!=relatedEntity)
			{		
				DesetupSyncUIScheduleEntity(true, true);
				_uIScheduleEntity = (UIScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, ref _alreadyFetchedUIScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIThemeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIThemeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.UIThemeEntityUsingUIThemeIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.UIThemeId } );		
			_uIThemeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIThemeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIThemeEntity(IEntityCore relatedEntity)
		{
			if(_uIThemeEntity!=relatedEntity)
			{		
				DesetupSyncUIThemeEntity(true, true);
				_uIThemeEntity = (UIThemeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.UIThemeEntityUsingUIThemeIdStatic, true, ref _alreadyFetchedUIThemeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIThemeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _wifiConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWifiConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _wifiConfigurationEntity, new PropertyChangedEventHandler( OnWifiConfigurationEntityPropertyChanged ), "WifiConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.WifiConfigurationEntityUsingWifiConfigurationIdStatic, true, signalRelatedEntity, "ClientConfigurationCollection", resetFKFields, new int[] { (int)ClientConfigurationFieldIndex.WifiConfigurationId } );		
			_wifiConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _wifiConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWifiConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_wifiConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncWifiConfigurationEntity(true, true);
				_wifiConfigurationEntity = (WifiConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _wifiConfigurationEntity, new PropertyChangedEventHandler( OnWifiConfigurationEntityPropertyChanged ), "WifiConfigurationEntity", Obymobi.Data.RelationClasses.StaticClientConfigurationRelations.WifiConfigurationEntityUsingWifiConfigurationIdStatic, true, ref _alreadyFetchedWifiConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWifiConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="clientConfigurationId">PK value for ClientConfiguration which data should be fetched into this ClientConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 clientConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ClientConfigurationFieldIndex.ClientConfigurationId].ForcedCurrentValueWrite(clientConfigurationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateClientConfigurationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ClientConfigurationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClientConfigurationRelations Relations
		{
			get	{ return new ClientConfigurationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfigurationRoute' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationRouteCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationRouteCollection")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.ClientConfigurationRouteEntity, 0, null, null, null, "ClientConfigurationRouteCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointCollection")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PmsActionRule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPmsActionRuleCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PmsActionRuleCollection(), (IEntityRelation)GetRelationsForField("PmsActionRuleCollection")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.PmsActionRuleEntity, 0, null, null, null, "PmsActionRuleCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementConfigurationCollection(), (IEntityRelation)GetRelationsForField("AdvertisementConfigurationEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.AdvertisementConfigurationEntity, 0, null, null, null, "AdvertisementConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntertainmentConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentConfigurationCollection(), (IEntityRelation)GetRelationsForField("EntertainmentConfigurationEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.EntertainmentConfigurationEntity, 0, null, null, null, "EntertainmentConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), (IEntityRelation)GetRelationsForField("MenuEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, null, "MenuEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceSchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceScheduleCollection(), (IEntityRelation)GetRelationsForField("PriceScheduleEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.PriceScheduleEntity, 0, null, null, null, "PriceScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlConfigurationCollection(), (IEntityRelation)GetRelationsForField("RoomControlConfigurationEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.RoomControlConfigurationEntity, 0, null, null, null, "RoomControlConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("UIModeEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "UIModeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UISchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleCollection(), (IEntityRelation)GetRelationsForField("UIScheduleEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.UIScheduleEntity, 0, null, null, null, "UIScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITheme'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIThemeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIThemeCollection(), (IEntityRelation)GetRelationsForField("UIThemeEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.UIThemeEntity, 0, null, null, null, "UIThemeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WifiConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWifiConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WifiConfigurationCollection(), (IEntityRelation)GetRelationsForField("WifiConfigurationEntity")[0], (int)Obymobi.Data.EntityType.ClientConfigurationEntity, (int)Obymobi.Data.EntityType.WifiConfigurationEntity, 0, null, null, null, "WifiConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientConfigurationId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientConfigurationId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.ClientConfigurationId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.Name, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.Name, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The MenuId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."MenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MenuId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.MenuId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.MenuId, value, true); }
		}

		/// <summary> The UIModeId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UIModeId
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.UIModeId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UIModeId, value, true); }
		}

		/// <summary> The UIThemeId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UIThemeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIThemeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.UIThemeId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UIThemeId, value, true); }
		}

		/// <summary> The UIScheduleId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UIScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.UIScheduleId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UIScheduleId, value, true); }
		}

		/// <summary> The RoomControlConfigurationId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RoomControlConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.RoomControlConfigurationId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RoomControlConfigurationId, value, true); }
		}

		/// <summary> The WifiConfigurationId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."WifiConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WifiConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.WifiConfigurationId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.WifiConfigurationId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientConfigurationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Pincode property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."Pincode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Pincode
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.Pincode, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.Pincode, value, true); }
		}

		/// <summary> The PincodeSU property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PincodeSU"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeSU
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.PincodeSU, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PincodeSU, value, true); }
		}

		/// <summary> The PincodeGM property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PincodeGM"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeGM
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.PincodeGM, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PincodeGM, value, true); }
		}

		/// <summary> The HidePrices property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."HidePrices"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrices
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.HidePrices, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.HidePrices, value, true); }
		}

		/// <summary> The ShowCurrencySymbol property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ShowCurrencySymbol"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowCurrencySymbol
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.ShowCurrencySymbol, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ShowCurrencySymbol, value, true); }
		}

		/// <summary> The ClearSessionOnTimeout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ClearSessionOnTimeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ClearSessionOnTimeout
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.ClearSessionOnTimeout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ClearSessionOnTimeout, value, true); }
		}

		/// <summary> The ResetTimeout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ResetTimeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResetTimeout
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.ResetTimeout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ResetTimeout, value, true); }
		}

		/// <summary> The ScreenTimeoutInterval property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ScreenTimeoutInterval"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenTimeoutInterval
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.ScreenTimeoutInterval, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ScreenTimeoutInterval, value, true); }
		}

		/// <summary> The DimLevelDull property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DimLevelDull"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelDull
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DimLevelDull, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DimLevelDull, value, true); }
		}

		/// <summary> The DimLevelMedium property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DimLevelMedium"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelMedium
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DimLevelMedium, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DimLevelMedium, value, true); }
		}

		/// <summary> The DimLevelBright property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DimLevelBright"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelBright
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DimLevelBright, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DimLevelBright, value, true); }
		}

		/// <summary> The DockedDimLevelDull property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DockedDimLevelDull"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelDull
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DockedDimLevelDull, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DockedDimLevelDull, value, true); }
		}

		/// <summary> The DockedDimLevelMedium property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DockedDimLevelMedium"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelMedium
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DockedDimLevelMedium, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DockedDimLevelMedium, value, true); }
		}

		/// <summary> The DockedDimLevelBright property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DockedDimLevelBright"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelBright
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DockedDimLevelBright, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DockedDimLevelBright, value, true); }
		}

		/// <summary> The PowerSaveTimeout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerSaveTimeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveTimeout
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PowerSaveTimeout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerSaveTimeout, value, true); }
		}

		/// <summary> The PowerSaveLevel property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerSaveLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveLevel
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PowerSaveLevel, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerSaveLevel, value, true); }
		}

		/// <summary> The OutOfChargeLevel property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."OutOfChargeLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeLevel
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.OutOfChargeLevel, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.OutOfChargeLevel, value, true); }
		}

		/// <summary> The DailyOrderReset property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DailyOrderReset"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DailyOrderReset
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.DailyOrderReset, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DailyOrderReset, value, true); }
		}

		/// <summary> The SleepTime property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."SleepTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SleepTime
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.SleepTime, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.SleepTime, value, true); }
		}

		/// <summary> The WakeUpTime property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."WakeUpTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String WakeUpTime
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.WakeUpTime, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.WakeUpTime, value, true); }
		}

		/// <summary> The HomepageSlideshowInterval property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."HomepageSlideshowInterval"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HomepageSlideshowInterval
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.HomepageSlideshowInterval, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.HomepageSlideshowInterval, value, true); }
		}

		/// <summary> The IsChargerRemovedDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsChargerRemovedDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedDialogEnabled, value, true); }
		}

		/// <summary> The IsChargerRemovedReminderDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsChargerRemovedReminderDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedReminderDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedReminderDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsChargerRemovedReminderDialogEnabled, value, true); }
		}

		/// <summary> The PowerButtonHardBehaviour property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerButtonHardBehaviour"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerButtonHardBehaviour
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PowerButtonHardBehaviour, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerButtonHardBehaviour, value, true); }
		}

		/// <summary> The PowerButtonSoftBehaviour property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PowerButtonSoftBehaviour"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerButtonSoftBehaviour
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PowerButtonSoftBehaviour, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PowerButtonSoftBehaviour, value, true); }
		}

		/// <summary> The ScreenOffMode property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ScreenOffMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenOffMode
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.ScreenOffMode, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ScreenOffMode, value, true); }
		}

		/// <summary> The ScreensaverMode property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."ScreensaverMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ScreensaverMode ScreensaverMode
		{
			get { return (Obymobi.Enums.ScreensaverMode)GetValue((int)ClientConfigurationFieldIndex.ScreensaverMode, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.ScreensaverMode, value, true); }
		}

		/// <summary> The DeviceRebootMethod property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."DeviceRebootMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceRebootMethod
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.DeviceRebootMethod, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.DeviceRebootMethod, value, true); }
		}

		/// <summary> The RoomserviceCharge property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RoomserviceCharge"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> RoomserviceCharge
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ClientConfigurationFieldIndex.RoomserviceCharge, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RoomserviceCharge, value, true); }
		}

		/// <summary> The AllowFreeText property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."AllowFreeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowFreeText
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.AllowFreeText, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.AllowFreeText, value, true); }
		}

		/// <summary> The BrowserAgeVerificationEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."BrowserAgeVerificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BrowserAgeVerificationEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationEnabled, value, true); }
		}

		/// <summary> The GooglePrinterId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."GooglePrinterId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterId
		{
			get { return (System.String)GetValue((int)ClientConfigurationFieldIndex.GooglePrinterId, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.GooglePrinterId, value, true); }
		}

		/// <summary> The PriceScheduleId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PriceScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.PriceScheduleId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PriceScheduleId, value, true); }
		}

		/// <summary> The AdvertisementConfigurationId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."AdvertisementConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AdvertisementConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.AdvertisementConfigurationId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.AdvertisementConfigurationId, value, true); }
		}

		/// <summary> The EntertainmentConfigurationId property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."EntertainmentConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientConfigurationFieldIndex.EntertainmentConfigurationId, false); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.EntertainmentConfigurationId, value, true); }
		}

		/// <summary> The BrowserAgeVerificationLayout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."BrowserAgeVerificationLayout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.BrowserAgeVerificationLayoutType BrowserAgeVerificationLayout
		{
			get { return (Obymobi.Enums.BrowserAgeVerificationLayoutType)GetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationLayout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.BrowserAgeVerificationLayout, value, true); }
		}

		/// <summary> The RestartApplicationDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RestartApplicationDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RestartApplicationDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.RestartApplicationDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RestartApplicationDialogEnabled, value, true); }
		}

		/// <summary> The RebootDeviceDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RebootDeviceDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RebootDeviceDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.RebootDeviceDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RebootDeviceDialogEnabled, value, true); }
		}

		/// <summary> The RestartTimeoutSeconds property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."RestartTimeoutSeconds"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RestartTimeoutSeconds
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.RestartTimeoutSeconds, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.RestartTimeoutSeconds, value, true); }
		}

		/// <summary> The PmsIntegration property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsIntegration"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsIntegration
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsIntegration, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsIntegration, value, true); }
		}

		/// <summary> The PmsAllowShowBill property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsAllowShowBill"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowBill
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsAllowShowBill, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsAllowShowBill, value, true); }
		}

		/// <summary> The PmsAllowExpressCheckout property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsAllowExpressCheckout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowExpressCheckout
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsAllowExpressCheckout, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsAllowExpressCheckout, value, true); }
		}

		/// <summary> The PmsAllowShowGuestName property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsAllowShowGuestName"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowGuestName
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsAllowShowGuestName, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsAllowShowGuestName, value, true); }
		}

		/// <summary> The PmsLockClientWhenNotCheckedIn property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsLockClientWhenNotCheckedIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsLockClientWhenNotCheckedIn
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.PmsLockClientWhenNotCheckedIn, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsLockClientWhenNotCheckedIn, value, true); }
		}

		/// <summary> The PmsWelcomeTimeoutMinutes property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsWelcomeTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsWelcomeTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PmsWelcomeTimeoutMinutes, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsWelcomeTimeoutMinutes, value, true); }
		}

		/// <summary> The PmsCheckoutCompleteTimeoutMinutes property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."PmsCheckoutCompleteTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsCheckoutCompleteTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)ClientConfigurationFieldIndex.PmsCheckoutCompleteTimeoutMinutes, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.PmsCheckoutCompleteTimeoutMinutes, value, true); }
		}

		/// <summary> The IsOrderitemAddedDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsOrderitemAddedDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsOrderitemAddedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsOrderitemAddedDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsOrderitemAddedDialogEnabled, value, true); }
		}

		/// <summary> The OrderCompletedNotificationEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."OrderCompletedNotificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderCompletedNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.OrderCompletedNotificationEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.OrderCompletedNotificationEnabled, value, true); }
		}

		/// <summary> The IsClearBasketDialogEnabled property of the Entity ClientConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientConfiguration"."IsClearBasketDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsClearBasketDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)ClientConfigurationFieldIndex.IsClearBasketDialogEnabled, true); }
			set	{ SetValue((int)ClientConfigurationFieldIndex.IsClearBasketDialogEnabled, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientConfigurationRouteCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection ClientConfigurationRouteCollection
		{
			get	{ return GetMultiClientConfigurationRouteCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationRouteCollection. When set to true, ClientConfigurationRouteCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationRouteCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientConfigurationRouteCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationRouteCollection
		{
			get	{ return _alwaysFetchClientConfigurationRouteCollection; }
			set	{ _alwaysFetchClientConfigurationRouteCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationRouteCollection already has been fetched. Setting this property to false when ClientConfigurationRouteCollection has been fetched
		/// will clear the ClientConfigurationRouteCollection collection well. Setting this property to true while ClientConfigurationRouteCollection hasn't been fetched disables lazy loading for ClientConfigurationRouteCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationRouteCollection
		{
			get { return _alreadyFetchedClientConfigurationRouteCollection;}
			set 
			{
				if(_alreadyFetchedClientConfigurationRouteCollection && !value && (_clientConfigurationRouteCollection != null))
				{
					_clientConfigurationRouteCollection.Clear();
				}
				_alreadyFetchedClientConfigurationRouteCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollection
		{
			get	{ return GetMultiDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollection. When set to true, DeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollection
		{
			get	{ return _alwaysFetchDeliverypointCollection; }
			set	{ _alwaysFetchDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollection already has been fetched. Setting this property to false when DeliverypointCollection has been fetched
		/// will clear the DeliverypointCollection collection well. Setting this property to true while DeliverypointCollection hasn't been fetched disables lazy loading for DeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollection
		{
			get { return _alreadyFetchedDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollection && !value && (_deliverypointCollection != null))
				{
					_deliverypointCollection.Clear();
				}
				_alreadyFetchedDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PmsActionRuleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPmsActionRuleCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PmsActionRuleCollection PmsActionRuleCollection
		{
			get	{ return GetMultiPmsActionRuleCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PmsActionRuleCollection. When set to true, PmsActionRuleCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PmsActionRuleCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPmsActionRuleCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPmsActionRuleCollection
		{
			get	{ return _alwaysFetchPmsActionRuleCollection; }
			set	{ _alwaysFetchPmsActionRuleCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PmsActionRuleCollection already has been fetched. Setting this property to false when PmsActionRuleCollection has been fetched
		/// will clear the PmsActionRuleCollection collection well. Setting this property to true while PmsActionRuleCollection hasn't been fetched disables lazy loading for PmsActionRuleCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPmsActionRuleCollection
		{
			get { return _alreadyFetchedPmsActionRuleCollection;}
			set 
			{
				if(_alreadyFetchedPmsActionRuleCollection && !value && (_pmsActionRuleCollection != null))
				{
					_pmsActionRuleCollection.Clear();
				}
				_alreadyFetchedPmsActionRuleCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AdvertisementConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementConfigurationEntity AdvertisementConfigurationEntity
		{
			get	{ return GetSingleAdvertisementConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "AdvertisementConfigurationEntity", _advertisementConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementConfigurationEntity. When set to true, AdvertisementConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementConfigurationEntity
		{
			get	{ return _alwaysFetchAdvertisementConfigurationEntity; }
			set	{ _alwaysFetchAdvertisementConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementConfigurationEntity already has been fetched. Setting this property to false when AdvertisementConfigurationEntity has been fetched
		/// will set AdvertisementConfigurationEntity to null as well. Setting this property to true while AdvertisementConfigurationEntity hasn't been fetched disables lazy loading for AdvertisementConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementConfigurationEntity
		{
			get { return _alreadyFetchedAdvertisementConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementConfigurationEntity && !value)
				{
					this.AdvertisementConfigurationEntity = null;
				}
				_alreadyFetchedAdvertisementConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementConfigurationEntity is not found
		/// in the database. When set to true, AdvertisementConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementConfigurationEntityReturnsNewIfNotFound; }
			set { _advertisementConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentConfigurationEntity EntertainmentConfigurationEntity
		{
			get	{ return GetSingleEntertainmentConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "EntertainmentConfigurationEntity", _entertainmentConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentConfigurationEntity. When set to true, EntertainmentConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentConfigurationEntity
		{
			get	{ return _alwaysFetchEntertainmentConfigurationEntity; }
			set	{ _alwaysFetchEntertainmentConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentConfigurationEntity already has been fetched. Setting this property to false when EntertainmentConfigurationEntity has been fetched
		/// will set EntertainmentConfigurationEntity to null as well. Setting this property to true while EntertainmentConfigurationEntity hasn't been fetched disables lazy loading for EntertainmentConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentConfigurationEntity
		{
			get { return _alreadyFetchedEntertainmentConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentConfigurationEntity && !value)
				{
					this.EntertainmentConfigurationEntity = null;
				}
				_alreadyFetchedEntertainmentConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentConfigurationEntity is not found
		/// in the database. When set to true, EntertainmentConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentConfigurationEntityReturnsNewIfNotFound; }
			set { _entertainmentConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MenuEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMenuEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MenuEntity MenuEntity
		{
			get	{ return GetSingleMenuEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMenuEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "MenuEntity", _menuEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MenuEntity. When set to true, MenuEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuEntity is accessed. You can always execute a forced fetch by calling GetSingleMenuEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuEntity
		{
			get	{ return _alwaysFetchMenuEntity; }
			set	{ _alwaysFetchMenuEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuEntity already has been fetched. Setting this property to false when MenuEntity has been fetched
		/// will set MenuEntity to null as well. Setting this property to true while MenuEntity hasn't been fetched disables lazy loading for MenuEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuEntity
		{
			get { return _alreadyFetchedMenuEntity;}
			set 
			{
				if(_alreadyFetchedMenuEntity && !value)
				{
					this.MenuEntity = null;
				}
				_alreadyFetchedMenuEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MenuEntity is not found
		/// in the database. When set to true, MenuEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MenuEntityReturnsNewIfNotFound
		{
			get	{ return _menuEntityReturnsNewIfNotFound; }
			set { _menuEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PriceScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceScheduleEntity PriceScheduleEntity
		{
			get	{ return GetSinglePriceScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "PriceScheduleEntity", _priceScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceScheduleEntity. When set to true, PriceScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceScheduleEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceScheduleEntity
		{
			get	{ return _alwaysFetchPriceScheduleEntity; }
			set	{ _alwaysFetchPriceScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceScheduleEntity already has been fetched. Setting this property to false when PriceScheduleEntity has been fetched
		/// will set PriceScheduleEntity to null as well. Setting this property to true while PriceScheduleEntity hasn't been fetched disables lazy loading for PriceScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceScheduleEntity
		{
			get { return _alreadyFetchedPriceScheduleEntity;}
			set 
			{
				if(_alreadyFetchedPriceScheduleEntity && !value)
				{
					this.PriceScheduleEntity = null;
				}
				_alreadyFetchedPriceScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceScheduleEntity is not found
		/// in the database. When set to true, PriceScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _priceScheduleEntityReturnsNewIfNotFound; }
			set { _priceScheduleEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlConfigurationEntity RoomControlConfigurationEntity
		{
			get	{ return GetSingleRoomControlConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "RoomControlConfigurationEntity", _roomControlConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlConfigurationEntity. When set to true, RoomControlConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlConfigurationEntity
		{
			get	{ return _alwaysFetchRoomControlConfigurationEntity; }
			set	{ _alwaysFetchRoomControlConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlConfigurationEntity already has been fetched. Setting this property to false when RoomControlConfigurationEntity has been fetched
		/// will set RoomControlConfigurationEntity to null as well. Setting this property to true while RoomControlConfigurationEntity hasn't been fetched disables lazy loading for RoomControlConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlConfigurationEntity
		{
			get { return _alreadyFetchedRoomControlConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlConfigurationEntity && !value)
				{
					this.RoomControlConfigurationEntity = null;
				}
				_alreadyFetchedRoomControlConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlConfigurationEntity is not found
		/// in the database. When set to true, RoomControlConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlConfigurationEntityReturnsNewIfNotFound; }
			set { _roomControlConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIModeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIModeEntity UIModeEntity
		{
			get	{ return GetSingleUIModeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIModeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "UIModeEntity", _uIModeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeEntity. When set to true, UIModeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIModeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeEntity
		{
			get	{ return _alwaysFetchUIModeEntity; }
			set	{ _alwaysFetchUIModeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeEntity already has been fetched. Setting this property to false when UIModeEntity has been fetched
		/// will set UIModeEntity to null as well. Setting this property to true while UIModeEntity hasn't been fetched disables lazy loading for UIModeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeEntity
		{
			get { return _alreadyFetchedUIModeEntity;}
			set 
			{
				if(_alreadyFetchedUIModeEntity && !value)
				{
					this.UIModeEntity = null;
				}
				_alreadyFetchedUIModeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIModeEntity is not found
		/// in the database. When set to true, UIModeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIModeEntityReturnsNewIfNotFound
		{
			get	{ return _uIModeEntityReturnsNewIfNotFound; }
			set { _uIModeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIScheduleEntity UIScheduleEntity
		{
			get	{ return GetSingleUIScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "UIScheduleEntity", _uIScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleEntity. When set to true, UIScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleEntity is accessed. You can always execute a forced fetch by calling GetSingleUIScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleEntity
		{
			get	{ return _alwaysFetchUIScheduleEntity; }
			set	{ _alwaysFetchUIScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleEntity already has been fetched. Setting this property to false when UIScheduleEntity has been fetched
		/// will set UIScheduleEntity to null as well. Setting this property to true while UIScheduleEntity hasn't been fetched disables lazy loading for UIScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleEntity
		{
			get { return _alreadyFetchedUIScheduleEntity;}
			set 
			{
				if(_alreadyFetchedUIScheduleEntity && !value)
				{
					this.UIScheduleEntity = null;
				}
				_alreadyFetchedUIScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIScheduleEntity is not found
		/// in the database. When set to true, UIScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _uIScheduleEntityReturnsNewIfNotFound; }
			set { _uIScheduleEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIThemeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIThemeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIThemeEntity UIThemeEntity
		{
			get	{ return GetSingleUIThemeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIThemeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "UIThemeEntity", _uIThemeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIThemeEntity. When set to true, UIThemeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIThemeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIThemeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIThemeEntity
		{
			get	{ return _alwaysFetchUIThemeEntity; }
			set	{ _alwaysFetchUIThemeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIThemeEntity already has been fetched. Setting this property to false when UIThemeEntity has been fetched
		/// will set UIThemeEntity to null as well. Setting this property to true while UIThemeEntity hasn't been fetched disables lazy loading for UIThemeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIThemeEntity
		{
			get { return _alreadyFetchedUIThemeEntity;}
			set 
			{
				if(_alreadyFetchedUIThemeEntity && !value)
				{
					this.UIThemeEntity = null;
				}
				_alreadyFetchedUIThemeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIThemeEntity is not found
		/// in the database. When set to true, UIThemeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIThemeEntityReturnsNewIfNotFound
		{
			get	{ return _uIThemeEntityReturnsNewIfNotFound; }
			set { _uIThemeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WifiConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWifiConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WifiConfigurationEntity WifiConfigurationEntity
		{
			get	{ return GetSingleWifiConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWifiConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientConfigurationCollection", "WifiConfigurationEntity", _wifiConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WifiConfigurationEntity. When set to true, WifiConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WifiConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleWifiConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWifiConfigurationEntity
		{
			get	{ return _alwaysFetchWifiConfigurationEntity; }
			set	{ _alwaysFetchWifiConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WifiConfigurationEntity already has been fetched. Setting this property to false when WifiConfigurationEntity has been fetched
		/// will set WifiConfigurationEntity to null as well. Setting this property to true while WifiConfigurationEntity hasn't been fetched disables lazy loading for WifiConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWifiConfigurationEntity
		{
			get { return _alreadyFetchedWifiConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedWifiConfigurationEntity && !value)
				{
					this.WifiConfigurationEntity = null;
				}
				_alreadyFetchedWifiConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WifiConfigurationEntity is not found
		/// in the database. When set to true, WifiConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WifiConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _wifiConfigurationEntityReturnsNewIfNotFound; }
			set { _wifiConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ClientConfigurationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
