﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'OutletSellerInformation'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class OutletSellerInformationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OutletSellerInformationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.OutletCollection	_outletCollection;
		private bool	_alwaysFetchOutletCollection, _alreadyFetchedOutletCollection;
		private Obymobi.Data.CollectionClasses.ReceiptCollection	_receiptCollection;
		private bool	_alwaysFetchReceiptCollection, _alreadyFetchedReceiptCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OutletCollection</summary>
			public static readonly string OutletCollection = "OutletCollection";
			/// <summary>Member name ReceiptCollection</summary>
			public static readonly string ReceiptCollection = "ReceiptCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OutletSellerInformationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OutletSellerInformationEntityBase() :base("OutletSellerInformationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		protected OutletSellerInformationEntityBase(System.Int32 outletSellerInformationId):base("OutletSellerInformationEntity")
		{
			InitClassFetch(outletSellerInformationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OutletSellerInformationEntityBase(System.Int32 outletSellerInformationId, IPrefetchPath prefetchPathToUse): base("OutletSellerInformationEntity")
		{
			InitClassFetch(outletSellerInformationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="validator">The custom validator object for this OutletSellerInformationEntity</param>
		protected OutletSellerInformationEntityBase(System.Int32 outletSellerInformationId, IValidator validator):base("OutletSellerInformationEntity")
		{
			InitClassFetch(outletSellerInformationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OutletSellerInformationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_outletCollection = (Obymobi.Data.CollectionClasses.OutletCollection)info.GetValue("_outletCollection", typeof(Obymobi.Data.CollectionClasses.OutletCollection));
			_alwaysFetchOutletCollection = info.GetBoolean("_alwaysFetchOutletCollection");
			_alreadyFetchedOutletCollection = info.GetBoolean("_alreadyFetchedOutletCollection");

			_receiptCollection = (Obymobi.Data.CollectionClasses.ReceiptCollection)info.GetValue("_receiptCollection", typeof(Obymobi.Data.CollectionClasses.ReceiptCollection));
			_alwaysFetchReceiptCollection = info.GetBoolean("_alwaysFetchReceiptCollection");
			_alreadyFetchedReceiptCollection = info.GetBoolean("_alreadyFetchedReceiptCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOutletCollection = (_outletCollection.Count > 0);
			_alreadyFetchedReceiptCollection = (_receiptCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "OutletCollection":
					toReturn.Add(Relations.OutletEntityUsingOutletSellerInformationId);
					break;
				case "ReceiptCollection":
					toReturn.Add(Relations.ReceiptEntityUsingOutletSellerInformationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_outletCollection", (!this.MarkedForDeletion?_outletCollection:null));
			info.AddValue("_alwaysFetchOutletCollection", _alwaysFetchOutletCollection);
			info.AddValue("_alreadyFetchedOutletCollection", _alreadyFetchedOutletCollection);
			info.AddValue("_receiptCollection", (!this.MarkedForDeletion?_receiptCollection:null));
			info.AddValue("_alwaysFetchReceiptCollection", _alwaysFetchReceiptCollection);
			info.AddValue("_alreadyFetchedReceiptCollection", _alreadyFetchedReceiptCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "OutletCollection":
					_alreadyFetchedOutletCollection = true;
					if(entity!=null)
					{
						this.OutletCollection.Add((OutletEntity)entity);
					}
					break;
				case "ReceiptCollection":
					_alreadyFetchedReceiptCollection = true;
					if(entity!=null)
					{
						this.ReceiptCollection.Add((ReceiptEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "OutletCollection":
					_outletCollection.Add((OutletEntity)relatedEntity);
					break;
				case "ReceiptCollection":
					_receiptCollection.Add((ReceiptEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "OutletCollection":
					this.PerformRelatedEntityRemoval(_outletCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReceiptCollection":
					this.PerformRelatedEntityRemoval(_receiptCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_outletCollection);
			toReturn.Add(_receiptCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletSellerInformationId)
		{
			return FetchUsingPK(outletSellerInformationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletSellerInformationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(outletSellerInformationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletSellerInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(outletSellerInformationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 outletSellerInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(outletSellerInformationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OutletSellerInformationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OutletSellerInformationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OutletEntity'</returns>
		public Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch)
		{
			return GetMultiOutletCollection(forceFetch, _outletCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OutletEntity'</returns>
		public Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOutletCollection(forceFetch, _outletCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOutletCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OutletCollection GetMultiOutletCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOutletCollection || forceFetch || _alwaysFetchOutletCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_outletCollection);
				_outletCollection.SuppressClearInGetMulti=!forceFetch;
				_outletCollection.EntityFactoryToUse = entityFactoryToUse;
				_outletCollection.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_outletCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOutletCollection = true;
			}
			return _outletCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OutletCollection'. These settings will be taken into account
		/// when the property OutletCollection is requested or GetMultiOutletCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOutletCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_outletCollection.SortClauses=sortClauses;
			_outletCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReceiptEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch)
		{
			return GetMultiReceiptCollection(forceFetch, _receiptCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReceiptEntity'</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReceiptCollection(forceFetch, _receiptCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReceiptCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ReceiptCollection GetMultiReceiptCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReceiptCollection || forceFetch || _alwaysFetchReceiptCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_receiptCollection);
				_receiptCollection.SuppressClearInGetMulti=!forceFetch;
				_receiptCollection.EntityFactoryToUse = entityFactoryToUse;
				_receiptCollection.GetMultiManyToOne(null, null, this, null, filter);
				_receiptCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReceiptCollection = true;
			}
			return _receiptCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReceiptCollection'. These settings will be taken into account
		/// when the property ReceiptCollection is requested or GetMultiReceiptCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReceiptCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_receiptCollection.SortClauses=sortClauses;
			_receiptCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("OutletCollection", _outletCollection);
			toReturn.Add("ReceiptCollection", _receiptCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="validator">The validator object for this OutletSellerInformationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 outletSellerInformationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(outletSellerInformationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_outletCollection = new Obymobi.Data.CollectionClasses.OutletCollection();
			_outletCollection.SetContainingEntityInfo(this, "OutletSellerInformationEntity");

			_receiptCollection = new Obymobi.Data.CollectionClasses.ReceiptCollection();
			_receiptCollection.SetContainingEntityInfo(this, "OutletSellerInformationEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutletSellerInformationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellerName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Address", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptReceiversEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptReceiversSms", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmsOriginator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiveOrderReceiptNotifications", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiveOrderConfirmationNotifications", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="outletSellerInformationId">PK value for OutletSellerInformation which data should be fetched into this OutletSellerInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 outletSellerInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OutletSellerInformationFieldIndex.OutletSellerInformationId].ForcedCurrentValueWrite(outletSellerInformationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOutletSellerInformationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OutletSellerInformationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OutletSellerInformationRelations Relations
		{
			get	{ return new OutletSellerInformationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Outlet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutletCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OutletCollection(), (IEntityRelation)GetRelationsForField("OutletCollection")[0], (int)Obymobi.Data.EntityType.OutletSellerInformationEntity, (int)Obymobi.Data.EntityType.OutletEntity, 0, null, null, null, "OutletCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Receipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiptCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReceiptCollection(), (IEntityRelation)GetRelationsForField("ReceiptCollection")[0], (int)Obymobi.Data.EntityType.OutletSellerInformationEntity, (int)Obymobi.Data.EntityType.ReceiptEntity, 0, null, null, null, "ReceiptCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OutletSellerInformationId property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."OutletSellerInformationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OutletSellerInformationId
		{
			get { return (System.Int32)GetValue((int)OutletSellerInformationFieldIndex.OutletSellerInformationId, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.OutletSellerInformationId, value, true); }
		}

		/// <summary> The SellerName property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."SellerName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SellerName
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.SellerName, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.SellerName, value, true); }
		}

		/// <summary> The Address property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."Address"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Address
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.Address, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.Address, value, true); }
		}

		/// <summary> The Phonenumber property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."Phonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.Phonenumber, value, true); }
		}

		/// <summary> The Email property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.Email, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.Email, value, true); }
		}

		/// <summary> The VatNumber property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."VatNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VatNumber
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.VatNumber, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.VatNumber, value, true); }
		}

		/// <summary> The ReceiptReceiversEmail property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiptReceiversEmail"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptReceiversEmail
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversEmail, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversEmail, value, true); }
		}

		/// <summary> The ReceiptReceiversSms property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiptReceiversSms"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptReceiversSms
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversSms, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiptReceiversSms, value, true); }
		}

		/// <summary> The SmsOriginator property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."SmsOriginator"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SmsOriginator
		{
			get { return (System.String)GetValue((int)OutletSellerInformationFieldIndex.SmsOriginator, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.SmsOriginator, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)OutletSellerInformationFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletSellerInformationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OutletSellerInformationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)OutletSellerInformationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ReceiveOrderReceiptNotifications property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiveOrderReceiptNotifications"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ReceiveOrderReceiptNotifications
		{
			get { return (System.Boolean)GetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderReceiptNotifications, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderReceiptNotifications, value, true); }
		}

		/// <summary> The ReceiveOrderConfirmationNotifications property of the Entity OutletSellerInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OutletSellerInformation"."ReceiveOrderConfirmationNotifications"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ReceiveOrderConfirmationNotifications
		{
			get { return (System.Boolean)GetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderConfirmationNotifications, true); }
			set	{ SetValue((int)OutletSellerInformationFieldIndex.ReceiveOrderConfirmationNotifications, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OutletEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOutletCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OutletCollection OutletCollection
		{
			get	{ return GetMultiOutletCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OutletCollection. When set to true, OutletCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutletCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOutletCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutletCollection
		{
			get	{ return _alwaysFetchOutletCollection; }
			set	{ _alwaysFetchOutletCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutletCollection already has been fetched. Setting this property to false when OutletCollection has been fetched
		/// will clear the OutletCollection collection well. Setting this property to true while OutletCollection hasn't been fetched disables lazy loading for OutletCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutletCollection
		{
			get { return _alreadyFetchedOutletCollection;}
			set 
			{
				if(_alreadyFetchedOutletCollection && !value && (_outletCollection != null))
				{
					_outletCollection.Clear();
				}
				_alreadyFetchedOutletCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReceiptEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReceiptCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ReceiptCollection ReceiptCollection
		{
			get	{ return GetMultiReceiptCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiptCollection. When set to true, ReceiptCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiptCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReceiptCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiptCollection
		{
			get	{ return _alwaysFetchReceiptCollection; }
			set	{ _alwaysFetchReceiptCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiptCollection already has been fetched. Setting this property to false when ReceiptCollection has been fetched
		/// will clear the ReceiptCollection collection well. Setting this property to true while ReceiptCollection hasn't been fetched disables lazy loading for ReceiptCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiptCollection
		{
			get { return _alreadyFetchedReceiptCollection;}
			set 
			{
				if(_alreadyFetchedReceiptCollection && !value && (_receiptCollection != null))
				{
					_receiptCollection.Clear();
				}
				_alreadyFetchedReceiptCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OutletSellerInformationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
