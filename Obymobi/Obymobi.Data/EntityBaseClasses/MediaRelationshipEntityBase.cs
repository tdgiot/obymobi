﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'MediaRelationship'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MediaRelationshipEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MediaRelationshipEntity"; }
		}
	
		#region Class Member Declarations
		private MediaEntity _mediaEntity;
		private bool	_alwaysFetchMediaEntity, _alreadyFetchedMediaEntity, _mediaEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name MediaEntity</summary>
			public static readonly string MediaEntity = "MediaEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MediaRelationshipEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MediaRelationshipEntityBase() :base("MediaRelationshipEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		protected MediaRelationshipEntityBase(System.Int32 mediaRelationshipId):base("MediaRelationshipEntity")
		{
			InitClassFetch(mediaRelationshipId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MediaRelationshipEntityBase(System.Int32 mediaRelationshipId, IPrefetchPath prefetchPathToUse): base("MediaRelationshipEntity")
		{
			InitClassFetch(mediaRelationshipId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="validator">The custom validator object for this MediaRelationshipEntity</param>
		protected MediaRelationshipEntityBase(System.Int32 mediaRelationshipId, IValidator validator):base("MediaRelationshipEntity")
		{
			InitClassFetch(mediaRelationshipId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaRelationshipEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_mediaEntity = (MediaEntity)info.GetValue("_mediaEntity", typeof(MediaEntity));
			if(_mediaEntity!=null)
			{
				_mediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaEntity = info.GetBoolean("_alwaysFetchMediaEntity");
			_alreadyFetchedMediaEntity = info.GetBoolean("_alreadyFetchedMediaEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MediaRelationshipFieldIndex)fieldIndex)
			{
				case MediaRelationshipFieldIndex.MediaId:
					DesetupSyncMediaEntity(true, false);
					_alreadyFetchedMediaEntity = false;
					break;
				case MediaRelationshipFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMediaEntity = (_mediaEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "MediaEntity":
					toReturn.Add(Relations.MediaEntityUsingMediaId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_mediaEntity", (!this.MarkedForDeletion?_mediaEntity:null));
			info.AddValue("_mediaEntityReturnsNewIfNotFound", _mediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaEntity", _alwaysFetchMediaEntity);
			info.AddValue("_alreadyFetchedMediaEntity", _alreadyFetchedMediaEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "MediaEntity":
					_alreadyFetchedMediaEntity = true;
					this.MediaEntity = (MediaEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "MediaEntity":
					SetupSyncMediaEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "MediaEntity":
					DesetupSyncMediaEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_mediaEntity!=null)
			{
				toReturn.Add(_mediaEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRelationshipId)
		{
			return FetchUsingPK(mediaRelationshipId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRelationshipId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(mediaRelationshipId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRelationshipId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(mediaRelationshipId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRelationshipId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(mediaRelationshipId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MediaRelationshipId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MediaRelationshipRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public MediaEntity GetSingleMediaEntity()
		{
			return GetSingleMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public virtual MediaEntity GetSingleMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaEntity || forceFetch || _alwaysFetchMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaEntityUsingMediaId);
				MediaEntity newEntity = new MediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MediaId);
				}
				if(fetchResult)
				{
					newEntity = (MediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaEntity = newEntity;
				_alreadyFetchedMediaEntity = fetchResult;
			}
			return _mediaEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("MediaEntity", _mediaEntity);
			toReturn.Add("ProductEntity", _productEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="validator">The validator object for this MediaRelationshipEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 mediaRelationshipId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(mediaRelationshipId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_mediaEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaRelationshipId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _mediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMediaRelationshipRelations.MediaEntityUsingMediaIdStatic, true, signalRelatedEntity, "MediaRelationshipCollection", resetFKFields, new int[] { (int)MediaRelationshipFieldIndex.MediaId } );		
			_mediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaEntity(true, true);
				_mediaEntity = (MediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMediaRelationshipRelations.MediaEntityUsingMediaIdStatic, true, ref _alreadyFetchedMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticMediaRelationshipRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "MediaRelationshipCollection", resetFKFields, new int[] { (int)MediaRelationshipFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticMediaRelationshipRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 mediaRelationshipId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MediaRelationshipFieldIndex.MediaRelationshipId].ForcedCurrentValueWrite(mediaRelationshipId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMediaRelationshipDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MediaRelationshipEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MediaRelationshipRelations Relations
		{
			get	{ return new MediaRelationshipRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaEntity")[0], (int)Obymobi.Data.EntityType.MediaRelationshipEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.MediaRelationshipEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MediaRelationshipId property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."MediaRelationshipId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MediaRelationshipId
		{
			get { return (System.Int32)GetValue((int)MediaRelationshipFieldIndex.MediaRelationshipId, true); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.MediaRelationshipId, value, true); }
		}

		/// <summary> The MediaId property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MediaId
		{
			get { return (System.Int32)GetValue((int)MediaRelationshipFieldIndex.MediaId, true); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.MediaId, value, true); }
		}

		/// <summary> The ProductId property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaRelationshipFieldIndex.ProductId, false); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.ProductId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MediaRelationshipFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MediaRelationshipFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaRelationshipFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaRelationshipFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity MediaRelationship<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRelationship"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaRelationshipFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)MediaRelationshipFieldIndex.ParentCompanyId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'MediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaEntity MediaEntity
		{
			get	{ return GetSingleMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaRelationshipCollection", "MediaEntity", _mediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaEntity. When set to true, MediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaEntity
		{
			get	{ return _alwaysFetchMediaEntity; }
			set	{ _alwaysFetchMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaEntity already has been fetched. Setting this property to false when MediaEntity has been fetched
		/// will set MediaEntity to null as well. Setting this property to true while MediaEntity hasn't been fetched disables lazy loading for MediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaEntity
		{
			get { return _alreadyFetchedMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaEntity && !value)
				{
					this.MediaEntity = null;
				}
				_alreadyFetchedMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaEntity is not found
		/// in the database. When set to true, MediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaEntityReturnsNewIfNotFound; }
			set { _mediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaRelationshipCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MediaRelationshipEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
