﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Genericcategory'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class GenericcategoryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "GenericcategoryEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CategoryCollection	_categoryCollection;
		private bool	_alwaysFetchCategoryCollection, _alreadyFetchedCategoryCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection	_genericcategoryCollection;
		private bool	_alwaysFetchGenericcategoryCollection, _alreadyFetchedGenericcategoryCollection;
		private Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection	_genericcategoryLanguageCollection;
		private bool	_alwaysFetchGenericcategoryLanguageCollection, _alreadyFetchedGenericcategoryLanguageCollection;
		private Obymobi.Data.CollectionClasses.GenericproductCollection	_genericproductCollection;
		private bool	_alwaysFetchGenericproductCollection, _alreadyFetchedGenericproductCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaCategory;
		private bool	_alwaysFetchMenuCollectionViaCategory, _alreadyFetchedMenuCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCategory;
		private bool	_alwaysFetchRouteCollectionViaCategory, _alreadyFetchedRouteCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;
		private BrandEntity _brandEntity;
		private bool	_alwaysFetchBrandEntity, _alreadyFetchedBrandEntity, _brandEntityReturnsNewIfNotFound;
		private GenericcategoryEntity _genericcategoryEntity;
		private bool	_alwaysFetchGenericcategoryEntity, _alreadyFetchedGenericcategoryEntity, _genericcategoryEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BrandEntity</summary>
			public static readonly string BrandEntity = "BrandEntity";
			/// <summary>Member name GenericcategoryEntity</summary>
			public static readonly string GenericcategoryEntity = "GenericcategoryEntity";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name GenericcategoryCollection</summary>
			public static readonly string GenericcategoryCollection = "GenericcategoryCollection";
			/// <summary>Member name GenericcategoryLanguageCollection</summary>
			public static readonly string GenericcategoryLanguageCollection = "GenericcategoryLanguageCollection";
			/// <summary>Member name GenericproductCollection</summary>
			public static readonly string GenericproductCollection = "GenericproductCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name MenuCollectionViaCategory</summary>
			public static readonly string MenuCollectionViaCategory = "MenuCollectionViaCategory";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name RouteCollectionViaCategory</summary>
			public static readonly string RouteCollectionViaCategory = "RouteCollectionViaCategory";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GenericcategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected GenericcategoryEntityBase() :base("GenericcategoryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		protected GenericcategoryEntityBase(System.Int32 genericcategoryId):base("GenericcategoryEntity")
		{
			InitClassFetch(genericcategoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected GenericcategoryEntityBase(System.Int32 genericcategoryId, IPrefetchPath prefetchPathToUse): base("GenericcategoryEntity")
		{
			InitClassFetch(genericcategoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="validator">The custom validator object for this GenericcategoryEntity</param>
		protected GenericcategoryEntityBase(System.Int32 genericcategoryId, IValidator validator):base("GenericcategoryEntity")
		{
			InitClassFetch(genericcategoryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericcategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_categoryCollection = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollection", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollection = info.GetBoolean("_alwaysFetchCategoryCollection");
			_alreadyFetchedCategoryCollection = info.GetBoolean("_alreadyFetchedCategoryCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_genericcategoryCollection = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollection", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollection = info.GetBoolean("_alwaysFetchGenericcategoryCollection");
			_alreadyFetchedGenericcategoryCollection = info.GetBoolean("_alreadyFetchedGenericcategoryCollection");

			_genericcategoryLanguageCollection = (Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection)info.GetValue("_genericcategoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection));
			_alwaysFetchGenericcategoryLanguageCollection = info.GetBoolean("_alwaysFetchGenericcategoryLanguageCollection");
			_alreadyFetchedGenericcategoryLanguageCollection = info.GetBoolean("_alreadyFetchedGenericcategoryLanguageCollection");

			_genericproductCollection = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollection = info.GetBoolean("_alwaysFetchGenericproductCollection");
			_alreadyFetchedGenericproductCollection = info.GetBoolean("_alreadyFetchedGenericproductCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");
			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_menuCollectionViaCategory = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaCategory = info.GetBoolean("_alwaysFetchMenuCollectionViaCategory");
			_alreadyFetchedMenuCollectionViaCategory = info.GetBoolean("_alreadyFetchedMenuCollectionViaCategory");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_routeCollectionViaCategory = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCategory = info.GetBoolean("_alwaysFetchRouteCollectionViaCategory");
			_alreadyFetchedRouteCollectionViaCategory = info.GetBoolean("_alreadyFetchedRouteCollectionViaCategory");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");
			_brandEntity = (BrandEntity)info.GetValue("_brandEntity", typeof(BrandEntity));
			if(_brandEntity!=null)
			{
				_brandEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_brandEntityReturnsNewIfNotFound = info.GetBoolean("_brandEntityReturnsNewIfNotFound");
			_alwaysFetchBrandEntity = info.GetBoolean("_alwaysFetchBrandEntity");
			_alreadyFetchedBrandEntity = info.GetBoolean("_alreadyFetchedBrandEntity");

			_genericcategoryEntity = (GenericcategoryEntity)info.GetValue("_genericcategoryEntity", typeof(GenericcategoryEntity));
			if(_genericcategoryEntity!=null)
			{
				_genericcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_genericcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchGenericcategoryEntity = info.GetBoolean("_alwaysFetchGenericcategoryEntity");
			_alreadyFetchedGenericcategoryEntity = info.GetBoolean("_alreadyFetchedGenericcategoryEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((GenericcategoryFieldIndex)fieldIndex)
			{
				case GenericcategoryFieldIndex.ParentGenericcategoryId:
					DesetupSyncGenericcategoryEntity(true, false);
					_alreadyFetchedGenericcategoryEntity = false;
					break;
				case GenericcategoryFieldIndex.BrandId:
					DesetupSyncBrandEntity(true, false);
					_alreadyFetchedBrandEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCategoryCollection = (_categoryCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedGenericcategoryCollection = (_genericcategoryCollection.Count > 0);
			_alreadyFetchedGenericcategoryLanguageCollection = (_genericcategoryLanguageCollection.Count > 0);
			_alreadyFetchedGenericproductCollection = (_genericproductCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedMenuCollectionViaCategory = (_menuCollectionViaCategory.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedRouteCollectionViaCategory = (_routeCollectionViaCategory.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
			_alreadyFetchedBrandEntity = (_brandEntity != null);
			_alreadyFetchedGenericcategoryEntity = (_genericcategoryEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BrandEntity":
					toReturn.Add(Relations.BrandEntityUsingBrandId);
					break;
				case "GenericcategoryEntity":
					toReturn.Add(Relations.GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryId);
					break;
				case "CategoryCollection":
					toReturn.Add(Relations.CategoryEntityUsingGenericcategoryId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingGenericcategoryId);
					break;
				case "GenericcategoryCollection":
					toReturn.Add(Relations.GenericcategoryEntityUsingParentGenericcategoryId);
					break;
				case "GenericcategoryLanguageCollection":
					toReturn.Add(Relations.GenericcategoryLanguageEntityUsingGenericcategoryId);
					break;
				case "GenericproductCollection":
					toReturn.Add(Relations.GenericproductEntityUsingGenericcategoryId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.MenuEntityUsingMenuId, "Category_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.RouteEntityUsingRouteId, "Category_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingGenericcategoryId, "GenericcategoryEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_categoryCollection", (!this.MarkedForDeletion?_categoryCollection:null));
			info.AddValue("_alwaysFetchCategoryCollection", _alwaysFetchCategoryCollection);
			info.AddValue("_alreadyFetchedCategoryCollection", _alreadyFetchedCategoryCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_genericcategoryCollection", (!this.MarkedForDeletion?_genericcategoryCollection:null));
			info.AddValue("_alwaysFetchGenericcategoryCollection", _alwaysFetchGenericcategoryCollection);
			info.AddValue("_alreadyFetchedGenericcategoryCollection", _alreadyFetchedGenericcategoryCollection);
			info.AddValue("_genericcategoryLanguageCollection", (!this.MarkedForDeletion?_genericcategoryLanguageCollection:null));
			info.AddValue("_alwaysFetchGenericcategoryLanguageCollection", _alwaysFetchGenericcategoryLanguageCollection);
			info.AddValue("_alreadyFetchedGenericcategoryLanguageCollection", _alreadyFetchedGenericcategoryLanguageCollection);
			info.AddValue("_genericproductCollection", (!this.MarkedForDeletion?_genericproductCollection:null));
			info.AddValue("_alwaysFetchGenericproductCollection", _alwaysFetchGenericproductCollection);
			info.AddValue("_alreadyFetchedGenericproductCollection", _alreadyFetchedGenericproductCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_menuCollectionViaCategory", (!this.MarkedForDeletion?_menuCollectionViaCategory:null));
			info.AddValue("_alwaysFetchMenuCollectionViaCategory", _alwaysFetchMenuCollectionViaCategory);
			info.AddValue("_alreadyFetchedMenuCollectionViaCategory", _alreadyFetchedMenuCollectionViaCategory);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_routeCollectionViaCategory", (!this.MarkedForDeletion?_routeCollectionViaCategory:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCategory", _alwaysFetchRouteCollectionViaCategory);
			info.AddValue("_alreadyFetchedRouteCollectionViaCategory", _alreadyFetchedRouteCollectionViaCategory);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);
			info.AddValue("_brandEntity", (!this.MarkedForDeletion?_brandEntity:null));
			info.AddValue("_brandEntityReturnsNewIfNotFound", _brandEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrandEntity", _alwaysFetchBrandEntity);
			info.AddValue("_alreadyFetchedBrandEntity", _alreadyFetchedBrandEntity);
			info.AddValue("_genericcategoryEntity", (!this.MarkedForDeletion?_genericcategoryEntity:null));
			info.AddValue("_genericcategoryEntityReturnsNewIfNotFound", _genericcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericcategoryEntity", _alwaysFetchGenericcategoryEntity);
			info.AddValue("_alreadyFetchedGenericcategoryEntity", _alreadyFetchedGenericcategoryEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BrandEntity":
					_alreadyFetchedBrandEntity = true;
					this.BrandEntity = (BrandEntity)entity;
					break;
				case "GenericcategoryEntity":
					_alreadyFetchedGenericcategoryEntity = true;
					this.GenericcategoryEntity = (GenericcategoryEntity)entity;
					break;
				case "CategoryCollection":
					_alreadyFetchedCategoryCollection = true;
					if(entity!=null)
					{
						this.CategoryCollection.Add((CategoryEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "GenericcategoryCollection":
					_alreadyFetchedGenericcategoryCollection = true;
					if(entity!=null)
					{
						this.GenericcategoryCollection.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericcategoryLanguageCollection":
					_alreadyFetchedGenericcategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.GenericcategoryLanguageCollection.Add((GenericcategoryLanguageEntity)entity);
					}
					break;
				case "GenericproductCollection":
					_alreadyFetchedGenericproductCollection = true;
					if(entity!=null)
					{
						this.GenericproductCollection.Add((GenericproductEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "MenuCollectionViaCategory":
					_alreadyFetchedMenuCollectionViaCategory = true;
					if(entity!=null)
					{
						this.MenuCollectionViaCategory.Add((MenuEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "RouteCollectionViaCategory":
					_alreadyFetchedRouteCollectionViaCategory = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCategory.Add((RouteEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					SetupSyncBrandEntity(relatedEntity);
					break;
				case "GenericcategoryEntity":
					SetupSyncGenericcategoryEntity(relatedEntity);
					break;
				case "CategoryCollection":
					_categoryCollection.Add((CategoryEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "GenericcategoryCollection":
					_genericcategoryCollection.Add((GenericcategoryEntity)relatedEntity);
					break;
				case "GenericcategoryLanguageCollection":
					_genericcategoryLanguageCollection.Add((GenericcategoryLanguageEntity)relatedEntity);
					break;
				case "GenericproductCollection":
					_genericproductCollection.Add((GenericproductEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BrandEntity":
					DesetupSyncBrandEntity(false, true);
					break;
				case "GenericcategoryEntity":
					DesetupSyncGenericcategoryEntity(false, true);
					break;
				case "CategoryCollection":
					this.PerformRelatedEntityRemoval(_categoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericcategoryCollection":
					this.PerformRelatedEntityRemoval(_genericcategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericcategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_genericcategoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericproductCollection":
					this.PerformRelatedEntityRemoval(_genericproductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_brandEntity!=null)
			{
				toReturn.Add(_brandEntity);
			}
			if(_genericcategoryEntity!=null)
			{
				toReturn.Add(_genericcategoryEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_categoryCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_genericcategoryCollection);
			toReturn.Add(_genericcategoryLanguageCollection);
			toReturn.Add(_genericproductCollection);
			toReturn.Add(_mediaCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericcategoryId)
		{
			return FetchUsingPK(genericcategoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericcategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(genericcategoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericcategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(genericcategoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 genericcategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(genericcategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.GenericcategoryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GenericcategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryCollection || forceFetch || _alwaysFetchCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollection);
				_categoryCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, filter);
				_categoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollection = true;
			}
			return _categoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollection'. These settings will be taken into account
		/// when the property CategoryCollection is requested or GetMultiCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollection.SortClauses=sortClauses;
			_categoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch)
		{
			return GetMultiGenericcategoryCollection(forceFetch, _genericcategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericcategoryCollection(forceFetch, _genericcategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericcategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollection || forceFetch || _alwaysFetchGenericcategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollection);
				_genericcategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollection.GetMultiManyToOne(null, this, filter);
				_genericcategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollection = true;
			}
			return _genericcategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollection'. These settings will be taken into account
		/// when the property GenericcategoryCollection is requested or GetMultiGenericcategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollection.SortClauses=sortClauses;
			_genericcategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiGenericcategoryLanguageCollection(forceFetch, _genericcategoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericcategoryLanguageCollection(forceFetch, _genericcategoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericcategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericcategoryLanguageCollection || forceFetch || _alwaysFetchGenericcategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryLanguageCollection);
				_genericcategoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryLanguageCollection.GetMultiManyToOne(this, null, filter);
				_genericcategoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryLanguageCollection = true;
			}
			return _genericcategoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryLanguageCollection'. These settings will be taken into account
		/// when the property GenericcategoryLanguageCollection is requested or GetMultiGenericcategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryLanguageCollection.SortClauses=sortClauses;
			_genericcategoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductCollection || forceFetch || _alwaysFetchGenericproductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollection);
				_genericproductCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollection.GetMultiManyToOne(null, this, null, null, filter);
				_genericproductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollection = true;
			}
			return _genericproductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollection'. These settings will be taken into account
		/// when the property GenericproductCollection is requested or GetMultiGenericproductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollection.SortClauses=sortClauses;
			_genericproductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch)
		{
			return GetMultiMenuCollectionViaCategory(forceFetch, _menuCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaCategory || forceFetch || _alwaysFetchMenuCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaCategory.GetMulti(filter, GetRelationsForField("MenuCollectionViaCategory"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaCategory = true;
			}
			return _menuCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaCategory'. These settings will be taken into account
		/// when the property MenuCollectionViaCategory is requested or GetMultiMenuCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaCategory.SortClauses=sortClauses;
			_menuCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCategory(forceFetch, _routeCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCategory || forceFetch || _alwaysFetchRouteCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCategory.GetMulti(filter, GetRelationsForField("RouteCollectionViaCategory"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCategory = true;
			}
			return _routeCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCategory'. These settings will be taken into account
		/// when the property RouteCollectionViaCategory is requested or GetMultiRouteCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCategory.SortClauses=sortClauses;
			_routeCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(GenericcategoryFields.GenericcategoryId, ComparisonOperator.Equal, this.GenericcategoryId, "GenericcategoryEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public BrandEntity GetSingleBrandEntity()
		{
			return GetSingleBrandEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public virtual BrandEntity GetSingleBrandEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrandEntity || forceFetch || _alwaysFetchBrandEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BrandEntityUsingBrandId);
				BrandEntity newEntity = new BrandEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrandId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BrandEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_brandEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BrandEntity = newEntity;
				_alreadyFetchedBrandEntity = fetchResult;
			}
			return _brandEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public GenericcategoryEntity GetSingleGenericcategoryEntity()
		{
			return GetSingleGenericcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericcategoryEntity' which is related to this entity.</returns>
		public virtual GenericcategoryEntity GetSingleGenericcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericcategoryEntity || forceFetch || _alwaysFetchGenericcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryId);
				GenericcategoryEntity newEntity = new GenericcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentGenericcategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericcategoryEntity = newEntity;
				_alreadyFetchedGenericcategoryEntity = fetchResult;
			}
			return _genericcategoryEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BrandEntity", _brandEntity);
			toReturn.Add("GenericcategoryEntity", _genericcategoryEntity);
			toReturn.Add("CategoryCollection", _categoryCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("GenericcategoryCollection", _genericcategoryCollection);
			toReturn.Add("GenericcategoryLanguageCollection", _genericcategoryLanguageCollection);
			toReturn.Add("GenericproductCollection", _genericproductCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("MenuCollectionViaCategory", _menuCollectionViaCategory);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("RouteCollectionViaCategory", _routeCollectionViaCategory);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="validator">The validator object for this GenericcategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 genericcategoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(genericcategoryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_categoryCollection = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollection.SetContainingEntityInfo(this, "GenericcategoryEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "GenericcategoryEntity");

			_genericcategoryCollection = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericcategoryCollection.SetContainingEntityInfo(this, "GenericcategoryEntity");

			_genericcategoryLanguageCollection = new Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection();
			_genericcategoryLanguageCollection.SetContainingEntityInfo(this, "GenericcategoryEntity");

			_genericproductCollection = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_genericproductCollection.SetContainingEntityInfo(this, "GenericcategoryEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "GenericcategoryEntity");
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_menuCollectionViaCategory = new Obymobi.Data.CollectionClasses.MenuCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_routeCollectionViaCategory = new Obymobi.Data.CollectionClasses.RouteCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_brandEntityReturnsNewIfNotFound = true;
			_genericcategoryEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentGenericcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericCategoryType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisibilityType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _brandEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrandEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticGenericcategoryRelations.BrandEntityUsingBrandIdStatic, true, signalRelatedEntity, "GenericcategoryCollection", resetFKFields, new int[] { (int)GenericcategoryFieldIndex.BrandId } );		
			_brandEntity = null;
		}
		
		/// <summary> setups the sync logic for member _brandEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrandEntity(IEntityCore relatedEntity)
		{
			if(_brandEntity!=relatedEntity)
			{		
				DesetupSyncBrandEntity(true, true);
				_brandEntity = (BrandEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticGenericcategoryRelations.BrandEntityUsingBrandIdStatic, true, ref _alreadyFetchedBrandEntity, new string[] { "BrandName" } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrandEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				case "Name":
					this.OnPropertyChanged("BrandName");
					break;
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticGenericcategoryRelations.GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryIdStatic, true, signalRelatedEntity, "GenericcategoryCollection", resetFKFields, new int[] { (int)GenericcategoryFieldIndex.ParentGenericcategoryId } );		
			_genericcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericcategoryEntity(IEntityCore relatedEntity)
		{
			if(_genericcategoryEntity!=relatedEntity)
			{		
				DesetupSyncGenericcategoryEntity(true, true);
				_genericcategoryEntity = (GenericcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericcategoryEntity, new PropertyChangedEventHandler( OnGenericcategoryEntityPropertyChanged ), "GenericcategoryEntity", Obymobi.Data.RelationClasses.StaticGenericcategoryRelations.GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryIdStatic, true, ref _alreadyFetchedGenericcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 genericcategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)GenericcategoryFieldIndex.GenericcategoryId].ForcedCurrentValueWrite(genericcategoryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateGenericcategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new GenericcategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GenericcategoryRelations Relations
		{
			get	{ return new GenericcategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryCollection")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryCollection")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, null, "GenericcategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GenericcategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.GenericcategoryLanguageEntity, 0, null, null, null, "GenericcategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), (IEntityRelation)GetRelationsForField("GenericproductCollection")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, null, "GenericproductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaCategory"), "MenuCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCategory"), "RouteCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingGenericcategoryId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Brand'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrandEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BrandCollection(), (IEntityRelation)GetRelationsForField("BrandEntity")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.BrandEntity, 0, null, null, null, "BrandEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryEntity")[0], (int)Obymobi.Data.EntityType.GenericcategoryEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, null, "GenericcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The GenericcategoryId property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."GenericcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 GenericcategoryId
		{
			get { return (System.Int32)GetValue((int)GenericcategoryFieldIndex.GenericcategoryId, true); }
			set	{ SetValue((int)GenericcategoryFieldIndex.GenericcategoryId, value, true); }
		}

		/// <summary> The ParentGenericcategoryId property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."ParentGenericcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentGenericcategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericcategoryFieldIndex.ParentGenericcategoryId, false); }
			set	{ SetValue((int)GenericcategoryFieldIndex.ParentGenericcategoryId, value, true); }
		}

		/// <summary> The Name property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)GenericcategoryFieldIndex.Name, true); }
			set	{ SetValue((int)GenericcategoryFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)GenericcategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)GenericcategoryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)GenericcategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)GenericcategoryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ExternalId property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericcategoryFieldIndex.ExternalId, false); }
			set	{ SetValue((int)GenericcategoryFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The GenericCategoryType property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."GenericcategoryType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericCategoryType
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericcategoryFieldIndex.GenericCategoryType, false); }
			set	{ SetValue((int)GenericcategoryFieldIndex.GenericCategoryType, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericcategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)GenericcategoryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)GenericcategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)GenericcategoryFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The BrandId property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."BrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BrandId
		{
			get { return (Nullable<System.Int32>)GetValue((int)GenericcategoryFieldIndex.BrandId, false); }
			set	{ SetValue((int)GenericcategoryFieldIndex.BrandId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)GenericcategoryFieldIndex.SortOrder, true); }
			set	{ SetValue((int)GenericcategoryFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The VisibilityType property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."VisibilityType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.VisibilityType VisibilityType
		{
			get { return (Obymobi.Enums.VisibilityType)GetValue((int)GenericcategoryFieldIndex.VisibilityType, true); }
			set	{ SetValue((int)GenericcategoryFieldIndex.VisibilityType, value, true); }
		}

		/// <summary> The Visible property of the Entity Genericcategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Genericcategory"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)GenericcategoryFieldIndex.Visible, true); }
			set	{ SetValue((int)GenericcategoryFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollection
		{
			get	{ return GetMultiCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollection. When set to true, CategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollection
		{
			get	{ return _alwaysFetchCategoryCollection; }
			set	{ _alwaysFetchCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollection already has been fetched. Setting this property to false when CategoryCollection has been fetched
		/// will clear the CategoryCollection collection well. Setting this property to true while CategoryCollection hasn't been fetched disables lazy loading for CategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollection
		{
			get { return _alreadyFetchedCategoryCollection;}
			set 
			{
				if(_alreadyFetchedCategoryCollection && !value && (_categoryCollection != null))
				{
					_categoryCollection.Clear();
				}
				_alreadyFetchedCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollection
		{
			get	{ return GetMultiGenericcategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollection. When set to true, GenericcategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericcategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollection
		{
			get	{ return _alwaysFetchGenericcategoryCollection; }
			set	{ _alwaysFetchGenericcategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollection already has been fetched. Setting this property to false when GenericcategoryCollection has been fetched
		/// will clear the GenericcategoryCollection collection well. Setting this property to true while GenericcategoryCollection hasn't been fetched disables lazy loading for GenericcategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollection
		{
			get { return _alreadyFetchedGenericcategoryCollection;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollection && !value && (_genericcategoryCollection != null))
				{
					_genericcategoryCollection.Clear();
				}
				_alreadyFetchedGenericcategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GenericcategoryLanguageCollection
		{
			get	{ return GetMultiGenericcategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryLanguageCollection. When set to true, GenericcategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericcategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryLanguageCollection
		{
			get	{ return _alwaysFetchGenericcategoryLanguageCollection; }
			set	{ _alwaysFetchGenericcategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryLanguageCollection already has been fetched. Setting this property to false when GenericcategoryLanguageCollection has been fetched
		/// will clear the GenericcategoryLanguageCollection collection well. Setting this property to true while GenericcategoryLanguageCollection hasn't been fetched disables lazy loading for GenericcategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryLanguageCollection
		{
			get { return _alreadyFetchedGenericcategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedGenericcategoryLanguageCollection && !value && (_genericcategoryLanguageCollection != null))
				{
					_genericcategoryLanguageCollection.Clear();
				}
				_alreadyFetchedGenericcategoryLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollection
		{
			get	{ return GetMultiGenericproductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollection. When set to true, GenericproductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollection
		{
			get	{ return _alwaysFetchGenericproductCollection; }
			set	{ _alwaysFetchGenericproductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollection already has been fetched. Setting this property to false when GenericproductCollection has been fetched
		/// will clear the GenericproductCollection collection well. Setting this property to true while GenericproductCollection hasn't been fetched disables lazy loading for GenericproductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollection
		{
			get { return _alreadyFetchedGenericproductCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductCollection && !value && (_genericproductCollection != null))
				{
					_genericproductCollection.Clear();
				}
				_alreadyFetchedGenericproductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaCategory
		{
			get { return GetMultiMenuCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaCategory. When set to true, MenuCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaCategory
		{
			get	{ return _alwaysFetchMenuCollectionViaCategory; }
			set	{ _alwaysFetchMenuCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaCategory already has been fetched. Setting this property to false when MenuCollectionViaCategory has been fetched
		/// will clear the MenuCollectionViaCategory collection well. Setting this property to true while MenuCollectionViaCategory hasn't been fetched disables lazy loading for MenuCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaCategory
		{
			get { return _alreadyFetchedMenuCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaCategory && !value && (_menuCollectionViaCategory != null))
				{
					_menuCollectionViaCategory.Clear();
				}
				_alreadyFetchedMenuCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCategory
		{
			get { return GetMultiRouteCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCategory. When set to true, RouteCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCategory
		{
			get	{ return _alwaysFetchRouteCollectionViaCategory; }
			set	{ _alwaysFetchRouteCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCategory already has been fetched. Setting this property to false when RouteCollectionViaCategory has been fetched
		/// will clear the RouteCollectionViaCategory collection well. Setting this property to true while RouteCollectionViaCategory hasn't been fetched disables lazy loading for RouteCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCategory
		{
			get { return _alreadyFetchedRouteCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCategory && !value && (_routeCollectionViaCategory != null))
				{
					_routeCollectionViaCategory.Clear();
				}
				_alreadyFetchedRouteCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'BrandEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrandEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual BrandEntity BrandEntity
		{
			get	{ return GetSingleBrandEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrandEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericcategoryCollection", "BrandEntity", _brandEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BrandEntity. When set to true, BrandEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BrandEntity is accessed. You can always execute a forced fetch by calling GetSingleBrandEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrandEntity
		{
			get	{ return _alwaysFetchBrandEntity; }
			set	{ _alwaysFetchBrandEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BrandEntity already has been fetched. Setting this property to false when BrandEntity has been fetched
		/// will set BrandEntity to null as well. Setting this property to true while BrandEntity hasn't been fetched disables lazy loading for BrandEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrandEntity
		{
			get { return _alreadyFetchedBrandEntity;}
			set 
			{
				if(_alreadyFetchedBrandEntity && !value)
				{
					this.BrandEntity = null;
				}
				_alreadyFetchedBrandEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BrandEntity is not found
		/// in the database. When set to true, BrandEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool BrandEntityReturnsNewIfNotFound
		{
			get	{ return _brandEntityReturnsNewIfNotFound; }
			set { _brandEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericcategoryEntity GenericcategoryEntity
		{
			get	{ return GetSingleGenericcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "GenericcategoryCollection", "GenericcategoryEntity", _genericcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryEntity. When set to true, GenericcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryEntity
		{
			get	{ return _alwaysFetchGenericcategoryEntity; }
			set	{ _alwaysFetchGenericcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryEntity already has been fetched. Setting this property to false when GenericcategoryEntity has been fetched
		/// will set GenericcategoryEntity to null as well. Setting this property to true while GenericcategoryEntity hasn't been fetched disables lazy loading for GenericcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryEntity
		{
			get { return _alreadyFetchedGenericcategoryEntity;}
			set 
			{
				if(_alreadyFetchedGenericcategoryEntity && !value)
				{
					this.GenericcategoryEntity = null;
				}
				_alreadyFetchedGenericcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericcategoryEntity is not found
		/// in the database. When set to true, GenericcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _genericcategoryEntityReturnsNewIfNotFound; }
			set { _genericcategoryEntityReturnsNewIfNotFound = value; }	
		}

 
		/// <summary> Gets the value of the related field this.BrandEntity.Name.<br/><br/></summary>
		[Dionysos.Data.DataGridViewColumnVisibleAttribute]
		public virtual System.String BrandName
		{
			get
			{
				BrandEntity relatedEntity = this.BrandEntity;
				return relatedEntity==null ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : relatedEntity.Name;
			}

		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.GenericcategoryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
