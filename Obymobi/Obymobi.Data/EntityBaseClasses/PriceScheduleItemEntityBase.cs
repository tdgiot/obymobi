﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PriceScheduleItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PriceScheduleItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PriceScheduleItemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection	_priceScheduleItemOccurrenceCollection;
		private bool	_alwaysFetchPriceScheduleItemOccurrenceCollection, _alreadyFetchedPriceScheduleItemOccurrenceCollection;
		private PriceLevelEntity _priceLevelEntity;
		private bool	_alwaysFetchPriceLevelEntity, _alreadyFetchedPriceLevelEntity, _priceLevelEntityReturnsNewIfNotFound;
		private PriceScheduleEntity _priceScheduleEntity;
		private bool	_alwaysFetchPriceScheduleEntity, _alreadyFetchedPriceScheduleEntity, _priceScheduleEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PriceLevelEntity</summary>
			public static readonly string PriceLevelEntity = "PriceLevelEntity";
			/// <summary>Member name PriceScheduleEntity</summary>
			public static readonly string PriceScheduleEntity = "PriceScheduleEntity";
			/// <summary>Member name PriceScheduleItemOccurrenceCollection</summary>
			public static readonly string PriceScheduleItemOccurrenceCollection = "PriceScheduleItemOccurrenceCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PriceScheduleItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PriceScheduleItemEntityBase() :base("PriceScheduleItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		protected PriceScheduleItemEntityBase(System.Int32 priceScheduleItemId):base("PriceScheduleItemEntity")
		{
			InitClassFetch(priceScheduleItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PriceScheduleItemEntityBase(System.Int32 priceScheduleItemId, IPrefetchPath prefetchPathToUse): base("PriceScheduleItemEntity")
		{
			InitClassFetch(priceScheduleItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="validator">The custom validator object for this PriceScheduleItemEntity</param>
		protected PriceScheduleItemEntityBase(System.Int32 priceScheduleItemId, IValidator validator):base("PriceScheduleItemEntity")
		{
			InitClassFetch(priceScheduleItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceScheduleItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_priceScheduleItemOccurrenceCollection = (Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection)info.GetValue("_priceScheduleItemOccurrenceCollection", typeof(Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection));
			_alwaysFetchPriceScheduleItemOccurrenceCollection = info.GetBoolean("_alwaysFetchPriceScheduleItemOccurrenceCollection");
			_alreadyFetchedPriceScheduleItemOccurrenceCollection = info.GetBoolean("_alreadyFetchedPriceScheduleItemOccurrenceCollection");
			_priceLevelEntity = (PriceLevelEntity)info.GetValue("_priceLevelEntity", typeof(PriceLevelEntity));
			if(_priceLevelEntity!=null)
			{
				_priceLevelEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceLevelEntityReturnsNewIfNotFound = info.GetBoolean("_priceLevelEntityReturnsNewIfNotFound");
			_alwaysFetchPriceLevelEntity = info.GetBoolean("_alwaysFetchPriceLevelEntity");
			_alreadyFetchedPriceLevelEntity = info.GetBoolean("_alreadyFetchedPriceLevelEntity");

			_priceScheduleEntity = (PriceScheduleEntity)info.GetValue("_priceScheduleEntity", typeof(PriceScheduleEntity));
			if(_priceScheduleEntity!=null)
			{
				_priceScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_priceScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchPriceScheduleEntity = info.GetBoolean("_alwaysFetchPriceScheduleEntity");
			_alreadyFetchedPriceScheduleEntity = info.GetBoolean("_alreadyFetchedPriceScheduleEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PriceScheduleItemFieldIndex)fieldIndex)
			{
				case PriceScheduleItemFieldIndex.PriceScheduleId:
					DesetupSyncPriceScheduleEntity(true, false);
					_alreadyFetchedPriceScheduleEntity = false;
					break;
				case PriceScheduleItemFieldIndex.PriceLevelId:
					DesetupSyncPriceLevelEntity(true, false);
					_alreadyFetchedPriceLevelEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPriceScheduleItemOccurrenceCollection = (_priceScheduleItemOccurrenceCollection.Count > 0);
			_alreadyFetchedPriceLevelEntity = (_priceLevelEntity != null);
			_alreadyFetchedPriceScheduleEntity = (_priceScheduleEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PriceLevelEntity":
					toReturn.Add(Relations.PriceLevelEntityUsingPriceLevelId);
					break;
				case "PriceScheduleEntity":
					toReturn.Add(Relations.PriceScheduleEntityUsingPriceScheduleId);
					break;
				case "PriceScheduleItemOccurrenceCollection":
					toReturn.Add(Relations.PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_priceScheduleItemOccurrenceCollection", (!this.MarkedForDeletion?_priceScheduleItemOccurrenceCollection:null));
			info.AddValue("_alwaysFetchPriceScheduleItemOccurrenceCollection", _alwaysFetchPriceScheduleItemOccurrenceCollection);
			info.AddValue("_alreadyFetchedPriceScheduleItemOccurrenceCollection", _alreadyFetchedPriceScheduleItemOccurrenceCollection);
			info.AddValue("_priceLevelEntity", (!this.MarkedForDeletion?_priceLevelEntity:null));
			info.AddValue("_priceLevelEntityReturnsNewIfNotFound", _priceLevelEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceLevelEntity", _alwaysFetchPriceLevelEntity);
			info.AddValue("_alreadyFetchedPriceLevelEntity", _alreadyFetchedPriceLevelEntity);
			info.AddValue("_priceScheduleEntity", (!this.MarkedForDeletion?_priceScheduleEntity:null));
			info.AddValue("_priceScheduleEntityReturnsNewIfNotFound", _priceScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceScheduleEntity", _alwaysFetchPriceScheduleEntity);
			info.AddValue("_alreadyFetchedPriceScheduleEntity", _alreadyFetchedPriceScheduleEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PriceLevelEntity":
					_alreadyFetchedPriceLevelEntity = true;
					this.PriceLevelEntity = (PriceLevelEntity)entity;
					break;
				case "PriceScheduleEntity":
					_alreadyFetchedPriceScheduleEntity = true;
					this.PriceScheduleEntity = (PriceScheduleEntity)entity;
					break;
				case "PriceScheduleItemOccurrenceCollection":
					_alreadyFetchedPriceScheduleItemOccurrenceCollection = true;
					if(entity!=null)
					{
						this.PriceScheduleItemOccurrenceCollection.Add((PriceScheduleItemOccurrenceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PriceLevelEntity":
					SetupSyncPriceLevelEntity(relatedEntity);
					break;
				case "PriceScheduleEntity":
					SetupSyncPriceScheduleEntity(relatedEntity);
					break;
				case "PriceScheduleItemOccurrenceCollection":
					_priceScheduleItemOccurrenceCollection.Add((PriceScheduleItemOccurrenceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PriceLevelEntity":
					DesetupSyncPriceLevelEntity(false, true);
					break;
				case "PriceScheduleEntity":
					DesetupSyncPriceScheduleEntity(false, true);
					break;
				case "PriceScheduleItemOccurrenceCollection":
					this.PerformRelatedEntityRemoval(_priceScheduleItemOccurrenceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_priceLevelEntity!=null)
			{
				toReturn.Add(_priceLevelEntity);
			}
			if(_priceScheduleEntity!=null)
			{
				toReturn.Add(_priceScheduleEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_priceScheduleItemOccurrenceCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemId)
		{
			return FetchUsingPK(priceScheduleItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(priceScheduleItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(priceScheduleItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 priceScheduleItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(priceScheduleItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PriceScheduleItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PriceScheduleItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PriceScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection GetMultiPriceScheduleItemOccurrenceCollection(bool forceFetch)
		{
			return GetMultiPriceScheduleItemOccurrenceCollection(forceFetch, _priceScheduleItemOccurrenceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PriceScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection GetMultiPriceScheduleItemOccurrenceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPriceScheduleItemOccurrenceCollection(forceFetch, _priceScheduleItemOccurrenceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection GetMultiPriceScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPriceScheduleItemOccurrenceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection GetMultiPriceScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPriceScheduleItemOccurrenceCollection || forceFetch || _alwaysFetchPriceScheduleItemOccurrenceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_priceScheduleItemOccurrenceCollection);
				_priceScheduleItemOccurrenceCollection.SuppressClearInGetMulti=!forceFetch;
				_priceScheduleItemOccurrenceCollection.EntityFactoryToUse = entityFactoryToUse;
				_priceScheduleItemOccurrenceCollection.GetMultiManyToOne(this, filter);
				_priceScheduleItemOccurrenceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPriceScheduleItemOccurrenceCollection = true;
			}
			return _priceScheduleItemOccurrenceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PriceScheduleItemOccurrenceCollection'. These settings will be taken into account
		/// when the property PriceScheduleItemOccurrenceCollection is requested or GetMultiPriceScheduleItemOccurrenceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPriceScheduleItemOccurrenceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_priceScheduleItemOccurrenceCollection.SortClauses=sortClauses;
			_priceScheduleItemOccurrenceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PriceLevelEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceLevelEntity' which is related to this entity.</returns>
		public PriceLevelEntity GetSinglePriceLevelEntity()
		{
			return GetSinglePriceLevelEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceLevelEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceLevelEntity' which is related to this entity.</returns>
		public virtual PriceLevelEntity GetSinglePriceLevelEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceLevelEntity || forceFetch || _alwaysFetchPriceLevelEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceLevelEntityUsingPriceLevelId);
				PriceLevelEntity newEntity = new PriceLevelEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceLevelId);
				}
				if(fetchResult)
				{
					newEntity = (PriceLevelEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceLevelEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceLevelEntity = newEntity;
				_alreadyFetchedPriceLevelEntity = fetchResult;
			}
			return _priceLevelEntity;
		}


		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public PriceScheduleEntity GetSinglePriceScheduleEntity()
		{
			return GetSinglePriceScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public virtual PriceScheduleEntity GetSinglePriceScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceScheduleEntity || forceFetch || _alwaysFetchPriceScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceScheduleEntityUsingPriceScheduleId);
				PriceScheduleEntity newEntity = new PriceScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceScheduleId);
				}
				if(fetchResult)
				{
					newEntity = (PriceScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceScheduleEntity = newEntity;
				_alreadyFetchedPriceScheduleEntity = fetchResult;
			}
			return _priceScheduleEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PriceLevelEntity", _priceLevelEntity);
			toReturn.Add("PriceScheduleEntity", _priceScheduleEntity);
			toReturn.Add("PriceScheduleItemOccurrenceCollection", _priceScheduleItemOccurrenceCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="validator">The validator object for this PriceScheduleItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 priceScheduleItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(priceScheduleItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_priceScheduleItemOccurrenceCollection = new Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection();
			_priceScheduleItemOccurrenceCollection.SetContainingEntityInfo(this, "PriceScheduleItemEntity");
			_priceLevelEntityReturnsNewIfNotFound = true;
			_priceScheduleEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceScheduleItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _priceLevelEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceLevelEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceLevelEntity, new PropertyChangedEventHandler( OnPriceLevelEntityPropertyChanged ), "PriceLevelEntity", Obymobi.Data.RelationClasses.StaticPriceScheduleItemRelations.PriceLevelEntityUsingPriceLevelIdStatic, true, signalRelatedEntity, "PriceScheduleItemCollection", resetFKFields, new int[] { (int)PriceScheduleItemFieldIndex.PriceLevelId } );		
			_priceLevelEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceLevelEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceLevelEntity(IEntityCore relatedEntity)
		{
			if(_priceLevelEntity!=relatedEntity)
			{		
				DesetupSyncPriceLevelEntity(true, true);
				_priceLevelEntity = (PriceLevelEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceLevelEntity, new PropertyChangedEventHandler( OnPriceLevelEntityPropertyChanged ), "PriceLevelEntity", Obymobi.Data.RelationClasses.StaticPriceScheduleItemRelations.PriceLevelEntityUsingPriceLevelIdStatic, true, ref _alreadyFetchedPriceLevelEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceLevelEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticPriceScheduleItemRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, signalRelatedEntity, "PriceScheduleItemCollection", resetFKFields, new int[] { (int)PriceScheduleItemFieldIndex.PriceScheduleId } );		
			_priceScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceScheduleEntity(IEntityCore relatedEntity)
		{
			if(_priceScheduleEntity!=relatedEntity)
			{		
				DesetupSyncPriceScheduleEntity(true, true);
				_priceScheduleEntity = (PriceScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticPriceScheduleItemRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, ref _alreadyFetchedPriceScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="priceScheduleItemId">PK value for PriceScheduleItem which data should be fetched into this PriceScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 priceScheduleItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PriceScheduleItemFieldIndex.PriceScheduleItemId].ForcedCurrentValueWrite(priceScheduleItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePriceScheduleItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PriceScheduleItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PriceScheduleItemRelations Relations
		{
			get	{ return new PriceScheduleItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceScheduleItemOccurrence' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceScheduleItemOccurrenceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection(), (IEntityRelation)GetRelationsForField("PriceScheduleItemOccurrenceCollection")[0], (int)Obymobi.Data.EntityType.PriceScheduleItemEntity, (int)Obymobi.Data.EntityType.PriceScheduleItemOccurrenceEntity, 0, null, null, null, "PriceScheduleItemOccurrenceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceLevel'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceLevelEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceLevelCollection(), (IEntityRelation)GetRelationsForField("PriceLevelEntity")[0], (int)Obymobi.Data.EntityType.PriceScheduleItemEntity, (int)Obymobi.Data.EntityType.PriceLevelEntity, 0, null, null, null, "PriceLevelEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceSchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceScheduleCollection(), (IEntityRelation)GetRelationsForField("PriceScheduleEntity")[0], (int)Obymobi.Data.EntityType.PriceScheduleItemEntity, (int)Obymobi.Data.EntityType.PriceScheduleEntity, 0, null, null, null, "PriceScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PriceScheduleItemId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."PriceScheduleItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PriceScheduleItemId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemFieldIndex.PriceScheduleItemId, true); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.PriceScheduleItemId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The PriceScheduleId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."PriceScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceScheduleId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemFieldIndex.PriceScheduleId, true); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.PriceScheduleId, value, true); }
		}

		/// <summary> The PriceLevelId property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."PriceLevelId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PriceLevelId
		{
			get { return (System.Int32)GetValue((int)PriceScheduleItemFieldIndex.PriceLevelId, true); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.PriceLevelId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PriceScheduleItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PriceScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PriceScheduleItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PriceScheduleItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PriceScheduleItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PriceScheduleItemOccurrenceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPriceScheduleItemOccurrenceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PriceScheduleItemOccurrenceCollection PriceScheduleItemOccurrenceCollection
		{
			get	{ return GetMultiPriceScheduleItemOccurrenceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PriceScheduleItemOccurrenceCollection. When set to true, PriceScheduleItemOccurrenceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceScheduleItemOccurrenceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPriceScheduleItemOccurrenceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceScheduleItemOccurrenceCollection
		{
			get	{ return _alwaysFetchPriceScheduleItemOccurrenceCollection; }
			set	{ _alwaysFetchPriceScheduleItemOccurrenceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceScheduleItemOccurrenceCollection already has been fetched. Setting this property to false when PriceScheduleItemOccurrenceCollection has been fetched
		/// will clear the PriceScheduleItemOccurrenceCollection collection well. Setting this property to true while PriceScheduleItemOccurrenceCollection hasn't been fetched disables lazy loading for PriceScheduleItemOccurrenceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceScheduleItemOccurrenceCollection
		{
			get { return _alreadyFetchedPriceScheduleItemOccurrenceCollection;}
			set 
			{
				if(_alreadyFetchedPriceScheduleItemOccurrenceCollection && !value && (_priceScheduleItemOccurrenceCollection != null))
				{
					_priceScheduleItemOccurrenceCollection.Clear();
				}
				_alreadyFetchedPriceScheduleItemOccurrenceCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PriceLevelEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceLevelEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceLevelEntity PriceLevelEntity
		{
			get	{ return GetSinglePriceLevelEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceLevelEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceScheduleItemCollection", "PriceLevelEntity", _priceLevelEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceLevelEntity. When set to true, PriceLevelEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceLevelEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceLevelEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceLevelEntity
		{
			get	{ return _alwaysFetchPriceLevelEntity; }
			set	{ _alwaysFetchPriceLevelEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceLevelEntity already has been fetched. Setting this property to false when PriceLevelEntity has been fetched
		/// will set PriceLevelEntity to null as well. Setting this property to true while PriceLevelEntity hasn't been fetched disables lazy loading for PriceLevelEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceLevelEntity
		{
			get { return _alreadyFetchedPriceLevelEntity;}
			set 
			{
				if(_alreadyFetchedPriceLevelEntity && !value)
				{
					this.PriceLevelEntity = null;
				}
				_alreadyFetchedPriceLevelEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceLevelEntity is not found
		/// in the database. When set to true, PriceLevelEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceLevelEntityReturnsNewIfNotFound
		{
			get	{ return _priceLevelEntityReturnsNewIfNotFound; }
			set { _priceLevelEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PriceScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceScheduleEntity PriceScheduleEntity
		{
			get	{ return GetSinglePriceScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PriceScheduleItemCollection", "PriceScheduleEntity", _priceScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceScheduleEntity. When set to true, PriceScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceScheduleEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceScheduleEntity
		{
			get	{ return _alwaysFetchPriceScheduleEntity; }
			set	{ _alwaysFetchPriceScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceScheduleEntity already has been fetched. Setting this property to false when PriceScheduleEntity has been fetched
		/// will set PriceScheduleEntity to null as well. Setting this property to true while PriceScheduleEntity hasn't been fetched disables lazy loading for PriceScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceScheduleEntity
		{
			get { return _alreadyFetchedPriceScheduleEntity;}
			set 
			{
				if(_alreadyFetchedPriceScheduleEntity && !value)
				{
					this.PriceScheduleEntity = null;
				}
				_alreadyFetchedPriceScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceScheduleEntity is not found
		/// in the database. When set to true, PriceScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _priceScheduleEntityReturnsNewIfNotFound; }
			set { _priceScheduleEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PriceScheduleItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
