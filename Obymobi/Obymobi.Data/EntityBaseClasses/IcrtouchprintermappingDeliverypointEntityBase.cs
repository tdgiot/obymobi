﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'IcrtouchprintermappingDeliverypoint'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class IcrtouchprintermappingDeliverypointEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "IcrtouchprintermappingDeliverypointEntity"; }
		}
	
		#region Class Member Declarations
		private DeliverypointEntity _deliverypointEntity;
		private bool	_alwaysFetchDeliverypointEntity, _alreadyFetchedDeliverypointEntity, _deliverypointEntityReturnsNewIfNotFound;
		private IcrtouchprintermappingEntity _icrtouchprintermappingEntity;
		private bool	_alwaysFetchIcrtouchprintermappingEntity, _alreadyFetchedIcrtouchprintermappingEntity, _icrtouchprintermappingEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeliverypointEntity</summary>
			public static readonly string DeliverypointEntity = "DeliverypointEntity";
			/// <summary>Member name IcrtouchprintermappingEntity</summary>
			public static readonly string IcrtouchprintermappingEntity = "IcrtouchprintermappingEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static IcrtouchprintermappingDeliverypointEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected IcrtouchprintermappingDeliverypointEntityBase() :base("IcrtouchprintermappingDeliverypointEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		protected IcrtouchprintermappingDeliverypointEntityBase(System.Int32 icrtouchprintermappingDeliverypointId):base("IcrtouchprintermappingDeliverypointEntity")
		{
			InitClassFetch(icrtouchprintermappingDeliverypointId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected IcrtouchprintermappingDeliverypointEntityBase(System.Int32 icrtouchprintermappingDeliverypointId, IPrefetchPath prefetchPathToUse): base("IcrtouchprintermappingDeliverypointEntity")
		{
			InitClassFetch(icrtouchprintermappingDeliverypointId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="validator">The custom validator object for this IcrtouchprintermappingDeliverypointEntity</param>
		protected IcrtouchprintermappingDeliverypointEntityBase(System.Int32 icrtouchprintermappingDeliverypointId, IValidator validator):base("IcrtouchprintermappingDeliverypointEntity")
		{
			InitClassFetch(icrtouchprintermappingDeliverypointId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected IcrtouchprintermappingDeliverypointEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deliverypointEntity = (DeliverypointEntity)info.GetValue("_deliverypointEntity", typeof(DeliverypointEntity));
			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity = info.GetBoolean("_alwaysFetchDeliverypointEntity");
			_alreadyFetchedDeliverypointEntity = info.GetBoolean("_alreadyFetchedDeliverypointEntity");

			_icrtouchprintermappingEntity = (IcrtouchprintermappingEntity)info.GetValue("_icrtouchprintermappingEntity", typeof(IcrtouchprintermappingEntity));
			if(_icrtouchprintermappingEntity!=null)
			{
				_icrtouchprintermappingEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_icrtouchprintermappingEntityReturnsNewIfNotFound = info.GetBoolean("_icrtouchprintermappingEntityReturnsNewIfNotFound");
			_alwaysFetchIcrtouchprintermappingEntity = info.GetBoolean("_alwaysFetchIcrtouchprintermappingEntity");
			_alreadyFetchedIcrtouchprintermappingEntity = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((IcrtouchprintermappingDeliverypointFieldIndex)fieldIndex)
			{
				case IcrtouchprintermappingDeliverypointFieldIndex.IcrtouchprintermappingId:
					DesetupSyncIcrtouchprintermappingEntity(true, false);
					_alreadyFetchedIcrtouchprintermappingEntity = false;
					break;
				case IcrtouchprintermappingDeliverypointFieldIndex.DeliverypointId:
					DesetupSyncDeliverypointEntity(true, false);
					_alreadyFetchedDeliverypointEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeliverypointEntity = (_deliverypointEntity != null);
			_alreadyFetchedIcrtouchprintermappingEntity = (_icrtouchprintermappingEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingDeliverypointId);
					break;
				case "IcrtouchprintermappingEntity":
					toReturn.Add(Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deliverypointEntity", (!this.MarkedForDeletion?_deliverypointEntity:null));
			info.AddValue("_deliverypointEntityReturnsNewIfNotFound", _deliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity", _alwaysFetchDeliverypointEntity);
			info.AddValue("_alreadyFetchedDeliverypointEntity", _alreadyFetchedDeliverypointEntity);
			info.AddValue("_icrtouchprintermappingEntity", (!this.MarkedForDeletion?_icrtouchprintermappingEntity:null));
			info.AddValue("_icrtouchprintermappingEntityReturnsNewIfNotFound", _icrtouchprintermappingEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchIcrtouchprintermappingEntity", _alwaysFetchIcrtouchprintermappingEntity);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingEntity", _alreadyFetchedIcrtouchprintermappingEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeliverypointEntity":
					_alreadyFetchedDeliverypointEntity = true;
					this.DeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "IcrtouchprintermappingEntity":
					_alreadyFetchedIcrtouchprintermappingEntity = true;
					this.IcrtouchprintermappingEntity = (IcrtouchprintermappingEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeliverypointEntity":
					SetupSyncDeliverypointEntity(relatedEntity);
					break;
				case "IcrtouchprintermappingEntity":
					SetupSyncIcrtouchprintermappingEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeliverypointEntity":
					DesetupSyncDeliverypointEntity(false, true);
					break;
				case "IcrtouchprintermappingEntity":
					DesetupSyncIcrtouchprintermappingEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deliverypointEntity!=null)
			{
				toReturn.Add(_deliverypointEntity);
			}
			if(_icrtouchprintermappingEntity!=null)
			{
				toReturn.Add(_icrtouchprintermappingEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 icrtouchprintermappingDeliverypointId)
		{
			return FetchUsingPK(icrtouchprintermappingDeliverypointId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 icrtouchprintermappingDeliverypointId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(icrtouchprintermappingDeliverypointId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 icrtouchprintermappingDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(icrtouchprintermappingDeliverypointId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 icrtouchprintermappingDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(icrtouchprintermappingDeliverypointId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.IcrtouchprintermappingDeliverypointId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new IcrtouchprintermappingDeliverypointRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity()
		{
			return GetSingleDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity || forceFetch || _alwaysFetchDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointId);
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointEntity = newEntity;
				_alreadyFetchedDeliverypointEntity = fetchResult;
			}
			return _deliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'IcrtouchprintermappingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'IcrtouchprintermappingEntity' which is related to this entity.</returns>
		public IcrtouchprintermappingEntity GetSingleIcrtouchprintermappingEntity()
		{
			return GetSingleIcrtouchprintermappingEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'IcrtouchprintermappingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'IcrtouchprintermappingEntity' which is related to this entity.</returns>
		public virtual IcrtouchprintermappingEntity GetSingleIcrtouchprintermappingEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedIcrtouchprintermappingEntity || forceFetch || _alwaysFetchIcrtouchprintermappingEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId);
				IcrtouchprintermappingEntity newEntity = new IcrtouchprintermappingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.IcrtouchprintermappingId);
				}
				if(fetchResult)
				{
					newEntity = (IcrtouchprintermappingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_icrtouchprintermappingEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.IcrtouchprintermappingEntity = newEntity;
				_alreadyFetchedIcrtouchprintermappingEntity = fetchResult;
			}
			return _icrtouchprintermappingEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeliverypointEntity", _deliverypointEntity);
			toReturn.Add("IcrtouchprintermappingEntity", _icrtouchprintermappingEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="validator">The validator object for this IcrtouchprintermappingDeliverypointEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 icrtouchprintermappingDeliverypointId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(icrtouchprintermappingDeliverypointId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_deliverypointEntityReturnsNewIfNotFound = true;
			_icrtouchprintermappingEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IcrtouchprintermappingDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IcrtouchprintermappingId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Printer10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticIcrtouchprintermappingDeliverypointRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, signalRelatedEntity, "IcrtouchprintermappingDeliverypointCollection", resetFKFields, new int[] { (int)IcrtouchprintermappingDeliverypointFieldIndex.DeliverypointId } );		
			_deliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointEntity(true, true);
				_deliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticIcrtouchprintermappingDeliverypointRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, ref _alreadyFetchedDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _icrtouchprintermappingEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncIcrtouchprintermappingEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _icrtouchprintermappingEntity, new PropertyChangedEventHandler( OnIcrtouchprintermappingEntityPropertyChanged ), "IcrtouchprintermappingEntity", Obymobi.Data.RelationClasses.StaticIcrtouchprintermappingDeliverypointRelations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingIdStatic, true, signalRelatedEntity, "IcrtouchprintermappingDeliverypointCollection", resetFKFields, new int[] { (int)IcrtouchprintermappingDeliverypointFieldIndex.IcrtouchprintermappingId } );		
			_icrtouchprintermappingEntity = null;
		}
		
		/// <summary> setups the sync logic for member _icrtouchprintermappingEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncIcrtouchprintermappingEntity(IEntityCore relatedEntity)
		{
			if(_icrtouchprintermappingEntity!=relatedEntity)
			{		
				DesetupSyncIcrtouchprintermappingEntity(true, true);
				_icrtouchprintermappingEntity = (IcrtouchprintermappingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _icrtouchprintermappingEntity, new PropertyChangedEventHandler( OnIcrtouchprintermappingEntityPropertyChanged ), "IcrtouchprintermappingEntity", Obymobi.Data.RelationClasses.StaticIcrtouchprintermappingDeliverypointRelations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingIdStatic, true, ref _alreadyFetchedIcrtouchprintermappingEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnIcrtouchprintermappingEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 icrtouchprintermappingDeliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)IcrtouchprintermappingDeliverypointFieldIndex.IcrtouchprintermappingDeliverypointId].ForcedCurrentValueWrite(icrtouchprintermappingDeliverypointId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateIcrtouchprintermappingDeliverypointDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new IcrtouchprintermappingDeliverypointEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static IcrtouchprintermappingDeliverypointRelations Relations
		{
			get	{ return new IcrtouchprintermappingDeliverypointRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointEntity")[0], (int)Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), (IEntityRelation)GetRelationsForField("IcrtouchprintermappingEntity")[0], (int)Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, null, "IcrtouchprintermappingEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The IcrtouchprintermappingDeliverypointId property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."IcrtouchprintermappingDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 IcrtouchprintermappingDeliverypointId
		{
			get { return (System.Int32)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.IcrtouchprintermappingDeliverypointId, true); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.IcrtouchprintermappingDeliverypointId, value, true); }
		}

		/// <summary> The IcrtouchprintermappingId property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."IcrtouchprintermappingId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IcrtouchprintermappingId
		{
			get { return (System.Int32)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.IcrtouchprintermappingId, true); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.IcrtouchprintermappingId, value, true); }
		}

		/// <summary> The DeliverypointId property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointId
		{
			get { return (System.Int32)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.DeliverypointId, true); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The Printer1 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer1
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer1, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer1, value, true); }
		}

		/// <summary> The Printer2 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer2
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer2, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer2, value, true); }
		}

		/// <summary> The Printer3 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer3
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer3, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer3, value, true); }
		}

		/// <summary> The Printer4 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer4
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer4, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer4, value, true); }
		}

		/// <summary> The Printer5 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer5
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer5, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer5, value, true); }
		}

		/// <summary> The Printer6 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer6"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer6
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer6, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer6, value, true); }
		}

		/// <summary> The Printer7 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer7"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer7
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer7, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer7, value, true); }
		}

		/// <summary> The Printer8 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer8"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer8
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer8, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer8, value, true); }
		}

		/// <summary> The Printer9 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer9"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer9
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer9, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer9, value, true); }
		}

		/// <summary> The Printer10 property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."Printer10"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Printer10
		{
			get { return (Nullable<System.Int32>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer10, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.Printer10, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity IcrtouchprintermappingDeliverypoint<br/><br/></summary>
		/// <remarks>Mapped on  table field: "IcrtouchprintermappingDeliverypoint"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)IcrtouchprintermappingDeliverypointFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity DeliverypointEntity
		{
			get	{ return GetSingleDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "IcrtouchprintermappingDeliverypointCollection", "DeliverypointEntity", _deliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity. When set to true, DeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity
		{
			get	{ return _alwaysFetchDeliverypointEntity; }
			set	{ _alwaysFetchDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity already has been fetched. Setting this property to false when DeliverypointEntity has been fetched
		/// will set DeliverypointEntity to null as well. Setting this property to true while DeliverypointEntity hasn't been fetched disables lazy loading for DeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity
		{
			get { return _alreadyFetchedDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity && !value)
				{
					this.DeliverypointEntity = null;
				}
				_alreadyFetchedDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity is not found
		/// in the database. When set to true, DeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntityReturnsNewIfNotFound; }
			set { _deliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'IcrtouchprintermappingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleIcrtouchprintermappingEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual IcrtouchprintermappingEntity IcrtouchprintermappingEntity
		{
			get	{ return GetSingleIcrtouchprintermappingEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncIcrtouchprintermappingEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "IcrtouchprintermappingDeliverypointCollection", "IcrtouchprintermappingEntity", _icrtouchprintermappingEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingEntity. When set to true, IcrtouchprintermappingEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingEntity is accessed. You can always execute a forced fetch by calling GetSingleIcrtouchprintermappingEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingEntity
		{
			get	{ return _alwaysFetchIcrtouchprintermappingEntity; }
			set	{ _alwaysFetchIcrtouchprintermappingEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingEntity already has been fetched. Setting this property to false when IcrtouchprintermappingEntity has been fetched
		/// will set IcrtouchprintermappingEntity to null as well. Setting this property to true while IcrtouchprintermappingEntity hasn't been fetched disables lazy loading for IcrtouchprintermappingEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingEntity
		{
			get { return _alreadyFetchedIcrtouchprintermappingEntity;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingEntity && !value)
				{
					this.IcrtouchprintermappingEntity = null;
				}
				_alreadyFetchedIcrtouchprintermappingEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property IcrtouchprintermappingEntity is not found
		/// in the database. When set to true, IcrtouchprintermappingEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool IcrtouchprintermappingEntityReturnsNewIfNotFound
		{
			get	{ return _icrtouchprintermappingEntityReturnsNewIfNotFound; }
			set { _icrtouchprintermappingEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
