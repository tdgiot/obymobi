﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'TerminalLog'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class TerminalLogEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "TerminalLogEntity"; }
		}
	
		#region Class Member Declarations
		private OrderEntity _orderEntity;
		private bool	_alwaysFetchOrderEntity, _alreadyFetchedOrderEntity, _orderEntityReturnsNewIfNotFound;
		private TerminalEntity _terminalEntity;
		private bool	_alwaysFetchTerminalEntity, _alreadyFetchedTerminalEntity, _terminalEntityReturnsNewIfNotFound;
		private TerminalLogFileEntity _terminalLogFileEntity;
		private bool	_alwaysFetchTerminalLogFileEntity, _alreadyFetchedTerminalLogFileEntity, _terminalLogFileEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OrderEntity</summary>
			public static readonly string OrderEntity = "OrderEntity";
			/// <summary>Member name TerminalEntity</summary>
			public static readonly string TerminalEntity = "TerminalEntity";
			/// <summary>Member name TerminalLogFileEntity</summary>
			public static readonly string TerminalLogFileEntity = "TerminalLogFileEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TerminalLogEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected TerminalLogEntityBase() :base("TerminalLogEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		protected TerminalLogEntityBase(System.Int32 terminalLogId):base("TerminalLogEntity")
		{
			InitClassFetch(terminalLogId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected TerminalLogEntityBase(System.Int32 terminalLogId, IPrefetchPath prefetchPathToUse): base("TerminalLogEntity")
		{
			InitClassFetch(terminalLogId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="validator">The custom validator object for this TerminalLogEntity</param>
		protected TerminalLogEntityBase(System.Int32 terminalLogId, IValidator validator):base("TerminalLogEntity")
		{
			InitClassFetch(terminalLogId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalLogEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_orderEntity = (OrderEntity)info.GetValue("_orderEntity", typeof(OrderEntity));
			if(_orderEntity!=null)
			{
				_orderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderEntityReturnsNewIfNotFound = info.GetBoolean("_orderEntityReturnsNewIfNotFound");
			_alwaysFetchOrderEntity = info.GetBoolean("_alwaysFetchOrderEntity");
			_alreadyFetchedOrderEntity = info.GetBoolean("_alreadyFetchedOrderEntity");

			_terminalEntity = (TerminalEntity)info.GetValue("_terminalEntity", typeof(TerminalEntity));
			if(_terminalEntity!=null)
			{
				_terminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalEntityReturnsNewIfNotFound = info.GetBoolean("_terminalEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalEntity = info.GetBoolean("_alwaysFetchTerminalEntity");
			_alreadyFetchedTerminalEntity = info.GetBoolean("_alreadyFetchedTerminalEntity");

			_terminalLogFileEntity = (TerminalLogFileEntity)info.GetValue("_terminalLogFileEntity", typeof(TerminalLogFileEntity));
			if(_terminalLogFileEntity!=null)
			{
				_terminalLogFileEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalLogFileEntityReturnsNewIfNotFound = info.GetBoolean("_terminalLogFileEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalLogFileEntity = info.GetBoolean("_alwaysFetchTerminalLogFileEntity");
			_alreadyFetchedTerminalLogFileEntity = info.GetBoolean("_alreadyFetchedTerminalLogFileEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TerminalLogFieldIndex)fieldIndex)
			{
				case TerminalLogFieldIndex.TerminalId:
					DesetupSyncTerminalEntity(true, false);
					_alreadyFetchedTerminalEntity = false;
					break;
				case TerminalLogFieldIndex.OrderId:
					DesetupSyncOrderEntity(true, false);
					_alreadyFetchedOrderEntity = false;
					break;
				case TerminalLogFieldIndex.TerminalLogFileId:
					DesetupSyncTerminalLogFileEntity(true, false);
					_alreadyFetchedTerminalLogFileEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrderEntity = (_orderEntity != null);
			_alreadyFetchedTerminalEntity = (_terminalEntity != null);
			_alreadyFetchedTerminalLogFileEntity = (_terminalLogFileEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "OrderEntity":
					toReturn.Add(Relations.OrderEntityUsingOrderId);
					break;
				case "TerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingTerminalId);
					break;
				case "TerminalLogFileEntity":
					toReturn.Add(Relations.TerminalLogFileEntityUsingTerminalLogFileId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_orderEntity", (!this.MarkedForDeletion?_orderEntity:null));
			info.AddValue("_orderEntityReturnsNewIfNotFound", _orderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderEntity", _alwaysFetchOrderEntity);
			info.AddValue("_alreadyFetchedOrderEntity", _alreadyFetchedOrderEntity);
			info.AddValue("_terminalEntity", (!this.MarkedForDeletion?_terminalEntity:null));
			info.AddValue("_terminalEntityReturnsNewIfNotFound", _terminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalEntity", _alwaysFetchTerminalEntity);
			info.AddValue("_alreadyFetchedTerminalEntity", _alreadyFetchedTerminalEntity);
			info.AddValue("_terminalLogFileEntity", (!this.MarkedForDeletion?_terminalLogFileEntity:null));
			info.AddValue("_terminalLogFileEntityReturnsNewIfNotFound", _terminalLogFileEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalLogFileEntity", _alwaysFetchTerminalLogFileEntity);
			info.AddValue("_alreadyFetchedTerminalLogFileEntity", _alreadyFetchedTerminalLogFileEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "OrderEntity":
					_alreadyFetchedOrderEntity = true;
					this.OrderEntity = (OrderEntity)entity;
					break;
				case "TerminalEntity":
					_alreadyFetchedTerminalEntity = true;
					this.TerminalEntity = (TerminalEntity)entity;
					break;
				case "TerminalLogFileEntity":
					_alreadyFetchedTerminalLogFileEntity = true;
					this.TerminalLogFileEntity = (TerminalLogFileEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "OrderEntity":
					SetupSyncOrderEntity(relatedEntity);
					break;
				case "TerminalEntity":
					SetupSyncTerminalEntity(relatedEntity);
					break;
				case "TerminalLogFileEntity":
					SetupSyncTerminalLogFileEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "OrderEntity":
					DesetupSyncOrderEntity(false, true);
					break;
				case "TerminalEntity":
					DesetupSyncTerminalEntity(false, true);
					break;
				case "TerminalLogFileEntity":
					DesetupSyncTerminalLogFileEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_orderEntity!=null)
			{
				toReturn.Add(_orderEntity);
			}
			if(_terminalEntity!=null)
			{
				toReturn.Add(_terminalEntity);
			}
			if(_terminalLogFileEntity!=null)
			{
				toReturn.Add(_terminalLogFileEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogId)
		{
			return FetchUsingPK(terminalLogId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(terminalLogId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(terminalLogId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(terminalLogId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TerminalLogId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TerminalLogRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrderEntity()
		{
			return GetSingleOrderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderEntity || forceFetch || _alwaysFetchOrderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderId);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderEntity = newEntity;
				_alreadyFetchedOrderEntity = fetchResult;
			}
			return _orderEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleTerminalEntity()
		{
			return GetSingleTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalEntity || forceFetch || _alwaysFetchTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalId);
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalEntity = newEntity;
				_alreadyFetchedTerminalEntity = fetchResult;
			}
			return _terminalEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalLogFileEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalLogFileEntity' which is related to this entity.</returns>
		public TerminalLogFileEntity GetSingleTerminalLogFileEntity()
		{
			return GetSingleTerminalLogFileEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalLogFileEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalLogFileEntity' which is related to this entity.</returns>
		public virtual TerminalLogFileEntity GetSingleTerminalLogFileEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalLogFileEntity || forceFetch || _alwaysFetchTerminalLogFileEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalLogFileEntityUsingTerminalLogFileId);
				TerminalLogFileEntity newEntity = new TerminalLogFileEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalLogFileId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalLogFileEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalLogFileEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalLogFileEntity = newEntity;
				_alreadyFetchedTerminalLogFileEntity = fetchResult;
			}
			return _terminalLogFileEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("OrderEntity", _orderEntity);
			toReturn.Add("TerminalEntity", _terminalEntity);
			toReturn.Add("TerminalLogFileEntity", _terminalLogFileEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="validator">The validator object for this TerminalLogEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 terminalLogId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(terminalLogId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_orderEntityReturnsNewIfNotFound = true;
			_terminalEntityReturnsNewIfNotFound = true;
			_terminalLogFileEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalLogId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Log", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalLogFileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _orderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticTerminalLogRelations.OrderEntityUsingOrderIdStatic, true, signalRelatedEntity, "TerminalLogCollection", resetFKFields, new int[] { (int)TerminalLogFieldIndex.OrderId } );		
			_orderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderEntity(IEntityCore relatedEntity)
		{
			if(_orderEntity!=relatedEntity)
			{		
				DesetupSyncOrderEntity(true, true);
				_orderEntity = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticTerminalLogRelations.OrderEntityUsingOrderIdStatic, true, ref _alreadyFetchedOrderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _terminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticTerminalLogRelations.TerminalEntityUsingTerminalIdStatic, true, signalRelatedEntity, "TerminalLogCollection", resetFKFields, new int[] { (int)TerminalLogFieldIndex.TerminalId } );		
			_terminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _terminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalEntity(IEntityCore relatedEntity)
		{
			if(_terminalEntity!=relatedEntity)
			{		
				DesetupSyncTerminalEntity(true, true);
				_terminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticTerminalLogRelations.TerminalEntityUsingTerminalIdStatic, true, ref _alreadyFetchedTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _terminalLogFileEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalLogFileEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalLogFileEntity, new PropertyChangedEventHandler( OnTerminalLogFileEntityPropertyChanged ), "TerminalLogFileEntity", Obymobi.Data.RelationClasses.StaticTerminalLogRelations.TerminalLogFileEntityUsingTerminalLogFileIdStatic, true, signalRelatedEntity, "TerminalLogCollection", resetFKFields, new int[] { (int)TerminalLogFieldIndex.TerminalLogFileId } );		
			_terminalLogFileEntity = null;
		}
		
		/// <summary> setups the sync logic for member _terminalLogFileEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalLogFileEntity(IEntityCore relatedEntity)
		{
			if(_terminalLogFileEntity!=relatedEntity)
			{		
				DesetupSyncTerminalLogFileEntity(true, true);
				_terminalLogFileEntity = (TerminalLogFileEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalLogFileEntity, new PropertyChangedEventHandler( OnTerminalLogFileEntityPropertyChanged ), "TerminalLogFileEntity", Obymobi.Data.RelationClasses.StaticTerminalLogRelations.TerminalLogFileEntityUsingTerminalLogFileIdStatic, true, ref _alreadyFetchedTerminalLogFileEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalLogFileEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 terminalLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TerminalLogFieldIndex.TerminalLogId].ForcedCurrentValueWrite(terminalLogId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTerminalLogDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TerminalLogEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TerminalLogRelations Relations
		{
			get	{ return new TerminalLogRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderEntity")[0], (int)Obymobi.Data.EntityType.TerminalLogEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalEntity")[0], (int)Obymobi.Data.EntityType.TerminalLogEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalLogFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalLogFileEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalLogFileCollection(), (IEntityRelation)GetRelationsForField("TerminalLogFileEntity")[0], (int)Obymobi.Data.EntityType.TerminalLogEntity, (int)Obymobi.Data.EntityType.TerminalLogFileEntity, 0, null, null, null, "TerminalLogFileEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The TerminalLogId property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."TerminalLogId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TerminalLogId
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.TerminalLogId, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.TerminalLogId, value, true); }
		}

		/// <summary> The TerminalId property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."TerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TerminalId
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.TerminalId, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.TerminalId, value, true); }
		}

		/// <summary> The OrderId property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalLogFieldIndex.OrderId, false); }
			set	{ SetValue((int)TerminalLogFieldIndex.OrderId, value, true); }
		}

		/// <summary> The OrderGuid property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."OrderGuid"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderGuid
		{
			get { return (System.String)GetValue((int)TerminalLogFieldIndex.OrderGuid, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.OrderGuid, value, true); }
		}

		/// <summary> The Type property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.Type, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.Type, value, true); }
		}

		/// <summary> The Message property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."Message"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)TerminalLogFieldIndex.Message, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.Message, value, true); }
		}

		/// <summary> The Status property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."Status"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Status
		{
			get { return (System.String)GetValue((int)TerminalLogFieldIndex.Status, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.Status, value, true); }
		}

		/// <summary> The Log property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."Log"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Log
		{
			get { return (System.String)GetValue((int)TerminalLogFieldIndex.Log, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.Log, value, true); }
		}

		/// <summary> The FromOperationMode property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."FromOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FromOperationMode
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.FromOperationMode, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.FromOperationMode, value, true); }
		}

		/// <summary> The ToOperationMode property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."ToOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ToOperationMode
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.ToOperationMode, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.ToOperationMode, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The TerminalLogFileId property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."TerminalLogFileId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalLogFileId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalLogFieldIndex.TerminalLogFileId, false); }
			set	{ SetValue((int)TerminalLogFieldIndex.TerminalLogFileId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)TerminalLogFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)TerminalLogFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalLogFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)TerminalLogFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity TerminalLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLog"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalLogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TerminalLogFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderEntity OrderEntity
		{
			get	{ return GetSingleOrderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalLogCollection", "OrderEntity", _orderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderEntity. When set to true, OrderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderEntity
		{
			get	{ return _alwaysFetchOrderEntity; }
			set	{ _alwaysFetchOrderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderEntity already has been fetched. Setting this property to false when OrderEntity has been fetched
		/// will set OrderEntity to null as well. Setting this property to true while OrderEntity hasn't been fetched disables lazy loading for OrderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderEntity
		{
			get { return _alreadyFetchedOrderEntity;}
			set 
			{
				if(_alreadyFetchedOrderEntity && !value)
				{
					this.OrderEntity = null;
				}
				_alreadyFetchedOrderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderEntity is not found
		/// in the database. When set to true, OrderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderEntityReturnsNewIfNotFound
		{
			get	{ return _orderEntityReturnsNewIfNotFound; }
			set { _orderEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity TerminalEntity
		{
			get	{ return GetSingleTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalLogCollection", "TerminalEntity", _terminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalEntity. When set to true, TerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalEntity
		{
			get	{ return _alwaysFetchTerminalEntity; }
			set	{ _alwaysFetchTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalEntity already has been fetched. Setting this property to false when TerminalEntity has been fetched
		/// will set TerminalEntity to null as well. Setting this property to true while TerminalEntity hasn't been fetched disables lazy loading for TerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalEntity
		{
			get { return _alreadyFetchedTerminalEntity;}
			set 
			{
				if(_alreadyFetchedTerminalEntity && !value)
				{
					this.TerminalEntity = null;
				}
				_alreadyFetchedTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalEntity is not found
		/// in the database. When set to true, TerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalEntityReturnsNewIfNotFound
		{
			get	{ return _terminalEntityReturnsNewIfNotFound; }
			set { _terminalEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalLogFileEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalLogFileEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalLogFileEntity TerminalLogFileEntity
		{
			get	{ return GetSingleTerminalLogFileEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTerminalLogFileEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TerminalLogCollection", "TerminalLogFileEntity", _terminalLogFileEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalLogFileEntity. When set to true, TerminalLogFileEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalLogFileEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalLogFileEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalLogFileEntity
		{
			get	{ return _alwaysFetchTerminalLogFileEntity; }
			set	{ _alwaysFetchTerminalLogFileEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalLogFileEntity already has been fetched. Setting this property to false when TerminalLogFileEntity has been fetched
		/// will set TerminalLogFileEntity to null as well. Setting this property to true while TerminalLogFileEntity hasn't been fetched disables lazy loading for TerminalLogFileEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalLogFileEntity
		{
			get { return _alreadyFetchedTerminalLogFileEntity;}
			set 
			{
				if(_alreadyFetchedTerminalLogFileEntity && !value)
				{
					this.TerminalLogFileEntity = null;
				}
				_alreadyFetchedTerminalLogFileEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalLogFileEntity is not found
		/// in the database. When set to true, TerminalLogFileEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalLogFileEntityReturnsNewIfNotFound
		{
			get	{ return _terminalLogFileEntityReturnsNewIfNotFound; }
			set { _terminalLogFileEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.TerminalLogEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
