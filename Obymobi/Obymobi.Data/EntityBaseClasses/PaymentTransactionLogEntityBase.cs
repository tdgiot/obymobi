﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PaymentTransactionLog'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PaymentTransactionLogEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PaymentTransactionLogEntity"; }
		}
	
		#region Class Member Declarations
		private PaymentTransactionEntity _paymentTransactionEntity;
		private bool	_alwaysFetchPaymentTransactionEntity, _alreadyFetchedPaymentTransactionEntity, _paymentTransactionEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PaymentTransactionEntity</summary>
			public static readonly string PaymentTransactionEntity = "PaymentTransactionEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentTransactionLogEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PaymentTransactionLogEntityBase() :base("PaymentTransactionLogEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		protected PaymentTransactionLogEntityBase(System.Int32 paymentTransactionLogId):base("PaymentTransactionLogEntity")
		{
			InitClassFetch(paymentTransactionLogId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PaymentTransactionLogEntityBase(System.Int32 paymentTransactionLogId, IPrefetchPath prefetchPathToUse): base("PaymentTransactionLogEntity")
		{
			InitClassFetch(paymentTransactionLogId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <param name="validator">The custom validator object for this PaymentTransactionLogEntity</param>
		protected PaymentTransactionLogEntityBase(System.Int32 paymentTransactionLogId, IValidator validator):base("PaymentTransactionLogEntity")
		{
			InitClassFetch(paymentTransactionLogId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentTransactionLogEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_paymentTransactionEntity = (PaymentTransactionEntity)info.GetValue("_paymentTransactionEntity", typeof(PaymentTransactionEntity));
			if(_paymentTransactionEntity!=null)
			{
				_paymentTransactionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentTransactionEntityReturnsNewIfNotFound = info.GetBoolean("_paymentTransactionEntityReturnsNewIfNotFound");
			_alwaysFetchPaymentTransactionEntity = info.GetBoolean("_alwaysFetchPaymentTransactionEntity");
			_alreadyFetchedPaymentTransactionEntity = info.GetBoolean("_alreadyFetchedPaymentTransactionEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentTransactionLogFieldIndex)fieldIndex)
			{
				case PaymentTransactionLogFieldIndex.PaymentTransactionId:
					DesetupSyncPaymentTransactionEntity(true, false);
					_alreadyFetchedPaymentTransactionEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPaymentTransactionEntity = (_paymentTransactionEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PaymentTransactionEntity":
					toReturn.Add(Relations.PaymentTransactionEntityUsingPaymentTransactionId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_paymentTransactionEntity", (!this.MarkedForDeletion?_paymentTransactionEntity:null));
			info.AddValue("_paymentTransactionEntityReturnsNewIfNotFound", _paymentTransactionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentTransactionEntity", _alwaysFetchPaymentTransactionEntity);
			info.AddValue("_alreadyFetchedPaymentTransactionEntity", _alreadyFetchedPaymentTransactionEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PaymentTransactionEntity":
					_alreadyFetchedPaymentTransactionEntity = true;
					this.PaymentTransactionEntity = (PaymentTransactionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PaymentTransactionEntity":
					SetupSyncPaymentTransactionEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PaymentTransactionEntity":
					DesetupSyncPaymentTransactionEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_paymentTransactionEntity!=null)
			{
				toReturn.Add(_paymentTransactionEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionLogId)
		{
			return FetchUsingPK(paymentTransactionLogId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionLogId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentTransactionLogId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionLogId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentTransactionLogId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentTransactionLogId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentTransactionLogId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentTransactionLogRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PaymentTransactionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentTransactionEntity' which is related to this entity.</returns>
		public PaymentTransactionEntity GetSinglePaymentTransactionEntity()
		{
			return GetSinglePaymentTransactionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentTransactionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentTransactionEntity' which is related to this entity.</returns>
		public virtual PaymentTransactionEntity GetSinglePaymentTransactionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentTransactionEntity || forceFetch || _alwaysFetchPaymentTransactionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentTransactionEntityUsingPaymentTransactionId);
				PaymentTransactionEntity newEntity = new PaymentTransactionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentTransactionId);
				}
				if(fetchResult)
				{
					newEntity = (PaymentTransactionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentTransactionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentTransactionEntity = newEntity;
				_alreadyFetchedPaymentTransactionEntity = fetchResult;
			}
			return _paymentTransactionEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PaymentTransactionEntity", _paymentTransactionEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <param name="validator">The validator object for this PaymentTransactionLogEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 paymentTransactionLogId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentTransactionLogId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_paymentTransactionEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentTransactionLogId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentTransactionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Success", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RawResponse", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _paymentTransactionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentTransactionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentTransactionEntity, new PropertyChangedEventHandler( OnPaymentTransactionEntityPropertyChanged ), "PaymentTransactionEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionLogRelations.PaymentTransactionEntityUsingPaymentTransactionIdStatic, true, signalRelatedEntity, "PaymentTransactionLogCollection", resetFKFields, new int[] { (int)PaymentTransactionLogFieldIndex.PaymentTransactionId } );		
			_paymentTransactionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _paymentTransactionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentTransactionEntity(IEntityCore relatedEntity)
		{
			if(_paymentTransactionEntity!=relatedEntity)
			{		
				DesetupSyncPaymentTransactionEntity(true, true);
				_paymentTransactionEntity = (PaymentTransactionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentTransactionEntity, new PropertyChangedEventHandler( OnPaymentTransactionEntityPropertyChanged ), "PaymentTransactionEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionLogRelations.PaymentTransactionEntityUsingPaymentTransactionIdStatic, true, ref _alreadyFetchedPaymentTransactionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentTransactionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentTransactionLogId">PK value for PaymentTransactionLog which data should be fetched into this PaymentTransactionLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 paymentTransactionLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentTransactionLogFieldIndex.PaymentTransactionLogId].ForcedCurrentValueWrite(paymentTransactionLogId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentTransactionLogDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentTransactionLogEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentTransactionLogRelations Relations
		{
			get	{ return new PaymentTransactionLogRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentTransaction'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentTransactionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentTransactionCollection(), (IEntityRelation)GetRelationsForField("PaymentTransactionEntity")[0], (int)Obymobi.Data.EntityType.PaymentTransactionLogEntity, (int)Obymobi.Data.EntityType.PaymentTransactionEntity, 0, null, null, null, "PaymentTransactionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PaymentTransactionLogId property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."PaymentTransactionLogId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PaymentTransactionLogId
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionLogFieldIndex.PaymentTransactionLogId, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.PaymentTransactionLogId, value, true); }
		}

		/// <summary> The PaymentTransactionId property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."PaymentTransactionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PaymentTransactionId
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionLogFieldIndex.PaymentTransactionId, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.PaymentTransactionId, value, true); }
		}

		/// <summary> The EventCode property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."EventCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EventCode
		{
			get { return (System.String)GetValue((int)PaymentTransactionLogFieldIndex.EventCode, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.EventCode, value, true); }
		}

		/// <summary> The EventText property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."EventText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EventText
		{
			get { return (System.String)GetValue((int)PaymentTransactionLogFieldIndex.EventText, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.EventText, value, true); }
		}

		/// <summary> The EventDate property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."EventDate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EventDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentTransactionLogFieldIndex.EventDate, false); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.EventDate, value, true); }
		}

		/// <summary> The Success property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."Success"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Success
		{
			get { return (System.Boolean)GetValue((int)PaymentTransactionLogFieldIndex.Success, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.Success, value, true); }
		}

		/// <summary> The RawResponse property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."RawResponse"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String RawResponse
		{
			get { return (System.String)GetValue((int)PaymentTransactionLogFieldIndex.RawResponse, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.RawResponse, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)PaymentTransactionLogFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentTransactionLogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LogType property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."LogType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PaymentTransactionLogType LogType
		{
			get { return (Obymobi.Enums.PaymentTransactionLogType)GetValue((int)PaymentTransactionLogFieldIndex.LogType, true); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.LogType, value, true); }
		}

		/// <summary> The Amount property of the Entity PaymentTransactionLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransactionLog"."Amount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> Amount
		{
			get { return (Nullable<System.Decimal>)GetValue((int)PaymentTransactionLogFieldIndex.Amount, false); }
			set	{ SetValue((int)PaymentTransactionLogFieldIndex.Amount, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PaymentTransactionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentTransactionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PaymentTransactionEntity PaymentTransactionEntity
		{
			get	{ return GetSinglePaymentTransactionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentTransactionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentTransactionLogCollection", "PaymentTransactionEntity", _paymentTransactionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentTransactionEntity. When set to true, PaymentTransactionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentTransactionEntity is accessed. You can always execute a forced fetch by calling GetSinglePaymentTransactionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentTransactionEntity
		{
			get	{ return _alwaysFetchPaymentTransactionEntity; }
			set	{ _alwaysFetchPaymentTransactionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentTransactionEntity already has been fetched. Setting this property to false when PaymentTransactionEntity has been fetched
		/// will set PaymentTransactionEntity to null as well. Setting this property to true while PaymentTransactionEntity hasn't been fetched disables lazy loading for PaymentTransactionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentTransactionEntity
		{
			get { return _alreadyFetchedPaymentTransactionEntity;}
			set 
			{
				if(_alreadyFetchedPaymentTransactionEntity && !value)
				{
					this.PaymentTransactionEntity = null;
				}
				_alreadyFetchedPaymentTransactionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentTransactionEntity is not found
		/// in the database. When set to true, PaymentTransactionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PaymentTransactionEntityReturnsNewIfNotFound
		{
			get	{ return _paymentTransactionEntityReturnsNewIfNotFound; }
			set { _paymentTransactionEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PaymentTransactionLogEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
