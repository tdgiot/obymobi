﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Entity base class which represents the base class for the entity 'CategoryPoscategory'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CategoryPoscategoryEntityBase : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations


		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private PoscategoryEntity _poscategoryEntity;
		private bool	_alwaysFetchPoscategoryEntity, _alreadyFetchedPoscategoryEntity, _poscategoryEntityReturnsNewIfNotFound;

		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
		
		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name PoscategoryEntity</summary>
			public static readonly string PoscategoryEntity = "PoscategoryEntity";



		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CategoryPoscategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CategoryPoscategoryEntityBase()
		{
			InitClassEmpty(null);
		}

	
		/// <summary>CTor</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		public CategoryPoscategoryEntityBase(System.Int32 categoryPoscategoryId)
		{
			InitClassFetch(categoryPoscategoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CategoryPoscategoryEntityBase(System.Int32 categoryPoscategoryId, IPrefetchPath prefetchPathToUse)
		{
			InitClassFetch(categoryPoscategoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <param name="validator">The custom validator object for this CategoryPoscategoryEntity</param>
		public CategoryPoscategoryEntityBase(System.Int32 categoryPoscategoryId, IValidator validator)
		{
			InitClassFetch(categoryPoscategoryId, validator, null);
		}
	

		/// <summary>Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CategoryPoscategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{


			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");
			_poscategoryEntity = (PoscategoryEntity)info.GetValue("_poscategoryEntity", typeof(PoscategoryEntity));
			if(_poscategoryEntity!=null)
			{
				_poscategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_poscategoryEntityReturnsNewIfNotFound = info.GetBoolean("_poscategoryEntityReturnsNewIfNotFound");
			_alwaysFetchPoscategoryEntity = info.GetBoolean("_alwaysFetchPoscategoryEntity");
			_alreadyFetchedPoscategoryEntity = info.GetBoolean("_alreadyFetchedPoscategoryEntity");

			base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CategoryPoscategoryFieldIndex)fieldIndex)
			{
				case CategoryPoscategoryFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case CategoryPoscategoryFieldIndex.PoscategoryId:
					DesetupSyncPoscategoryEntity(true, false);
					_alreadyFetchedPoscategoryEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
		
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PostReadXmlFixups()
		{


			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedPoscategoryEntity = (_poscategoryEntity != null);

		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return CategoryPoscategoryEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CategoryEntity":
					toReturn.Add(CategoryPoscategoryEntity.Relations.CategoryEntityUsingCategoryId);
					break;
				case "PoscategoryEntity":
					toReturn.Add(CategoryPoscategoryEntity.Relations.PoscategoryEntityUsingPoscategoryId);
					break;



				default:

					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.
		/// Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{


			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_poscategoryEntity", (!this.MarkedForDeletion?_poscategoryEntity:null));
			info.AddValue("_poscategoryEntityReturnsNewIfNotFound", _poscategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPoscategoryEntity", _alwaysFetchPoscategoryEntity);
			info.AddValue("_alreadyFetchedPoscategoryEntity", _alreadyFetchedPoscategoryEntity);

			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity entity)
		{
			switch(propertyName)
			{
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "PoscategoryEntity":
					_alreadyFetchedPoscategoryEntity = true;
					this.PoscategoryEntity = (PoscategoryEntity)entity;
					break;



				default:

					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "PoscategoryEntity":
					SetupSyncPoscategoryEntity(relatedEntity);
					break;


				default:

					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "PoscategoryEntity":
					DesetupSyncPoscategoryEntity(false, true);
					break;


				default:

					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These
		/// entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		public override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();


			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		public override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_poscategoryEntity!=null)
			{
				toReturn.Add(_poscategoryEntity);
			}


			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. The contents of the ArrayList is
		/// used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		public override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		

		

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryPoscategoryId)
		{
			return FetchUsingPK(categoryPoscategoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryPoscategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(categoryPoscategoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryPoscategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return Fetch(categoryPoscategoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 categoryPoscategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(categoryPoscategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. 
		/// Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CategoryPoscategoryId, null, null, null);
		}

		/// <summary> Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(CategoryPoscategoryFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(CategoryPoscategoryFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new CategoryPoscategoryRelations().GetAllRelations();
		}




		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !base.IsSerializing && !base.IsDeserializing  && !base.InDesignMode)			
			{
				bool performLazyLoading = base.CheckIfLazyLoadingShouldOccur(CategoryPoscategoryEntity.Relations.CategoryEntityUsingCategoryId);

				CategoryEntity newEntity = new CategoryEntity();
				if(base.ParticipatesInTransaction)
				{
					base.Transaction.Add(newEntity);
				}
				bool fetchResult = false;
				if(performLazyLoading)
				{
					fetchResult = newEntity.FetchUsingPK(this.CategoryId);
				}
				if(fetchResult)
				{
					if(base.ActiveContext!=null)
					{
						newEntity = (CategoryEntity)base.ActiveContext.Get(newEntity);
					}
					this.CategoryEntity = newEntity;
				}
				else
				{
					if(_categoryEntityReturnsNewIfNotFound)
					{
						if(performLazyLoading || (!performLazyLoading && (_categoryEntity == null)))
						{
							this.CategoryEntity = newEntity;
						}
					}
					else
					{
						this.CategoryEntity = null;
					}
				}
				_alreadyFetchedCategoryEntity = fetchResult;
				if(base.ParticipatesInTransaction && !fetchResult)
				{
					base.Transaction.Remove(newEntity);
				}
			}
			return _categoryEntity;
		}

		/// <summary> Retrieves the related entity of type 'PoscategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PoscategoryEntity' which is related to this entity.</returns>
		public PoscategoryEntity GetSinglePoscategoryEntity()
		{
			return GetSinglePoscategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PoscategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PoscategoryEntity' which is related to this entity.</returns>
		public virtual PoscategoryEntity GetSinglePoscategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPoscategoryEntity || forceFetch || _alwaysFetchPoscategoryEntity) && !base.IsSerializing && !base.IsDeserializing  && !base.InDesignMode)			
			{
				bool performLazyLoading = base.CheckIfLazyLoadingShouldOccur(CategoryPoscategoryEntity.Relations.PoscategoryEntityUsingPoscategoryId);

				PoscategoryEntity newEntity = new PoscategoryEntity();
				if(base.ParticipatesInTransaction)
				{
					base.Transaction.Add(newEntity);
				}
				bool fetchResult = false;
				if(performLazyLoading)
				{
					fetchResult = newEntity.FetchUsingPK(this.PoscategoryId);
				}
				if(fetchResult)
				{
					if(base.ActiveContext!=null)
					{
						newEntity = (PoscategoryEntity)base.ActiveContext.Get(newEntity);
					}
					this.PoscategoryEntity = newEntity;
				}
				else
				{
					if(_poscategoryEntityReturnsNewIfNotFound)
					{
						if(performLazyLoading || (!performLazyLoading && (_poscategoryEntity == null)))
						{
							this.PoscategoryEntity = newEntity;
						}
					}
					else
					{
						this.PoscategoryEntity = null;
					}
				}
				_alreadyFetchedPoscategoryEntity = fetchResult;
				if(base.ParticipatesInTransaction && !fetchResult)
				{
					base.Transaction.Remove(newEntity);
				}
			}
			return _poscategoryEntity;
		}


		/// <summary> Performs the insert action of a new Entity to the persistent storage.</summary>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool InsertEntity()
		{
			CategoryPoscategoryDAO dao = (CategoryPoscategoryDAO)CreateDAOInstance();
			return dao.AddNew(base.Fields, base.Transaction);
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{


			if(_categoryEntity!=null)
			{
				_categoryEntity.ActiveContext = base.ActiveContext;
			}
			if(_poscategoryEntity!=null)
			{
				_poscategoryEntity.ActiveContext = base.ActiveContext;
			}


		}


		/// <summary> Performs the update action of an existing Entity to the persistent storage.</summary>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool UpdateEntity()
		{
			CategoryPoscategoryDAO dao = (CategoryPoscategoryDAO)CreateDAOInstance();
			return dao.UpdateExisting(base.Fields, base.Transaction);
		}
		
		/// <summary> Performs the update action of an existing Entity to the persistent storage.</summary>
		/// <param name="updateRestriction">Predicate expression, meant for concurrency checks in an Update query</param>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool UpdateEntity(IPredicate updateRestriction)
		{
			CategoryPoscategoryDAO dao = (CategoryPoscategoryDAO)CreateDAOInstance();
			return dao.UpdateExisting(base.Fields, base.Transaction, updateRestriction);
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		protected virtual void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			base.Fields = CreateFields();
			base.IsNew=true;
			base.Validator = validatorToUse;

			InitClassMembers();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();
		}
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.CategoryPoscategoryEntity);
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("PoscategoryEntity", _poscategoryEntity);



			return toReturn;
		}
		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <param name="validator">The validator object for this CategoryPoscategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected virtual void InitClassFetch(System.Int32 categoryPoscategoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			base.Validator = validator;
			InitClassMembers();
			base.Fields = CreateFields();
			bool wasSuccesful = Fetch(categoryPoscategoryId, prefetchPathToUse, null, null);
			base.IsNew = !wasSuccesful;

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{


			_categoryEntity = null;
			_categoryEntityReturnsNewIfNotFound = true;
			_alwaysFetchCategoryEntity = false;
			_alreadyFetchedCategoryEntity = false;
			_poscategoryEntity = null;
			_poscategoryEntityReturnsNewIfNotFound = true;
			_alwaysFetchPoscategoryEntity = false;
			_alreadyFetchedPoscategoryEntity = false;


			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("CategoryPoscategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("PoscategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Updated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion


		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", CategoryPoscategoryEntity.Relations.CategoryEntityUsingCategoryId, true, signalRelatedEntity, "CategoryPoscategoryCollection", resetFKFields, new int[] { (int)CategoryPoscategoryFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntity relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", CategoryPoscategoryEntity.Relations.CategoryEntityUsingCategoryId, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _poscategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPoscategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _poscategoryEntity, new PropertyChangedEventHandler( OnPoscategoryEntityPropertyChanged ), "PoscategoryEntity", CategoryPoscategoryEntity.Relations.PoscategoryEntityUsingPoscategoryId, true, signalRelatedEntity, "CategoryPoscategoryCollection", resetFKFields, new int[] { (int)CategoryPoscategoryFieldIndex.PoscategoryId } );		
			_poscategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _poscategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPoscategoryEntity(IEntity relatedEntity)
		{
			if(_poscategoryEntity!=relatedEntity)
			{		
				DesetupSyncPoscategoryEntity(true, true);
				_poscategoryEntity = (PoscategoryEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _poscategoryEntity, new PropertyChangedEventHandler( OnPoscategoryEntityPropertyChanged ), "PoscategoryEntity", CategoryPoscategoryEntity.Relations.PoscategoryEntityUsingPoscategoryId, true, ref _alreadyFetchedPoscategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPoscategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}


		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="categoryPoscategoryId">PK value for CategoryPoscategory which data should be fetched into this CategoryPoscategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 categoryPoscategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				IDao dao = this.CreateDAOInstance();
				base.Fields[(int)CategoryPoscategoryFieldIndex.CategoryPoscategoryId].ForcedCurrentValueWrite(categoryPoscategoryId);
				dao.FetchExisting(this, base.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (base.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCategoryPoscategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CategoryPoscategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CategoryPoscategoryRelations Relations
		{
			get	{ return new CategoryPoscategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}




		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get
			{
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(),
					(IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.CategoryPoscategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Poscategory' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPoscategoryEntity
		{
			get
			{
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PoscategoryCollection(),
					(IEntityRelation)GetRelationsForField("PoscategoryEntity")[0], (int)Obymobi.Data.EntityType.CategoryPoscategoryEntity, (int)Obymobi.Data.EntityType.PoscategoryEntity, 0, null, null, null, "PoscategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}


		/// <summary>Returns the full name for this entity, which is important for the DAO to find back persistence info for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override string LLBLGenProEntityName
		{
			get { return "CategoryPoscategoryEntity";}
		}

		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CategoryPoscategoryEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return CategoryPoscategoryEntity.FieldsCustomProperties;}
		}

		/// <summary> The CategoryPoscategoryId property of the Entity CategoryPoscategory<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "CategoryPoscategory"."CategoryPoscategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CategoryPoscategoryId
		{
			get { return (System.Int32)GetValue((int)CategoryPoscategoryFieldIndex.CategoryPoscategoryId, true); }
			set	{ SetValue((int)CategoryPoscategoryFieldIndex.CategoryPoscategoryId, value, true); }
		}
		/// <summary> The CategoryId property of the Entity CategoryPoscategory<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "CategoryPoscategory"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CategoryId
		{
			get { return (System.Int32)GetValue((int)CategoryPoscategoryFieldIndex.CategoryId, true); }
			set	{ SetValue((int)CategoryPoscategoryFieldIndex.CategoryId, value, true); }
		}
		/// <summary> The PoscategoryId property of the Entity CategoryPoscategory<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "CategoryPoscategory"."PoscategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PoscategoryId
		{
			get { return (System.Int32)GetValue((int)CategoryPoscategoryFieldIndex.PoscategoryId, true); }
			set	{ SetValue((int)CategoryPoscategoryFieldIndex.PoscategoryId, value, true); }
		}
		/// <summary> The Created property of the Entity CategoryPoscategory<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "CategoryPoscategory"."Created"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Created
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CategoryPoscategoryFieldIndex.Created, false); }
			set	{ SetValue((int)CategoryPoscategoryFieldIndex.Created, value, true); }
		}
		/// <summary> The CreatedBy property of the Entity CategoryPoscategory<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "CategoryPoscategory"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CategoryPoscategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CategoryPoscategoryFieldIndex.CreatedBy, value, true); }
		}
		/// <summary> The Updated property of the Entity CategoryPoscategory<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "CategoryPoscategory"."Updated"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CategoryPoscategoryFieldIndex.Updated, false); }
			set	{ SetValue((int)CategoryPoscategoryFieldIndex.Updated, value, true); }
		}
		/// <summary> The UpdatedBy property of the Entity CategoryPoscategory<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "CategoryPoscategory"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CategoryPoscategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CategoryPoscategoryFieldIndex.UpdatedBy, value, true); }
		}



		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.</summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					if(value==null)
					{
						if(_categoryEntity != null)
						{
							_categoryEntity.UnsetRelatedEntity(this, "CategoryPoscategoryCollection");
						}
					}
					else
					{
						if(_categoryEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "CategoryPoscategoryCollection");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute
		/// a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PoscategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.</summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePoscategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PoscategoryEntity PoscategoryEntity
		{
			get	{ return GetSinglePoscategoryEntity(false); }
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncPoscategoryEntity(value);
				}
				else
				{
					if(value==null)
					{
						if(_poscategoryEntity != null)
						{
							_poscategoryEntity.UnsetRelatedEntity(this, "CategoryPoscategoryCollection");
						}
					}
					else
					{
						if(_poscategoryEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "CategoryPoscategoryCollection");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PoscategoryEntity. When set to true, PoscategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PoscategoryEntity is accessed. You can always execute
		/// a forced fetch by calling GetSinglePoscategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPoscategoryEntity
		{
			get	{ return _alwaysFetchPoscategoryEntity; }
			set	{ _alwaysFetchPoscategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PoscategoryEntity already has been fetched. Setting this property to false when PoscategoryEntity has been fetched
		/// will set PoscategoryEntity to null as well. Setting this property to true while PoscategoryEntity hasn't been fetched disables lazy loading for PoscategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPoscategoryEntity
		{
			get { return _alreadyFetchedPoscategoryEntity;}
			set 
			{
				if(_alreadyFetchedPoscategoryEntity && !value)
				{
					this.PoscategoryEntity = null;
				}
				_alreadyFetchedPoscategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PoscategoryEntity is not found
		/// in the database. When set to true, PoscategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PoscategoryEntityReturnsNewIfNotFound
		{
			get	{ return _poscategoryEntityReturnsNewIfNotFound; }
			set { _poscategoryEntityReturnsNewIfNotFound = value; }	
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CategoryPoscategoryEntity; }
		}
		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}
