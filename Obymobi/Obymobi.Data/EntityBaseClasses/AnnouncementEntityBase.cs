﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Announcement'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AnnouncementEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AnnouncementEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection	_announcementLanguageCollection;
		private bool	_alwaysFetchAnnouncementLanguageCollection, _alreadyFetchedAnnouncementLanguageCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection	_deliverypointgroupAnnouncementCollection;
		private bool	_alwaysFetchDeliverypointgroupAnnouncementCollection, _alreadyFetchedDeliverypointgroupAnnouncementCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup, _alreadyFetchedCompanyCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchMenuCollectionViaDeliverypointgroup, _alreadyFetchedMenuCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup, _alreadyFetchedRouteCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup_, _alreadyFetchedRouteCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchTerminalCollectionViaDeliverypointgroup, _alreadyFetchedTerminalCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup, _alreadyFetchedUIModeCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup_, _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup__, _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private CategoryEntity _categoryEntity_;
		private bool	_alwaysFetchCategoryEntity_, _alreadyFetchedCategoryEntity_, _categoryEntity_ReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private EntertainmentcategoryEntity _entertainmentcategoryEntity;
		private bool	_alwaysFetchEntertainmentcategoryEntity, _alreadyFetchedEntertainmentcategoryEntity, _entertainmentcategoryEntityReturnsNewIfNotFound;
		private EntertainmentcategoryEntity _entertainmentcategoryEntity_;
		private bool	_alwaysFetchEntertainmentcategoryEntity_, _alreadyFetchedEntertainmentcategoryEntity_, _entertainmentcategoryEntity_ReturnsNewIfNotFound;
		private MediaEntity _mediaEntity;
		private bool	_alwaysFetchMediaEntity, _alreadyFetchedMediaEntity, _mediaEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name CategoryEntity_</summary>
			public static readonly string CategoryEntity_ = "CategoryEntity_";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name EntertainmentcategoryEntity</summary>
			public static readonly string EntertainmentcategoryEntity = "EntertainmentcategoryEntity";
			/// <summary>Member name EntertainmentcategoryEntity_</summary>
			public static readonly string EntertainmentcategoryEntity_ = "EntertainmentcategoryEntity_";
			/// <summary>Member name MediaEntity</summary>
			public static readonly string MediaEntity = "MediaEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name AnnouncementLanguageCollection</summary>
			public static readonly string AnnouncementLanguageCollection = "AnnouncementLanguageCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name DeliverypointgroupAnnouncementCollection</summary>
			public static readonly string DeliverypointgroupAnnouncementCollection = "DeliverypointgroupAnnouncementCollection";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup = "CompanyCollectionViaDeliverypointgroup";
			/// <summary>Member name MenuCollectionViaDeliverypointgroup</summary>
			public static readonly string MenuCollectionViaDeliverypointgroup = "MenuCollectionViaDeliverypointgroup";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup = "PosdeliverypointgroupCollectionViaDeliverypointgroup";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup = "RouteCollectionViaDeliverypointgroup";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup_</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup_ = "RouteCollectionViaDeliverypointgroup_";
			/// <summary>Member name TerminalCollectionViaDeliverypointgroup</summary>
			public static readonly string TerminalCollectionViaDeliverypointgroup = "TerminalCollectionViaDeliverypointgroup";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup = "UIModeCollectionViaDeliverypointgroup";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup_</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup_ = "UIModeCollectionViaDeliverypointgroup_";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup__</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup__ = "UIModeCollectionViaDeliverypointgroup__";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AnnouncementEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AnnouncementEntityBase() :base("AnnouncementEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		protected AnnouncementEntityBase(System.Int32 announcementId):base("AnnouncementEntity")
		{
			InitClassFetch(announcementId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AnnouncementEntityBase(System.Int32 announcementId, IPrefetchPath prefetchPathToUse): base("AnnouncementEntity")
		{
			InitClassFetch(announcementId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="validator">The custom validator object for this AnnouncementEntity</param>
		protected AnnouncementEntityBase(System.Int32 announcementId, IValidator validator):base("AnnouncementEntity")
		{
			InitClassFetch(announcementId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AnnouncementEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_announcementLanguageCollection = (Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection)info.GetValue("_announcementLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection));
			_alwaysFetchAnnouncementLanguageCollection = info.GetBoolean("_alwaysFetchAnnouncementLanguageCollection");
			_alreadyFetchedAnnouncementLanguageCollection = info.GetBoolean("_alreadyFetchedAnnouncementLanguageCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");

			_deliverypointgroupAnnouncementCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection)info.GetValue("_deliverypointgroupAnnouncementCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection));
			_alwaysFetchDeliverypointgroupAnnouncementCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupAnnouncementCollection");
			_alreadyFetchedDeliverypointgroupAnnouncementCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupAnnouncementCollection");
			_companyCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup");

			_menuCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchMenuCollectionViaDeliverypointgroup");
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedMenuCollectionViaDeliverypointgroup");

			_posdeliverypointgroupCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup");

			_routeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup");

			_routeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup_");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup_");

			_terminalCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchTerminalCollectionViaDeliverypointgroup");
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedTerminalCollectionViaDeliverypointgroup");

			_uIModeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup");

			_uIModeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup_");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_");

			_uIModeCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup__");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__");
			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_categoryEntity_ = (CategoryEntity)info.GetValue("_categoryEntity_", typeof(CategoryEntity));
			if(_categoryEntity_!=null)
			{
				_categoryEntity_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntity_ReturnsNewIfNotFound = info.GetBoolean("_categoryEntity_ReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity_ = info.GetBoolean("_alwaysFetchCategoryEntity_");
			_alreadyFetchedCategoryEntity_ = info.GetBoolean("_alreadyFetchedCategoryEntity_");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_entertainmentcategoryEntity = (EntertainmentcategoryEntity)info.GetValue("_entertainmentcategoryEntity", typeof(EntertainmentcategoryEntity));
			if(_entertainmentcategoryEntity!=null)
			{
				_entertainmentcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentcategoryEntity = info.GetBoolean("_alwaysFetchEntertainmentcategoryEntity");
			_alreadyFetchedEntertainmentcategoryEntity = info.GetBoolean("_alreadyFetchedEntertainmentcategoryEntity");

			_entertainmentcategoryEntity_ = (EntertainmentcategoryEntity)info.GetValue("_entertainmentcategoryEntity_", typeof(EntertainmentcategoryEntity));
			if(_entertainmentcategoryEntity_!=null)
			{
				_entertainmentcategoryEntity_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentcategoryEntity_ReturnsNewIfNotFound = info.GetBoolean("_entertainmentcategoryEntity_ReturnsNewIfNotFound");
			_alwaysFetchEntertainmentcategoryEntity_ = info.GetBoolean("_alwaysFetchEntertainmentcategoryEntity_");
			_alreadyFetchedEntertainmentcategoryEntity_ = info.GetBoolean("_alreadyFetchedEntertainmentcategoryEntity_");

			_mediaEntity = (MediaEntity)info.GetValue("_mediaEntity", typeof(MediaEntity));
			if(_mediaEntity!=null)
			{
				_mediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaEntity = info.GetBoolean("_alwaysFetchMediaEntity");
			_alreadyFetchedMediaEntity = info.GetBoolean("_alreadyFetchedMediaEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AnnouncementFieldIndex)fieldIndex)
			{
				case AnnouncementFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case AnnouncementFieldIndex.OnYesCategory:
					DesetupSyncCategoryEntity_(true, false);
					_alreadyFetchedCategoryEntity_ = false;
					break;
				case AnnouncementFieldIndex.OnNoCategory:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case AnnouncementFieldIndex.OnYesEntertainmentCategory:
					DesetupSyncEntertainmentcategoryEntity(true, false);
					_alreadyFetchedEntertainmentcategoryEntity = false;
					break;
				case AnnouncementFieldIndex.OnNoEntertainmentCategory:
					DesetupSyncEntertainmentcategoryEntity_(true, false);
					_alreadyFetchedEntertainmentcategoryEntity_ = false;
					break;
				case AnnouncementFieldIndex.OnYesEntertainment:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case AnnouncementFieldIndex.MediaId:
					DesetupSyncMediaEntity(true, false);
					_alreadyFetchedMediaEntity = false;
					break;
				case AnnouncementFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case AnnouncementFieldIndex.OnYesProduct:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAnnouncementLanguageCollection = (_announcementLanguageCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupAnnouncementCollection = (_deliverypointgroupAnnouncementCollection.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = (_companyCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = (_menuCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = (_posdeliverypointgroupCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = (_routeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = (_routeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = (_terminalCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = (_uIModeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = (_uIModeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = (_uIModeCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedCategoryEntity_ = (_categoryEntity_ != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedEntertainmentcategoryEntity = (_entertainmentcategoryEntity != null);
			_alreadyFetchedEntertainmentcategoryEntity_ = (_entertainmentcategoryEntity_ != null);
			_alreadyFetchedMediaEntity = (_mediaEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingOnNoCategory);
					break;
				case "CategoryEntity_":
					toReturn.Add(Relations.CategoryEntityUsingOnYesCategory);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingOnYesEntertainment);
					break;
				case "EntertainmentcategoryEntity":
					toReturn.Add(Relations.EntertainmentcategoryEntityUsingOnYesEntertainmentCategory);
					break;
				case "EntertainmentcategoryEntity_":
					toReturn.Add(Relations.EntertainmentcategoryEntityUsingOnNoEntertainmentCategory);
					break;
				case "MediaEntity":
					toReturn.Add(Relations.MediaEntityUsingMediaId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingOnYesProduct);
					break;
				case "AnnouncementLanguageCollection":
					toReturn.Add(Relations.AnnouncementLanguageEntityUsingAnnouncementId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingAnnouncementId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId);
					break;
				case "DeliverypointgroupAnnouncementCollection":
					toReturn.Add(Relations.DeliverypointgroupAnnouncementEntityUsingAnnouncementId);
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId, "AnnouncementEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_announcementLanguageCollection", (!this.MarkedForDeletion?_announcementLanguageCollection:null));
			info.AddValue("_alwaysFetchAnnouncementLanguageCollection", _alwaysFetchAnnouncementLanguageCollection);
			info.AddValue("_alreadyFetchedAnnouncementLanguageCollection", _alreadyFetchedAnnouncementLanguageCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_deliverypointgroupAnnouncementCollection", (!this.MarkedForDeletion?_deliverypointgroupAnnouncementCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupAnnouncementCollection", _alwaysFetchDeliverypointgroupAnnouncementCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupAnnouncementCollection", _alreadyFetchedDeliverypointgroupAnnouncementCollection);
			info.AddValue("_companyCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup", _alwaysFetchCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup", _alreadyFetchedCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_menuCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_menuCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchMenuCollectionViaDeliverypointgroup", _alwaysFetchMenuCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedMenuCollectionViaDeliverypointgroup", _alreadyFetchedMenuCollectionViaDeliverypointgroup);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_routeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup", _alwaysFetchRouteCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup", _alreadyFetchedRouteCollectionViaDeliverypointgroup);
			info.AddValue("_routeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup_", _alwaysFetchRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup_", _alreadyFetchedRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_terminalCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_terminalCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaDeliverypointgroup", _alwaysFetchTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedTerminalCollectionViaDeliverypointgroup", _alreadyFetchedTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup", _alwaysFetchUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup", _alreadyFetchedUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup_", _alwaysFetchUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_", _alreadyFetchedUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup__", _alwaysFetchUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__", _alreadyFetchedUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_categoryEntity_", (!this.MarkedForDeletion?_categoryEntity_:null));
			info.AddValue("_categoryEntity_ReturnsNewIfNotFound", _categoryEntity_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity_", _alwaysFetchCategoryEntity_);
			info.AddValue("_alreadyFetchedCategoryEntity_", _alreadyFetchedCategoryEntity_);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_entertainmentcategoryEntity", (!this.MarkedForDeletion?_entertainmentcategoryEntity:null));
			info.AddValue("_entertainmentcategoryEntityReturnsNewIfNotFound", _entertainmentcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentcategoryEntity", _alwaysFetchEntertainmentcategoryEntity);
			info.AddValue("_alreadyFetchedEntertainmentcategoryEntity", _alreadyFetchedEntertainmentcategoryEntity);
			info.AddValue("_entertainmentcategoryEntity_", (!this.MarkedForDeletion?_entertainmentcategoryEntity_:null));
			info.AddValue("_entertainmentcategoryEntity_ReturnsNewIfNotFound", _entertainmentcategoryEntity_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentcategoryEntity_", _alwaysFetchEntertainmentcategoryEntity_);
			info.AddValue("_alreadyFetchedEntertainmentcategoryEntity_", _alreadyFetchedEntertainmentcategoryEntity_);
			info.AddValue("_mediaEntity", (!this.MarkedForDeletion?_mediaEntity:null));
			info.AddValue("_mediaEntityReturnsNewIfNotFound", _mediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaEntity", _alwaysFetchMediaEntity);
			info.AddValue("_alreadyFetchedMediaEntity", _alreadyFetchedMediaEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "CategoryEntity_":
					_alreadyFetchedCategoryEntity_ = true;
					this.CategoryEntity_ = (CategoryEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "EntertainmentcategoryEntity":
					_alreadyFetchedEntertainmentcategoryEntity = true;
					this.EntertainmentcategoryEntity = (EntertainmentcategoryEntity)entity;
					break;
				case "EntertainmentcategoryEntity_":
					_alreadyFetchedEntertainmentcategoryEntity_ = true;
					this.EntertainmentcategoryEntity_ = (EntertainmentcategoryEntity)entity;
					break;
				case "MediaEntity":
					_alreadyFetchedMediaEntity = true;
					this.MediaEntity = (MediaEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "AnnouncementLanguageCollection":
					_alreadyFetchedAnnouncementLanguageCollection = true;
					if(entity!=null)
					{
						this.AnnouncementLanguageCollection.Add((AnnouncementLanguageEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupAnnouncementCollection":
					_alreadyFetchedDeliverypointgroupAnnouncementCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupAnnouncementCollection.Add((DeliverypointgroupAnnouncementEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup.Add((CompanyEntity)entity);
					}
					break;
				case "MenuCollectionViaDeliverypointgroup":
					_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.MenuCollectionViaDeliverypointgroup.Add((MenuEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup_.Add((RouteEntity)entity);
					}
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaDeliverypointgroup.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup_.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup__.Add((UIModeEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "CategoryEntity_":
					SetupSyncCategoryEntity_(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "EntertainmentcategoryEntity":
					SetupSyncEntertainmentcategoryEntity(relatedEntity);
					break;
				case "EntertainmentcategoryEntity_":
					SetupSyncEntertainmentcategoryEntity_(relatedEntity);
					break;
				case "MediaEntity":
					SetupSyncMediaEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "AnnouncementLanguageCollection":
					_announcementLanguageCollection.Add((AnnouncementLanguageEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "DeliverypointgroupAnnouncementCollection":
					_deliverypointgroupAnnouncementCollection.Add((DeliverypointgroupAnnouncementEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "CategoryEntity_":
					DesetupSyncCategoryEntity_(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "EntertainmentcategoryEntity":
					DesetupSyncEntertainmentcategoryEntity(false, true);
					break;
				case "EntertainmentcategoryEntity_":
					DesetupSyncEntertainmentcategoryEntity_(false, true);
					break;
				case "MediaEntity":
					DesetupSyncMediaEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "AnnouncementLanguageCollection":
					this.PerformRelatedEntityRemoval(_announcementLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupAnnouncementCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupAnnouncementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_categoryEntity_!=null)
			{
				toReturn.Add(_categoryEntity_);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_entertainmentcategoryEntity!=null)
			{
				toReturn.Add(_entertainmentcategoryEntity);
			}
			if(_entertainmentcategoryEntity_!=null)
			{
				toReturn.Add(_entertainmentcategoryEntity_);
			}
			if(_mediaEntity!=null)
			{
				toReturn.Add(_mediaEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_announcementLanguageCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_deliverypointgroupCollection);
			toReturn.Add(_deliverypointgroupAnnouncementCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 announcementId)
		{
			return FetchUsingPK(announcementId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 announcementId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(announcementId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 announcementId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(announcementId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 announcementId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(announcementId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AnnouncementId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AnnouncementRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch)
		{
			return GetMultiAnnouncementLanguageCollection(forceFetch, _announcementLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementLanguageCollection(forceFetch, _announcementLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementLanguageCollection || forceFetch || _alwaysFetchAnnouncementLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementLanguageCollection);
				_announcementLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_announcementLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_announcementLanguageCollection.GetMultiManyToOne(this, null, filter);
				_announcementLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementLanguageCollection = true;
			}
			return _announcementLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementLanguageCollection'. These settings will be taken into account
		/// when the property AnnouncementLanguageCollection is requested or GetMultiAnnouncementLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementLanguageCollection.SortClauses=sortClauses;
			_announcementLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupAnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupAnnouncementCollection(forceFetch, _deliverypointgroupAnnouncementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupAnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupAnnouncementCollection(forceFetch, _deliverypointgroupAnnouncementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupAnnouncementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupAnnouncementCollection || forceFetch || _alwaysFetchDeliverypointgroupAnnouncementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupAnnouncementCollection);
				_deliverypointgroupAnnouncementCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupAnnouncementCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupAnnouncementCollection.GetMultiManyToOne(this, null, filter);
				_deliverypointgroupAnnouncementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupAnnouncementCollection = true;
			}
			return _deliverypointgroupAnnouncementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupAnnouncementCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupAnnouncementCollection is requested or GetMultiDeliverypointgroupAnnouncementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupAnnouncementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupAnnouncementCollection.SortClauses=sortClauses;
			_deliverypointgroupAnnouncementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup(forceFetch, _companyCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
			}
			return _companyCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup is requested or GetMultiCompanyCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiMenuCollectionViaDeliverypointgroup(forceFetch, _menuCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchMenuCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("MenuCollectionViaDeliverypointgroup"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
			}
			return _menuCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property MenuCollectionViaDeliverypointgroup is requested or GetMultiMenuCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_menuCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup(forceFetch, _routeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
			}
			return _routeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup is requested or GetMultiRouteCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup_(forceFetch, _routeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
			}
			return _routeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup_ is requested or GetMultiRouteCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaDeliverypointgroup(forceFetch, _terminalCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchTerminalCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
			}
			return _terminalCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property TerminalCollectionViaDeliverypointgroup is requested or GetMultiTerminalCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_terminalCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup(forceFetch, _uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
			}
			return _uIModeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup is requested or GetMultiUIModeCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup_(forceFetch, _uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup_ is requested or GetMultiUIModeCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup__(forceFetch, _uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AnnouncementFields.AnnouncementId, ComparisonOperator.Equal, this.AnnouncementId, "AnnouncementEntity__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup__ is requested or GetMultiUIModeCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingOnNoCategory);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OnNoCategory.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity_()
		{
			return GetSingleCategoryEntity_(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity_(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity_ || forceFetch || _alwaysFetchCategoryEntity_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingOnYesCategory);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OnYesCategory.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntity_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity_ = newEntity;
				_alreadyFetchedCategoryEntity_ = fetchResult;
			}
			return _categoryEntity_;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingOnYesEntertainment);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OnYesEntertainment.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity()
		{
			return GetSingleEntertainmentcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public virtual EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentcategoryEntity || forceFetch || _alwaysFetchEntertainmentcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentcategoryEntityUsingOnYesEntertainmentCategory);
				EntertainmentcategoryEntity newEntity = new EntertainmentcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OnYesEntertainmentCategory.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentcategoryEntity = newEntity;
				_alreadyFetchedEntertainmentcategoryEntity = fetchResult;
			}
			return _entertainmentcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity_()
		{
			return GetSingleEntertainmentcategoryEntity_(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public virtual EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity_(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentcategoryEntity_ || forceFetch || _alwaysFetchEntertainmentcategoryEntity_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentcategoryEntityUsingOnNoEntertainmentCategory);
				EntertainmentcategoryEntity newEntity = new EntertainmentcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OnNoEntertainmentCategory.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentcategoryEntity_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentcategoryEntity_ = newEntity;
				_alreadyFetchedEntertainmentcategoryEntity_ = fetchResult;
			}
			return _entertainmentcategoryEntity_;
		}


		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public MediaEntity GetSingleMediaEntity()
		{
			return GetSingleMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public virtual MediaEntity GetSingleMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaEntity || forceFetch || _alwaysFetchMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaEntityUsingMediaId);
				MediaEntity newEntity = new MediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MediaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaEntity = newEntity;
				_alreadyFetchedMediaEntity = fetchResult;
			}
			return _mediaEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingOnYesProduct);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OnYesProduct.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("CategoryEntity_", _categoryEntity_);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("EntertainmentcategoryEntity", _entertainmentcategoryEntity);
			toReturn.Add("EntertainmentcategoryEntity_", _entertainmentcategoryEntity_);
			toReturn.Add("MediaEntity", _mediaEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("AnnouncementLanguageCollection", _announcementLanguageCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("DeliverypointgroupAnnouncementCollection", _deliverypointgroupAnnouncementCollection);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup", _companyCollectionViaDeliverypointgroup);
			toReturn.Add("MenuCollectionViaDeliverypointgroup", _menuCollectionViaDeliverypointgroup);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup", _posdeliverypointgroupCollectionViaDeliverypointgroup);
			toReturn.Add("RouteCollectionViaDeliverypointgroup", _routeCollectionViaDeliverypointgroup);
			toReturn.Add("RouteCollectionViaDeliverypointgroup_", _routeCollectionViaDeliverypointgroup_);
			toReturn.Add("TerminalCollectionViaDeliverypointgroup", _terminalCollectionViaDeliverypointgroup);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup", _uIModeCollectionViaDeliverypointgroup);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup_", _uIModeCollectionViaDeliverypointgroup_);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup__", _uIModeCollectionViaDeliverypointgroup__);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="validator">The validator object for this AnnouncementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 announcementId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(announcementId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_announcementLanguageCollection = new Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection();
			_announcementLanguageCollection.SetContainingEntityInfo(this, "AnnouncementEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "AnnouncementEntity");

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "AnnouncementEntity");

			_deliverypointgroupAnnouncementCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection();
			_deliverypointgroupAnnouncementCollection.SetContainingEntityInfo(this, "AnnouncementEntity");
			_companyCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_menuCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.MenuCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_routeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_terminalCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_categoryEntityReturnsNewIfNotFound = true;
			_categoryEntity_ReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_entertainmentEntityReturnsNewIfNotFound = true;
			_entertainmentcategoryEntityReturnsNewIfNotFound = true;
			_entertainmentcategoryEntity_ReturnsNewIfNotFound = true;
			_mediaEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnnouncementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Title", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DateToShow", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeToShow", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Recurring", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringPeriod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringBeginDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringBeginTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringEndDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringEndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnYes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnYesCategory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnNoCategory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnYesEntertainmentCategory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnNoEntertainmentCategory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Duration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnYesEntertainment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OnYesProduct", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.CategoryEntityUsingOnNoCategoryStatic, true, signalRelatedEntity, "AnnouncementCollection_", resetFKFields, new int[] { (int)AnnouncementFieldIndex.OnNoCategory } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.CategoryEntityUsingOnNoCategoryStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _categoryEntity_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity_, new PropertyChangedEventHandler( OnCategoryEntity_PropertyChanged ), "CategoryEntity_", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.CategoryEntityUsingOnYesCategoryStatic, true, signalRelatedEntity, "AnnouncementCollection", resetFKFields, new int[] { (int)AnnouncementFieldIndex.OnYesCategory } );		
			_categoryEntity_ = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity_(IEntityCore relatedEntity)
		{
			if(_categoryEntity_!=relatedEntity)
			{		
				DesetupSyncCategoryEntity_(true, true);
				_categoryEntity_ = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity_, new PropertyChangedEventHandler( OnCategoryEntity_PropertyChanged ), "CategoryEntity_", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.CategoryEntityUsingOnYesCategoryStatic, true, ref _alreadyFetchedCategoryEntity_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntity_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "AnnouncementCollection", resetFKFields, new int[] { (int)AnnouncementFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "AnnouncementCollection", resetFKFields, new int[] { (int)AnnouncementFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.EntertainmentEntityUsingOnYesEntertainmentStatic, true, signalRelatedEntity, "AnnouncementCollection", resetFKFields, new int[] { (int)AnnouncementFieldIndex.OnYesEntertainment } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.EntertainmentEntityUsingOnYesEntertainmentStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.EntertainmentcategoryEntityUsingOnYesEntertainmentCategoryStatic, true, signalRelatedEntity, "AnnouncementCollection", resetFKFields, new int[] { (int)AnnouncementFieldIndex.OnYesEntertainmentCategory } );		
			_entertainmentcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentcategoryEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentcategoryEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentcategoryEntity(true, true);
				_entertainmentcategoryEntity = (EntertainmentcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.EntertainmentcategoryEntityUsingOnYesEntertainmentCategoryStatic, true, ref _alreadyFetchedEntertainmentcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentcategoryEntity_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentcategoryEntity_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentcategoryEntity_, new PropertyChangedEventHandler( OnEntertainmentcategoryEntity_PropertyChanged ), "EntertainmentcategoryEntity_", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.EntertainmentcategoryEntityUsingOnNoEntertainmentCategoryStatic, true, signalRelatedEntity, "AnnouncementCollection_", resetFKFields, new int[] { (int)AnnouncementFieldIndex.OnNoEntertainmentCategory } );		
			_entertainmentcategoryEntity_ = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentcategoryEntity_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentcategoryEntity_(IEntityCore relatedEntity)
		{
			if(_entertainmentcategoryEntity_!=relatedEntity)
			{		
				DesetupSyncEntertainmentcategoryEntity_(true, true);
				_entertainmentcategoryEntity_ = (EntertainmentcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentcategoryEntity_, new PropertyChangedEventHandler( OnEntertainmentcategoryEntity_PropertyChanged ), "EntertainmentcategoryEntity_", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.EntertainmentcategoryEntityUsingOnNoEntertainmentCategoryStatic, true, ref _alreadyFetchedEntertainmentcategoryEntity_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentcategoryEntity_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.MediaEntityUsingMediaIdStatic, true, signalRelatedEntity, "AnnouncementCollection", resetFKFields, new int[] { (int)AnnouncementFieldIndex.MediaId } );		
			_mediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaEntity(true, true);
				_mediaEntity = (MediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.MediaEntityUsingMediaIdStatic, true, ref _alreadyFetchedMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.ProductEntityUsingOnYesProductStatic, true, signalRelatedEntity, "AnnouncementCollection", resetFKFields, new int[] { (int)AnnouncementFieldIndex.OnYesProduct } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticAnnouncementRelations.ProductEntityUsingOnYesProductStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 announcementId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AnnouncementFieldIndex.AnnouncementId].ForcedCurrentValueWrite(announcementId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAnnouncementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AnnouncementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AnnouncementRelations Relations
		{
			get	{ return new AnnouncementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AnnouncementLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection(), (IEntityRelation)GetRelationsForField("AnnouncementLanguageCollection")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.AnnouncementLanguageEntity, 0, null, null, null, "AnnouncementLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupAnnouncement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupAnnouncementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupAnnouncementCollection")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity, 0, null, null, null, "DeliverypointgroupAnnouncementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"), "CompanyCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaDeliverypointgroup"), "MenuCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"), "PosdeliverypointgroupCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup"), "RouteCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"), "RouteCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"), "TerminalCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"), "UIModeCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"), "UIModeCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"), "UIModeCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity_
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity_")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), (IEntityRelation)GetRelationsForField("EntertainmentcategoryEntity")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, null, "EntertainmentcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryEntity_
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), (IEntityRelation)GetRelationsForField("EntertainmentcategoryEntity_")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, null, "EntertainmentcategoryEntity_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaEntity")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.AnnouncementEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AnnouncementId property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."AnnouncementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AnnouncementId
		{
			get { return (System.Int32)GetValue((int)AnnouncementFieldIndex.AnnouncementId, true); }
			set	{ SetValue((int)AnnouncementFieldIndex.AnnouncementId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.CompanyId, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Title property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."Title"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Title
		{
			get { return (System.String)GetValue((int)AnnouncementFieldIndex.Title, true); }
			set	{ SetValue((int)AnnouncementFieldIndex.Title, value, true); }
		}

		/// <summary> The Text property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."Text"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)AnnouncementFieldIndex.Text, true); }
			set	{ SetValue((int)AnnouncementFieldIndex.Text, value, true); }
		}

		/// <summary> The DateToShow property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."DateToShow"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DateToShow
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.DateToShow, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.DateToShow, value, true); }
		}

		/// <summary> The TimeToShow property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."TimeToShow"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TimeToShow
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.TimeToShow, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.TimeToShow, value, true); }
		}

		/// <summary> The Recurring property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."Recurring"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Recurring
		{
			get { return (Nullable<System.Boolean>)GetValue((int)AnnouncementFieldIndex.Recurring, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.Recurring, value, true); }
		}

		/// <summary> The RecurringPeriod property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."RecurringPeriod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Obymobi.Enums.AnnouncementRecurringPeriod> RecurringPeriod
		{
			get { return (Nullable<Obymobi.Enums.AnnouncementRecurringPeriod>)GetValue((int)AnnouncementFieldIndex.RecurringPeriod, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.RecurringPeriod, value, true); }
		}

		/// <summary> The RecurringBeginDate property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."RecurringBeginDate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurringBeginDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.RecurringBeginDate, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.RecurringBeginDate, value, true); }
		}

		/// <summary> The RecurringBeginTime property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."RecurringBeginTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurringBeginTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.RecurringBeginTime, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.RecurringBeginTime, value, true); }
		}

		/// <summary> The RecurringEndDate property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."RecurringEndDate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurringEndDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.RecurringEndDate, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.RecurringEndDate, value, true); }
		}

		/// <summary> The RecurringEndTime property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."RecurringEndTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RecurringEndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.RecurringEndTime, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.RecurringEndTime, value, true); }
		}

		/// <summary> The RecurringAmount property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."RecurringAmount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RecurringAmount
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.RecurringAmount, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.RecurringAmount, value, true); }
		}

		/// <summary> The RecurringMinutes property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."RecurringMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RecurringMinutes
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.RecurringMinutes, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.RecurringMinutes, value, true); }
		}

		/// <summary> The DialogType property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."DialogType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DialogType
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.DialogType, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.DialogType, value, true); }
		}

		/// <summary> The OnYes property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnYes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnYes
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnYes, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnYes, value, true); }
		}

		/// <summary> The OnNo property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnNo"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnNo
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnNo, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnNo, value, true); }
		}

		/// <summary> The OnYesCategory property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnYesCategory"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnYesCategory
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnYesCategory, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnYesCategory, value, true); }
		}

		/// <summary> The OnNoCategory property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnNoCategory"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnNoCategory
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnNoCategory, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnNoCategory, value, true); }
		}

		/// <summary> The OnYesEntertainmentCategory property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnYesEntertainmentCategory"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnYesEntertainmentCategory
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnYesEntertainmentCategory, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnYesEntertainmentCategory, value, true); }
		}

		/// <summary> The OnNoEntertainmentCategory property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnNoEntertainmentCategory"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnNoEntertainmentCategory
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnNoEntertainmentCategory, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnNoEntertainmentCategory, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)AnnouncementFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)AnnouncementFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)AnnouncementFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)AnnouncementFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Duration property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."Duration"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Duration
		{
			get { return (System.Int32)GetValue((int)AnnouncementFieldIndex.Duration, true); }
			set	{ SetValue((int)AnnouncementFieldIndex.Duration, value, true); }
		}

		/// <summary> The OnYesEntertainment property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnYesEntertainment"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnYesEntertainment
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnYesEntertainment, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnYesEntertainment, value, true); }
		}

		/// <summary> The MediaId property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.MediaId, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.MediaId, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The OnYesProduct property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."OnYesProduct"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OnYesProduct
		{
			get { return (Nullable<System.Int32>)GetValue((int)AnnouncementFieldIndex.OnYesProduct, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.OnYesProduct, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Announcement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Announcement"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AnnouncementFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AnnouncementFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection AnnouncementLanguageCollection
		{
			get	{ return GetMultiAnnouncementLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementLanguageCollection. When set to true, AnnouncementLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementLanguageCollection
		{
			get	{ return _alwaysFetchAnnouncementLanguageCollection; }
			set	{ _alwaysFetchAnnouncementLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementLanguageCollection already has been fetched. Setting this property to false when AnnouncementLanguageCollection has been fetched
		/// will clear the AnnouncementLanguageCollection collection well. Setting this property to true while AnnouncementLanguageCollection hasn't been fetched disables lazy loading for AnnouncementLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementLanguageCollection
		{
			get { return _alreadyFetchedAnnouncementLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAnnouncementLanguageCollection && !value && (_announcementLanguageCollection != null))
				{
					_announcementLanguageCollection.Clear();
				}
				_alreadyFetchedAnnouncementLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupAnnouncementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection DeliverypointgroupAnnouncementCollection
		{
			get	{ return GetMultiDeliverypointgroupAnnouncementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupAnnouncementCollection. When set to true, DeliverypointgroupAnnouncementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupAnnouncementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupAnnouncementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupAnnouncementCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupAnnouncementCollection; }
			set	{ _alwaysFetchDeliverypointgroupAnnouncementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupAnnouncementCollection already has been fetched. Setting this property to false when DeliverypointgroupAnnouncementCollection has been fetched
		/// will clear the DeliverypointgroupAnnouncementCollection collection well. Setting this property to true while DeliverypointgroupAnnouncementCollection hasn't been fetched disables lazy loading for DeliverypointgroupAnnouncementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupAnnouncementCollection
		{
			get { return _alreadyFetchedDeliverypointgroupAnnouncementCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupAnnouncementCollection && !value && (_deliverypointgroupAnnouncementCollection != null))
				{
					_deliverypointgroupAnnouncementCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupAnnouncementCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup. When set to true, CompanyCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup && !value && (_companyCollectionViaDeliverypointgroup != null))
				{
					_companyCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaDeliverypointgroup
		{
			get { return GetMultiMenuCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaDeliverypointgroup. When set to true, MenuCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchMenuCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchMenuCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when MenuCollectionViaDeliverypointgroup has been fetched
		/// will clear the MenuCollectionViaDeliverypointgroup collection well. Setting this property to true while MenuCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for MenuCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedMenuCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaDeliverypointgroup && !value && (_menuCollectionViaDeliverypointgroup != null))
				{
					_menuCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup. When set to true, RouteCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup && !value && (_routeCollectionViaDeliverypointgroup != null))
				{
					_routeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup_
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup_. When set to true, RouteCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup_ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup_ && !value && (_routeCollectionViaDeliverypointgroup_ != null))
				{
					_routeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaDeliverypointgroup
		{
			get { return GetMultiTerminalCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaDeliverypointgroup. When set to true, TerminalCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchTerminalCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchTerminalCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when TerminalCollectionViaDeliverypointgroup has been fetched
		/// will clear the TerminalCollectionViaDeliverypointgroup collection well. Setting this property to true while TerminalCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for TerminalCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedTerminalCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaDeliverypointgroup && !value && (_terminalCollectionViaDeliverypointgroup != null))
				{
					_terminalCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup. When set to true, UIModeCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup && !value && (_uIModeCollectionViaDeliverypointgroup != null))
				{
					_uIModeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup_
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup_. When set to true, UIModeCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup_ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ && !value && (_uIModeCollectionViaDeliverypointgroup_ != null))
				{
					_uIModeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup__
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup__. When set to true, UIModeCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup__ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ && !value && (_uIModeCollectionViaDeliverypointgroup__ != null))
				{
					_uIModeCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection_", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity_
		{
			get	{ return GetSingleCategoryEntity_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection", "CategoryEntity_", _categoryEntity_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity_. When set to true, CategoryEntity_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity_ is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity_
		{
			get	{ return _alwaysFetchCategoryEntity_; }
			set	{ _alwaysFetchCategoryEntity_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity_ already has been fetched. Setting this property to false when CategoryEntity_ has been fetched
		/// will set CategoryEntity_ to null as well. Setting this property to true while CategoryEntity_ hasn't been fetched disables lazy loading for CategoryEntity_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity_
		{
			get { return _alreadyFetchedCategoryEntity_;}
			set 
			{
				if(_alreadyFetchedCategoryEntity_ && !value)
				{
					this.CategoryEntity_ = null;
				}
				_alreadyFetchedCategoryEntity_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity_ is not found
		/// in the database. When set to true, CategoryEntity_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntity_ReturnsNewIfNotFound
		{
			get	{ return _categoryEntity_ReturnsNewIfNotFound; }
			set { _categoryEntity_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentcategoryEntity EntertainmentcategoryEntity
		{
			get	{ return GetSingleEntertainmentcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection", "EntertainmentcategoryEntity", _entertainmentcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryEntity. When set to true, EntertainmentcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryEntity
		{
			get	{ return _alwaysFetchEntertainmentcategoryEntity; }
			set	{ _alwaysFetchEntertainmentcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryEntity already has been fetched. Setting this property to false when EntertainmentcategoryEntity has been fetched
		/// will set EntertainmentcategoryEntity to null as well. Setting this property to true while EntertainmentcategoryEntity hasn't been fetched disables lazy loading for EntertainmentcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryEntity
		{
			get { return _alreadyFetchedEntertainmentcategoryEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryEntity && !value)
				{
					this.EntertainmentcategoryEntity = null;
				}
				_alreadyFetchedEntertainmentcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentcategoryEntity is not found
		/// in the database. When set to true, EntertainmentcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentcategoryEntityReturnsNewIfNotFound; }
			set { _entertainmentcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentcategoryEntity_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentcategoryEntity EntertainmentcategoryEntity_
		{
			get	{ return GetSingleEntertainmentcategoryEntity_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentcategoryEntity_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection_", "EntertainmentcategoryEntity_", _entertainmentcategoryEntity_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryEntity_. When set to true, EntertainmentcategoryEntity_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryEntity_ is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentcategoryEntity_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryEntity_
		{
			get	{ return _alwaysFetchEntertainmentcategoryEntity_; }
			set	{ _alwaysFetchEntertainmentcategoryEntity_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryEntity_ already has been fetched. Setting this property to false when EntertainmentcategoryEntity_ has been fetched
		/// will set EntertainmentcategoryEntity_ to null as well. Setting this property to true while EntertainmentcategoryEntity_ hasn't been fetched disables lazy loading for EntertainmentcategoryEntity_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryEntity_
		{
			get { return _alreadyFetchedEntertainmentcategoryEntity_;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryEntity_ && !value)
				{
					this.EntertainmentcategoryEntity_ = null;
				}
				_alreadyFetchedEntertainmentcategoryEntity_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentcategoryEntity_ is not found
		/// in the database. When set to true, EntertainmentcategoryEntity_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentcategoryEntity_ReturnsNewIfNotFound
		{
			get	{ return _entertainmentcategoryEntity_ReturnsNewIfNotFound; }
			set { _entertainmentcategoryEntity_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaEntity MediaEntity
		{
			get	{ return GetSingleMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection", "MediaEntity", _mediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaEntity. When set to true, MediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaEntity
		{
			get	{ return _alwaysFetchMediaEntity; }
			set	{ _alwaysFetchMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaEntity already has been fetched. Setting this property to false when MediaEntity has been fetched
		/// will set MediaEntity to null as well. Setting this property to true while MediaEntity hasn't been fetched disables lazy loading for MediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaEntity
		{
			get { return _alreadyFetchedMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaEntity && !value)
				{
					this.MediaEntity = null;
				}
				_alreadyFetchedMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaEntity is not found
		/// in the database. When set to true, MediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaEntityReturnsNewIfNotFound; }
			set { _mediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AnnouncementCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AnnouncementEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
