﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ApplicationConfiguration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ApplicationConfigurationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ApplicationConfigurationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ActionCollection	_actionCollection;
		private bool	_alwaysFetchActionCollection, _alreadyFetchedActionCollection;
		private Obymobi.Data.CollectionClasses.FeatureFlagCollection	_featureFlagCollection;
		private bool	_alwaysFetchFeatureFlagCollection, _alreadyFetchedFeatureFlagCollection;
		private Obymobi.Data.CollectionClasses.LandingPageCollection	_landingPageCollection;
		private bool	_alwaysFetchLandingPageCollection, _alreadyFetchedLandingPageCollection;
		private Obymobi.Data.CollectionClasses.NavigationMenuCollection	_navigationMenuCollection;
		private bool	_alwaysFetchNavigationMenuCollection, _alreadyFetchedNavigationMenuCollection;
		private Obymobi.Data.CollectionClasses.ThemeCollection	_themeCollection;
		private bool	_alwaysFetchThemeCollection, _alreadyFetchedThemeCollection;
		private Obymobi.Data.CollectionClasses.WidgetCollection	_widgetCollection;
		private bool	_alwaysFetchWidgetCollection, _alreadyFetchedWidgetCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private LandingPageEntity _defaultLandingPageEntity;
		private bool	_alwaysFetchDefaultLandingPageEntity, _alreadyFetchedDefaultLandingPageEntity, _defaultLandingPageEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DefaultLandingPageEntity</summary>
			public static readonly string DefaultLandingPageEntity = "DefaultLandingPageEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name ActionCollection</summary>
			public static readonly string ActionCollection = "ActionCollection";
			/// <summary>Member name FeatureFlagCollection</summary>
			public static readonly string FeatureFlagCollection = "FeatureFlagCollection";
			/// <summary>Member name LandingPageCollection</summary>
			public static readonly string LandingPageCollection = "LandingPageCollection";
			/// <summary>Member name NavigationMenuCollection</summary>
			public static readonly string NavigationMenuCollection = "NavigationMenuCollection";
			/// <summary>Member name ThemeCollection</summary>
			public static readonly string ThemeCollection = "ThemeCollection";
			/// <summary>Member name WidgetCollection</summary>
			public static readonly string WidgetCollection = "WidgetCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ApplicationConfigurationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ApplicationConfigurationEntityBase() :base("ApplicationConfigurationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		protected ApplicationConfigurationEntityBase(System.Int32 applicationConfigurationId):base("ApplicationConfigurationEntity")
		{
			InitClassFetch(applicationConfigurationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ApplicationConfigurationEntityBase(System.Int32 applicationConfigurationId, IPrefetchPath prefetchPathToUse): base("ApplicationConfigurationEntity")
		{
			InitClassFetch(applicationConfigurationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="validator">The custom validator object for this ApplicationConfigurationEntity</param>
		protected ApplicationConfigurationEntityBase(System.Int32 applicationConfigurationId, IValidator validator):base("ApplicationConfigurationEntity")
		{
			InitClassFetch(applicationConfigurationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ApplicationConfigurationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_actionCollection = (Obymobi.Data.CollectionClasses.ActionCollection)info.GetValue("_actionCollection", typeof(Obymobi.Data.CollectionClasses.ActionCollection));
			_alwaysFetchActionCollection = info.GetBoolean("_alwaysFetchActionCollection");
			_alreadyFetchedActionCollection = info.GetBoolean("_alreadyFetchedActionCollection");

			_featureFlagCollection = (Obymobi.Data.CollectionClasses.FeatureFlagCollection)info.GetValue("_featureFlagCollection", typeof(Obymobi.Data.CollectionClasses.FeatureFlagCollection));
			_alwaysFetchFeatureFlagCollection = info.GetBoolean("_alwaysFetchFeatureFlagCollection");
			_alreadyFetchedFeatureFlagCollection = info.GetBoolean("_alreadyFetchedFeatureFlagCollection");

			_landingPageCollection = (Obymobi.Data.CollectionClasses.LandingPageCollection)info.GetValue("_landingPageCollection", typeof(Obymobi.Data.CollectionClasses.LandingPageCollection));
			_alwaysFetchLandingPageCollection = info.GetBoolean("_alwaysFetchLandingPageCollection");
			_alreadyFetchedLandingPageCollection = info.GetBoolean("_alreadyFetchedLandingPageCollection");

			_navigationMenuCollection = (Obymobi.Data.CollectionClasses.NavigationMenuCollection)info.GetValue("_navigationMenuCollection", typeof(Obymobi.Data.CollectionClasses.NavigationMenuCollection));
			_alwaysFetchNavigationMenuCollection = info.GetBoolean("_alwaysFetchNavigationMenuCollection");
			_alreadyFetchedNavigationMenuCollection = info.GetBoolean("_alreadyFetchedNavigationMenuCollection");

			_themeCollection = (Obymobi.Data.CollectionClasses.ThemeCollection)info.GetValue("_themeCollection", typeof(Obymobi.Data.CollectionClasses.ThemeCollection));
			_alwaysFetchThemeCollection = info.GetBoolean("_alwaysFetchThemeCollection");
			_alreadyFetchedThemeCollection = info.GetBoolean("_alreadyFetchedThemeCollection");

			_widgetCollection = (Obymobi.Data.CollectionClasses.WidgetCollection)info.GetValue("_widgetCollection", typeof(Obymobi.Data.CollectionClasses.WidgetCollection));
			_alwaysFetchWidgetCollection = info.GetBoolean("_alwaysFetchWidgetCollection");
			_alreadyFetchedWidgetCollection = info.GetBoolean("_alreadyFetchedWidgetCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");
			_defaultLandingPageEntity = (LandingPageEntity)info.GetValue("_defaultLandingPageEntity", typeof(LandingPageEntity));
			if(_defaultLandingPageEntity!=null)
			{
				_defaultLandingPageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_defaultLandingPageEntityReturnsNewIfNotFound = info.GetBoolean("_defaultLandingPageEntityReturnsNewIfNotFound");
			_alwaysFetchDefaultLandingPageEntity = info.GetBoolean("_alwaysFetchDefaultLandingPageEntity");
			_alreadyFetchedDefaultLandingPageEntity = info.GetBoolean("_alreadyFetchedDefaultLandingPageEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ApplicationConfigurationFieldIndex)fieldIndex)
			{
				case ApplicationConfigurationFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case ApplicationConfigurationFieldIndex.LandingPageId:
					DesetupSyncDefaultLandingPageEntity(true, false);
					_alreadyFetchedDefaultLandingPageEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedActionCollection = (_actionCollection.Count > 0);
			_alreadyFetchedFeatureFlagCollection = (_featureFlagCollection.Count > 0);
			_alreadyFetchedLandingPageCollection = (_landingPageCollection.Count > 0);
			_alreadyFetchedNavigationMenuCollection = (_navigationMenuCollection.Count > 0);
			_alreadyFetchedThemeCollection = (_themeCollection.Count > 0);
			_alreadyFetchedWidgetCollection = (_widgetCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedDefaultLandingPageEntity = (_defaultLandingPageEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DefaultLandingPageEntity":
					toReturn.Add(Relations.LandingPageEntityUsingLandingPageId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "ActionCollection":
					toReturn.Add(Relations.ActionEntityUsingApplicationConfigurationId);
					break;
				case "FeatureFlagCollection":
					toReturn.Add(Relations.FeatureFlagEntityUsingApplicationConfigurationId);
					break;
				case "LandingPageCollection":
					toReturn.Add(Relations.LandingPageEntityUsingApplicationConfigurationId);
					break;
				case "NavigationMenuCollection":
					toReturn.Add(Relations.NavigationMenuEntityUsingApplicationConfigurationId);
					break;
				case "ThemeCollection":
					toReturn.Add(Relations.ThemeEntityUsingApplicationConfigurationId);
					break;
				case "WidgetCollection":
					toReturn.Add(Relations.WidgetEntityUsingApplicationConfigurationId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingApplicationConfigurationId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingApplicationConfigurationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_actionCollection", (!this.MarkedForDeletion?_actionCollection:null));
			info.AddValue("_alwaysFetchActionCollection", _alwaysFetchActionCollection);
			info.AddValue("_alreadyFetchedActionCollection", _alreadyFetchedActionCollection);
			info.AddValue("_featureFlagCollection", (!this.MarkedForDeletion?_featureFlagCollection:null));
			info.AddValue("_alwaysFetchFeatureFlagCollection", _alwaysFetchFeatureFlagCollection);
			info.AddValue("_alreadyFetchedFeatureFlagCollection", _alreadyFetchedFeatureFlagCollection);
			info.AddValue("_landingPageCollection", (!this.MarkedForDeletion?_landingPageCollection:null));
			info.AddValue("_alwaysFetchLandingPageCollection", _alwaysFetchLandingPageCollection);
			info.AddValue("_alreadyFetchedLandingPageCollection", _alreadyFetchedLandingPageCollection);
			info.AddValue("_navigationMenuCollection", (!this.MarkedForDeletion?_navigationMenuCollection:null));
			info.AddValue("_alwaysFetchNavigationMenuCollection", _alwaysFetchNavigationMenuCollection);
			info.AddValue("_alreadyFetchedNavigationMenuCollection", _alreadyFetchedNavigationMenuCollection);
			info.AddValue("_themeCollection", (!this.MarkedForDeletion?_themeCollection:null));
			info.AddValue("_alwaysFetchThemeCollection", _alwaysFetchThemeCollection);
			info.AddValue("_alreadyFetchedThemeCollection", _alreadyFetchedThemeCollection);
			info.AddValue("_widgetCollection", (!this.MarkedForDeletion?_widgetCollection:null));
			info.AddValue("_alwaysFetchWidgetCollection", _alwaysFetchWidgetCollection);
			info.AddValue("_alreadyFetchedWidgetCollection", _alreadyFetchedWidgetCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_defaultLandingPageEntity", (!this.MarkedForDeletion?_defaultLandingPageEntity:null));
			info.AddValue("_defaultLandingPageEntityReturnsNewIfNotFound", _defaultLandingPageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDefaultLandingPageEntity", _alwaysFetchDefaultLandingPageEntity);
			info.AddValue("_alreadyFetchedDefaultLandingPageEntity", _alreadyFetchedDefaultLandingPageEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DefaultLandingPageEntity":
					_alreadyFetchedDefaultLandingPageEntity = true;
					this.DefaultLandingPageEntity = (LandingPageEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "ActionCollection":
					_alreadyFetchedActionCollection = true;
					if(entity!=null)
					{
						this.ActionCollection.Add((ActionEntity)entity);
					}
					break;
				case "FeatureFlagCollection":
					_alreadyFetchedFeatureFlagCollection = true;
					if(entity!=null)
					{
						this.FeatureFlagCollection.Add((FeatureFlagEntity)entity);
					}
					break;
				case "LandingPageCollection":
					_alreadyFetchedLandingPageCollection = true;
					if(entity!=null)
					{
						this.LandingPageCollection.Add((LandingPageEntity)entity);
					}
					break;
				case "NavigationMenuCollection":
					_alreadyFetchedNavigationMenuCollection = true;
					if(entity!=null)
					{
						this.NavigationMenuCollection.Add((NavigationMenuEntity)entity);
					}
					break;
				case "ThemeCollection":
					_alreadyFetchedThemeCollection = true;
					if(entity!=null)
					{
						this.ThemeCollection.Add((ThemeEntity)entity);
					}
					break;
				case "WidgetCollection":
					_alreadyFetchedWidgetCollection = true;
					if(entity!=null)
					{
						this.WidgetCollection.Add((WidgetEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DefaultLandingPageEntity":
					SetupSyncDefaultLandingPageEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "ActionCollection":
					_actionCollection.Add((ActionEntity)relatedEntity);
					break;
				case "FeatureFlagCollection":
					_featureFlagCollection.Add((FeatureFlagEntity)relatedEntity);
					break;
				case "LandingPageCollection":
					_landingPageCollection.Add((LandingPageEntity)relatedEntity);
					break;
				case "NavigationMenuCollection":
					_navigationMenuCollection.Add((NavigationMenuEntity)relatedEntity);
					break;
				case "ThemeCollection":
					_themeCollection.Add((ThemeEntity)relatedEntity);
					break;
				case "WidgetCollection":
					_widgetCollection.Add((WidgetEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DefaultLandingPageEntity":
					DesetupSyncDefaultLandingPageEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "ActionCollection":
					this.PerformRelatedEntityRemoval(_actionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FeatureFlagCollection":
					this.PerformRelatedEntityRemoval(_featureFlagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "LandingPageCollection":
					this.PerformRelatedEntityRemoval(_landingPageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NavigationMenuCollection":
					this.PerformRelatedEntityRemoval(_navigationMenuCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ThemeCollection":
					this.PerformRelatedEntityRemoval(_themeCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WidgetCollection":
					this.PerformRelatedEntityRemoval(_widgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_defaultLandingPageEntity!=null)
			{
				toReturn.Add(_defaultLandingPageEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_actionCollection);
			toReturn.Add(_featureFlagCollection);
			toReturn.Add(_landingPageCollection);
			toReturn.Add(_navigationMenuCollection);
			toReturn.Add(_themeCollection);
			toReturn.Add(_widgetCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationConfigurationId)
		{
			return FetchUsingPK(applicationConfigurationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(applicationConfigurationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(applicationConfigurationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 applicationConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(applicationConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ApplicationConfigurationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ApplicationConfigurationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ActionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch)
		{
			return GetMultiActionCollection(forceFetch, _actionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ActionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiActionCollection(forceFetch, _actionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiActionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedActionCollection || forceFetch || _alwaysFetchActionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_actionCollection);
				_actionCollection.SuppressClearInGetMulti=!forceFetch;
				_actionCollection.EntityFactoryToUse = entityFactoryToUse;
				_actionCollection.GetMultiManyToOne(this, null, null, null, null, filter);
				_actionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedActionCollection = true;
			}
			return _actionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ActionCollection'. These settings will be taken into account
		/// when the property ActionCollection is requested or GetMultiActionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersActionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_actionCollection.SortClauses=sortClauses;
			_actionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FeatureFlagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FeatureFlagEntity'</returns>
		public Obymobi.Data.CollectionClasses.FeatureFlagCollection GetMultiFeatureFlagCollection(bool forceFetch)
		{
			return GetMultiFeatureFlagCollection(forceFetch, _featureFlagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FeatureFlagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FeatureFlagEntity'</returns>
		public Obymobi.Data.CollectionClasses.FeatureFlagCollection GetMultiFeatureFlagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFeatureFlagCollection(forceFetch, _featureFlagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FeatureFlagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.FeatureFlagCollection GetMultiFeatureFlagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFeatureFlagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FeatureFlagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.FeatureFlagCollection GetMultiFeatureFlagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFeatureFlagCollection || forceFetch || _alwaysFetchFeatureFlagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_featureFlagCollection);
				_featureFlagCollection.SuppressClearInGetMulti=!forceFetch;
				_featureFlagCollection.EntityFactoryToUse = entityFactoryToUse;
				_featureFlagCollection.GetMultiManyToOne(this, null, filter);
				_featureFlagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedFeatureFlagCollection = true;
			}
			return _featureFlagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'FeatureFlagCollection'. These settings will be taken into account
		/// when the property FeatureFlagCollection is requested or GetMultiFeatureFlagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFeatureFlagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_featureFlagCollection.SortClauses=sortClauses;
			_featureFlagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch)
		{
			return GetMultiLandingPageCollection(forceFetch, _landingPageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLandingPageCollection(forceFetch, _landingPageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLandingPageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.LandingPageCollection GetMultiLandingPageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLandingPageCollection || forceFetch || _alwaysFetchLandingPageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_landingPageCollection);
				_landingPageCollection.SuppressClearInGetMulti=!forceFetch;
				_landingPageCollection.EntityFactoryToUse = entityFactoryToUse;
				_landingPageCollection.GetMultiManyToOne(this, null, null, null, filter);
				_landingPageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedLandingPageCollection = true;
			}
			return _landingPageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'LandingPageCollection'. These settings will be taken into account
		/// when the property LandingPageCollection is requested or GetMultiLandingPageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLandingPageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_landingPageCollection.SortClauses=sortClauses;
			_landingPageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuCollection GetMultiNavigationMenuCollection(bool forceFetch)
		{
			return GetMultiNavigationMenuCollection(forceFetch, _navigationMenuCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuCollection GetMultiNavigationMenuCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNavigationMenuCollection(forceFetch, _navigationMenuCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuCollection GetMultiNavigationMenuCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNavigationMenuCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuCollection GetMultiNavigationMenuCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNavigationMenuCollection || forceFetch || _alwaysFetchNavigationMenuCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_navigationMenuCollection);
				_navigationMenuCollection.SuppressClearInGetMulti=!forceFetch;
				_navigationMenuCollection.EntityFactoryToUse = entityFactoryToUse;
				_navigationMenuCollection.GetMultiManyToOne(this, filter);
				_navigationMenuCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedNavigationMenuCollection = true;
			}
			return _navigationMenuCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'NavigationMenuCollection'. These settings will be taken into account
		/// when the property NavigationMenuCollection is requested or GetMultiNavigationMenuCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNavigationMenuCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_navigationMenuCollection.SortClauses=sortClauses;
			_navigationMenuCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ThemeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ThemeEntity'</returns>
		public Obymobi.Data.CollectionClasses.ThemeCollection GetMultiThemeCollection(bool forceFetch)
		{
			return GetMultiThemeCollection(forceFetch, _themeCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ThemeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ThemeEntity'</returns>
		public Obymobi.Data.CollectionClasses.ThemeCollection GetMultiThemeCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiThemeCollection(forceFetch, _themeCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ThemeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ThemeCollection GetMultiThemeCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiThemeCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ThemeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ThemeCollection GetMultiThemeCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedThemeCollection || forceFetch || _alwaysFetchThemeCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_themeCollection);
				_themeCollection.SuppressClearInGetMulti=!forceFetch;
				_themeCollection.EntityFactoryToUse = entityFactoryToUse;
				_themeCollection.GetMultiManyToOne(this, filter);
				_themeCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedThemeCollection = true;
			}
			return _themeCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ThemeCollection'. These settings will be taken into account
		/// when the property ThemeCollection is requested or GetMultiThemeCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersThemeCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_themeCollection.SortClauses=sortClauses;
			_themeCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch)
		{
			return GetMultiWidgetCollection(forceFetch, _widgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWidgetCollection(forceFetch, _widgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWidgetCollection || forceFetch || _alwaysFetchWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_widgetCollection);
				_widgetCollection.SuppressClearInGetMulti=!forceFetch;
				_widgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_widgetCollection.GetMultiManyToOne(null, this, filter);
				_widgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedWidgetCollection = true;
			}
			return _widgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'WidgetCollection'. These settings will be taken into account
		/// when the property WidgetCollection is requested or GetMultiWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_widgetCollection.SortClauses=sortClauses;
			_widgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public LandingPageEntity GetSingleDefaultLandingPageEntity()
		{
			return GetSingleDefaultLandingPageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public virtual LandingPageEntity GetSingleDefaultLandingPageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDefaultLandingPageEntity || forceFetch || _alwaysFetchDefaultLandingPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LandingPageEntityUsingLandingPageId);
				LandingPageEntity newEntity = new LandingPageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LandingPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LandingPageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_defaultLandingPageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DefaultLandingPageEntity = newEntity;
				_alreadyFetchedDefaultLandingPageEntity = fetchResult;
			}
			return _defaultLandingPageEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DefaultLandingPageEntity", _defaultLandingPageEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("ActionCollection", _actionCollection);
			toReturn.Add("FeatureFlagCollection", _featureFlagCollection);
			toReturn.Add("LandingPageCollection", _landingPageCollection);
			toReturn.Add("NavigationMenuCollection", _navigationMenuCollection);
			toReturn.Add("ThemeCollection", _themeCollection);
			toReturn.Add("WidgetCollection", _widgetCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="validator">The validator object for this ApplicationConfigurationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 applicationConfigurationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(applicationConfigurationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_actionCollection = new Obymobi.Data.CollectionClasses.ActionCollection();
			_actionCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");

			_featureFlagCollection = new Obymobi.Data.CollectionClasses.FeatureFlagCollection();
			_featureFlagCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");

			_landingPageCollection = new Obymobi.Data.CollectionClasses.LandingPageCollection();
			_landingPageCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");

			_navigationMenuCollection = new Obymobi.Data.CollectionClasses.NavigationMenuCollection();
			_navigationMenuCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");

			_themeCollection = new Obymobi.Data.CollectionClasses.ThemeCollection();
			_themeCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");

			_widgetCollection = new Obymobi.Data.CollectionClasses.WidgetCollection();
			_widgetCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "ApplicationConfigurationEntity");
			_defaultLandingPageEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Updated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LandingPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnableCookiewall", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnalyticsEnvironment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GoogleAnalyticsId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GoogleTagManagerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductSwipingEnabled", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _defaultLandingPageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDefaultLandingPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _defaultLandingPageEntity, new PropertyChangedEventHandler( OnDefaultLandingPageEntityPropertyChanged ), "DefaultLandingPageEntity", Obymobi.Data.RelationClasses.StaticApplicationConfigurationRelations.LandingPageEntityUsingLandingPageIdStatic, true, signalRelatedEntity, "DefaultApplicationConfigurationCollection", resetFKFields, new int[] { (int)ApplicationConfigurationFieldIndex.LandingPageId } );		
			_defaultLandingPageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _defaultLandingPageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDefaultLandingPageEntity(IEntityCore relatedEntity)
		{
			if(_defaultLandingPageEntity!=relatedEntity)
			{		
				DesetupSyncDefaultLandingPageEntity(true, true);
				_defaultLandingPageEntity = (LandingPageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _defaultLandingPageEntity, new PropertyChangedEventHandler( OnDefaultLandingPageEntityPropertyChanged ), "DefaultLandingPageEntity", Obymobi.Data.RelationClasses.StaticApplicationConfigurationRelations.LandingPageEntityUsingLandingPageIdStatic, true, ref _alreadyFetchedDefaultLandingPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDefaultLandingPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticApplicationConfigurationRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ApplicationConfigurationCollection", resetFKFields, new int[] { (int)ApplicationConfigurationFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticApplicationConfigurationRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 applicationConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ApplicationConfigurationFieldIndex.ApplicationConfigurationId].ForcedCurrentValueWrite(applicationConfigurationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateApplicationConfigurationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ApplicationConfigurationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ApplicationConfigurationRelations Relations
		{
			get	{ return new ApplicationConfigurationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Action' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionCollection(), (IEntityRelation)GetRelationsForField("ActionCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.ActionEntity, 0, null, null, null, "ActionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FeatureFlag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFeatureFlagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.FeatureFlagCollection(), (IEntityRelation)GetRelationsForField("FeatureFlagCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.FeatureFlagEntity, 0, null, null, null, "FeatureFlagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("LandingPageCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "LandingPageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenu' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.NavigationMenuEntity, 0, null, null, null, "NavigationMenuCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Theme' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathThemeCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ThemeCollection(), (IEntityRelation)GetRelationsForField("ThemeCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.ThemeEntity, 0, null, null, null, "ThemeCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Widget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetCollection(), (IEntityRelation)GetRelationsForField("WidgetCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.WidgetEntity, 0, null, null, null, "WidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDefaultLandingPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("DefaultLandingPageEntity")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "DefaultLandingPageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ApplicationConfigurationId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ApplicationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ApplicationConfigurationId
		{
			get { return (System.Int32)GetValue((int)ApplicationConfigurationFieldIndex.ApplicationConfigurationId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.ApplicationConfigurationId, value, true); }
		}

		/// <summary> The ExternalId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 6<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ExternalId
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.ExternalId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ApplicationConfigurationFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.Name, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.Name, value, true); }
		}

		/// <summary> The ShortName property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ShortName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortName
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.ShortName, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.ShortName, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApplicationConfigurationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The Updated property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."Updated"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApplicationConfigurationFieldIndex.Updated, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.Updated, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApplicationConfigurationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ApplicationConfigurationFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The LandingPageId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."LandingPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LandingPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApplicationConfigurationFieldIndex.LandingPageId, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.LandingPageId, value, true); }
		}

		/// <summary> The Description property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1024<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.Description, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.Description, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApplicationConfigurationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CultureCode property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."CultureCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.CultureCode, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.CultureCode, value, true); }
		}

		/// <summary> The EnableCookiewall property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."EnableCookieWall"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EnableCookiewall
		{
			get { return (System.Boolean)GetValue((int)ApplicationConfigurationFieldIndex.EnableCookiewall, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.EnableCookiewall, value, true); }
		}

		/// <summary> The AnalyticsEnvironment property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."AnalyticsEnvironment"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AnalyticsEnvironment AnalyticsEnvironment
		{
			get { return (Obymobi.Enums.AnalyticsEnvironment)GetValue((int)ApplicationConfigurationFieldIndex.AnalyticsEnvironment, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.AnalyticsEnvironment, value, true); }
		}

		/// <summary> The GoogleAnalyticsId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."GoogleAnalyticsId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GoogleAnalyticsId
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.GoogleAnalyticsId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.GoogleAnalyticsId, value, true); }
		}

		/// <summary> The GoogleTagManagerId property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."GoogleTagManagerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GoogleTagManagerId
		{
			get { return (System.String)GetValue((int)ApplicationConfigurationFieldIndex.GoogleTagManagerId, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.GoogleTagManagerId, value, true); }
		}

		/// <summary> The ProductSwipingEnabled property of the Entity ApplicationConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ApplicationConfiguration"."ProductSwipingEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ProductSwipingEnabled
		{
			get { return (System.Boolean)GetValue((int)ApplicationConfigurationFieldIndex.ProductSwipingEnabled, true); }
			set	{ SetValue((int)ApplicationConfigurationFieldIndex.ProductSwipingEnabled, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiActionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ActionCollection ActionCollection
		{
			get	{ return GetMultiActionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ActionCollection. When set to true, ActionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiActionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionCollection
		{
			get	{ return _alwaysFetchActionCollection; }
			set	{ _alwaysFetchActionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionCollection already has been fetched. Setting this property to false when ActionCollection has been fetched
		/// will clear the ActionCollection collection well. Setting this property to true while ActionCollection hasn't been fetched disables lazy loading for ActionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionCollection
		{
			get { return _alreadyFetchedActionCollection;}
			set 
			{
				if(_alreadyFetchedActionCollection && !value && (_actionCollection != null))
				{
					_actionCollection.Clear();
				}
				_alreadyFetchedActionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FeatureFlagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFeatureFlagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.FeatureFlagCollection FeatureFlagCollection
		{
			get	{ return GetMultiFeatureFlagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FeatureFlagCollection. When set to true, FeatureFlagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FeatureFlagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiFeatureFlagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFeatureFlagCollection
		{
			get	{ return _alwaysFetchFeatureFlagCollection; }
			set	{ _alwaysFetchFeatureFlagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FeatureFlagCollection already has been fetched. Setting this property to false when FeatureFlagCollection has been fetched
		/// will clear the FeatureFlagCollection collection well. Setting this property to true while FeatureFlagCollection hasn't been fetched disables lazy loading for FeatureFlagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFeatureFlagCollection
		{
			get { return _alreadyFetchedFeatureFlagCollection;}
			set 
			{
				if(_alreadyFetchedFeatureFlagCollection && !value && (_featureFlagCollection != null))
				{
					_featureFlagCollection.Clear();
				}
				_alreadyFetchedFeatureFlagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LandingPageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLandingPageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LandingPageCollection LandingPageCollection
		{
			get	{ return GetMultiLandingPageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageCollection. When set to true, LandingPageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiLandingPageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageCollection
		{
			get	{ return _alwaysFetchLandingPageCollection; }
			set	{ _alwaysFetchLandingPageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageCollection already has been fetched. Setting this property to false when LandingPageCollection has been fetched
		/// will clear the LandingPageCollection collection well. Setting this property to true while LandingPageCollection hasn't been fetched disables lazy loading for LandingPageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageCollection
		{
			get { return _alreadyFetchedLandingPageCollection;}
			set 
			{
				if(_alreadyFetchedLandingPageCollection && !value && (_landingPageCollection != null))
				{
					_landingPageCollection.Clear();
				}
				_alreadyFetchedLandingPageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NavigationMenuEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNavigationMenuCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuCollection NavigationMenuCollection
		{
			get	{ return GetMultiNavigationMenuCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuCollection. When set to true, NavigationMenuCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuCollection is accessed. You can always execute/ a forced fetch by calling GetMultiNavigationMenuCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuCollection
		{
			get	{ return _alwaysFetchNavigationMenuCollection; }
			set	{ _alwaysFetchNavigationMenuCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuCollection already has been fetched. Setting this property to false when NavigationMenuCollection has been fetched
		/// will clear the NavigationMenuCollection collection well. Setting this property to true while NavigationMenuCollection hasn't been fetched disables lazy loading for NavigationMenuCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuCollection
		{
			get { return _alreadyFetchedNavigationMenuCollection;}
			set 
			{
				if(_alreadyFetchedNavigationMenuCollection && !value && (_navigationMenuCollection != null))
				{
					_navigationMenuCollection.Clear();
				}
				_alreadyFetchedNavigationMenuCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ThemeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiThemeCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ThemeCollection ThemeCollection
		{
			get	{ return GetMultiThemeCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ThemeCollection. When set to true, ThemeCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ThemeCollection is accessed. You can always execute/ a forced fetch by calling GetMultiThemeCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchThemeCollection
		{
			get	{ return _alwaysFetchThemeCollection; }
			set	{ _alwaysFetchThemeCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ThemeCollection already has been fetched. Setting this property to false when ThemeCollection has been fetched
		/// will clear the ThemeCollection collection well. Setting this property to true while ThemeCollection hasn't been fetched disables lazy loading for ThemeCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedThemeCollection
		{
			get { return _alreadyFetchedThemeCollection;}
			set 
			{
				if(_alreadyFetchedThemeCollection && !value && (_themeCollection != null))
				{
					_themeCollection.Clear();
				}
				_alreadyFetchedThemeCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.WidgetCollection WidgetCollection
		{
			get	{ return GetMultiWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetCollection. When set to true, WidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetCollection
		{
			get	{ return _alwaysFetchWidgetCollection; }
			set	{ _alwaysFetchWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetCollection already has been fetched. Setting this property to false when WidgetCollection has been fetched
		/// will clear the WidgetCollection collection well. Setting this property to true while WidgetCollection hasn't been fetched disables lazy loading for WidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetCollection
		{
			get { return _alreadyFetchedWidgetCollection;}
			set 
			{
				if(_alreadyFetchedWidgetCollection && !value && (_widgetCollection != null))
				{
					_widgetCollection.Clear();
				}
				_alreadyFetchedWidgetCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'LandingPageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDefaultLandingPageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LandingPageEntity DefaultLandingPageEntity
		{
			get	{ return GetSingleDefaultLandingPageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDefaultLandingPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DefaultApplicationConfigurationCollection", "DefaultLandingPageEntity", _defaultLandingPageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DefaultLandingPageEntity. When set to true, DefaultLandingPageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DefaultLandingPageEntity is accessed. You can always execute a forced fetch by calling GetSingleDefaultLandingPageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDefaultLandingPageEntity
		{
			get	{ return _alwaysFetchDefaultLandingPageEntity; }
			set	{ _alwaysFetchDefaultLandingPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DefaultLandingPageEntity already has been fetched. Setting this property to false when DefaultLandingPageEntity has been fetched
		/// will set DefaultLandingPageEntity to null as well. Setting this property to true while DefaultLandingPageEntity hasn't been fetched disables lazy loading for DefaultLandingPageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDefaultLandingPageEntity
		{
			get { return _alreadyFetchedDefaultLandingPageEntity;}
			set 
			{
				if(_alreadyFetchedDefaultLandingPageEntity && !value)
				{
					this.DefaultLandingPageEntity = null;
				}
				_alreadyFetchedDefaultLandingPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DefaultLandingPageEntity is not found
		/// in the database. When set to true, DefaultLandingPageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DefaultLandingPageEntityReturnsNewIfNotFound
		{
			get	{ return _defaultLandingPageEntityReturnsNewIfNotFound; }
			set { _defaultLandingPageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApplicationConfigurationCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
