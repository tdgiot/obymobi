﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'WidgetGroupWidget'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class WidgetGroupWidgetEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "WidgetGroupWidgetEntity"; }
		}
	
		#region Class Member Declarations
		private WidgetEntity _parentWidgetEntity;
		private bool	_alwaysFetchParentWidgetEntity, _alreadyFetchedParentWidgetEntity, _parentWidgetEntityReturnsNewIfNotFound;
		private WidgetGroupEntity _parentWidgetGroupEntity;
		private bool	_alwaysFetchParentWidgetGroupEntity, _alreadyFetchedParentWidgetGroupEntity, _parentWidgetGroupEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentWidgetEntity</summary>
			public static readonly string ParentWidgetEntity = "ParentWidgetEntity";
			/// <summary>Member name ParentWidgetGroupEntity</summary>
			public static readonly string ParentWidgetGroupEntity = "ParentWidgetGroupEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static WidgetGroupWidgetEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected WidgetGroupWidgetEntityBase() :base("WidgetGroupWidgetEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		protected WidgetGroupWidgetEntityBase(System.Int32 widgetGroupId, System.Int32 widgetId):base("WidgetGroupWidgetEntity")
		{
			InitClassFetch(widgetGroupId, widgetId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected WidgetGroupWidgetEntityBase(System.Int32 widgetGroupId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse): base("WidgetGroupWidgetEntity")
		{
			InitClassFetch(widgetGroupId, widgetId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="validator">The custom validator object for this WidgetGroupWidgetEntity</param>
		protected WidgetGroupWidgetEntityBase(System.Int32 widgetGroupId, System.Int32 widgetId, IValidator validator):base("WidgetGroupWidgetEntity")
		{
			InitClassFetch(widgetGroupId, widgetId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WidgetGroupWidgetEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_parentWidgetEntity = (WidgetEntity)info.GetValue("_parentWidgetEntity", typeof(WidgetEntity));
			if(_parentWidgetEntity!=null)
			{
				_parentWidgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentWidgetEntityReturnsNewIfNotFound = info.GetBoolean("_parentWidgetEntityReturnsNewIfNotFound");
			_alwaysFetchParentWidgetEntity = info.GetBoolean("_alwaysFetchParentWidgetEntity");
			_alreadyFetchedParentWidgetEntity = info.GetBoolean("_alreadyFetchedParentWidgetEntity");

			_parentWidgetGroupEntity = (WidgetGroupEntity)info.GetValue("_parentWidgetGroupEntity", typeof(WidgetGroupEntity));
			if(_parentWidgetGroupEntity!=null)
			{
				_parentWidgetGroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentWidgetGroupEntityReturnsNewIfNotFound = info.GetBoolean("_parentWidgetGroupEntityReturnsNewIfNotFound");
			_alwaysFetchParentWidgetGroupEntity = info.GetBoolean("_alwaysFetchParentWidgetGroupEntity");
			_alreadyFetchedParentWidgetGroupEntity = info.GetBoolean("_alreadyFetchedParentWidgetGroupEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((WidgetGroupWidgetFieldIndex)fieldIndex)
			{
				case WidgetGroupWidgetFieldIndex.WidgetGroupId:
					DesetupSyncParentWidgetGroupEntity(true, false);
					_alreadyFetchedParentWidgetGroupEntity = false;
					break;
				case WidgetGroupWidgetFieldIndex.WidgetId:
					DesetupSyncParentWidgetEntity(true, false);
					_alreadyFetchedParentWidgetEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedParentWidgetEntity = (_parentWidgetEntity != null);
			_alreadyFetchedParentWidgetGroupEntity = (_parentWidgetGroupEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParentWidgetEntity":
					toReturn.Add(Relations.WidgetEntityUsingWidgetId);
					break;
				case "ParentWidgetGroupEntity":
					toReturn.Add(Relations.WidgetGroupEntityUsingWidgetGroupId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_parentWidgetEntity", (!this.MarkedForDeletion?_parentWidgetEntity:null));
			info.AddValue("_parentWidgetEntityReturnsNewIfNotFound", _parentWidgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentWidgetEntity", _alwaysFetchParentWidgetEntity);
			info.AddValue("_alreadyFetchedParentWidgetEntity", _alreadyFetchedParentWidgetEntity);
			info.AddValue("_parentWidgetGroupEntity", (!this.MarkedForDeletion?_parentWidgetGroupEntity:null));
			info.AddValue("_parentWidgetGroupEntityReturnsNewIfNotFound", _parentWidgetGroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentWidgetGroupEntity", _alwaysFetchParentWidgetGroupEntity);
			info.AddValue("_alreadyFetchedParentWidgetGroupEntity", _alreadyFetchedParentWidgetGroupEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParentWidgetEntity":
					_alreadyFetchedParentWidgetEntity = true;
					this.ParentWidgetEntity = (WidgetEntity)entity;
					break;
				case "ParentWidgetGroupEntity":
					_alreadyFetchedParentWidgetGroupEntity = true;
					this.ParentWidgetGroupEntity = (WidgetGroupEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParentWidgetEntity":
					SetupSyncParentWidgetEntity(relatedEntity);
					break;
				case "ParentWidgetGroupEntity":
					SetupSyncParentWidgetGroupEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParentWidgetEntity":
					DesetupSyncParentWidgetEntity(false, true);
					break;
				case "ParentWidgetGroupEntity":
					DesetupSyncParentWidgetGroupEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_parentWidgetEntity!=null)
			{
				toReturn.Add(_parentWidgetEntity);
			}
			if(_parentWidgetGroupEntity!=null)
			{
				toReturn.Add(_parentWidgetGroupEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetGroupId, System.Int32 widgetId)
		{
			return FetchUsingPK(widgetGroupId, widgetId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetGroupId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(widgetGroupId, widgetId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetGroupId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(widgetGroupId, widgetId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetGroupId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(widgetGroupId, widgetId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.WidgetGroupId, this.WidgetId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new WidgetGroupWidgetRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public WidgetEntity GetSingleParentWidgetEntity()
		{
			return GetSingleParentWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public virtual WidgetEntity GetSingleParentWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentWidgetEntity || forceFetch || _alwaysFetchParentWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetEntityUsingWidgetId);
				WidgetEntity newEntity = (WidgetEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetEntity.FetchPolymorphic(this.Transaction, this.WidgetId, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentWidgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentWidgetEntity = newEntity;
				_alreadyFetchedParentWidgetEntity = fetchResult;
			}
			return _parentWidgetEntity;
		}


		/// <summary> Retrieves the related entity of type 'WidgetGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetGroupEntity' which is related to this entity.</returns>
		public WidgetGroupEntity GetSingleParentWidgetGroupEntity()
		{
			return GetSingleParentWidgetGroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetGroupEntity' which is related to this entity.</returns>
		public virtual WidgetGroupEntity GetSingleParentWidgetGroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentWidgetGroupEntity || forceFetch || _alwaysFetchParentWidgetGroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetGroupEntityUsingWidgetGroupId);
				WidgetGroupEntity newEntity = (WidgetGroupEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetGroupEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetGroupEntity.FetchPolymorphic(this.Transaction, this.WidgetGroupId, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentWidgetGroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentWidgetGroupEntity = newEntity;
				_alreadyFetchedParentWidgetGroupEntity = fetchResult;
			}
			return _parentWidgetGroupEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParentWidgetEntity", _parentWidgetEntity);
			toReturn.Add("ParentWidgetGroupEntity", _parentWidgetGroupEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="validator">The validator object for this WidgetGroupWidgetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 widgetGroupId, System.Int32 widgetId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(widgetGroupId, widgetId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_parentWidgetEntityReturnsNewIfNotFound = true;
			_parentWidgetGroupEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetGroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _parentWidgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentWidgetEntity, new PropertyChangedEventHandler( OnParentWidgetEntityPropertyChanged ), "ParentWidgetEntity", Obymobi.Data.RelationClasses.StaticWidgetGroupWidgetRelations.WidgetEntityUsingWidgetIdStatic, true, signalRelatedEntity, "ChildWidgetCollection", resetFKFields, new int[] { (int)WidgetGroupWidgetFieldIndex.WidgetId } );		
			_parentWidgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _parentWidgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentWidgetEntity(IEntityCore relatedEntity)
		{
			if(_parentWidgetEntity!=relatedEntity)
			{		
				DesetupSyncParentWidgetEntity(true, true);
				_parentWidgetEntity = (WidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentWidgetEntity, new PropertyChangedEventHandler( OnParentWidgetEntityPropertyChanged ), "ParentWidgetEntity", Obymobi.Data.RelationClasses.StaticWidgetGroupWidgetRelations.WidgetEntityUsingWidgetIdStatic, true, ref _alreadyFetchedParentWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parentWidgetGroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentWidgetGroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentWidgetGroupEntity, new PropertyChangedEventHandler( OnParentWidgetGroupEntityPropertyChanged ), "ParentWidgetGroupEntity", Obymobi.Data.RelationClasses.StaticWidgetGroupWidgetRelations.WidgetGroupEntityUsingWidgetGroupIdStatic, true, signalRelatedEntity, "ChildWidgetGroupWidgetCollection", resetFKFields, new int[] { (int)WidgetGroupWidgetFieldIndex.WidgetGroupId } );		
			_parentWidgetGroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _parentWidgetGroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentWidgetGroupEntity(IEntityCore relatedEntity)
		{
			if(_parentWidgetGroupEntity!=relatedEntity)
			{		
				DesetupSyncParentWidgetGroupEntity(true, true);
				_parentWidgetGroupEntity = (WidgetGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentWidgetGroupEntity, new PropertyChangedEventHandler( OnParentWidgetGroupEntityPropertyChanged ), "ParentWidgetGroupEntity", Obymobi.Data.RelationClasses.StaticWidgetGroupWidgetRelations.WidgetGroupEntityUsingWidgetGroupIdStatic, true, ref _alreadyFetchedParentWidgetGroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentWidgetGroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 widgetGroupId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)WidgetGroupWidgetFieldIndex.WidgetGroupId].ForcedCurrentValueWrite(widgetGroupId);
				this.Fields[(int)WidgetGroupWidgetFieldIndex.WidgetId].ForcedCurrentValueWrite(widgetId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateWidgetGroupWidgetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new WidgetGroupWidgetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static WidgetGroupWidgetRelations Relations
		{
			get	{ return new WidgetGroupWidgetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Widget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetCollection(), (IEntityRelation)GetRelationsForField("ParentWidgetEntity")[0], (int)Obymobi.Data.EntityType.WidgetGroupWidgetEntity, (int)Obymobi.Data.EntityType.WidgetEntity, 0, null, null, null, "ParentWidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WidgetGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentWidgetGroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetGroupCollection(), (IEntityRelation)GetRelationsForField("ParentWidgetGroupEntity")[0], (int)Obymobi.Data.EntityType.WidgetGroupWidgetEntity, (int)Obymobi.Data.EntityType.WidgetGroupEntity, 0, null, null, null, "ParentWidgetGroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The WidgetGroupId property of the Entity WidgetGroupWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "WidgetGroupWidget"."WidgetGroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 WidgetGroupId
		{
			get { return (System.Int32)GetValue((int)WidgetGroupWidgetFieldIndex.WidgetGroupId, true); }
			set	{ SetValue((int)WidgetGroupWidgetFieldIndex.WidgetGroupId, value, true); }
		}

		/// <summary> The WidgetId property of the Entity WidgetGroupWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "WidgetGroupWidget"."WidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 WidgetId
		{
			get { return (System.Int32)GetValue((int)WidgetGroupWidgetFieldIndex.WidgetId, true); }
			set	{ SetValue((int)WidgetGroupWidgetFieldIndex.WidgetId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity WidgetGroupWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "WidgetGroupWidget"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)WidgetGroupWidgetFieldIndex.SortOrder, true); }
			set	{ SetValue((int)WidgetGroupWidgetFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity WidgetGroupWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "WidgetGroupWidget"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)WidgetGroupWidgetFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)WidgetGroupWidgetFieldIndex.ParentCompanyId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'WidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetEntity ParentWidgetEntity
		{
			get	{ return GetSingleParentWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildWidgetCollection", "ParentWidgetEntity", _parentWidgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentWidgetEntity. When set to true, ParentWidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentWidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleParentWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentWidgetEntity
		{
			get	{ return _alwaysFetchParentWidgetEntity; }
			set	{ _alwaysFetchParentWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentWidgetEntity already has been fetched. Setting this property to false when ParentWidgetEntity has been fetched
		/// will set ParentWidgetEntity to null as well. Setting this property to true while ParentWidgetEntity hasn't been fetched disables lazy loading for ParentWidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentWidgetEntity
		{
			get { return _alreadyFetchedParentWidgetEntity;}
			set 
			{
				if(_alreadyFetchedParentWidgetEntity && !value)
				{
					this.ParentWidgetEntity = null;
				}
				_alreadyFetchedParentWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentWidgetEntity is not found
		/// in the database. When set to true, ParentWidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ParentWidgetEntityReturnsNewIfNotFound
		{
			get	{ return _parentWidgetEntityReturnsNewIfNotFound; }
			set { _parentWidgetEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WidgetGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentWidgetGroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetGroupEntity ParentWidgetGroupEntity
		{
			get	{ return GetSingleParentWidgetGroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentWidgetGroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildWidgetGroupWidgetCollection", "ParentWidgetGroupEntity", _parentWidgetGroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentWidgetGroupEntity. When set to true, ParentWidgetGroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentWidgetGroupEntity is accessed. You can always execute a forced fetch by calling GetSingleParentWidgetGroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentWidgetGroupEntity
		{
			get	{ return _alwaysFetchParentWidgetGroupEntity; }
			set	{ _alwaysFetchParentWidgetGroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentWidgetGroupEntity already has been fetched. Setting this property to false when ParentWidgetGroupEntity has been fetched
		/// will set ParentWidgetGroupEntity to null as well. Setting this property to true while ParentWidgetGroupEntity hasn't been fetched disables lazy loading for ParentWidgetGroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentWidgetGroupEntity
		{
			get { return _alreadyFetchedParentWidgetGroupEntity;}
			set 
			{
				if(_alreadyFetchedParentWidgetGroupEntity && !value)
				{
					this.ParentWidgetGroupEntity = null;
				}
				_alreadyFetchedParentWidgetGroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentWidgetGroupEntity is not found
		/// in the database. When set to true, ParentWidgetGroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ParentWidgetGroupEntityReturnsNewIfNotFound
		{
			get	{ return _parentWidgetGroupEntityReturnsNewIfNotFound; }
			set { _parentWidgetGroupEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.WidgetGroupWidgetEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
