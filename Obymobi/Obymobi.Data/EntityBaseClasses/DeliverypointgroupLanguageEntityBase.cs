﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'DeliverypointgroupLanguage'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeliverypointgroupLanguageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "DeliverypointgroupLanguageEntity"; }
		}
	
		#region Class Member Declarations
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private LanguageEntity _languageEntity;
		private bool	_alwaysFetchLanguageEntity, _alreadyFetchedLanguageEntity, _languageEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name LanguageEntity</summary>
			public static readonly string LanguageEntity = "LanguageEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeliverypointgroupLanguageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected DeliverypointgroupLanguageEntityBase() :base("DeliverypointgroupLanguageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		protected DeliverypointgroupLanguageEntityBase(System.Int32 deliverypointgroupLanguageId):base("DeliverypointgroupLanguageEntity")
		{
			InitClassFetch(deliverypointgroupLanguageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected DeliverypointgroupLanguageEntityBase(System.Int32 deliverypointgroupLanguageId, IPrefetchPath prefetchPathToUse): base("DeliverypointgroupLanguageEntity")
		{
			InitClassFetch(deliverypointgroupLanguageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <param name="validator">The custom validator object for this DeliverypointgroupLanguageEntity</param>
		protected DeliverypointgroupLanguageEntityBase(System.Int32 deliverypointgroupLanguageId, IValidator validator):base("DeliverypointgroupLanguageEntity")
		{
			InitClassFetch(deliverypointgroupLanguageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointgroupLanguageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_languageEntity = (LanguageEntity)info.GetValue("_languageEntity", typeof(LanguageEntity));
			if(_languageEntity!=null)
			{
				_languageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_languageEntityReturnsNewIfNotFound = info.GetBoolean("_languageEntityReturnsNewIfNotFound");
			_alwaysFetchLanguageEntity = info.GetBoolean("_alwaysFetchLanguageEntity");
			_alreadyFetchedLanguageEntity = info.GetBoolean("_alreadyFetchedLanguageEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeliverypointgroupLanguageFieldIndex)fieldIndex)
			{
				case DeliverypointgroupLanguageFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case DeliverypointgroupLanguageFieldIndex.LanguageId:
					DesetupSyncLanguageEntity(true, false);
					_alreadyFetchedLanguageEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedLanguageEntity = (_languageEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "LanguageEntity":
					toReturn.Add(Relations.LanguageEntityUsingLanguageId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_languageEntity", (!this.MarkedForDeletion?_languageEntity:null));
			info.AddValue("_languageEntityReturnsNewIfNotFound", _languageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLanguageEntity", _alwaysFetchLanguageEntity);
			info.AddValue("_alreadyFetchedLanguageEntity", _alreadyFetchedLanguageEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "LanguageEntity":
					_alreadyFetchedLanguageEntity = true;
					this.LanguageEntity = (LanguageEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "LanguageEntity":
					SetupSyncLanguageEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "LanguageEntity":
					DesetupSyncLanguageEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_languageEntity!=null)
			{
				toReturn.Add(_languageEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupLanguageId)
		{
			return FetchUsingPK(deliverypointgroupLanguageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupLanguageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deliverypointgroupLanguageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deliverypointgroupLanguageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deliverypointgroupLanguageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeliverypointgroupLanguageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeliverypointgroupLanguageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId);
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public LanguageEntity GetSingleLanguageEntity()
		{
			return GetSingleLanguageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public virtual LanguageEntity GetSingleLanguageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLanguageEntity || forceFetch || _alwaysFetchLanguageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LanguageEntityUsingLanguageId);
				LanguageEntity newEntity = new LanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LanguageId);
				}
				if(fetchResult)
				{
					newEntity = (LanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_languageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LanguageEntity = newEntity;
				_alreadyFetchedLanguageEntity = fetchResult;
			}
			return _languageEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("LanguageEntity", _languageEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <param name="validator">The validator object for this DeliverypointgroupLanguageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 deliverypointgroupLanguageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deliverypointgroupLanguageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_languageEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupLanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderFailedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderFailedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompletedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompletedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderSavingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderSavingMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceSavingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceSavingMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingSavingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingSavingMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingProcessedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingProcessedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl1Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl1Zoom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl2Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl2Zoom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl3Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl3Zoom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointCaption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckinFailedText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckinFailedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutApproveText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsDeviceLockedText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsDeviceLockedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsRestartText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsRestartTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsWelcomeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsWelcomeTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutCompleteText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutCompleteTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomserviceChargeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SuggestionsCaption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintingConfirmationTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintingConfirmationText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PagesCaption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintingSucceededTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintingSucceededText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedDialogTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedDialogText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedReminderDialogTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedReminderDialogText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TurnOffPrivacyTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TurnOffPrivacyText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ItemCurrentlyUnavailableText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceFailedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceFailedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlarmSetWhileNotChargingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlarmSetWhileNotChargingText", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupLanguageRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "DeliverypointgroupLanguageCollection", resetFKFields, new int[] { (int)DeliverypointgroupLanguageFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupLanguageRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _languageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLanguageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, signalRelatedEntity, "DeliverypointgroupLanguageCollection", resetFKFields, new int[] { (int)DeliverypointgroupLanguageFieldIndex.LanguageId } );		
			_languageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _languageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLanguageEntity(IEntityCore relatedEntity)
		{
			if(_languageEntity!=relatedEntity)
			{		
				DesetupSyncLanguageEntity(true, true);
				_languageEntity = (LanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, ref _alreadyFetchedLanguageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLanguageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deliverypointgroupLanguageId">PK value for DeliverypointgroupLanguage which data should be fetched into this DeliverypointgroupLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deliverypointgroupLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeliverypointgroupLanguageFieldIndex.DeliverypointgroupLanguageId].ForcedCurrentValueWrite(deliverypointgroupLanguageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliverypointgroupLanguageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeliverypointgroupLanguageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeliverypointgroupLanguageRelations Relations
		{
			get	{ return new DeliverypointgroupLanguageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), (IEntityRelation)GetRelationsForField("LanguageEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, null, "LanguageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeliverypointgroupLanguageId property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."DeliverypointgroupLanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointgroupLanguageId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupLanguageFieldIndex.DeliverypointgroupLanguageId, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.DeliverypointgroupLanguageId, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupLanguageFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The LanguageId property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."LanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LanguageId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupLanguageFieldIndex.LanguageId, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.LanguageId, value, true); }
		}

		/// <summary> The OutOfChargeTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OutOfChargeTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OutOfChargeTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OutOfChargeTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OutOfChargeTitle, value, true); }
		}

		/// <summary> The OutOfChargeMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OutOfChargeMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OutOfChargeMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OutOfChargeMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OutOfChargeMessage, value, true); }
		}

		/// <summary> The OrderProcessedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderProcessedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderProcessedTitle, value, true); }
		}

		/// <summary> The OrderProcessedMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderProcessedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderProcessedMessage, value, true); }
		}

		/// <summary> The OrderFailedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderFailedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderFailedTitle, value, true); }
		}

		/// <summary> The OrderFailedMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderFailedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderFailedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderFailedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderFailedMessage, value, true); }
		}

		/// <summary> The OrderCompletedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderCompletedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompletedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderCompletedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderCompletedTitle, value, true); }
		}

		/// <summary> The OrderCompletedMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderCompletedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompletedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderCompletedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderCompletedMessage, value, true); }
		}

		/// <summary> The OrderSavingTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderSavingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderSavingTitle, value, true); }
		}

		/// <summary> The OrderSavingMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."OrderSavingMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.OrderSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.OrderSavingMessage, value, true); }
		}

		/// <summary> The ServiceSavingTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ServiceSavingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceSavingTitle, value, true); }
		}

		/// <summary> The ServiceSavingMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ServiceSavingMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceSavingMessage, value, true); }
		}

		/// <summary> The ServiceProcessedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ServiceProcessedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceProcessedTitle, value, true); }
		}

		/// <summary> The ServiceProcessedMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ServiceProcessedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceProcessedMessage, value, true); }
		}

		/// <summary> The RatingSavingTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."RatingSavingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.RatingSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.RatingSavingTitle, value, true); }
		}

		/// <summary> The RatingSavingMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."RatingSavingMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.RatingSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.RatingSavingMessage, value, true); }
		}

		/// <summary> The RatingProcessedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."RatingProcessedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.RatingProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.RatingProcessedTitle, value, true); }
		}

		/// <summary> The RatingProcessedMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."RatingProcessedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.RatingProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.RatingProcessedMessage, value, true); }
		}

		/// <summary> The HotelUrl1 property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl1
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl1, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl1, value, true); }
		}

		/// <summary> The HotelUrl1Caption property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl1Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl1Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl1Caption, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl1Caption, value, true); }
		}

		/// <summary> The HotelUrl1Zoom property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl1Zoom"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl1Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl1Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl1Zoom, value, true); }
		}

		/// <summary> The HotelUrl2 property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl2
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl2, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl2, value, true); }
		}

		/// <summary> The HotelUrl2Caption property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl2Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl2Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl2Caption, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl2Caption, value, true); }
		}

		/// <summary> The HotelUrl2Zoom property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl2Zoom"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl2Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl2Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl2Zoom, value, true); }
		}

		/// <summary> The HotelUrl3 property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl3
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl3, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl3, value, true); }
		}

		/// <summary> The HotelUrl3Caption property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl3Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl3Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl3Caption, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl3Caption, value, true); }
		}

		/// <summary> The HotelUrl3Zoom property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."HotelUrl3Zoom"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl3Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl3Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.HotelUrl3Zoom, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupLanguageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The DeliverypointCaption property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."DeliverypointCaption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointCaption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.DeliverypointCaption, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.DeliverypointCaption, value, true); }
		}

		/// <summary> The PmsCheckinFailedText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsCheckinFailedText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckinFailedText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckinFailedText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckinFailedText, value, true); }
		}

		/// <summary> The PmsCheckinFailedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsCheckinFailedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckinFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckinFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckinFailedTitle, value, true); }
		}

		/// <summary> The PmsCheckoutApproveText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsCheckoutApproveText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutApproveText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutApproveText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutApproveText, value, true); }
		}

		/// <summary> The PmsDeviceLockedText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsDeviceLockedText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsDeviceLockedText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsDeviceLockedText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsDeviceLockedText, value, true); }
		}

		/// <summary> The PmsDeviceLockedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsDeviceLockedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsDeviceLockedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsDeviceLockedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsDeviceLockedTitle, value, true); }
		}

		/// <summary> The PmsRestartText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsRestartText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsRestartText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsRestartText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsRestartText, value, true); }
		}

		/// <summary> The PmsRestartTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsRestartTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsRestartTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsRestartTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsRestartTitle, value, true); }
		}

		/// <summary> The PmsWelcomeText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsWelcomeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsWelcomeText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsWelcomeText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsWelcomeText, value, true); }
		}

		/// <summary> The PmsWelcomeTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsWelcomeTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsWelcomeTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsWelcomeTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsWelcomeTitle, value, true); }
		}

		/// <summary> The PmsCheckoutCompleteText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsCheckoutCompleteText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutCompleteText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutCompleteText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutCompleteText, value, true); }
		}

		/// <summary> The PmsCheckoutCompleteTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PmsCheckoutCompleteTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutCompleteTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutCompleteTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutCompleteTitle, value, true); }
		}

		/// <summary> The Name property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.Name, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.Name, value, true); }
		}

		/// <summary> The RoomserviceChargeText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."RoomserviceChargeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomserviceChargeText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.RoomserviceChargeText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.RoomserviceChargeText, value, true); }
		}

		/// <summary> The SuggestionsCaption property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."SuggestionsCaption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SuggestionsCaption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.SuggestionsCaption, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.SuggestionsCaption, value, true); }
		}

		/// <summary> The PrintingConfirmationTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PrintingConfirmationTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintingConfirmationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingConfirmationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingConfirmationTitle, value, true); }
		}

		/// <summary> The PrintingConfirmationText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PrintingConfirmationText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintingConfirmationText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingConfirmationText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingConfirmationText, value, true); }
		}

		/// <summary> The PagesCaption property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PagesCaption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PagesCaption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PagesCaption, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PagesCaption, value, true); }
		}

		/// <summary> The PrintingSucceededTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PrintingSucceededTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintingSucceededTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingSucceededTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingSucceededTitle, value, true); }
		}

		/// <summary> The PrintingSucceededText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."PrintingSucceededText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintingSucceededText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingSucceededText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.PrintingSucceededText, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupLanguageFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The ChargerRemovedDialogTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ChargerRemovedDialogTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedDialogTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedDialogTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedDialogTitle, value, true); }
		}

		/// <summary> The ChargerRemovedDialogText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ChargerRemovedDialogText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedDialogText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedDialogText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedDialogText, value, true); }
		}

		/// <summary> The ChargerRemovedReminderDialogTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ChargerRemovedReminderDialogTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedReminderDialogTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedReminderDialogTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedReminderDialogTitle, value, true); }
		}

		/// <summary> The ChargerRemovedReminderDialogText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ChargerRemovedReminderDialogText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedReminderDialogText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedReminderDialogText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedReminderDialogText, value, true); }
		}

		/// <summary> The TurnOffPrivacyTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."TurnOffPrivacyTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TurnOffPrivacyTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.TurnOffPrivacyTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.TurnOffPrivacyTitle, value, true); }
		}

		/// <summary> The TurnOffPrivacyText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."TurnOffPrivacyText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TurnOffPrivacyText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.TurnOffPrivacyText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.TurnOffPrivacyText, value, true); }
		}

		/// <summary> The ItemCurrentlyUnavailableText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ItemCurrentlyUnavailableText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ItemCurrentlyUnavailableText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ItemCurrentlyUnavailableText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ItemCurrentlyUnavailableText, value, true); }
		}

		/// <summary> The ServiceFailedTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ServiceFailedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceFailedTitle, value, true); }
		}

		/// <summary> The ServiceFailedMessage property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."ServiceFailedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceFailedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceFailedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.ServiceFailedMessage, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupLanguageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupLanguageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The AlarmSetWhileNotChargingTitle property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."AlarmSetWhileNotChargingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlarmSetWhileNotChargingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.AlarmSetWhileNotChargingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.AlarmSetWhileNotChargingTitle, value, true); }
		}

		/// <summary> The AlarmSetWhileNotChargingText property of the Entity DeliverypointgroupLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupLanguage"."AlarmSetWhileNotChargingText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlarmSetWhileNotChargingText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupLanguageFieldIndex.AlarmSetWhileNotChargingText, true); }
			set	{ SetValue((int)DeliverypointgroupLanguageFieldIndex.AlarmSetWhileNotChargingText, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupLanguageCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLanguageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LanguageEntity LanguageEntity
		{
			get	{ return GetSingleLanguageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLanguageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupLanguageCollection", "LanguageEntity", _languageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageEntity. When set to true, LanguageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageEntity is accessed. You can always execute a forced fetch by calling GetSingleLanguageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageEntity
		{
			get	{ return _alwaysFetchLanguageEntity; }
			set	{ _alwaysFetchLanguageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageEntity already has been fetched. Setting this property to false when LanguageEntity has been fetched
		/// will set LanguageEntity to null as well. Setting this property to true while LanguageEntity hasn't been fetched disables lazy loading for LanguageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageEntity
		{
			get { return _alreadyFetchedLanguageEntity;}
			set 
			{
				if(_alreadyFetchedLanguageEntity && !value)
				{
					this.LanguageEntity = null;
				}
				_alreadyFetchedLanguageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LanguageEntity is not found
		/// in the database. When set to true, LanguageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LanguageEntityReturnsNewIfNotFound
		{
			get	{ return _languageEntityReturnsNewIfNotFound; }
			set { _languageEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
