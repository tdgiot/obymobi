﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'TerminalLogFile'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class TerminalLogFileEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "TerminalLogFileEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.TerminalLogCollection	_terminalLogCollection;
		private bool	_alwaysFetchTerminalLogCollection, _alreadyFetchedTerminalLogCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaTerminalLog;
		private bool	_alwaysFetchOrderCollectionViaTerminalLog, _alreadyFetchedOrderCollectionViaTerminalLog;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaTerminalLog;
		private bool	_alwaysFetchTerminalCollectionViaTerminalLog, _alreadyFetchedTerminalCollectionViaTerminalLog;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TerminalLogCollection</summary>
			public static readonly string TerminalLogCollection = "TerminalLogCollection";
			/// <summary>Member name OrderCollectionViaTerminalLog</summary>
			public static readonly string OrderCollectionViaTerminalLog = "OrderCollectionViaTerminalLog";
			/// <summary>Member name TerminalCollectionViaTerminalLog</summary>
			public static readonly string TerminalCollectionViaTerminalLog = "TerminalCollectionViaTerminalLog";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TerminalLogFileEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected TerminalLogFileEntityBase() :base("TerminalLogFileEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		protected TerminalLogFileEntityBase(System.Int32 terminalLogFileId):base("TerminalLogFileEntity")
		{
			InitClassFetch(terminalLogFileId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected TerminalLogFileEntityBase(System.Int32 terminalLogFileId, IPrefetchPath prefetchPathToUse): base("TerminalLogFileEntity")
		{
			InitClassFetch(terminalLogFileId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <param name="validator">The custom validator object for this TerminalLogFileEntity</param>
		protected TerminalLogFileEntityBase(System.Int32 terminalLogFileId, IValidator validator):base("TerminalLogFileEntity")
		{
			InitClassFetch(terminalLogFileId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalLogFileEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_terminalLogCollection = (Obymobi.Data.CollectionClasses.TerminalLogCollection)info.GetValue("_terminalLogCollection", typeof(Obymobi.Data.CollectionClasses.TerminalLogCollection));
			_alwaysFetchTerminalLogCollection = info.GetBoolean("_alwaysFetchTerminalLogCollection");
			_alreadyFetchedTerminalLogCollection = info.GetBoolean("_alreadyFetchedTerminalLogCollection");
			_orderCollectionViaTerminalLog = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaTerminalLog", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaTerminalLog = info.GetBoolean("_alwaysFetchOrderCollectionViaTerminalLog");
			_alreadyFetchedOrderCollectionViaTerminalLog = info.GetBoolean("_alreadyFetchedOrderCollectionViaTerminalLog");

			_terminalCollectionViaTerminalLog = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaTerminalLog", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaTerminalLog = info.GetBoolean("_alwaysFetchTerminalCollectionViaTerminalLog");
			_alreadyFetchedTerminalCollectionViaTerminalLog = info.GetBoolean("_alreadyFetchedTerminalCollectionViaTerminalLog");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTerminalLogCollection = (_terminalLogCollection.Count > 0);
			_alreadyFetchedOrderCollectionViaTerminalLog = (_orderCollectionViaTerminalLog.Count > 0);
			_alreadyFetchedTerminalCollectionViaTerminalLog = (_terminalCollectionViaTerminalLog.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TerminalLogCollection":
					toReturn.Add(Relations.TerminalLogEntityUsingTerminalLogFileId);
					break;
				case "OrderCollectionViaTerminalLog":
					toReturn.Add(Relations.TerminalLogEntityUsingTerminalLogFileId, "TerminalLogFileEntity__", "TerminalLog_", JoinHint.None);
					toReturn.Add(TerminalLogEntity.Relations.OrderEntityUsingOrderId, "TerminalLog_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaTerminalLog":
					toReturn.Add(Relations.TerminalLogEntityUsingTerminalLogFileId, "TerminalLogFileEntity__", "TerminalLog_", JoinHint.None);
					toReturn.Add(TerminalLogEntity.Relations.TerminalEntityUsingTerminalId, "TerminalLog_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_terminalLogCollection", (!this.MarkedForDeletion?_terminalLogCollection:null));
			info.AddValue("_alwaysFetchTerminalLogCollection", _alwaysFetchTerminalLogCollection);
			info.AddValue("_alreadyFetchedTerminalLogCollection", _alreadyFetchedTerminalLogCollection);
			info.AddValue("_orderCollectionViaTerminalLog", (!this.MarkedForDeletion?_orderCollectionViaTerminalLog:null));
			info.AddValue("_alwaysFetchOrderCollectionViaTerminalLog", _alwaysFetchOrderCollectionViaTerminalLog);
			info.AddValue("_alreadyFetchedOrderCollectionViaTerminalLog", _alreadyFetchedOrderCollectionViaTerminalLog);
			info.AddValue("_terminalCollectionViaTerminalLog", (!this.MarkedForDeletion?_terminalCollectionViaTerminalLog:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaTerminalLog", _alwaysFetchTerminalCollectionViaTerminalLog);
			info.AddValue("_alreadyFetchedTerminalCollectionViaTerminalLog", _alreadyFetchedTerminalCollectionViaTerminalLog);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TerminalLogCollection":
					_alreadyFetchedTerminalLogCollection = true;
					if(entity!=null)
					{
						this.TerminalLogCollection.Add((TerminalLogEntity)entity);
					}
					break;
				case "OrderCollectionViaTerminalLog":
					_alreadyFetchedOrderCollectionViaTerminalLog = true;
					if(entity!=null)
					{
						this.OrderCollectionViaTerminalLog.Add((OrderEntity)entity);
					}
					break;
				case "TerminalCollectionViaTerminalLog":
					_alreadyFetchedTerminalCollectionViaTerminalLog = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaTerminalLog.Add((TerminalEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TerminalLogCollection":
					_terminalLogCollection.Add((TerminalLogEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TerminalLogCollection":
					this.PerformRelatedEntityRemoval(_terminalLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_terminalLogCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogFileId)
		{
			return FetchUsingPK(terminalLogFileId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogFileId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(terminalLogFileId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogFileId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(terminalLogFileId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 terminalLogFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(terminalLogFileId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TerminalLogFileId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TerminalLogFileRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch)
		{
			return GetMultiTerminalLogCollection(forceFetch, _terminalLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalLogCollection(forceFetch, _terminalLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogCollection GetMultiTerminalLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalLogCollection || forceFetch || _alwaysFetchTerminalLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalLogCollection);
				_terminalLogCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalLogCollection.GetMultiManyToOne(null, null, this, filter);
				_terminalLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalLogCollection = true;
			}
			return _terminalLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalLogCollection'. These settings will be taken into account
		/// when the property TerminalLogCollection is requested or GetMultiTerminalLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalLogCollection.SortClauses=sortClauses;
			_terminalLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaTerminalLog(bool forceFetch)
		{
			return GetMultiOrderCollectionViaTerminalLog(forceFetch, _orderCollectionViaTerminalLog.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaTerminalLog(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaTerminalLog || forceFetch || _alwaysFetchOrderCollectionViaTerminalLog) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaTerminalLog);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalLogFileFields.TerminalLogFileId, ComparisonOperator.Equal, this.TerminalLogFileId, "TerminalLogFileEntity__"));
				_orderCollectionViaTerminalLog.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaTerminalLog.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaTerminalLog.GetMulti(filter, GetRelationsForField("OrderCollectionViaTerminalLog"));
				_orderCollectionViaTerminalLog.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaTerminalLog = true;
			}
			return _orderCollectionViaTerminalLog;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaTerminalLog'. These settings will be taken into account
		/// when the property OrderCollectionViaTerminalLog is requested or GetMultiOrderCollectionViaTerminalLog is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaTerminalLog(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaTerminalLog.SortClauses=sortClauses;
			_orderCollectionViaTerminalLog.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminalLog(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaTerminalLog(forceFetch, _terminalCollectionViaTerminalLog.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminalLog(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaTerminalLog || forceFetch || _alwaysFetchTerminalCollectionViaTerminalLog) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaTerminalLog);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TerminalLogFileFields.TerminalLogFileId, ComparisonOperator.Equal, this.TerminalLogFileId, "TerminalLogFileEntity__"));
				_terminalCollectionViaTerminalLog.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaTerminalLog.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaTerminalLog.GetMulti(filter, GetRelationsForField("TerminalCollectionViaTerminalLog"));
				_terminalCollectionViaTerminalLog.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaTerminalLog = true;
			}
			return _terminalCollectionViaTerminalLog;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaTerminalLog'. These settings will be taken into account
		/// when the property TerminalCollectionViaTerminalLog is requested or GetMultiTerminalCollectionViaTerminalLog is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaTerminalLog(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaTerminalLog.SortClauses=sortClauses;
			_terminalCollectionViaTerminalLog.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TerminalLogCollection", _terminalLogCollection);
			toReturn.Add("OrderCollectionViaTerminalLog", _orderCollectionViaTerminalLog);
			toReturn.Add("TerminalCollectionViaTerminalLog", _terminalCollectionViaTerminalLog);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <param name="validator">The validator object for this TerminalLogFileEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 terminalLogFileId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(terminalLogFileId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_terminalLogCollection = new Obymobi.Data.CollectionClasses.TerminalLogCollection();
			_terminalLogCollection.SetContainingEntityInfo(this, "TerminalLogFileEntity");
			_orderCollectionViaTerminalLog = new Obymobi.Data.CollectionClasses.OrderCollection();
			_terminalCollectionViaTerminalLog = new Obymobi.Data.CollectionClasses.TerminalCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalLogFileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Application", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogDate", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="terminalLogFileId">PK value for TerminalLogFile which data should be fetched into this TerminalLogFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 terminalLogFileId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TerminalLogFileFieldIndex.TerminalLogFileId].ForcedCurrentValueWrite(terminalLogFileId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTerminalLogFileDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TerminalLogFileEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TerminalLogFileRelations Relations
		{
			get	{ return new TerminalLogFileRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalLogCollection(), (IEntityRelation)GetRelationsForField("TerminalLogCollection")[0], (int)Obymobi.Data.EntityType.TerminalLogFileEntity, (int)Obymobi.Data.EntityType.TerminalLogEntity, 0, null, null, null, "TerminalLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaTerminalLog
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalLogEntityUsingTerminalLogFileId;
				intermediateRelation.SetAliases(string.Empty, "TerminalLog_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalLogFileEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaTerminalLog"), "OrderCollectionViaTerminalLog", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaTerminalLog
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalLogEntityUsingTerminalLogFileId;
				intermediateRelation.SetAliases(string.Empty, "TerminalLog_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.TerminalLogFileEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaTerminalLog"), "TerminalCollectionViaTerminalLog", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The TerminalLogFileId property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."TerminalLogFileId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TerminalLogFileId
		{
			get { return (System.Int32)GetValue((int)TerminalLogFileFieldIndex.TerminalLogFileId, true); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.TerminalLogFileId, value, true); }
		}

		/// <summary> The Message property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."Message"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)TerminalLogFileFieldIndex.Message, true); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.Message, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalLogFileFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)TerminalLogFileFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Application property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."Application"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Application
		{
			get { return (System.String)GetValue((int)TerminalLogFileFieldIndex.Application, true); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.Application, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalLogFileFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalLogFileFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LogDate property of the Entity TerminalLogFile<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TerminalLogFile"."LogDate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LogDate
		{
			get { return (System.DateTime)GetValue((int)TerminalLogFileFieldIndex.LogDate, true); }
			set	{ SetValue((int)TerminalLogFileFieldIndex.LogDate, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TerminalLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalLogCollection TerminalLogCollection
		{
			get	{ return GetMultiTerminalLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalLogCollection. When set to true, TerminalLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalLogCollection
		{
			get	{ return _alwaysFetchTerminalLogCollection; }
			set	{ _alwaysFetchTerminalLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalLogCollection already has been fetched. Setting this property to false when TerminalLogCollection has been fetched
		/// will clear the TerminalLogCollection collection well. Setting this property to true while TerminalLogCollection hasn't been fetched disables lazy loading for TerminalLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalLogCollection
		{
			get { return _alreadyFetchedTerminalLogCollection;}
			set 
			{
				if(_alreadyFetchedTerminalLogCollection && !value && (_terminalLogCollection != null))
				{
					_terminalLogCollection.Clear();
				}
				_alreadyFetchedTerminalLogCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaTerminalLog()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaTerminalLog
		{
			get { return GetMultiOrderCollectionViaTerminalLog(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaTerminalLog. When set to true, OrderCollectionViaTerminalLog is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaTerminalLog is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaTerminalLog(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaTerminalLog
		{
			get	{ return _alwaysFetchOrderCollectionViaTerminalLog; }
			set	{ _alwaysFetchOrderCollectionViaTerminalLog = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaTerminalLog already has been fetched. Setting this property to false when OrderCollectionViaTerminalLog has been fetched
		/// will clear the OrderCollectionViaTerminalLog collection well. Setting this property to true while OrderCollectionViaTerminalLog hasn't been fetched disables lazy loading for OrderCollectionViaTerminalLog</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaTerminalLog
		{
			get { return _alreadyFetchedOrderCollectionViaTerminalLog;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaTerminalLog && !value && (_orderCollectionViaTerminalLog != null))
				{
					_orderCollectionViaTerminalLog.Clear();
				}
				_alreadyFetchedOrderCollectionViaTerminalLog = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaTerminalLog()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaTerminalLog
		{
			get { return GetMultiTerminalCollectionViaTerminalLog(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaTerminalLog. When set to true, TerminalCollectionViaTerminalLog is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaTerminalLog is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaTerminalLog(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaTerminalLog
		{
			get	{ return _alwaysFetchTerminalCollectionViaTerminalLog; }
			set	{ _alwaysFetchTerminalCollectionViaTerminalLog = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaTerminalLog already has been fetched. Setting this property to false when TerminalCollectionViaTerminalLog has been fetched
		/// will clear the TerminalCollectionViaTerminalLog collection well. Setting this property to true while TerminalCollectionViaTerminalLog hasn't been fetched disables lazy loading for TerminalCollectionViaTerminalLog</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaTerminalLog
		{
			get { return _alreadyFetchedTerminalCollectionViaTerminalLog;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaTerminalLog && !value && (_terminalCollectionViaTerminalLog != null))
				{
					_terminalCollectionViaTerminalLog.Clear();
				}
				_alreadyFetchedTerminalCollectionViaTerminalLog = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.TerminalLogFileEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
