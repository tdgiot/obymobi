﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UIScheduleItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UIScheduleItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UIScheduleItemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection	_uIScheduleItemOccurrenceCollection;
		private bool	_alwaysFetchUIScheduleItemOccurrenceCollection, _alreadyFetchedUIScheduleItemOccurrenceCollection;
		private MediaEntity _mediaEntity;
		private bool	_alwaysFetchMediaEntity, _alreadyFetchedMediaEntity, _mediaEntityReturnsNewIfNotFound;
		private ReportProcessingTaskTemplateEntity _reportProcessingTaskTemplateEntity;
		private bool	_alwaysFetchReportProcessingTaskTemplateEntity, _alreadyFetchedReportProcessingTaskTemplateEntity, _reportProcessingTaskTemplateEntityReturnsNewIfNotFound;
		private ScheduledMessageEntity _scheduledMessageEntity;
		private bool	_alwaysFetchScheduledMessageEntity, _alreadyFetchedScheduledMessageEntity, _scheduledMessageEntityReturnsNewIfNotFound;
		private UIScheduleEntity _uIScheduleEntity;
		private bool	_alwaysFetchUIScheduleEntity, _alreadyFetchedUIScheduleEntity, _uIScheduleEntityReturnsNewIfNotFound;
		private UIWidgetEntity _uIWidgetEntity;
		private bool	_alwaysFetchUIWidgetEntity, _alreadyFetchedUIWidgetEntity, _uIWidgetEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name MediaEntity</summary>
			public static readonly string MediaEntity = "MediaEntity";
			/// <summary>Member name ReportProcessingTaskTemplateEntity</summary>
			public static readonly string ReportProcessingTaskTemplateEntity = "ReportProcessingTaskTemplateEntity";
			/// <summary>Member name ScheduledMessageEntity</summary>
			public static readonly string ScheduledMessageEntity = "ScheduledMessageEntity";
			/// <summary>Member name UIScheduleEntity</summary>
			public static readonly string UIScheduleEntity = "UIScheduleEntity";
			/// <summary>Member name UIWidgetEntity</summary>
			public static readonly string UIWidgetEntity = "UIWidgetEntity";
			/// <summary>Member name UIScheduleItemOccurrenceCollection</summary>
			public static readonly string UIScheduleItemOccurrenceCollection = "UIScheduleItemOccurrenceCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UIScheduleItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UIScheduleItemEntityBase() :base("UIScheduleItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		protected UIScheduleItemEntityBase(System.Int32 uIScheduleItemId):base("UIScheduleItemEntity")
		{
			InitClassFetch(uIScheduleItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UIScheduleItemEntityBase(System.Int32 uIScheduleItemId, IPrefetchPath prefetchPathToUse): base("UIScheduleItemEntity")
		{
			InitClassFetch(uIScheduleItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <param name="validator">The custom validator object for this UIScheduleItemEntity</param>
		protected UIScheduleItemEntityBase(System.Int32 uIScheduleItemId, IValidator validator):base("UIScheduleItemEntity")
		{
			InitClassFetch(uIScheduleItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIScheduleItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_uIScheduleItemOccurrenceCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection)info.GetValue("_uIScheduleItemOccurrenceCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection));
			_alwaysFetchUIScheduleItemOccurrenceCollection = info.GetBoolean("_alwaysFetchUIScheduleItemOccurrenceCollection");
			_alreadyFetchedUIScheduleItemOccurrenceCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemOccurrenceCollection");
			_mediaEntity = (MediaEntity)info.GetValue("_mediaEntity", typeof(MediaEntity));
			if(_mediaEntity!=null)
			{
				_mediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaEntity = info.GetBoolean("_alwaysFetchMediaEntity");
			_alreadyFetchedMediaEntity = info.GetBoolean("_alreadyFetchedMediaEntity");

			_reportProcessingTaskTemplateEntity = (ReportProcessingTaskTemplateEntity)info.GetValue("_reportProcessingTaskTemplateEntity", typeof(ReportProcessingTaskTemplateEntity));
			if(_reportProcessingTaskTemplateEntity!=null)
			{
				_reportProcessingTaskTemplateEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportProcessingTaskTemplateEntityReturnsNewIfNotFound = info.GetBoolean("_reportProcessingTaskTemplateEntityReturnsNewIfNotFound");
			_alwaysFetchReportProcessingTaskTemplateEntity = info.GetBoolean("_alwaysFetchReportProcessingTaskTemplateEntity");
			_alreadyFetchedReportProcessingTaskTemplateEntity = info.GetBoolean("_alreadyFetchedReportProcessingTaskTemplateEntity");

			_scheduledMessageEntity = (ScheduledMessageEntity)info.GetValue("_scheduledMessageEntity", typeof(ScheduledMessageEntity));
			if(_scheduledMessageEntity!=null)
			{
				_scheduledMessageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_scheduledMessageEntityReturnsNewIfNotFound = info.GetBoolean("_scheduledMessageEntityReturnsNewIfNotFound");
			_alwaysFetchScheduledMessageEntity = info.GetBoolean("_alwaysFetchScheduledMessageEntity");
			_alreadyFetchedScheduledMessageEntity = info.GetBoolean("_alreadyFetchedScheduledMessageEntity");

			_uIScheduleEntity = (UIScheduleEntity)info.GetValue("_uIScheduleEntity", typeof(UIScheduleEntity));
			if(_uIScheduleEntity!=null)
			{
				_uIScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_uIScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchUIScheduleEntity = info.GetBoolean("_alwaysFetchUIScheduleEntity");
			_alreadyFetchedUIScheduleEntity = info.GetBoolean("_alreadyFetchedUIScheduleEntity");

			_uIWidgetEntity = (UIWidgetEntity)info.GetValue("_uIWidgetEntity", typeof(UIWidgetEntity));
			if(_uIWidgetEntity!=null)
			{
				_uIWidgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIWidgetEntityReturnsNewIfNotFound = info.GetBoolean("_uIWidgetEntityReturnsNewIfNotFound");
			_alwaysFetchUIWidgetEntity = info.GetBoolean("_alwaysFetchUIWidgetEntity");
			_alreadyFetchedUIWidgetEntity = info.GetBoolean("_alreadyFetchedUIWidgetEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UIScheduleItemFieldIndex)fieldIndex)
			{
				case UIScheduleItemFieldIndex.UIScheduleId:
					DesetupSyncUIScheduleEntity(true, false);
					_alreadyFetchedUIScheduleEntity = false;
					break;
				case UIScheduleItemFieldIndex.UIWidgetId:
					DesetupSyncUIWidgetEntity(true, false);
					_alreadyFetchedUIWidgetEntity = false;
					break;
				case UIScheduleItemFieldIndex.MediaId:
					DesetupSyncMediaEntity(true, false);
					_alreadyFetchedMediaEntity = false;
					break;
				case UIScheduleItemFieldIndex.ScheduledMessageId:
					DesetupSyncScheduledMessageEntity(true, false);
					_alreadyFetchedScheduledMessageEntity = false;
					break;
				case UIScheduleItemFieldIndex.ReportProcessingTaskTemplateId:
					DesetupSyncReportProcessingTaskTemplateEntity(true, false);
					_alreadyFetchedReportProcessingTaskTemplateEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUIScheduleItemOccurrenceCollection = (_uIScheduleItemOccurrenceCollection.Count > 0);
			_alreadyFetchedMediaEntity = (_mediaEntity != null);
			_alreadyFetchedReportProcessingTaskTemplateEntity = (_reportProcessingTaskTemplateEntity != null);
			_alreadyFetchedScheduledMessageEntity = (_scheduledMessageEntity != null);
			_alreadyFetchedUIScheduleEntity = (_uIScheduleEntity != null);
			_alreadyFetchedUIWidgetEntity = (_uIWidgetEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "MediaEntity":
					toReturn.Add(Relations.MediaEntityUsingMediaId);
					break;
				case "ReportProcessingTaskTemplateEntity":
					toReturn.Add(Relations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId);
					break;
				case "ScheduledMessageEntity":
					toReturn.Add(Relations.ScheduledMessageEntityUsingScheduledMessageId);
					break;
				case "UIScheduleEntity":
					toReturn.Add(Relations.UIScheduleEntityUsingUIScheduleId);
					break;
				case "UIWidgetEntity":
					toReturn.Add(Relations.UIWidgetEntityUsingUIWidgetId);
					break;
				case "UIScheduleItemOccurrenceCollection":
					toReturn.Add(Relations.UIScheduleItemOccurrenceEntityUsingUIScheduleItemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_uIScheduleItemOccurrenceCollection", (!this.MarkedForDeletion?_uIScheduleItemOccurrenceCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemOccurrenceCollection", _alwaysFetchUIScheduleItemOccurrenceCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemOccurrenceCollection", _alreadyFetchedUIScheduleItemOccurrenceCollection);
			info.AddValue("_mediaEntity", (!this.MarkedForDeletion?_mediaEntity:null));
			info.AddValue("_mediaEntityReturnsNewIfNotFound", _mediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaEntity", _alwaysFetchMediaEntity);
			info.AddValue("_alreadyFetchedMediaEntity", _alreadyFetchedMediaEntity);
			info.AddValue("_reportProcessingTaskTemplateEntity", (!this.MarkedForDeletion?_reportProcessingTaskTemplateEntity:null));
			info.AddValue("_reportProcessingTaskTemplateEntityReturnsNewIfNotFound", _reportProcessingTaskTemplateEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReportProcessingTaskTemplateEntity", _alwaysFetchReportProcessingTaskTemplateEntity);
			info.AddValue("_alreadyFetchedReportProcessingTaskTemplateEntity", _alreadyFetchedReportProcessingTaskTemplateEntity);
			info.AddValue("_scheduledMessageEntity", (!this.MarkedForDeletion?_scheduledMessageEntity:null));
			info.AddValue("_scheduledMessageEntityReturnsNewIfNotFound", _scheduledMessageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchScheduledMessageEntity", _alwaysFetchScheduledMessageEntity);
			info.AddValue("_alreadyFetchedScheduledMessageEntity", _alreadyFetchedScheduledMessageEntity);
			info.AddValue("_uIScheduleEntity", (!this.MarkedForDeletion?_uIScheduleEntity:null));
			info.AddValue("_uIScheduleEntityReturnsNewIfNotFound", _uIScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIScheduleEntity", _alwaysFetchUIScheduleEntity);
			info.AddValue("_alreadyFetchedUIScheduleEntity", _alreadyFetchedUIScheduleEntity);
			info.AddValue("_uIWidgetEntity", (!this.MarkedForDeletion?_uIWidgetEntity:null));
			info.AddValue("_uIWidgetEntityReturnsNewIfNotFound", _uIWidgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIWidgetEntity", _alwaysFetchUIWidgetEntity);
			info.AddValue("_alreadyFetchedUIWidgetEntity", _alreadyFetchedUIWidgetEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "MediaEntity":
					_alreadyFetchedMediaEntity = true;
					this.MediaEntity = (MediaEntity)entity;
					break;
				case "ReportProcessingTaskTemplateEntity":
					_alreadyFetchedReportProcessingTaskTemplateEntity = true;
					this.ReportProcessingTaskTemplateEntity = (ReportProcessingTaskTemplateEntity)entity;
					break;
				case "ScheduledMessageEntity":
					_alreadyFetchedScheduledMessageEntity = true;
					this.ScheduledMessageEntity = (ScheduledMessageEntity)entity;
					break;
				case "UIScheduleEntity":
					_alreadyFetchedUIScheduleEntity = true;
					this.UIScheduleEntity = (UIScheduleEntity)entity;
					break;
				case "UIWidgetEntity":
					_alreadyFetchedUIWidgetEntity = true;
					this.UIWidgetEntity = (UIWidgetEntity)entity;
					break;
				case "UIScheduleItemOccurrenceCollection":
					_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "MediaEntity":
					SetupSyncMediaEntity(relatedEntity);
					break;
				case "ReportProcessingTaskTemplateEntity":
					SetupSyncReportProcessingTaskTemplateEntity(relatedEntity);
					break;
				case "ScheduledMessageEntity":
					SetupSyncScheduledMessageEntity(relatedEntity);
					break;
				case "UIScheduleEntity":
					SetupSyncUIScheduleEntity(relatedEntity);
					break;
				case "UIWidgetEntity":
					SetupSyncUIWidgetEntity(relatedEntity);
					break;
				case "UIScheduleItemOccurrenceCollection":
					_uIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "MediaEntity":
					DesetupSyncMediaEntity(false, true);
					break;
				case "ReportProcessingTaskTemplateEntity":
					DesetupSyncReportProcessingTaskTemplateEntity(false, true);
					break;
				case "ScheduledMessageEntity":
					DesetupSyncScheduledMessageEntity(false, true);
					break;
				case "UIScheduleEntity":
					DesetupSyncUIScheduleEntity(false, true);
					break;
				case "UIWidgetEntity":
					DesetupSyncUIWidgetEntity(false, true);
					break;
				case "UIScheduleItemOccurrenceCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemOccurrenceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_mediaEntity!=null)
			{
				toReturn.Add(_mediaEntity);
			}
			if(_reportProcessingTaskTemplateEntity!=null)
			{
				toReturn.Add(_reportProcessingTaskTemplateEntity);
			}
			if(_scheduledMessageEntity!=null)
			{
				toReturn.Add(_scheduledMessageEntity);
			}
			if(_uIScheduleEntity!=null)
			{
				toReturn.Add(_uIScheduleEntity);
			}
			if(_uIWidgetEntity!=null)
			{
				toReturn.Add(_uIWidgetEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_uIScheduleItemOccurrenceCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemId)
		{
			return FetchUsingPK(uIScheduleItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uIScheduleItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uIScheduleItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIScheduleItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uIScheduleItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UIScheduleItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UIScheduleItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemOccurrenceCollection || forceFetch || _alwaysFetchUIScheduleItemOccurrenceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemOccurrenceCollection);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemOccurrenceCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemOccurrenceCollection.GetMultiManyToOne(null, null, null, this, null, filter);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
			}
			return _uIScheduleItemOccurrenceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemOccurrenceCollection'. These settings will be taken into account
		/// when the property UIScheduleItemOccurrenceCollection is requested or GetMultiUIScheduleItemOccurrenceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemOccurrenceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemOccurrenceCollection.SortClauses=sortClauses;
			_uIScheduleItemOccurrenceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public MediaEntity GetSingleMediaEntity()
		{
			return GetSingleMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public virtual MediaEntity GetSingleMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaEntity || forceFetch || _alwaysFetchMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaEntityUsingMediaId);
				MediaEntity newEntity = new MediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MediaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaEntity = newEntity;
				_alreadyFetchedMediaEntity = fetchResult;
			}
			return _mediaEntity;
		}


		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskTemplateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportProcessingTaskTemplateEntity' which is related to this entity.</returns>
		public ReportProcessingTaskTemplateEntity GetSingleReportProcessingTaskTemplateEntity()
		{
			return GetSingleReportProcessingTaskTemplateEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskTemplateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportProcessingTaskTemplateEntity' which is related to this entity.</returns>
		public virtual ReportProcessingTaskTemplateEntity GetSingleReportProcessingTaskTemplateEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReportProcessingTaskTemplateEntity || forceFetch || _alwaysFetchReportProcessingTaskTemplateEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId);
				ReportProcessingTaskTemplateEntity newEntity = new ReportProcessingTaskTemplateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReportProcessingTaskTemplateId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReportProcessingTaskTemplateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportProcessingTaskTemplateEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReportProcessingTaskTemplateEntity = newEntity;
				_alreadyFetchedReportProcessingTaskTemplateEntity = fetchResult;
			}
			return _reportProcessingTaskTemplateEntity;
		}


		/// <summary> Retrieves the related entity of type 'ScheduledMessageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ScheduledMessageEntity' which is related to this entity.</returns>
		public ScheduledMessageEntity GetSingleScheduledMessageEntity()
		{
			return GetSingleScheduledMessageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ScheduledMessageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ScheduledMessageEntity' which is related to this entity.</returns>
		public virtual ScheduledMessageEntity GetSingleScheduledMessageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedScheduledMessageEntity || forceFetch || _alwaysFetchScheduledMessageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ScheduledMessageEntityUsingScheduledMessageId);
				ScheduledMessageEntity newEntity = new ScheduledMessageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ScheduledMessageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ScheduledMessageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_scheduledMessageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ScheduledMessageEntity = newEntity;
				_alreadyFetchedScheduledMessageEntity = fetchResult;
			}
			return _scheduledMessageEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public UIScheduleEntity GetSingleUIScheduleEntity()
		{
			return GetSingleUIScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public virtual UIScheduleEntity GetSingleUIScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIScheduleEntity || forceFetch || _alwaysFetchUIScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIScheduleEntityUsingUIScheduleId);
				UIScheduleEntity newEntity = new UIScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIScheduleId);
				}
				if(fetchResult)
				{
					newEntity = (UIScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIScheduleEntity = newEntity;
				_alreadyFetchedUIScheduleEntity = fetchResult;
			}
			return _uIScheduleEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public UIWidgetEntity GetSingleUIWidgetEntity()
		{
			return GetSingleUIWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIWidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIWidgetEntity' which is related to this entity.</returns>
		public virtual UIWidgetEntity GetSingleUIWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIWidgetEntity || forceFetch || _alwaysFetchUIWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIWidgetEntityUsingUIWidgetId);
				UIWidgetEntity newEntity = new UIWidgetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIWidgetId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIWidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIWidgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIWidgetEntity = newEntity;
				_alreadyFetchedUIWidgetEntity = fetchResult;
			}
			return _uIWidgetEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("MediaEntity", _mediaEntity);
			toReturn.Add("ReportProcessingTaskTemplateEntity", _reportProcessingTaskTemplateEntity);
			toReturn.Add("ScheduledMessageEntity", _scheduledMessageEntity);
			toReturn.Add("UIScheduleEntity", _uIScheduleEntity);
			toReturn.Add("UIWidgetEntity", _uIWidgetEntity);
			toReturn.Add("UIScheduleItemOccurrenceCollection", _uIScheduleItemOccurrenceCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <param name="validator">The validator object for this UIScheduleItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uIScheduleItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uIScheduleItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_uIScheduleItemOccurrenceCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection();
			_uIScheduleItemOccurrenceCollection.SetContainingEntityInfo(this, "UIScheduleItemEntity");
			_mediaEntityReturnsNewIfNotFound = true;
			_reportProcessingTaskTemplateEntityReturnsNewIfNotFound = true;
			_scheduledMessageEntityReturnsNewIfNotFound = true;
			_uIScheduleEntityReturnsNewIfNotFound = true;
			_uIWidgetEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIWidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduledMessageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportProcessingTaskTemplateId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _mediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.MediaEntityUsingMediaIdStatic, true, signalRelatedEntity, "UIScheduleItemCollection", resetFKFields, new int[] { (int)UIScheduleItemFieldIndex.MediaId } );		
			_mediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaEntity(true, true);
				_mediaEntity = (MediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.MediaEntityUsingMediaIdStatic, true, ref _alreadyFetchedMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _reportProcessingTaskTemplateEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReportProcessingTaskTemplateEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _reportProcessingTaskTemplateEntity, new PropertyChangedEventHandler( OnReportProcessingTaskTemplateEntityPropertyChanged ), "ReportProcessingTaskTemplateEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateIdStatic, true, signalRelatedEntity, "UIScheduleItemCollection", resetFKFields, new int[] { (int)UIScheduleItemFieldIndex.ReportProcessingTaskTemplateId } );		
			_reportProcessingTaskTemplateEntity = null;
		}
		
		/// <summary> setups the sync logic for member _reportProcessingTaskTemplateEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReportProcessingTaskTemplateEntity(IEntityCore relatedEntity)
		{
			if(_reportProcessingTaskTemplateEntity!=relatedEntity)
			{		
				DesetupSyncReportProcessingTaskTemplateEntity(true, true);
				_reportProcessingTaskTemplateEntity = (ReportProcessingTaskTemplateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _reportProcessingTaskTemplateEntity, new PropertyChangedEventHandler( OnReportProcessingTaskTemplateEntityPropertyChanged ), "ReportProcessingTaskTemplateEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateIdStatic, true, ref _alreadyFetchedReportProcessingTaskTemplateEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportProcessingTaskTemplateEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _scheduledMessageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncScheduledMessageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _scheduledMessageEntity, new PropertyChangedEventHandler( OnScheduledMessageEntityPropertyChanged ), "ScheduledMessageEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.ScheduledMessageEntityUsingScheduledMessageIdStatic, true, signalRelatedEntity, "UIScheduleItemCollection", resetFKFields, new int[] { (int)UIScheduleItemFieldIndex.ScheduledMessageId } );		
			_scheduledMessageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _scheduledMessageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncScheduledMessageEntity(IEntityCore relatedEntity)
		{
			if(_scheduledMessageEntity!=relatedEntity)
			{		
				DesetupSyncScheduledMessageEntity(true, true);
				_scheduledMessageEntity = (ScheduledMessageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _scheduledMessageEntity, new PropertyChangedEventHandler( OnScheduledMessageEntityPropertyChanged ), "ScheduledMessageEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.ScheduledMessageEntityUsingScheduledMessageIdStatic, true, ref _alreadyFetchedScheduledMessageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnScheduledMessageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, signalRelatedEntity, "UIScheduleItemCollection", resetFKFields, new int[] { (int)UIScheduleItemFieldIndex.UIScheduleId } );		
			_uIScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIScheduleEntity(IEntityCore relatedEntity)
		{
			if(_uIScheduleEntity!=relatedEntity)
			{		
				DesetupSyncUIScheduleEntity(true, true);
				_uIScheduleEntity = (UIScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, ref _alreadyFetchedUIScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIWidgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, signalRelatedEntity, "UIScheduleItemCollection", resetFKFields, new int[] { (int)UIScheduleItemFieldIndex.UIWidgetId } );		
			_uIWidgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIWidgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIWidgetEntity(IEntityCore relatedEntity)
		{
			if(_uIWidgetEntity!=relatedEntity)
			{		
				DesetupSyncUIWidgetEntity(true, true);
				_uIWidgetEntity = (UIWidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIWidgetEntity, new PropertyChangedEventHandler( OnUIWidgetEntityPropertyChanged ), "UIWidgetEntity", Obymobi.Data.RelationClasses.StaticUIScheduleItemRelations.UIWidgetEntityUsingUIWidgetIdStatic, true, ref _alreadyFetchedUIWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uIScheduleItemId">PK value for UIScheduleItem which data should be fetched into this UIScheduleItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uIScheduleItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UIScheduleItemFieldIndex.UIScheduleItemId].ForcedCurrentValueWrite(uIScheduleItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIScheduleItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UIScheduleItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UIScheduleItemRelations Relations
		{
			get	{ return new UIScheduleItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItemOccurrence' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemOccurrenceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemOccurrenceCollection")[0], (int)Obymobi.Data.EntityType.UIScheduleItemEntity, (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, 0, null, null, null, "UIScheduleItemOccurrenceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportProcessingTaskTemplate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportProcessingTaskTemplateEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReportProcessingTaskTemplateCollection(), (IEntityRelation)GetRelationsForField("ReportProcessingTaskTemplateEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemEntity, (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity, 0, null, null, null, "ReportProcessingTaskTemplateEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UISchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleCollection(), (IEntityRelation)GetRelationsForField("UIScheduleEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemEntity, (int)Obymobi.Data.EntityType.UIScheduleEntity, 0, null, null, null, "UIScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetEntity")[0], (int)Obymobi.Data.EntityType.UIScheduleItemEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UIScheduleItemId property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."UIScheduleItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UIScheduleItemId
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemFieldIndex.UIScheduleItemId, true); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.UIScheduleItemId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The UIScheduleId property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."UIScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UIScheduleId
		{
			get { return (System.Int32)GetValue((int)UIScheduleItemFieldIndex.UIScheduleId, true); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.UIScheduleId, value, true); }
		}

		/// <summary> The UIWidgetId property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."UIWidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIWidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemFieldIndex.UIWidgetId, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.UIWidgetId, value, true); }
		}

		/// <summary> The MediaId property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemFieldIndex.MediaId, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.MediaId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIScheduleItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ScheduledMessageId property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."ScheduledMessageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ScheduledMessageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemFieldIndex.ScheduledMessageId, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.ScheduledMessageId, value, true); }
		}

		/// <summary> The ReportProcessingTaskTemplateId property of the Entity UIScheduleItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UIScheduleItem"."ReportProcessingTaskTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReportProcessingTaskTemplateId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIScheduleItemFieldIndex.ReportProcessingTaskTemplateId, false); }
			set	{ SetValue((int)UIScheduleItemFieldIndex.ReportProcessingTaskTemplateId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemOccurrenceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection UIScheduleItemOccurrenceCollection
		{
			get	{ return GetMultiUIScheduleItemOccurrenceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemOccurrenceCollection. When set to true, UIScheduleItemOccurrenceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemOccurrenceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemOccurrenceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemOccurrenceCollection
		{
			get	{ return _alwaysFetchUIScheduleItemOccurrenceCollection; }
			set	{ _alwaysFetchUIScheduleItemOccurrenceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemOccurrenceCollection already has been fetched. Setting this property to false when UIScheduleItemOccurrenceCollection has been fetched
		/// will clear the UIScheduleItemOccurrenceCollection collection well. Setting this property to true while UIScheduleItemOccurrenceCollection hasn't been fetched disables lazy loading for UIScheduleItemOccurrenceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemOccurrenceCollection
		{
			get { return _alreadyFetchedUIScheduleItemOccurrenceCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemOccurrenceCollection && !value && (_uIScheduleItemOccurrenceCollection != null))
				{
					_uIScheduleItemOccurrenceCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemOccurrenceCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'MediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaEntity MediaEntity
		{
			get	{ return GetSingleMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemCollection", "MediaEntity", _mediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaEntity. When set to true, MediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaEntity
		{
			get	{ return _alwaysFetchMediaEntity; }
			set	{ _alwaysFetchMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaEntity already has been fetched. Setting this property to false when MediaEntity has been fetched
		/// will set MediaEntity to null as well. Setting this property to true while MediaEntity hasn't been fetched disables lazy loading for MediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaEntity
		{
			get { return _alreadyFetchedMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaEntity && !value)
				{
					this.MediaEntity = null;
				}
				_alreadyFetchedMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaEntity is not found
		/// in the database. When set to true, MediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaEntityReturnsNewIfNotFound; }
			set { _mediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportProcessingTaskTemplateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReportProcessingTaskTemplateEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReportProcessingTaskTemplateEntity ReportProcessingTaskTemplateEntity
		{
			get	{ return GetSingleReportProcessingTaskTemplateEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReportProcessingTaskTemplateEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemCollection", "ReportProcessingTaskTemplateEntity", _reportProcessingTaskTemplateEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReportProcessingTaskTemplateEntity. When set to true, ReportProcessingTaskTemplateEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportProcessingTaskTemplateEntity is accessed. You can always execute a forced fetch by calling GetSingleReportProcessingTaskTemplateEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportProcessingTaskTemplateEntity
		{
			get	{ return _alwaysFetchReportProcessingTaskTemplateEntity; }
			set	{ _alwaysFetchReportProcessingTaskTemplateEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportProcessingTaskTemplateEntity already has been fetched. Setting this property to false when ReportProcessingTaskTemplateEntity has been fetched
		/// will set ReportProcessingTaskTemplateEntity to null as well. Setting this property to true while ReportProcessingTaskTemplateEntity hasn't been fetched disables lazy loading for ReportProcessingTaskTemplateEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportProcessingTaskTemplateEntity
		{
			get { return _alreadyFetchedReportProcessingTaskTemplateEntity;}
			set 
			{
				if(_alreadyFetchedReportProcessingTaskTemplateEntity && !value)
				{
					this.ReportProcessingTaskTemplateEntity = null;
				}
				_alreadyFetchedReportProcessingTaskTemplateEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReportProcessingTaskTemplateEntity is not found
		/// in the database. When set to true, ReportProcessingTaskTemplateEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReportProcessingTaskTemplateEntityReturnsNewIfNotFound
		{
			get	{ return _reportProcessingTaskTemplateEntityReturnsNewIfNotFound; }
			set { _reportProcessingTaskTemplateEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ScheduledMessageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleScheduledMessageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ScheduledMessageEntity ScheduledMessageEntity
		{
			get	{ return GetSingleScheduledMessageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncScheduledMessageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemCollection", "ScheduledMessageEntity", _scheduledMessageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageEntity. When set to true, ScheduledMessageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageEntity is accessed. You can always execute a forced fetch by calling GetSingleScheduledMessageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageEntity
		{
			get	{ return _alwaysFetchScheduledMessageEntity; }
			set	{ _alwaysFetchScheduledMessageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageEntity already has been fetched. Setting this property to false when ScheduledMessageEntity has been fetched
		/// will set ScheduledMessageEntity to null as well. Setting this property to true while ScheduledMessageEntity hasn't been fetched disables lazy loading for ScheduledMessageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageEntity
		{
			get { return _alreadyFetchedScheduledMessageEntity;}
			set 
			{
				if(_alreadyFetchedScheduledMessageEntity && !value)
				{
					this.ScheduledMessageEntity = null;
				}
				_alreadyFetchedScheduledMessageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ScheduledMessageEntity is not found
		/// in the database. When set to true, ScheduledMessageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ScheduledMessageEntityReturnsNewIfNotFound
		{
			get	{ return _scheduledMessageEntityReturnsNewIfNotFound; }
			set { _scheduledMessageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIScheduleEntity UIScheduleEntity
		{
			get	{ return GetSingleUIScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemCollection", "UIScheduleEntity", _uIScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleEntity. When set to true, UIScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleEntity is accessed. You can always execute a forced fetch by calling GetSingleUIScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleEntity
		{
			get	{ return _alwaysFetchUIScheduleEntity; }
			set	{ _alwaysFetchUIScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleEntity already has been fetched. Setting this property to false when UIScheduleEntity has been fetched
		/// will set UIScheduleEntity to null as well. Setting this property to true while UIScheduleEntity hasn't been fetched disables lazy loading for UIScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleEntity
		{
			get { return _alreadyFetchedUIScheduleEntity;}
			set 
			{
				if(_alreadyFetchedUIScheduleEntity && !value)
				{
					this.UIScheduleEntity = null;
				}
				_alreadyFetchedUIScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIScheduleEntity is not found
		/// in the database. When set to true, UIScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _uIScheduleEntityReturnsNewIfNotFound; }
			set { _uIScheduleEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIWidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIWidgetEntity UIWidgetEntity
		{
			get	{ return GetSingleUIWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIScheduleItemCollection", "UIWidgetEntity", _uIWidgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetEntity. When set to true, UIWidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleUIWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetEntity
		{
			get	{ return _alwaysFetchUIWidgetEntity; }
			set	{ _alwaysFetchUIWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetEntity already has been fetched. Setting this property to false when UIWidgetEntity has been fetched
		/// will set UIWidgetEntity to null as well. Setting this property to true while UIWidgetEntity hasn't been fetched disables lazy loading for UIWidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetEntity
		{
			get { return _alreadyFetchedUIWidgetEntity;}
			set 
			{
				if(_alreadyFetchedUIWidgetEntity && !value)
				{
					this.UIWidgetEntity = null;
				}
				_alreadyFetchedUIWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIWidgetEntity is not found
		/// in the database. When set to true, UIWidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIWidgetEntityReturnsNewIfNotFound
		{
			get	{ return _uIWidgetEntityReturnsNewIfNotFound; }
			set { _uIWidgetEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UIScheduleItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
