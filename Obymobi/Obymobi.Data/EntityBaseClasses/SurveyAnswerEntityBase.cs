﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SurveyAnswer'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SurveyAnswerEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SurveyAnswerEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection	_surveyAnswerLanguageCollection;
		private bool	_alwaysFetchSurveyAnswerLanguageCollection, _alreadyFetchedSurveyAnswerLanguageCollection;
		private Obymobi.Data.CollectionClasses.SurveyResultCollection	_surveyResultCollection;
		private bool	_alwaysFetchSurveyResultCollection, _alreadyFetchedSurveyResultCollection;
		private SurveyQuestionEntity _surveyQuestionEntity;
		private bool	_alwaysFetchSurveyQuestionEntity, _alreadyFetchedSurveyQuestionEntity, _surveyQuestionEntityReturnsNewIfNotFound;
		private SurveyQuestionEntity _targetSurveyQuestionEntity;
		private bool	_alwaysFetchTargetSurveyQuestionEntity, _alreadyFetchedTargetSurveyQuestionEntity, _targetSurveyQuestionEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SurveyQuestionEntity</summary>
			public static readonly string SurveyQuestionEntity = "SurveyQuestionEntity";
			/// <summary>Member name TargetSurveyQuestionEntity</summary>
			public static readonly string TargetSurveyQuestionEntity = "TargetSurveyQuestionEntity";
			/// <summary>Member name SurveyAnswerLanguageCollection</summary>
			public static readonly string SurveyAnswerLanguageCollection = "SurveyAnswerLanguageCollection";
			/// <summary>Member name SurveyResultCollection</summary>
			public static readonly string SurveyResultCollection = "SurveyResultCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyAnswerEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SurveyAnswerEntityBase() :base("SurveyAnswerEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		protected SurveyAnswerEntityBase(System.Int32 surveyAnswerId):base("SurveyAnswerEntity")
		{
			InitClassFetch(surveyAnswerId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SurveyAnswerEntityBase(System.Int32 surveyAnswerId, IPrefetchPath prefetchPathToUse): base("SurveyAnswerEntity")
		{
			InitClassFetch(surveyAnswerId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <param name="validator">The custom validator object for this SurveyAnswerEntity</param>
		protected SurveyAnswerEntityBase(System.Int32 surveyAnswerId, IValidator validator):base("SurveyAnswerEntity")
		{
			InitClassFetch(surveyAnswerId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyAnswerEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_surveyAnswerLanguageCollection = (Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection)info.GetValue("_surveyAnswerLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection));
			_alwaysFetchSurveyAnswerLanguageCollection = info.GetBoolean("_alwaysFetchSurveyAnswerLanguageCollection");
			_alreadyFetchedSurveyAnswerLanguageCollection = info.GetBoolean("_alreadyFetchedSurveyAnswerLanguageCollection");

			_surveyResultCollection = (Obymobi.Data.CollectionClasses.SurveyResultCollection)info.GetValue("_surveyResultCollection", typeof(Obymobi.Data.CollectionClasses.SurveyResultCollection));
			_alwaysFetchSurveyResultCollection = info.GetBoolean("_alwaysFetchSurveyResultCollection");
			_alreadyFetchedSurveyResultCollection = info.GetBoolean("_alreadyFetchedSurveyResultCollection");
			_surveyQuestionEntity = (SurveyQuestionEntity)info.GetValue("_surveyQuestionEntity", typeof(SurveyQuestionEntity));
			if(_surveyQuestionEntity!=null)
			{
				_surveyQuestionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyQuestionEntityReturnsNewIfNotFound = info.GetBoolean("_surveyQuestionEntityReturnsNewIfNotFound");
			_alwaysFetchSurveyQuestionEntity = info.GetBoolean("_alwaysFetchSurveyQuestionEntity");
			_alreadyFetchedSurveyQuestionEntity = info.GetBoolean("_alreadyFetchedSurveyQuestionEntity");

			_targetSurveyQuestionEntity = (SurveyQuestionEntity)info.GetValue("_targetSurveyQuestionEntity", typeof(SurveyQuestionEntity));
			if(_targetSurveyQuestionEntity!=null)
			{
				_targetSurveyQuestionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_targetSurveyQuestionEntityReturnsNewIfNotFound = info.GetBoolean("_targetSurveyQuestionEntityReturnsNewIfNotFound");
			_alwaysFetchTargetSurveyQuestionEntity = info.GetBoolean("_alwaysFetchTargetSurveyQuestionEntity");
			_alreadyFetchedTargetSurveyQuestionEntity = info.GetBoolean("_alreadyFetchedTargetSurveyQuestionEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyAnswerFieldIndex)fieldIndex)
			{
				case SurveyAnswerFieldIndex.SurveyQuestionId:
					DesetupSyncSurveyQuestionEntity(true, false);
					_alreadyFetchedSurveyQuestionEntity = false;
					break;
				case SurveyAnswerFieldIndex.TargetSurveyQuestionId:
					DesetupSyncTargetSurveyQuestionEntity(true, false);
					_alreadyFetchedTargetSurveyQuestionEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSurveyAnswerLanguageCollection = (_surveyAnswerLanguageCollection.Count > 0);
			_alreadyFetchedSurveyResultCollection = (_surveyResultCollection.Count > 0);
			_alreadyFetchedSurveyQuestionEntity = (_surveyQuestionEntity != null);
			_alreadyFetchedTargetSurveyQuestionEntity = (_targetSurveyQuestionEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SurveyQuestionEntity":
					toReturn.Add(Relations.SurveyQuestionEntityUsingSurveyQuestionId);
					break;
				case "TargetSurveyQuestionEntity":
					toReturn.Add(Relations.SurveyQuestionEntityUsingTargetSurveyQuestionId);
					break;
				case "SurveyAnswerLanguageCollection":
					toReturn.Add(Relations.SurveyAnswerLanguageEntityUsingSurveyAnswerId);
					break;
				case "SurveyResultCollection":
					toReturn.Add(Relations.SurveyResultEntityUsingSurveyAnswerId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_surveyAnswerLanguageCollection", (!this.MarkedForDeletion?_surveyAnswerLanguageCollection:null));
			info.AddValue("_alwaysFetchSurveyAnswerLanguageCollection", _alwaysFetchSurveyAnswerLanguageCollection);
			info.AddValue("_alreadyFetchedSurveyAnswerLanguageCollection", _alreadyFetchedSurveyAnswerLanguageCollection);
			info.AddValue("_surveyResultCollection", (!this.MarkedForDeletion?_surveyResultCollection:null));
			info.AddValue("_alwaysFetchSurveyResultCollection", _alwaysFetchSurveyResultCollection);
			info.AddValue("_alreadyFetchedSurveyResultCollection", _alreadyFetchedSurveyResultCollection);
			info.AddValue("_surveyQuestionEntity", (!this.MarkedForDeletion?_surveyQuestionEntity:null));
			info.AddValue("_surveyQuestionEntityReturnsNewIfNotFound", _surveyQuestionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyQuestionEntity", _alwaysFetchSurveyQuestionEntity);
			info.AddValue("_alreadyFetchedSurveyQuestionEntity", _alreadyFetchedSurveyQuestionEntity);
			info.AddValue("_targetSurveyQuestionEntity", (!this.MarkedForDeletion?_targetSurveyQuestionEntity:null));
			info.AddValue("_targetSurveyQuestionEntityReturnsNewIfNotFound", _targetSurveyQuestionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTargetSurveyQuestionEntity", _alwaysFetchTargetSurveyQuestionEntity);
			info.AddValue("_alreadyFetchedTargetSurveyQuestionEntity", _alreadyFetchedTargetSurveyQuestionEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SurveyQuestionEntity":
					_alreadyFetchedSurveyQuestionEntity = true;
					this.SurveyQuestionEntity = (SurveyQuestionEntity)entity;
					break;
				case "TargetSurveyQuestionEntity":
					_alreadyFetchedTargetSurveyQuestionEntity = true;
					this.TargetSurveyQuestionEntity = (SurveyQuestionEntity)entity;
					break;
				case "SurveyAnswerLanguageCollection":
					_alreadyFetchedSurveyAnswerLanguageCollection = true;
					if(entity!=null)
					{
						this.SurveyAnswerLanguageCollection.Add((SurveyAnswerLanguageEntity)entity);
					}
					break;
				case "SurveyResultCollection":
					_alreadyFetchedSurveyResultCollection = true;
					if(entity!=null)
					{
						this.SurveyResultCollection.Add((SurveyResultEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SurveyQuestionEntity":
					SetupSyncSurveyQuestionEntity(relatedEntity);
					break;
				case "TargetSurveyQuestionEntity":
					SetupSyncTargetSurveyQuestionEntity(relatedEntity);
					break;
				case "SurveyAnswerLanguageCollection":
					_surveyAnswerLanguageCollection.Add((SurveyAnswerLanguageEntity)relatedEntity);
					break;
				case "SurveyResultCollection":
					_surveyResultCollection.Add((SurveyResultEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SurveyQuestionEntity":
					DesetupSyncSurveyQuestionEntity(false, true);
					break;
				case "TargetSurveyQuestionEntity":
					DesetupSyncTargetSurveyQuestionEntity(false, true);
					break;
				case "SurveyAnswerLanguageCollection":
					this.PerformRelatedEntityRemoval(_surveyAnswerLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyResultCollection":
					this.PerformRelatedEntityRemoval(_surveyResultCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_surveyQuestionEntity!=null)
			{
				toReturn.Add(_surveyQuestionEntity);
			}
			if(_targetSurveyQuestionEntity!=null)
			{
				toReturn.Add(_targetSurveyQuestionEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_surveyAnswerLanguageCollection);
			toReturn.Add(_surveyResultCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyAnswerId)
		{
			return FetchUsingPK(surveyAnswerId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyAnswerId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyAnswerId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyAnswerId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyAnswerId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyAnswerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyAnswerId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyAnswerId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyAnswerRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch)
		{
			return GetMultiSurveyAnswerLanguageCollection(forceFetch, _surveyAnswerLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyAnswerLanguageCollection(forceFetch, _surveyAnswerLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyAnswerLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyAnswerLanguageCollection || forceFetch || _alwaysFetchSurveyAnswerLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyAnswerLanguageCollection);
				_surveyAnswerLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyAnswerLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyAnswerLanguageCollection.GetMultiManyToOne(null, this, filter);
				_surveyAnswerLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyAnswerLanguageCollection = true;
			}
			return _surveyAnswerLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyAnswerLanguageCollection'. These settings will be taken into account
		/// when the property SurveyAnswerLanguageCollection is requested or GetMultiSurveyAnswerLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyAnswerLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyAnswerLanguageCollection.SortClauses=sortClauses;
			_surveyAnswerLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyResultCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyResultCollection || forceFetch || _alwaysFetchSurveyResultCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyResultCollection);
				_surveyResultCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyResultCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyResultCollection.GetMultiManyToOne(null, null, this, null, filter);
				_surveyResultCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyResultCollection = true;
			}
			return _surveyResultCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyResultCollection'. These settings will be taken into account
		/// when the property SurveyResultCollection is requested or GetMultiSurveyResultCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyResultCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyResultCollection.SortClauses=sortClauses;
			_surveyResultCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public SurveyQuestionEntity GetSingleSurveyQuestionEntity()
		{
			return GetSingleSurveyQuestionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public virtual SurveyQuestionEntity GetSingleSurveyQuestionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyQuestionEntity || forceFetch || _alwaysFetchSurveyQuestionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyQuestionEntityUsingSurveyQuestionId);
				SurveyQuestionEntity newEntity = new SurveyQuestionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyQuestionId);
				}
				if(fetchResult)
				{
					newEntity = (SurveyQuestionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyQuestionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyQuestionEntity = newEntity;
				_alreadyFetchedSurveyQuestionEntity = fetchResult;
			}
			return _surveyQuestionEntity;
		}


		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public SurveyQuestionEntity GetSingleTargetSurveyQuestionEntity()
		{
			return GetSingleTargetSurveyQuestionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public virtual SurveyQuestionEntity GetSingleTargetSurveyQuestionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTargetSurveyQuestionEntity || forceFetch || _alwaysFetchTargetSurveyQuestionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyQuestionEntityUsingTargetSurveyQuestionId);
				SurveyQuestionEntity newEntity = new SurveyQuestionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TargetSurveyQuestionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyQuestionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_targetSurveyQuestionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TargetSurveyQuestionEntity = newEntity;
				_alreadyFetchedTargetSurveyQuestionEntity = fetchResult;
			}
			return _targetSurveyQuestionEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SurveyQuestionEntity", _surveyQuestionEntity);
			toReturn.Add("TargetSurveyQuestionEntity", _targetSurveyQuestionEntity);
			toReturn.Add("SurveyAnswerLanguageCollection", _surveyAnswerLanguageCollection);
			toReturn.Add("SurveyResultCollection", _surveyResultCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <param name="validator">The validator object for this SurveyAnswerEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 surveyAnswerId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyAnswerId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_surveyAnswerLanguageCollection = new Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection();
			_surveyAnswerLanguageCollection.SetContainingEntityInfo(this, "SurveyAnswerEntity");

			_surveyResultCollection = new Obymobi.Data.CollectionClasses.SurveyResultCollection();
			_surveyResultCollection.SetContainingEntityInfo(this, "SurveyAnswerEntity");
			_surveyQuestionEntityReturnsNewIfNotFound = true;
			_targetSurveyQuestionEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyAnswerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyQuestionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Answer", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TargetSurveyQuestionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _surveyQuestionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyQuestionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyQuestionEntity, new PropertyChangedEventHandler( OnSurveyQuestionEntityPropertyChanged ), "SurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyAnswerRelations.SurveyQuestionEntityUsingSurveyQuestionIdStatic, true, signalRelatedEntity, "SurveyAnswerCollection", resetFKFields, new int[] { (int)SurveyAnswerFieldIndex.SurveyQuestionId } );		
			_surveyQuestionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _surveyQuestionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyQuestionEntity(IEntityCore relatedEntity)
		{
			if(_surveyQuestionEntity!=relatedEntity)
			{		
				DesetupSyncSurveyQuestionEntity(true, true);
				_surveyQuestionEntity = (SurveyQuestionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyQuestionEntity, new PropertyChangedEventHandler( OnSurveyQuestionEntityPropertyChanged ), "SurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyAnswerRelations.SurveyQuestionEntityUsingSurveyQuestionIdStatic, true, ref _alreadyFetchedSurveyQuestionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyQuestionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _targetSurveyQuestionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTargetSurveyQuestionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _targetSurveyQuestionEntity, new PropertyChangedEventHandler( OnTargetSurveyQuestionEntityPropertyChanged ), "TargetSurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyAnswerRelations.SurveyQuestionEntityUsingTargetSurveyQuestionIdStatic, true, signalRelatedEntity, "SurveyAnswerTargetCollection", resetFKFields, new int[] { (int)SurveyAnswerFieldIndex.TargetSurveyQuestionId } );		
			_targetSurveyQuestionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _targetSurveyQuestionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTargetSurveyQuestionEntity(IEntityCore relatedEntity)
		{
			if(_targetSurveyQuestionEntity!=relatedEntity)
			{		
				DesetupSyncTargetSurveyQuestionEntity(true, true);
				_targetSurveyQuestionEntity = (SurveyQuestionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _targetSurveyQuestionEntity, new PropertyChangedEventHandler( OnTargetSurveyQuestionEntityPropertyChanged ), "TargetSurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyAnswerRelations.SurveyQuestionEntityUsingTargetSurveyQuestionIdStatic, true, ref _alreadyFetchedTargetSurveyQuestionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTargetSurveyQuestionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyAnswerId">PK value for SurveyAnswer which data should be fetched into this SurveyAnswer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 surveyAnswerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyAnswerFieldIndex.SurveyAnswerId].ForcedCurrentValueWrite(surveyAnswerId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyAnswerDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyAnswerEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyAnswerRelations Relations
		{
			get	{ return new SurveyAnswerRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswerLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswerLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswerLanguageCollection")[0], (int)Obymobi.Data.EntityType.SurveyAnswerEntity, (int)Obymobi.Data.EntityType.SurveyAnswerLanguageEntity, 0, null, null, null, "SurveyAnswerLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyResultCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyResultCollection(), (IEntityRelation)GetRelationsForField("SurveyResultCollection")[0], (int)Obymobi.Data.EntityType.SurveyAnswerEntity, (int)Obymobi.Data.EntityType.SurveyResultEntity, 0, null, null, null, "SurveyResultCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestionEntity")[0], (int)Obymobi.Data.EntityType.SurveyAnswerEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, null, "SurveyQuestionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTargetSurveyQuestionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("TargetSurveyQuestionEntity")[0], (int)Obymobi.Data.EntityType.SurveyAnswerEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, null, "TargetSurveyQuestionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SurveyAnswerId property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."SurveyAnswerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SurveyAnswerId
		{
			get { return (System.Int32)GetValue((int)SurveyAnswerFieldIndex.SurveyAnswerId, true); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.SurveyAnswerId, value, true); }
		}

		/// <summary> The SurveyQuestionId property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."SurveyQuestionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SurveyQuestionId
		{
			get { return (System.Int32)GetValue((int)SurveyAnswerFieldIndex.SurveyQuestionId, true); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.SurveyQuestionId, value, true); }
		}

		/// <summary> The Answer property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."Answer"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Answer
		{
			get { return (System.String)GetValue((int)SurveyAnswerFieldIndex.Answer, true); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.Answer, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyAnswerFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyAnswerFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The SortOrder property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)SurveyAnswerFieldIndex.SortOrder, true); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The TargetSurveyQuestionId property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."TargetSurveyQuestionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TargetSurveyQuestionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyAnswerFieldIndex.TargetSurveyQuestionId, false); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.TargetSurveyQuestionId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)SurveyAnswerFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyAnswerFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SurveyAnswer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyAnswer"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyAnswerFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SurveyAnswerFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyAnswerLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection SurveyAnswerLanguageCollection
		{
			get	{ return GetMultiSurveyAnswerLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswerLanguageCollection. When set to true, SurveyAnswerLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswerLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyAnswerLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswerLanguageCollection
		{
			get	{ return _alwaysFetchSurveyAnswerLanguageCollection; }
			set	{ _alwaysFetchSurveyAnswerLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswerLanguageCollection already has been fetched. Setting this property to false when SurveyAnswerLanguageCollection has been fetched
		/// will clear the SurveyAnswerLanguageCollection collection well. Setting this property to true while SurveyAnswerLanguageCollection hasn't been fetched disables lazy loading for SurveyAnswerLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswerLanguageCollection
		{
			get { return _alreadyFetchedSurveyAnswerLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSurveyAnswerLanguageCollection && !value && (_surveyAnswerLanguageCollection != null))
				{
					_surveyAnswerLanguageCollection.Clear();
				}
				_alreadyFetchedSurveyAnswerLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyResultCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection SurveyResultCollection
		{
			get	{ return GetMultiSurveyResultCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyResultCollection. When set to true, SurveyResultCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyResultCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyResultCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyResultCollection
		{
			get	{ return _alwaysFetchSurveyResultCollection; }
			set	{ _alwaysFetchSurveyResultCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyResultCollection already has been fetched. Setting this property to false when SurveyResultCollection has been fetched
		/// will clear the SurveyResultCollection collection well. Setting this property to true while SurveyResultCollection hasn't been fetched disables lazy loading for SurveyResultCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyResultCollection
		{
			get { return _alreadyFetchedSurveyResultCollection;}
			set 
			{
				if(_alreadyFetchedSurveyResultCollection && !value && (_surveyResultCollection != null))
				{
					_surveyResultCollection.Clear();
				}
				_alreadyFetchedSurveyResultCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SurveyQuestionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyQuestionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyQuestionEntity SurveyQuestionEntity
		{
			get	{ return GetSingleSurveyQuestionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyQuestionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyAnswerCollection", "SurveyQuestionEntity", _surveyQuestionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionEntity. When set to true, SurveyQuestionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionEntity is accessed. You can always execute a forced fetch by calling GetSingleSurveyQuestionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionEntity
		{
			get	{ return _alwaysFetchSurveyQuestionEntity; }
			set	{ _alwaysFetchSurveyQuestionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionEntity already has been fetched. Setting this property to false when SurveyQuestionEntity has been fetched
		/// will set SurveyQuestionEntity to null as well. Setting this property to true while SurveyQuestionEntity hasn't been fetched disables lazy loading for SurveyQuestionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionEntity
		{
			get { return _alreadyFetchedSurveyQuestionEntity;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionEntity && !value)
				{
					this.SurveyQuestionEntity = null;
				}
				_alreadyFetchedSurveyQuestionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyQuestionEntity is not found
		/// in the database. When set to true, SurveyQuestionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SurveyQuestionEntityReturnsNewIfNotFound
		{
			get	{ return _surveyQuestionEntityReturnsNewIfNotFound; }
			set { _surveyQuestionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyQuestionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTargetSurveyQuestionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyQuestionEntity TargetSurveyQuestionEntity
		{
			get	{ return GetSingleTargetSurveyQuestionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTargetSurveyQuestionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyAnswerTargetCollection", "TargetSurveyQuestionEntity", _targetSurveyQuestionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TargetSurveyQuestionEntity. When set to true, TargetSurveyQuestionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TargetSurveyQuestionEntity is accessed. You can always execute a forced fetch by calling GetSingleTargetSurveyQuestionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTargetSurveyQuestionEntity
		{
			get	{ return _alwaysFetchTargetSurveyQuestionEntity; }
			set	{ _alwaysFetchTargetSurveyQuestionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TargetSurveyQuestionEntity already has been fetched. Setting this property to false when TargetSurveyQuestionEntity has been fetched
		/// will set TargetSurveyQuestionEntity to null as well. Setting this property to true while TargetSurveyQuestionEntity hasn't been fetched disables lazy loading for TargetSurveyQuestionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTargetSurveyQuestionEntity
		{
			get { return _alreadyFetchedTargetSurveyQuestionEntity;}
			set 
			{
				if(_alreadyFetchedTargetSurveyQuestionEntity && !value)
				{
					this.TargetSurveyQuestionEntity = null;
				}
				_alreadyFetchedTargetSurveyQuestionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TargetSurveyQuestionEntity is not found
		/// in the database. When set to true, TargetSurveyQuestionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TargetSurveyQuestionEntityReturnsNewIfNotFound
		{
			get	{ return _targetSurveyQuestionEntityReturnsNewIfNotFound; }
			set { _targetSurveyQuestionEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SurveyAnswerEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
