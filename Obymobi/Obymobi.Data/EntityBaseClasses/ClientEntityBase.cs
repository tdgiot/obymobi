﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Client'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ClientEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ClientEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientEntertainmentCollection	_clientEntertainmentCollection;
		private bool	_alwaysFetchClientEntertainmentCollection, _alreadyFetchedClientEntertainmentCollection;
		private Obymobi.Data.CollectionClasses.ClientLogCollection	_clientLogCollection;
		private bool	_alwaysFetchClientLogCollection, _alreadyFetchedClientLogCollection;
		private Obymobi.Data.CollectionClasses.ClientStateCollection	_clientStateCollection;
		private bool	_alwaysFetchClientStateCollection, _alreadyFetchedClientStateCollection;
		private Obymobi.Data.CollectionClasses.CustomerCollection	_customerCollection;
		private bool	_alwaysFetchCustomerCollection, _alreadyFetchedCustomerCollection;
		private Obymobi.Data.CollectionClasses.GameSessionCollection	_gameSessionCollection;
		private bool	_alwaysFetchGameSessionCollection, _alreadyFetchedGameSessionCollection;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessageRecipientCollection	_messageRecipientCollection;
		private bool	_alwaysFetchMessageRecipientCollection, _alreadyFetchedMessageRecipientCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_receivedNetmessageCollection;
		private bool	_alwaysFetchReceivedNetmessageCollection, _alreadyFetchedReceivedNetmessageCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_sentNetmessageCollection;
		private bool	_alwaysFetchSentNetmessageCollection, _alreadyFetchedSentNetmessageCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private Obymobi.Data.CollectionClasses.ScheduledCommandCollection	_scheduledCommandCollection;
		private bool	_alwaysFetchScheduledCommandCollection, _alreadyFetchedScheduledCommandCollection;
		private Obymobi.Data.CollectionClasses.SurveyResultCollection	_surveyResultCollection;
		private bool	_alwaysFetchSurveyResultCollection, _alreadyFetchedSurveyResultCollection;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMessage;
		private bool	_alwaysFetchCategoryCollectionViaMessage, _alreadyFetchedCategoryCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.ClientLogFileCollection _clientLogFileCollectionViaClientLog;
		private bool	_alwaysFetchClientLogFileCollectionViaClientLog, _alreadyFetchedClientLogFileCollectionViaClientLog;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaMessage;
		private bool	_alwaysFetchDeliverypointCollectionViaMessage, _alreadyFetchedDeliverypointCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaNetmessage;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaNetmessage, _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessage;
		private bool	_alwaysFetchEntertainmentCollectionViaMessage, _alreadyFetchedEntertainmentCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaMessage;
		private bool	_alwaysFetchMediaCollectionViaMessage, _alreadyFetchedMediaCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrder;
		private bool	_alwaysFetchOrderCollectionViaOrder, _alreadyFetchedOrderCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage, _alreadyFetchedTerminalCollectionViaNetmessage;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointEntity _deliverypointEntity;
		private bool	_alwaysFetchDeliverypointEntity, _alreadyFetchedDeliverypointEntity, _deliverypointEntityReturnsNewIfNotFound;
		private DeliverypointEntity _lastDeliverypointEntity;
		private bool	_alwaysFetchLastDeliverypointEntity, _alreadyFetchedLastDeliverypointEntity, _lastDeliverypointEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private DeviceEntity _deviceEntity;
		private bool	_alwaysFetchDeviceEntity, _alreadyFetchedDeviceEntity, _deviceEntityReturnsNewIfNotFound;
		private RoomControlAreaEntity _roomControlAreaEntity;
		private bool	_alwaysFetchRoomControlAreaEntity, _alreadyFetchedRoomControlAreaEntity, _roomControlAreaEntityReturnsNewIfNotFound;
		private TimestampEntity _timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection, _timestampCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointEntity</summary>
			public static readonly string DeliverypointEntity = "DeliverypointEntity";
			/// <summary>Member name LastDeliverypointEntity</summary>
			public static readonly string LastDeliverypointEntity = "LastDeliverypointEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name DeviceEntity</summary>
			public static readonly string DeviceEntity = "DeviceEntity";
			/// <summary>Member name RoomControlAreaEntity</summary>
			public static readonly string RoomControlAreaEntity = "RoomControlAreaEntity";
			/// <summary>Member name ClientEntertainmentCollection</summary>
			public static readonly string ClientEntertainmentCollection = "ClientEntertainmentCollection";
			/// <summary>Member name ClientLogCollection</summary>
			public static readonly string ClientLogCollection = "ClientLogCollection";
			/// <summary>Member name ClientStateCollection</summary>
			public static readonly string ClientStateCollection = "ClientStateCollection";
			/// <summary>Member name CustomerCollection</summary>
			public static readonly string CustomerCollection = "CustomerCollection";
			/// <summary>Member name GameSessionCollection</summary>
			public static readonly string GameSessionCollection = "GameSessionCollection";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessageRecipientCollection</summary>
			public static readonly string MessageRecipientCollection = "MessageRecipientCollection";
			/// <summary>Member name ReceivedNetmessageCollection</summary>
			public static readonly string ReceivedNetmessageCollection = "ReceivedNetmessageCollection";
			/// <summary>Member name SentNetmessageCollection</summary>
			public static readonly string SentNetmessageCollection = "SentNetmessageCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name ScheduledCommandCollection</summary>
			public static readonly string ScheduledCommandCollection = "ScheduledCommandCollection";
			/// <summary>Member name SurveyResultCollection</summary>
			public static readonly string SurveyResultCollection = "SurveyResultCollection";
			/// <summary>Member name CategoryCollectionViaMessage</summary>
			public static readonly string CategoryCollectionViaMessage = "CategoryCollectionViaMessage";
			/// <summary>Member name ClientLogFileCollectionViaClientLog</summary>
			public static readonly string ClientLogFileCollectionViaClientLog = "ClientLogFileCollectionViaClientLog";
			/// <summary>Member name DeliverypointCollectionViaMessage</summary>
			public static readonly string DeliverypointCollectionViaMessage = "DeliverypointCollectionViaMessage";
			/// <summary>Member name DeliverypointgroupCollectionViaNetmessage</summary>
			public static readonly string DeliverypointgroupCollectionViaNetmessage = "DeliverypointgroupCollectionViaNetmessage";
			/// <summary>Member name EntertainmentCollectionViaMessage</summary>
			public static readonly string EntertainmentCollectionViaMessage = "EntertainmentCollectionViaMessage";
			/// <summary>Member name MediaCollectionViaMessage</summary>
			public static readonly string MediaCollectionViaMessage = "MediaCollectionViaMessage";
			/// <summary>Member name OrderCollectionViaOrder</summary>
			public static readonly string OrderCollectionViaOrder = "OrderCollectionViaOrder";
			/// <summary>Member name TerminalCollectionViaNetmessage</summary>
			public static readonly string TerminalCollectionViaNetmessage = "TerminalCollectionViaNetmessage";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClientEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ClientEntityBase() :base("ClientEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		protected ClientEntityBase(System.Int32 clientId):base("ClientEntity")
		{
			InitClassFetch(clientId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ClientEntityBase(System.Int32 clientId, IPrefetchPath prefetchPathToUse): base("ClientEntity")
		{
			InitClassFetch(clientId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="validator">The custom validator object for this ClientEntity</param>
		protected ClientEntityBase(System.Int32 clientId, IValidator validator):base("ClientEntity")
		{
			InitClassFetch(clientId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientEntertainmentCollection = (Obymobi.Data.CollectionClasses.ClientEntertainmentCollection)info.GetValue("_clientEntertainmentCollection", typeof(Obymobi.Data.CollectionClasses.ClientEntertainmentCollection));
			_alwaysFetchClientEntertainmentCollection = info.GetBoolean("_alwaysFetchClientEntertainmentCollection");
			_alreadyFetchedClientEntertainmentCollection = info.GetBoolean("_alreadyFetchedClientEntertainmentCollection");

			_clientLogCollection = (Obymobi.Data.CollectionClasses.ClientLogCollection)info.GetValue("_clientLogCollection", typeof(Obymobi.Data.CollectionClasses.ClientLogCollection));
			_alwaysFetchClientLogCollection = info.GetBoolean("_alwaysFetchClientLogCollection");
			_alreadyFetchedClientLogCollection = info.GetBoolean("_alreadyFetchedClientLogCollection");

			_clientStateCollection = (Obymobi.Data.CollectionClasses.ClientStateCollection)info.GetValue("_clientStateCollection", typeof(Obymobi.Data.CollectionClasses.ClientStateCollection));
			_alwaysFetchClientStateCollection = info.GetBoolean("_alwaysFetchClientStateCollection");
			_alreadyFetchedClientStateCollection = info.GetBoolean("_alreadyFetchedClientStateCollection");

			_customerCollection = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollection", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollection = info.GetBoolean("_alwaysFetchCustomerCollection");
			_alreadyFetchedCustomerCollection = info.GetBoolean("_alreadyFetchedCustomerCollection");

			_gameSessionCollection = (Obymobi.Data.CollectionClasses.GameSessionCollection)info.GetValue("_gameSessionCollection", typeof(Obymobi.Data.CollectionClasses.GameSessionCollection));
			_alwaysFetchGameSessionCollection = info.GetBoolean("_alwaysFetchGameSessionCollection");
			_alreadyFetchedGameSessionCollection = info.GetBoolean("_alreadyFetchedGameSessionCollection");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messageRecipientCollection = (Obymobi.Data.CollectionClasses.MessageRecipientCollection)info.GetValue("_messageRecipientCollection", typeof(Obymobi.Data.CollectionClasses.MessageRecipientCollection));
			_alwaysFetchMessageRecipientCollection = info.GetBoolean("_alwaysFetchMessageRecipientCollection");
			_alreadyFetchedMessageRecipientCollection = info.GetBoolean("_alreadyFetchedMessageRecipientCollection");

			_receivedNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_receivedNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchReceivedNetmessageCollection = info.GetBoolean("_alwaysFetchReceivedNetmessageCollection");
			_alreadyFetchedReceivedNetmessageCollection = info.GetBoolean("_alreadyFetchedReceivedNetmessageCollection");

			_sentNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_sentNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchSentNetmessageCollection = info.GetBoolean("_alwaysFetchSentNetmessageCollection");
			_alreadyFetchedSentNetmessageCollection = info.GetBoolean("_alreadyFetchedSentNetmessageCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");

			_scheduledCommandCollection = (Obymobi.Data.CollectionClasses.ScheduledCommandCollection)info.GetValue("_scheduledCommandCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledCommandCollection));
			_alwaysFetchScheduledCommandCollection = info.GetBoolean("_alwaysFetchScheduledCommandCollection");
			_alreadyFetchedScheduledCommandCollection = info.GetBoolean("_alreadyFetchedScheduledCommandCollection");

			_surveyResultCollection = (Obymobi.Data.CollectionClasses.SurveyResultCollection)info.GetValue("_surveyResultCollection", typeof(Obymobi.Data.CollectionClasses.SurveyResultCollection));
			_alwaysFetchSurveyResultCollection = info.GetBoolean("_alwaysFetchSurveyResultCollection");
			_alreadyFetchedSurveyResultCollection = info.GetBoolean("_alreadyFetchedSurveyResultCollection");
			_categoryCollectionViaMessage = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMessage = info.GetBoolean("_alwaysFetchCategoryCollectionViaMessage");
			_alreadyFetchedCategoryCollectionViaMessage = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMessage");

			_clientLogFileCollectionViaClientLog = (Obymobi.Data.CollectionClasses.ClientLogFileCollection)info.GetValue("_clientLogFileCollectionViaClientLog", typeof(Obymobi.Data.CollectionClasses.ClientLogFileCollection));
			_alwaysFetchClientLogFileCollectionViaClientLog = info.GetBoolean("_alwaysFetchClientLogFileCollectionViaClientLog");
			_alreadyFetchedClientLogFileCollectionViaClientLog = info.GetBoolean("_alreadyFetchedClientLogFileCollectionViaClientLog");

			_deliverypointCollectionViaMessage = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaMessage = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaMessage");
			_alreadyFetchedDeliverypointCollectionViaMessage = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaMessage");

			_deliverypointgroupCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaNetmessage");
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage");

			_entertainmentCollectionViaMessage = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessage = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessage");
			_alreadyFetchedEntertainmentCollectionViaMessage = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessage");

			_mediaCollectionViaMessage = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaMessage = info.GetBoolean("_alwaysFetchMediaCollectionViaMessage");
			_alreadyFetchedMediaCollectionViaMessage = info.GetBoolean("_alreadyFetchedMediaCollectionViaMessage");

			_orderCollectionViaOrder = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrder = info.GetBoolean("_alwaysFetchOrderCollectionViaOrder");
			_alreadyFetchedOrderCollectionViaOrder = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrder");

			_terminalCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage");
			_alreadyFetchedTerminalCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointEntity = (DeliverypointEntity)info.GetValue("_deliverypointEntity", typeof(DeliverypointEntity));
			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity = info.GetBoolean("_alwaysFetchDeliverypointEntity");
			_alreadyFetchedDeliverypointEntity = info.GetBoolean("_alreadyFetchedDeliverypointEntity");

			_lastDeliverypointEntity = (DeliverypointEntity)info.GetValue("_lastDeliverypointEntity", typeof(DeliverypointEntity));
			if(_lastDeliverypointEntity!=null)
			{
				_lastDeliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_lastDeliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_lastDeliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchLastDeliverypointEntity = info.GetBoolean("_alwaysFetchLastDeliverypointEntity");
			_alreadyFetchedLastDeliverypointEntity = info.GetBoolean("_alreadyFetchedLastDeliverypointEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_deviceEntity = (DeviceEntity)info.GetValue("_deviceEntity", typeof(DeviceEntity));
			if(_deviceEntity!=null)
			{
				_deviceEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceEntityReturnsNewIfNotFound = info.GetBoolean("_deviceEntityReturnsNewIfNotFound");
			_alwaysFetchDeviceEntity = info.GetBoolean("_alwaysFetchDeviceEntity");
			_alreadyFetchedDeviceEntity = info.GetBoolean("_alreadyFetchedDeviceEntity");

			_roomControlAreaEntity = (RoomControlAreaEntity)info.GetValue("_roomControlAreaEntity", typeof(RoomControlAreaEntity));
			if(_roomControlAreaEntity!=null)
			{
				_roomControlAreaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlAreaEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlAreaEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlAreaEntity = info.GetBoolean("_alwaysFetchRoomControlAreaEntity");
			_alreadyFetchedRoomControlAreaEntity = info.GetBoolean("_alreadyFetchedRoomControlAreaEntity");
			_timestampCollection = (TimestampEntity)info.GetValue("_timestampCollection", typeof(TimestampEntity));
			if(_timestampCollection!=null)
			{
				_timestampCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timestampCollectionReturnsNewIfNotFound = info.GetBoolean("_timestampCollectionReturnsNewIfNotFound");
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ClientFieldIndex)fieldIndex)
			{
				case ClientFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case ClientFieldIndex.DeliverypointGroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case ClientFieldIndex.DeviceId:
					DesetupSyncDeviceEntity(true, false);
					_alreadyFetchedDeviceEntity = false;
					break;
				case ClientFieldIndex.DeliverypointId:
					DesetupSyncDeliverypointEntity(true, false);
					_alreadyFetchedDeliverypointEntity = false;
					break;
				case ClientFieldIndex.LastDeliverypointId:
					DesetupSyncLastDeliverypointEntity(true, false);
					_alreadyFetchedLastDeliverypointEntity = false;
					break;
				case ClientFieldIndex.RoomControlAreaId:
					DesetupSyncRoomControlAreaEntity(true, false);
					_alreadyFetchedRoomControlAreaEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientEntertainmentCollection = (_clientEntertainmentCollection.Count > 0);
			_alreadyFetchedClientLogCollection = (_clientLogCollection.Count > 0);
			_alreadyFetchedClientStateCollection = (_clientStateCollection.Count > 0);
			_alreadyFetchedCustomerCollection = (_customerCollection.Count > 0);
			_alreadyFetchedGameSessionCollection = (_gameSessionCollection.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessageRecipientCollection = (_messageRecipientCollection.Count > 0);
			_alreadyFetchedReceivedNetmessageCollection = (_receivedNetmessageCollection.Count > 0);
			_alreadyFetchedSentNetmessageCollection = (_sentNetmessageCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedScheduledCommandCollection = (_scheduledCommandCollection.Count > 0);
			_alreadyFetchedSurveyResultCollection = (_surveyResultCollection.Count > 0);
			_alreadyFetchedCategoryCollectionViaMessage = (_categoryCollectionViaMessage.Count > 0);
			_alreadyFetchedClientLogFileCollectionViaClientLog = (_clientLogFileCollectionViaClientLog.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaMessage = (_deliverypointCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = (_deliverypointgroupCollectionViaNetmessage.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessage = (_entertainmentCollectionViaMessage.Count > 0);
			_alreadyFetchedMediaCollectionViaMessage = (_mediaCollectionViaMessage.Count > 0);
			_alreadyFetchedOrderCollectionViaOrder = (_orderCollectionViaOrder.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage = (_terminalCollectionViaNetmessage.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointEntity = (_deliverypointEntity != null);
			_alreadyFetchedLastDeliverypointEntity = (_lastDeliverypointEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedDeviceEntity = (_deviceEntity != null);
			_alreadyFetchedRoomControlAreaEntity = (_roomControlAreaEntity != null);
			_alreadyFetchedTimestampCollection = (_timestampCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingDeliverypointId);
					break;
				case "LastDeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingLastDeliverypointId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
					break;
				case "DeviceEntity":
					toReturn.Add(Relations.DeviceEntityUsingDeviceId);
					break;
				case "RoomControlAreaEntity":
					toReturn.Add(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
					break;
				case "ClientEntertainmentCollection":
					toReturn.Add(Relations.ClientEntertainmentEntityUsingClientId);
					break;
				case "ClientLogCollection":
					toReturn.Add(Relations.ClientLogEntityUsingClientId);
					break;
				case "ClientStateCollection":
					toReturn.Add(Relations.ClientStateEntityUsingClientId);
					break;
				case "CustomerCollection":
					toReturn.Add(Relations.CustomerEntityUsingClientId);
					break;
				case "GameSessionCollection":
					toReturn.Add(Relations.GameSessionEntityUsingClientId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingClientId);
					break;
				case "MessageRecipientCollection":
					toReturn.Add(Relations.MessageRecipientEntityUsingClientId);
					break;
				case "ReceivedNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverClientId);
					break;
				case "SentNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingSenderClientId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingClientId);
					break;
				case "ScheduledCommandCollection":
					toReturn.Add(Relations.ScheduledCommandEntityUsingClientId);
					break;
				case "SurveyResultCollection":
					toReturn.Add(Relations.SurveyResultEntityUsingClientId);
					break;
				case "CategoryCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingClientId, "ClientEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CategoryEntityUsingCategoryId, "Message_", string.Empty, JoinHint.None);
					break;
				case "ClientLogFileCollectionViaClientLog":
					toReturn.Add(Relations.ClientLogEntityUsingClientId, "ClientEntity__", "ClientLog_", JoinHint.None);
					toReturn.Add(ClientLogEntity.Relations.ClientLogFileEntityUsingClientLogFileId, "ClientLog_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingClientId, "ClientEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverClientId, "ClientEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingClientId, "ClientEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Message_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingClientId, "ClientEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.MediaEntityUsingMediaId, "Message_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingClientId, "ClientEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.OrderEntityUsingMasterOrderId, "Order_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderClientId, "ClientEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingSenderTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingClientId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientEntertainmentCollection", (!this.MarkedForDeletion?_clientEntertainmentCollection:null));
			info.AddValue("_alwaysFetchClientEntertainmentCollection", _alwaysFetchClientEntertainmentCollection);
			info.AddValue("_alreadyFetchedClientEntertainmentCollection", _alreadyFetchedClientEntertainmentCollection);
			info.AddValue("_clientLogCollection", (!this.MarkedForDeletion?_clientLogCollection:null));
			info.AddValue("_alwaysFetchClientLogCollection", _alwaysFetchClientLogCollection);
			info.AddValue("_alreadyFetchedClientLogCollection", _alreadyFetchedClientLogCollection);
			info.AddValue("_clientStateCollection", (!this.MarkedForDeletion?_clientStateCollection:null));
			info.AddValue("_alwaysFetchClientStateCollection", _alwaysFetchClientStateCollection);
			info.AddValue("_alreadyFetchedClientStateCollection", _alreadyFetchedClientStateCollection);
			info.AddValue("_customerCollection", (!this.MarkedForDeletion?_customerCollection:null));
			info.AddValue("_alwaysFetchCustomerCollection", _alwaysFetchCustomerCollection);
			info.AddValue("_alreadyFetchedCustomerCollection", _alreadyFetchedCustomerCollection);
			info.AddValue("_gameSessionCollection", (!this.MarkedForDeletion?_gameSessionCollection:null));
			info.AddValue("_alwaysFetchGameSessionCollection", _alwaysFetchGameSessionCollection);
			info.AddValue("_alreadyFetchedGameSessionCollection", _alreadyFetchedGameSessionCollection);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messageRecipientCollection", (!this.MarkedForDeletion?_messageRecipientCollection:null));
			info.AddValue("_alwaysFetchMessageRecipientCollection", _alwaysFetchMessageRecipientCollection);
			info.AddValue("_alreadyFetchedMessageRecipientCollection", _alreadyFetchedMessageRecipientCollection);
			info.AddValue("_receivedNetmessageCollection", (!this.MarkedForDeletion?_receivedNetmessageCollection:null));
			info.AddValue("_alwaysFetchReceivedNetmessageCollection", _alwaysFetchReceivedNetmessageCollection);
			info.AddValue("_alreadyFetchedReceivedNetmessageCollection", _alreadyFetchedReceivedNetmessageCollection);
			info.AddValue("_sentNetmessageCollection", (!this.MarkedForDeletion?_sentNetmessageCollection:null));
			info.AddValue("_alwaysFetchSentNetmessageCollection", _alwaysFetchSentNetmessageCollection);
			info.AddValue("_alreadyFetchedSentNetmessageCollection", _alreadyFetchedSentNetmessageCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_scheduledCommandCollection", (!this.MarkedForDeletion?_scheduledCommandCollection:null));
			info.AddValue("_alwaysFetchScheduledCommandCollection", _alwaysFetchScheduledCommandCollection);
			info.AddValue("_alreadyFetchedScheduledCommandCollection", _alreadyFetchedScheduledCommandCollection);
			info.AddValue("_surveyResultCollection", (!this.MarkedForDeletion?_surveyResultCollection:null));
			info.AddValue("_alwaysFetchSurveyResultCollection", _alwaysFetchSurveyResultCollection);
			info.AddValue("_alreadyFetchedSurveyResultCollection", _alreadyFetchedSurveyResultCollection);
			info.AddValue("_categoryCollectionViaMessage", (!this.MarkedForDeletion?_categoryCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMessage", _alwaysFetchCategoryCollectionViaMessage);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMessage", _alreadyFetchedCategoryCollectionViaMessage);
			info.AddValue("_clientLogFileCollectionViaClientLog", (!this.MarkedForDeletion?_clientLogFileCollectionViaClientLog:null));
			info.AddValue("_alwaysFetchClientLogFileCollectionViaClientLog", _alwaysFetchClientLogFileCollectionViaClientLog);
			info.AddValue("_alreadyFetchedClientLogFileCollectionViaClientLog", _alreadyFetchedClientLogFileCollectionViaClientLog);
			info.AddValue("_deliverypointCollectionViaMessage", (!this.MarkedForDeletion?_deliverypointCollectionViaMessage:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaMessage", _alwaysFetchDeliverypointCollectionViaMessage);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaMessage", _alreadyFetchedDeliverypointCollectionViaMessage);
			info.AddValue("_deliverypointgroupCollectionViaNetmessage", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaNetmessage", _alwaysFetchDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage", _alreadyFetchedDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_entertainmentCollectionViaMessage", (!this.MarkedForDeletion?_entertainmentCollectionViaMessage:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessage", _alwaysFetchEntertainmentCollectionViaMessage);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessage", _alreadyFetchedEntertainmentCollectionViaMessage);
			info.AddValue("_mediaCollectionViaMessage", (!this.MarkedForDeletion?_mediaCollectionViaMessage:null));
			info.AddValue("_alwaysFetchMediaCollectionViaMessage", _alwaysFetchMediaCollectionViaMessage);
			info.AddValue("_alreadyFetchedMediaCollectionViaMessage", _alreadyFetchedMediaCollectionViaMessage);
			info.AddValue("_orderCollectionViaOrder", (!this.MarkedForDeletion?_orderCollectionViaOrder:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrder", _alwaysFetchOrderCollectionViaOrder);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrder", _alreadyFetchedOrderCollectionViaOrder);
			info.AddValue("_terminalCollectionViaNetmessage", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage", _alwaysFetchTerminalCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage", _alreadyFetchedTerminalCollectionViaNetmessage);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_deliverypointEntity", (!this.MarkedForDeletion?_deliverypointEntity:null));
			info.AddValue("_deliverypointEntityReturnsNewIfNotFound", _deliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity", _alwaysFetchDeliverypointEntity);
			info.AddValue("_alreadyFetchedDeliverypointEntity", _alreadyFetchedDeliverypointEntity);
			info.AddValue("_lastDeliverypointEntity", (!this.MarkedForDeletion?_lastDeliverypointEntity:null));
			info.AddValue("_lastDeliverypointEntityReturnsNewIfNotFound", _lastDeliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLastDeliverypointEntity", _alwaysFetchLastDeliverypointEntity);
			info.AddValue("_alreadyFetchedLastDeliverypointEntity", _alreadyFetchedLastDeliverypointEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_deviceEntity", (!this.MarkedForDeletion?_deviceEntity:null));
			info.AddValue("_deviceEntityReturnsNewIfNotFound", _deviceEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceEntity", _alwaysFetchDeviceEntity);
			info.AddValue("_alreadyFetchedDeviceEntity", _alreadyFetchedDeviceEntity);
			info.AddValue("_roomControlAreaEntity", (!this.MarkedForDeletion?_roomControlAreaEntity:null));
			info.AddValue("_roomControlAreaEntityReturnsNewIfNotFound", _roomControlAreaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlAreaEntity", _alwaysFetchRoomControlAreaEntity);
			info.AddValue("_alreadyFetchedRoomControlAreaEntity", _alreadyFetchedRoomControlAreaEntity);

			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_timestampCollectionReturnsNewIfNotFound", _timestampCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointEntity":
					_alreadyFetchedDeliverypointEntity = true;
					this.DeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "LastDeliverypointEntity":
					_alreadyFetchedLastDeliverypointEntity = true;
					this.LastDeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "DeviceEntity":
					_alreadyFetchedDeviceEntity = true;
					this.DeviceEntity = (DeviceEntity)entity;
					break;
				case "RoomControlAreaEntity":
					_alreadyFetchedRoomControlAreaEntity = true;
					this.RoomControlAreaEntity = (RoomControlAreaEntity)entity;
					break;
				case "ClientEntertainmentCollection":
					_alreadyFetchedClientEntertainmentCollection = true;
					if(entity!=null)
					{
						this.ClientEntertainmentCollection.Add((ClientEntertainmentEntity)entity);
					}
					break;
				case "ClientLogCollection":
					_alreadyFetchedClientLogCollection = true;
					if(entity!=null)
					{
						this.ClientLogCollection.Add((ClientLogEntity)entity);
					}
					break;
				case "ClientStateCollection":
					_alreadyFetchedClientStateCollection = true;
					if(entity!=null)
					{
						this.ClientStateCollection.Add((ClientStateEntity)entity);
					}
					break;
				case "CustomerCollection":
					_alreadyFetchedCustomerCollection = true;
					if(entity!=null)
					{
						this.CustomerCollection.Add((CustomerEntity)entity);
					}
					break;
				case "GameSessionCollection":
					_alreadyFetchedGameSessionCollection = true;
					if(entity!=null)
					{
						this.GameSessionCollection.Add((GameSessionEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessageRecipientCollection":
					_alreadyFetchedMessageRecipientCollection = true;
					if(entity!=null)
					{
						this.MessageRecipientCollection.Add((MessageRecipientEntity)entity);
					}
					break;
				case "ReceivedNetmessageCollection":
					_alreadyFetchedReceivedNetmessageCollection = true;
					if(entity!=null)
					{
						this.ReceivedNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "SentNetmessageCollection":
					_alreadyFetchedSentNetmessageCollection = true;
					if(entity!=null)
					{
						this.SentNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				case "ScheduledCommandCollection":
					_alreadyFetchedScheduledCommandCollection = true;
					if(entity!=null)
					{
						this.ScheduledCommandCollection.Add((ScheduledCommandEntity)entity);
					}
					break;
				case "SurveyResultCollection":
					_alreadyFetchedSurveyResultCollection = true;
					if(entity!=null)
					{
						this.SurveyResultCollection.Add((SurveyResultEntity)entity);
					}
					break;
				case "CategoryCollectionViaMessage":
					_alreadyFetchedCategoryCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMessage.Add((CategoryEntity)entity);
					}
					break;
				case "ClientLogFileCollectionViaClientLog":
					_alreadyFetchedClientLogFileCollectionViaClientLog = true;
					if(entity!=null)
					{
						this.ClientLogFileCollectionViaClientLog.Add((ClientLogFileEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaMessage":
					_alreadyFetchedDeliverypointCollectionViaMessage = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaMessage.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaNetmessage.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessage":
					_alreadyFetchedEntertainmentCollectionViaMessage = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessage.Add((EntertainmentEntity)entity);
					}
					break;
				case "MediaCollectionViaMessage":
					_alreadyFetchedMediaCollectionViaMessage = true;
					if(entity!=null)
					{
						this.MediaCollectionViaMessage.Add((MediaEntity)entity);
					}
					break;
				case "OrderCollectionViaOrder":
					_alreadyFetchedOrderCollectionViaOrder = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrder.Add((OrderEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage":
					_alreadyFetchedTerminalCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage.Add((TerminalEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					this.TimestampCollection = (TimestampEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointEntity":
					SetupSyncDeliverypointEntity(relatedEntity);
					break;
				case "LastDeliverypointEntity":
					SetupSyncLastDeliverypointEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "DeviceEntity":
					SetupSyncDeviceEntity(relatedEntity);
					break;
				case "RoomControlAreaEntity":
					SetupSyncRoomControlAreaEntity(relatedEntity);
					break;
				case "ClientEntertainmentCollection":
					_clientEntertainmentCollection.Add((ClientEntertainmentEntity)relatedEntity);
					break;
				case "ClientLogCollection":
					_clientLogCollection.Add((ClientLogEntity)relatedEntity);
					break;
				case "ClientStateCollection":
					_clientStateCollection.Add((ClientStateEntity)relatedEntity);
					break;
				case "CustomerCollection":
					_customerCollection.Add((CustomerEntity)relatedEntity);
					break;
				case "GameSessionCollection":
					_gameSessionCollection.Add((GameSessionEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessageRecipientCollection":
					_messageRecipientCollection.Add((MessageRecipientEntity)relatedEntity);
					break;
				case "ReceivedNetmessageCollection":
					_receivedNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "SentNetmessageCollection":
					_sentNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				case "ScheduledCommandCollection":
					_scheduledCommandCollection.Add((ScheduledCommandEntity)relatedEntity);
					break;
				case "SurveyResultCollection":
					_surveyResultCollection.Add((SurveyResultEntity)relatedEntity);
					break;
				case "TimestampCollection":
					SetupSyncTimestampCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointEntity":
					DesetupSyncDeliverypointEntity(false, true);
					break;
				case "LastDeliverypointEntity":
					DesetupSyncLastDeliverypointEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "DeviceEntity":
					DesetupSyncDeviceEntity(false, true);
					break;
				case "RoomControlAreaEntity":
					DesetupSyncRoomControlAreaEntity(false, true);
					break;
				case "ClientEntertainmentCollection":
					this.PerformRelatedEntityRemoval(_clientEntertainmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientLogCollection":
					this.PerformRelatedEntityRemoval(_clientLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientStateCollection":
					this.PerformRelatedEntityRemoval(_clientStateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomerCollection":
					this.PerformRelatedEntityRemoval(_customerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GameSessionCollection":
					this.PerformRelatedEntityRemoval(_gameSessionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageRecipientCollection":
					this.PerformRelatedEntityRemoval(_messageRecipientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReceivedNetmessageCollection":
					this.PerformRelatedEntityRemoval(_receivedNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SentNetmessageCollection":
					this.PerformRelatedEntityRemoval(_sentNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledCommandCollection":
					this.PerformRelatedEntityRemoval(_scheduledCommandCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyResultCollection":
					this.PerformRelatedEntityRemoval(_surveyResultCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					DesetupSyncTimestampCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_timestampCollection!=null)
			{
				toReturn.Add(_timestampCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointEntity!=null)
			{
				toReturn.Add(_deliverypointEntity);
			}
			if(_lastDeliverypointEntity!=null)
			{
				toReturn.Add(_lastDeliverypointEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_deviceEntity!=null)
			{
				toReturn.Add(_deviceEntity);
			}
			if(_roomControlAreaEntity!=null)
			{
				toReturn.Add(_roomControlAreaEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientEntertainmentCollection);
			toReturn.Add(_clientLogCollection);
			toReturn.Add(_clientStateCollection);
			toReturn.Add(_customerCollection);
			toReturn.Add(_gameSessionCollection);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messageRecipientCollection);
			toReturn.Add(_receivedNetmessageCollection);
			toReturn.Add(_sentNetmessageCollection);
			toReturn.Add(_orderCollection);
			toReturn.Add(_scheduledCommandCollection);
			toReturn.Add(_surveyResultCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientId)
		{
			return FetchUsingPK(clientId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(clientId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(clientId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(clientId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ClientId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClientRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientEntertainmentCollection GetMultiClientEntertainmentCollection(bool forceFetch)
		{
			return GetMultiClientEntertainmentCollection(forceFetch, _clientEntertainmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientEntertainmentCollection GetMultiClientEntertainmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientEntertainmentCollection(forceFetch, _clientEntertainmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientEntertainmentCollection GetMultiClientEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientEntertainmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientEntertainmentCollection GetMultiClientEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientEntertainmentCollection || forceFetch || _alwaysFetchClientEntertainmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientEntertainmentCollection);
				_clientEntertainmentCollection.SuppressClearInGetMulti=!forceFetch;
				_clientEntertainmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientEntertainmentCollection.GetMultiManyToOne(this, null, filter);
				_clientEntertainmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientEntertainmentCollection = true;
			}
			return _clientEntertainmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientEntertainmentCollection'. These settings will be taken into account
		/// when the property ClientEntertainmentCollection is requested or GetMultiClientEntertainmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientEntertainmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientEntertainmentCollection.SortClauses=sortClauses;
			_clientEntertainmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch)
		{
			return GetMultiClientLogCollection(forceFetch, _clientLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientLogCollection(forceFetch, _clientLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientLogCollection GetMultiClientLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientLogCollection || forceFetch || _alwaysFetchClientLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientLogCollection);
				_clientLogCollection.SuppressClearInGetMulti=!forceFetch;
				_clientLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientLogCollection.GetMultiManyToOne(this, null, filter);
				_clientLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientLogCollection = true;
			}
			return _clientLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientLogCollection'. These settings will be taken into account
		/// when the property ClientLogCollection is requested or GetMultiClientLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientLogCollection.SortClauses=sortClauses;
			_clientLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientStateEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientStateCollection GetMultiClientStateCollection(bool forceFetch)
		{
			return GetMultiClientStateCollection(forceFetch, _clientStateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientStateEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientStateCollection GetMultiClientStateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientStateCollection(forceFetch, _clientStateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientStateCollection GetMultiClientStateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientStateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientStateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientStateCollection GetMultiClientStateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientStateCollection || forceFetch || _alwaysFetchClientStateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientStateCollection);
				_clientStateCollection.SuppressClearInGetMulti=!forceFetch;
				_clientStateCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientStateCollection.GetMultiManyToOne(this, filter);
				_clientStateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientStateCollection = true;
			}
			return _clientStateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientStateCollection'. These settings will be taken into account
		/// when the property ClientStateCollection is requested or GetMultiClientStateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientStateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientStateCollection.SortClauses=sortClauses;
			_clientStateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollection(bool forceFetch)
		{
			return GetMultiCustomerCollection(forceFetch, _customerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomerCollection(forceFetch, _customerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomerCollection || forceFetch || _alwaysFetchCustomerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollection);
				_customerCollection.SuppressClearInGetMulti=!forceFetch;
				_customerCollection.EntityFactoryToUse = entityFactoryToUse;
				_customerCollection.GetMultiManyToOne(this, filter);
				_customerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollection = true;
			}
			return _customerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollection'. These settings will be taken into account
		/// when the property CustomerCollection is requested or GetMultiCustomerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollection.SortClauses=sortClauses;
			_customerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GameSessionEntity'</returns>
		public Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch)
		{
			return GetMultiGameSessionCollection(forceFetch, _gameSessionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GameSessionEntity'</returns>
		public Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGameSessionCollection(forceFetch, _gameSessionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGameSessionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GameSessionCollection GetMultiGameSessionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGameSessionCollection || forceFetch || _alwaysFetchGameSessionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_gameSessionCollection);
				_gameSessionCollection.SuppressClearInGetMulti=!forceFetch;
				_gameSessionCollection.EntityFactoryToUse = entityFactoryToUse;
				_gameSessionCollection.GetMultiManyToOne(this, null, null, filter);
				_gameSessionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGameSessionCollection = true;
			}
			return _gameSessionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GameSessionCollection'. These settings will be taken into account
		/// when the property GameSessionCollection is requested or GetMultiGameSessionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGameSessionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_gameSessionCollection.SortClauses=sortClauses;
			_gameSessionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageRecipientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageRecipientCollection || forceFetch || _alwaysFetchMessageRecipientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageRecipientCollection);
				_messageRecipientCollection.SuppressClearInGetMulti=!forceFetch;
				_messageRecipientCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageRecipientCollection.GetMultiManyToOne(null, this, null, null, null, null, null, filter);
				_messageRecipientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageRecipientCollection = true;
			}
			return _messageRecipientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageRecipientCollection'. These settings will be taken into account
		/// when the property MessageRecipientCollection is requested or GetMultiMessageRecipientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageRecipientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageRecipientCollection.SortClauses=sortClauses;
			_messageRecipientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReceivedNetmessageCollection || forceFetch || _alwaysFetchReceivedNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_receivedNetmessageCollection);
				_receivedNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_receivedNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_receivedNetmessageCollection.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, filter);
				_receivedNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReceivedNetmessageCollection = true;
			}
			return _receivedNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReceivedNetmessageCollection'. These settings will be taken into account
		/// when the property ReceivedNetmessageCollection is requested or GetMultiReceivedNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReceivedNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_receivedNetmessageCollection.SortClauses=sortClauses;
			_receivedNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSentNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSentNetmessageCollection || forceFetch || _alwaysFetchSentNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sentNetmessageCollection);
				_sentNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_sentNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_sentNetmessageCollection.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, filter);
				_sentNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSentNetmessageCollection = true;
			}
			return _sentNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SentNetmessageCollection'. These settings will be taken into account
		/// when the property SentNetmessageCollection is requested or GetMultiSentNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSentNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sentNetmessageCollection.SortClauses=sortClauses;
			_sentNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledCommandEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch)
		{
			return GetMultiScheduledCommandCollection(forceFetch, _scheduledCommandCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledCommandEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledCommandCollection(forceFetch, _scheduledCommandCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledCommandCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledCommandCollection GetMultiScheduledCommandCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledCommandCollection || forceFetch || _alwaysFetchScheduledCommandCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledCommandCollection);
				_scheduledCommandCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledCommandCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledCommandCollection.GetMultiManyToOne(this, null, null, filter);
				_scheduledCommandCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledCommandCollection = true;
			}
			return _scheduledCommandCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledCommandCollection'. These settings will be taken into account
		/// when the property ScheduledCommandCollection is requested or GetMultiScheduledCommandCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledCommandCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledCommandCollection.SortClauses=sortClauses;
			_scheduledCommandCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyResultCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyResultCollection || forceFetch || _alwaysFetchSurveyResultCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyResultCollection);
				_surveyResultCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyResultCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyResultCollection.GetMultiManyToOne(this, null, null, null, filter);
				_surveyResultCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyResultCollection = true;
			}
			return _surveyResultCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyResultCollection'. These settings will be taken into account
		/// when the property SurveyResultCollection is requested or GetMultiSurveyResultCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyResultCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyResultCollection.SortClauses=sortClauses;
			_surveyResultCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMessage(forceFetch, _categoryCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMessage || forceFetch || _alwaysFetchCategoryCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMessage.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMessage"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMessage = true;
			}
			return _categoryCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMessage'. These settings will be taken into account
		/// when the property CategoryCollectionViaMessage is requested or GetMultiCategoryCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMessage.SortClauses=sortClauses;
			_categoryCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientLogFileEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientLogFileEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientLogFileCollection GetMultiClientLogFileCollectionViaClientLog(bool forceFetch)
		{
			return GetMultiClientLogFileCollectionViaClientLog(forceFetch, _clientLogFileCollectionViaClientLog.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientLogFileEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientLogFileCollection GetMultiClientLogFileCollectionViaClientLog(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientLogFileCollectionViaClientLog || forceFetch || _alwaysFetchClientLogFileCollectionViaClientLog) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientLogFileCollectionViaClientLog);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_clientLogFileCollectionViaClientLog.SuppressClearInGetMulti=!forceFetch;
				_clientLogFileCollectionViaClientLog.EntityFactoryToUse = entityFactoryToUse;
				_clientLogFileCollectionViaClientLog.GetMulti(filter, GetRelationsForField("ClientLogFileCollectionViaClientLog"));
				_clientLogFileCollectionViaClientLog.SuppressClearInGetMulti=false;
				_alreadyFetchedClientLogFileCollectionViaClientLog = true;
			}
			return _clientLogFileCollectionViaClientLog;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientLogFileCollectionViaClientLog'. These settings will be taken into account
		/// when the property ClientLogFileCollectionViaClientLog is requested or GetMultiClientLogFileCollectionViaClientLog is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientLogFileCollectionViaClientLog(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientLogFileCollectionViaClientLog.SortClauses=sortClauses;
			_clientLogFileCollectionViaClientLog.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaMessage(forceFetch, _deliverypointCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaMessage || forceFetch || _alwaysFetchDeliverypointCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaMessage.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaMessage"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaMessage = true;
			}
			return _deliverypointCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaMessage'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaMessage is requested or GetMultiDeliverypointCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaMessage.SortClauses=sortClauses;
			_deliverypointCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaNetmessage(forceFetch, _deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaNetmessage || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
			}
			return _deliverypointgroupCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaNetmessage'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaNetmessage is requested or GetMultiDeliverypointgroupCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaNetmessage.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessage(forceFetch, _entertainmentCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessage || forceFetch || _alwaysFetchEntertainmentCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessage.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessage"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessage = true;
			}
			return _entertainmentCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessage'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessage is requested or GetMultiEntertainmentCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessage.SortClauses=sortClauses;
			_entertainmentCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch)
		{
			return GetMultiMediaCollectionViaMessage(forceFetch, _mediaCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaMessage || forceFetch || _alwaysFetchMediaCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaMessage.GetMulti(filter, GetRelationsForField("MediaCollectionViaMessage"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaMessage = true;
			}
			return _mediaCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaMessage'. These settings will be taken into account
		/// when the property MediaCollectionViaMessage is requested or GetMultiMediaCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaMessage.SortClauses=sortClauses;
			_mediaCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrder(forceFetch, _orderCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrder || forceFetch || _alwaysFetchOrderCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrder.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrder"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrder = true;
			}
			return _orderCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrder'. These settings will be taken into account
		/// when the property OrderCollectionViaOrder is requested or GetMultiOrderCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrder.SortClauses=sortClauses;
			_orderCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage(forceFetch, _terminalCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ClientFields.ClientId, ComparisonOperator.Equal, this.ClientId, "ClientEntity__"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage = true;
			}
			return _terminalCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage is requested or GetMultiTerminalCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity()
		{
			return GetSingleDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity || forceFetch || _alwaysFetchDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointEntity = newEntity;
				_alreadyFetchedDeliverypointEntity = fetchResult;
			}
			return _deliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleLastDeliverypointEntity()
		{
			return GetSingleLastDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleLastDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLastDeliverypointEntity || forceFetch || _alwaysFetchLastDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingLastDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LastDeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_lastDeliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LastDeliverypointEntity = newEntity;
				_alreadyFetchedLastDeliverypointEntity = fetchResult;
			}
			return _lastDeliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointGroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public DeviceEntity GetSingleDeviceEntity()
		{
			return GetSingleDeviceEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public virtual DeviceEntity GetSingleDeviceEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceEntity || forceFetch || _alwaysFetchDeviceEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceEntityUsingDeviceId);
				DeviceEntity newEntity = new DeviceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceEntity = newEntity;
				_alreadyFetchedDeviceEntity = fetchResult;
			}
			return _deviceEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public RoomControlAreaEntity GetSingleRoomControlAreaEntity()
		{
			return GetSingleRoomControlAreaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlAreaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlAreaEntity' which is related to this entity.</returns>
		public virtual RoomControlAreaEntity GetSingleRoomControlAreaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlAreaEntity || forceFetch || _alwaysFetchRoomControlAreaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlAreaEntityUsingRoomControlAreaId);
				RoomControlAreaEntity newEntity = new RoomControlAreaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlAreaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlAreaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlAreaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlAreaEntity = newEntity;
				_alreadyFetchedRoomControlAreaEntity = fetchResult;
			}
			return _roomControlAreaEntity;
		}

		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public TimestampEntity GetSingleTimestampCollection()
		{
			return GetSingleTimestampCollection(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public virtual TimestampEntity GetSingleTimestampCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimestampEntityUsingClientId);
				TimestampEntity newEntity = new TimestampEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCClientId(this.ClientId);
				}
				if(fetchResult)
				{
					newEntity = (TimestampEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timestampCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimestampCollection = newEntity;
				_alreadyFetchedTimestampCollection = fetchResult;
			}
			return _timestampCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointEntity", _deliverypointEntity);
			toReturn.Add("LastDeliverypointEntity", _lastDeliverypointEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("DeviceEntity", _deviceEntity);
			toReturn.Add("RoomControlAreaEntity", _roomControlAreaEntity);
			toReturn.Add("ClientEntertainmentCollection", _clientEntertainmentCollection);
			toReturn.Add("ClientLogCollection", _clientLogCollection);
			toReturn.Add("ClientStateCollection", _clientStateCollection);
			toReturn.Add("CustomerCollection", _customerCollection);
			toReturn.Add("GameSessionCollection", _gameSessionCollection);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessageRecipientCollection", _messageRecipientCollection);
			toReturn.Add("ReceivedNetmessageCollection", _receivedNetmessageCollection);
			toReturn.Add("SentNetmessageCollection", _sentNetmessageCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			toReturn.Add("ScheduledCommandCollection", _scheduledCommandCollection);
			toReturn.Add("SurveyResultCollection", _surveyResultCollection);
			toReturn.Add("CategoryCollectionViaMessage", _categoryCollectionViaMessage);
			toReturn.Add("ClientLogFileCollectionViaClientLog", _clientLogFileCollectionViaClientLog);
			toReturn.Add("DeliverypointCollectionViaMessage", _deliverypointCollectionViaMessage);
			toReturn.Add("DeliverypointgroupCollectionViaNetmessage", _deliverypointgroupCollectionViaNetmessage);
			toReturn.Add("EntertainmentCollectionViaMessage", _entertainmentCollectionViaMessage);
			toReturn.Add("MediaCollectionViaMessage", _mediaCollectionViaMessage);
			toReturn.Add("OrderCollectionViaOrder", _orderCollectionViaOrder);
			toReturn.Add("TerminalCollectionViaNetmessage", _terminalCollectionViaNetmessage);
			toReturn.Add("TimestampCollection", _timestampCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="validator">The validator object for this ClientEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 clientId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(clientId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientEntertainmentCollection = new Obymobi.Data.CollectionClasses.ClientEntertainmentCollection();
			_clientEntertainmentCollection.SetContainingEntityInfo(this, "ClientEntity");

			_clientLogCollection = new Obymobi.Data.CollectionClasses.ClientLogCollection();
			_clientLogCollection.SetContainingEntityInfo(this, "ClientEntity");

			_clientStateCollection = new Obymobi.Data.CollectionClasses.ClientStateCollection();
			_clientStateCollection.SetContainingEntityInfo(this, "ClientEntity");

			_customerCollection = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_customerCollection.SetContainingEntityInfo(this, "ClientEntity");

			_gameSessionCollection = new Obymobi.Data.CollectionClasses.GameSessionCollection();
			_gameSessionCollection.SetContainingEntityInfo(this, "ClientEntity");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "ClientEntity");

			_messageRecipientCollection = new Obymobi.Data.CollectionClasses.MessageRecipientCollection();
			_messageRecipientCollection.SetContainingEntityInfo(this, "ClientEntity");

			_receivedNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_receivedNetmessageCollection.SetContainingEntityInfo(this, "ReceiverClientEntity");

			_sentNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_sentNetmessageCollection.SetContainingEntityInfo(this, "SenderClientEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "ClientEntity");

			_scheduledCommandCollection = new Obymobi.Data.CollectionClasses.ScheduledCommandCollection();
			_scheduledCommandCollection.SetContainingEntityInfo(this, "ClientEntity");

			_surveyResultCollection = new Obymobi.Data.CollectionClasses.SurveyResultCollection();
			_surveyResultCollection.SetContainingEntityInfo(this, "ClientEntity");
			_categoryCollectionViaMessage = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_clientLogFileCollectionViaClientLog = new Obymobi.Data.CollectionClasses.ClientLogFileCollection();
			_deliverypointCollectionViaMessage = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaMessage = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_mediaCollectionViaMessage = new Obymobi.Data.CollectionClasses.MediaCollection();
			_orderCollectionViaOrder = new Obymobi.Data.CollectionClasses.OrderCollection();
			_terminalCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointEntityReturnsNewIfNotFound = true;
			_lastDeliverypointEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_deviceEntityReturnsNewIfNotFound = true;
			_roomControlAreaEntityReturnsNewIfNotFound = true;
			_timestampCollectionReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ImageVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointGroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogToFile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HeartbeatPoll", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeNotificationsSent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStateOnline", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastStateOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BluetoothKeyboardConnected", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WakeUpTimeUtc", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeNotificationLastSentUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlConnected", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadedSuccessfully", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WakeUpTimeUtcUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DoNotDisturbActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceRoomActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUserInteraction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusUpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastWifiSsid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTestClient", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastApiVersion", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticClientRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ClientCollection", resetFKFields, new int[] { (int)ClientFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticClientRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, signalRelatedEntity, "ClientCollection", resetFKFields, new int[] { (int)ClientFieldIndex.DeliverypointId } );		
			_deliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointEntity(true, true);
				_deliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, ref _alreadyFetchedDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _lastDeliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLastDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _lastDeliverypointEntity, new PropertyChangedEventHandler( OnLastDeliverypointEntityPropertyChanged ), "LastDeliverypointEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeliverypointEntityUsingLastDeliverypointIdStatic, true, signalRelatedEntity, "ClientCollection_", resetFKFields, new int[] { (int)ClientFieldIndex.LastDeliverypointId } );		
			_lastDeliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _lastDeliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLastDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_lastDeliverypointEntity!=relatedEntity)
			{		
				DesetupSyncLastDeliverypointEntity(true, true);
				_lastDeliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _lastDeliverypointEntity, new PropertyChangedEventHandler( OnLastDeliverypointEntityPropertyChanged ), "LastDeliverypointEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeliverypointEntityUsingLastDeliverypointIdStatic, true, ref _alreadyFetchedLastDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLastDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeliverypointgroupEntityUsingDeliverypointGroupIdStatic, true, signalRelatedEntity, "ClientCollection", resetFKFields, new int[] { (int)ClientFieldIndex.DeliverypointGroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeliverypointgroupEntityUsingDeliverypointGroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceEntity, new PropertyChangedEventHandler( OnDeviceEntityPropertyChanged ), "DeviceEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeviceEntityUsingDeviceIdStatic, true, signalRelatedEntity, "ClientCollection", resetFKFields, new int[] { (int)ClientFieldIndex.DeviceId } );		
			_deviceEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deviceEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceEntity(IEntityCore relatedEntity)
		{
			if(_deviceEntity!=relatedEntity)
			{		
				DesetupSyncDeviceEntity(true, true);
				_deviceEntity = (DeviceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceEntity, new PropertyChangedEventHandler( OnDeviceEntityPropertyChanged ), "DeviceEntity", Obymobi.Data.RelationClasses.StaticClientRelations.DeviceEntityUsingDeviceIdStatic, true, ref _alreadyFetchedDeviceEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlAreaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticClientRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, signalRelatedEntity, "ClientCollection", resetFKFields, new int[] { (int)ClientFieldIndex.RoomControlAreaId } );		
			_roomControlAreaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlAreaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlAreaEntity(IEntityCore relatedEntity)
		{
			if(_roomControlAreaEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlAreaEntity(true, true);
				_roomControlAreaEntity = (RoomControlAreaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlAreaEntity, new PropertyChangedEventHandler( OnRoomControlAreaEntityPropertyChanged ), "RoomControlAreaEntity", Obymobi.Data.RelationClasses.StaticClientRelations.RoomControlAreaEntityUsingRoomControlAreaIdStatic, true, ref _alreadyFetchedRoomControlAreaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlAreaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timestampCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimestampCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticClientRelations.TimestampEntityUsingClientIdStatic, false, signalRelatedEntity, "ClientEntity", false, new int[] { (int)ClientFieldIndex.ClientId } );
			_timestampCollection = null;
		}
	
		/// <summary> setups the sync logic for member _timestampCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimestampCollection(IEntityCore relatedEntity)
		{
			if(_timestampCollection!=relatedEntity)
			{
				DesetupSyncTimestampCollection(true, true);
				_timestampCollection = (TimestampEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticClientRelations.TimestampEntityUsingClientIdStatic, false, ref _alreadyFetchedTimestampCollection, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimestampCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 clientId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ClientFieldIndex.ClientId].ForcedCurrentValueWrite(clientId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateClientDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ClientEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClientRelations Relations
		{
			get	{ return new ClientRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientEntertainment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntertainmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientEntertainmentCollection(), (IEntityRelation)GetRelationsForField("ClientEntertainmentCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.ClientEntertainmentEntity, 0, null, null, null, "ClientEntertainmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientLogCollection(), (IEntityRelation)GetRelationsForField("ClientLogCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.ClientLogEntity, 0, null, null, null, "ClientLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientState' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientStateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientStateCollection(), (IEntityRelation)GetRelationsForField("ClientStateCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.ClientStateEntity, 0, null, null, null, "ClientStateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), (IEntityRelation)GetRelationsForField("CustomerCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, null, "CustomerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GameSession' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGameSessionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GameSessionCollection(), (IEntityRelation)GetRelationsForField("GameSessionCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.GameSessionEntity, 0, null, null, null, "GameSessionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageRecipient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageRecipientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageRecipientCollection(), (IEntityRelation)GetRelationsForField("MessageRecipientCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.MessageRecipientEntity, 0, null, null, null, "MessageRecipientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceivedNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("ReceivedNetmessageCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "ReceivedNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSentNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("SentNetmessageCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "SentNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledCommand' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledCommandCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledCommandCollection(), (IEntityRelation)GetRelationsForField("ScheduledCommandCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.ScheduledCommandEntity, 0, null, null, null, "ScheduledCommandCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyResultCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyResultCollection(), (IEntityRelation)GetRelationsForField("SurveyResultCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.SurveyResultEntity, 0, null, null, null, "SurveyResultCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingClientId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMessage"), "CategoryCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientLogFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientLogFileCollectionViaClientLog
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientLogEntityUsingClientId;
				intermediateRelation.SetAliases(string.Empty, "ClientLog_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientLogFileCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.ClientLogFileEntity, 0, null, null, GetRelationsForField("ClientLogFileCollectionViaClientLog"), "ClientLogFileCollectionViaClientLog", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingClientId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaMessage"), "DeliverypointCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverClientId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"), "DeliverypointgroupCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingClientId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessage"), "EntertainmentCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingClientId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaMessage"), "MediaCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingClientId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrder"), "OrderCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderClientId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage"), "TerminalCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointEntity")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLastDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("LastDeliverypointEntity")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "LastDeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), (IEntityRelation)GetRelationsForField("DeviceEntity")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, null, "DeviceEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlArea'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlAreaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlAreaCollection(), (IEntityRelation)GetRelationsForField("RoomControlAreaEntity")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.RoomControlAreaEntity, 0, null, null, null, "RoomControlAreaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.ClientEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientId
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.ClientId, true); }
			set	{ SetValue((int)ClientFieldIndex.ClientId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ClientFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The ImageVersion property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."ImageVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ImageVersion
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.ImageVersion, true); }
			set	{ SetValue((int)ClientFieldIndex.ImageVersion, value, true); }
		}

		/// <summary> The Notes property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."Notes"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.Notes, true); }
			set	{ SetValue((int)ClientFieldIndex.Notes, value, true); }
		}

		/// <summary> The DeliverypointGroupId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DeliverypointGroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointGroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.DeliverypointGroupId, false); }
			set	{ SetValue((int)ClientFieldIndex.DeliverypointGroupId, value, true); }
		}

		/// <summary> The OperationMode property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."OperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OperationMode
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.OperationMode, true); }
			set	{ SetValue((int)ClientFieldIndex.OperationMode, value, true); }
		}

		/// <summary> The LastStatus property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LastStatus
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.LastStatus, false); }
			set	{ SetValue((int)ClientFieldIndex.LastStatus, value, true); }
		}

		/// <summary> The LastStatusText property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastStatusText
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.LastStatusText, true); }
			set	{ SetValue((int)ClientFieldIndex.LastStatusText, value, true); }
		}

		/// <summary> The LogToFile property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LogToFile"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LogToFile
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.LogToFile, true); }
			set	{ SetValue((int)ClientFieldIndex.LogToFile, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ClientFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ClientFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The DeviceId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DeviceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.DeviceId, false); }
			set	{ SetValue((int)ClientFieldIndex.DeviceId, value, true); }
		}

		/// <summary> The HeartbeatPoll property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."HeartbeatPoll"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HeartbeatPoll
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.HeartbeatPoll, true); }
			set	{ SetValue((int)ClientFieldIndex.HeartbeatPoll, value, true); }
		}

		/// <summary> The OutOfChargeNotificationsSent property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."OutOfChargeNotificationsSent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeNotificationsSent
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.OutOfChargeNotificationsSent, true); }
			set	{ SetValue((int)ClientFieldIndex.OutOfChargeNotificationsSent, value, true); }
		}

		/// <summary> The DeliverypointId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.DeliverypointId, false); }
			set	{ SetValue((int)ClientFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The LastDeliverypointId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LastDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.LastDeliverypointId, false); }
			set	{ SetValue((int)ClientFieldIndex.LastDeliverypointId, value, true); }
		}

		/// <summary> The LastOperationMode property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LastOperationMode
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.LastOperationMode, false); }
			set	{ SetValue((int)ClientFieldIndex.LastOperationMode, value, true); }
		}

		/// <summary> The LastStateOnline property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStateOnline"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> LastStateOnline
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ClientFieldIndex.LastStateOnline, false); }
			set	{ SetValue((int)ClientFieldIndex.LastStateOnline, value, true); }
		}

		/// <summary> The LastStateOperationMode property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastStateOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LastStateOperationMode
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.LastStateOperationMode, false); }
			set	{ SetValue((int)ClientFieldIndex.LastStateOperationMode, value, true); }
		}

		/// <summary> The BluetoothKeyboardConnected property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."BluetoothKeyboardConnected"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BluetoothKeyboardConnected
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.BluetoothKeyboardConnected, true); }
			set	{ SetValue((int)ClientFieldIndex.BluetoothKeyboardConnected, value, true); }
		}

		/// <summary> The RoomControlAreaId property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."RoomControlAreaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientFieldIndex.RoomControlAreaId, false); }
			set	{ SetValue((int)ClientFieldIndex.RoomControlAreaId, value, true); }
		}

		/// <summary> The WakeUpTimeUtc property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."WakeUpTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> WakeUpTimeUtc
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.WakeUpTimeUtc, false); }
			set	{ SetValue((int)ClientFieldIndex.WakeUpTimeUtc, value, true); }
		}

		/// <summary> The OutOfChargeNotificationLastSentUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."OutOfChargeNotificationLastSentUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> OutOfChargeNotificationLastSentUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.OutOfChargeNotificationLastSentUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.OutOfChargeNotificationLastSentUTC, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The RoomControlConnected property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."RoomControlConnected"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RoomControlConnected
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.RoomControlConnected, true); }
			set	{ SetValue((int)ClientFieldIndex.RoomControlConnected, value, true); }
		}

		/// <summary> The LoadedSuccessfully property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LoadedSuccessfully"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LoadedSuccessfully
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.LoadedSuccessfully, true); }
			set	{ SetValue((int)ClientFieldIndex.LoadedSuccessfully, value, true); }
		}

		/// <summary> The WakeUpTimeUtcUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."StatusUpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> WakeUpTimeUtcUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.WakeUpTimeUtcUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.WakeUpTimeUtcUTC, value, true); }
		}

		/// <summary> The DoNotDisturbActive property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."DoNotDisturbActive"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DoNotDisturbActive
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.DoNotDisturbActive, true); }
			set	{ SetValue((int)ClientFieldIndex.DoNotDisturbActive, value, true); }
		}

		/// <summary> The ServiceRoomActive property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."ServiceRoomActive"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ServiceRoomActive
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.ServiceRoomActive, true); }
			set	{ SetValue((int)ClientFieldIndex.ServiceRoomActive, value, true); }
		}

		/// <summary> The LastUserInteraction property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastUserInteraction"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastUserInteraction
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClientFieldIndex.LastUserInteraction, false); }
			set	{ SetValue((int)ClientFieldIndex.LastUserInteraction, value, true); }
		}

		/// <summary> The StatusUpdatedUTC property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."StatusUpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StatusUpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientFieldIndex.StatusUpdatedUTC, false); }
			set	{ SetValue((int)ClientFieldIndex.StatusUpdatedUTC, value, true); }
		}

		/// <summary> The LastWifiSsid property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastWifiSsid"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastWifiSsid
		{
			get { return (System.String)GetValue((int)ClientFieldIndex.LastWifiSsid, true); }
			set	{ SetValue((int)ClientFieldIndex.LastWifiSsid, value, true); }
		}

		/// <summary> The IsTestClient property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."IsTestClient"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTestClient
		{
			get { return (System.Boolean)GetValue((int)ClientFieldIndex.IsTestClient, true); }
			set	{ SetValue((int)ClientFieldIndex.IsTestClient, value, true); }
		}

		/// <summary> The LastApiVersion property of the Entity Client<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Client"."LastApiVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LastApiVersion
		{
			get { return (System.Int32)GetValue((int)ClientFieldIndex.LastApiVersion, true); }
			set	{ SetValue((int)ClientFieldIndex.LastApiVersion, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientEntertainmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientEntertainmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientEntertainmentCollection ClientEntertainmentCollection
		{
			get	{ return GetMultiClientEntertainmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntertainmentCollection. When set to true, ClientEntertainmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntertainmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientEntertainmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntertainmentCollection
		{
			get	{ return _alwaysFetchClientEntertainmentCollection; }
			set	{ _alwaysFetchClientEntertainmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntertainmentCollection already has been fetched. Setting this property to false when ClientEntertainmentCollection has been fetched
		/// will clear the ClientEntertainmentCollection collection well. Setting this property to true while ClientEntertainmentCollection hasn't been fetched disables lazy loading for ClientEntertainmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntertainmentCollection
		{
			get { return _alreadyFetchedClientEntertainmentCollection;}
			set 
			{
				if(_alreadyFetchedClientEntertainmentCollection && !value && (_clientEntertainmentCollection != null))
				{
					_clientEntertainmentCollection.Clear();
				}
				_alreadyFetchedClientEntertainmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientLogCollection ClientLogCollection
		{
			get	{ return GetMultiClientLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientLogCollection. When set to true, ClientLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientLogCollection
		{
			get	{ return _alwaysFetchClientLogCollection; }
			set	{ _alwaysFetchClientLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientLogCollection already has been fetched. Setting this property to false when ClientLogCollection has been fetched
		/// will clear the ClientLogCollection collection well. Setting this property to true while ClientLogCollection hasn't been fetched disables lazy loading for ClientLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientLogCollection
		{
			get { return _alreadyFetchedClientLogCollection;}
			set 
			{
				if(_alreadyFetchedClientLogCollection && !value && (_clientLogCollection != null))
				{
					_clientLogCollection.Clear();
				}
				_alreadyFetchedClientLogCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientStateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientStateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientStateCollection ClientStateCollection
		{
			get	{ return GetMultiClientStateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientStateCollection. When set to true, ClientStateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientStateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientStateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientStateCollection
		{
			get	{ return _alwaysFetchClientStateCollection; }
			set	{ _alwaysFetchClientStateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientStateCollection already has been fetched. Setting this property to false when ClientStateCollection has been fetched
		/// will clear the ClientStateCollection collection well. Setting this property to true while ClientStateCollection hasn't been fetched disables lazy loading for ClientStateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientStateCollection
		{
			get { return _alreadyFetchedClientStateCollection;}
			set 
			{
				if(_alreadyFetchedClientStateCollection && !value && (_clientStateCollection != null))
				{
					_clientStateCollection.Clear();
				}
				_alreadyFetchedClientStateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollection
		{
			get	{ return GetMultiCustomerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollection. When set to true, CustomerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollection
		{
			get	{ return _alwaysFetchCustomerCollection; }
			set	{ _alwaysFetchCustomerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollection already has been fetched. Setting this property to false when CustomerCollection has been fetched
		/// will clear the CustomerCollection collection well. Setting this property to true while CustomerCollection hasn't been fetched disables lazy loading for CustomerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollection
		{
			get { return _alreadyFetchedCustomerCollection;}
			set 
			{
				if(_alreadyFetchedCustomerCollection && !value && (_customerCollection != null))
				{
					_customerCollection.Clear();
				}
				_alreadyFetchedCustomerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GameSessionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGameSessionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GameSessionCollection GameSessionCollection
		{
			get	{ return GetMultiGameSessionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GameSessionCollection. When set to true, GameSessionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GameSessionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGameSessionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGameSessionCollection
		{
			get	{ return _alwaysFetchGameSessionCollection; }
			set	{ _alwaysFetchGameSessionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GameSessionCollection already has been fetched. Setting this property to false when GameSessionCollection has been fetched
		/// will clear the GameSessionCollection collection well. Setting this property to true while GameSessionCollection hasn't been fetched disables lazy loading for GameSessionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGameSessionCollection
		{
			get { return _alreadyFetchedGameSessionCollection;}
			set 
			{
				if(_alreadyFetchedGameSessionCollection && !value && (_gameSessionCollection != null))
				{
					_gameSessionCollection.Clear();
				}
				_alreadyFetchedGameSessionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageRecipientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection MessageRecipientCollection
		{
			get	{ return GetMultiMessageRecipientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageRecipientCollection. When set to true, MessageRecipientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageRecipientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageRecipientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageRecipientCollection
		{
			get	{ return _alwaysFetchMessageRecipientCollection; }
			set	{ _alwaysFetchMessageRecipientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageRecipientCollection already has been fetched. Setting this property to false when MessageRecipientCollection has been fetched
		/// will clear the MessageRecipientCollection collection well. Setting this property to true while MessageRecipientCollection hasn't been fetched disables lazy loading for MessageRecipientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageRecipientCollection
		{
			get { return _alreadyFetchedMessageRecipientCollection;}
			set 
			{
				if(_alreadyFetchedMessageRecipientCollection && !value && (_messageRecipientCollection != null))
				{
					_messageRecipientCollection.Clear();
				}
				_alreadyFetchedMessageRecipientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReceivedNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection ReceivedNetmessageCollection
		{
			get	{ return GetMultiReceivedNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReceivedNetmessageCollection. When set to true, ReceivedNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceivedNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReceivedNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceivedNetmessageCollection
		{
			get	{ return _alwaysFetchReceivedNetmessageCollection; }
			set	{ _alwaysFetchReceivedNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceivedNetmessageCollection already has been fetched. Setting this property to false when ReceivedNetmessageCollection has been fetched
		/// will clear the ReceivedNetmessageCollection collection well. Setting this property to true while ReceivedNetmessageCollection hasn't been fetched disables lazy loading for ReceivedNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceivedNetmessageCollection
		{
			get { return _alreadyFetchedReceivedNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedReceivedNetmessageCollection && !value && (_receivedNetmessageCollection != null))
				{
					_receivedNetmessageCollection.Clear();
				}
				_alreadyFetchedReceivedNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSentNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection SentNetmessageCollection
		{
			get	{ return GetMultiSentNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SentNetmessageCollection. When set to true, SentNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SentNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSentNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSentNetmessageCollection
		{
			get	{ return _alwaysFetchSentNetmessageCollection; }
			set	{ _alwaysFetchSentNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SentNetmessageCollection already has been fetched. Setting this property to false when SentNetmessageCollection has been fetched
		/// will clear the SentNetmessageCollection collection well. Setting this property to true while SentNetmessageCollection hasn't been fetched disables lazy loading for SentNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSentNetmessageCollection
		{
			get { return _alreadyFetchedSentNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedSentNetmessageCollection && !value && (_sentNetmessageCollection != null))
				{
					_sentNetmessageCollection.Clear();
				}
				_alreadyFetchedSentNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledCommandEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledCommandCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledCommandCollection ScheduledCommandCollection
		{
			get	{ return GetMultiScheduledCommandCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledCommandCollection. When set to true, ScheduledCommandCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledCommandCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledCommandCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledCommandCollection
		{
			get	{ return _alwaysFetchScheduledCommandCollection; }
			set	{ _alwaysFetchScheduledCommandCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledCommandCollection already has been fetched. Setting this property to false when ScheduledCommandCollection has been fetched
		/// will clear the ScheduledCommandCollection collection well. Setting this property to true while ScheduledCommandCollection hasn't been fetched disables lazy loading for ScheduledCommandCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledCommandCollection
		{
			get { return _alreadyFetchedScheduledCommandCollection;}
			set 
			{
				if(_alreadyFetchedScheduledCommandCollection && !value && (_scheduledCommandCollection != null))
				{
					_scheduledCommandCollection.Clear();
				}
				_alreadyFetchedScheduledCommandCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyResultCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection SurveyResultCollection
		{
			get	{ return GetMultiSurveyResultCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyResultCollection. When set to true, SurveyResultCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyResultCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyResultCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyResultCollection
		{
			get	{ return _alwaysFetchSurveyResultCollection; }
			set	{ _alwaysFetchSurveyResultCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyResultCollection already has been fetched. Setting this property to false when SurveyResultCollection has been fetched
		/// will clear the SurveyResultCollection collection well. Setting this property to true while SurveyResultCollection hasn't been fetched disables lazy loading for SurveyResultCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyResultCollection
		{
			get { return _alreadyFetchedSurveyResultCollection;}
			set 
			{
				if(_alreadyFetchedSurveyResultCollection && !value && (_surveyResultCollection != null))
				{
					_surveyResultCollection.Clear();
				}
				_alreadyFetchedSurveyResultCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMessage
		{
			get { return GetMultiCategoryCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMessage. When set to true, CategoryCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMessage
		{
			get	{ return _alwaysFetchCategoryCollectionViaMessage; }
			set	{ _alwaysFetchCategoryCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMessage already has been fetched. Setting this property to false when CategoryCollectionViaMessage has been fetched
		/// will clear the CategoryCollectionViaMessage collection well. Setting this property to true while CategoryCollectionViaMessage hasn't been fetched disables lazy loading for CategoryCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMessage
		{
			get { return _alreadyFetchedCategoryCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMessage && !value && (_categoryCollectionViaMessage != null))
				{
					_categoryCollectionViaMessage.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientLogFileEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientLogFileCollectionViaClientLog()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientLogFileCollection ClientLogFileCollectionViaClientLog
		{
			get { return GetMultiClientLogFileCollectionViaClientLog(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientLogFileCollectionViaClientLog. When set to true, ClientLogFileCollectionViaClientLog is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientLogFileCollectionViaClientLog is accessed. You can always execute a forced fetch by calling GetMultiClientLogFileCollectionViaClientLog(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientLogFileCollectionViaClientLog
		{
			get	{ return _alwaysFetchClientLogFileCollectionViaClientLog; }
			set	{ _alwaysFetchClientLogFileCollectionViaClientLog = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientLogFileCollectionViaClientLog already has been fetched. Setting this property to false when ClientLogFileCollectionViaClientLog has been fetched
		/// will clear the ClientLogFileCollectionViaClientLog collection well. Setting this property to true while ClientLogFileCollectionViaClientLog hasn't been fetched disables lazy loading for ClientLogFileCollectionViaClientLog</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientLogFileCollectionViaClientLog
		{
			get { return _alreadyFetchedClientLogFileCollectionViaClientLog;}
			set 
			{
				if(_alreadyFetchedClientLogFileCollectionViaClientLog && !value && (_clientLogFileCollectionViaClientLog != null))
				{
					_clientLogFileCollectionViaClientLog.Clear();
				}
				_alreadyFetchedClientLogFileCollectionViaClientLog = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaMessage
		{
			get { return GetMultiDeliverypointCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaMessage. When set to true, DeliverypointCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaMessage
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaMessage; }
			set	{ _alwaysFetchDeliverypointCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaMessage already has been fetched. Setting this property to false when DeliverypointCollectionViaMessage has been fetched
		/// will clear the DeliverypointCollectionViaMessage collection well. Setting this property to true while DeliverypointCollectionViaMessage hasn't been fetched disables lazy loading for DeliverypointCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaMessage
		{
			get { return _alreadyFetchedDeliverypointCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaMessage && !value && (_deliverypointCollectionViaMessage != null))
				{
					_deliverypointCollectionViaMessage.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaNetmessage
		{
			get { return GetMultiDeliverypointgroupCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaNetmessage. When set to true, DeliverypointgroupCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaNetmessage
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaNetmessage; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaNetmessage already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaNetmessage has been fetched
		/// will clear the DeliverypointgroupCollectionViaNetmessage collection well. Setting this property to true while DeliverypointgroupCollectionViaNetmessage hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaNetmessage
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaNetmessage && !value && (_deliverypointgroupCollectionViaNetmessage != null))
				{
					_deliverypointgroupCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessage
		{
			get { return GetMultiEntertainmentCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessage. When set to true, EntertainmentCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessage
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessage; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessage already has been fetched. Setting this property to false when EntertainmentCollectionViaMessage has been fetched
		/// will clear the EntertainmentCollectionViaMessage collection well. Setting this property to true while EntertainmentCollectionViaMessage hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessage
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessage && !value && (_entertainmentCollectionViaMessage != null))
				{
					_entertainmentCollectionViaMessage.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaMessage
		{
			get { return GetMultiMediaCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaMessage. When set to true, MediaCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaMessage
		{
			get	{ return _alwaysFetchMediaCollectionViaMessage; }
			set	{ _alwaysFetchMediaCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaMessage already has been fetched. Setting this property to false when MediaCollectionViaMessage has been fetched
		/// will clear the MediaCollectionViaMessage collection well. Setting this property to true while MediaCollectionViaMessage hasn't been fetched disables lazy loading for MediaCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaMessage
		{
			get { return _alreadyFetchedMediaCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaMessage && !value && (_mediaCollectionViaMessage != null))
				{
					_mediaCollectionViaMessage.Clear();
				}
				_alreadyFetchedMediaCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrder
		{
			get { return GetMultiOrderCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrder. When set to true, OrderCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrder
		{
			get	{ return _alwaysFetchOrderCollectionViaOrder; }
			set	{ _alwaysFetchOrderCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrder already has been fetched. Setting this property to false when OrderCollectionViaOrder has been fetched
		/// will clear the OrderCollectionViaOrder collection well. Setting this property to true while OrderCollectionViaOrder hasn't been fetched disables lazy loading for OrderCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrder
		{
			get { return _alreadyFetchedOrderCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrder && !value && (_orderCollectionViaOrder != null))
				{
					_orderCollectionViaOrder.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage
		{
			get { return GetMultiTerminalCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage. When set to true, TerminalCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage has been fetched
		/// will clear the TerminalCollectionViaNetmessage collection well. Setting this property to true while TerminalCollectionViaNetmessage hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage && !value && (_terminalCollectionViaNetmessage != null))
				{
					_terminalCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity DeliverypointEntity
		{
			get	{ return GetSingleDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientCollection", "DeliverypointEntity", _deliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity. When set to true, DeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity
		{
			get	{ return _alwaysFetchDeliverypointEntity; }
			set	{ _alwaysFetchDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity already has been fetched. Setting this property to false when DeliverypointEntity has been fetched
		/// will set DeliverypointEntity to null as well. Setting this property to true while DeliverypointEntity hasn't been fetched disables lazy loading for DeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity
		{
			get { return _alreadyFetchedDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity && !value)
				{
					this.DeliverypointEntity = null;
				}
				_alreadyFetchedDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity is not found
		/// in the database. When set to true, DeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntityReturnsNewIfNotFound; }
			set { _deliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLastDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity LastDeliverypointEntity
		{
			get	{ return GetSingleLastDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLastDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientCollection_", "LastDeliverypointEntity", _lastDeliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LastDeliverypointEntity. When set to true, LastDeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LastDeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleLastDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLastDeliverypointEntity
		{
			get	{ return _alwaysFetchLastDeliverypointEntity; }
			set	{ _alwaysFetchLastDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LastDeliverypointEntity already has been fetched. Setting this property to false when LastDeliverypointEntity has been fetched
		/// will set LastDeliverypointEntity to null as well. Setting this property to true while LastDeliverypointEntity hasn't been fetched disables lazy loading for LastDeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLastDeliverypointEntity
		{
			get { return _alreadyFetchedLastDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedLastDeliverypointEntity && !value)
				{
					this.LastDeliverypointEntity = null;
				}
				_alreadyFetchedLastDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LastDeliverypointEntity is not found
		/// in the database. When set to true, LastDeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LastDeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _lastDeliverypointEntityReturnsNewIfNotFound; }
			set { _lastDeliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeviceEntity DeviceEntity
		{
			get	{ return GetSingleDeviceEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientCollection", "DeviceEntity", _deviceEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceEntity. When set to true, DeviceEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceEntity is accessed. You can always execute a forced fetch by calling GetSingleDeviceEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceEntity
		{
			get	{ return _alwaysFetchDeviceEntity; }
			set	{ _alwaysFetchDeviceEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceEntity already has been fetched. Setting this property to false when DeviceEntity has been fetched
		/// will set DeviceEntity to null as well. Setting this property to true while DeviceEntity hasn't been fetched disables lazy loading for DeviceEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceEntity
		{
			get { return _alreadyFetchedDeviceEntity;}
			set 
			{
				if(_alreadyFetchedDeviceEntity && !value)
				{
					this.DeviceEntity = null;
				}
				_alreadyFetchedDeviceEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceEntity is not found
		/// in the database. When set to true, DeviceEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeviceEntityReturnsNewIfNotFound
		{
			get	{ return _deviceEntityReturnsNewIfNotFound; }
			set { _deviceEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlAreaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlAreaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlAreaEntity RoomControlAreaEntity
		{
			get	{ return GetSingleRoomControlAreaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlAreaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientCollection", "RoomControlAreaEntity", _roomControlAreaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlAreaEntity. When set to true, RoomControlAreaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlAreaEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlAreaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlAreaEntity
		{
			get	{ return _alwaysFetchRoomControlAreaEntity; }
			set	{ _alwaysFetchRoomControlAreaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlAreaEntity already has been fetched. Setting this property to false when RoomControlAreaEntity has been fetched
		/// will set RoomControlAreaEntity to null as well. Setting this property to true while RoomControlAreaEntity hasn't been fetched disables lazy loading for RoomControlAreaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlAreaEntity
		{
			get { return _alreadyFetchedRoomControlAreaEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlAreaEntity && !value)
				{
					this.RoomControlAreaEntity = null;
				}
				_alreadyFetchedRoomControlAreaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlAreaEntity is not found
		/// in the database. When set to true, RoomControlAreaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlAreaEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlAreaEntityReturnsNewIfNotFound; }
			set { _roomControlAreaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimestampEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimestampEntity TimestampCollection
		{
			get	{ return GetSingleTimestampCollection(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTimestampCollection(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_timestampCollection !=null);
						DesetupSyncTimestampCollection(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TimestampCollection");
						}
					}
					else
					{
						if(_timestampCollection!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "ClientEntity");
							SetupSyncTimestampCollection(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute a forced fetch by calling GetSingleTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will set TimestampCollection to null as well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value)
				{
					this.TimestampCollection = null;
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimestampCollection is not found
		/// in the database. When set to true, TimestampCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimestampCollectionReturnsNewIfNotFound
		{
			get	{ return _timestampCollectionReturnsNewIfNotFound; }
			set	{ _timestampCollectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ClientEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
