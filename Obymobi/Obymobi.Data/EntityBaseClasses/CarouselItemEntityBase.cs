﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'CarouselItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CarouselItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CarouselItemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private WidgetActionButtonEntity _widgetActionButtonEntity;
		private bool	_alwaysFetchWidgetActionButtonEntity, _alreadyFetchedWidgetActionButtonEntity, _widgetActionButtonEntityReturnsNewIfNotFound;
		private WidgetCarouselEntity _widgetCarouselEntity;
		private bool	_alwaysFetchWidgetCarouselEntity, _alreadyFetchedWidgetCarouselEntity, _widgetCarouselEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name WidgetActionButtonEntity</summary>
			public static readonly string WidgetActionButtonEntity = "WidgetActionButtonEntity";
			/// <summary>Member name WidgetCarouselEntity</summary>
			public static readonly string WidgetCarouselEntity = "WidgetCarouselEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CarouselItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CarouselItemEntityBase() :base("CarouselItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		protected CarouselItemEntityBase(System.Int32 carouselItemId):base("CarouselItemEntity")
		{
			InitClassFetch(carouselItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CarouselItemEntityBase(System.Int32 carouselItemId, IPrefetchPath prefetchPathToUse): base("CarouselItemEntity")
		{
			InitClassFetch(carouselItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="validator">The custom validator object for this CarouselItemEntity</param>
		protected CarouselItemEntityBase(System.Int32 carouselItemId, IValidator validator):base("CarouselItemEntity")
		{
			InitClassFetch(carouselItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CarouselItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");
			_widgetActionButtonEntity = (WidgetActionButtonEntity)info.GetValue("_widgetActionButtonEntity", typeof(WidgetActionButtonEntity));
			if(_widgetActionButtonEntity!=null)
			{
				_widgetActionButtonEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_widgetActionButtonEntityReturnsNewIfNotFound = info.GetBoolean("_widgetActionButtonEntityReturnsNewIfNotFound");
			_alwaysFetchWidgetActionButtonEntity = info.GetBoolean("_alwaysFetchWidgetActionButtonEntity");
			_alreadyFetchedWidgetActionButtonEntity = info.GetBoolean("_alreadyFetchedWidgetActionButtonEntity");

			_widgetCarouselEntity = (WidgetCarouselEntity)info.GetValue("_widgetCarouselEntity", typeof(WidgetCarouselEntity));
			if(_widgetCarouselEntity!=null)
			{
				_widgetCarouselEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_widgetCarouselEntityReturnsNewIfNotFound = info.GetBoolean("_widgetCarouselEntityReturnsNewIfNotFound");
			_alwaysFetchWidgetCarouselEntity = info.GetBoolean("_alwaysFetchWidgetCarouselEntity");
			_alreadyFetchedWidgetCarouselEntity = info.GetBoolean("_alreadyFetchedWidgetCarouselEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CarouselItemFieldIndex)fieldIndex)
			{
				case CarouselItemFieldIndex.WidgetCarouselId:
					DesetupSyncWidgetCarouselEntity(true, false);
					_alreadyFetchedWidgetCarouselEntity = false;
					break;
				case CarouselItemFieldIndex.WidgetActionButtonId:
					DesetupSyncWidgetActionButtonEntity(true, false);
					_alreadyFetchedWidgetActionButtonEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedWidgetActionButtonEntity = (_widgetActionButtonEntity != null);
			_alreadyFetchedWidgetCarouselEntity = (_widgetCarouselEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "WidgetActionButtonEntity":
					toReturn.Add(Relations.WidgetActionButtonEntityUsingWidgetActionButtonId);
					break;
				case "WidgetCarouselEntity":
					toReturn.Add(Relations.WidgetCarouselEntityUsingWidgetCarouselId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingCarouselItemId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingCarouselItemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_widgetActionButtonEntity", (!this.MarkedForDeletion?_widgetActionButtonEntity:null));
			info.AddValue("_widgetActionButtonEntityReturnsNewIfNotFound", _widgetActionButtonEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWidgetActionButtonEntity", _alwaysFetchWidgetActionButtonEntity);
			info.AddValue("_alreadyFetchedWidgetActionButtonEntity", _alreadyFetchedWidgetActionButtonEntity);
			info.AddValue("_widgetCarouselEntity", (!this.MarkedForDeletion?_widgetCarouselEntity:null));
			info.AddValue("_widgetCarouselEntityReturnsNewIfNotFound", _widgetCarouselEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWidgetCarouselEntity", _alwaysFetchWidgetCarouselEntity);
			info.AddValue("_alreadyFetchedWidgetCarouselEntity", _alreadyFetchedWidgetCarouselEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "WidgetActionButtonEntity":
					_alreadyFetchedWidgetActionButtonEntity = true;
					this.WidgetActionButtonEntity = (WidgetActionButtonEntity)entity;
					break;
				case "WidgetCarouselEntity":
					_alreadyFetchedWidgetCarouselEntity = true;
					this.WidgetCarouselEntity = (WidgetCarouselEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "WidgetActionButtonEntity":
					SetupSyncWidgetActionButtonEntity(relatedEntity);
					break;
				case "WidgetCarouselEntity":
					SetupSyncWidgetCarouselEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "WidgetActionButtonEntity":
					DesetupSyncWidgetActionButtonEntity(false, true);
					break;
				case "WidgetCarouselEntity":
					DesetupSyncWidgetCarouselEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_widgetActionButtonEntity!=null)
			{
				toReturn.Add(_widgetActionButtonEntity);
			}
			if(_widgetCarouselEntity!=null)
			{
				toReturn.Add(_widgetCarouselEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 carouselItemId)
		{
			return FetchUsingPK(carouselItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 carouselItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(carouselItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 carouselItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(carouselItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 carouselItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(carouselItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CarouselItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CarouselItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'WidgetActionButtonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetActionButtonEntity' which is related to this entity.</returns>
		public WidgetActionButtonEntity GetSingleWidgetActionButtonEntity()
		{
			return GetSingleWidgetActionButtonEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetActionButtonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetActionButtonEntity' which is related to this entity.</returns>
		public virtual WidgetActionButtonEntity GetSingleWidgetActionButtonEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWidgetActionButtonEntity || forceFetch || _alwaysFetchWidgetActionButtonEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetActionButtonEntityUsingWidgetActionButtonId);
				WidgetActionButtonEntity newEntity = (WidgetActionButtonEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetActionButtonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetActionButtonEntity.FetchPolymorphic(this.Transaction, this.WidgetActionButtonId.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetActionButtonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_widgetActionButtonEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WidgetActionButtonEntity = newEntity;
				_alreadyFetchedWidgetActionButtonEntity = fetchResult;
			}
			return _widgetActionButtonEntity;
		}


		/// <summary> Retrieves the related entity of type 'WidgetCarouselEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetCarouselEntity' which is related to this entity.</returns>
		public WidgetCarouselEntity GetSingleWidgetCarouselEntity()
		{
			return GetSingleWidgetCarouselEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetCarouselEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetCarouselEntity' which is related to this entity.</returns>
		public virtual WidgetCarouselEntity GetSingleWidgetCarouselEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWidgetCarouselEntity || forceFetch || _alwaysFetchWidgetCarouselEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetCarouselEntityUsingWidgetCarouselId);
				WidgetCarouselEntity newEntity = (WidgetCarouselEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetCarouselEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetCarouselEntity.FetchPolymorphic(this.Transaction, this.WidgetCarouselId, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetCarouselEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_widgetCarouselEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WidgetCarouselEntity = newEntity;
				_alreadyFetchedWidgetCarouselEntity = fetchResult;
			}
			return _widgetCarouselEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("WidgetActionButtonEntity", _widgetActionButtonEntity);
			toReturn.Add("WidgetCarouselEntity", _widgetCarouselEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="validator">The validator object for this CarouselItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 carouselItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(carouselItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "CarouselItemEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "CarouselItemEntity");
			_widgetActionButtonEntityReturnsNewIfNotFound = true;
			_widgetCarouselEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CarouselItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetCarouselId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetActionButtonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OverlayType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _widgetActionButtonEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWidgetActionButtonEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _widgetActionButtonEntity, new PropertyChangedEventHandler( OnWidgetActionButtonEntityPropertyChanged ), "WidgetActionButtonEntity", Obymobi.Data.RelationClasses.StaticCarouselItemRelations.WidgetActionButtonEntityUsingWidgetActionButtonIdStatic, true, signalRelatedEntity, "CarouselItemCollection", resetFKFields, new int[] { (int)CarouselItemFieldIndex.WidgetActionButtonId } );		
			_widgetActionButtonEntity = null;
		}
		
		/// <summary> setups the sync logic for member _widgetActionButtonEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWidgetActionButtonEntity(IEntityCore relatedEntity)
		{
			if(_widgetActionButtonEntity!=relatedEntity)
			{		
				DesetupSyncWidgetActionButtonEntity(true, true);
				_widgetActionButtonEntity = (WidgetActionButtonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _widgetActionButtonEntity, new PropertyChangedEventHandler( OnWidgetActionButtonEntityPropertyChanged ), "WidgetActionButtonEntity", Obymobi.Data.RelationClasses.StaticCarouselItemRelations.WidgetActionButtonEntityUsingWidgetActionButtonIdStatic, true, ref _alreadyFetchedWidgetActionButtonEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWidgetActionButtonEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _widgetCarouselEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWidgetCarouselEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _widgetCarouselEntity, new PropertyChangedEventHandler( OnWidgetCarouselEntityPropertyChanged ), "WidgetCarouselEntity", Obymobi.Data.RelationClasses.StaticCarouselItemRelations.WidgetCarouselEntityUsingWidgetCarouselIdStatic, true, signalRelatedEntity, "CarouselItemCollection", resetFKFields, new int[] { (int)CarouselItemFieldIndex.WidgetCarouselId } );		
			_widgetCarouselEntity = null;
		}
		
		/// <summary> setups the sync logic for member _widgetCarouselEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWidgetCarouselEntity(IEntityCore relatedEntity)
		{
			if(_widgetCarouselEntity!=relatedEntity)
			{		
				DesetupSyncWidgetCarouselEntity(true, true);
				_widgetCarouselEntity = (WidgetCarouselEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _widgetCarouselEntity, new PropertyChangedEventHandler( OnWidgetCarouselEntityPropertyChanged ), "WidgetCarouselEntity", Obymobi.Data.RelationClasses.StaticCarouselItemRelations.WidgetCarouselEntityUsingWidgetCarouselIdStatic, true, ref _alreadyFetchedWidgetCarouselEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWidgetCarouselEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 carouselItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CarouselItemFieldIndex.CarouselItemId].ForcedCurrentValueWrite(carouselItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCarouselItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CarouselItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CarouselItemRelations Relations
		{
			get	{ return new CarouselItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.CarouselItemEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.CarouselItemEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WidgetActionButton'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetActionButtonEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetActionButtonCollection(), (IEntityRelation)GetRelationsForField("WidgetActionButtonEntity")[0], (int)Obymobi.Data.EntityType.CarouselItemEntity, (int)Obymobi.Data.EntityType.WidgetActionButtonEntity, 0, null, null, null, "WidgetActionButtonEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WidgetCarousel'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetCarouselEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetCarouselCollection(), (IEntityRelation)GetRelationsForField("WidgetCarouselEntity")[0], (int)Obymobi.Data.EntityType.CarouselItemEntity, (int)Obymobi.Data.EntityType.WidgetCarouselEntity, 0, null, null, null, "WidgetCarouselEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CarouselItemId property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."CarouselItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CarouselItemId
		{
			get { return (System.Int32)GetValue((int)CarouselItemFieldIndex.CarouselItemId, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.CarouselItemId, value, true); }
		}

		/// <summary> The WidgetCarouselId property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."WidgetCarouselId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WidgetCarouselId
		{
			get { return (System.Int32)GetValue((int)CarouselItemFieldIndex.WidgetCarouselId, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.WidgetCarouselId, value, true); }
		}

		/// <summary> The WidgetActionButtonId property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."WidgetActionButtonId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WidgetActionButtonId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CarouselItemFieldIndex.WidgetActionButtonId, false); }
			set	{ SetValue((int)CarouselItemFieldIndex.WidgetActionButtonId, value, true); }
		}

		/// <summary> The Message property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."Message"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)CarouselItemFieldIndex.Message, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.Message, value, true); }
		}

		/// <summary> The SortOrder property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)CarouselItemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)CarouselItemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)CarouselItemFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)CarouselItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)CarouselItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CarouselItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CarouselItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)CarouselItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)CarouselItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Name property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CarouselItemFieldIndex.Name, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.Name, value, true); }
		}

		/// <summary> The OverlayType property of the Entity CarouselItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CarouselItem"."OverlayType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OverlayType OverlayType
		{
			get { return (Obymobi.Enums.OverlayType)GetValue((int)CarouselItemFieldIndex.OverlayType, true); }
			set	{ SetValue((int)CarouselItemFieldIndex.OverlayType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'WidgetActionButtonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWidgetActionButtonEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetActionButtonEntity WidgetActionButtonEntity
		{
			get	{ return GetSingleWidgetActionButtonEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWidgetActionButtonEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CarouselItemCollection", "WidgetActionButtonEntity", _widgetActionButtonEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetActionButtonEntity. When set to true, WidgetActionButtonEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetActionButtonEntity is accessed. You can always execute a forced fetch by calling GetSingleWidgetActionButtonEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetActionButtonEntity
		{
			get	{ return _alwaysFetchWidgetActionButtonEntity; }
			set	{ _alwaysFetchWidgetActionButtonEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetActionButtonEntity already has been fetched. Setting this property to false when WidgetActionButtonEntity has been fetched
		/// will set WidgetActionButtonEntity to null as well. Setting this property to true while WidgetActionButtonEntity hasn't been fetched disables lazy loading for WidgetActionButtonEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetActionButtonEntity
		{
			get { return _alreadyFetchedWidgetActionButtonEntity;}
			set 
			{
				if(_alreadyFetchedWidgetActionButtonEntity && !value)
				{
					this.WidgetActionButtonEntity = null;
				}
				_alreadyFetchedWidgetActionButtonEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WidgetActionButtonEntity is not found
		/// in the database. When set to true, WidgetActionButtonEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WidgetActionButtonEntityReturnsNewIfNotFound
		{
			get	{ return _widgetActionButtonEntityReturnsNewIfNotFound; }
			set { _widgetActionButtonEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WidgetCarouselEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWidgetCarouselEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetCarouselEntity WidgetCarouselEntity
		{
			get	{ return GetSingleWidgetCarouselEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWidgetCarouselEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CarouselItemCollection", "WidgetCarouselEntity", _widgetCarouselEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetCarouselEntity. When set to true, WidgetCarouselEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetCarouselEntity is accessed. You can always execute a forced fetch by calling GetSingleWidgetCarouselEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetCarouselEntity
		{
			get	{ return _alwaysFetchWidgetCarouselEntity; }
			set	{ _alwaysFetchWidgetCarouselEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetCarouselEntity already has been fetched. Setting this property to false when WidgetCarouselEntity has been fetched
		/// will set WidgetCarouselEntity to null as well. Setting this property to true while WidgetCarouselEntity hasn't been fetched disables lazy loading for WidgetCarouselEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetCarouselEntity
		{
			get { return _alreadyFetchedWidgetCarouselEntity;}
			set 
			{
				if(_alreadyFetchedWidgetCarouselEntity && !value)
				{
					this.WidgetCarouselEntity = null;
				}
				_alreadyFetchedWidgetCarouselEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WidgetCarouselEntity is not found
		/// in the database. When set to true, WidgetCarouselEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WidgetCarouselEntityReturnsNewIfNotFound
		{
			get	{ return _widgetCarouselEntityReturnsNewIfNotFound; }
			set { _widgetCarouselEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CarouselItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
