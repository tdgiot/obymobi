﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Posalteration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PosalterationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PosalterationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationCollection	_alterationCollection;
		private bool	_alwaysFetchAlterationCollection, _alreadyFetchedAlterationCollection;
		private Obymobi.Data.CollectionClasses.GenericalterationCollection _genericalterationCollectionViaAlteration;
		private bool	_alwaysFetchGenericalterationCollectionViaAlteration, _alreadyFetchedGenericalterationCollectionViaAlteration;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name GenericalterationCollectionViaAlteration</summary>
			public static readonly string GenericalterationCollectionViaAlteration = "GenericalterationCollectionViaAlteration";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PosalterationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PosalterationEntityBase() :base("PosalterationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		protected PosalterationEntityBase(System.Int32 posalterationId):base("PosalterationEntity")
		{
			InitClassFetch(posalterationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PosalterationEntityBase(System.Int32 posalterationId, IPrefetchPath prefetchPathToUse): base("PosalterationEntity")
		{
			InitClassFetch(posalterationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="validator">The custom validator object for this PosalterationEntity</param>
		protected PosalterationEntityBase(System.Int32 posalterationId, IValidator validator):base("PosalterationEntity")
		{
			InitClassFetch(posalterationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PosalterationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationCollection = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollection", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollection = info.GetBoolean("_alwaysFetchAlterationCollection");
			_alreadyFetchedAlterationCollection = info.GetBoolean("_alreadyFetchedAlterationCollection");
			_genericalterationCollectionViaAlteration = (Obymobi.Data.CollectionClasses.GenericalterationCollection)info.GetValue("_genericalterationCollectionViaAlteration", typeof(Obymobi.Data.CollectionClasses.GenericalterationCollection));
			_alwaysFetchGenericalterationCollectionViaAlteration = info.GetBoolean("_alwaysFetchGenericalterationCollectionViaAlteration");
			_alreadyFetchedGenericalterationCollectionViaAlteration = info.GetBoolean("_alreadyFetchedGenericalterationCollectionViaAlteration");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PosalterationFieldIndex)fieldIndex)
			{
				case PosalterationFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationCollection = (_alterationCollection.Count > 0);
			_alreadyFetchedGenericalterationCollectionViaAlteration = (_genericalterationCollectionViaAlteration.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "AlterationCollection":
					toReturn.Add(Relations.AlterationEntityUsingPosalterationId);
					break;
				case "GenericalterationCollectionViaAlteration":
					toReturn.Add(Relations.AlterationEntityUsingPosalterationId, "PosalterationEntity__", "Alteration_", JoinHint.None);
					toReturn.Add(AlterationEntity.Relations.GenericalterationEntityUsingGenericalterationId, "Alteration_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationCollection", (!this.MarkedForDeletion?_alterationCollection:null));
			info.AddValue("_alwaysFetchAlterationCollection", _alwaysFetchAlterationCollection);
			info.AddValue("_alreadyFetchedAlterationCollection", _alreadyFetchedAlterationCollection);
			info.AddValue("_genericalterationCollectionViaAlteration", (!this.MarkedForDeletion?_genericalterationCollectionViaAlteration:null));
			info.AddValue("_alwaysFetchGenericalterationCollectionViaAlteration", _alwaysFetchGenericalterationCollectionViaAlteration);
			info.AddValue("_alreadyFetchedGenericalterationCollectionViaAlteration", _alreadyFetchedGenericalterationCollectionViaAlteration);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "AlterationCollection":
					_alreadyFetchedAlterationCollection = true;
					if(entity!=null)
					{
						this.AlterationCollection.Add((AlterationEntity)entity);
					}
					break;
				case "GenericalterationCollectionViaAlteration":
					_alreadyFetchedGenericalterationCollectionViaAlteration = true;
					if(entity!=null)
					{
						this.GenericalterationCollectionViaAlteration.Add((GenericalterationEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "AlterationCollection":
					_alterationCollection.Add((AlterationEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "AlterationCollection":
					this.PerformRelatedEntityRemoval(_alterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posalterationId)
		{
			return FetchUsingPK(posalterationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posalterationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(posalterationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posalterationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(posalterationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 posalterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(posalterationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PosalterationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PosalterationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationCollection || forceFetch || _alwaysFetchAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollection);
				_alterationCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollection.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_alterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollection = true;
			}
			return _alterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollection'. These settings will be taken into account
		/// when the property AlterationCollection is requested or GetMultiAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollection.SortClauses=sortClauses;
			_alterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericalterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollectionViaAlteration(bool forceFetch)
		{
			return GetMultiGenericalterationCollectionViaAlteration(forceFetch, _genericalterationCollectionViaAlteration.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericalterationCollection GetMultiGenericalterationCollectionViaAlteration(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericalterationCollectionViaAlteration || forceFetch || _alwaysFetchGenericalterationCollectionViaAlteration) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericalterationCollectionViaAlteration);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PosalterationFields.PosalterationId, ComparisonOperator.Equal, this.PosalterationId, "PosalterationEntity__"));
				_genericalterationCollectionViaAlteration.SuppressClearInGetMulti=!forceFetch;
				_genericalterationCollectionViaAlteration.EntityFactoryToUse = entityFactoryToUse;
				_genericalterationCollectionViaAlteration.GetMulti(filter, GetRelationsForField("GenericalterationCollectionViaAlteration"));
				_genericalterationCollectionViaAlteration.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericalterationCollectionViaAlteration = true;
			}
			return _genericalterationCollectionViaAlteration;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericalterationCollectionViaAlteration'. These settings will be taken into account
		/// when the property GenericalterationCollectionViaAlteration is requested or GetMultiGenericalterationCollectionViaAlteration is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericalterationCollectionViaAlteration(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericalterationCollectionViaAlteration.SortClauses=sortClauses;
			_genericalterationCollectionViaAlteration.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("AlterationCollection", _alterationCollection);
			toReturn.Add("GenericalterationCollectionViaAlteration", _genericalterationCollectionViaAlteration);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="validator">The validator object for this PosalterationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 posalterationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(posalterationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationCollection = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationCollection.SetContainingEntityInfo(this, "PosAlterationEntity");
			_genericalterationCollectionViaAlteration = new Obymobi.Data.CollectionClasses.GenericalterationCollection();
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosalterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedInBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SynchronisationBatchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueCenter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPosalterationRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "PosalterationCollection", resetFKFields, new int[] { (int)PosalterationFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticPosalterationRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 posalterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PosalterationFieldIndex.PosalterationId].ForcedCurrentValueWrite(posalterationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePosalterationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PosalterationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PosalterationRelations Relations
		{
			get	{ return new PosalterationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationCollection")[0], (int)Obymobi.Data.EntityType.PosalterationEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationCollectionViaAlteration
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AlterationEntityUsingPosalterationId;
				intermediateRelation.SetAliases(string.Empty, "Alteration_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PosalterationEntity, (int)Obymobi.Data.EntityType.GenericalterationEntity, 0, null, null, GetRelationsForField("GenericalterationCollectionViaAlteration"), "GenericalterationCollectionViaAlteration", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.PosalterationEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PosalterationId property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."PosalterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PosalterationId
		{
			get { return (System.Int32)GetValue((int)PosalterationFieldIndex.PosalterationId, true); }
			set	{ SetValue((int)PosalterationFieldIndex.PosalterationId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)PosalterationFieldIndex.CompanyId, true); }
			set	{ SetValue((int)PosalterationFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The ExternalId property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ExternalId
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.ExternalId, true); }
			set	{ SetValue((int)PosalterationFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The Name property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.Name, true); }
			set	{ SetValue((int)PosalterationFieldIndex.Name, value, true); }
		}

		/// <summary> The MinOptions property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."MinOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MinOptions
		{
			get { return (Nullable<System.Int32>)GetValue((int)PosalterationFieldIndex.MinOptions, false); }
			set	{ SetValue((int)PosalterationFieldIndex.MinOptions, value, true); }
		}

		/// <summary> The MaxOptions property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."MaxOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MaxOptions
		{
			get { return (Nullable<System.Int32>)GetValue((int)PosalterationFieldIndex.MaxOptions, false); }
			set	{ SetValue((int)PosalterationFieldIndex.MaxOptions, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)PosalterationFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The CreatedInBatchId property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."CreatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PosalterationFieldIndex.CreatedInBatchId, false); }
			set	{ SetValue((int)PosalterationFieldIndex.CreatedInBatchId, value, true); }
		}

		/// <summary> The UpdatedInBatchId property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."UpdatedInBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UpdatedInBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PosalterationFieldIndex.UpdatedInBatchId, false); }
			set	{ SetValue((int)PosalterationFieldIndex.UpdatedInBatchId, value, true); }
		}

		/// <summary> The SynchronisationBatchId property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."SynchronisationBatchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SynchronisationBatchId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PosalterationFieldIndex.SynchronisationBatchId, false); }
			set	{ SetValue((int)PosalterationFieldIndex.SynchronisationBatchId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PosalterationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PosalterationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PosalterationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PosalterationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PosalterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PosalterationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PosalterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PosalterationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The RevenueCenter property of the Entity Posalteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Posalteration"."RevenueCenter"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RevenueCenter
		{
			get { return (System.String)GetValue((int)PosalterationFieldIndex.RevenueCenter, true); }
			set	{ SetValue((int)PosalterationFieldIndex.RevenueCenter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollection
		{
			get	{ return GetMultiAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollection. When set to true, AlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollection
		{
			get	{ return _alwaysFetchAlterationCollection; }
			set	{ _alwaysFetchAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollection already has been fetched. Setting this property to false when AlterationCollection has been fetched
		/// will clear the AlterationCollection collection well. Setting this property to true while AlterationCollection hasn't been fetched disables lazy loading for AlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollection
		{
			get { return _alreadyFetchedAlterationCollection;}
			set 
			{
				if(_alreadyFetchedAlterationCollection && !value && (_alterationCollection != null))
				{
					_alterationCollection.Clear();
				}
				_alreadyFetchedAlterationCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericalterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericalterationCollectionViaAlteration()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericalterationCollection GenericalterationCollectionViaAlteration
		{
			get { return GetMultiGenericalterationCollectionViaAlteration(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationCollectionViaAlteration. When set to true, GenericalterationCollectionViaAlteration is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationCollectionViaAlteration is accessed. You can always execute a forced fetch by calling GetMultiGenericalterationCollectionViaAlteration(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationCollectionViaAlteration
		{
			get	{ return _alwaysFetchGenericalterationCollectionViaAlteration; }
			set	{ _alwaysFetchGenericalterationCollectionViaAlteration = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationCollectionViaAlteration already has been fetched. Setting this property to false when GenericalterationCollectionViaAlteration has been fetched
		/// will clear the GenericalterationCollectionViaAlteration collection well. Setting this property to true while GenericalterationCollectionViaAlteration hasn't been fetched disables lazy loading for GenericalterationCollectionViaAlteration</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationCollectionViaAlteration
		{
			get { return _alreadyFetchedGenericalterationCollectionViaAlteration;}
			set 
			{
				if(_alreadyFetchedGenericalterationCollectionViaAlteration && !value && (_genericalterationCollectionViaAlteration != null))
				{
					_genericalterationCollectionViaAlteration.Clear();
				}
				_alreadyFetchedGenericalterationCollectionViaAlteration = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PosalterationCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PosalterationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
