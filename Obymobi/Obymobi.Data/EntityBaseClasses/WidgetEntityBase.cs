﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Widget'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class WidgetEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "WidgetEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.LandingPageWidgetCollection	_landingPageWidgetCollection;
		private bool	_alwaysFetchLandingPageWidgetCollection, _alreadyFetchedLandingPageWidgetCollection;
		private Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection	_navigationMenuWidgetCollection;
		private bool	_alwaysFetchNavigationMenuWidgetCollection, _alreadyFetchedNavigationMenuWidgetCollection;
		private Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection	_childWidgetCollection;
		private bool	_alwaysFetchChildWidgetCollection, _alreadyFetchedChildWidgetCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private ActionEntity _actionEntity;
		private bool	_alwaysFetchActionEntity, _alreadyFetchedActionEntity, _actionEntityReturnsNewIfNotFound;
		private ApplicationConfigurationEntity _applicationConfigurationEntity;
		private bool	_alwaysFetchApplicationConfigurationEntity, _alreadyFetchedApplicationConfigurationEntity, _applicationConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ActionEntity</summary>
			public static readonly string ActionEntity = "ActionEntity";
			/// <summary>Member name ApplicationConfigurationEntity</summary>
			public static readonly string ApplicationConfigurationEntity = "ApplicationConfigurationEntity";
			/// <summary>Member name LandingPageWidgetCollection</summary>
			public static readonly string LandingPageWidgetCollection = "LandingPageWidgetCollection";
			/// <summary>Member name NavigationMenuWidgetCollection</summary>
			public static readonly string NavigationMenuWidgetCollection = "NavigationMenuWidgetCollection";
			/// <summary>Member name ChildWidgetCollection</summary>
			public static readonly string ChildWidgetCollection = "ChildWidgetCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static WidgetEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected WidgetEntityBase() :base("WidgetEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		protected WidgetEntityBase(System.Int32 widgetId):base("WidgetEntity")
		{
			InitClassFetch(widgetId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected WidgetEntityBase(System.Int32 widgetId, IPrefetchPath prefetchPathToUse): base("WidgetEntity")
		{
			InitClassFetch(widgetId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="validator">The custom validator object for this WidgetEntity</param>
		protected WidgetEntityBase(System.Int32 widgetId, IValidator validator):base("WidgetEntity")
		{
			InitClassFetch(widgetId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WidgetEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_landingPageWidgetCollection = (Obymobi.Data.CollectionClasses.LandingPageWidgetCollection)info.GetValue("_landingPageWidgetCollection", typeof(Obymobi.Data.CollectionClasses.LandingPageWidgetCollection));
			_alwaysFetchLandingPageWidgetCollection = info.GetBoolean("_alwaysFetchLandingPageWidgetCollection");
			_alreadyFetchedLandingPageWidgetCollection = info.GetBoolean("_alreadyFetchedLandingPageWidgetCollection");

			_navigationMenuWidgetCollection = (Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection)info.GetValue("_navigationMenuWidgetCollection", typeof(Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection));
			_alwaysFetchNavigationMenuWidgetCollection = info.GetBoolean("_alwaysFetchNavigationMenuWidgetCollection");
			_alreadyFetchedNavigationMenuWidgetCollection = info.GetBoolean("_alreadyFetchedNavigationMenuWidgetCollection");

			_childWidgetCollection = (Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection)info.GetValue("_childWidgetCollection", typeof(Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection));
			_alwaysFetchChildWidgetCollection = info.GetBoolean("_alwaysFetchChildWidgetCollection");
			_alreadyFetchedChildWidgetCollection = info.GetBoolean("_alreadyFetchedChildWidgetCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");
			_actionEntity = (ActionEntity)info.GetValue("_actionEntity", typeof(ActionEntity));
			if(_actionEntity!=null)
			{
				_actionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionEntityReturnsNewIfNotFound = info.GetBoolean("_actionEntityReturnsNewIfNotFound");
			_alwaysFetchActionEntity = info.GetBoolean("_alwaysFetchActionEntity");
			_alreadyFetchedActionEntity = info.GetBoolean("_alreadyFetchedActionEntity");

			_applicationConfigurationEntity = (ApplicationConfigurationEntity)info.GetValue("_applicationConfigurationEntity", typeof(ApplicationConfigurationEntity));
			if(_applicationConfigurationEntity!=null)
			{
				_applicationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_applicationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_applicationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchApplicationConfigurationEntity = info.GetBoolean("_alwaysFetchApplicationConfigurationEntity");
			_alreadyFetchedApplicationConfigurationEntity = info.GetBoolean("_alreadyFetchedApplicationConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((WidgetFieldIndex)fieldIndex)
			{
				case WidgetFieldIndex.ApplicationConfigurationId:
					DesetupSyncApplicationConfigurationEntity(true, false);
					_alreadyFetchedApplicationConfigurationEntity = false;
					break;
				case WidgetFieldIndex.ActionId:
					DesetupSyncActionEntity(true, false);
					_alreadyFetchedActionEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLandingPageWidgetCollection = (_landingPageWidgetCollection.Count > 0);
			_alreadyFetchedNavigationMenuWidgetCollection = (_navigationMenuWidgetCollection.Count > 0);
			_alreadyFetchedChildWidgetCollection = (_childWidgetCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedActionEntity = (_actionEntity != null);
			_alreadyFetchedApplicationConfigurationEntity = (_applicationConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ActionEntity":
					toReturn.Add(Relations.ActionEntityUsingActionId);
					break;
				case "ApplicationConfigurationEntity":
					toReturn.Add(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
					break;
				case "LandingPageWidgetCollection":
					toReturn.Add(Relations.LandingPageWidgetEntityUsingWidgetId);
					break;
				case "NavigationMenuWidgetCollection":
					toReturn.Add(Relations.NavigationMenuWidgetEntityUsingWidgetId);
					break;
				case "ChildWidgetCollection":
					toReturn.Add(Relations.WidgetGroupWidgetEntityUsingWidgetId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingWidgetId);
					break;
				default:
					break;				
			}
			return toReturn;
		}

		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public  static IPredicateExpression GetEntityTypeFilter()
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("WidgetEntity", false);
		}
		
		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <param name="negate">Flag to produce a NOT filter, (true), or a normal filter (false). </param>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public  static IPredicateExpression GetEntityTypeFilter(bool negate)
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("WidgetEntity", negate);
		}

		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_landingPageWidgetCollection", (!this.MarkedForDeletion?_landingPageWidgetCollection:null));
			info.AddValue("_alwaysFetchLandingPageWidgetCollection", _alwaysFetchLandingPageWidgetCollection);
			info.AddValue("_alreadyFetchedLandingPageWidgetCollection", _alreadyFetchedLandingPageWidgetCollection);
			info.AddValue("_navigationMenuWidgetCollection", (!this.MarkedForDeletion?_navigationMenuWidgetCollection:null));
			info.AddValue("_alwaysFetchNavigationMenuWidgetCollection", _alwaysFetchNavigationMenuWidgetCollection);
			info.AddValue("_alreadyFetchedNavigationMenuWidgetCollection", _alreadyFetchedNavigationMenuWidgetCollection);
			info.AddValue("_childWidgetCollection", (!this.MarkedForDeletion?_childWidgetCollection:null));
			info.AddValue("_alwaysFetchChildWidgetCollection", _alwaysFetchChildWidgetCollection);
			info.AddValue("_alreadyFetchedChildWidgetCollection", _alreadyFetchedChildWidgetCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_actionEntity", (!this.MarkedForDeletion?_actionEntity:null));
			info.AddValue("_actionEntityReturnsNewIfNotFound", _actionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionEntity", _alwaysFetchActionEntity);
			info.AddValue("_alreadyFetchedActionEntity", _alreadyFetchedActionEntity);
			info.AddValue("_applicationConfigurationEntity", (!this.MarkedForDeletion?_applicationConfigurationEntity:null));
			info.AddValue("_applicationConfigurationEntityReturnsNewIfNotFound", _applicationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApplicationConfigurationEntity", _alwaysFetchApplicationConfigurationEntity);
			info.AddValue("_alreadyFetchedApplicationConfigurationEntity", _alreadyFetchedApplicationConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ActionEntity":
					_alreadyFetchedActionEntity = true;
					this.ActionEntity = (ActionEntity)entity;
					break;
				case "ApplicationConfigurationEntity":
					_alreadyFetchedApplicationConfigurationEntity = true;
					this.ApplicationConfigurationEntity = (ApplicationConfigurationEntity)entity;
					break;
				case "LandingPageWidgetCollection":
					_alreadyFetchedLandingPageWidgetCollection = true;
					if(entity!=null)
					{
						this.LandingPageWidgetCollection.Add((LandingPageWidgetEntity)entity);
					}
					break;
				case "NavigationMenuWidgetCollection":
					_alreadyFetchedNavigationMenuWidgetCollection = true;
					if(entity!=null)
					{
						this.NavigationMenuWidgetCollection.Add((NavigationMenuWidgetEntity)entity);
					}
					break;
				case "ChildWidgetCollection":
					_alreadyFetchedChildWidgetCollection = true;
					if(entity!=null)
					{
						this.ChildWidgetCollection.Add((WidgetGroupWidgetEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ActionEntity":
					SetupSyncActionEntity(relatedEntity);
					break;
				case "ApplicationConfigurationEntity":
					SetupSyncApplicationConfigurationEntity(relatedEntity);
					break;
				case "LandingPageWidgetCollection":
					_landingPageWidgetCollection.Add((LandingPageWidgetEntity)relatedEntity);
					break;
				case "NavigationMenuWidgetCollection":
					_navigationMenuWidgetCollection.Add((NavigationMenuWidgetEntity)relatedEntity);
					break;
				case "ChildWidgetCollection":
					_childWidgetCollection.Add((WidgetGroupWidgetEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ActionEntity":
					DesetupSyncActionEntity(false, true);
					break;
				case "ApplicationConfigurationEntity":
					DesetupSyncApplicationConfigurationEntity(false, true);
					break;
				case "LandingPageWidgetCollection":
					this.PerformRelatedEntityRemoval(_landingPageWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NavigationMenuWidgetCollection":
					this.PerformRelatedEntityRemoval(_navigationMenuWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ChildWidgetCollection":
					this.PerformRelatedEntityRemoval(_childWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_actionEntity!=null)
			{
				toReturn.Add(_actionEntity);
			}
			if(_applicationConfigurationEntity!=null)
			{
				toReturn.Add(_applicationConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_landingPageWidgetCollection);
			toReturn.Add(_navigationMenuWidgetCollection);
			toReturn.Add(_childWidgetCollection);
			toReturn.Add(_customTextCollection);

			return toReturn;
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static  WidgetEntity FetchPolymorphic(ITransaction transactionToUse, System.Int32 widgetId, Context contextToUse)
		{
			return FetchPolymorphic(transactionToUse, widgetId, contextToUse, null);
		}
				
		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static  WidgetEntity FetchPolymorphic(ITransaction transactionToUse, System.Int32 widgetId, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.WidgetEntity);
			fields.ForcedValueWrite((int)WidgetFieldIndex.WidgetId, widgetId);
			return (WidgetEntity)new WidgetDAO().FetchExistingPolymorphic(transactionToUse, fields, contextToUse, excludedIncludedFields);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetId)
		{
			return FetchUsingPK(widgetId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(widgetId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(widgetId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(widgetId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.WidgetId, null, null, null);
		}

		/// <summary>Determines whether this entity is a subType of the entity represented by the passed in enum value, which represents a value in the Obymobi.Data.EntityType enum</summary>
		/// <param name="typeOfEntity">Type of entity.</param>
		/// <returns>true if the passed in type is a supertype of this entity, otherwise false</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckIfIsSubTypeOf(int typeOfEntity)
		{
			return InheritanceInfoProviderSingleton.GetInstance().CheckIfIsSubTypeOf("WidgetEntity", ((Obymobi.Data.EntityType)typeOfEntity).ToString());
		}
				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new WidgetRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'LandingPageWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageWidgetCollection GetMultiLandingPageWidgetCollection(bool forceFetch)
		{
			return GetMultiLandingPageWidgetCollection(forceFetch, _landingPageWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LandingPageWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.LandingPageWidgetCollection GetMultiLandingPageWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLandingPageWidgetCollection(forceFetch, _landingPageWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LandingPageWidgetCollection GetMultiLandingPageWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLandingPageWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LandingPageWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.LandingPageWidgetCollection GetMultiLandingPageWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLandingPageWidgetCollection || forceFetch || _alwaysFetchLandingPageWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_landingPageWidgetCollection);
				_landingPageWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_landingPageWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_landingPageWidgetCollection.GetMultiManyToOne(null, this, filter);
				_landingPageWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedLandingPageWidgetCollection = true;
			}
			return _landingPageWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'LandingPageWidgetCollection'. These settings will be taken into account
		/// when the property LandingPageWidgetCollection is requested or GetMultiLandingPageWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLandingPageWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_landingPageWidgetCollection.SortClauses=sortClauses;
			_landingPageWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch)
		{
			return GetMultiNavigationMenuWidgetCollection(forceFetch, _navigationMenuWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNavigationMenuWidgetCollection(forceFetch, _navigationMenuWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNavigationMenuWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection GetMultiNavigationMenuWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNavigationMenuWidgetCollection || forceFetch || _alwaysFetchNavigationMenuWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_navigationMenuWidgetCollection);
				_navigationMenuWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_navigationMenuWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_navigationMenuWidgetCollection.GetMultiManyToOne(null, this, filter);
				_navigationMenuWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedNavigationMenuWidgetCollection = true;
			}
			return _navigationMenuWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'NavigationMenuWidgetCollection'. These settings will be taken into account
		/// when the property NavigationMenuWidgetCollection is requested or GetMultiNavigationMenuWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNavigationMenuWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_navigationMenuWidgetCollection.SortClauses=sortClauses;
			_navigationMenuWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WidgetGroupWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WidgetGroupWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection GetMultiChildWidgetCollection(bool forceFetch)
		{
			return GetMultiChildWidgetCollection(forceFetch, _childWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WidgetGroupWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WidgetGroupWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection GetMultiChildWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChildWidgetCollection(forceFetch, _childWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WidgetGroupWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection GetMultiChildWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChildWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WidgetGroupWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection GetMultiChildWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChildWidgetCollection || forceFetch || _alwaysFetchChildWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_childWidgetCollection);
				_childWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_childWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_childWidgetCollection.GetMultiManyToOne(this, null, filter);
				_childWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedChildWidgetCollection = true;
			}
			return _childWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ChildWidgetCollection'. These settings will be taken into account
		/// when the property ChildWidgetCollection is requested or GetMultiChildWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChildWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_childWidgetCollection.SortClauses=sortClauses;
			_childWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ActionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ActionEntity' which is related to this entity.</returns>
		public ActionEntity GetSingleActionEntity()
		{
			return GetSingleActionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ActionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ActionEntity' which is related to this entity.</returns>
		public virtual ActionEntity GetSingleActionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionEntity || forceFetch || _alwaysFetchActionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ActionEntityUsingActionId);
				ActionEntity newEntity = new ActionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ActionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionEntity = newEntity;
				_alreadyFetchedActionEntity = fetchResult;
			}
			return _actionEntity;
		}


		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity()
		{
			return GetSingleApplicationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public virtual ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedApplicationConfigurationEntity || forceFetch || _alwaysFetchApplicationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
				ApplicationConfigurationEntity newEntity = new ApplicationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApplicationConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (ApplicationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_applicationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ApplicationConfigurationEntity = newEntity;
				_alreadyFetchedApplicationConfigurationEntity = fetchResult;
			}
			return _applicationConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ActionEntity", _actionEntity);
			toReturn.Add("ApplicationConfigurationEntity", _applicationConfigurationEntity);
			toReturn.Add("LandingPageWidgetCollection", _landingPageWidgetCollection);
			toReturn.Add("NavigationMenuWidgetCollection", _navigationMenuWidgetCollection);
			toReturn.Add("ChildWidgetCollection", _childWidgetCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="validator">The validator object for this WidgetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 widgetId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(widgetId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_landingPageWidgetCollection = new Obymobi.Data.CollectionClasses.LandingPageWidgetCollection();
			_landingPageWidgetCollection.SetContainingEntityInfo(this, "WidgetEntity");

			_navigationMenuWidgetCollection = new Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection();
			_navigationMenuWidgetCollection.SetContainingEntityInfo(this, "WidgetEntity");

			_childWidgetCollection = new Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection();
			_childWidgetCollection.SetContainingEntityInfo(this, "ParentWidgetEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "WidgetEntity");
			_actionEntityReturnsNewIfNotFound = true;
			_applicationConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _actionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionEntity, new PropertyChangedEventHandler( OnActionEntityPropertyChanged ), "ActionEntity", Obymobi.Data.RelationClasses.StaticWidgetRelations.ActionEntityUsingActionIdStatic, true, signalRelatedEntity, "WidgetCollection", resetFKFields, new int[] { (int)WidgetFieldIndex.ActionId } );		
			_actionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionEntity(IEntityCore relatedEntity)
		{
			if(_actionEntity!=relatedEntity)
			{		
				DesetupSyncActionEntity(true, true);
				_actionEntity = (ActionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionEntity, new PropertyChangedEventHandler( OnActionEntityPropertyChanged ), "ActionEntity", Obymobi.Data.RelationClasses.StaticWidgetRelations.ActionEntityUsingActionIdStatic, true, ref _alreadyFetchedActionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApplicationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticWidgetRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, signalRelatedEntity, "WidgetCollection", resetFKFields, new int[] { (int)WidgetFieldIndex.ApplicationConfigurationId } );		
			_applicationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApplicationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_applicationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncApplicationConfigurationEntity(true, true);
				_applicationConfigurationEntity = (ApplicationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticWidgetRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, ref _alreadyFetchedApplicationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApplicationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="widgetId">PK value for Widget which data should be fetched into this Widget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)WidgetFieldIndex.WidgetId].ForcedCurrentValueWrite(widgetId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateWidgetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new WidgetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static WidgetRelations Relations
		{
			get	{ return new WidgetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPageWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageWidgetCollection(), (IEntityRelation)GetRelationsForField("LandingPageWidgetCollection")[0], (int)Obymobi.Data.EntityType.WidgetEntity, (int)Obymobi.Data.EntityType.LandingPageWidgetEntity, 0, null, null, null, "LandingPageWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenuWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuWidgetCollection")[0], (int)Obymobi.Data.EntityType.WidgetEntity, (int)Obymobi.Data.EntityType.NavigationMenuWidgetEntity, 0, null, null, null, "NavigationMenuWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WidgetGroupWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChildWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection(), (IEntityRelation)GetRelationsForField("ChildWidgetCollection")[0], (int)Obymobi.Data.EntityType.WidgetEntity, (int)Obymobi.Data.EntityType.WidgetGroupWidgetEntity, 0, null, null, null, "ChildWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.WidgetEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Action'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionCollection(), (IEntityRelation)GetRelationsForField("ActionEntity")[0], (int)Obymobi.Data.EntityType.WidgetEntity, (int)Obymobi.Data.EntityType.ActionEntity, 0, null, null, null, "ActionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationConfigurationCollection(), (IEntityRelation)GetRelationsForField("ApplicationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.WidgetEntity, (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, 0, null, null, null, "ApplicationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The WidgetId property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."WidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 WidgetId
		{
			get { return (System.Int32)GetValue((int)WidgetFieldIndex.WidgetId, true); }
			set	{ SetValue((int)WidgetFieldIndex.WidgetId, value, true); }
		}

		/// <summary> The ApplicationConfigurationId property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."ApplicationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApplicationConfigurationId
		{
			get { return (System.Int32)GetValue((int)WidgetFieldIndex.ApplicationConfigurationId, true); }
			set	{ SetValue((int)WidgetFieldIndex.ApplicationConfigurationId, value, true); }
		}

		/// <summary> The Name property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)WidgetFieldIndex.Name, true); }
			set	{ SetValue((int)WidgetFieldIndex.Name, value, true); }
		}

		/// <summary> The ActionId property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."ActionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)WidgetFieldIndex.ActionId, false); }
			set	{ SetValue((int)WidgetFieldIndex.ActionId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)WidgetFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)WidgetFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)WidgetFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)WidgetFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)WidgetFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)WidgetFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)WidgetFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)WidgetFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)WidgetFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)WidgetFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The Visible property of the Entity Widget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Widget"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)WidgetFieldIndex.Visible, true); }
			set	{ SetValue((int)WidgetFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'LandingPageWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLandingPageWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LandingPageWidgetCollection LandingPageWidgetCollection
		{
			get	{ return GetMultiLandingPageWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageWidgetCollection. When set to true, LandingPageWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiLandingPageWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageWidgetCollection
		{
			get	{ return _alwaysFetchLandingPageWidgetCollection; }
			set	{ _alwaysFetchLandingPageWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageWidgetCollection already has been fetched. Setting this property to false when LandingPageWidgetCollection has been fetched
		/// will clear the LandingPageWidgetCollection collection well. Setting this property to true while LandingPageWidgetCollection hasn't been fetched disables lazy loading for LandingPageWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageWidgetCollection
		{
			get { return _alreadyFetchedLandingPageWidgetCollection;}
			set 
			{
				if(_alreadyFetchedLandingPageWidgetCollection && !value && (_landingPageWidgetCollection != null))
				{
					_landingPageWidgetCollection.Clear();
				}
				_alreadyFetchedLandingPageWidgetCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NavigationMenuWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNavigationMenuWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuWidgetCollection NavigationMenuWidgetCollection
		{
			get	{ return GetMultiNavigationMenuWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuWidgetCollection. When set to true, NavigationMenuWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiNavigationMenuWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuWidgetCollection
		{
			get	{ return _alwaysFetchNavigationMenuWidgetCollection; }
			set	{ _alwaysFetchNavigationMenuWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuWidgetCollection already has been fetched. Setting this property to false when NavigationMenuWidgetCollection has been fetched
		/// will clear the NavigationMenuWidgetCollection collection well. Setting this property to true while NavigationMenuWidgetCollection hasn't been fetched disables lazy loading for NavigationMenuWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuWidgetCollection
		{
			get { return _alreadyFetchedNavigationMenuWidgetCollection;}
			set 
			{
				if(_alreadyFetchedNavigationMenuWidgetCollection && !value && (_navigationMenuWidgetCollection != null))
				{
					_navigationMenuWidgetCollection.Clear();
				}
				_alreadyFetchedNavigationMenuWidgetCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'WidgetGroupWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChildWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.WidgetGroupWidgetCollection ChildWidgetCollection
		{
			get	{ return GetMultiChildWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ChildWidgetCollection. When set to true, ChildWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ChildWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiChildWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChildWidgetCollection
		{
			get	{ return _alwaysFetchChildWidgetCollection; }
			set	{ _alwaysFetchChildWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ChildWidgetCollection already has been fetched. Setting this property to false when ChildWidgetCollection has been fetched
		/// will clear the ChildWidgetCollection collection well. Setting this property to true while ChildWidgetCollection hasn't been fetched disables lazy loading for ChildWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChildWidgetCollection
		{
			get { return _alreadyFetchedChildWidgetCollection;}
			set 
			{
				if(_alreadyFetchedChildWidgetCollection && !value && (_childWidgetCollection != null))
				{
					_childWidgetCollection.Clear();
				}
				_alreadyFetchedChildWidgetCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ActionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ActionEntity ActionEntity
		{
			get	{ return GetSingleActionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "WidgetCollection", "ActionEntity", _actionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionEntity. When set to true, ActionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionEntity is accessed. You can always execute a forced fetch by calling GetSingleActionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionEntity
		{
			get	{ return _alwaysFetchActionEntity; }
			set	{ _alwaysFetchActionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionEntity already has been fetched. Setting this property to false when ActionEntity has been fetched
		/// will set ActionEntity to null as well. Setting this property to true while ActionEntity hasn't been fetched disables lazy loading for ActionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionEntity
		{
			get { return _alreadyFetchedActionEntity;}
			set 
			{
				if(_alreadyFetchedActionEntity && !value)
				{
					this.ActionEntity = null;
				}
				_alreadyFetchedActionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionEntity is not found
		/// in the database. When set to true, ActionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionEntityReturnsNewIfNotFound
		{
			get	{ return _actionEntityReturnsNewIfNotFound; }
			set { _actionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ApplicationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApplicationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ApplicationConfigurationEntity ApplicationConfigurationEntity
		{
			get	{ return GetSingleApplicationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApplicationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "WidgetCollection", "ApplicationConfigurationEntity", _applicationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationConfigurationEntity. When set to true, ApplicationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleApplicationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationConfigurationEntity
		{
			get	{ return _alwaysFetchApplicationConfigurationEntity; }
			set	{ _alwaysFetchApplicationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationConfigurationEntity already has been fetched. Setting this property to false when ApplicationConfigurationEntity has been fetched
		/// will set ApplicationConfigurationEntity to null as well. Setting this property to true while ApplicationConfigurationEntity hasn't been fetched disables lazy loading for ApplicationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationConfigurationEntity
		{
			get { return _alreadyFetchedApplicationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedApplicationConfigurationEntity && !value)
				{
					this.ApplicationConfigurationEntity = null;
				}
				_alreadyFetchedApplicationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ApplicationConfigurationEntity is not found
		/// in the database. When set to true, ApplicationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ApplicationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _applicationConfigurationEntityReturnsNewIfNotFound; }
			set { _applicationConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.TargetPerEntity;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.WidgetEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
