﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Message'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MessageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MessageEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.MessageRecipientCollection	_messageRecipientCollection;
		private bool	_alwaysFetchMessageRecipientCollection, _alreadyFetchedMessageRecipientCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection	_scheduledMessageHistoryCollection;
		private bool	_alwaysFetchScheduledMessageHistoryCollection, _alreadyFetchedScheduledMessageHistoryCollection;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private ClientEntity _clientEntity;
		private bool	_alwaysFetchClientEntity, _alreadyFetchedClientEntity, _clientEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private CustomerEntity _customerEntity;
		private bool	_alwaysFetchCustomerEntity, _alreadyFetchedCustomerEntity, _customerEntityReturnsNewIfNotFound;
		private DeliverypointEntity _deliverypointEntity;
		private bool	_alwaysFetchDeliverypointEntity, _alreadyFetchedDeliverypointEntity, _deliverypointEntityReturnsNewIfNotFound;
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private MediaEntity _mediaEntity;
		private bool	_alwaysFetchMediaEntity, _alreadyFetchedMediaEntity, _mediaEntityReturnsNewIfNotFound;
		private OrderEntity _orderEntity;
		private bool	_alwaysFetchOrderEntity, _alreadyFetchedOrderEntity, _orderEntityReturnsNewIfNotFound;
		private PageEntity _pageEntity;
		private bool	_alwaysFetchPageEntity, _alreadyFetchedPageEntity, _pageEntityReturnsNewIfNotFound;
		private ProductCategoryEntity _productCategoryEntity;
		private bool	_alwaysFetchProductCategoryEntity, _alreadyFetchedProductCategoryEntity, _productCategoryEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name ClientEntity</summary>
			public static readonly string ClientEntity = "ClientEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name CustomerEntity</summary>
			public static readonly string CustomerEntity = "CustomerEntity";
			/// <summary>Member name DeliverypointEntity</summary>
			public static readonly string DeliverypointEntity = "DeliverypointEntity";
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name MediaEntity</summary>
			public static readonly string MediaEntity = "MediaEntity";
			/// <summary>Member name OrderEntity</summary>
			public static readonly string OrderEntity = "OrderEntity";
			/// <summary>Member name PageEntity</summary>
			public static readonly string PageEntity = "PageEntity";
			/// <summary>Member name ProductCategoryEntity</summary>
			public static readonly string ProductCategoryEntity = "ProductCategoryEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name MessageRecipientCollection</summary>
			public static readonly string MessageRecipientCollection = "MessageRecipientCollection";
			/// <summary>Member name ScheduledMessageHistoryCollection</summary>
			public static readonly string ScheduledMessageHistoryCollection = "ScheduledMessageHistoryCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MessageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MessageEntityBase() :base("MessageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		protected MessageEntityBase(System.Int32 messageId):base("MessageEntity")
		{
			InitClassFetch(messageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MessageEntityBase(System.Int32 messageId, IPrefetchPath prefetchPathToUse): base("MessageEntity")
		{
			InitClassFetch(messageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="validator">The custom validator object for this MessageEntity</param>
		protected MessageEntityBase(System.Int32 messageId, IValidator validator):base("MessageEntity")
		{
			InitClassFetch(messageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_messageRecipientCollection = (Obymobi.Data.CollectionClasses.MessageRecipientCollection)info.GetValue("_messageRecipientCollection", typeof(Obymobi.Data.CollectionClasses.MessageRecipientCollection));
			_alwaysFetchMessageRecipientCollection = info.GetBoolean("_alwaysFetchMessageRecipientCollection");
			_alreadyFetchedMessageRecipientCollection = info.GetBoolean("_alreadyFetchedMessageRecipientCollection");

			_scheduledMessageHistoryCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection)info.GetValue("_scheduledMessageHistoryCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection));
			_alwaysFetchScheduledMessageHistoryCollection = info.GetBoolean("_alwaysFetchScheduledMessageHistoryCollection");
			_alreadyFetchedScheduledMessageHistoryCollection = info.GetBoolean("_alreadyFetchedScheduledMessageHistoryCollection");
			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_clientEntity = (ClientEntity)info.GetValue("_clientEntity", typeof(ClientEntity));
			if(_clientEntity!=null)
			{
				_clientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientEntityReturnsNewIfNotFound = info.GetBoolean("_clientEntityReturnsNewIfNotFound");
			_alwaysFetchClientEntity = info.GetBoolean("_alwaysFetchClientEntity");
			_alreadyFetchedClientEntity = info.GetBoolean("_alreadyFetchedClientEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_customerEntity = (CustomerEntity)info.GetValue("_customerEntity", typeof(CustomerEntity));
			if(_customerEntity!=null)
			{
				_customerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerEntityReturnsNewIfNotFound = info.GetBoolean("_customerEntityReturnsNewIfNotFound");
			_alwaysFetchCustomerEntity = info.GetBoolean("_alwaysFetchCustomerEntity");
			_alreadyFetchedCustomerEntity = info.GetBoolean("_alreadyFetchedCustomerEntity");

			_deliverypointEntity = (DeliverypointEntity)info.GetValue("_deliverypointEntity", typeof(DeliverypointEntity));
			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity = info.GetBoolean("_alwaysFetchDeliverypointEntity");
			_alreadyFetchedDeliverypointEntity = info.GetBoolean("_alreadyFetchedDeliverypointEntity");

			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_mediaEntity = (MediaEntity)info.GetValue("_mediaEntity", typeof(MediaEntity));
			if(_mediaEntity!=null)
			{
				_mediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaEntity = info.GetBoolean("_alwaysFetchMediaEntity");
			_alreadyFetchedMediaEntity = info.GetBoolean("_alreadyFetchedMediaEntity");

			_orderEntity = (OrderEntity)info.GetValue("_orderEntity", typeof(OrderEntity));
			if(_orderEntity!=null)
			{
				_orderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderEntityReturnsNewIfNotFound = info.GetBoolean("_orderEntityReturnsNewIfNotFound");
			_alwaysFetchOrderEntity = info.GetBoolean("_alwaysFetchOrderEntity");
			_alreadyFetchedOrderEntity = info.GetBoolean("_alreadyFetchedOrderEntity");

			_pageEntity = (PageEntity)info.GetValue("_pageEntity", typeof(PageEntity));
			if(_pageEntity!=null)
			{
				_pageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageEntityReturnsNewIfNotFound = info.GetBoolean("_pageEntityReturnsNewIfNotFound");
			_alwaysFetchPageEntity = info.GetBoolean("_alwaysFetchPageEntity");
			_alreadyFetchedPageEntity = info.GetBoolean("_alreadyFetchedPageEntity");

			_productCategoryEntity = (ProductCategoryEntity)info.GetValue("_productCategoryEntity", typeof(ProductCategoryEntity));
			if(_productCategoryEntity!=null)
			{
				_productCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_productCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchProductCategoryEntity = info.GetBoolean("_alwaysFetchProductCategoryEntity");
			_alreadyFetchedProductCategoryEntity = info.GetBoolean("_alreadyFetchedProductCategoryEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MessageFieldIndex)fieldIndex)
			{
				case MessageFieldIndex.CustomerId:
					DesetupSyncCustomerEntity(true, false);
					_alreadyFetchedCustomerEntity = false;
					break;
				case MessageFieldIndex.ClientId:
					DesetupSyncClientEntity(true, false);
					_alreadyFetchedClientEntity = false;
					break;
				case MessageFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case MessageFieldIndex.OrderId:
					DesetupSyncOrderEntity(true, false);
					_alreadyFetchedOrderEntity = false;
					break;
				case MessageFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case MessageFieldIndex.MediaId:
					DesetupSyncMediaEntity(true, false);
					_alreadyFetchedMediaEntity = false;
					break;
				case MessageFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case MessageFieldIndex.DeliverypointId:
					DesetupSyncDeliverypointEntity(true, false);
					_alreadyFetchedDeliverypointEntity = false;
					break;
				case MessageFieldIndex.PageId:
					DesetupSyncPageEntity(true, false);
					_alreadyFetchedPageEntity = false;
					break;
				case MessageFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case MessageFieldIndex.ProductCategoryId:
					DesetupSyncProductCategoryEntity(true, false);
					_alreadyFetchedProductCategoryEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMessageRecipientCollection = (_messageRecipientCollection.Count > 0);
			_alreadyFetchedScheduledMessageHistoryCollection = (_scheduledMessageHistoryCollection.Count > 0);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedClientEntity = (_clientEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedCustomerEntity = (_customerEntity != null);
			_alreadyFetchedDeliverypointEntity = (_deliverypointEntity != null);
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedMediaEntity = (_mediaEntity != null);
			_alreadyFetchedOrderEntity = (_orderEntity != null);
			_alreadyFetchedPageEntity = (_pageEntity != null);
			_alreadyFetchedProductCategoryEntity = (_productCategoryEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "ClientEntity":
					toReturn.Add(Relations.ClientEntityUsingClientId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "CustomerEntity":
					toReturn.Add(Relations.CustomerEntityUsingCustomerId);
					break;
				case "DeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingDeliverypointId);
					break;
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				case "MediaEntity":
					toReturn.Add(Relations.MediaEntityUsingMediaId);
					break;
				case "OrderEntity":
					toReturn.Add(Relations.OrderEntityUsingOrderId);
					break;
				case "PageEntity":
					toReturn.Add(Relations.PageEntityUsingPageId);
					break;
				case "ProductCategoryEntity":
					toReturn.Add(Relations.ProductCategoryEntityUsingProductCategoryId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "MessageRecipientCollection":
					toReturn.Add(Relations.MessageRecipientEntityUsingMessageId);
					break;
				case "ScheduledMessageHistoryCollection":
					toReturn.Add(Relations.ScheduledMessageHistoryEntityUsingMessageId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_messageRecipientCollection", (!this.MarkedForDeletion?_messageRecipientCollection:null));
			info.AddValue("_alwaysFetchMessageRecipientCollection", _alwaysFetchMessageRecipientCollection);
			info.AddValue("_alreadyFetchedMessageRecipientCollection", _alreadyFetchedMessageRecipientCollection);
			info.AddValue("_scheduledMessageHistoryCollection", (!this.MarkedForDeletion?_scheduledMessageHistoryCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageHistoryCollection", _alwaysFetchScheduledMessageHistoryCollection);
			info.AddValue("_alreadyFetchedScheduledMessageHistoryCollection", _alreadyFetchedScheduledMessageHistoryCollection);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_clientEntity", (!this.MarkedForDeletion?_clientEntity:null));
			info.AddValue("_clientEntityReturnsNewIfNotFound", _clientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientEntity", _alwaysFetchClientEntity);
			info.AddValue("_alreadyFetchedClientEntity", _alreadyFetchedClientEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_customerEntity", (!this.MarkedForDeletion?_customerEntity:null));
			info.AddValue("_customerEntityReturnsNewIfNotFound", _customerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerEntity", _alwaysFetchCustomerEntity);
			info.AddValue("_alreadyFetchedCustomerEntity", _alreadyFetchedCustomerEntity);
			info.AddValue("_deliverypointEntity", (!this.MarkedForDeletion?_deliverypointEntity:null));
			info.AddValue("_deliverypointEntityReturnsNewIfNotFound", _deliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity", _alwaysFetchDeliverypointEntity);
			info.AddValue("_alreadyFetchedDeliverypointEntity", _alreadyFetchedDeliverypointEntity);
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_mediaEntity", (!this.MarkedForDeletion?_mediaEntity:null));
			info.AddValue("_mediaEntityReturnsNewIfNotFound", _mediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaEntity", _alwaysFetchMediaEntity);
			info.AddValue("_alreadyFetchedMediaEntity", _alreadyFetchedMediaEntity);
			info.AddValue("_orderEntity", (!this.MarkedForDeletion?_orderEntity:null));
			info.AddValue("_orderEntityReturnsNewIfNotFound", _orderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderEntity", _alwaysFetchOrderEntity);
			info.AddValue("_alreadyFetchedOrderEntity", _alreadyFetchedOrderEntity);
			info.AddValue("_pageEntity", (!this.MarkedForDeletion?_pageEntity:null));
			info.AddValue("_pageEntityReturnsNewIfNotFound", _pageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageEntity", _alwaysFetchPageEntity);
			info.AddValue("_alreadyFetchedPageEntity", _alreadyFetchedPageEntity);
			info.AddValue("_productCategoryEntity", (!this.MarkedForDeletion?_productCategoryEntity:null));
			info.AddValue("_productCategoryEntityReturnsNewIfNotFound", _productCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductCategoryEntity", _alwaysFetchProductCategoryEntity);
			info.AddValue("_alreadyFetchedProductCategoryEntity", _alreadyFetchedProductCategoryEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "ClientEntity":
					_alreadyFetchedClientEntity = true;
					this.ClientEntity = (ClientEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "CustomerEntity":
					_alreadyFetchedCustomerEntity = true;
					this.CustomerEntity = (CustomerEntity)entity;
					break;
				case "DeliverypointEntity":
					_alreadyFetchedDeliverypointEntity = true;
					this.DeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "MediaEntity":
					_alreadyFetchedMediaEntity = true;
					this.MediaEntity = (MediaEntity)entity;
					break;
				case "OrderEntity":
					_alreadyFetchedOrderEntity = true;
					this.OrderEntity = (OrderEntity)entity;
					break;
				case "PageEntity":
					_alreadyFetchedPageEntity = true;
					this.PageEntity = (PageEntity)entity;
					break;
				case "ProductCategoryEntity":
					_alreadyFetchedProductCategoryEntity = true;
					this.ProductCategoryEntity = (ProductCategoryEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "MessageRecipientCollection":
					_alreadyFetchedMessageRecipientCollection = true;
					if(entity!=null)
					{
						this.MessageRecipientCollection.Add((MessageRecipientEntity)entity);
					}
					break;
				case "ScheduledMessageHistoryCollection":
					_alreadyFetchedScheduledMessageHistoryCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageHistoryCollection.Add((ScheduledMessageHistoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "ClientEntity":
					SetupSyncClientEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "CustomerEntity":
					SetupSyncCustomerEntity(relatedEntity);
					break;
				case "DeliverypointEntity":
					SetupSyncDeliverypointEntity(relatedEntity);
					break;
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "MediaEntity":
					SetupSyncMediaEntity(relatedEntity);
					break;
				case "OrderEntity":
					SetupSyncOrderEntity(relatedEntity);
					break;
				case "PageEntity":
					SetupSyncPageEntity(relatedEntity);
					break;
				case "ProductCategoryEntity":
					SetupSyncProductCategoryEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "MessageRecipientCollection":
					_messageRecipientCollection.Add((MessageRecipientEntity)relatedEntity);
					break;
				case "ScheduledMessageHistoryCollection":
					_scheduledMessageHistoryCollection.Add((ScheduledMessageHistoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "ClientEntity":
					DesetupSyncClientEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "CustomerEntity":
					DesetupSyncCustomerEntity(false, true);
					break;
				case "DeliverypointEntity":
					DesetupSyncDeliverypointEntity(false, true);
					break;
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "MediaEntity":
					DesetupSyncMediaEntity(false, true);
					break;
				case "OrderEntity":
					DesetupSyncOrderEntity(false, true);
					break;
				case "PageEntity":
					DesetupSyncPageEntity(false, true);
					break;
				case "ProductCategoryEntity":
					DesetupSyncProductCategoryEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "MessageRecipientCollection":
					this.PerformRelatedEntityRemoval(_messageRecipientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageHistoryCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_clientEntity!=null)
			{
				toReturn.Add(_clientEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_customerEntity!=null)
			{
				toReturn.Add(_customerEntity);
			}
			if(_deliverypointEntity!=null)
			{
				toReturn.Add(_deliverypointEntity);
			}
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_mediaEntity!=null)
			{
				toReturn.Add(_mediaEntity);
			}
			if(_orderEntity!=null)
			{
				toReturn.Add(_orderEntity);
			}
			if(_pageEntity!=null)
			{
				toReturn.Add(_pageEntity);
			}
			if(_productCategoryEntity!=null)
			{
				toReturn.Add(_productCategoryEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_messageRecipientCollection);
			toReturn.Add(_scheduledMessageHistoryCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageId)
		{
			return FetchUsingPK(messageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(messageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(messageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(messageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MessageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MessageRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageRecipientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageRecipientCollection || forceFetch || _alwaysFetchMessageRecipientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageRecipientCollection);
				_messageRecipientCollection.SuppressClearInGetMulti=!forceFetch;
				_messageRecipientCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageRecipientCollection.GetMultiManyToOne(null, null, null, null, null, this, null, filter);
				_messageRecipientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageRecipientCollection = true;
			}
			return _messageRecipientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageRecipientCollection'. These settings will be taken into account
		/// when the property MessageRecipientCollection is requested or GetMultiMessageRecipientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageRecipientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageRecipientCollection.SortClauses=sortClauses;
			_messageRecipientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, _scheduledMessageHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, _scheduledMessageHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection GetMultiScheduledMessageHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageHistoryCollection || forceFetch || _alwaysFetchScheduledMessageHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageHistoryCollection);
				_scheduledMessageHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageHistoryCollection.GetMultiManyToOne(this, null, null, null, filter);
				_scheduledMessageHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageHistoryCollection = true;
			}
			return _scheduledMessageHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageHistoryCollection'. These settings will be taken into account
		/// when the property ScheduledMessageHistoryCollection is requested or GetMultiScheduledMessageHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageHistoryCollection.SortClauses=sortClauses;
			_scheduledMessageHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientEntity()
		{
			return GetSingleClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientEntity || forceFetch || _alwaysFetchClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientEntity = newEntity;
				_alreadyFetchedClientEntity = fetchResult;
			}
			return _clientEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public CustomerEntity GetSingleCustomerEntity()
		{
			return GetSingleCustomerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public virtual CustomerEntity GetSingleCustomerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerEntity || forceFetch || _alwaysFetchCustomerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerEntityUsingCustomerId);
				CustomerEntity newEntity = new CustomerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerEntity = newEntity;
				_alreadyFetchedCustomerEntity = fetchResult;
			}
			return _customerEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity()
		{
			return GetSingleDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity || forceFetch || _alwaysFetchDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointEntity = newEntity;
				_alreadyFetchedDeliverypointEntity = fetchResult;
			}
			return _deliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public MediaEntity GetSingleMediaEntity()
		{
			return GetSingleMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public virtual MediaEntity GetSingleMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaEntity || forceFetch || _alwaysFetchMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaEntityUsingMediaId);
				MediaEntity newEntity = new MediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MediaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaEntity = newEntity;
				_alreadyFetchedMediaEntity = fetchResult;
			}
			return _mediaEntity;
		}


		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrderEntity()
		{
			return GetSingleOrderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderEntity || forceFetch || _alwaysFetchOrderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderId);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderEntity = newEntity;
				_alreadyFetchedOrderEntity = fetchResult;
			}
			return _orderEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public PageEntity GetSinglePageEntity()
		{
			return GetSinglePageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public virtual PageEntity GetSinglePageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageEntity || forceFetch || _alwaysFetchPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageEntityUsingPageId);
				PageEntity newEntity = new PageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageEntity = newEntity;
				_alreadyFetchedPageEntity = fetchResult;
			}
			return _pageEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public ProductCategoryEntity GetSingleProductCategoryEntity()
		{
			return GetSingleProductCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public virtual ProductCategoryEntity GetSingleProductCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductCategoryEntity || forceFetch || _alwaysFetchProductCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductCategoryEntityUsingProductCategoryId);
				ProductCategoryEntity newEntity = new ProductCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductCategoryEntity = newEntity;
				_alreadyFetchedProductCategoryEntity = fetchResult;
			}
			return _productCategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("ClientEntity", _clientEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("CustomerEntity", _customerEntity);
			toReturn.Add("DeliverypointEntity", _deliverypointEntity);
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("MediaEntity", _mediaEntity);
			toReturn.Add("OrderEntity", _orderEntity);
			toReturn.Add("PageEntity", _pageEntity);
			toReturn.Add("ProductCategoryEntity", _productCategoryEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("MessageRecipientCollection", _messageRecipientCollection);
			toReturn.Add("ScheduledMessageHistoryCollection", _scheduledMessageHistoryCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="validator">The validator object for this MessageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 messageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(messageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_messageRecipientCollection = new Obymobi.Data.CollectionClasses.MessageRecipientCollection();
			_messageRecipientCollection.SetContainingEntityInfo(this, "MessageEntity");

			_scheduledMessageHistoryCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection();
			_scheduledMessageHistoryCollection.SetContainingEntityInfo(this, "MessageEntity");
			_categoryEntityReturnsNewIfNotFound = true;
			_clientEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_customerEntityReturnsNewIfNotFound = true;
			_deliverypointEntityReturnsNewIfNotFound = true;
			_entertainmentEntityReturnsNewIfNotFound = true;
			_mediaEntityReturnsNewIfNotFound = true;
			_orderEntityReturnsNewIfNotFound = true;
			_pageEntityReturnsNewIfNotFound = true;
			_productCategoryEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Title", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Duration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Urgent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageButtonType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Url", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Queued", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyOnYes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageLayoutType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsAsString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsOkResultCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsOkResultAsString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsYesResultCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsYesResultAsString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsNoResultCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsNoResultAsString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsTimeOutResultCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsTimeOutResultAsString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsClearManuallyResultCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecipientsClearManuallyResultAsString", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.ClientEntityUsingClientIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.ClientId } );		
			_clientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientEntity(IEntityCore relatedEntity)
		{
			if(_clientEntity!=relatedEntity)
			{		
				DesetupSyncClientEntity(true, true);
				_clientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.ClientEntityUsingClientIdStatic, true, ref _alreadyFetchedClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _customerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.CustomerEntityUsingCustomerIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.CustomerId } );		
			_customerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _customerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerEntity(IEntityCore relatedEntity)
		{
			if(_customerEntity!=relatedEntity)
			{		
				DesetupSyncCustomerEntity(true, true);
				_customerEntity = (CustomerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.CustomerEntityUsingCustomerIdStatic, true, ref _alreadyFetchedCustomerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.DeliverypointId } );		
			_deliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointEntity(true, true);
				_deliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, ref _alreadyFetchedDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.MediaEntityUsingMediaIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.MediaId } );		
			_mediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaEntity(true, true);
				_mediaEntity = (MediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.MediaEntityUsingMediaIdStatic, true, ref _alreadyFetchedMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.OrderEntityUsingOrderIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.OrderId } );		
			_orderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderEntity(IEntityCore relatedEntity)
		{
			if(_orderEntity!=relatedEntity)
			{		
				DesetupSyncOrderEntity(true, true);
				_orderEntity = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.OrderEntityUsingOrderIdStatic, true, ref _alreadyFetchedOrderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.PageEntityUsingPageIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.PageId } );		
			_pageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageEntity(IEntityCore relatedEntity)
		{
			if(_pageEntity!=relatedEntity)
			{		
				DesetupSyncPageEntity(true, true);
				_pageEntity = (PageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.PageEntityUsingPageIdStatic, true, ref _alreadyFetchedPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.ProductCategoryId } );		
			_productCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductCategoryEntity(IEntityCore relatedEntity)
		{
			if(_productCategoryEntity!=relatedEntity)
			{		
				DesetupSyncProductCategoryEntity(true, true);
				_productCategoryEntity = (ProductCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, ref _alreadyFetchedProductCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "MessageCollection", resetFKFields, new int[] { (int)MessageFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticMessageRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="messageId">PK value for Message which data should be fetched into this Message object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 messageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MessageFieldIndex.MessageId].ForcedCurrentValueWrite(messageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMessageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MessageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MessageRelations Relations
		{
			get	{ return new MessageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageRecipient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageRecipientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageRecipientCollection(), (IEntityRelation)GetRelationsForField("MessageRecipientCollection")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.MessageRecipientEntity, 0, null, null, null, "MessageRecipientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessageHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageHistoryCollection")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.ScheduledMessageHistoryEntity, 0, null, null, null, "ScheduledMessageHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), (IEntityRelation)GetRelationsForField("CustomerEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, null, "CustomerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.ProductCategoryEntity, 0, null, null, null, "ProductCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.MessageEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MessageId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."MessageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MessageId
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.MessageId, true); }
			set	{ SetValue((int)MessageFieldIndex.MessageId, value, true); }
		}

		/// <summary> The CustomerId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."CustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.CustomerId, false); }
			set	{ SetValue((int)MessageFieldIndex.CustomerId, value, true); }
		}

		/// <summary> The ClientId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.ClientId, false); }
			set	{ SetValue((int)MessageFieldIndex.ClientId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.CompanyId, true); }
			set	{ SetValue((int)MessageFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Title property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."Title"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Title
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.Title, true); }
			set	{ SetValue((int)MessageFieldIndex.Title, value, true); }
		}

		/// <summary> The Message property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."Message"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.Message, true); }
			set	{ SetValue((int)MessageFieldIndex.Message, value, true); }
		}

		/// <summary> The OrderId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.OrderId, false); }
			set	{ SetValue((int)MessageFieldIndex.OrderId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MessageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MessageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Duration property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."Duration"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Duration
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.Duration, true); }
			set	{ SetValue((int)MessageFieldIndex.Duration, value, true); }
		}

		/// <summary> The Urgent property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."Urgent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Urgent
		{
			get { return (System.Boolean)GetValue((int)MessageFieldIndex.Urgent, true); }
			set	{ SetValue((int)MessageFieldIndex.Urgent, value, true); }
		}

		/// <summary> The MessageButtonType property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."MessageButtonType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessageButtonType
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.MessageButtonType, true); }
			set	{ SetValue((int)MessageFieldIndex.MessageButtonType, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)MessageFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The MediaId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.MediaId, false); }
			set	{ SetValue((int)MessageFieldIndex.MediaId, value, true); }
		}

		/// <summary> The CategoryId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.CategoryId, false); }
			set	{ SetValue((int)MessageFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The DeliverypointId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.DeliverypointId, false); }
			set	{ SetValue((int)MessageFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The ProductId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.ProductId, false); }
			set	{ SetValue((int)MessageFieldIndex.ProductId, value, true); }
		}

		/// <summary> The Url property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."Url"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Url
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.Url, true); }
			set	{ SetValue((int)MessageFieldIndex.Url, value, true); }
		}

		/// <summary> The PageId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."PageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.PageId, false); }
			set	{ SetValue((int)MessageFieldIndex.PageId, value, true); }
		}

		/// <summary> The SiteId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.SiteId, false); }
			set	{ SetValue((int)MessageFieldIndex.SiteId, value, true); }
		}

		/// <summary> The ProductCategoryId property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."ProductCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageFieldIndex.ProductCategoryId, false); }
			set	{ SetValue((int)MessageFieldIndex.ProductCategoryId, value, true); }
		}

		/// <summary> The Queued property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."Queued"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Queued
		{
			get { return (System.Boolean)GetValue((int)MessageFieldIndex.Queued, true); }
			set	{ SetValue((int)MessageFieldIndex.Queued, value, true); }
		}

		/// <summary> The NotifyOnYes property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."NotifyOnYes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyOnYes
		{
			get { return (System.Boolean)GetValue((int)MessageFieldIndex.NotifyOnYes, true); }
			set	{ SetValue((int)MessageFieldIndex.NotifyOnYes, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MessageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MessageFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The MessageLayoutType property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."MessageLayoutType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.MessageLayoutType MessageLayoutType
		{
			get { return (Obymobi.Enums.MessageLayoutType)GetValue((int)MessageFieldIndex.MessageLayoutType, true); }
			set	{ SetValue((int)MessageFieldIndex.MessageLayoutType, value, true); }
		}

		/// <summary> The RecipientType property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.RecipientType RecipientType
		{
			get { return (Obymobi.Enums.RecipientType)GetValue((int)MessageFieldIndex.RecipientType, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientType, value, true); }
		}

		/// <summary> The RecipientsCount property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecipientsCount
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.RecipientsCount, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsCount, value, true); }
		}

		/// <summary> The RecipientsAsString property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsAsString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RecipientsAsString
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.RecipientsAsString, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsAsString, value, true); }
		}

		/// <summary> The RecipientsOkResultCount property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsOkResultCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecipientsOkResultCount
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.RecipientsOkResultCount, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsOkResultCount, value, true); }
		}

		/// <summary> The RecipientsOkResultAsString property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsOkResultAsString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RecipientsOkResultAsString
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.RecipientsOkResultAsString, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsOkResultAsString, value, true); }
		}

		/// <summary> The RecipientsYesResultCount property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsYesResultCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecipientsYesResultCount
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.RecipientsYesResultCount, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsYesResultCount, value, true); }
		}

		/// <summary> The RecipientsYesResultAsString property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsYesResultAsString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RecipientsYesResultAsString
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.RecipientsYesResultAsString, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsYesResultAsString, value, true); }
		}

		/// <summary> The RecipientsNoResultCount property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsNoResultCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecipientsNoResultCount
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.RecipientsNoResultCount, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsNoResultCount, value, true); }
		}

		/// <summary> The RecipientsNoResultAsString property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsNoResultAsString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RecipientsNoResultAsString
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.RecipientsNoResultAsString, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsNoResultAsString, value, true); }
		}

		/// <summary> The RecipientsTimeOutResultCount property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsTimeOutResultCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecipientsTimeOutResultCount
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.RecipientsTimeOutResultCount, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsTimeOutResultCount, value, true); }
		}

		/// <summary> The RecipientsTimeOutResultAsString property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsTimeOutResultAsString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RecipientsTimeOutResultAsString
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.RecipientsTimeOutResultAsString, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsTimeOutResultAsString, value, true); }
		}

		/// <summary> The RecipientsClearManuallyResultCount property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsClearManuallyResultCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RecipientsClearManuallyResultCount
		{
			get { return (System.Int32)GetValue((int)MessageFieldIndex.RecipientsClearManuallyResultCount, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsClearManuallyResultCount, value, true); }
		}

		/// <summary> The RecipientsClearManuallyResultAsString property of the Entity Message<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Message"."RecipientsClearManuallyResultAsString"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RecipientsClearManuallyResultAsString
		{
			get { return (System.String)GetValue((int)MessageFieldIndex.RecipientsClearManuallyResultAsString, true); }
			set	{ SetValue((int)MessageFieldIndex.RecipientsClearManuallyResultAsString, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageRecipientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection MessageRecipientCollection
		{
			get	{ return GetMultiMessageRecipientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageRecipientCollection. When set to true, MessageRecipientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageRecipientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageRecipientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageRecipientCollection
		{
			get	{ return _alwaysFetchMessageRecipientCollection; }
			set	{ _alwaysFetchMessageRecipientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageRecipientCollection already has been fetched. Setting this property to false when MessageRecipientCollection has been fetched
		/// will clear the MessageRecipientCollection collection well. Setting this property to true while MessageRecipientCollection hasn't been fetched disables lazy loading for MessageRecipientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageRecipientCollection
		{
			get { return _alreadyFetchedMessageRecipientCollection;}
			set 
			{
				if(_alreadyFetchedMessageRecipientCollection && !value && (_messageRecipientCollection != null))
				{
					_messageRecipientCollection.Clear();
				}
				_alreadyFetchedMessageRecipientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageHistoryCollection ScheduledMessageHistoryCollection
		{
			get	{ return GetMultiScheduledMessageHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageHistoryCollection. When set to true, ScheduledMessageHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageHistoryCollection
		{
			get	{ return _alwaysFetchScheduledMessageHistoryCollection; }
			set	{ _alwaysFetchScheduledMessageHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageHistoryCollection already has been fetched. Setting this property to false when ScheduledMessageHistoryCollection has been fetched
		/// will clear the ScheduledMessageHistoryCollection collection well. Setting this property to true while ScheduledMessageHistoryCollection hasn't been fetched disables lazy loading for ScheduledMessageHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageHistoryCollection
		{
			get { return _alreadyFetchedScheduledMessageHistoryCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageHistoryCollection && !value && (_scheduledMessageHistoryCollection != null))
				{
					_scheduledMessageHistoryCollection.Clear();
				}
				_alreadyFetchedScheduledMessageHistoryCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ClientEntity
		{
			get	{ return GetSingleClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "ClientEntity", _clientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntity. When set to true, ClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntity is accessed. You can always execute a forced fetch by calling GetSingleClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntity
		{
			get	{ return _alwaysFetchClientEntity; }
			set	{ _alwaysFetchClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntity already has been fetched. Setting this property to false when ClientEntity has been fetched
		/// will set ClientEntity to null as well. Setting this property to true while ClientEntity hasn't been fetched disables lazy loading for ClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntity
		{
			get { return _alreadyFetchedClientEntity;}
			set 
			{
				if(_alreadyFetchedClientEntity && !value)
				{
					this.ClientEntity = null;
				}
				_alreadyFetchedClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientEntity is not found
		/// in the database. When set to true, ClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientEntityReturnsNewIfNotFound
		{
			get	{ return _clientEntityReturnsNewIfNotFound; }
			set { _clientEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CustomerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CustomerEntity CustomerEntity
		{
			get	{ return GetSingleCustomerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "CustomerEntity", _customerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerEntity. When set to true, CustomerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerEntity is accessed. You can always execute a forced fetch by calling GetSingleCustomerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerEntity
		{
			get	{ return _alwaysFetchCustomerEntity; }
			set	{ _alwaysFetchCustomerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerEntity already has been fetched. Setting this property to false when CustomerEntity has been fetched
		/// will set CustomerEntity to null as well. Setting this property to true while CustomerEntity hasn't been fetched disables lazy loading for CustomerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerEntity
		{
			get { return _alreadyFetchedCustomerEntity;}
			set 
			{
				if(_alreadyFetchedCustomerEntity && !value)
				{
					this.CustomerEntity = null;
				}
				_alreadyFetchedCustomerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerEntity is not found
		/// in the database. When set to true, CustomerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CustomerEntityReturnsNewIfNotFound
		{
			get	{ return _customerEntityReturnsNewIfNotFound; }
			set { _customerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity DeliverypointEntity
		{
			get	{ return GetSingleDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "DeliverypointEntity", _deliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity. When set to true, DeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity
		{
			get	{ return _alwaysFetchDeliverypointEntity; }
			set	{ _alwaysFetchDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity already has been fetched. Setting this property to false when DeliverypointEntity has been fetched
		/// will set DeliverypointEntity to null as well. Setting this property to true while DeliverypointEntity hasn't been fetched disables lazy loading for DeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity
		{
			get { return _alreadyFetchedDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity && !value)
				{
					this.DeliverypointEntity = null;
				}
				_alreadyFetchedDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity is not found
		/// in the database. When set to true, DeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntityReturnsNewIfNotFound; }
			set { _deliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaEntity MediaEntity
		{
			get	{ return GetSingleMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "MediaEntity", _mediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaEntity. When set to true, MediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaEntity
		{
			get	{ return _alwaysFetchMediaEntity; }
			set	{ _alwaysFetchMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaEntity already has been fetched. Setting this property to false when MediaEntity has been fetched
		/// will set MediaEntity to null as well. Setting this property to true while MediaEntity hasn't been fetched disables lazy loading for MediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaEntity
		{
			get { return _alreadyFetchedMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaEntity && !value)
				{
					this.MediaEntity = null;
				}
				_alreadyFetchedMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaEntity is not found
		/// in the database. When set to true, MediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaEntityReturnsNewIfNotFound; }
			set { _mediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderEntity OrderEntity
		{
			get	{ return GetSingleOrderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "OrderEntity", _orderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderEntity. When set to true, OrderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderEntity
		{
			get	{ return _alwaysFetchOrderEntity; }
			set	{ _alwaysFetchOrderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderEntity already has been fetched. Setting this property to false when OrderEntity has been fetched
		/// will set OrderEntity to null as well. Setting this property to true while OrderEntity hasn't been fetched disables lazy loading for OrderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderEntity
		{
			get { return _alreadyFetchedOrderEntity;}
			set 
			{
				if(_alreadyFetchedOrderEntity && !value)
				{
					this.OrderEntity = null;
				}
				_alreadyFetchedOrderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderEntity is not found
		/// in the database. When set to true, OrderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderEntityReturnsNewIfNotFound
		{
			get	{ return _orderEntityReturnsNewIfNotFound; }
			set { _orderEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageEntity PageEntity
		{
			get	{ return GetSinglePageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "PageEntity", _pageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageEntity. When set to true, PageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageEntity is accessed. You can always execute a forced fetch by calling GetSinglePageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageEntity
		{
			get	{ return _alwaysFetchPageEntity; }
			set	{ _alwaysFetchPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageEntity already has been fetched. Setting this property to false when PageEntity has been fetched
		/// will set PageEntity to null as well. Setting this property to true while PageEntity hasn't been fetched disables lazy loading for PageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageEntity
		{
			get { return _alreadyFetchedPageEntity;}
			set 
			{
				if(_alreadyFetchedPageEntity && !value)
				{
					this.PageEntity = null;
				}
				_alreadyFetchedPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageEntity is not found
		/// in the database. When set to true, PageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageEntityReturnsNewIfNotFound
		{
			get	{ return _pageEntityReturnsNewIfNotFound; }
			set { _pageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductCategoryEntity ProductCategoryEntity
		{
			get	{ return GetSingleProductCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "ProductCategoryEntity", _productCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryEntity. When set to true, ProductCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleProductCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryEntity
		{
			get	{ return _alwaysFetchProductCategoryEntity; }
			set	{ _alwaysFetchProductCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryEntity already has been fetched. Setting this property to false when ProductCategoryEntity has been fetched
		/// will set ProductCategoryEntity to null as well. Setting this property to true while ProductCategoryEntity hasn't been fetched disables lazy loading for ProductCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryEntity
		{
			get { return _alreadyFetchedProductCategoryEntity;}
			set 
			{
				if(_alreadyFetchedProductCategoryEntity && !value)
				{
					this.ProductCategoryEntity = null;
				}
				_alreadyFetchedProductCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductCategoryEntity is not found
		/// in the database. When set to true, ProductCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _productCategoryEntityReturnsNewIfNotFound; }
			set { _productCategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MessageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
