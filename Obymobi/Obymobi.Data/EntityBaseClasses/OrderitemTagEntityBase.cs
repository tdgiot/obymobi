﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'OrderitemTag'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class OrderitemTagEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OrderitemTagEntity"; }
		}
	
		#region Class Member Declarations
		private OrderitemEntity _orderitemEntity;
		private bool	_alwaysFetchOrderitemEntity, _alreadyFetchedOrderitemEntity, _orderitemEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private ProductCategoryTagEntity _productCategoryTagEntity;
		private bool	_alwaysFetchProductCategoryTagEntity, _alreadyFetchedProductCategoryTagEntity, _productCategoryTagEntityReturnsNewIfNotFound;
		private TagEntity _tagEntity;
		private bool	_alwaysFetchTagEntity, _alreadyFetchedTagEntity, _tagEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OrderitemEntity</summary>
			public static readonly string OrderitemEntity = "OrderitemEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name ProductCategoryTagEntity</summary>
			public static readonly string ProductCategoryTagEntity = "ProductCategoryTagEntity";
			/// <summary>Member name TagEntity</summary>
			public static readonly string TagEntity = "TagEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderitemTagEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OrderitemTagEntityBase() :base("OrderitemTagEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		protected OrderitemTagEntityBase(System.Int32 orderitemTagId):base("OrderitemTagEntity")
		{
			InitClassFetch(orderitemTagId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OrderitemTagEntityBase(System.Int32 orderitemTagId, IPrefetchPath prefetchPathToUse): base("OrderitemTagEntity")
		{
			InitClassFetch(orderitemTagId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="validator">The custom validator object for this OrderitemTagEntity</param>
		protected OrderitemTagEntityBase(System.Int32 orderitemTagId, IValidator validator):base("OrderitemTagEntity")
		{
			InitClassFetch(orderitemTagId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemTagEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_orderitemEntity = (OrderitemEntity)info.GetValue("_orderitemEntity", typeof(OrderitemEntity));
			if(_orderitemEntity!=null)
			{
				_orderitemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderitemEntityReturnsNewIfNotFound = info.GetBoolean("_orderitemEntityReturnsNewIfNotFound");
			_alwaysFetchOrderitemEntity = info.GetBoolean("_alwaysFetchOrderitemEntity");
			_alreadyFetchedOrderitemEntity = info.GetBoolean("_alreadyFetchedOrderitemEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_productCategoryTagEntity = (ProductCategoryTagEntity)info.GetValue("_productCategoryTagEntity", typeof(ProductCategoryTagEntity));
			if(_productCategoryTagEntity!=null)
			{
				_productCategoryTagEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productCategoryTagEntityReturnsNewIfNotFound = info.GetBoolean("_productCategoryTagEntityReturnsNewIfNotFound");
			_alwaysFetchProductCategoryTagEntity = info.GetBoolean("_alwaysFetchProductCategoryTagEntity");
			_alreadyFetchedProductCategoryTagEntity = info.GetBoolean("_alreadyFetchedProductCategoryTagEntity");

			_tagEntity = (TagEntity)info.GetValue("_tagEntity", typeof(TagEntity));
			if(_tagEntity!=null)
			{
				_tagEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tagEntityReturnsNewIfNotFound = info.GetBoolean("_tagEntityReturnsNewIfNotFound");
			_alwaysFetchTagEntity = info.GetBoolean("_alwaysFetchTagEntity");
			_alreadyFetchedTagEntity = info.GetBoolean("_alreadyFetchedTagEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderitemTagFieldIndex)fieldIndex)
			{
				case OrderitemTagFieldIndex.OrderitemId:
					DesetupSyncOrderitemEntity(true, false);
					_alreadyFetchedOrderitemEntity = false;
					break;
				case OrderitemTagFieldIndex.ProductCategoryTagId:
					DesetupSyncProductCategoryTagEntity(true, false);
					_alreadyFetchedProductCategoryTagEntity = false;
					break;
				case OrderitemTagFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case OrderitemTagFieldIndex.TagId:
					DesetupSyncTagEntity(true, false);
					_alreadyFetchedTagEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrderitemEntity = (_orderitemEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedProductCategoryTagEntity = (_productCategoryTagEntity != null);
			_alreadyFetchedTagEntity = (_tagEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "OrderitemEntity":
					toReturn.Add(Relations.OrderitemEntityUsingOrderitemId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "ProductCategoryTagEntity":
					toReturn.Add(Relations.ProductCategoryTagEntityUsingProductCategoryTagId);
					break;
				case "TagEntity":
					toReturn.Add(Relations.TagEntityUsingTagId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_orderitemEntity", (!this.MarkedForDeletion?_orderitemEntity:null));
			info.AddValue("_orderitemEntityReturnsNewIfNotFound", _orderitemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderitemEntity", _alwaysFetchOrderitemEntity);
			info.AddValue("_alreadyFetchedOrderitemEntity", _alreadyFetchedOrderitemEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_productCategoryTagEntity", (!this.MarkedForDeletion?_productCategoryTagEntity:null));
			info.AddValue("_productCategoryTagEntityReturnsNewIfNotFound", _productCategoryTagEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductCategoryTagEntity", _alwaysFetchProductCategoryTagEntity);
			info.AddValue("_alreadyFetchedProductCategoryTagEntity", _alreadyFetchedProductCategoryTagEntity);
			info.AddValue("_tagEntity", (!this.MarkedForDeletion?_tagEntity:null));
			info.AddValue("_tagEntityReturnsNewIfNotFound", _tagEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTagEntity", _alwaysFetchTagEntity);
			info.AddValue("_alreadyFetchedTagEntity", _alreadyFetchedTagEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "OrderitemEntity":
					_alreadyFetchedOrderitemEntity = true;
					this.OrderitemEntity = (OrderitemEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "ProductCategoryTagEntity":
					_alreadyFetchedProductCategoryTagEntity = true;
					this.ProductCategoryTagEntity = (ProductCategoryTagEntity)entity;
					break;
				case "TagEntity":
					_alreadyFetchedTagEntity = true;
					this.TagEntity = (TagEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "OrderitemEntity":
					SetupSyncOrderitemEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "ProductCategoryTagEntity":
					SetupSyncProductCategoryTagEntity(relatedEntity);
					break;
				case "TagEntity":
					SetupSyncTagEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "OrderitemEntity":
					DesetupSyncOrderitemEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "ProductCategoryTagEntity":
					DesetupSyncProductCategoryTagEntity(false, true);
					break;
				case "TagEntity":
					DesetupSyncTagEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_orderitemEntity!=null)
			{
				toReturn.Add(_orderitemEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_productCategoryTagEntity!=null)
			{
				toReturn.Add(_productCategoryTagEntity);
			}
			if(_tagEntity!=null)
			{
				toReturn.Add(_tagEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemTagId)
		{
			return FetchUsingPK(orderitemTagId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemTagId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderitemTagId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemTagId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderitemTagId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemTagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderitemTagId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderitemTagId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderitemTagRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'OrderitemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderitemEntity' which is related to this entity.</returns>
		public OrderitemEntity GetSingleOrderitemEntity()
		{
			return GetSingleOrderitemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderitemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderitemEntity' which is related to this entity.</returns>
		public virtual OrderitemEntity GetSingleOrderitemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderitemEntity || forceFetch || _alwaysFetchOrderitemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderitemEntityUsingOrderitemId);
				OrderitemEntity newEntity = new OrderitemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderitemId);
				}
				if(fetchResult)
				{
					newEntity = (OrderitemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderitemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderitemEntity = newEntity;
				_alreadyFetchedOrderitemEntity = fetchResult;
			}
			return _orderitemEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductCategoryTagEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductCategoryTagEntity' which is related to this entity.</returns>
		public ProductCategoryTagEntity GetSingleProductCategoryTagEntity()
		{
			return GetSingleProductCategoryTagEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductCategoryTagEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductCategoryTagEntity' which is related to this entity.</returns>
		public virtual ProductCategoryTagEntity GetSingleProductCategoryTagEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductCategoryTagEntity || forceFetch || _alwaysFetchProductCategoryTagEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductCategoryTagEntityUsingProductCategoryTagId);
				ProductCategoryTagEntity newEntity = new ProductCategoryTagEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductCategoryTagId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductCategoryTagEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productCategoryTagEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductCategoryTagEntity = newEntity;
				_alreadyFetchedProductCategoryTagEntity = fetchResult;
			}
			return _productCategoryTagEntity;
		}


		/// <summary> Retrieves the related entity of type 'TagEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TagEntity' which is related to this entity.</returns>
		public TagEntity GetSingleTagEntity()
		{
			return GetSingleTagEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TagEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TagEntity' which is related to this entity.</returns>
		public virtual TagEntity GetSingleTagEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTagEntity || forceFetch || _alwaysFetchTagEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TagEntityUsingTagId);
				TagEntity newEntity = new TagEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TagId);
				}
				if(fetchResult)
				{
					newEntity = (TagEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tagEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TagEntity = newEntity;
				_alreadyFetchedTagEntity = fetchResult;
			}
			return _tagEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("OrderitemEntity", _orderitemEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("ProductCategoryTagEntity", _productCategoryTagEntity);
			toReturn.Add("TagEntity", _tagEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="validator">The validator object for this OrderitemTagEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 orderitemTagId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderitemTagId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_orderitemEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_productCategoryTagEntityReturnsNewIfNotFound = true;
			_tagEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderitemTagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductCategoryTagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _orderitemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderitemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderitemEntity, new PropertyChangedEventHandler( OnOrderitemEntityPropertyChanged ), "OrderitemEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.OrderitemEntityUsingOrderitemIdStatic, true, signalRelatedEntity, "OrderitemTagCollection", resetFKFields, new int[] { (int)OrderitemTagFieldIndex.OrderitemId } );		
			_orderitemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderitemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderitemEntity(IEntityCore relatedEntity)
		{
			if(_orderitemEntity!=relatedEntity)
			{		
				DesetupSyncOrderitemEntity(true, true);
				_orderitemEntity = (OrderitemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderitemEntity, new PropertyChangedEventHandler( OnOrderitemEntityPropertyChanged ), "OrderitemEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.OrderitemEntityUsingOrderitemIdStatic, true, ref _alreadyFetchedOrderitemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderitemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "OrderitemTagCollection", resetFKFields, new int[] { (int)OrderitemTagFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productCategoryTagEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductCategoryTagEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productCategoryTagEntity, new PropertyChangedEventHandler( OnProductCategoryTagEntityPropertyChanged ), "ProductCategoryTagEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.ProductCategoryTagEntityUsingProductCategoryTagIdStatic, true, signalRelatedEntity, "OrderitemTagCollection", resetFKFields, new int[] { (int)OrderitemTagFieldIndex.ProductCategoryTagId } );		
			_productCategoryTagEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productCategoryTagEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductCategoryTagEntity(IEntityCore relatedEntity)
		{
			if(_productCategoryTagEntity!=relatedEntity)
			{		
				DesetupSyncProductCategoryTagEntity(true, true);
				_productCategoryTagEntity = (ProductCategoryTagEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productCategoryTagEntity, new PropertyChangedEventHandler( OnProductCategoryTagEntityPropertyChanged ), "ProductCategoryTagEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.ProductCategoryTagEntityUsingProductCategoryTagIdStatic, true, ref _alreadyFetchedProductCategoryTagEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductCategoryTagEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tagEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTagEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tagEntity, new PropertyChangedEventHandler( OnTagEntityPropertyChanged ), "TagEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.TagEntityUsingTagIdStatic, true, signalRelatedEntity, "OrderitemTagCollection", resetFKFields, new int[] { (int)OrderitemTagFieldIndex.TagId } );		
			_tagEntity = null;
		}
		
		/// <summary> setups the sync logic for member _tagEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTagEntity(IEntityCore relatedEntity)
		{
			if(_tagEntity!=relatedEntity)
			{		
				DesetupSyncTagEntity(true, true);
				_tagEntity = (TagEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tagEntity, new PropertyChangedEventHandler( OnTagEntityPropertyChanged ), "TagEntity", Obymobi.Data.RelationClasses.StaticOrderitemTagRelations.TagEntityUsingTagIdStatic, true, ref _alreadyFetchedTagEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTagEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderitemTagId">PK value for OrderitemTag which data should be fetched into this OrderitemTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 orderitemTagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderitemTagFieldIndex.OrderitemTagId].ForcedCurrentValueWrite(orderitemTagId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderitemTagDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderitemTagEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderitemTagRelations Relations
		{
			get	{ return new OrderitemTagRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Orderitem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemEntity")[0], (int)Obymobi.Data.EntityType.OrderitemTagEntity, (int)Obymobi.Data.EntityType.OrderitemEntity, 0, null, null, null, "OrderitemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.OrderitemTagEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategoryTag'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryTagEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryTagCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryTagEntity")[0], (int)Obymobi.Data.EntityType.OrderitemTagEntity, (int)Obymobi.Data.EntityType.ProductCategoryTagEntity, 0, null, null, null, "ProductCategoryTagEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tag'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTagEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TagCollection(), (IEntityRelation)GetRelationsForField("TagEntity")[0], (int)Obymobi.Data.EntityType.OrderitemTagEntity, (int)Obymobi.Data.EntityType.TagEntity, 0, null, null, null, "TagEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OrderitemTagId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."OrderitemTagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderitemTagId
		{
			get { return (System.Int32)GetValue((int)OrderitemTagFieldIndex.OrderitemTagId, true); }
			set	{ SetValue((int)OrderitemTagFieldIndex.OrderitemTagId, value, true); }
		}

		/// <summary> The OrderitemId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."OrderitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemTagFieldIndex.OrderitemId, true); }
			set	{ SetValue((int)OrderitemTagFieldIndex.OrderitemId, value, true); }
		}

		/// <summary> The ProductCategoryTagId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."ProductCategoryTagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductCategoryTagId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemTagFieldIndex.ProductCategoryTagId, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.ProductCategoryTagId, value, true); }
		}

		/// <summary> The ProductId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemTagFieldIndex.ProductId, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.ProductId, value, true); }
		}

		/// <summary> The TagId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."TagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)OrderitemTagFieldIndex.TagId, true); }
			set	{ SetValue((int)OrderitemTagFieldIndex.TagId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)OrderitemTagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)OrderitemTagFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity OrderitemTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemTag"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemTagFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)OrderitemTagFieldIndex.ParentCompanyId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'OrderitemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderitemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderitemEntity OrderitemEntity
		{
			get	{ return GetSingleOrderitemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderitemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemTagCollection", "OrderitemEntity", _orderitemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemEntity. When set to true, OrderitemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderitemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemEntity
		{
			get	{ return _alwaysFetchOrderitemEntity; }
			set	{ _alwaysFetchOrderitemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemEntity already has been fetched. Setting this property to false when OrderitemEntity has been fetched
		/// will set OrderitemEntity to null as well. Setting this property to true while OrderitemEntity hasn't been fetched disables lazy loading for OrderitemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemEntity
		{
			get { return _alreadyFetchedOrderitemEntity;}
			set 
			{
				if(_alreadyFetchedOrderitemEntity && !value)
				{
					this.OrderitemEntity = null;
				}
				_alreadyFetchedOrderitemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderitemEntity is not found
		/// in the database. When set to true, OrderitemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderitemEntityReturnsNewIfNotFound
		{
			get	{ return _orderitemEntityReturnsNewIfNotFound; }
			set { _orderitemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemTagCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductCategoryTagEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductCategoryTagEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductCategoryTagEntity ProductCategoryTagEntity
		{
			get	{ return GetSingleProductCategoryTagEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductCategoryTagEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemTagCollection", "ProductCategoryTagEntity", _productCategoryTagEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryTagEntity. When set to true, ProductCategoryTagEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryTagEntity is accessed. You can always execute a forced fetch by calling GetSingleProductCategoryTagEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryTagEntity
		{
			get	{ return _alwaysFetchProductCategoryTagEntity; }
			set	{ _alwaysFetchProductCategoryTagEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryTagEntity already has been fetched. Setting this property to false when ProductCategoryTagEntity has been fetched
		/// will set ProductCategoryTagEntity to null as well. Setting this property to true while ProductCategoryTagEntity hasn't been fetched disables lazy loading for ProductCategoryTagEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryTagEntity
		{
			get { return _alreadyFetchedProductCategoryTagEntity;}
			set 
			{
				if(_alreadyFetchedProductCategoryTagEntity && !value)
				{
					this.ProductCategoryTagEntity = null;
				}
				_alreadyFetchedProductCategoryTagEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductCategoryTagEntity is not found
		/// in the database. When set to true, ProductCategoryTagEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductCategoryTagEntityReturnsNewIfNotFound
		{
			get	{ return _productCategoryTagEntityReturnsNewIfNotFound; }
			set { _productCategoryTagEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TagEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTagEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TagEntity TagEntity
		{
			get	{ return GetSingleTagEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTagEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemTagCollection", "TagEntity", _tagEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TagEntity. When set to true, TagEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TagEntity is accessed. You can always execute a forced fetch by calling GetSingleTagEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTagEntity
		{
			get	{ return _alwaysFetchTagEntity; }
			set	{ _alwaysFetchTagEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TagEntity already has been fetched. Setting this property to false when TagEntity has been fetched
		/// will set TagEntity to null as well. Setting this property to true while TagEntity hasn't been fetched disables lazy loading for TagEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTagEntity
		{
			get { return _alreadyFetchedTagEntity;}
			set 
			{
				if(_alreadyFetchedTagEntity && !value)
				{
					this.TagEntity = null;
				}
				_alreadyFetchedTagEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TagEntity is not found
		/// in the database. When set to true, TagEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TagEntityReturnsNewIfNotFound
		{
			get	{ return _tagEntityReturnsNewIfNotFound; }
			set { _tagEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OrderitemTagEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
