﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'NavigationMenuWidget'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class NavigationMenuWidgetEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "NavigationMenuWidgetEntity"; }
		}
	
		#region Class Member Declarations
		private NavigationMenuEntity _navigationMenuEntity;
		private bool	_alwaysFetchNavigationMenuEntity, _alreadyFetchedNavigationMenuEntity, _navigationMenuEntityReturnsNewIfNotFound;
		private WidgetEntity _widgetEntity;
		private bool	_alwaysFetchWidgetEntity, _alreadyFetchedWidgetEntity, _widgetEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name NavigationMenuEntity</summary>
			public static readonly string NavigationMenuEntity = "NavigationMenuEntity";
			/// <summary>Member name WidgetEntity</summary>
			public static readonly string WidgetEntity = "WidgetEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NavigationMenuWidgetEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected NavigationMenuWidgetEntityBase() :base("NavigationMenuWidgetEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		protected NavigationMenuWidgetEntityBase(System.Int32 navigationMenuId, System.Int32 widgetId):base("NavigationMenuWidgetEntity")
		{
			InitClassFetch(navigationMenuId, widgetId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected NavigationMenuWidgetEntityBase(System.Int32 navigationMenuId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse): base("NavigationMenuWidgetEntity")
		{
			InitClassFetch(navigationMenuId, widgetId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="validator">The custom validator object for this NavigationMenuWidgetEntity</param>
		protected NavigationMenuWidgetEntityBase(System.Int32 navigationMenuId, System.Int32 widgetId, IValidator validator):base("NavigationMenuWidgetEntity")
		{
			InitClassFetch(navigationMenuId, widgetId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NavigationMenuWidgetEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_navigationMenuEntity = (NavigationMenuEntity)info.GetValue("_navigationMenuEntity", typeof(NavigationMenuEntity));
			if(_navigationMenuEntity!=null)
			{
				_navigationMenuEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_navigationMenuEntityReturnsNewIfNotFound = info.GetBoolean("_navigationMenuEntityReturnsNewIfNotFound");
			_alwaysFetchNavigationMenuEntity = info.GetBoolean("_alwaysFetchNavigationMenuEntity");
			_alreadyFetchedNavigationMenuEntity = info.GetBoolean("_alreadyFetchedNavigationMenuEntity");

			_widgetEntity = (WidgetEntity)info.GetValue("_widgetEntity", typeof(WidgetEntity));
			if(_widgetEntity!=null)
			{
				_widgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_widgetEntityReturnsNewIfNotFound = info.GetBoolean("_widgetEntityReturnsNewIfNotFound");
			_alwaysFetchWidgetEntity = info.GetBoolean("_alwaysFetchWidgetEntity");
			_alreadyFetchedWidgetEntity = info.GetBoolean("_alreadyFetchedWidgetEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((NavigationMenuWidgetFieldIndex)fieldIndex)
			{
				case NavigationMenuWidgetFieldIndex.NavigationMenuId:
					DesetupSyncNavigationMenuEntity(true, false);
					_alreadyFetchedNavigationMenuEntity = false;
					break;
				case NavigationMenuWidgetFieldIndex.WidgetId:
					DesetupSyncWidgetEntity(true, false);
					_alreadyFetchedWidgetEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedNavigationMenuEntity = (_navigationMenuEntity != null);
			_alreadyFetchedWidgetEntity = (_widgetEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "NavigationMenuEntity":
					toReturn.Add(Relations.NavigationMenuEntityUsingNavigationMenuId);
					break;
				case "WidgetEntity":
					toReturn.Add(Relations.WidgetEntityUsingWidgetId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_navigationMenuEntity", (!this.MarkedForDeletion?_navigationMenuEntity:null));
			info.AddValue("_navigationMenuEntityReturnsNewIfNotFound", _navigationMenuEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNavigationMenuEntity", _alwaysFetchNavigationMenuEntity);
			info.AddValue("_alreadyFetchedNavigationMenuEntity", _alreadyFetchedNavigationMenuEntity);
			info.AddValue("_widgetEntity", (!this.MarkedForDeletion?_widgetEntity:null));
			info.AddValue("_widgetEntityReturnsNewIfNotFound", _widgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWidgetEntity", _alwaysFetchWidgetEntity);
			info.AddValue("_alreadyFetchedWidgetEntity", _alreadyFetchedWidgetEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "NavigationMenuEntity":
					_alreadyFetchedNavigationMenuEntity = true;
					this.NavigationMenuEntity = (NavigationMenuEntity)entity;
					break;
				case "WidgetEntity":
					_alreadyFetchedWidgetEntity = true;
					this.WidgetEntity = (WidgetEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "NavigationMenuEntity":
					SetupSyncNavigationMenuEntity(relatedEntity);
					break;
				case "WidgetEntity":
					SetupSyncWidgetEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "NavigationMenuEntity":
					DesetupSyncNavigationMenuEntity(false, true);
					break;
				case "WidgetEntity":
					DesetupSyncWidgetEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_navigationMenuEntity!=null)
			{
				toReturn.Add(_navigationMenuEntity);
			}
			if(_widgetEntity!=null)
			{
				toReturn.Add(_widgetEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId, System.Int32 widgetId)
		{
			return FetchUsingPK(navigationMenuId, widgetId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(navigationMenuId, widgetId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(navigationMenuId, widgetId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 navigationMenuId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(navigationMenuId, widgetId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.NavigationMenuId, this.WidgetId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NavigationMenuWidgetRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'NavigationMenuEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'NavigationMenuEntity' which is related to this entity.</returns>
		public NavigationMenuEntity GetSingleNavigationMenuEntity()
		{
			return GetSingleNavigationMenuEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'NavigationMenuEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'NavigationMenuEntity' which is related to this entity.</returns>
		public virtual NavigationMenuEntity GetSingleNavigationMenuEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedNavigationMenuEntity || forceFetch || _alwaysFetchNavigationMenuEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.NavigationMenuEntityUsingNavigationMenuId);
				NavigationMenuEntity newEntity = new NavigationMenuEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NavigationMenuId);
				}
				if(fetchResult)
				{
					newEntity = (NavigationMenuEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_navigationMenuEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.NavigationMenuEntity = newEntity;
				_alreadyFetchedNavigationMenuEntity = fetchResult;
			}
			return _navigationMenuEntity;
		}


		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public WidgetEntity GetSingleWidgetEntity()
		{
			return GetSingleWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public virtual WidgetEntity GetSingleWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWidgetEntity || forceFetch || _alwaysFetchWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetEntityUsingWidgetId);
				WidgetEntity newEntity = (WidgetEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetEntity.FetchPolymorphic(this.Transaction, this.WidgetId, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_widgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WidgetEntity = newEntity;
				_alreadyFetchedWidgetEntity = fetchResult;
			}
			return _widgetEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("NavigationMenuEntity", _navigationMenuEntity);
			toReturn.Add("WidgetEntity", _widgetEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="validator">The validator object for this NavigationMenuWidgetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 navigationMenuId, System.Int32 widgetId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(navigationMenuId, widgetId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_navigationMenuEntityReturnsNewIfNotFound = true;
			_widgetEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NavigationMenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _navigationMenuEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNavigationMenuEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _navigationMenuEntity, new PropertyChangedEventHandler( OnNavigationMenuEntityPropertyChanged ), "NavigationMenuEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuWidgetRelations.NavigationMenuEntityUsingNavigationMenuIdStatic, true, signalRelatedEntity, "NavigationMenuWidgetCollection", resetFKFields, new int[] { (int)NavigationMenuWidgetFieldIndex.NavigationMenuId } );		
			_navigationMenuEntity = null;
		}
		
		/// <summary> setups the sync logic for member _navigationMenuEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNavigationMenuEntity(IEntityCore relatedEntity)
		{
			if(_navigationMenuEntity!=relatedEntity)
			{		
				DesetupSyncNavigationMenuEntity(true, true);
				_navigationMenuEntity = (NavigationMenuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _navigationMenuEntity, new PropertyChangedEventHandler( OnNavigationMenuEntityPropertyChanged ), "NavigationMenuEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuWidgetRelations.NavigationMenuEntityUsingNavigationMenuIdStatic, true, ref _alreadyFetchedNavigationMenuEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNavigationMenuEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _widgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _widgetEntity, new PropertyChangedEventHandler( OnWidgetEntityPropertyChanged ), "WidgetEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuWidgetRelations.WidgetEntityUsingWidgetIdStatic, true, signalRelatedEntity, "NavigationMenuWidgetCollection", resetFKFields, new int[] { (int)NavigationMenuWidgetFieldIndex.WidgetId } );		
			_widgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _widgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWidgetEntity(IEntityCore relatedEntity)
		{
			if(_widgetEntity!=relatedEntity)
			{		
				DesetupSyncWidgetEntity(true, true);
				_widgetEntity = (WidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _widgetEntity, new PropertyChangedEventHandler( OnWidgetEntityPropertyChanged ), "WidgetEntity", Obymobi.Data.RelationClasses.StaticNavigationMenuWidgetRelations.WidgetEntityUsingWidgetIdStatic, true, ref _alreadyFetchedWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 navigationMenuId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)NavigationMenuWidgetFieldIndex.NavigationMenuId].ForcedCurrentValueWrite(navigationMenuId);
				this.Fields[(int)NavigationMenuWidgetFieldIndex.WidgetId].ForcedCurrentValueWrite(widgetId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateNavigationMenuWidgetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new NavigationMenuWidgetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NavigationMenuWidgetRelations Relations
		{
			get	{ return new NavigationMenuWidgetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuEntity")[0], (int)Obymobi.Data.EntityType.NavigationMenuWidgetEntity, (int)Obymobi.Data.EntityType.NavigationMenuEntity, 0, null, null, null, "NavigationMenuEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Widget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetCollection(), (IEntityRelation)GetRelationsForField("WidgetEntity")[0], (int)Obymobi.Data.EntityType.NavigationMenuWidgetEntity, (int)Obymobi.Data.EntityType.WidgetEntity, 0, null, null, null, "WidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The NavigationMenuId property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."NavigationMenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 NavigationMenuId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuWidgetFieldIndex.NavigationMenuId, true); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.NavigationMenuId, value, true); }
		}

		/// <summary> The WidgetId property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."WidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 WidgetId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuWidgetFieldIndex.WidgetId, true); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.WidgetId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)NavigationMenuWidgetFieldIndex.SortOrder, true); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)NavigationMenuWidgetFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)NavigationMenuWidgetFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)NavigationMenuWidgetFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NavigationMenuWidgetFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity NavigationMenuWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "NavigationMenuWidget"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)NavigationMenuWidgetFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)NavigationMenuWidgetFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'NavigationMenuEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNavigationMenuEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual NavigationMenuEntity NavigationMenuEntity
		{
			get	{ return GetSingleNavigationMenuEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNavigationMenuEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NavigationMenuWidgetCollection", "NavigationMenuEntity", _navigationMenuEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuEntity. When set to true, NavigationMenuEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuEntity is accessed. You can always execute a forced fetch by calling GetSingleNavigationMenuEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuEntity
		{
			get	{ return _alwaysFetchNavigationMenuEntity; }
			set	{ _alwaysFetchNavigationMenuEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuEntity already has been fetched. Setting this property to false when NavigationMenuEntity has been fetched
		/// will set NavigationMenuEntity to null as well. Setting this property to true while NavigationMenuEntity hasn't been fetched disables lazy loading for NavigationMenuEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuEntity
		{
			get { return _alreadyFetchedNavigationMenuEntity;}
			set 
			{
				if(_alreadyFetchedNavigationMenuEntity && !value)
				{
					this.NavigationMenuEntity = null;
				}
				_alreadyFetchedNavigationMenuEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property NavigationMenuEntity is not found
		/// in the database. When set to true, NavigationMenuEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool NavigationMenuEntityReturnsNewIfNotFound
		{
			get	{ return _navigationMenuEntityReturnsNewIfNotFound; }
			set { _navigationMenuEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetEntity WidgetEntity
		{
			get	{ return GetSingleWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NavigationMenuWidgetCollection", "WidgetEntity", _widgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetEntity. When set to true, WidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetEntity
		{
			get	{ return _alwaysFetchWidgetEntity; }
			set	{ _alwaysFetchWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetEntity already has been fetched. Setting this property to false when WidgetEntity has been fetched
		/// will set WidgetEntity to null as well. Setting this property to true while WidgetEntity hasn't been fetched disables lazy loading for WidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetEntity
		{
			get { return _alreadyFetchedWidgetEntity;}
			set 
			{
				if(_alreadyFetchedWidgetEntity && !value)
				{
					this.WidgetEntity = null;
				}
				_alreadyFetchedWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WidgetEntity is not found
		/// in the database. When set to true, WidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WidgetEntityReturnsNewIfNotFound
		{
			get	{ return _widgetEntityReturnsNewIfNotFound; }
			set { _widgetEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.NavigationMenuWidgetEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
