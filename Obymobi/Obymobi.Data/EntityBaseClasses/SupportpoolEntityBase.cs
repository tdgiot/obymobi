﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Supportpool'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SupportpoolEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SupportpoolEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CompanyCollection	_companyCollection;
		private bool	_alwaysFetchCompanyCollection, _alreadyFetchedCompanyCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection	_orderRoutestephandlerCollection;
		private bool	_alwaysFetchOrderRoutestephandlerCollection, _alreadyFetchedOrderRoutestephandlerCollection;
		private Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection	_orderRoutestephandlerHistoryCollection;
		private bool	_alwaysFetchOrderRoutestephandlerHistoryCollection, _alreadyFetchedOrderRoutestephandlerHistoryCollection;
		private Obymobi.Data.CollectionClasses.RoutestephandlerCollection	_routestephandlerCollection;
		private bool	_alwaysFetchRoutestephandlerCollection, _alreadyFetchedRoutestephandlerCollection;
		private Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection	_supportpoolNotificationRecipientCollection;
		private bool	_alwaysFetchSupportpoolNotificationRecipientCollection, _alreadyFetchedSupportpoolNotificationRecipientCollection;
		private Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection	_supportpoolSupportagentCollection;
		private bool	_alwaysFetchSupportpoolSupportagentCollection, _alreadyFetchedSupportpoolSupportagentCollection;
		private Obymobi.Data.CollectionClasses.CompanyOwnerCollection _companyOwnerCollectionViaCompany;
		private bool	_alwaysFetchCompanyOwnerCollectionViaCompany, _alreadyFetchedCompanyOwnerCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.CountryCollection _countryCollectionViaCompany;
		private bool	_alwaysFetchCountryCollectionViaCompany, _alreadyFetchedCountryCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.CurrencyCollection _currencyCollectionViaCompany;
		private bool	_alwaysFetchCurrencyCollectionViaCompany, _alreadyFetchedCurrencyCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.LanguageCollection _languageCollectionViaCompany;
		private bool	_alwaysFetchLanguageCollectionViaCompany, _alreadyFetchedLanguageCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrderRoutestephandler;
		private bool	_alwaysFetchOrderCollectionViaOrderRoutestephandler, _alreadyFetchedOrderCollectionViaOrderRoutestephandler;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrderRoutestephandlerHistory;
		private bool	_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory, _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCompany;
		private bool	_alwaysFetchRouteCollectionViaCompany, _alreadyFetchedRouteCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.RoutestepCollection _routestepCollectionViaRoutestephandler;
		private bool	_alwaysFetchRoutestepCollectionViaRoutestephandler, _alreadyFetchedRoutestepCollectionViaRoutestephandler;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandler;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandler, _alreadyFetchedTerminalCollectionViaOrderRoutestephandler;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandler_;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandler_, _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandlerHistory;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory, _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaOrderRoutestephandlerHistory_;
		private bool	_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_, _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaRoutestephandler;
		private bool	_alwaysFetchTerminalCollectionViaRoutestephandler, _alreadyFetchedTerminalCollectionViaRoutestephandler;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name OrderRoutestephandlerCollection</summary>
			public static readonly string OrderRoutestephandlerCollection = "OrderRoutestephandlerCollection";
			/// <summary>Member name OrderRoutestephandlerHistoryCollection</summary>
			public static readonly string OrderRoutestephandlerHistoryCollection = "OrderRoutestephandlerHistoryCollection";
			/// <summary>Member name RoutestephandlerCollection</summary>
			public static readonly string RoutestephandlerCollection = "RoutestephandlerCollection";
			/// <summary>Member name SupportpoolNotificationRecipientCollection</summary>
			public static readonly string SupportpoolNotificationRecipientCollection = "SupportpoolNotificationRecipientCollection";
			/// <summary>Member name SupportpoolSupportagentCollection</summary>
			public static readonly string SupportpoolSupportagentCollection = "SupportpoolSupportagentCollection";
			/// <summary>Member name CompanyOwnerCollectionViaCompany</summary>
			public static readonly string CompanyOwnerCollectionViaCompany = "CompanyOwnerCollectionViaCompany";
			/// <summary>Member name CountryCollectionViaCompany</summary>
			public static readonly string CountryCollectionViaCompany = "CountryCollectionViaCompany";
			/// <summary>Member name CurrencyCollectionViaCompany</summary>
			public static readonly string CurrencyCollectionViaCompany = "CurrencyCollectionViaCompany";
			/// <summary>Member name LanguageCollectionViaCompany</summary>
			public static readonly string LanguageCollectionViaCompany = "LanguageCollectionViaCompany";
			/// <summary>Member name OrderCollectionViaOrderRoutestephandler</summary>
			public static readonly string OrderCollectionViaOrderRoutestephandler = "OrderCollectionViaOrderRoutestephandler";
			/// <summary>Member name OrderCollectionViaOrderRoutestephandlerHistory</summary>
			public static readonly string OrderCollectionViaOrderRoutestephandlerHistory = "OrderCollectionViaOrderRoutestephandlerHistory";
			/// <summary>Member name RouteCollectionViaCompany</summary>
			public static readonly string RouteCollectionViaCompany = "RouteCollectionViaCompany";
			/// <summary>Member name RoutestepCollectionViaRoutestephandler</summary>
			public static readonly string RoutestepCollectionViaRoutestephandler = "RoutestepCollectionViaRoutestephandler";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandler</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandler = "TerminalCollectionViaOrderRoutestephandler";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandler_</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandler_ = "TerminalCollectionViaOrderRoutestephandler_";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandlerHistory</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandlerHistory = "TerminalCollectionViaOrderRoutestephandlerHistory";
			/// <summary>Member name TerminalCollectionViaOrderRoutestephandlerHistory_</summary>
			public static readonly string TerminalCollectionViaOrderRoutestephandlerHistory_ = "TerminalCollectionViaOrderRoutestephandlerHistory_";
			/// <summary>Member name TerminalCollectionViaRoutestephandler</summary>
			public static readonly string TerminalCollectionViaRoutestephandler = "TerminalCollectionViaRoutestephandler";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SupportpoolEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SupportpoolEntityBase() :base("SupportpoolEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		protected SupportpoolEntityBase(System.Int32 supportpoolId):base("SupportpoolEntity")
		{
			InitClassFetch(supportpoolId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SupportpoolEntityBase(System.Int32 supportpoolId, IPrefetchPath prefetchPathToUse): base("SupportpoolEntity")
		{
			InitClassFetch(supportpoolId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="validator">The custom validator object for this SupportpoolEntity</param>
		protected SupportpoolEntityBase(System.Int32 supportpoolId, IValidator validator):base("SupportpoolEntity")
		{
			InitClassFetch(supportpoolId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportpoolEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_companyCollection = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollection", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollection = info.GetBoolean("_alwaysFetchCompanyCollection");
			_alreadyFetchedCompanyCollection = info.GetBoolean("_alreadyFetchedCompanyCollection");

			_orderRoutestephandlerCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection)info.GetValue("_orderRoutestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection));
			_alwaysFetchOrderRoutestephandlerCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerCollection");
			_alreadyFetchedOrderRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerCollection");

			_orderRoutestephandlerHistoryCollection = (Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection)info.GetValue("_orderRoutestephandlerHistoryCollection", typeof(Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection));
			_alwaysFetchOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alwaysFetchOrderRoutestephandlerHistoryCollection");
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = info.GetBoolean("_alreadyFetchedOrderRoutestephandlerHistoryCollection");

			_routestephandlerCollection = (Obymobi.Data.CollectionClasses.RoutestephandlerCollection)info.GetValue("_routestephandlerCollection", typeof(Obymobi.Data.CollectionClasses.RoutestephandlerCollection));
			_alwaysFetchRoutestephandlerCollection = info.GetBoolean("_alwaysFetchRoutestephandlerCollection");
			_alreadyFetchedRoutestephandlerCollection = info.GetBoolean("_alreadyFetchedRoutestephandlerCollection");

			_supportpoolNotificationRecipientCollection = (Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection)info.GetValue("_supportpoolNotificationRecipientCollection", typeof(Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection));
			_alwaysFetchSupportpoolNotificationRecipientCollection = info.GetBoolean("_alwaysFetchSupportpoolNotificationRecipientCollection");
			_alreadyFetchedSupportpoolNotificationRecipientCollection = info.GetBoolean("_alreadyFetchedSupportpoolNotificationRecipientCollection");

			_supportpoolSupportagentCollection = (Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection)info.GetValue("_supportpoolSupportagentCollection", typeof(Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection));
			_alwaysFetchSupportpoolSupportagentCollection = info.GetBoolean("_alwaysFetchSupportpoolSupportagentCollection");
			_alreadyFetchedSupportpoolSupportagentCollection = info.GetBoolean("_alreadyFetchedSupportpoolSupportagentCollection");
			_companyOwnerCollectionViaCompany = (Obymobi.Data.CollectionClasses.CompanyOwnerCollection)info.GetValue("_companyOwnerCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CompanyOwnerCollection));
			_alwaysFetchCompanyOwnerCollectionViaCompany = info.GetBoolean("_alwaysFetchCompanyOwnerCollectionViaCompany");
			_alreadyFetchedCompanyOwnerCollectionViaCompany = info.GetBoolean("_alreadyFetchedCompanyOwnerCollectionViaCompany");

			_countryCollectionViaCompany = (Obymobi.Data.CollectionClasses.CountryCollection)info.GetValue("_countryCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CountryCollection));
			_alwaysFetchCountryCollectionViaCompany = info.GetBoolean("_alwaysFetchCountryCollectionViaCompany");
			_alreadyFetchedCountryCollectionViaCompany = info.GetBoolean("_alreadyFetchedCountryCollectionViaCompany");

			_currencyCollectionViaCompany = (Obymobi.Data.CollectionClasses.CurrencyCollection)info.GetValue("_currencyCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CurrencyCollection));
			_alwaysFetchCurrencyCollectionViaCompany = info.GetBoolean("_alwaysFetchCurrencyCollectionViaCompany");
			_alreadyFetchedCurrencyCollectionViaCompany = info.GetBoolean("_alreadyFetchedCurrencyCollectionViaCompany");

			_languageCollectionViaCompany = (Obymobi.Data.CollectionClasses.LanguageCollection)info.GetValue("_languageCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.LanguageCollection));
			_alwaysFetchLanguageCollectionViaCompany = info.GetBoolean("_alwaysFetchLanguageCollectionViaCompany");
			_alreadyFetchedLanguageCollectionViaCompany = info.GetBoolean("_alreadyFetchedLanguageCollectionViaCompany");

			_orderCollectionViaOrderRoutestephandler = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrderRoutestephandler", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrderRoutestephandler = info.GetBoolean("_alwaysFetchOrderCollectionViaOrderRoutestephandler");
			_alreadyFetchedOrderCollectionViaOrderRoutestephandler = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrderRoutestephandler");

			_orderCollectionViaOrderRoutestephandlerHistory = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrderRoutestephandlerHistory", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory");
			_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory");

			_routeCollectionViaCompany = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCompany = info.GetBoolean("_alwaysFetchRouteCollectionViaCompany");
			_alreadyFetchedRouteCollectionViaCompany = info.GetBoolean("_alreadyFetchedRouteCollectionViaCompany");

			_routestepCollectionViaRoutestephandler = (Obymobi.Data.CollectionClasses.RoutestepCollection)info.GetValue("_routestepCollectionViaRoutestephandler", typeof(Obymobi.Data.CollectionClasses.RoutestepCollection));
			_alwaysFetchRoutestepCollectionViaRoutestephandler = info.GetBoolean("_alwaysFetchRoutestepCollectionViaRoutestephandler");
			_alreadyFetchedRoutestepCollectionViaRoutestephandler = info.GetBoolean("_alreadyFetchedRoutestepCollectionViaRoutestephandler");

			_terminalCollectionViaOrderRoutestephandler = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandler", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandler = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandler");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler");

			_terminalCollectionViaOrderRoutestephandler_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandler_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandler_");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_");

			_terminalCollectionViaOrderRoutestephandlerHistory = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandlerHistory", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory");

			_terminalCollectionViaOrderRoutestephandlerHistory_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaOrderRoutestephandlerHistory_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_");
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_");

			_terminalCollectionViaRoutestephandler = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaRoutestephandler", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaRoutestephandler = info.GetBoolean("_alwaysFetchTerminalCollectionViaRoutestephandler");
			_alreadyFetchedTerminalCollectionViaRoutestephandler = info.GetBoolean("_alreadyFetchedTerminalCollectionViaRoutestephandler");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCompanyCollection = (_companyCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerCollection = (_orderRoutestephandlerCollection.Count > 0);
			_alreadyFetchedOrderRoutestephandlerHistoryCollection = (_orderRoutestephandlerHistoryCollection.Count > 0);
			_alreadyFetchedRoutestephandlerCollection = (_routestephandlerCollection.Count > 0);
			_alreadyFetchedSupportpoolNotificationRecipientCollection = (_supportpoolNotificationRecipientCollection.Count > 0);
			_alreadyFetchedSupportpoolSupportagentCollection = (_supportpoolSupportagentCollection.Count > 0);
			_alreadyFetchedCompanyOwnerCollectionViaCompany = (_companyOwnerCollectionViaCompany.Count > 0);
			_alreadyFetchedCountryCollectionViaCompany = (_countryCollectionViaCompany.Count > 0);
			_alreadyFetchedCurrencyCollectionViaCompany = (_currencyCollectionViaCompany.Count > 0);
			_alreadyFetchedLanguageCollectionViaCompany = (_languageCollectionViaCompany.Count > 0);
			_alreadyFetchedOrderCollectionViaOrderRoutestephandler = (_orderCollectionViaOrderRoutestephandler.Count > 0);
			_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = (_orderCollectionViaOrderRoutestephandlerHistory.Count > 0);
			_alreadyFetchedRouteCollectionViaCompany = (_routeCollectionViaCompany.Count > 0);
			_alreadyFetchedRoutestepCollectionViaRoutestephandler = (_routestepCollectionViaRoutestephandler.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = (_terminalCollectionViaOrderRoutestephandler.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = (_terminalCollectionViaOrderRoutestephandler_.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = (_terminalCollectionViaOrderRoutestephandlerHistory.Count > 0);
			_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = (_terminalCollectionViaOrderRoutestephandlerHistory_.Count > 0);
			_alreadyFetchedTerminalCollectionViaRoutestephandler = (_terminalCollectionViaRoutestephandler.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyCollection":
					toReturn.Add(Relations.CompanyEntityUsingSupportpoolId);
					break;
				case "OrderRoutestephandlerCollection":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingSupportpoolId);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingSupportpoolId);
					break;
				case "RoutestephandlerCollection":
					toReturn.Add(Relations.RoutestephandlerEntityUsingSupportpoolId);
					break;
				case "SupportpoolNotificationRecipientCollection":
					toReturn.Add(Relations.SupportpoolNotificationRecipientEntityUsingSupportpoolId);
					break;
				case "SupportpoolSupportagentCollection":
					toReturn.Add(Relations.SupportpoolSupportagentEntityUsingSupportpoolId);
					break;
				case "CompanyOwnerCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingSupportpoolId, "SupportpoolEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CompanyOwnerEntityUsingCompanyOwnerId, "Company_", string.Empty, JoinHint.None);
					break;
				case "CountryCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingSupportpoolId, "SupportpoolEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CountryEntityUsingCountryId, "Company_", string.Empty, JoinHint.None);
					break;
				case "CurrencyCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingSupportpoolId, "SupportpoolEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CurrencyEntityUsingCurrencyId, "Company_", string.Empty, JoinHint.None);
					break;
				case "LanguageCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingSupportpoolId, "SupportpoolEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.LanguageEntityUsingLanguageId, "Company_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrderRoutestephandler":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingSupportpoolId, "SupportpoolEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.OrderEntityUsingOrderId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrderRoutestephandlerHistory":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingSupportpoolId, "SupportpoolEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.OrderEntityUsingOrderId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingSupportpoolId, "SupportpoolEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.RouteEntityUsingRouteId, "Company_", string.Empty, JoinHint.None);
					break;
				case "RoutestepCollectionViaRoutestephandler":
					toReturn.Add(Relations.RoutestephandlerEntityUsingSupportpoolId, "SupportpoolEntity__", "Routestephandler_", JoinHint.None);
					toReturn.Add(RoutestephandlerEntity.Relations.RoutestepEntityUsingRoutestepId, "Routestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandler":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingSupportpoolId, "SupportpoolEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandler_":
					toReturn.Add(Relations.OrderRoutestephandlerEntityUsingSupportpoolId, "SupportpoolEntity__", "OrderRoutestephandler_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerEntity.Relations.TerminalEntityUsingTerminalId, "OrderRoutestephandler_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingSupportpoolId, "SupportpoolEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.TerminalEntityUsingForwardedFromTerminalId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory_":
					toReturn.Add(Relations.OrderRoutestephandlerHistoryEntityUsingSupportpoolId, "SupportpoolEntity__", "OrderRoutestephandlerHistory_", JoinHint.None);
					toReturn.Add(OrderRoutestephandlerHistoryEntity.Relations.TerminalEntityUsingTerminalId, "OrderRoutestephandlerHistory_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaRoutestephandler":
					toReturn.Add(Relations.RoutestephandlerEntityUsingSupportpoolId, "SupportpoolEntity__", "Routestephandler_", JoinHint.None);
					toReturn.Add(RoutestephandlerEntity.Relations.TerminalEntityUsingTerminalId, "Routestephandler_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_companyCollection", (!this.MarkedForDeletion?_companyCollection:null));
			info.AddValue("_alwaysFetchCompanyCollection", _alwaysFetchCompanyCollection);
			info.AddValue("_alreadyFetchedCompanyCollection", _alreadyFetchedCompanyCollection);
			info.AddValue("_orderRoutestephandlerCollection", (!this.MarkedForDeletion?_orderRoutestephandlerCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerCollection", _alwaysFetchOrderRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerCollection", _alreadyFetchedOrderRoutestephandlerCollection);
			info.AddValue("_orderRoutestephandlerHistoryCollection", (!this.MarkedForDeletion?_orderRoutestephandlerHistoryCollection:null));
			info.AddValue("_alwaysFetchOrderRoutestephandlerHistoryCollection", _alwaysFetchOrderRoutestephandlerHistoryCollection);
			info.AddValue("_alreadyFetchedOrderRoutestephandlerHistoryCollection", _alreadyFetchedOrderRoutestephandlerHistoryCollection);
			info.AddValue("_routestephandlerCollection", (!this.MarkedForDeletion?_routestephandlerCollection:null));
			info.AddValue("_alwaysFetchRoutestephandlerCollection", _alwaysFetchRoutestephandlerCollection);
			info.AddValue("_alreadyFetchedRoutestephandlerCollection", _alreadyFetchedRoutestephandlerCollection);
			info.AddValue("_supportpoolNotificationRecipientCollection", (!this.MarkedForDeletion?_supportpoolNotificationRecipientCollection:null));
			info.AddValue("_alwaysFetchSupportpoolNotificationRecipientCollection", _alwaysFetchSupportpoolNotificationRecipientCollection);
			info.AddValue("_alreadyFetchedSupportpoolNotificationRecipientCollection", _alreadyFetchedSupportpoolNotificationRecipientCollection);
			info.AddValue("_supportpoolSupportagentCollection", (!this.MarkedForDeletion?_supportpoolSupportagentCollection:null));
			info.AddValue("_alwaysFetchSupportpoolSupportagentCollection", _alwaysFetchSupportpoolSupportagentCollection);
			info.AddValue("_alreadyFetchedSupportpoolSupportagentCollection", _alreadyFetchedSupportpoolSupportagentCollection);
			info.AddValue("_companyOwnerCollectionViaCompany", (!this.MarkedForDeletion?_companyOwnerCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCompanyOwnerCollectionViaCompany", _alwaysFetchCompanyOwnerCollectionViaCompany);
			info.AddValue("_alreadyFetchedCompanyOwnerCollectionViaCompany", _alreadyFetchedCompanyOwnerCollectionViaCompany);
			info.AddValue("_countryCollectionViaCompany", (!this.MarkedForDeletion?_countryCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCountryCollectionViaCompany", _alwaysFetchCountryCollectionViaCompany);
			info.AddValue("_alreadyFetchedCountryCollectionViaCompany", _alreadyFetchedCountryCollectionViaCompany);
			info.AddValue("_currencyCollectionViaCompany", (!this.MarkedForDeletion?_currencyCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCurrencyCollectionViaCompany", _alwaysFetchCurrencyCollectionViaCompany);
			info.AddValue("_alreadyFetchedCurrencyCollectionViaCompany", _alreadyFetchedCurrencyCollectionViaCompany);
			info.AddValue("_languageCollectionViaCompany", (!this.MarkedForDeletion?_languageCollectionViaCompany:null));
			info.AddValue("_alwaysFetchLanguageCollectionViaCompany", _alwaysFetchLanguageCollectionViaCompany);
			info.AddValue("_alreadyFetchedLanguageCollectionViaCompany", _alreadyFetchedLanguageCollectionViaCompany);
			info.AddValue("_orderCollectionViaOrderRoutestephandler", (!this.MarkedForDeletion?_orderCollectionViaOrderRoutestephandler:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrderRoutestephandler", _alwaysFetchOrderCollectionViaOrderRoutestephandler);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrderRoutestephandler", _alreadyFetchedOrderCollectionViaOrderRoutestephandler);
			info.AddValue("_orderCollectionViaOrderRoutestephandlerHistory", (!this.MarkedForDeletion?_orderCollectionViaOrderRoutestephandlerHistory:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory", _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory", _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_routeCollectionViaCompany", (!this.MarkedForDeletion?_routeCollectionViaCompany:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCompany", _alwaysFetchRouteCollectionViaCompany);
			info.AddValue("_alreadyFetchedRouteCollectionViaCompany", _alreadyFetchedRouteCollectionViaCompany);
			info.AddValue("_routestepCollectionViaRoutestephandler", (!this.MarkedForDeletion?_routestepCollectionViaRoutestephandler:null));
			info.AddValue("_alwaysFetchRoutestepCollectionViaRoutestephandler", _alwaysFetchRoutestepCollectionViaRoutestephandler);
			info.AddValue("_alreadyFetchedRoutestepCollectionViaRoutestephandler", _alreadyFetchedRoutestepCollectionViaRoutestephandler);
			info.AddValue("_terminalCollectionViaOrderRoutestephandler", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandler:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandler", _alwaysFetchTerminalCollectionViaOrderRoutestephandler);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler", _alreadyFetchedTerminalCollectionViaOrderRoutestephandler);
			info.AddValue("_terminalCollectionViaOrderRoutestephandler_", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandler_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandler_", _alwaysFetchTerminalCollectionViaOrderRoutestephandler_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_", _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_);
			info.AddValue("_terminalCollectionViaOrderRoutestephandlerHistory", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandlerHistory:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory", _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory", _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory);
			info.AddValue("_terminalCollectionViaOrderRoutestephandlerHistory_", (!this.MarkedForDeletion?_terminalCollectionViaOrderRoutestephandlerHistory_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_", _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_", _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_);
			info.AddValue("_terminalCollectionViaRoutestephandler", (!this.MarkedForDeletion?_terminalCollectionViaRoutestephandler:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaRoutestephandler", _alwaysFetchTerminalCollectionViaRoutestephandler);
			info.AddValue("_alreadyFetchedTerminalCollectionViaRoutestephandler", _alreadyFetchedTerminalCollectionViaRoutestephandler);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyCollection":
					_alreadyFetchedCompanyCollection = true;
					if(entity!=null)
					{
						this.CompanyCollection.Add((CompanyEntity)entity);
					}
					break;
				case "OrderRoutestephandlerCollection":
					_alreadyFetchedOrderRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)entity);
					}
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
					if(entity!=null)
					{
						this.OrderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)entity);
					}
					break;
				case "RoutestephandlerCollection":
					_alreadyFetchedRoutestephandlerCollection = true;
					if(entity!=null)
					{
						this.RoutestephandlerCollection.Add((RoutestephandlerEntity)entity);
					}
					break;
				case "SupportpoolNotificationRecipientCollection":
					_alreadyFetchedSupportpoolNotificationRecipientCollection = true;
					if(entity!=null)
					{
						this.SupportpoolNotificationRecipientCollection.Add((SupportpoolNotificationRecipientEntity)entity);
					}
					break;
				case "SupportpoolSupportagentCollection":
					_alreadyFetchedSupportpoolSupportagentCollection = true;
					if(entity!=null)
					{
						this.SupportpoolSupportagentCollection.Add((SupportpoolSupportagentEntity)entity);
					}
					break;
				case "CompanyOwnerCollectionViaCompany":
					_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CompanyOwnerCollectionViaCompany.Add((CompanyOwnerEntity)entity);
					}
					break;
				case "CountryCollectionViaCompany":
					_alreadyFetchedCountryCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CountryCollectionViaCompany.Add((CountryEntity)entity);
					}
					break;
				case "CurrencyCollectionViaCompany":
					_alreadyFetchedCurrencyCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CurrencyCollectionViaCompany.Add((CurrencyEntity)entity);
					}
					break;
				case "LanguageCollectionViaCompany":
					_alreadyFetchedLanguageCollectionViaCompany = true;
					if(entity!=null)
					{
						this.LanguageCollectionViaCompany.Add((LanguageEntity)entity);
					}
					break;
				case "OrderCollectionViaOrderRoutestephandler":
					_alreadyFetchedOrderCollectionViaOrderRoutestephandler = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrderRoutestephandler.Add((OrderEntity)entity);
					}
					break;
				case "OrderCollectionViaOrderRoutestephandlerHistory":
					_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrderRoutestephandlerHistory.Add((OrderEntity)entity);
					}
					break;
				case "RouteCollectionViaCompany":
					_alreadyFetchedRouteCollectionViaCompany = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCompany.Add((RouteEntity)entity);
					}
					break;
				case "RoutestepCollectionViaRoutestephandler":
					_alreadyFetchedRoutestepCollectionViaRoutestephandler = true;
					if(entity!=null)
					{
						this.RoutestepCollectionViaRoutestephandler.Add((RoutestepEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandler":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandler.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandler_":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandler_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandlerHistory.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaOrderRoutestephandlerHistory_":
					_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaOrderRoutestephandlerHistory_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaRoutestephandler":
					_alreadyFetchedTerminalCollectionViaRoutestephandler = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaRoutestephandler.Add((TerminalEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyCollection":
					_companyCollection.Add((CompanyEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerCollection":
					_orderRoutestephandlerCollection.Add((OrderRoutestephandlerEntity)relatedEntity);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					_orderRoutestephandlerHistoryCollection.Add((OrderRoutestephandlerHistoryEntity)relatedEntity);
					break;
				case "RoutestephandlerCollection":
					_routestephandlerCollection.Add((RoutestephandlerEntity)relatedEntity);
					break;
				case "SupportpoolNotificationRecipientCollection":
					_supportpoolNotificationRecipientCollection.Add((SupportpoolNotificationRecipientEntity)relatedEntity);
					break;
				case "SupportpoolSupportagentCollection":
					_supportpoolSupportagentCollection.Add((SupportpoolSupportagentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyCollection":
					this.PerformRelatedEntityRemoval(_companyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderRoutestephandlerHistoryCollection":
					this.PerformRelatedEntityRemoval(_orderRoutestephandlerHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoutestephandlerCollection":
					this.PerformRelatedEntityRemoval(_routestephandlerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SupportpoolNotificationRecipientCollection":
					this.PerformRelatedEntityRemoval(_supportpoolNotificationRecipientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SupportpoolSupportagentCollection":
					this.PerformRelatedEntityRemoval(_supportpoolSupportagentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_companyCollection);
			toReturn.Add(_orderRoutestephandlerCollection);
			toReturn.Add(_orderRoutestephandlerHistoryCollection);
			toReturn.Add(_routestephandlerCollection);
			toReturn.Add(_supportpoolNotificationRecipientCollection);
			toReturn.Add(_supportpoolSupportagentCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolId)
		{
			return FetchUsingPK(supportpoolId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(supportpoolId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(supportpoolId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(supportpoolId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SupportpoolId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SupportpoolRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyCollection || forceFetch || _alwaysFetchCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollection);
				_companyCollection.SuppressClearInGetMulti=!forceFetch;
				_companyCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyCollection.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, filter);
				_companyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollection = true;
			}
			return _companyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollection'. These settings will be taken into account
		/// when the property CompanyCollection is requested or GetMultiCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollection.SortClauses=sortClauses;
			_companyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, _orderRoutestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection GetMultiOrderRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerCollection || forceFetch || _alwaysFetchOrderRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerCollection);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerCollection.GetMultiManyToOne(null, null, this, null, null, filter);
				_orderRoutestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerCollection = true;
			}
			return _orderRoutestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerCollection is requested or GetMultiOrderRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerCollection.SortClauses=sortClauses;
			_orderRoutestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderRoutestephandlerHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, _orderRoutestephandlerHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderRoutestephandlerHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection GetMultiOrderRoutestephandlerHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderRoutestephandlerHistoryCollection || forceFetch || _alwaysFetchOrderRoutestephandlerHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderRoutestephandlerHistoryCollection);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_orderRoutestephandlerHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderRoutestephandlerHistoryCollection.GetMultiManyToOne(null, null, this, null, null, filter);
				_orderRoutestephandlerHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = true;
			}
			return _orderRoutestephandlerHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderRoutestephandlerHistoryCollection'. These settings will be taken into account
		/// when the property OrderRoutestephandlerHistoryCollection is requested or GetMultiOrderRoutestephandlerHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderRoutestephandlerHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderRoutestephandlerHistoryCollection.SortClauses=sortClauses;
			_orderRoutestephandlerHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoutestephandlerEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, _routestephandlerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutestephandlerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection GetMultiRoutestephandlerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutestephandlerCollection || forceFetch || _alwaysFetchRoutestephandlerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routestephandlerCollection);
				_routestephandlerCollection.SuppressClearInGetMulti=!forceFetch;
				_routestephandlerCollection.EntityFactoryToUse = entityFactoryToUse;
				_routestephandlerCollection.GetMultiManyToOne(null, null, this, null, filter);
				_routestephandlerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutestephandlerCollection = true;
			}
			return _routestephandlerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutestephandlerCollection'. These settings will be taken into account
		/// when the property RoutestephandlerCollection is requested or GetMultiRoutestephandlerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutestephandlerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routestephandlerCollection.SortClauses=sortClauses;
			_routestephandlerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolNotificationRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolNotificationRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection GetMultiSupportpoolNotificationRecipientCollection(bool forceFetch)
		{
			return GetMultiSupportpoolNotificationRecipientCollection(forceFetch, _supportpoolNotificationRecipientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolNotificationRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolNotificationRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection GetMultiSupportpoolNotificationRecipientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSupportpoolNotificationRecipientCollection(forceFetch, _supportpoolNotificationRecipientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolNotificationRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection GetMultiSupportpoolNotificationRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSupportpoolNotificationRecipientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolNotificationRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection GetMultiSupportpoolNotificationRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSupportpoolNotificationRecipientCollection || forceFetch || _alwaysFetchSupportpoolNotificationRecipientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolNotificationRecipientCollection);
				_supportpoolNotificationRecipientCollection.SuppressClearInGetMulti=!forceFetch;
				_supportpoolNotificationRecipientCollection.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolNotificationRecipientCollection.GetMultiManyToOne(this, filter);
				_supportpoolNotificationRecipientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolNotificationRecipientCollection = true;
			}
			return _supportpoolNotificationRecipientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolNotificationRecipientCollection'. These settings will be taken into account
		/// when the property SupportpoolNotificationRecipientCollection is requested or GetMultiSupportpoolNotificationRecipientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolNotificationRecipientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolNotificationRecipientCollection.SortClauses=sortClauses;
			_supportpoolNotificationRecipientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolSupportagentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolSupportagentEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection GetMultiSupportpoolSupportagentCollection(bool forceFetch)
		{
			return GetMultiSupportpoolSupportagentCollection(forceFetch, _supportpoolSupportagentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolSupportagentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolSupportagentEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection GetMultiSupportpoolSupportagentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSupportpoolSupportagentCollection(forceFetch, _supportpoolSupportagentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolSupportagentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection GetMultiSupportpoolSupportagentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSupportpoolSupportagentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolSupportagentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection GetMultiSupportpoolSupportagentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSupportpoolSupportagentCollection || forceFetch || _alwaysFetchSupportpoolSupportagentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolSupportagentCollection);
				_supportpoolSupportagentCollection.SuppressClearInGetMulti=!forceFetch;
				_supportpoolSupportagentCollection.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolSupportagentCollection.GetMultiManyToOne(null, this, filter);
				_supportpoolSupportagentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolSupportagentCollection = true;
			}
			return _supportpoolSupportagentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolSupportagentCollection'. These settings will be taken into account
		/// when the property SupportpoolSupportagentCollection is requested or GetMultiSupportpoolSupportagentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolSupportagentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolSupportagentCollection.SortClauses=sortClauses;
			_supportpoolSupportagentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyOwnerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCompanyOwnerCollectionViaCompany(forceFetch, _companyOwnerCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyOwnerCollectionViaCompany || forceFetch || _alwaysFetchCompanyOwnerCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyOwnerCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_companyOwnerCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_companyOwnerCollectionViaCompany.GetMulti(filter, GetRelationsForField("CompanyOwnerCollectionViaCompany"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
			}
			return _companyOwnerCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyOwnerCollectionViaCompany'. These settings will be taken into account
		/// when the property CompanyOwnerCollectionViaCompany is requested or GetMultiCompanyOwnerCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyOwnerCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyOwnerCollectionViaCompany.SortClauses=sortClauses;
			_companyOwnerCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CountryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCountryCollectionViaCompany(forceFetch, _countryCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCountryCollectionViaCompany || forceFetch || _alwaysFetchCountryCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_countryCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_countryCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_countryCollectionViaCompany.GetMulti(filter, GetRelationsForField("CountryCollectionViaCompany"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCountryCollectionViaCompany = true;
			}
			return _countryCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CountryCollectionViaCompany'. These settings will be taken into account
		/// when the property CountryCollectionViaCompany is requested or GetMultiCountryCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCountryCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_countryCollectionViaCompany.SortClauses=sortClauses;
			_countryCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CurrencyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCurrencyCollectionViaCompany(forceFetch, _currencyCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCurrencyCollectionViaCompany || forceFetch || _alwaysFetchCurrencyCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_currencyCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_currencyCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_currencyCollectionViaCompany.GetMulti(filter, GetRelationsForField("CurrencyCollectionViaCompany"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCurrencyCollectionViaCompany = true;
			}
			return _currencyCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CurrencyCollectionViaCompany'. These settings will be taken into account
		/// when the property CurrencyCollectionViaCompany is requested or GetMultiCurrencyCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCurrencyCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_currencyCollectionViaCompany.SortClauses=sortClauses;
			_currencyCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaCompany(bool forceFetch)
		{
			return GetMultiLanguageCollectionViaCompany(forceFetch, _languageCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedLanguageCollectionViaCompany || forceFetch || _alwaysFetchLanguageCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_languageCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_languageCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_languageCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_languageCollectionViaCompany.GetMulti(filter, GetRelationsForField("LanguageCollectionViaCompany"));
				_languageCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedLanguageCollectionViaCompany = true;
			}
			return _languageCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'LanguageCollectionViaCompany'. These settings will be taken into account
		/// when the property LanguageCollectionViaCompany is requested or GetMultiLanguageCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLanguageCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_languageCollectionViaCompany.SortClauses=sortClauses;
			_languageCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandler(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrderRoutestephandler(forceFetch, _orderCollectionViaOrderRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrderRoutestephandler || forceFetch || _alwaysFetchOrderCollectionViaOrderRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrderRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_orderCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrderRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrderRoutestephandler.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrderRoutestephandler"));
				_orderCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrderRoutestephandler = true;
			}
			return _orderCollectionViaOrderRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrderRoutestephandler'. These settings will be taken into account
		/// when the property OrderCollectionViaOrderRoutestephandler is requested or GetMultiOrderCollectionViaOrderRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrderRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrderRoutestephandler.SortClauses=sortClauses;
			_orderCollectionViaOrderRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandlerHistory(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrderRoutestephandlerHistory(forceFetch, _orderCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrderRoutestephandlerHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory || forceFetch || _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrderRoutestephandlerHistory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_orderCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrderRoutestephandlerHistory.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrderRoutestephandlerHistory"));
				_orderCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = true;
			}
			return _orderCollectionViaOrderRoutestephandlerHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrderRoutestephandlerHistory'. These settings will be taken into account
		/// when the property OrderCollectionViaOrderRoutestephandlerHistory is requested or GetMultiOrderCollectionViaOrderRoutestephandlerHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrderRoutestephandlerHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrderRoutestephandlerHistory.SortClauses=sortClauses;
			_orderCollectionViaOrderRoutestephandlerHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCompany(forceFetch, _routeCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCompany || forceFetch || _alwaysFetchRouteCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCompany.GetMulti(filter, GetRelationsForField("RouteCollectionViaCompany"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCompany = true;
			}
			return _routeCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCompany'. These settings will be taken into account
		/// when the property RouteCollectionViaCompany is requested or GetMultiRouteCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCompany.SortClauses=sortClauses;
			_routeCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoutestepEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollectionViaRoutestephandler(bool forceFetch)
		{
			return GetMultiRoutestepCollectionViaRoutestephandler(forceFetch, _routestepCollectionViaRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollectionViaRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRoutestepCollectionViaRoutestephandler || forceFetch || _alwaysFetchRoutestepCollectionViaRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routestepCollectionViaRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_routestepCollectionViaRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_routestepCollectionViaRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_routestepCollectionViaRoutestephandler.GetMulti(filter, GetRelationsForField("RoutestepCollectionViaRoutestephandler"));
				_routestepCollectionViaRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutestepCollectionViaRoutestephandler = true;
			}
			return _routestepCollectionViaRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutestepCollectionViaRoutestephandler'. These settings will be taken into account
		/// when the property RoutestepCollectionViaRoutestephandler is requested or GetMultiRoutestepCollectionViaRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutestepCollectionViaRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routestepCollectionViaRoutestephandler.SortClauses=sortClauses;
			_routestepCollectionViaRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandler(forceFetch, _terminalCollectionViaOrderRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandler || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_terminalCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandler.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler"));
				_terminalCollectionViaOrderRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = true;
			}
			return _terminalCollectionViaOrderRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandler'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandler is requested or GetMultiTerminalCollectionViaOrderRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandler.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandler_(forceFetch, _terminalCollectionViaOrderRoutestephandler_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandler_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandler_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandler_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_terminalCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandler_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandler_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler_"));
				_terminalCollectionViaOrderRoutestephandler_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = true;
			}
			return _terminalCollectionViaOrderRoutestephandler_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandler_'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandler_ is requested or GetMultiTerminalCollectionViaOrderRoutestephandler_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandler_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandler_.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandler_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(forceFetch, _terminalCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandlerHistory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_terminalCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandlerHistory.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandlerHistory.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory"));
				_terminalCollectionViaOrderRoutestephandlerHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = true;
			}
			return _terminalCollectionViaOrderRoutestephandlerHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandlerHistory'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandlerHistory is requested or GetMultiTerminalCollectionViaOrderRoutestephandlerHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandlerHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandlerHistory.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandlerHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(forceFetch, _terminalCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ || forceFetch || _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaOrderRoutestephandlerHistory_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_terminalCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaOrderRoutestephandlerHistory_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaOrderRoutestephandlerHistory_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory_"));
				_terminalCollectionViaOrderRoutestephandlerHistory_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = true;
			}
			return _terminalCollectionViaOrderRoutestephandlerHistory_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaOrderRoutestephandlerHistory_'. These settings will be taken into account
		/// when the property TerminalCollectionViaOrderRoutestephandlerHistory_ is requested or GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaOrderRoutestephandlerHistory_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaOrderRoutestephandlerHistory_.SortClauses=sortClauses;
			_terminalCollectionViaOrderRoutestephandlerHistory_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaRoutestephandler(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaRoutestephandler(forceFetch, _terminalCollectionViaRoutestephandler.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaRoutestephandler(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaRoutestephandler || forceFetch || _alwaysFetchTerminalCollectionViaRoutestephandler) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaRoutestephandler);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupportpoolFields.SupportpoolId, ComparisonOperator.Equal, this.SupportpoolId, "SupportpoolEntity__"));
				_terminalCollectionViaRoutestephandler.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaRoutestephandler.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaRoutestephandler.GetMulti(filter, GetRelationsForField("TerminalCollectionViaRoutestephandler"));
				_terminalCollectionViaRoutestephandler.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaRoutestephandler = true;
			}
			return _terminalCollectionViaRoutestephandler;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaRoutestephandler'. These settings will be taken into account
		/// when the property TerminalCollectionViaRoutestephandler is requested or GetMultiTerminalCollectionViaRoutestephandler is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaRoutestephandler(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaRoutestephandler.SortClauses=sortClauses;
			_terminalCollectionViaRoutestephandler.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyCollection", _companyCollection);
			toReturn.Add("OrderRoutestephandlerCollection", _orderRoutestephandlerCollection);
			toReturn.Add("OrderRoutestephandlerHistoryCollection", _orderRoutestephandlerHistoryCollection);
			toReturn.Add("RoutestephandlerCollection", _routestephandlerCollection);
			toReturn.Add("SupportpoolNotificationRecipientCollection", _supportpoolNotificationRecipientCollection);
			toReturn.Add("SupportpoolSupportagentCollection", _supportpoolSupportagentCollection);
			toReturn.Add("CompanyOwnerCollectionViaCompany", _companyOwnerCollectionViaCompany);
			toReturn.Add("CountryCollectionViaCompany", _countryCollectionViaCompany);
			toReturn.Add("CurrencyCollectionViaCompany", _currencyCollectionViaCompany);
			toReturn.Add("LanguageCollectionViaCompany", _languageCollectionViaCompany);
			toReturn.Add("OrderCollectionViaOrderRoutestephandler", _orderCollectionViaOrderRoutestephandler);
			toReturn.Add("OrderCollectionViaOrderRoutestephandlerHistory", _orderCollectionViaOrderRoutestephandlerHistory);
			toReturn.Add("RouteCollectionViaCompany", _routeCollectionViaCompany);
			toReturn.Add("RoutestepCollectionViaRoutestephandler", _routestepCollectionViaRoutestephandler);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandler", _terminalCollectionViaOrderRoutestephandler);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandler_", _terminalCollectionViaOrderRoutestephandler_);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandlerHistory", _terminalCollectionViaOrderRoutestephandlerHistory);
			toReturn.Add("TerminalCollectionViaOrderRoutestephandlerHistory_", _terminalCollectionViaOrderRoutestephandlerHistory_);
			toReturn.Add("TerminalCollectionViaRoutestephandler", _terminalCollectionViaRoutestephandler);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="validator">The validator object for this SupportpoolEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 supportpoolId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(supportpoolId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_companyCollection = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollection.SetContainingEntityInfo(this, "SupportpoolEntity");

			_orderRoutestephandlerCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection();
			_orderRoutestephandlerCollection.SetContainingEntityInfo(this, "SupportpoolEntity");

			_orderRoutestephandlerHistoryCollection = new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection();
			_orderRoutestephandlerHistoryCollection.SetContainingEntityInfo(this, "SupportpoolEntity");

			_routestephandlerCollection = new Obymobi.Data.CollectionClasses.RoutestephandlerCollection();
			_routestephandlerCollection.SetContainingEntityInfo(this, "SupportpoolEntity");

			_supportpoolNotificationRecipientCollection = new Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection();
			_supportpoolNotificationRecipientCollection.SetContainingEntityInfo(this, "SupportpoolEntity");

			_supportpoolSupportagentCollection = new Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection();
			_supportpoolSupportagentCollection.SetContainingEntityInfo(this, "SupportpoolEntity");
			_companyOwnerCollectionViaCompany = new Obymobi.Data.CollectionClasses.CompanyOwnerCollection();
			_countryCollectionViaCompany = new Obymobi.Data.CollectionClasses.CountryCollection();
			_currencyCollectionViaCompany = new Obymobi.Data.CollectionClasses.CurrencyCollection();
			_languageCollectionViaCompany = new Obymobi.Data.CollectionClasses.LanguageCollection();
			_orderCollectionViaOrderRoutestephandler = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollectionViaOrderRoutestephandlerHistory = new Obymobi.Data.CollectionClasses.OrderCollection();
			_routeCollectionViaCompany = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routestepCollectionViaRoutestephandler = new Obymobi.Data.CollectionClasses.RoutestepCollection();
			_terminalCollectionViaOrderRoutestephandler = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandler_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandlerHistory = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaOrderRoutestephandlerHistory_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaRoutestephandler = new Obymobi.Data.CollectionClasses.TerminalCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupportpoolId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemSupportPool", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContentSupportPool", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="supportpoolId">PK value for Supportpool which data should be fetched into this Supportpool object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 supportpoolId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SupportpoolFieldIndex.SupportpoolId].ForcedCurrentValueWrite(supportpoolId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSupportpoolDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SupportpoolEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SupportpoolRelations Relations
		{
			get	{ return new SupportpoolRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyCollection")[0], (int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, 0, null, null, null, "OrderRoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderRoutestephandlerHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderRoutestephandlerHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection(), (IEntityRelation)GetRelationsForField("OrderRoutestephandlerHistoryCollection")[0], (int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity, 0, null, null, null, "OrderRoutestephandlerHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestephandler' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestephandlerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestephandlerCollection(), (IEntityRelation)GetRelationsForField("RoutestephandlerCollection")[0], (int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.RoutestephandlerEntity, 0, null, null, null, "RoutestephandlerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SupportpoolNotificationRecipient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolNotificationRecipientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection(), (IEntityRelation)GetRelationsForField("SupportpoolNotificationRecipientCollection")[0], (int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.SupportpoolNotificationRecipientEntity, 0, null, null, null, "SupportpoolNotificationRecipientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SupportpoolSupportagent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolSupportagentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection(), (IEntityRelation)GetRelationsForField("SupportpoolSupportagentCollection")[0], (int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.SupportpoolSupportagentEntity, 0, null, null, null, "SupportpoolSupportagentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyOwner'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyOwnerCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyOwnerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.CompanyOwnerEntity, 0, null, null, GetRelationsForField("CompanyOwnerCollectionViaCompany"), "CompanyOwnerCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Country'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCountryCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CountryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.CountryEntity, 0, null, null, GetRelationsForField("CountryCollectionViaCompany"), "CountryCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, GetRelationsForField("CurrencyCollectionViaCompany"), "CurrencyCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, GetRelationsForField("LanguageCollectionViaCompany"), "LanguageCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrderRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrderRoutestephandler"), "OrderCollectionViaOrderRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrderRoutestephandlerHistory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrderRoutestephandlerHistory"), "OrderCollectionViaOrderRoutestephandlerHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCompany"), "RouteCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestep'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestepCollectionViaRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RoutestephandlerEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "Routestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestepCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.RoutestepEntity, 0, null, null, GetRelationsForField("RoutestepCollectionViaRoutestephandler"), "RoutestepCollectionViaRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler"), "TerminalCollectionViaOrderRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandler_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandler_"), "TerminalCollectionViaOrderRoutestephandler_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory"), "TerminalCollectionViaOrderRoutestephandlerHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderRoutestephandlerHistoryEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "OrderRoutestephandlerHistory_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaOrderRoutestephandlerHistory_"), "TerminalCollectionViaOrderRoutestephandlerHistory_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaRoutestephandler
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RoutestephandlerEntityUsingSupportpoolId;
				intermediateRelation.SetAliases(string.Empty, "Routestephandler_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupportpoolEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaRoutestephandler"), "TerminalCollectionViaRoutestephandler", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SupportpoolId property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."SupportpoolId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SupportpoolId
		{
			get { return (System.Int32)GetValue((int)SupportpoolFieldIndex.SupportpoolId, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.SupportpoolId, value, true); }
		}

		/// <summary> The Name property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SupportpoolFieldIndex.Name, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.Name, value, true); }
		}

		/// <summary> The Phonenumber property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Phonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)SupportpoolFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.Phonenumber, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SupportpoolFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SupportpoolFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Email property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)SupportpoolFieldIndex.Email, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.Email, value, true); }
		}

		/// <summary> The SystemSupportPool property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."SystemSupportPool"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SystemSupportPool
		{
			get { return (System.Boolean)GetValue((int)SupportpoolFieldIndex.SystemSupportPool, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.SystemSupportPool, value, true); }
		}

		/// <summary> The ContentSupportPool property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."ContentSupportPool"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ContentSupportPool
		{
			get { return (System.Boolean)GetValue((int)SupportpoolFieldIndex.ContentSupportPool, true); }
			set	{ SetValue((int)SupportpoolFieldIndex.ContentSupportPool, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SupportpoolFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Supportpool<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supportpool"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SupportpoolFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollection
		{
			get	{ return GetMultiCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollection. When set to true, CompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollection
		{
			get	{ return _alwaysFetchCompanyCollection; }
			set	{ _alwaysFetchCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollection already has been fetched. Setting this property to false when CompanyCollection has been fetched
		/// will clear the CompanyCollection collection well. Setting this property to true while CompanyCollection hasn't been fetched disables lazy loading for CompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollection
		{
			get { return _alreadyFetchedCompanyCollection;}
			set 
			{
				if(_alreadyFetchedCompanyCollection && !value && (_companyCollection != null))
				{
					_companyCollection.Clear();
				}
				_alreadyFetchedCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerCollection OrderRoutestephandlerCollection
		{
			get	{ return GetMultiOrderRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerCollection. When set to true, OrderRoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerCollection already has been fetched. Setting this property to false when OrderRoutestephandlerCollection has been fetched
		/// will clear the OrderRoutestephandlerCollection collection well. Setting this property to true while OrderRoutestephandlerCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerCollection && !value && (_orderRoutestephandlerCollection != null))
				{
					_orderRoutestephandlerCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderRoutestephandlerHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderRoutestephandlerHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderRoutestephandlerHistoryCollection OrderRoutestephandlerHistoryCollection
		{
			get	{ return GetMultiOrderRoutestephandlerHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderRoutestephandlerHistoryCollection. When set to true, OrderRoutestephandlerHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderRoutestephandlerHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderRoutestephandlerHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderRoutestephandlerHistoryCollection
		{
			get	{ return _alwaysFetchOrderRoutestephandlerHistoryCollection; }
			set	{ _alwaysFetchOrderRoutestephandlerHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderRoutestephandlerHistoryCollection already has been fetched. Setting this property to false when OrderRoutestephandlerHistoryCollection has been fetched
		/// will clear the OrderRoutestephandlerHistoryCollection collection well. Setting this property to true while OrderRoutestephandlerHistoryCollection hasn't been fetched disables lazy loading for OrderRoutestephandlerHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderRoutestephandlerHistoryCollection
		{
			get { return _alreadyFetchedOrderRoutestephandlerHistoryCollection;}
			set 
			{
				if(_alreadyFetchedOrderRoutestephandlerHistoryCollection && !value && (_orderRoutestephandlerHistoryCollection != null))
				{
					_orderRoutestephandlerHistoryCollection.Clear();
				}
				_alreadyFetchedOrderRoutestephandlerHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoutestephandlerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutestephandlerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoutestephandlerCollection RoutestephandlerCollection
		{
			get	{ return GetMultiRoutestephandlerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestephandlerCollection. When set to true, RoutestephandlerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestephandlerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoutestephandlerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestephandlerCollection
		{
			get	{ return _alwaysFetchRoutestephandlerCollection; }
			set	{ _alwaysFetchRoutestephandlerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestephandlerCollection already has been fetched. Setting this property to false when RoutestephandlerCollection has been fetched
		/// will clear the RoutestephandlerCollection collection well. Setting this property to true while RoutestephandlerCollection hasn't been fetched disables lazy loading for RoutestephandlerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestephandlerCollection
		{
			get { return _alreadyFetchedRoutestephandlerCollection;}
			set 
			{
				if(_alreadyFetchedRoutestephandlerCollection && !value && (_routestephandlerCollection != null))
				{
					_routestephandlerCollection.Clear();
				}
				_alreadyFetchedRoutestephandlerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SupportpoolNotificationRecipientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolNotificationRecipientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolNotificationRecipientCollection SupportpoolNotificationRecipientCollection
		{
			get	{ return GetMultiSupportpoolNotificationRecipientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolNotificationRecipientCollection. When set to true, SupportpoolNotificationRecipientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolNotificationRecipientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSupportpoolNotificationRecipientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolNotificationRecipientCollection
		{
			get	{ return _alwaysFetchSupportpoolNotificationRecipientCollection; }
			set	{ _alwaysFetchSupportpoolNotificationRecipientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolNotificationRecipientCollection already has been fetched. Setting this property to false when SupportpoolNotificationRecipientCollection has been fetched
		/// will clear the SupportpoolNotificationRecipientCollection collection well. Setting this property to true while SupportpoolNotificationRecipientCollection hasn't been fetched disables lazy loading for SupportpoolNotificationRecipientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolNotificationRecipientCollection
		{
			get { return _alreadyFetchedSupportpoolNotificationRecipientCollection;}
			set 
			{
				if(_alreadyFetchedSupportpoolNotificationRecipientCollection && !value && (_supportpoolNotificationRecipientCollection != null))
				{
					_supportpoolNotificationRecipientCollection.Clear();
				}
				_alreadyFetchedSupportpoolNotificationRecipientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SupportpoolSupportagentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolSupportagentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolSupportagentCollection SupportpoolSupportagentCollection
		{
			get	{ return GetMultiSupportpoolSupportagentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolSupportagentCollection. When set to true, SupportpoolSupportagentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolSupportagentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSupportpoolSupportagentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolSupportagentCollection
		{
			get	{ return _alwaysFetchSupportpoolSupportagentCollection; }
			set	{ _alwaysFetchSupportpoolSupportagentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolSupportagentCollection already has been fetched. Setting this property to false when SupportpoolSupportagentCollection has been fetched
		/// will clear the SupportpoolSupportagentCollection collection well. Setting this property to true while SupportpoolSupportagentCollection hasn't been fetched disables lazy loading for SupportpoolSupportagentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolSupportagentCollection
		{
			get { return _alreadyFetchedSupportpoolSupportagentCollection;}
			set 
			{
				if(_alreadyFetchedSupportpoolSupportagentCollection && !value && (_supportpoolSupportagentCollection != null))
				{
					_supportpoolSupportagentCollection.Clear();
				}
				_alreadyFetchedSupportpoolSupportagentCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyOwnerCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyOwnerCollection CompanyOwnerCollectionViaCompany
		{
			get { return GetMultiCompanyOwnerCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyOwnerCollectionViaCompany. When set to true, CompanyOwnerCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyOwnerCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCompanyOwnerCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyOwnerCollectionViaCompany
		{
			get	{ return _alwaysFetchCompanyOwnerCollectionViaCompany; }
			set	{ _alwaysFetchCompanyOwnerCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyOwnerCollectionViaCompany already has been fetched. Setting this property to false when CompanyOwnerCollectionViaCompany has been fetched
		/// will clear the CompanyOwnerCollectionViaCompany collection well. Setting this property to true while CompanyOwnerCollectionViaCompany hasn't been fetched disables lazy loading for CompanyOwnerCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyOwnerCollectionViaCompany
		{
			get { return _alreadyFetchedCompanyOwnerCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCompanyOwnerCollectionViaCompany && !value && (_companyOwnerCollectionViaCompany != null))
				{
					_companyOwnerCollectionViaCompany.Clear();
				}
				_alreadyFetchedCompanyOwnerCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCountryCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CountryCollection CountryCollectionViaCompany
		{
			get { return GetMultiCountryCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CountryCollectionViaCompany. When set to true, CountryCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CountryCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCountryCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCountryCollectionViaCompany
		{
			get	{ return _alwaysFetchCountryCollectionViaCompany; }
			set	{ _alwaysFetchCountryCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CountryCollectionViaCompany already has been fetched. Setting this property to false when CountryCollectionViaCompany has been fetched
		/// will clear the CountryCollectionViaCompany collection well. Setting this property to true while CountryCollectionViaCompany hasn't been fetched disables lazy loading for CountryCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCountryCollectionViaCompany
		{
			get { return _alreadyFetchedCountryCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCountryCollectionViaCompany && !value && (_countryCollectionViaCompany != null))
				{
					_countryCollectionViaCompany.Clear();
				}
				_alreadyFetchedCountryCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCurrencyCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CurrencyCollection CurrencyCollectionViaCompany
		{
			get { return GetMultiCurrencyCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyCollectionViaCompany. When set to true, CurrencyCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCurrencyCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyCollectionViaCompany
		{
			get	{ return _alwaysFetchCurrencyCollectionViaCompany; }
			set	{ _alwaysFetchCurrencyCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyCollectionViaCompany already has been fetched. Setting this property to false when CurrencyCollectionViaCompany has been fetched
		/// will clear the CurrencyCollectionViaCompany collection well. Setting this property to true while CurrencyCollectionViaCompany hasn't been fetched disables lazy loading for CurrencyCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyCollectionViaCompany
		{
			get { return _alreadyFetchedCurrencyCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCurrencyCollectionViaCompany && !value && (_currencyCollectionViaCompany != null))
				{
					_currencyCollectionViaCompany.Clear();
				}
				_alreadyFetchedCurrencyCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLanguageCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LanguageCollection LanguageCollectionViaCompany
		{
			get { return GetMultiLanguageCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageCollectionViaCompany. When set to true, LanguageCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiLanguageCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageCollectionViaCompany
		{
			get	{ return _alwaysFetchLanguageCollectionViaCompany; }
			set	{ _alwaysFetchLanguageCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageCollectionViaCompany already has been fetched. Setting this property to false when LanguageCollectionViaCompany has been fetched
		/// will clear the LanguageCollectionViaCompany collection well. Setting this property to true while LanguageCollectionViaCompany hasn't been fetched disables lazy loading for LanguageCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageCollectionViaCompany
		{
			get { return _alreadyFetchedLanguageCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedLanguageCollectionViaCompany && !value && (_languageCollectionViaCompany != null))
				{
					_languageCollectionViaCompany.Clear();
				}
				_alreadyFetchedLanguageCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrderRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrderRoutestephandler
		{
			get { return GetMultiOrderCollectionViaOrderRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrderRoutestephandler. When set to true, OrderCollectionViaOrderRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrderRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrderRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrderRoutestephandler
		{
			get	{ return _alwaysFetchOrderCollectionViaOrderRoutestephandler; }
			set	{ _alwaysFetchOrderCollectionViaOrderRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrderRoutestephandler already has been fetched. Setting this property to false when OrderCollectionViaOrderRoutestephandler has been fetched
		/// will clear the OrderCollectionViaOrderRoutestephandler collection well. Setting this property to true while OrderCollectionViaOrderRoutestephandler hasn't been fetched disables lazy loading for OrderCollectionViaOrderRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrderRoutestephandler
		{
			get { return _alreadyFetchedOrderCollectionViaOrderRoutestephandler;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrderRoutestephandler && !value && (_orderCollectionViaOrderRoutestephandler != null))
				{
					_orderCollectionViaOrderRoutestephandler.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrderRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrderRoutestephandlerHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrderRoutestephandlerHistory
		{
			get { return GetMultiOrderCollectionViaOrderRoutestephandlerHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrderRoutestephandlerHistory. When set to true, OrderCollectionViaOrderRoutestephandlerHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrderRoutestephandlerHistory is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrderRoutestephandlerHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrderRoutestephandlerHistory
		{
			get	{ return _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory; }
			set	{ _alwaysFetchOrderCollectionViaOrderRoutestephandlerHistory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrderRoutestephandlerHistory already has been fetched. Setting this property to false when OrderCollectionViaOrderRoutestephandlerHistory has been fetched
		/// will clear the OrderCollectionViaOrderRoutestephandlerHistory collection well. Setting this property to true while OrderCollectionViaOrderRoutestephandlerHistory hasn't been fetched disables lazy loading for OrderCollectionViaOrderRoutestephandlerHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory
		{
			get { return _alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory && !value && (_orderCollectionViaOrderRoutestephandlerHistory != null))
				{
					_orderCollectionViaOrderRoutestephandlerHistory.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrderRoutestephandlerHistory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCompany
		{
			get { return GetMultiRouteCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCompany. When set to true, RouteCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCompany
		{
			get	{ return _alwaysFetchRouteCollectionViaCompany; }
			set	{ _alwaysFetchRouteCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCompany already has been fetched. Setting this property to false when RouteCollectionViaCompany has been fetched
		/// will clear the RouteCollectionViaCompany collection well. Setting this property to true while RouteCollectionViaCompany hasn't been fetched disables lazy loading for RouteCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCompany
		{
			get { return _alreadyFetchedRouteCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCompany && !value && (_routeCollectionViaCompany != null))
				{
					_routeCollectionViaCompany.Clear();
				}
				_alreadyFetchedRouteCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutestepCollectionViaRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoutestepCollection RoutestepCollectionViaRoutestephandler
		{
			get { return GetMultiRoutestepCollectionViaRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestepCollectionViaRoutestephandler. When set to true, RoutestepCollectionViaRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestepCollectionViaRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiRoutestepCollectionViaRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestepCollectionViaRoutestephandler
		{
			get	{ return _alwaysFetchRoutestepCollectionViaRoutestephandler; }
			set	{ _alwaysFetchRoutestepCollectionViaRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestepCollectionViaRoutestephandler already has been fetched. Setting this property to false when RoutestepCollectionViaRoutestephandler has been fetched
		/// will clear the RoutestepCollectionViaRoutestephandler collection well. Setting this property to true while RoutestepCollectionViaRoutestephandler hasn't been fetched disables lazy loading for RoutestepCollectionViaRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestepCollectionViaRoutestephandler
		{
			get { return _alreadyFetchedRoutestepCollectionViaRoutestephandler;}
			set 
			{
				if(_alreadyFetchedRoutestepCollectionViaRoutestephandler && !value && (_routestepCollectionViaRoutestephandler != null))
				{
					_routestepCollectionViaRoutestephandler.Clear();
				}
				_alreadyFetchedRoutestepCollectionViaRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandler
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandler. When set to true, TerminalCollectionViaOrderRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandler
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandler; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandler already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandler has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandler collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandler hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandler
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandler;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandler && !value && (_terminalCollectionViaOrderRoutestephandler != null))
				{
					_terminalCollectionViaOrderRoutestephandler.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandler_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandler_
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandler_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandler_. When set to true, TerminalCollectionViaOrderRoutestephandler_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandler_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandler_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandler_
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandler_; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandler_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandler_ already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandler_ has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandler_ collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandler_ hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandler_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandler_
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandler_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ && !value && (_terminalCollectionViaOrderRoutestephandler_ != null))
				{
					_terminalCollectionViaOrderRoutestephandler_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandler_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandlerHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandlerHistory
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandlerHistory. When set to true, TerminalCollectionViaOrderRoutestephandlerHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandlerHistory is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandlerHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandlerHistory already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandlerHistory has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandlerHistory collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandlerHistory hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandlerHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory && !value && (_terminalCollectionViaOrderRoutestephandlerHistory != null))
				{
					_terminalCollectionViaOrderRoutestephandlerHistory.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get { return GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaOrderRoutestephandlerHistory_. When set to true, TerminalCollectionViaOrderRoutestephandlerHistory_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaOrderRoutestephandlerHistory_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaOrderRoutestephandlerHistory_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get	{ return _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_; }
			set	{ _alwaysFetchTerminalCollectionViaOrderRoutestephandlerHistory_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaOrderRoutestephandlerHistory_ already has been fetched. Setting this property to false when TerminalCollectionViaOrderRoutestephandlerHistory_ has been fetched
		/// will clear the TerminalCollectionViaOrderRoutestephandlerHistory_ collection well. Setting this property to true while TerminalCollectionViaOrderRoutestephandlerHistory_ hasn't been fetched disables lazy loading for TerminalCollectionViaOrderRoutestephandlerHistory_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_
		{
			get { return _alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ && !value && (_terminalCollectionViaOrderRoutestephandlerHistory_ != null))
				{
					_terminalCollectionViaOrderRoutestephandlerHistory_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaOrderRoutestephandlerHistory_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaRoutestephandler()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaRoutestephandler
		{
			get { return GetMultiTerminalCollectionViaRoutestephandler(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaRoutestephandler. When set to true, TerminalCollectionViaRoutestephandler is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaRoutestephandler is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaRoutestephandler(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaRoutestephandler
		{
			get	{ return _alwaysFetchTerminalCollectionViaRoutestephandler; }
			set	{ _alwaysFetchTerminalCollectionViaRoutestephandler = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaRoutestephandler already has been fetched. Setting this property to false when TerminalCollectionViaRoutestephandler has been fetched
		/// will clear the TerminalCollectionViaRoutestephandler collection well. Setting this property to true while TerminalCollectionViaRoutestephandler hasn't been fetched disables lazy loading for TerminalCollectionViaRoutestephandler</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaRoutestephandler
		{
			get { return _alreadyFetchedTerminalCollectionViaRoutestephandler;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaRoutestephandler && !value && (_terminalCollectionViaRoutestephandler != null))
				{
					_terminalCollectionViaRoutestephandler.Clear();
				}
				_alreadyFetchedTerminalCollectionViaRoutestephandler = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SupportpoolEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
