﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ProductAlteration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ProductAlterationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ProductAlterationEntity"; }
		}
	
		#region Class Member Declarations
		private AlterationEntity _alterationEntity;
		private bool	_alwaysFetchAlterationEntity, _alreadyFetchedAlterationEntity, _alterationEntityReturnsNewIfNotFound;
		private PosproductPosalterationEntity _posproductPosalterationEntity;
		private bool	_alwaysFetchPosproductPosalterationEntity, _alreadyFetchedPosproductPosalterationEntity, _posproductPosalterationEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationEntity</summary>
			public static readonly string AlterationEntity = "AlterationEntity";
			/// <summary>Member name PosproductPosalterationEntity</summary>
			public static readonly string PosproductPosalterationEntity = "PosproductPosalterationEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProductAlterationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ProductAlterationEntityBase() :base("ProductAlterationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		protected ProductAlterationEntityBase(System.Int32 productAlterationId):base("ProductAlterationEntity")
		{
			InitClassFetch(productAlterationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ProductAlterationEntityBase(System.Int32 productAlterationId, IPrefetchPath prefetchPathToUse): base("ProductAlterationEntity")
		{
			InitClassFetch(productAlterationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="validator">The custom validator object for this ProductAlterationEntity</param>
		protected ProductAlterationEntityBase(System.Int32 productAlterationId, IValidator validator):base("ProductAlterationEntity")
		{
			InitClassFetch(productAlterationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductAlterationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationEntity = (AlterationEntity)info.GetValue("_alterationEntity", typeof(AlterationEntity));
			if(_alterationEntity!=null)
			{
				_alterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationEntityReturnsNewIfNotFound = info.GetBoolean("_alterationEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationEntity = info.GetBoolean("_alwaysFetchAlterationEntity");
			_alreadyFetchedAlterationEntity = info.GetBoolean("_alreadyFetchedAlterationEntity");

			_posproductPosalterationEntity = (PosproductPosalterationEntity)info.GetValue("_posproductPosalterationEntity", typeof(PosproductPosalterationEntity));
			if(_posproductPosalterationEntity!=null)
			{
				_posproductPosalterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posproductPosalterationEntityReturnsNewIfNotFound = info.GetBoolean("_posproductPosalterationEntityReturnsNewIfNotFound");
			_alwaysFetchPosproductPosalterationEntity = info.GetBoolean("_alwaysFetchPosproductPosalterationEntity");
			_alreadyFetchedPosproductPosalterationEntity = info.GetBoolean("_alreadyFetchedPosproductPosalterationEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProductAlterationFieldIndex)fieldIndex)
			{
				case ProductAlterationFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case ProductAlterationFieldIndex.AlterationId:
					DesetupSyncAlterationEntity(true, false);
					_alreadyFetchedAlterationEntity = false;
					break;
				case ProductAlterationFieldIndex.PosproductPosalterationId:
					DesetupSyncPosproductPosalterationEntity(true, false);
					_alreadyFetchedPosproductPosalterationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationEntity = (_alterationEntity != null);
			_alreadyFetchedPosproductPosalterationEntity = (_posproductPosalterationEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationId);
					break;
				case "PosproductPosalterationEntity":
					toReturn.Add(Relations.PosproductPosalterationEntityUsingPosproductPosalterationId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationEntity", (!this.MarkedForDeletion?_alterationEntity:null));
			info.AddValue("_alterationEntityReturnsNewIfNotFound", _alterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationEntity", _alwaysFetchAlterationEntity);
			info.AddValue("_alreadyFetchedAlterationEntity", _alreadyFetchedAlterationEntity);
			info.AddValue("_posproductPosalterationEntity", (!this.MarkedForDeletion?_posproductPosalterationEntity:null));
			info.AddValue("_posproductPosalterationEntityReturnsNewIfNotFound", _posproductPosalterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosproductPosalterationEntity", _alwaysFetchPosproductPosalterationEntity);
			info.AddValue("_alreadyFetchedPosproductPosalterationEntity", _alreadyFetchedPosproductPosalterationEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationEntity":
					_alreadyFetchedAlterationEntity = true;
					this.AlterationEntity = (AlterationEntity)entity;
					break;
				case "PosproductPosalterationEntity":
					_alreadyFetchedPosproductPosalterationEntity = true;
					this.PosproductPosalterationEntity = (PosproductPosalterationEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					SetupSyncAlterationEntity(relatedEntity);
					break;
				case "PosproductPosalterationEntity":
					SetupSyncPosproductPosalterationEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					DesetupSyncAlterationEntity(false, true);
					break;
				case "PosproductPosalterationEntity":
					DesetupSyncPosproductPosalterationEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_alterationEntity!=null)
			{
				toReturn.Add(_alterationEntity);
			}
			if(_posproductPosalterationEntity!=null)
			{
				toReturn.Add(_posproductPosalterationEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productAlterationId)
		{
			return FetchUsingPK(productAlterationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productAlterationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(productAlterationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productAlterationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(productAlterationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productAlterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(productAlterationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProductAlterationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProductAlterationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleAlterationEntity()
		{
			return GetSingleAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationEntity || forceFetch || _alwaysFetchAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationId);
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationEntity = newEntity;
				_alreadyFetchedAlterationEntity = fetchResult;
			}
			return _alterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'PosproductPosalterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosproductPosalterationEntity' which is related to this entity.</returns>
		public PosproductPosalterationEntity GetSinglePosproductPosalterationEntity()
		{
			return GetSinglePosproductPosalterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosproductPosalterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosproductPosalterationEntity' which is related to this entity.</returns>
		public virtual PosproductPosalterationEntity GetSinglePosproductPosalterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosproductPosalterationEntity || forceFetch || _alwaysFetchPosproductPosalterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PosproductPosalterationEntityUsingPosproductPosalterationId);
				PosproductPosalterationEntity newEntity = new PosproductPosalterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PosproductPosalterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PosproductPosalterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_posproductPosalterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PosproductPosalterationEntity = newEntity;
				_alreadyFetchedPosproductPosalterationEntity = fetchResult;
			}
			return _posproductPosalterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId);
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationEntity", _alterationEntity);
			toReturn.Add("PosproductPosalterationEntity", _posproductPosalterationEntity);
			toReturn.Add("ProductEntity", _productEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="validator">The validator object for this ProductAlterationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 productAlterationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(productAlterationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_alterationEntityReturnsNewIfNotFound = true;
			_posproductPosalterationEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductAlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosproductPosalterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericproductGenericalterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _alterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticProductAlterationRelations.AlterationEntityUsingAlterationIdStatic, true, signalRelatedEntity, "ProductAlterationCollection", resetFKFields, new int[] { (int)ProductAlterationFieldIndex.AlterationId } );		
			_alterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationEntity(IEntityCore relatedEntity)
		{
			if(_alterationEntity!=relatedEntity)
			{		
				DesetupSyncAlterationEntity(true, true);
				_alterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticProductAlterationRelations.AlterationEntityUsingAlterationIdStatic, true, ref _alreadyFetchedAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posproductPosalterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosproductPosalterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _posproductPosalterationEntity, new PropertyChangedEventHandler( OnPosproductPosalterationEntityPropertyChanged ), "PosproductPosalterationEntity", Obymobi.Data.RelationClasses.StaticProductAlterationRelations.PosproductPosalterationEntityUsingPosproductPosalterationIdStatic, true, signalRelatedEntity, "ProductAlterationCollection", resetFKFields, new int[] { (int)ProductAlterationFieldIndex.PosproductPosalterationId } );		
			_posproductPosalterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posproductPosalterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosproductPosalterationEntity(IEntityCore relatedEntity)
		{
			if(_posproductPosalterationEntity!=relatedEntity)
			{		
				DesetupSyncPosproductPosalterationEntity(true, true);
				_posproductPosalterationEntity = (PosproductPosalterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _posproductPosalterationEntity, new PropertyChangedEventHandler( OnPosproductPosalterationEntityPropertyChanged ), "PosproductPosalterationEntity", Obymobi.Data.RelationClasses.StaticProductAlterationRelations.PosproductPosalterationEntityUsingPosproductPosalterationIdStatic, true, ref _alreadyFetchedPosproductPosalterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosproductPosalterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticProductAlterationRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "ProductAlterationCollection", resetFKFields, new int[] { (int)ProductAlterationFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticProductAlterationRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 productAlterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProductAlterationFieldIndex.ProductAlterationId].ForcedCurrentValueWrite(productAlterationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProductAlterationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProductAlterationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProductAlterationRelations Relations
		{
			get	{ return new ProductAlterationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationEntity")[0], (int)Obymobi.Data.EntityType.ProductAlterationEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PosproductPosalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosproductPosalterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosproductPosalterationCollection(), (IEntityRelation)GetRelationsForField("PosproductPosalterationEntity")[0], (int)Obymobi.Data.EntityType.ProductAlterationEntity, (int)Obymobi.Data.EntityType.PosproductPosalterationEntity, 0, null, null, null, "PosproductPosalterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.ProductAlterationEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ProductAlterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."ProductAlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductAlterationId
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.ProductAlterationId, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.ProductAlterationId, value, true); }
		}

		/// <summary> The ProductId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductId
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.ProductId, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.ProductId, value, true); }
		}

		/// <summary> The AlterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.AlterationId, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The PosproductPosalterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."PosproductPosalterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosproductPosalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductAlterationFieldIndex.PosproductPosalterationId, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.PosproductPosalterationId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The GenericproductGenericalterationId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."GenericproductGenericalterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericproductGenericalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductAlterationFieldIndex.GenericproductGenericalterationId, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.GenericproductGenericalterationId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductAlterationFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductAlterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductAlterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductAlterationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The Version property of the Entity ProductAlteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductAlteration"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)ProductAlterationFieldIndex.Version, true); }
			set	{ SetValue((int)ProductAlterationFieldIndex.Version, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity AlterationEntity
		{
			get	{ return GetSingleAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductAlterationCollection", "AlterationEntity", _alterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationEntity. When set to true, AlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationEntity
		{
			get	{ return _alwaysFetchAlterationEntity; }
			set	{ _alwaysFetchAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationEntity already has been fetched. Setting this property to false when AlterationEntity has been fetched
		/// will set AlterationEntity to null as well. Setting this property to true while AlterationEntity hasn't been fetched disables lazy loading for AlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationEntity
		{
			get { return _alreadyFetchedAlterationEntity;}
			set 
			{
				if(_alreadyFetchedAlterationEntity && !value)
				{
					this.AlterationEntity = null;
				}
				_alreadyFetchedAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationEntity is not found
		/// in the database. When set to true, AlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationEntityReturnsNewIfNotFound
		{
			get	{ return _alterationEntityReturnsNewIfNotFound; }
			set { _alterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PosproductPosalterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosproductPosalterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PosproductPosalterationEntity PosproductPosalterationEntity
		{
			get	{ return GetSinglePosproductPosalterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPosproductPosalterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductAlterationCollection", "PosproductPosalterationEntity", _posproductPosalterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosproductPosalterationEntity. When set to true, PosproductPosalterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosproductPosalterationEntity is accessed. You can always execute a forced fetch by calling GetSinglePosproductPosalterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosproductPosalterationEntity
		{
			get	{ return _alwaysFetchPosproductPosalterationEntity; }
			set	{ _alwaysFetchPosproductPosalterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosproductPosalterationEntity already has been fetched. Setting this property to false when PosproductPosalterationEntity has been fetched
		/// will set PosproductPosalterationEntity to null as well. Setting this property to true while PosproductPosalterationEntity hasn't been fetched disables lazy loading for PosproductPosalterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosproductPosalterationEntity
		{
			get { return _alreadyFetchedPosproductPosalterationEntity;}
			set 
			{
				if(_alreadyFetchedPosproductPosalterationEntity && !value)
				{
					this.PosproductPosalterationEntity = null;
				}
				_alreadyFetchedPosproductPosalterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosproductPosalterationEntity is not found
		/// in the database. When set to true, PosproductPosalterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosproductPosalterationEntityReturnsNewIfNotFound
		{
			get	{ return _posproductPosalterationEntityReturnsNewIfNotFound; }
			set { _posproductPosalterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductAlterationCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ProductAlterationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
