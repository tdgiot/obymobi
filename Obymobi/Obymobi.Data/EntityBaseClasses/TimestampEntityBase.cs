﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Timestamp'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class TimestampEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "TimestampEntity"; }
		}
	
		#region Class Member Declarations
		private TerminalEntity _terminalEntity;
		private bool	_alwaysFetchTerminalEntity, _alreadyFetchedTerminalEntity, _terminalEntityReturnsNewIfNotFound;
		private UIModeEntity _uIModeEntity;
		private bool	_alwaysFetchUIModeEntity, _alreadyFetchedUIModeEntity, _uIModeEntityReturnsNewIfNotFound;
		private UIScheduleEntity _uIScheduleEntity;
		private bool	_alwaysFetchUIScheduleEntity, _alreadyFetchedUIScheduleEntity, _uIScheduleEntityReturnsNewIfNotFound;
		private UIThemeEntity _uIThemeEntity;
		private bool	_alwaysFetchUIThemeEntity, _alreadyFetchedUIThemeEntity, _uIThemeEntityReturnsNewIfNotFound;
		private AdvertisementConfigurationEntity _advertisementConfigurationEntity;
		private bool	_alwaysFetchAdvertisementConfigurationEntity, _alreadyFetchedAdvertisementConfigurationEntity, _advertisementConfigurationEntityReturnsNewIfNotFound;
		private ClientEntity _clientEntity;
		private bool	_alwaysFetchClientEntity, _alreadyFetchedClientEntity, _clientEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private DeliverypointEntity _deliverypointEntity;
		private bool	_alwaysFetchDeliverypointEntity, _alreadyFetchedDeliverypointEntity, _deliverypointEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private EntertainmentConfigurationEntity _entertainmentConfigurationEntity;
		private bool	_alwaysFetchEntertainmentConfigurationEntity, _alreadyFetchedEntertainmentConfigurationEntity, _entertainmentConfigurationEntityReturnsNewIfNotFound;
		private InfraredConfigurationEntity _infraredConfigurationEntity;
		private bool	_alwaysFetchInfraredConfigurationEntity, _alreadyFetchedInfraredConfigurationEntity, _infraredConfigurationEntityReturnsNewIfNotFound;
		private MapEntity _mapEntity;
		private bool	_alwaysFetchMapEntity, _alreadyFetchedMapEntity, _mapEntityReturnsNewIfNotFound;
		private MenuEntity _menuEntity;
		private bool	_alwaysFetchMenuEntity, _alreadyFetchedMenuEntity, _menuEntityReturnsNewIfNotFound;
		private PointOfInterestEntity _pointOfInterestEntity;
		private bool	_alwaysFetchPointOfInterestEntity, _alreadyFetchedPointOfInterestEntity, _pointOfInterestEntityReturnsNewIfNotFound;
		private PriceScheduleEntity _priceScheduleEntity;
		private bool	_alwaysFetchPriceScheduleEntity, _alreadyFetchedPriceScheduleEntity, _priceScheduleEntityReturnsNewIfNotFound;
		private RoomControlConfigurationEntity _roomControlConfigurationEntity;
		private bool	_alwaysFetchRoomControlConfigurationEntity, _alreadyFetchedRoomControlConfigurationEntity, _roomControlConfigurationEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;
		private TerminalConfigurationEntity _terminalConfigurationEntity;
		private bool	_alwaysFetchTerminalConfigurationEntity, _alreadyFetchedTerminalConfigurationEntity, _terminalConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TerminalEntity</summary>
			public static readonly string TerminalEntity = "TerminalEntity";
			/// <summary>Member name UIModeEntity</summary>
			public static readonly string UIModeEntity = "UIModeEntity";
			/// <summary>Member name UIScheduleEntity</summary>
			public static readonly string UIScheduleEntity = "UIScheduleEntity";
			/// <summary>Member name UIThemeEntity</summary>
			public static readonly string UIThemeEntity = "UIThemeEntity";
			/// <summary>Member name AdvertisementConfigurationEntity</summary>
			public static readonly string AdvertisementConfigurationEntity = "AdvertisementConfigurationEntity";
			/// <summary>Member name ClientEntity</summary>
			public static readonly string ClientEntity = "ClientEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name DeliverypointEntity</summary>
			public static readonly string DeliverypointEntity = "DeliverypointEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name EntertainmentConfigurationEntity</summary>
			public static readonly string EntertainmentConfigurationEntity = "EntertainmentConfigurationEntity";
			/// <summary>Member name InfraredConfigurationEntity</summary>
			public static readonly string InfraredConfigurationEntity = "InfraredConfigurationEntity";
			/// <summary>Member name MapEntity</summary>
			public static readonly string MapEntity = "MapEntity";
			/// <summary>Member name MenuEntity</summary>
			public static readonly string MenuEntity = "MenuEntity";
			/// <summary>Member name PointOfInterestEntity</summary>
			public static readonly string PointOfInterestEntity = "PointOfInterestEntity";
			/// <summary>Member name PriceScheduleEntity</summary>
			public static readonly string PriceScheduleEntity = "PriceScheduleEntity";
			/// <summary>Member name RoomControlConfigurationEntity</summary>
			public static readonly string RoomControlConfigurationEntity = "RoomControlConfigurationEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name TerminalConfigurationEntity</summary>
			public static readonly string TerminalConfigurationEntity = "TerminalConfigurationEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TimestampEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected TimestampEntityBase() :base("TimestampEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		protected TimestampEntityBase(System.Int32 timestampId):base("TimestampEntity")
		{
			InitClassFetch(timestampId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected TimestampEntityBase(System.Int32 timestampId, IPrefetchPath prefetchPathToUse): base("TimestampEntity")
		{
			InitClassFetch(timestampId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <param name="validator">The custom validator object for this TimestampEntity</param>
		protected TimestampEntityBase(System.Int32 timestampId, IValidator validator):base("TimestampEntity")
		{
			InitClassFetch(timestampId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TimestampEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_terminalEntity = (TerminalEntity)info.GetValue("_terminalEntity", typeof(TerminalEntity));
			if(_terminalEntity!=null)
			{
				_terminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalEntityReturnsNewIfNotFound = info.GetBoolean("_terminalEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalEntity = info.GetBoolean("_alwaysFetchTerminalEntity");
			_alreadyFetchedTerminalEntity = info.GetBoolean("_alreadyFetchedTerminalEntity");

			_uIModeEntity = (UIModeEntity)info.GetValue("_uIModeEntity", typeof(UIModeEntity));
			if(_uIModeEntity!=null)
			{
				_uIModeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIModeEntityReturnsNewIfNotFound = info.GetBoolean("_uIModeEntityReturnsNewIfNotFound");
			_alwaysFetchUIModeEntity = info.GetBoolean("_alwaysFetchUIModeEntity");
			_alreadyFetchedUIModeEntity = info.GetBoolean("_alreadyFetchedUIModeEntity");

			_uIScheduleEntity = (UIScheduleEntity)info.GetValue("_uIScheduleEntity", typeof(UIScheduleEntity));
			if(_uIScheduleEntity!=null)
			{
				_uIScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_uIScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchUIScheduleEntity = info.GetBoolean("_alwaysFetchUIScheduleEntity");
			_alreadyFetchedUIScheduleEntity = info.GetBoolean("_alreadyFetchedUIScheduleEntity");

			_uIThemeEntity = (UIThemeEntity)info.GetValue("_uIThemeEntity", typeof(UIThemeEntity));
			if(_uIThemeEntity!=null)
			{
				_uIThemeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIThemeEntityReturnsNewIfNotFound = info.GetBoolean("_uIThemeEntityReturnsNewIfNotFound");
			_alwaysFetchUIThemeEntity = info.GetBoolean("_alwaysFetchUIThemeEntity");
			_alreadyFetchedUIThemeEntity = info.GetBoolean("_alreadyFetchedUIThemeEntity");
			_advertisementConfigurationEntity = (AdvertisementConfigurationEntity)info.GetValue("_advertisementConfigurationEntity", typeof(AdvertisementConfigurationEntity));
			if(_advertisementConfigurationEntity!=null)
			{
				_advertisementConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementConfigurationEntity = info.GetBoolean("_alwaysFetchAdvertisementConfigurationEntity");
			_alreadyFetchedAdvertisementConfigurationEntity = info.GetBoolean("_alreadyFetchedAdvertisementConfigurationEntity");

			_clientEntity = (ClientEntity)info.GetValue("_clientEntity", typeof(ClientEntity));
			if(_clientEntity!=null)
			{
				_clientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientEntityReturnsNewIfNotFound = info.GetBoolean("_clientEntityReturnsNewIfNotFound");
			_alwaysFetchClientEntity = info.GetBoolean("_alwaysFetchClientEntity");
			_alreadyFetchedClientEntity = info.GetBoolean("_alreadyFetchedClientEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_deliverypointEntity = (DeliverypointEntity)info.GetValue("_deliverypointEntity", typeof(DeliverypointEntity));
			if(_deliverypointEntity!=null)
			{
				_deliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointEntity = info.GetBoolean("_alwaysFetchDeliverypointEntity");
			_alreadyFetchedDeliverypointEntity = info.GetBoolean("_alreadyFetchedDeliverypointEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_entertainmentConfigurationEntity = (EntertainmentConfigurationEntity)info.GetValue("_entertainmentConfigurationEntity", typeof(EntertainmentConfigurationEntity));
			if(_entertainmentConfigurationEntity!=null)
			{
				_entertainmentConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentConfigurationEntity = info.GetBoolean("_alwaysFetchEntertainmentConfigurationEntity");
			_alreadyFetchedEntertainmentConfigurationEntity = info.GetBoolean("_alreadyFetchedEntertainmentConfigurationEntity");

			_infraredConfigurationEntity = (InfraredConfigurationEntity)info.GetValue("_infraredConfigurationEntity", typeof(InfraredConfigurationEntity));
			if(_infraredConfigurationEntity!=null)
			{
				_infraredConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_infraredConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_infraredConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchInfraredConfigurationEntity = info.GetBoolean("_alwaysFetchInfraredConfigurationEntity");
			_alreadyFetchedInfraredConfigurationEntity = info.GetBoolean("_alreadyFetchedInfraredConfigurationEntity");

			_mapEntity = (MapEntity)info.GetValue("_mapEntity", typeof(MapEntity));
			if(_mapEntity!=null)
			{
				_mapEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mapEntityReturnsNewIfNotFound = info.GetBoolean("_mapEntityReturnsNewIfNotFound");
			_alwaysFetchMapEntity = info.GetBoolean("_alwaysFetchMapEntity");
			_alreadyFetchedMapEntity = info.GetBoolean("_alreadyFetchedMapEntity");

			_menuEntity = (MenuEntity)info.GetValue("_menuEntity", typeof(MenuEntity));
			if(_menuEntity!=null)
			{
				_menuEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_menuEntityReturnsNewIfNotFound = info.GetBoolean("_menuEntityReturnsNewIfNotFound");
			_alwaysFetchMenuEntity = info.GetBoolean("_alwaysFetchMenuEntity");
			_alreadyFetchedMenuEntity = info.GetBoolean("_alreadyFetchedMenuEntity");

			_pointOfInterestEntity = (PointOfInterestEntity)info.GetValue("_pointOfInterestEntity", typeof(PointOfInterestEntity));
			if(_pointOfInterestEntity!=null)
			{
				_pointOfInterestEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pointOfInterestEntityReturnsNewIfNotFound = info.GetBoolean("_pointOfInterestEntityReturnsNewIfNotFound");
			_alwaysFetchPointOfInterestEntity = info.GetBoolean("_alwaysFetchPointOfInterestEntity");
			_alreadyFetchedPointOfInterestEntity = info.GetBoolean("_alreadyFetchedPointOfInterestEntity");

			_priceScheduleEntity = (PriceScheduleEntity)info.GetValue("_priceScheduleEntity", typeof(PriceScheduleEntity));
			if(_priceScheduleEntity!=null)
			{
				_priceScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_priceScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchPriceScheduleEntity = info.GetBoolean("_alwaysFetchPriceScheduleEntity");
			_alreadyFetchedPriceScheduleEntity = info.GetBoolean("_alreadyFetchedPriceScheduleEntity");

			_roomControlConfigurationEntity = (RoomControlConfigurationEntity)info.GetValue("_roomControlConfigurationEntity", typeof(RoomControlConfigurationEntity));
			if(_roomControlConfigurationEntity!=null)
			{
				_roomControlConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlConfigurationEntity = info.GetBoolean("_alwaysFetchRoomControlConfigurationEntity");
			_alreadyFetchedRoomControlConfigurationEntity = info.GetBoolean("_alreadyFetchedRoomControlConfigurationEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");

			_terminalConfigurationEntity = (TerminalConfigurationEntity)info.GetValue("_terminalConfigurationEntity", typeof(TerminalConfigurationEntity));
			if(_terminalConfigurationEntity!=null)
			{
				_terminalConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_terminalConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalConfigurationEntity = info.GetBoolean("_alwaysFetchTerminalConfigurationEntity");
			_alreadyFetchedTerminalConfigurationEntity = info.GetBoolean("_alreadyFetchedTerminalConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TimestampFieldIndex)fieldIndex)
			{
				case TimestampFieldIndex.AdvertisementConfigurationId:
					DesetupSyncAdvertisementConfigurationEntity(true, false);
					_alreadyFetchedAdvertisementConfigurationEntity = false;
					break;
				case TimestampFieldIndex.ClientId:
					DesetupSyncClientEntity(true, false);
					_alreadyFetchedClientEntity = false;
					break;
				case TimestampFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case TimestampFieldIndex.DeliverypointId:
					DesetupSyncDeliverypointEntity(true, false);
					_alreadyFetchedDeliverypointEntity = false;
					break;
				case TimestampFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case TimestampFieldIndex.EntertainmentConfigurationId:
					DesetupSyncEntertainmentConfigurationEntity(true, false);
					_alreadyFetchedEntertainmentConfigurationEntity = false;
					break;
				case TimestampFieldIndex.InfraredConfigurationId:
					DesetupSyncInfraredConfigurationEntity(true, false);
					_alreadyFetchedInfraredConfigurationEntity = false;
					break;
				case TimestampFieldIndex.MapId:
					DesetupSyncMapEntity(true, false);
					_alreadyFetchedMapEntity = false;
					break;
				case TimestampFieldIndex.MenuId:
					DesetupSyncMenuEntity(true, false);
					_alreadyFetchedMenuEntity = false;
					break;
				case TimestampFieldIndex.PointOfInterestId:
					DesetupSyncPointOfInterestEntity(true, false);
					_alreadyFetchedPointOfInterestEntity = false;
					break;
				case TimestampFieldIndex.PriceScheduleId:
					DesetupSyncPriceScheduleEntity(true, false);
					_alreadyFetchedPriceScheduleEntity = false;
					break;
				case TimestampFieldIndex.RoomControlConfigurationId:
					DesetupSyncRoomControlConfigurationEntity(true, false);
					_alreadyFetchedRoomControlConfigurationEntity = false;
					break;
				case TimestampFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case TimestampFieldIndex.TerminalId:
					DesetupSyncTerminalEntity(true, false);
					_alreadyFetchedTerminalEntity = false;
					break;
				case TimestampFieldIndex.TerminalConfigurationId:
					DesetupSyncTerminalConfigurationEntity(true, false);
					_alreadyFetchedTerminalConfigurationEntity = false;
					break;
				case TimestampFieldIndex.UIModeId:
					DesetupSyncUIModeEntity(true, false);
					_alreadyFetchedUIModeEntity = false;
					break;
				case TimestampFieldIndex.UIScheduleId:
					DesetupSyncUIScheduleEntity(true, false);
					_alreadyFetchedUIScheduleEntity = false;
					break;
				case TimestampFieldIndex.UIThemeId:
					DesetupSyncUIThemeEntity(true, false);
					_alreadyFetchedUIThemeEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTerminalEntity = (_terminalEntity != null);
			_alreadyFetchedUIModeEntity = (_uIModeEntity != null);
			_alreadyFetchedUIScheduleEntity = (_uIScheduleEntity != null);
			_alreadyFetchedUIThemeEntity = (_uIThemeEntity != null);
			_alreadyFetchedAdvertisementConfigurationEntity = (_advertisementConfigurationEntity != null);
			_alreadyFetchedClientEntity = (_clientEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedDeliverypointEntity = (_deliverypointEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedEntertainmentConfigurationEntity = (_entertainmentConfigurationEntity != null);
			_alreadyFetchedInfraredConfigurationEntity = (_infraredConfigurationEntity != null);
			_alreadyFetchedMapEntity = (_mapEntity != null);
			_alreadyFetchedMenuEntity = (_menuEntity != null);
			_alreadyFetchedPointOfInterestEntity = (_pointOfInterestEntity != null);
			_alreadyFetchedPriceScheduleEntity = (_priceScheduleEntity != null);
			_alreadyFetchedRoomControlConfigurationEntity = (_roomControlConfigurationEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
			_alreadyFetchedTerminalConfigurationEntity = (_terminalConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingTerminalId);
					break;
				case "UIModeEntity":
					toReturn.Add(Relations.UIModeEntityUsingUIModeId);
					break;
				case "UIScheduleEntity":
					toReturn.Add(Relations.UIScheduleEntityUsingUIScheduleId);
					break;
				case "UIThemeEntity":
					toReturn.Add(Relations.UIThemeEntityUsingUIThemeId);
					break;
				case "AdvertisementConfigurationEntity":
					toReturn.Add(Relations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
					break;
				case "ClientEntity":
					toReturn.Add(Relations.ClientEntityUsingClientId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "DeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingDeliverypointId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "EntertainmentConfigurationEntity":
					toReturn.Add(Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
					break;
				case "InfraredConfigurationEntity":
					toReturn.Add(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
					break;
				case "MapEntity":
					toReturn.Add(Relations.MapEntityUsingMapId);
					break;
				case "MenuEntity":
					toReturn.Add(Relations.MenuEntityUsingMenuId);
					break;
				case "PointOfInterestEntity":
					toReturn.Add(Relations.PointOfInterestEntityUsingPointOfInterestId);
					break;
				case "PriceScheduleEntity":
					toReturn.Add(Relations.PriceScheduleEntityUsingPriceScheduleId);
					break;
				case "RoomControlConfigurationEntity":
					toReturn.Add(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "TerminalConfigurationEntity":
					toReturn.Add(Relations.TerminalConfigurationEntityUsingTerminalConfigurationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_terminalEntity", (!this.MarkedForDeletion?_terminalEntity:null));
			info.AddValue("_terminalEntityReturnsNewIfNotFound", _terminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalEntity", _alwaysFetchTerminalEntity);
			info.AddValue("_alreadyFetchedTerminalEntity", _alreadyFetchedTerminalEntity);
			info.AddValue("_uIModeEntity", (!this.MarkedForDeletion?_uIModeEntity:null));
			info.AddValue("_uIModeEntityReturnsNewIfNotFound", _uIModeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIModeEntity", _alwaysFetchUIModeEntity);
			info.AddValue("_alreadyFetchedUIModeEntity", _alreadyFetchedUIModeEntity);
			info.AddValue("_uIScheduleEntity", (!this.MarkedForDeletion?_uIScheduleEntity:null));
			info.AddValue("_uIScheduleEntityReturnsNewIfNotFound", _uIScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIScheduleEntity", _alwaysFetchUIScheduleEntity);
			info.AddValue("_alreadyFetchedUIScheduleEntity", _alreadyFetchedUIScheduleEntity);
			info.AddValue("_uIThemeEntity", (!this.MarkedForDeletion?_uIThemeEntity:null));
			info.AddValue("_uIThemeEntityReturnsNewIfNotFound", _uIThemeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIThemeEntity", _alwaysFetchUIThemeEntity);
			info.AddValue("_alreadyFetchedUIThemeEntity", _alreadyFetchedUIThemeEntity);

			info.AddValue("_advertisementConfigurationEntity", (!this.MarkedForDeletion?_advertisementConfigurationEntity:null));
			info.AddValue("_advertisementConfigurationEntityReturnsNewIfNotFound", _advertisementConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementConfigurationEntity", _alwaysFetchAdvertisementConfigurationEntity);
			info.AddValue("_alreadyFetchedAdvertisementConfigurationEntity", _alreadyFetchedAdvertisementConfigurationEntity);

			info.AddValue("_clientEntity", (!this.MarkedForDeletion?_clientEntity:null));
			info.AddValue("_clientEntityReturnsNewIfNotFound", _clientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientEntity", _alwaysFetchClientEntity);
			info.AddValue("_alreadyFetchedClientEntity", _alreadyFetchedClientEntity);

			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			info.AddValue("_deliverypointEntity", (!this.MarkedForDeletion?_deliverypointEntity:null));
			info.AddValue("_deliverypointEntityReturnsNewIfNotFound", _deliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointEntity", _alwaysFetchDeliverypointEntity);
			info.AddValue("_alreadyFetchedDeliverypointEntity", _alreadyFetchedDeliverypointEntity);

			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);

			info.AddValue("_entertainmentConfigurationEntity", (!this.MarkedForDeletion?_entertainmentConfigurationEntity:null));
			info.AddValue("_entertainmentConfigurationEntityReturnsNewIfNotFound", _entertainmentConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentConfigurationEntity", _alwaysFetchEntertainmentConfigurationEntity);
			info.AddValue("_alreadyFetchedEntertainmentConfigurationEntity", _alreadyFetchedEntertainmentConfigurationEntity);

			info.AddValue("_infraredConfigurationEntity", (!this.MarkedForDeletion?_infraredConfigurationEntity:null));
			info.AddValue("_infraredConfigurationEntityReturnsNewIfNotFound", _infraredConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInfraredConfigurationEntity", _alwaysFetchInfraredConfigurationEntity);
			info.AddValue("_alreadyFetchedInfraredConfigurationEntity", _alreadyFetchedInfraredConfigurationEntity);

			info.AddValue("_mapEntity", (!this.MarkedForDeletion?_mapEntity:null));
			info.AddValue("_mapEntityReturnsNewIfNotFound", _mapEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMapEntity", _alwaysFetchMapEntity);
			info.AddValue("_alreadyFetchedMapEntity", _alreadyFetchedMapEntity);

			info.AddValue("_menuEntity", (!this.MarkedForDeletion?_menuEntity:null));
			info.AddValue("_menuEntityReturnsNewIfNotFound", _menuEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMenuEntity", _alwaysFetchMenuEntity);
			info.AddValue("_alreadyFetchedMenuEntity", _alreadyFetchedMenuEntity);

			info.AddValue("_pointOfInterestEntity", (!this.MarkedForDeletion?_pointOfInterestEntity:null));
			info.AddValue("_pointOfInterestEntityReturnsNewIfNotFound", _pointOfInterestEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPointOfInterestEntity", _alwaysFetchPointOfInterestEntity);
			info.AddValue("_alreadyFetchedPointOfInterestEntity", _alreadyFetchedPointOfInterestEntity);

			info.AddValue("_priceScheduleEntity", (!this.MarkedForDeletion?_priceScheduleEntity:null));
			info.AddValue("_priceScheduleEntityReturnsNewIfNotFound", _priceScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceScheduleEntity", _alwaysFetchPriceScheduleEntity);
			info.AddValue("_alreadyFetchedPriceScheduleEntity", _alreadyFetchedPriceScheduleEntity);

			info.AddValue("_roomControlConfigurationEntity", (!this.MarkedForDeletion?_roomControlConfigurationEntity:null));
			info.AddValue("_roomControlConfigurationEntityReturnsNewIfNotFound", _roomControlConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlConfigurationEntity", _alwaysFetchRoomControlConfigurationEntity);
			info.AddValue("_alreadyFetchedRoomControlConfigurationEntity", _alreadyFetchedRoomControlConfigurationEntity);

			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);

			info.AddValue("_terminalConfigurationEntity", (!this.MarkedForDeletion?_terminalConfigurationEntity:null));
			info.AddValue("_terminalConfigurationEntityReturnsNewIfNotFound", _terminalConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalConfigurationEntity", _alwaysFetchTerminalConfigurationEntity);
			info.AddValue("_alreadyFetchedTerminalConfigurationEntity", _alreadyFetchedTerminalConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TerminalEntity":
					_alreadyFetchedTerminalEntity = true;
					this.TerminalEntity = (TerminalEntity)entity;
					break;
				case "UIModeEntity":
					_alreadyFetchedUIModeEntity = true;
					this.UIModeEntity = (UIModeEntity)entity;
					break;
				case "UIScheduleEntity":
					_alreadyFetchedUIScheduleEntity = true;
					this.UIScheduleEntity = (UIScheduleEntity)entity;
					break;
				case "UIThemeEntity":
					_alreadyFetchedUIThemeEntity = true;
					this.UIThemeEntity = (UIThemeEntity)entity;
					break;
				case "AdvertisementConfigurationEntity":
					_alreadyFetchedAdvertisementConfigurationEntity = true;
					this.AdvertisementConfigurationEntity = (AdvertisementConfigurationEntity)entity;
					break;
				case "ClientEntity":
					_alreadyFetchedClientEntity = true;
					this.ClientEntity = (ClientEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "DeliverypointEntity":
					_alreadyFetchedDeliverypointEntity = true;
					this.DeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "EntertainmentConfigurationEntity":
					_alreadyFetchedEntertainmentConfigurationEntity = true;
					this.EntertainmentConfigurationEntity = (EntertainmentConfigurationEntity)entity;
					break;
				case "InfraredConfigurationEntity":
					_alreadyFetchedInfraredConfigurationEntity = true;
					this.InfraredConfigurationEntity = (InfraredConfigurationEntity)entity;
					break;
				case "MapEntity":
					_alreadyFetchedMapEntity = true;
					this.MapEntity = (MapEntity)entity;
					break;
				case "MenuEntity":
					_alreadyFetchedMenuEntity = true;
					this.MenuEntity = (MenuEntity)entity;
					break;
				case "PointOfInterestEntity":
					_alreadyFetchedPointOfInterestEntity = true;
					this.PointOfInterestEntity = (PointOfInterestEntity)entity;
					break;
				case "PriceScheduleEntity":
					_alreadyFetchedPriceScheduleEntity = true;
					this.PriceScheduleEntity = (PriceScheduleEntity)entity;
					break;
				case "RoomControlConfigurationEntity":
					_alreadyFetchedRoomControlConfigurationEntity = true;
					this.RoomControlConfigurationEntity = (RoomControlConfigurationEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "TerminalConfigurationEntity":
					_alreadyFetchedTerminalConfigurationEntity = true;
					this.TerminalConfigurationEntity = (TerminalConfigurationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TerminalEntity":
					SetupSyncTerminalEntity(relatedEntity);
					break;
				case "UIModeEntity":
					SetupSyncUIModeEntity(relatedEntity);
					break;
				case "UIScheduleEntity":
					SetupSyncUIScheduleEntity(relatedEntity);
					break;
				case "UIThemeEntity":
					SetupSyncUIThemeEntity(relatedEntity);
					break;
				case "AdvertisementConfigurationEntity":
					SetupSyncAdvertisementConfigurationEntity(relatedEntity);
					break;
				case "ClientEntity":
					SetupSyncClientEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "DeliverypointEntity":
					SetupSyncDeliverypointEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "EntertainmentConfigurationEntity":
					SetupSyncEntertainmentConfigurationEntity(relatedEntity);
					break;
				case "InfraredConfigurationEntity":
					SetupSyncInfraredConfigurationEntity(relatedEntity);
					break;
				case "MapEntity":
					SetupSyncMapEntity(relatedEntity);
					break;
				case "MenuEntity":
					SetupSyncMenuEntity(relatedEntity);
					break;
				case "PointOfInterestEntity":
					SetupSyncPointOfInterestEntity(relatedEntity);
					break;
				case "PriceScheduleEntity":
					SetupSyncPriceScheduleEntity(relatedEntity);
					break;
				case "RoomControlConfigurationEntity":
					SetupSyncRoomControlConfigurationEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "TerminalConfigurationEntity":
					SetupSyncTerminalConfigurationEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TerminalEntity":
					DesetupSyncTerminalEntity(false, true);
					break;
				case "UIModeEntity":
					DesetupSyncUIModeEntity(false, true);
					break;
				case "UIScheduleEntity":
					DesetupSyncUIScheduleEntity(false, true);
					break;
				case "UIThemeEntity":
					DesetupSyncUIThemeEntity(false, true);
					break;
				case "AdvertisementConfigurationEntity":
					DesetupSyncAdvertisementConfigurationEntity(false, true);
					break;
				case "ClientEntity":
					DesetupSyncClientEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "DeliverypointEntity":
					DesetupSyncDeliverypointEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "EntertainmentConfigurationEntity":
					DesetupSyncEntertainmentConfigurationEntity(false, true);
					break;
				case "InfraredConfigurationEntity":
					DesetupSyncInfraredConfigurationEntity(false, true);
					break;
				case "MapEntity":
					DesetupSyncMapEntity(false, true);
					break;
				case "MenuEntity":
					DesetupSyncMenuEntity(false, true);
					break;
				case "PointOfInterestEntity":
					DesetupSyncPointOfInterestEntity(false, true);
					break;
				case "PriceScheduleEntity":
					DesetupSyncPriceScheduleEntity(false, true);
					break;
				case "RoomControlConfigurationEntity":
					DesetupSyncRoomControlConfigurationEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "TerminalConfigurationEntity":
					DesetupSyncTerminalConfigurationEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_terminalEntity!=null)
			{
				toReturn.Add(_terminalEntity);
			}
			if(_uIModeEntity!=null)
			{
				toReturn.Add(_uIModeEntity);
			}
			if(_uIScheduleEntity!=null)
			{
				toReturn.Add(_uIScheduleEntity);
			}
			if(_uIThemeEntity!=null)
			{
				toReturn.Add(_uIThemeEntity);
			}
			if(_advertisementConfigurationEntity!=null)
			{
				toReturn.Add(_advertisementConfigurationEntity);
			}
			if(_clientEntity!=null)
			{
				toReturn.Add(_clientEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_deliverypointEntity!=null)
			{
				toReturn.Add(_deliverypointEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_entertainmentConfigurationEntity!=null)
			{
				toReturn.Add(_entertainmentConfigurationEntity);
			}
			if(_infraredConfigurationEntity!=null)
			{
				toReturn.Add(_infraredConfigurationEntity);
			}
			if(_mapEntity!=null)
			{
				toReturn.Add(_mapEntity);
			}
			if(_menuEntity!=null)
			{
				toReturn.Add(_menuEntity);
			}
			if(_pointOfInterestEntity!=null)
			{
				toReturn.Add(_pointOfInterestEntity);
			}
			if(_priceScheduleEntity!=null)
			{
				toReturn.Add(_priceScheduleEntity);
			}
			if(_roomControlConfigurationEntity!=null)
			{
				toReturn.Add(_roomControlConfigurationEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			if(_terminalConfigurationEntity!=null)
			{
				toReturn.Add(_terminalConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationId(Nullable<System.Int32> advertisementConfigurationId)
		{
			return FetchUsingUCAdvertisementConfigurationId( advertisementConfigurationId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationId(Nullable<System.Int32> advertisementConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAdvertisementConfigurationId( advertisementConfigurationId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationId(Nullable<System.Int32> advertisementConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAdvertisementConfigurationId( advertisementConfigurationId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="advertisementConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAdvertisementConfigurationId(Nullable<System.Int32> advertisementConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCAdvertisementConfigurationId(this, this.Transaction, advertisementConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientId(Nullable<System.Int32> clientId)
		{
			return FetchUsingUCClientId( clientId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientId(Nullable<System.Int32> clientId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCClientId( clientId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientId(Nullable<System.Int32> clientId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCClientId( clientId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientId(Nullable<System.Int32> clientId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCClientId(this, this.Transaction, clientId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="companyId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCompanyId(Nullable<System.Int32> companyId)
		{
			return FetchUsingUCCompanyId( companyId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="companyId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCompanyId(Nullable<System.Int32> companyId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCCompanyId( companyId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="companyId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCompanyId(Nullable<System.Int32> companyId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCCompanyId( companyId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="companyId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCCompanyId(Nullable<System.Int32> companyId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCCompanyId(this, this.Transaction, companyId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointId(Nullable<System.Int32> deliverypointId)
		{
			return FetchUsingUCDeliverypointId( deliverypointId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointId(Nullable<System.Int32> deliverypointId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCDeliverypointId( deliverypointId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointId(Nullable<System.Int32> deliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCDeliverypointId( deliverypointId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointId(Nullable<System.Int32> deliverypointId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCDeliverypointId(this, this.Transaction, deliverypointId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupId(Nullable<System.Int32> deliverypointgroupId)
		{
			return FetchUsingUCDeliverypointgroupId( deliverypointgroupId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupId(Nullable<System.Int32> deliverypointgroupId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCDeliverypointgroupId( deliverypointgroupId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupId(Nullable<System.Int32> deliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCDeliverypointgroupId( deliverypointgroupId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="deliverypointgroupId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCDeliverypointgroupId(Nullable<System.Int32> deliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCDeliverypointgroupId(this, this.Transaction, deliverypointgroupId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationId(Nullable<System.Int32> entertainmentConfigurationId)
		{
			return FetchUsingUCEntertainmentConfigurationId( entertainmentConfigurationId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationId(Nullable<System.Int32> entertainmentConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCEntertainmentConfigurationId( entertainmentConfigurationId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationId(Nullable<System.Int32> entertainmentConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCEntertainmentConfigurationId( entertainmentConfigurationId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationId(Nullable<System.Int32> entertainmentConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCEntertainmentConfigurationId(this, this.Transaction, entertainmentConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="infraredConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCInfraredConfigurationId(Nullable<System.Int32> infraredConfigurationId)
		{
			return FetchUsingUCInfraredConfigurationId( infraredConfigurationId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="infraredConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCInfraredConfigurationId(Nullable<System.Int32> infraredConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCInfraredConfigurationId( infraredConfigurationId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="infraredConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCInfraredConfigurationId(Nullable<System.Int32> infraredConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCInfraredConfigurationId( infraredConfigurationId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="infraredConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCInfraredConfigurationId(Nullable<System.Int32> infraredConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCInfraredConfigurationId(this, this.Transaction, infraredConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="mapId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMapId(Nullable<System.Int32> mapId)
		{
			return FetchUsingUCMapId( mapId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="mapId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMapId(Nullable<System.Int32> mapId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCMapId( mapId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="mapId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMapId(Nullable<System.Int32> mapId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCMapId( mapId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="mapId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMapId(Nullable<System.Int32> mapId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCMapId(this, this.Transaction, mapId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="menuId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMenuId(Nullable<System.Int32> menuId)
		{
			return FetchUsingUCMenuId( menuId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="menuId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMenuId(Nullable<System.Int32> menuId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCMenuId( menuId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="menuId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMenuId(Nullable<System.Int32> menuId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCMenuId( menuId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="menuId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCMenuId(Nullable<System.Int32> menuId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCMenuId(this, this.Transaction, menuId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestId(Nullable<System.Int32> pointOfInterestId)
		{
			return FetchUsingUCPointOfInterestId( pointOfInterestId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestId(Nullable<System.Int32> pointOfInterestId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPointOfInterestId( pointOfInterestId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestId(Nullable<System.Int32> pointOfInterestId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPointOfInterestId( pointOfInterestId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="pointOfInterestId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPointOfInterestId(Nullable<System.Int32> pointOfInterestId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCPointOfInterestId(this, this.Transaction, pointOfInterestId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="priceScheduleId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPriceScheduleId(Nullable<System.Int32> priceScheduleId)
		{
			return FetchUsingUCPriceScheduleId( priceScheduleId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="priceScheduleId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPriceScheduleId(Nullable<System.Int32> priceScheduleId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPriceScheduleId( priceScheduleId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="priceScheduleId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPriceScheduleId(Nullable<System.Int32> priceScheduleId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPriceScheduleId( priceScheduleId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="priceScheduleId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPriceScheduleId(Nullable<System.Int32> priceScheduleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCPriceScheduleId(this, this.Transaction, priceScheduleId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="roomControlConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCRoomControlConfigurationId(Nullable<System.Int32> roomControlConfigurationId)
		{
			return FetchUsingUCRoomControlConfigurationId( roomControlConfigurationId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="roomControlConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCRoomControlConfigurationId(Nullable<System.Int32> roomControlConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCRoomControlConfigurationId( roomControlConfigurationId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="roomControlConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCRoomControlConfigurationId(Nullable<System.Int32> roomControlConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCRoomControlConfigurationId( roomControlConfigurationId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="roomControlConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCRoomControlConfigurationId(Nullable<System.Int32> roomControlConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCRoomControlConfigurationId(this, this.Transaction, roomControlConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="siteId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSiteId(Nullable<System.Int32> siteId)
		{
			return FetchUsingUCSiteId( siteId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="siteId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSiteId(Nullable<System.Int32> siteId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCSiteId( siteId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="siteId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSiteId(Nullable<System.Int32> siteId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCSiteId( siteId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="siteId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSiteId(Nullable<System.Int32> siteId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCSiteId(this, this.Transaction, siteId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="terminalConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTerminalConfigurationId(Nullable<System.Int32> terminalConfigurationId)
		{
			return FetchUsingUCTerminalConfigurationId( terminalConfigurationId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="terminalConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTerminalConfigurationId(Nullable<System.Int32> terminalConfigurationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCTerminalConfigurationId( terminalConfigurationId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="terminalConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTerminalConfigurationId(Nullable<System.Int32> terminalConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCTerminalConfigurationId( terminalConfigurationId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="terminalConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTerminalConfigurationId(Nullable<System.Int32> terminalConfigurationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TimestampDAO)CreateDAOInstance()).FetchTimestampUsingUCTerminalConfigurationId(this, this.Transaction, terminalConfigurationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 timestampId)
		{
			return FetchUsingPK(timestampId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 timestampId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(timestampId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 timestampId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(timestampId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 timestampId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(timestampId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TimestampId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TimestampRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleTerminalEntity()
		{
			return GetSingleTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalEntity || forceFetch || _alwaysFetchTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalEntity = newEntity;
				_alreadyFetchedTerminalEntity = fetchResult;
			}
			return _terminalEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public UIModeEntity GetSingleUIModeEntity()
		{
			return GetSingleUIModeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public virtual UIModeEntity GetSingleUIModeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIModeEntity || forceFetch || _alwaysFetchUIModeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIModeEntityUsingUIModeId);
				UIModeEntity newEntity = new UIModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIModeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIModeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIModeEntity = newEntity;
				_alreadyFetchedUIModeEntity = fetchResult;
			}
			return _uIModeEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public UIScheduleEntity GetSingleUIScheduleEntity()
		{
			return GetSingleUIScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public virtual UIScheduleEntity GetSingleUIScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIScheduleEntity || forceFetch || _alwaysFetchUIScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIScheduleEntityUsingUIScheduleId);
				UIScheduleEntity newEntity = new UIScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIScheduleEntity = newEntity;
				_alreadyFetchedUIScheduleEntity = fetchResult;
			}
			return _uIScheduleEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public UIThemeEntity GetSingleUIThemeEntity()
		{
			return GetSingleUIThemeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public virtual UIThemeEntity GetSingleUIThemeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIThemeEntity || forceFetch || _alwaysFetchUIThemeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIThemeEntityUsingUIThemeId);
				UIThemeEntity newEntity = new UIThemeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIThemeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIThemeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIThemeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIThemeEntity = newEntity;
				_alreadyFetchedUIThemeEntity = fetchResult;
			}
			return _uIThemeEntity;
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementConfigurationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementConfigurationEntity' which is related to this entity.</returns>
		public AdvertisementConfigurationEntity GetSingleAdvertisementConfigurationEntity()
		{
			return GetSingleAdvertisementConfigurationEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'AdvertisementConfigurationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementConfigurationEntity' which is related to this entity.</returns>
		public virtual AdvertisementConfigurationEntity GetSingleAdvertisementConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementConfigurationEntity || forceFetch || _alwaysFetchAdvertisementConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
				AdvertisementConfigurationEntity newEntity = new AdvertisementConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementConfigurationEntity = newEntity;
				_alreadyFetchedAdvertisementConfigurationEntity = fetchResult;
			}
			return _advertisementConfigurationEntity;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientEntity()
		{
			return GetSingleClientEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientEntity || forceFetch || _alwaysFetchClientEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientEntity = newEntity;
				_alreadyFetchedClientEntity = fetchResult;
			}
			return _clientEntity;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleDeliverypointEntity()
		{
			return GetSingleDeliverypointEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointEntity || forceFetch || _alwaysFetchDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointEntity = newEntity;
				_alreadyFetchedDeliverypointEntity = fetchResult;
			}
			return _deliverypointEntity;
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentConfigurationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentConfigurationEntity' which is related to this entity.</returns>
		public EntertainmentConfigurationEntity GetSingleEntertainmentConfigurationEntity()
		{
			return GetSingleEntertainmentConfigurationEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'EntertainmentConfigurationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentConfigurationEntity' which is related to this entity.</returns>
		public virtual EntertainmentConfigurationEntity GetSingleEntertainmentConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentConfigurationEntity || forceFetch || _alwaysFetchEntertainmentConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
				EntertainmentConfigurationEntity newEntity = new EntertainmentConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentConfigurationEntity = newEntity;
				_alreadyFetchedEntertainmentConfigurationEntity = fetchResult;
			}
			return _entertainmentConfigurationEntity;
		}

		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public InfraredConfigurationEntity GetSingleInfraredConfigurationEntity()
		{
			return GetSingleInfraredConfigurationEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public virtual InfraredConfigurationEntity GetSingleInfraredConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedInfraredConfigurationEntity || forceFetch || _alwaysFetchInfraredConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
				InfraredConfigurationEntity newEntity = new InfraredConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InfraredConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InfraredConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_infraredConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InfraredConfigurationEntity = newEntity;
				_alreadyFetchedInfraredConfigurationEntity = fetchResult;
			}
			return _infraredConfigurationEntity;
		}

		/// <summary> Retrieves the related entity of type 'MapEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'MapEntity' which is related to this entity.</returns>
		public MapEntity GetSingleMapEntity()
		{
			return GetSingleMapEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'MapEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MapEntity' which is related to this entity.</returns>
		public virtual MapEntity GetSingleMapEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMapEntity || forceFetch || _alwaysFetchMapEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MapEntityUsingMapId);
				MapEntity newEntity = new MapEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MapId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MapEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mapEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MapEntity = newEntity;
				_alreadyFetchedMapEntity = fetchResult;
			}
			return _mapEntity;
		}

		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public MenuEntity GetSingleMenuEntity()
		{
			return GetSingleMenuEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public virtual MenuEntity GetSingleMenuEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMenuEntity || forceFetch || _alwaysFetchMenuEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MenuEntityUsingMenuId);
				MenuEntity newEntity = new MenuEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MenuId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MenuEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_menuEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MenuEntity = newEntity;
				_alreadyFetchedMenuEntity = fetchResult;
			}
			return _menuEntity;
		}

		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public PointOfInterestEntity GetSinglePointOfInterestEntity()
		{
			return GetSinglePointOfInterestEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PointOfInterestEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PointOfInterestEntity' which is related to this entity.</returns>
		public virtual PointOfInterestEntity GetSinglePointOfInterestEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPointOfInterestEntity || forceFetch || _alwaysFetchPointOfInterestEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PointOfInterestEntityUsingPointOfInterestId);
				PointOfInterestEntity newEntity = new PointOfInterestEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PointOfInterestId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PointOfInterestEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pointOfInterestEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PointOfInterestEntity = newEntity;
				_alreadyFetchedPointOfInterestEntity = fetchResult;
			}
			return _pointOfInterestEntity;
		}

		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public PriceScheduleEntity GetSinglePriceScheduleEntity()
		{
			return GetSinglePriceScheduleEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public virtual PriceScheduleEntity GetSinglePriceScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceScheduleEntity || forceFetch || _alwaysFetchPriceScheduleEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceScheduleEntityUsingPriceScheduleId);
				PriceScheduleEntity newEntity = new PriceScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PriceScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceScheduleEntity = newEntity;
				_alreadyFetchedPriceScheduleEntity = fetchResult;
			}
			return _priceScheduleEntity;
		}

		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity()
		{
			return GetSingleRoomControlConfigurationEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public virtual RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlConfigurationEntity || forceFetch || _alwaysFetchRoomControlConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
				RoomControlConfigurationEntity newEntity = new RoomControlConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlConfigurationEntity = newEntity;
				_alreadyFetchedRoomControlConfigurationEntity = fetchResult;
			}
			return _roomControlConfigurationEntity;
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}

		/// <summary> Retrieves the related entity of type 'TerminalConfigurationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TerminalConfigurationEntity' which is related to this entity.</returns>
		public TerminalConfigurationEntity GetSingleTerminalConfigurationEntity()
		{
			return GetSingleTerminalConfigurationEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TerminalConfigurationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalConfigurationEntity' which is related to this entity.</returns>
		public virtual TerminalConfigurationEntity GetSingleTerminalConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalConfigurationEntity || forceFetch || _alwaysFetchTerminalConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalConfigurationEntityUsingTerminalConfigurationId);
				TerminalConfigurationEntity newEntity = new TerminalConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalConfigurationEntity = newEntity;
				_alreadyFetchedTerminalConfigurationEntity = fetchResult;
			}
			return _terminalConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TerminalEntity", _terminalEntity);
			toReturn.Add("UIModeEntity", _uIModeEntity);
			toReturn.Add("UIScheduleEntity", _uIScheduleEntity);
			toReturn.Add("UIThemeEntity", _uIThemeEntity);
			toReturn.Add("AdvertisementConfigurationEntity", _advertisementConfigurationEntity);
			toReturn.Add("ClientEntity", _clientEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("DeliverypointEntity", _deliverypointEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("EntertainmentConfigurationEntity", _entertainmentConfigurationEntity);
			toReturn.Add("InfraredConfigurationEntity", _infraredConfigurationEntity);
			toReturn.Add("MapEntity", _mapEntity);
			toReturn.Add("MenuEntity", _menuEntity);
			toReturn.Add("PointOfInterestEntity", _pointOfInterestEntity);
			toReturn.Add("PriceScheduleEntity", _priceScheduleEntity);
			toReturn.Add("RoomControlConfigurationEntity", _roomControlConfigurationEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("TerminalConfigurationEntity", _terminalConfigurationEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <param name="validator">The validator object for this TimestampEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 timestampId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(timestampId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_terminalEntityReturnsNewIfNotFound = true;
			_uIModeEntityReturnsNewIfNotFound = true;
			_uIScheduleEntityReturnsNewIfNotFound = true;
			_uIThemeEntityReturnsNewIfNotFound = true;
			_advertisementConfigurationEntityReturnsNewIfNotFound = true;
			_clientEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_deliverypointEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_entertainmentConfigurationEntityReturnsNewIfNotFound = true;
			_infraredConfigurationEntityReturnsNewIfNotFound = true;
			_mapEntityReturnsNewIfNotFound = true;
			_menuEntityReturnsNewIfNotFound = true;
			_pointOfInterestEntityReturnsNewIfNotFound = true;
			_priceScheduleEntityReturnsNewIfNotFound = true;
			_roomControlConfigurationEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			_terminalConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimestampId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAdvertisementConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAdvertisementConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAdvertisementConfigurationCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAdvertisementConfigurationCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAdvertisementConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAdvertisementConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedClientUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedClientUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedClientConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedClientConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedClientConfigurationCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedClientConfigurationCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedClientConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedClientConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAmenitiesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAmenitiesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAmenitiesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAmenitiesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAnnouncementActionsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAnnouncementActionsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAvailabilitiesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAvailabilitiesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAvailabilitiesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAvailabilitiesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedCloudStorageAccountsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedCloudStorageAccountsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedCompanyUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedCompanyUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedCompanyCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedCompanyCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedCompanyMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedCompanyMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMessagegroupsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMessagegroupsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMessageTemplatesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMessageTemplatesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedPmsTerminalStatusUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedPmsTerminalStatusUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIModesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIModesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIModesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIModesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIModesMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIModesMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedCompanyVenueCategoriesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedCompanyVenueCategoriesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedCompanyVenueCategoriesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedCompanyVenueCategoriesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMessagesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMessagesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedDeliverypointUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedDeliverypointUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedDeliverypointsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedDeliverypointsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedDeliveryTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedDeliveryTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedNetmessagesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedNetmessagesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedEntertainmentMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedEntertainmentMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedEntertainmentConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedEntertainmentConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedEntertainmentConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedEntertainmentConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedEntertainmentCategoriesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedEntertainmentCategoriesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedEntertainmentCategoriesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedEntertainmentCategoriesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedEntertainmentCategoriesMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedEntertainmentCategoriesMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfraredConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedInfraredConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedInfraredConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MapId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMapUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMapUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAlterationsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAlterationsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAlterationsCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAlterationsCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedAlterationsMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedAlterationsMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMenuUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMenuUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMenuCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMenuCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMenuMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMenuMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedProductgroupsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedProductgroupsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedProductgroupsCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedProductgroupsCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedProductgroupsMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedProductgroupsMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedProductsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedProductsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedProductsCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedProductsCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedProductsMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedProductsMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedSchedulesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedSchedulesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedPointOfInterestUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedPointOfInterestUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedPointOfInterestCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedPointOfInterestCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedPointOfInterestMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedPointOfInterestMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedPointOfInterestVenueCategoriesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedPointOfInterestVenueCategoriesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedPointOfInterestVenueCategoriesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedPointOfInterestVenueCategoriesCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedPriceScheduleUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedPriceScheduleUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedRoomControlConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedRoomControlConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedRoomControlConfigurationCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedRoomControlConfigurationCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedRoomControlConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedRoomControlConfigurationMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedSiteUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedSiteUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedSiteCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedSiteCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedSiteMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedSiteMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedTerminalUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedTerminalUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedOrdersHistoryUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedOrdersHistoryUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedOrdersMasterUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedOrdersMasterUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedOrdersUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedOrdersUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedTerminalConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedTerminalConfigurationUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIModeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIModeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIModeCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIModeCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIModeMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIModeMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIScheduleUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIScheduleUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIThemeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIThemeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIThemeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedUIThemeMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUIThemeMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedCompanyReleasesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedCompanyReleasesUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMapCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMapCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedMapMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedMapMediaUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModifiedInfraredConfigurationCustomTextUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedInfraredConfigurationCustomTextUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _terminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.TerminalEntityUsingTerminalIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.TerminalId } );		
			_terminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _terminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalEntity(IEntityCore relatedEntity)
		{
			if(_terminalEntity!=relatedEntity)
			{		
				DesetupSyncTerminalEntity(true, true);
				_terminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.TerminalEntityUsingTerminalIdStatic, true, ref _alreadyFetchedTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIModeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIModeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.UIModeEntityUsingUIModeIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.UIModeId } );		
			_uIModeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIModeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIModeEntity(IEntityCore relatedEntity)
		{
			if(_uIModeEntity!=relatedEntity)
			{		
				DesetupSyncUIModeEntity(true, true);
				_uIModeEntity = (UIModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.UIModeEntityUsingUIModeIdStatic, true, ref _alreadyFetchedUIModeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIModeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.UIScheduleId } );		
			_uIScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIScheduleEntity(IEntityCore relatedEntity)
		{
			if(_uIScheduleEntity!=relatedEntity)
			{		
				DesetupSyncUIScheduleEntity(true, true);
				_uIScheduleEntity = (UIScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, ref _alreadyFetchedUIScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIThemeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIThemeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.UIThemeEntityUsingUIThemeIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.UIThemeId } );		
			_uIThemeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIThemeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIThemeEntity(IEntityCore relatedEntity)
		{
			if(_uIThemeEntity!=relatedEntity)
			{		
				DesetupSyncUIThemeEntity(true, true);
				_uIThemeEntity = (UIThemeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.UIThemeEntityUsingUIThemeIdStatic, true, ref _alreadyFetchedUIThemeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIThemeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _advertisementConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementConfigurationEntity, new PropertyChangedEventHandler( OnAdvertisementConfigurationEntityPropertyChanged ), "AdvertisementConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.AdvertisementConfigurationId } );
			_advertisementConfigurationEntity = null;
		}
	
		/// <summary> setups the sync logic for member _advertisementConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_advertisementConfigurationEntity!=relatedEntity)
			{
				DesetupSyncAdvertisementConfigurationEntity(true, true);
				_advertisementConfigurationEntity = (AdvertisementConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementConfigurationEntity, new PropertyChangedEventHandler( OnAdvertisementConfigurationEntityPropertyChanged ), "AdvertisementConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic, true, ref _alreadyFetchedAdvertisementConfigurationEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.ClientEntityUsingClientIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.ClientId } );
			_clientEntity = null;
		}
	
		/// <summary> setups the sync logic for member _clientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientEntity(IEntityCore relatedEntity)
		{
			if(_clientEntity!=relatedEntity)
			{
				DesetupSyncClientEntity(true, true);
				_clientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.ClientEntityUsingClientIdStatic, true, ref _alreadyFetchedClientEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.CompanyId } );
			_companyEntity = null;
		}
	
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.DeliverypointId } );
			_deliverypointEntity = null;
		}
	
		/// <summary> setups the sync logic for member _deliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointEntity!=relatedEntity)
			{
				DesetupSyncDeliverypointEntity(true, true);
				_deliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointEntity, new PropertyChangedEventHandler( OnDeliverypointEntityPropertyChanged ), "DeliverypointEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.DeliverypointEntityUsingDeliverypointIdStatic, true, ref _alreadyFetchedDeliverypointEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.DeliverypointgroupId } );
			_deliverypointgroupEntity = null;
		}
	
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.DeliverypointgroupEntityUsingDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentConfigurationEntity, new PropertyChangedEventHandler( OnEntertainmentConfigurationEntityPropertyChanged ), "EntertainmentConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.EntertainmentConfigurationId } );
			_entertainmentConfigurationEntity = null;
		}
	
		/// <summary> setups the sync logic for member _entertainmentConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentConfigurationEntity!=relatedEntity)
			{
				DesetupSyncEntertainmentConfigurationEntity(true, true);
				_entertainmentConfigurationEntity = (EntertainmentConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentConfigurationEntity, new PropertyChangedEventHandler( OnEntertainmentConfigurationEntityPropertyChanged ), "EntertainmentConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic, true, ref _alreadyFetchedEntertainmentConfigurationEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInfraredConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.InfraredConfigurationId } );
			_infraredConfigurationEntity = null;
		}
	
		/// <summary> setups the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInfraredConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_infraredConfigurationEntity!=relatedEntity)
			{
				DesetupSyncInfraredConfigurationEntity(true, true);
				_infraredConfigurationEntity = (InfraredConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, ref _alreadyFetchedInfraredConfigurationEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInfraredConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mapEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMapEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mapEntity, new PropertyChangedEventHandler( OnMapEntityPropertyChanged ), "MapEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.MapEntityUsingMapIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.MapId } );
			_mapEntity = null;
		}
	
		/// <summary> setups the sync logic for member _mapEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMapEntity(IEntityCore relatedEntity)
		{
			if(_mapEntity!=relatedEntity)
			{
				DesetupSyncMapEntity(true, true);
				_mapEntity = (MapEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mapEntity, new PropertyChangedEventHandler( OnMapEntityPropertyChanged ), "MapEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.MapEntityUsingMapIdStatic, true, ref _alreadyFetchedMapEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMapEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _menuEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMenuEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.MenuEntityUsingMenuIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.MenuId } );
			_menuEntity = null;
		}
	
		/// <summary> setups the sync logic for member _menuEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMenuEntity(IEntityCore relatedEntity)
		{
			if(_menuEntity!=relatedEntity)
			{
				DesetupSyncMenuEntity(true, true);
				_menuEntity = (MenuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.MenuEntityUsingMenuIdStatic, true, ref _alreadyFetchedMenuEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMenuEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPointOfInterestEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.PointOfInterestId } );
			_pointOfInterestEntity = null;
		}
	
		/// <summary> setups the sync logic for member _pointOfInterestEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPointOfInterestEntity(IEntityCore relatedEntity)
		{
			if(_pointOfInterestEntity!=relatedEntity)
			{
				DesetupSyncPointOfInterestEntity(true, true);
				_pointOfInterestEntity = (PointOfInterestEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pointOfInterestEntity, new PropertyChangedEventHandler( OnPointOfInterestEntityPropertyChanged ), "PointOfInterestEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.PointOfInterestEntityUsingPointOfInterestIdStatic, true, ref _alreadyFetchedPointOfInterestEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPointOfInterestEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.PriceScheduleId } );
			_priceScheduleEntity = null;
		}
	
		/// <summary> setups the sync logic for member _priceScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceScheduleEntity(IEntityCore relatedEntity)
		{
			if(_priceScheduleEntity!=relatedEntity)
			{
				DesetupSyncPriceScheduleEntity(true, true);
				_priceScheduleEntity = (PriceScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, ref _alreadyFetchedPriceScheduleEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.RoomControlConfigurationId } );
			_roomControlConfigurationEntity = null;
		}
	
		/// <summary> setups the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_roomControlConfigurationEntity!=relatedEntity)
			{
				DesetupSyncRoomControlConfigurationEntity(true, true);
				_roomControlConfigurationEntity = (RoomControlConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, ref _alreadyFetchedRoomControlConfigurationEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.SiteId } );
			_siteEntity = null;
		}
	
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _terminalConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalConfigurationEntity, new PropertyChangedEventHandler( OnTerminalConfigurationEntityPropertyChanged ), "TerminalConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.TerminalConfigurationEntityUsingTerminalConfigurationIdStatic, true, signalRelatedEntity, "TimestampCollection", resetFKFields, new int[] { (int)TimestampFieldIndex.TerminalConfigurationId } );
			_terminalConfigurationEntity = null;
		}
	
		/// <summary> setups the sync logic for member _terminalConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_terminalConfigurationEntity!=relatedEntity)
			{
				DesetupSyncTerminalConfigurationEntity(true, true);
				_terminalConfigurationEntity = (TerminalConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalConfigurationEntity, new PropertyChangedEventHandler( OnTerminalConfigurationEntityPropertyChanged ), "TerminalConfigurationEntity", Obymobi.Data.RelationClasses.StaticTimestampRelations.TerminalConfigurationEntityUsingTerminalConfigurationIdStatic, true, ref _alreadyFetchedTerminalConfigurationEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="timestampId">PK value for Timestamp which data should be fetched into this Timestamp object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 timestampId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TimestampFieldIndex.TimestampId].ForcedCurrentValueWrite(timestampId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTimestampDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TimestampEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TimestampRelations Relations
		{
			get	{ return new TimestampRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("UIModeEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "UIModeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UISchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleCollection(), (IEntityRelation)GetRelationsForField("UIScheduleEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.UIScheduleEntity, 0, null, null, null, "UIScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITheme'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIThemeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIThemeCollection(), (IEntityRelation)GetRelationsForField("UIThemeEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.UIThemeEntity, 0, null, null, null, "UIThemeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementConfigurationCollection(), (IEntityRelation)GetRelationsForField("AdvertisementConfigurationEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.AdvertisementConfigurationEntity, 0, null, null, null, "AdvertisementConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntertainmentConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentConfigurationCollection(), (IEntityRelation)GetRelationsForField("EntertainmentConfigurationEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.EntertainmentConfigurationEntity, 0, null, null, null, "EntertainmentConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InfraredConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInfraredConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.InfraredConfigurationCollection(), (IEntityRelation)GetRelationsForField("InfraredConfigurationEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, 0, null, null, null, "InfraredConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Map'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMapEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MapCollection(), (IEntityRelation)GetRelationsForField("MapEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.MapEntity, 0, null, null, null, "MapEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), (IEntityRelation)GetRelationsForField("MenuEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, null, "MenuEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceSchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceScheduleCollection(), (IEntityRelation)GetRelationsForField("PriceScheduleEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.PriceScheduleEntity, 0, null, null, null, "PriceScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlConfigurationCollection(), (IEntityRelation)GetRelationsForField("RoomControlConfigurationEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.RoomControlConfigurationEntity, 0, null, null, null, "RoomControlConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TerminalConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalConfigurationCollection(), (IEntityRelation)GetRelationsForField("TerminalConfigurationEntity")[0], (int)Obymobi.Data.EntityType.TimestampEntity, (int)Obymobi.Data.EntityType.TerminalConfigurationEntity, 0, null, null, null, "TerminalConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The TimestampId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."TimestampId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 TimestampId
		{
			get { return (System.Int32)GetValue((int)TimestampFieldIndex.TimestampId, true); }
			set	{ SetValue((int)TimestampFieldIndex.TimestampId, value, true); }
		}

		/// <summary> The AdvertisementConfigurationId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."AdvertisementConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AdvertisementConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.AdvertisementConfigurationId, false); }
			set	{ SetValue((int)TimestampFieldIndex.AdvertisementConfigurationId, value, true); }
		}

		/// <summary> The ModifiedAdvertisementConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAdvertisementConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAdvertisementConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAdvertisementConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAdvertisementConfigurationUTC, value, true); }
		}

		/// <summary> The PublishedAdvertisementConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAdvertisementConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAdvertisementConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAdvertisementConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAdvertisementConfigurationUTC, value, true); }
		}

		/// <summary> The ModifiedAdvertisementConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAdvertisementConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAdvertisementConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAdvertisementConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAdvertisementConfigurationCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedAdvertisementConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAdvertisementConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAdvertisementConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAdvertisementConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAdvertisementConfigurationCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedAdvertisementConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAdvertisementConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAdvertisementConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAdvertisementConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAdvertisementConfigurationMediaUTC, value, true); }
		}

		/// <summary> The PublishedAdvertisementConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAdvertisementConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAdvertisementConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAdvertisementConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAdvertisementConfigurationMediaUTC, value, true); }
		}

		/// <summary> The ClientId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.ClientId, false); }
			set	{ SetValue((int)TimestampFieldIndex.ClientId, value, true); }
		}

		/// <summary> The ModifiedClientUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedClientUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedClientUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedClientUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedClientUTC, value, true); }
		}

		/// <summary> The PublishedClientUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedClientUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedClientUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedClientUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedClientUTC, value, true); }
		}

		/// <summary> The ClientConfigurationId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)TimestampFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The ModifiedClientConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedClientConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedClientConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedClientConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedClientConfigurationUTC, value, true); }
		}

		/// <summary> The PublishedClientConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedClientConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedClientConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedClientConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedClientConfigurationUTC, value, true); }
		}

		/// <summary> The ModifiedClientConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedClientConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedClientConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedClientConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedClientConfigurationCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedClientConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedClientConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedClientConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedClientConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedClientConfigurationCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedClientConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedClientConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedClientConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedClientConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedClientConfigurationMediaUTC, value, true); }
		}

		/// <summary> The PublishedClientConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedClientConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedClientConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedClientConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedClientConfigurationMediaUTC, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.CompanyId, false); }
			set	{ SetValue((int)TimestampFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The ModifiedAmenitiesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAmenitiesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAmenitiesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAmenitiesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAmenitiesUTC, value, true); }
		}

		/// <summary> The PublishedAmenitiesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAmenitiesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAmenitiesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAmenitiesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAmenitiesUTC, value, true); }
		}

		/// <summary> The ModifiedAmenitiesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAmenitiesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAmenitiesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAmenitiesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAmenitiesCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedAmenitiesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAmenitiesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAmenitiesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAmenitiesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAmenitiesCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedAnnouncementActionsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAnnouncementActionsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAnnouncementActionsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAnnouncementActionsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAnnouncementActionsUTC, value, true); }
		}

		/// <summary> The PublishedAnnouncementActionsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAnnouncementActionsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAnnouncementActionsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAnnouncementActionsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAnnouncementActionsUTC, value, true); }
		}

		/// <summary> The ModifiedAvailabilitiesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAvailabilitiesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAvailabilitiesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAvailabilitiesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAvailabilitiesUTC, value, true); }
		}

		/// <summary> The PublishedAvailabilitiesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAvailabilitiesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAvailabilitiesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAvailabilitiesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAvailabilitiesUTC, value, true); }
		}

		/// <summary> The ModifiedAvailabilitiesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAvailabilitiesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAvailabilitiesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAvailabilitiesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAvailabilitiesCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedAvailabilitiesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAvailabilitiesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAvailabilitiesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAvailabilitiesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAvailabilitiesCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedCloudStorageAccountsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedCloudStorageAccountsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedCloudStorageAccountsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedCloudStorageAccountsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedCloudStorageAccountsUTC, value, true); }
		}

		/// <summary> The PublishedCloudStorageAccountsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedCloudStorageAccountsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedCloudStorageAccountsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedCloudStorageAccountsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedCloudStorageAccountsUTC, value, true); }
		}

		/// <summary> The ModifiedCompanyUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedCompanyUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedCompanyUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedCompanyUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedCompanyUTC, value, true); }
		}

		/// <summary> The PublishedCompanyUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedCompanyUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedCompanyUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedCompanyUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedCompanyUTC, value, true); }
		}

		/// <summary> The ModifiedCompanyCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedCompanyCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedCompanyCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedCompanyCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedCompanyCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedCompanyCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedCompanyCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedCompanyCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedCompanyCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedCompanyCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedCompanyMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedCompanyMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedCompanyMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedCompanyMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedCompanyMediaUTC, value, true); }
		}

		/// <summary> The PublishedCompanyMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedCompanyMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedCompanyMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedCompanyMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedCompanyMediaUTC, value, true); }
		}

		/// <summary> The ModifiedMessagegroupsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMessagegroupsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMessagegroupsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMessagegroupsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMessagegroupsUTC, value, true); }
		}

		/// <summary> The PublishedMessagegroupsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMessagegroupsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMessagegroupsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMessagegroupsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMessagegroupsUTC, value, true); }
		}

		/// <summary> The ModifiedMessageTemplatesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMessageTemplatesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMessageTemplatesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMessageTemplatesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMessageTemplatesUTC, value, true); }
		}

		/// <summary> The PublishedMessageTemplatesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMessageTemplatesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMessageTemplatesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMessageTemplatesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMessageTemplatesUTC, value, true); }
		}

		/// <summary> The ModifiedPmsTerminalStatusUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedPmsTerminalStatusUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedPmsTerminalStatusUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedPmsTerminalStatusUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedPmsTerminalStatusUTC, value, true); }
		}

		/// <summary> The PublishedPmsTerminalStatusUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedPmsTerminalStatusUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedPmsTerminalStatusUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedPmsTerminalStatusUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedPmsTerminalStatusUTC, value, true); }
		}

		/// <summary> The ModifiedUIModesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIModesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIModesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIModesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIModesUTC, value, true); }
		}

		/// <summary> The PublishedUIModesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIModesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIModesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIModesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIModesUTC, value, true); }
		}

		/// <summary> The ModifiedUIModesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIModesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIModesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIModesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIModesCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedUIModesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIModesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIModesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIModesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIModesCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedUIModesMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIModesMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIModesMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIModesMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIModesMediaUTC, value, true); }
		}

		/// <summary> The PublishedUIModesMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIModesMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIModesMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIModesMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIModesMediaUTC, value, true); }
		}

		/// <summary> The ModifiedCompanyVenueCategoriesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedCompanyVenueCategoriesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedCompanyVenueCategoriesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedCompanyVenueCategoriesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedCompanyVenueCategoriesUTC, value, true); }
		}

		/// <summary> The PublishedCompanyVenueCategoriesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedCompanyVenueCategoriesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedCompanyVenueCategoriesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedCompanyVenueCategoriesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedCompanyVenueCategoriesUTC, value, true); }
		}

		/// <summary> The ModifiedCompanyVenueCategoriesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedCompanyVenueCategoriesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedCompanyVenueCategoriesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedCompanyVenueCategoriesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedCompanyVenueCategoriesCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedCompanyVenueCategoriesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedCompanyVenueCategoriesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedCompanyVenueCategoriesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedCompanyVenueCategoriesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedCompanyVenueCategoriesCustomTextUTC, value, true); }
		}

		/// <summary> The DeliverypointId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.DeliverypointId, false); }
			set	{ SetValue((int)TimestampFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The ModifiedMessagesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMessagesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMessagesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMessagesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMessagesUTC, value, true); }
		}

		/// <summary> The PublishedMessagesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMessagesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMessagesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMessagesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMessagesUTC, value, true); }
		}

		/// <summary> The ModifiedDeliverypointUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedDeliverypointUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedDeliverypointUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedDeliverypointUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedDeliverypointUTC, value, true); }
		}

		/// <summary> The PublishedDeliverypointUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedDeliverypointUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedDeliverypointUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedDeliverypointUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedDeliverypointUTC, value, true); }
		}

		/// <summary> The DeliverypointgroupId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.DeliverypointgroupId, false); }
			set	{ SetValue((int)TimestampFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The ModifiedDeliverypointsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedDeliverypointsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedDeliverypointsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedDeliverypointsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedDeliverypointsUTC, value, true); }
		}

		/// <summary> The PublishedDeliverypointsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedDeliverypointsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedDeliverypointsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedDeliverypointsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedDeliverypointsUTC, value, true); }
		}

		/// <summary> The ModifiedDeliveryTimeUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedDeliveryTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedDeliveryTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedDeliveryTimeUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedDeliveryTimeUTC, value, true); }
		}

		/// <summary> The PublishedDeliveryTimeUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedDeliveryTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedDeliveryTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedDeliveryTimeUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedDeliveryTimeUTC, value, true); }
		}

		/// <summary> The DeviceIdentifier property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."DeviceIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeviceIdentifier
		{
			get { return (System.String)GetValue((int)TimestampFieldIndex.DeviceIdentifier, true); }
			set	{ SetValue((int)TimestampFieldIndex.DeviceIdentifier, value, true); }
		}

		/// <summary> The ModifiedNetmessagesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedNetmessagesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedNetmessagesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedNetmessagesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedNetmessagesUTC, value, true); }
		}

		/// <summary> The PublishedNetmessagesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedNetmessagesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedNetmessagesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedNetmessagesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedNetmessagesUTC, value, true); }
		}

		/// <summary> The EntertainmentConfigurationId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."EntertainmentConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.EntertainmentConfigurationId, false); }
			set	{ SetValue((int)TimestampFieldIndex.EntertainmentConfigurationId, value, true); }
		}

		/// <summary> The ModifiedEntertainmentMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedEntertainmentMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedEntertainmentMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedEntertainmentMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedEntertainmentMediaUTC, value, true); }
		}

		/// <summary> The PublishedEntertainmentMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedEntertainmentMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedEntertainmentMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedEntertainmentMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedEntertainmentMediaUTC, value, true); }
		}

		/// <summary> The ModifiedEntertainmentConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedEntertainmentConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedEntertainmentConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedEntertainmentConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedEntertainmentConfigurationUTC, value, true); }
		}

		/// <summary> The PublishedEntertainmentConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedEntertainmentConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedEntertainmentConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedEntertainmentConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedEntertainmentConfigurationUTC, value, true); }
		}

		/// <summary> The ModifiedEntertainmentConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedEntertainmentConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedEntertainmentConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedEntertainmentConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedEntertainmentConfigurationMediaUTC, value, true); }
		}

		/// <summary> The PublishedEntertainmentConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedEntertainmentConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedEntertainmentConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedEntertainmentConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedEntertainmentConfigurationMediaUTC, value, true); }
		}

		/// <summary> The ModifiedEntertainmentCategoriesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedEntertainmentCategoriesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedEntertainmentCategoriesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedEntertainmentCategoriesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedEntertainmentCategoriesUTC, value, true); }
		}

		/// <summary> The PublishedEntertainmentCategoriesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedEntertainmentCategoriesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedEntertainmentCategoriesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedEntertainmentCategoriesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedEntertainmentCategoriesUTC, value, true); }
		}

		/// <summary> The ModifiedEntertainmentCategoriesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedEntertainmentCategoriesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedEntertainmentCategoriesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedEntertainmentCategoriesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedEntertainmentCategoriesCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedEntertainmentCategoriesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedEntertainmentCategoriesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedEntertainmentCategoriesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedEntertainmentCategoriesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedEntertainmentCategoriesCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedEntertainmentCategoriesMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedEntertainmentCategoriesMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedEntertainmentCategoriesMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedEntertainmentCategoriesMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedEntertainmentCategoriesMediaUTC, value, true); }
		}

		/// <summary> The PublishedEntertainmentCategoriesMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedEntertainmentCategoriesMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedEntertainmentCategoriesMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedEntertainmentCategoriesMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedEntertainmentCategoriesMediaUTC, value, true); }
		}

		/// <summary> The InfraredConfigurationId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."InfraredConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InfraredConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.InfraredConfigurationId, false); }
			set	{ SetValue((int)TimestampFieldIndex.InfraredConfigurationId, value, true); }
		}

		/// <summary> The ModifiedInfraredConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedInfraredConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedInfraredConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedInfraredConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedInfraredConfigurationUTC, value, true); }
		}

		/// <summary> The PublishedInfraredConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedInfraredConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedInfraredConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedInfraredConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedInfraredConfigurationUTC, value, true); }
		}

		/// <summary> The MapId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."MapId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MapId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.MapId, false); }
			set	{ SetValue((int)TimestampFieldIndex.MapId, value, true); }
		}

		/// <summary> The ModifiedMapUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMapUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMapUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMapUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMapUTC, value, true); }
		}

		/// <summary> The PublishedMapUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMapUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMapUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMapUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMapUTC, value, true); }
		}

		/// <summary> The MenuId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."MenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.MenuId, false); }
			set	{ SetValue((int)TimestampFieldIndex.MenuId, value, true); }
		}

		/// <summary> The ModifiedAlterationsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAlterationsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAlterationsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAlterationsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAlterationsUTC, value, true); }
		}

		/// <summary> The PublishedAlterationsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAlterationsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAlterationsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAlterationsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAlterationsUTC, value, true); }
		}

		/// <summary> The ModifiedAlterationsCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAlterationsCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAlterationsCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAlterationsCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAlterationsCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedAlterationsCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAlterationsCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAlterationsCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAlterationsCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAlterationsCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedAlterationsMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedAlterationsMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedAlterationsMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedAlterationsMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedAlterationsMediaUTC, value, true); }
		}

		/// <summary> The PublishedAlterationsMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedAlterationsMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedAlterationsMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedAlterationsMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedAlterationsMediaUTC, value, true); }
		}

		/// <summary> The ModifiedMenuUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMenuUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMenuUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMenuUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMenuUTC, value, true); }
		}

		/// <summary> The PublishedMenuUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMenuUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMenuUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMenuUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMenuUTC, value, true); }
		}

		/// <summary> The ModifiedMenuCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMenuCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMenuCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMenuCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMenuCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedMenuCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMenuCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMenuCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMenuCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMenuCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedMenuMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMenuMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMenuMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMenuMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMenuMediaUTC, value, true); }
		}

		/// <summary> The PublishedMenuMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMenuMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMenuMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMenuMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMenuMediaUTC, value, true); }
		}

		/// <summary> The ModifiedProductgroupsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedProductgroupsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedProductgroupsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedProductgroupsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedProductgroupsUTC, value, true); }
		}

		/// <summary> The PublishedProductgroupsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedProductgroupsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedProductgroupsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedProductgroupsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedProductgroupsUTC, value, true); }
		}

		/// <summary> The ModifiedProductgroupsCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedProductgroupsCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedProductgroupsCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedProductgroupsCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedProductgroupsCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedProductgroupsCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedProductgroupsCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedProductgroupsCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedProductgroupsCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedProductgroupsCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedProductgroupsMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedProductgroupsMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedProductgroupsMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedProductgroupsMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedProductgroupsMediaUTC, value, true); }
		}

		/// <summary> The PublishedProductgroupsMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedProductgroupsMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedProductgroupsMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedProductgroupsMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedProductgroupsMediaUTC, value, true); }
		}

		/// <summary> The ModifiedProductsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedProductsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedProductsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedProductsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedProductsUTC, value, true); }
		}

		/// <summary> The PublishedProductsUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedProductsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedProductsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedProductsUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedProductsUTC, value, true); }
		}

		/// <summary> The ModifiedProductsCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedProductsCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedProductsCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedProductsCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedProductsCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedProductsCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedProductsCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedProductsCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedProductsCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedProductsCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedProductsMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedProductsMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedProductsMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedProductsMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedProductsMediaUTC, value, true); }
		}

		/// <summary> The PublishedProductsMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedProductsMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedProductsMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedProductsMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedProductsMediaUTC, value, true); }
		}

		/// <summary> The ModifiedSchedulesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedSchedulesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedSchedulesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedSchedulesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedSchedulesUTC, value, true); }
		}

		/// <summary> The PublishedSchedulesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedSchedulesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedSchedulesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedSchedulesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedSchedulesUTC, value, true); }
		}

		/// <summary> The PointOfInterestId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PointOfInterestId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PointOfInterestId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.PointOfInterestId, false); }
			set	{ SetValue((int)TimestampFieldIndex.PointOfInterestId, value, true); }
		}

		/// <summary> The ModifiedPointOfInterestUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedPointOfInterestUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedPointOfInterestUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedPointOfInterestUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedPointOfInterestUTC, value, true); }
		}

		/// <summary> The PublishedPointOfInterestUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedPointOfInterestUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedPointOfInterestUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedPointOfInterestUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedPointOfInterestUTC, value, true); }
		}

		/// <summary> The ModifiedPointOfInterestCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedPointOfInterestCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedPointOfInterestCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedPointOfInterestCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedPointOfInterestCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedPointOfInterestCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedPointOfInterestCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedPointOfInterestCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedPointOfInterestCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedPointOfInterestCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedPointOfInterestMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedPointOfInterestMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedPointOfInterestMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedPointOfInterestMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedPointOfInterestMediaUTC, value, true); }
		}

		/// <summary> The PublishedPointOfInterestMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedPointOfInterestMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedPointOfInterestMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedPointOfInterestMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedPointOfInterestMediaUTC, value, true); }
		}

		/// <summary> The ModifiedPointOfInterestVenueCategoriesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedPointOfInterestVenueCategoriesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedPointOfInterestVenueCategoriesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesUTC, value, true); }
		}

		/// <summary> The PublishedPointOfInterestVenueCategoriesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedPointOfInterestVenueCategoriesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedPointOfInterestVenueCategoriesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesUTC, value, true); }
		}

		/// <summary> The ModifiedPointOfInterestVenueCategoriesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedPointOfInterestVenueCategoriesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedPointOfInterestVenueCategoriesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedPointOfInterestVenueCategoriesCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedPointOfInterestVenueCategoriesCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedPointOfInterestVenueCategoriesCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesCustomTextUTC, value, true); }
		}

		/// <summary> The PriceScheduleId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PriceScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.PriceScheduleId, false); }
			set	{ SetValue((int)TimestampFieldIndex.PriceScheduleId, value, true); }
		}

		/// <summary> The ModifiedPriceScheduleUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedPriceScheduleUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedPriceScheduleUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedPriceScheduleUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedPriceScheduleUTC, value, true); }
		}

		/// <summary> The PublishedPriceScheduleUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedPriceScheduleUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedPriceScheduleUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedPriceScheduleUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedPriceScheduleUTC, value, true); }
		}

		/// <summary> The RoomControlConfigurationId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."RoomControlConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.RoomControlConfigurationId, false); }
			set	{ SetValue((int)TimestampFieldIndex.RoomControlConfigurationId, value, true); }
		}

		/// <summary> The ModifiedRoomControlConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedRoomControlConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedRoomControlConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedRoomControlConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedRoomControlConfigurationUTC, value, true); }
		}

		/// <summary> The PublishedRoomControlConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedRoomControlConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedRoomControlConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedRoomControlConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedRoomControlConfigurationUTC, value, true); }
		}

		/// <summary> The ModifiedRoomControlConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedRoomControlConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedRoomControlConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedRoomControlConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedRoomControlConfigurationCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedRoomControlConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedRoomControlConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedRoomControlConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedRoomControlConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedRoomControlConfigurationCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedRoomControlConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedRoomControlConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedRoomControlConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedRoomControlConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedRoomControlConfigurationMediaUTC, value, true); }
		}

		/// <summary> The PublishedRoomControlConfigurationMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedRoomControlConfigurationMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedRoomControlConfigurationMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedRoomControlConfigurationMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedRoomControlConfigurationMediaUTC, value, true); }
		}

		/// <summary> The SiteId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.SiteId, false); }
			set	{ SetValue((int)TimestampFieldIndex.SiteId, value, true); }
		}

		/// <summary> The ModifiedSiteUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedSiteUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedSiteUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedSiteUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedSiteUTC, value, true); }
		}

		/// <summary> The PublishedSiteUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedSiteUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedSiteUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedSiteUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedSiteUTC, value, true); }
		}

		/// <summary> The ModifiedSiteCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedSiteCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedSiteCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedSiteCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedSiteCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedSiteCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedSiteCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedSiteCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedSiteCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedSiteCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedSiteMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedSiteMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedSiteMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedSiteMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedSiteMediaUTC, value, true); }
		}

		/// <summary> The PublishedSiteMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedSiteMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedSiteMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedSiteMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedSiteMediaUTC, value, true); }
		}

		/// <summary> The TerminalId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."TerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.TerminalId, false); }
			set	{ SetValue((int)TimestampFieldIndex.TerminalId, value, true); }
		}

		/// <summary> The ModifiedTerminalUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedTerminalUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedTerminalUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedTerminalUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedTerminalUTC, value, true); }
		}

		/// <summary> The PublishedTerminalUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedTerminalUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedTerminalUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedTerminalUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedTerminalUTC, value, true); }
		}

		/// <summary> The ModifiedOrdersHistoryUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedOrdersHistoryUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedOrdersHistoryUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedOrdersHistoryUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedOrdersHistoryUTC, value, true); }
		}

		/// <summary> The PublishedOrdersHistoryUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedOrdersHistoryUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedOrdersHistoryUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedOrdersHistoryUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedOrdersHistoryUTC, value, true); }
		}

		/// <summary> The ModifiedOrdersMasterUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedOrdersMasterUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedOrdersMasterUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedOrdersMasterUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedOrdersMasterUTC, value, true); }
		}

		/// <summary> The PublishedOrdersMasterUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedOrdersMasterUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedOrdersMasterUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedOrdersMasterUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedOrdersMasterUTC, value, true); }
		}

		/// <summary> The ModifiedOrdersUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedOrdersUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedOrdersUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedOrdersUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedOrdersUTC, value, true); }
		}

		/// <summary> The PublishedOrdersUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedOrdersUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedOrdersUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedOrdersUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedOrdersUTC, value, true); }
		}

		/// <summary> The TerminalConfigurationId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."TerminalConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.TerminalConfigurationId, false); }
			set	{ SetValue((int)TimestampFieldIndex.TerminalConfigurationId, value, true); }
		}

		/// <summary> The ModifiedTerminalConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedTerminalConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedTerminalConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedTerminalConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedTerminalConfigurationUTC, value, true); }
		}

		/// <summary> The PublishedTerminalConfigurationUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedTerminalConfigurationUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedTerminalConfigurationUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedTerminalConfigurationUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedTerminalConfigurationUTC, value, true); }
		}

		/// <summary> The UIModeId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."UIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.UIModeId, false); }
			set	{ SetValue((int)TimestampFieldIndex.UIModeId, value, true); }
		}

		/// <summary> The ModifiedUIModeUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIModeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIModeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIModeUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIModeUTC, value, true); }
		}

		/// <summary> The PublishedUIModeUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIModeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIModeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIModeUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIModeUTC, value, true); }
		}

		/// <summary> The ModifiedUIModeCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIModeCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIModeCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIModeCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIModeCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedUIModeCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIModeCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIModeCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIModeCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIModeCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedUIModeMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIModeMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIModeMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIModeMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIModeMediaUTC, value, true); }
		}

		/// <summary> The PublishedUIModeMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIModeMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIModeMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIModeMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIModeMediaUTC, value, true); }
		}

		/// <summary> The UIScheduleId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."UIScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.UIScheduleId, false); }
			set	{ SetValue((int)TimestampFieldIndex.UIScheduleId, value, true); }
		}

		/// <summary> The ModifiedUIScheduleUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIScheduleUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIScheduleUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIScheduleUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIScheduleUTC, value, true); }
		}

		/// <summary> The PublishedUIScheduleUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIScheduleUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIScheduleUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIScheduleUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIScheduleUTC, value, true); }
		}

		/// <summary> The UIThemeId property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."UIThemeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIThemeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.UIThemeId, false); }
			set	{ SetValue((int)TimestampFieldIndex.UIThemeId, value, true); }
		}

		/// <summary> The ModifiedUIThemeUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIThemeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIThemeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIThemeUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIThemeUTC, value, true); }
		}

		/// <summary> The PublishedUIThemeUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIThemeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIThemeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIThemeUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIThemeUTC, value, true); }
		}

		/// <summary> The ModifiedUIThemeMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedUIThemeMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedUIThemeMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedUIThemeMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedUIThemeMediaUTC, value, true); }
		}

		/// <summary> The PublishedUIThemeMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedUIThemeMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedUIThemeMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedUIThemeMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedUIThemeMediaUTC, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)TimestampFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)TimestampFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)TimestampFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ModifiedCompanyReleasesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedCompanyReleasesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedCompanyReleasesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedCompanyReleasesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedCompanyReleasesUTC, value, true); }
		}

		/// <summary> The PublishedCompanyReleasesUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedCompanyReleasesUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedCompanyReleasesUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedCompanyReleasesUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedCompanyReleasesUTC, value, true); }
		}

		/// <summary> The ModifiedMapCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMapCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMapCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMapCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMapCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedMapCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMapCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMapCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMapCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMapCustomTextUTC, value, true); }
		}

		/// <summary> The ModifiedMapMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedMapMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedMapMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedMapMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedMapMediaUTC, value, true); }
		}

		/// <summary> The PublishedMapMediaUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedMapMediaUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedMapMediaUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedMapMediaUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedMapMediaUTC, value, true); }
		}

		/// <summary> The ModifiedInfraredConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."ModifiedInfraredConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ModifiedInfraredConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.ModifiedInfraredConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.ModifiedInfraredConfigurationCustomTextUTC, value, true); }
		}

		/// <summary> The PublishedInfraredConfigurationCustomTextUTC property of the Entity Timestamp<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Timestamp"."PublishedInfraredConfigurationCustomTextUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PublishedInfraredConfigurationCustomTextUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TimestampFieldIndex.PublishedInfraredConfigurationCustomTextUTC, false); }
			set	{ SetValue((int)TimestampFieldIndex.PublishedInfraredConfigurationCustomTextUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity TerminalEntity
		{
			get	{ return GetSingleTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TimestampCollection", "TerminalEntity", _terminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalEntity. When set to true, TerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalEntity
		{
			get	{ return _alwaysFetchTerminalEntity; }
			set	{ _alwaysFetchTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalEntity already has been fetched. Setting this property to false when TerminalEntity has been fetched
		/// will set TerminalEntity to null as well. Setting this property to true while TerminalEntity hasn't been fetched disables lazy loading for TerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalEntity
		{
			get { return _alreadyFetchedTerminalEntity;}
			set 
			{
				if(_alreadyFetchedTerminalEntity && !value)
				{
					this.TerminalEntity = null;
				}
				_alreadyFetchedTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalEntity is not found
		/// in the database. When set to true, TerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalEntityReturnsNewIfNotFound
		{
			get	{ return _terminalEntityReturnsNewIfNotFound; }
			set { _terminalEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIModeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIModeEntity UIModeEntity
		{
			get	{ return GetSingleUIModeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIModeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TimestampCollection", "UIModeEntity", _uIModeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeEntity. When set to true, UIModeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIModeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeEntity
		{
			get	{ return _alwaysFetchUIModeEntity; }
			set	{ _alwaysFetchUIModeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeEntity already has been fetched. Setting this property to false when UIModeEntity has been fetched
		/// will set UIModeEntity to null as well. Setting this property to true while UIModeEntity hasn't been fetched disables lazy loading for UIModeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeEntity
		{
			get { return _alreadyFetchedUIModeEntity;}
			set 
			{
				if(_alreadyFetchedUIModeEntity && !value)
				{
					this.UIModeEntity = null;
				}
				_alreadyFetchedUIModeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIModeEntity is not found
		/// in the database. When set to true, UIModeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIModeEntityReturnsNewIfNotFound
		{
			get	{ return _uIModeEntityReturnsNewIfNotFound; }
			set { _uIModeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIScheduleEntity UIScheduleEntity
		{
			get	{ return GetSingleUIScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TimestampCollection", "UIScheduleEntity", _uIScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleEntity. When set to true, UIScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleEntity is accessed. You can always execute a forced fetch by calling GetSingleUIScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleEntity
		{
			get	{ return _alwaysFetchUIScheduleEntity; }
			set	{ _alwaysFetchUIScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleEntity already has been fetched. Setting this property to false when UIScheduleEntity has been fetched
		/// will set UIScheduleEntity to null as well. Setting this property to true while UIScheduleEntity hasn't been fetched disables lazy loading for UIScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleEntity
		{
			get { return _alreadyFetchedUIScheduleEntity;}
			set 
			{
				if(_alreadyFetchedUIScheduleEntity && !value)
				{
					this.UIScheduleEntity = null;
				}
				_alreadyFetchedUIScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIScheduleEntity is not found
		/// in the database. When set to true, UIScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _uIScheduleEntityReturnsNewIfNotFound; }
			set { _uIScheduleEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIThemeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIThemeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIThemeEntity UIThemeEntity
		{
			get	{ return GetSingleUIThemeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIThemeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TimestampCollection", "UIThemeEntity", _uIThemeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIThemeEntity. When set to true, UIThemeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIThemeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIThemeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIThemeEntity
		{
			get	{ return _alwaysFetchUIThemeEntity; }
			set	{ _alwaysFetchUIThemeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIThemeEntity already has been fetched. Setting this property to false when UIThemeEntity has been fetched
		/// will set UIThemeEntity to null as well. Setting this property to true while UIThemeEntity hasn't been fetched disables lazy loading for UIThemeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIThemeEntity
		{
			get { return _alreadyFetchedUIThemeEntity;}
			set 
			{
				if(_alreadyFetchedUIThemeEntity && !value)
				{
					this.UIThemeEntity = null;
				}
				_alreadyFetchedUIThemeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIThemeEntity is not found
		/// in the database. When set to true, UIThemeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIThemeEntityReturnsNewIfNotFound
		{
			get	{ return _uIThemeEntityReturnsNewIfNotFound; }
			set { _uIThemeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AdvertisementConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementConfigurationEntity AdvertisementConfigurationEntity
		{
			get	{ return GetSingleAdvertisementConfigurationEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementConfigurationEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_advertisementConfigurationEntity !=null);
						DesetupSyncAdvertisementConfigurationEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("AdvertisementConfigurationEntity");
						}
					}
					else
					{
						if(_advertisementConfigurationEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncAdvertisementConfigurationEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementConfigurationEntity. When set to true, AdvertisementConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementConfigurationEntity
		{
			get	{ return _alwaysFetchAdvertisementConfigurationEntity; }
			set	{ _alwaysFetchAdvertisementConfigurationEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementConfigurationEntity already has been fetched. Setting this property to false when AdvertisementConfigurationEntity has been fetched
		/// will set AdvertisementConfigurationEntity to null as well. Setting this property to true while AdvertisementConfigurationEntity hasn't been fetched disables lazy loading for AdvertisementConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementConfigurationEntity
		{
			get { return _alreadyFetchedAdvertisementConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementConfigurationEntity && !value)
				{
					this.AdvertisementConfigurationEntity = null;
				}
				_alreadyFetchedAdvertisementConfigurationEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementConfigurationEntity is not found
		/// in the database. When set to true, AdvertisementConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementConfigurationEntityReturnsNewIfNotFound; }
			set	{ _advertisementConfigurationEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ClientEntity
		{
			get	{ return GetSingleClientEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncClientEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_clientEntity !=null);
						DesetupSyncClientEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ClientEntity");
						}
					}
					else
					{
						if(_clientEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncClientEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntity. When set to true, ClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntity is accessed. You can always execute a forced fetch by calling GetSingleClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntity
		{
			get	{ return _alwaysFetchClientEntity; }
			set	{ _alwaysFetchClientEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntity already has been fetched. Setting this property to false when ClientEntity has been fetched
		/// will set ClientEntity to null as well. Setting this property to true while ClientEntity hasn't been fetched disables lazy loading for ClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntity
		{
			get { return _alreadyFetchedClientEntity;}
			set 
			{
				if(_alreadyFetchedClientEntity && !value)
				{
					this.ClientEntity = null;
				}
				_alreadyFetchedClientEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientEntity is not found
		/// in the database. When set to true, ClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientEntityReturnsNewIfNotFound
		{
			get	{ return _clientEntityReturnsNewIfNotFound; }
			set	{ _clientEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_companyEntity !=null);
						DesetupSyncCompanyEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("CompanyEntity");
						}
					}
					else
					{
						if(_companyEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncCompanyEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set	{ _companyEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity DeliverypointEntity
		{
			get	{ return GetSingleDeliverypointEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_deliverypointEntity !=null);
						DesetupSyncDeliverypointEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("DeliverypointEntity");
						}
					}
					else
					{
						if(_deliverypointEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncDeliverypointEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointEntity. When set to true, DeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointEntity
		{
			get	{ return _alwaysFetchDeliverypointEntity; }
			set	{ _alwaysFetchDeliverypointEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointEntity already has been fetched. Setting this property to false when DeliverypointEntity has been fetched
		/// will set DeliverypointEntity to null as well. Setting this property to true while DeliverypointEntity hasn't been fetched disables lazy loading for DeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointEntity
		{
			get { return _alreadyFetchedDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointEntity && !value)
				{
					this.DeliverypointEntity = null;
				}
				_alreadyFetchedDeliverypointEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointEntity is not found
		/// in the database. When set to true, DeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointEntityReturnsNewIfNotFound; }
			set	{ _deliverypointEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_deliverypointgroupEntity !=null);
						DesetupSyncDeliverypointgroupEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("DeliverypointgroupEntity");
						}
					}
					else
					{
						if(_deliverypointgroupEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncDeliverypointgroupEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set	{ _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'EntertainmentConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentConfigurationEntity EntertainmentConfigurationEntity
		{
			get	{ return GetSingleEntertainmentConfigurationEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentConfigurationEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_entertainmentConfigurationEntity !=null);
						DesetupSyncEntertainmentConfigurationEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("EntertainmentConfigurationEntity");
						}
					}
					else
					{
						if(_entertainmentConfigurationEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncEntertainmentConfigurationEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentConfigurationEntity. When set to true, EntertainmentConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentConfigurationEntity
		{
			get	{ return _alwaysFetchEntertainmentConfigurationEntity; }
			set	{ _alwaysFetchEntertainmentConfigurationEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentConfigurationEntity already has been fetched. Setting this property to false when EntertainmentConfigurationEntity has been fetched
		/// will set EntertainmentConfigurationEntity to null as well. Setting this property to true while EntertainmentConfigurationEntity hasn't been fetched disables lazy loading for EntertainmentConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentConfigurationEntity
		{
			get { return _alreadyFetchedEntertainmentConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentConfigurationEntity && !value)
				{
					this.EntertainmentConfigurationEntity = null;
				}
				_alreadyFetchedEntertainmentConfigurationEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentConfigurationEntity is not found
		/// in the database. When set to true, EntertainmentConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentConfigurationEntityReturnsNewIfNotFound; }
			set	{ _entertainmentConfigurationEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'InfraredConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInfraredConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual InfraredConfigurationEntity InfraredConfigurationEntity
		{
			get	{ return GetSingleInfraredConfigurationEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncInfraredConfigurationEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_infraredConfigurationEntity !=null);
						DesetupSyncInfraredConfigurationEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("InfraredConfigurationEntity");
						}
					}
					else
					{
						if(_infraredConfigurationEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncInfraredConfigurationEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InfraredConfigurationEntity. When set to true, InfraredConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InfraredConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleInfraredConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInfraredConfigurationEntity
		{
			get	{ return _alwaysFetchInfraredConfigurationEntity; }
			set	{ _alwaysFetchInfraredConfigurationEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property InfraredConfigurationEntity already has been fetched. Setting this property to false when InfraredConfigurationEntity has been fetched
		/// will set InfraredConfigurationEntity to null as well. Setting this property to true while InfraredConfigurationEntity hasn't been fetched disables lazy loading for InfraredConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInfraredConfigurationEntity
		{
			get { return _alreadyFetchedInfraredConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedInfraredConfigurationEntity && !value)
				{
					this.InfraredConfigurationEntity = null;
				}
				_alreadyFetchedInfraredConfigurationEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InfraredConfigurationEntity is not found
		/// in the database. When set to true, InfraredConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool InfraredConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _infraredConfigurationEntityReturnsNewIfNotFound; }
			set	{ _infraredConfigurationEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'MapEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMapEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MapEntity MapEntity
		{
			get	{ return GetSingleMapEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncMapEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_mapEntity !=null);
						DesetupSyncMapEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("MapEntity");
						}
					}
					else
					{
						if(_mapEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncMapEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MapEntity. When set to true, MapEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MapEntity is accessed. You can always execute a forced fetch by calling GetSingleMapEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMapEntity
		{
			get	{ return _alwaysFetchMapEntity; }
			set	{ _alwaysFetchMapEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property MapEntity already has been fetched. Setting this property to false when MapEntity has been fetched
		/// will set MapEntity to null as well. Setting this property to true while MapEntity hasn't been fetched disables lazy loading for MapEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMapEntity
		{
			get { return _alreadyFetchedMapEntity;}
			set 
			{
				if(_alreadyFetchedMapEntity && !value)
				{
					this.MapEntity = null;
				}
				_alreadyFetchedMapEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MapEntity is not found
		/// in the database. When set to true, MapEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MapEntityReturnsNewIfNotFound
		{
			get	{ return _mapEntityReturnsNewIfNotFound; }
			set	{ _mapEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'MenuEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMenuEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MenuEntity MenuEntity
		{
			get	{ return GetSingleMenuEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncMenuEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_menuEntity !=null);
						DesetupSyncMenuEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("MenuEntity");
						}
					}
					else
					{
						if(_menuEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncMenuEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MenuEntity. When set to true, MenuEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuEntity is accessed. You can always execute a forced fetch by calling GetSingleMenuEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuEntity
		{
			get	{ return _alwaysFetchMenuEntity; }
			set	{ _alwaysFetchMenuEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property MenuEntity already has been fetched. Setting this property to false when MenuEntity has been fetched
		/// will set MenuEntity to null as well. Setting this property to true while MenuEntity hasn't been fetched disables lazy loading for MenuEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuEntity
		{
			get { return _alreadyFetchedMenuEntity;}
			set 
			{
				if(_alreadyFetchedMenuEntity && !value)
				{
					this.MenuEntity = null;
				}
				_alreadyFetchedMenuEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MenuEntity is not found
		/// in the database. When set to true, MenuEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MenuEntityReturnsNewIfNotFound
		{
			get	{ return _menuEntityReturnsNewIfNotFound; }
			set	{ _menuEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PointOfInterestEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePointOfInterestEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PointOfInterestEntity PointOfInterestEntity
		{
			get	{ return GetSinglePointOfInterestEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPointOfInterestEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_pointOfInterestEntity !=null);
						DesetupSyncPointOfInterestEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PointOfInterestEntity");
						}
					}
					else
					{
						if(_pointOfInterestEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncPointOfInterestEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestEntity. When set to true, PointOfInterestEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestEntity is accessed. You can always execute a forced fetch by calling GetSinglePointOfInterestEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestEntity
		{
			get	{ return _alwaysFetchPointOfInterestEntity; }
			set	{ _alwaysFetchPointOfInterestEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestEntity already has been fetched. Setting this property to false when PointOfInterestEntity has been fetched
		/// will set PointOfInterestEntity to null as well. Setting this property to true while PointOfInterestEntity hasn't been fetched disables lazy loading for PointOfInterestEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestEntity
		{
			get { return _alreadyFetchedPointOfInterestEntity;}
			set 
			{
				if(_alreadyFetchedPointOfInterestEntity && !value)
				{
					this.PointOfInterestEntity = null;
				}
				_alreadyFetchedPointOfInterestEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PointOfInterestEntity is not found
		/// in the database. When set to true, PointOfInterestEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PointOfInterestEntityReturnsNewIfNotFound
		{
			get	{ return _pointOfInterestEntityReturnsNewIfNotFound; }
			set	{ _pointOfInterestEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PriceScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceScheduleEntity PriceScheduleEntity
		{
			get	{ return GetSinglePriceScheduleEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPriceScheduleEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_priceScheduleEntity !=null);
						DesetupSyncPriceScheduleEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PriceScheduleEntity");
						}
					}
					else
					{
						if(_priceScheduleEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncPriceScheduleEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceScheduleEntity. When set to true, PriceScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceScheduleEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceScheduleEntity
		{
			get	{ return _alwaysFetchPriceScheduleEntity; }
			set	{ _alwaysFetchPriceScheduleEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PriceScheduleEntity already has been fetched. Setting this property to false when PriceScheduleEntity has been fetched
		/// will set PriceScheduleEntity to null as well. Setting this property to true while PriceScheduleEntity hasn't been fetched disables lazy loading for PriceScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceScheduleEntity
		{
			get { return _alreadyFetchedPriceScheduleEntity;}
			set 
			{
				if(_alreadyFetchedPriceScheduleEntity && !value)
				{
					this.PriceScheduleEntity = null;
				}
				_alreadyFetchedPriceScheduleEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceScheduleEntity is not found
		/// in the database. When set to true, PriceScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _priceScheduleEntityReturnsNewIfNotFound; }
			set	{ _priceScheduleEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'RoomControlConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlConfigurationEntity RoomControlConfigurationEntity
		{
			get	{ return GetSingleRoomControlConfigurationEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlConfigurationEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_roomControlConfigurationEntity !=null);
						DesetupSyncRoomControlConfigurationEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("RoomControlConfigurationEntity");
						}
					}
					else
					{
						if(_roomControlConfigurationEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncRoomControlConfigurationEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlConfigurationEntity. When set to true, RoomControlConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlConfigurationEntity
		{
			get	{ return _alwaysFetchRoomControlConfigurationEntity; }
			set	{ _alwaysFetchRoomControlConfigurationEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlConfigurationEntity already has been fetched. Setting this property to false when RoomControlConfigurationEntity has been fetched
		/// will set RoomControlConfigurationEntity to null as well. Setting this property to true while RoomControlConfigurationEntity hasn't been fetched disables lazy loading for RoomControlConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlConfigurationEntity
		{
			get { return _alreadyFetchedRoomControlConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlConfigurationEntity && !value)
				{
					this.RoomControlConfigurationEntity = null;
				}
				_alreadyFetchedRoomControlConfigurationEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlConfigurationEntity is not found
		/// in the database. When set to true, RoomControlConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlConfigurationEntityReturnsNewIfNotFound; }
			set	{ _roomControlConfigurationEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_siteEntity !=null);
						DesetupSyncSiteEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("SiteEntity");
						}
					}
					else
					{
						if(_siteEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncSiteEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set	{ _siteEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'TerminalConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalConfigurationEntity TerminalConfigurationEntity
		{
			get	{ return GetSingleTerminalConfigurationEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTerminalConfigurationEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_terminalConfigurationEntity !=null);
						DesetupSyncTerminalConfigurationEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TerminalConfigurationEntity");
						}
					}
					else
					{
						if(_terminalConfigurationEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TimestampCollection");
							SetupSyncTerminalConfigurationEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalConfigurationEntity. When set to true, TerminalConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalConfigurationEntity
		{
			get	{ return _alwaysFetchTerminalConfigurationEntity; }
			set	{ _alwaysFetchTerminalConfigurationEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalConfigurationEntity already has been fetched. Setting this property to false when TerminalConfigurationEntity has been fetched
		/// will set TerminalConfigurationEntity to null as well. Setting this property to true while TerminalConfigurationEntity hasn't been fetched disables lazy loading for TerminalConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalConfigurationEntity
		{
			get { return _alreadyFetchedTerminalConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedTerminalConfigurationEntity && !value)
				{
					this.TerminalConfigurationEntity = null;
				}
				_alreadyFetchedTerminalConfigurationEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalConfigurationEntity is not found
		/// in the database. When set to true, TerminalConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _terminalConfigurationEntityReturnsNewIfNotFound; }
			set	{ _terminalConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.TimestampEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
