﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Entity base class which represents the base class for the entity 'DeliverypointgroupPosdeliverypointgroupId'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeliverypointgroupPosdeliverypointgroupIdEntityBase : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations


		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private PosdeliverypointgroupEntity _posdeliverypointgroupEntity;
		private bool	_alwaysFetchPosdeliverypointgroupEntity, _alreadyFetchedPosdeliverypointgroupEntity, _posdeliverypointgroupEntityReturnsNewIfNotFound;

		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
		
		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name PosdeliverypointgroupEntity</summary>
			public static readonly string PosdeliverypointgroupEntity = "PosdeliverypointgroupEntity";



		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeliverypointgroupPosdeliverypointgroupIdEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeliverypointgroupPosdeliverypointgroupIdEntityBase()
		{
			InitClassEmpty(null);
		}

	
		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		public DeliverypointgroupPosdeliverypointgroupIdEntityBase(System.Int32 deliverypointgroupPosdeliverypointgroupId)
		{
			InitClassFetch(deliverypointgroupPosdeliverypointgroupId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeliverypointgroupPosdeliverypointgroupIdEntityBase(System.Int32 deliverypointgroupPosdeliverypointgroupId, IPrefetchPath prefetchPathToUse)
		{
			InitClassFetch(deliverypointgroupPosdeliverypointgroupId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <param name="validator">The custom validator object for this DeliverypointgroupPosdeliverypointgroupIdEntity</param>
		public DeliverypointgroupPosdeliverypointgroupIdEntityBase(System.Int32 deliverypointgroupPosdeliverypointgroupId, IValidator validator)
		{
			InitClassFetch(deliverypointgroupPosdeliverypointgroupId, validator, null);
		}
	

		/// <summary>Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointgroupPosdeliverypointgroupIdEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{


			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");
			_posdeliverypointgroupEntity = (PosdeliverypointgroupEntity)info.GetValue("_posdeliverypointgroupEntity", typeof(PosdeliverypointgroupEntity));
			if(_posdeliverypointgroupEntity!=null)
			{
				_posdeliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posdeliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_posdeliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchPosdeliverypointgroupEntity = info.GetBoolean("_alwaysFetchPosdeliverypointgroupEntity");
			_alreadyFetchedPosdeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupEntity");

			base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeliverypointgroupPosdeliverypointgroupIdFieldIndex)fieldIndex)
			{
				case DeliverypointgroupPosdeliverypointgroupIdFieldIndex.DeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				case DeliverypointgroupPosdeliverypointgroupIdFieldIndex.PosdeliverypointgroupId:
					DesetupSyncPosdeliverypointgroupEntity(true, false);
					_alreadyFetchedPosdeliverypointgroupEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
		
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PostReadXmlFixups()
		{


			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedPosdeliverypointgroupEntity = (_posdeliverypointgroupEntity != null);

		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return DeliverypointgroupPosdeliverypointgroupIdEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeliverypointgroupEntity":
					toReturn.Add(DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "PosdeliverypointgroupEntity":
					toReturn.Add(DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId);
					break;



				default:

					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.
		/// Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{


			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_posdeliverypointgroupEntity", (!this.MarkedForDeletion?_posdeliverypointgroupEntity:null));
			info.AddValue("_posdeliverypointgroupEntityReturnsNewIfNotFound", _posdeliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosdeliverypointgroupEntity", _alwaysFetchPosdeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupEntity", _alreadyFetchedPosdeliverypointgroupEntity);

			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity entity)
		{
			switch(propertyName)
			{
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "PosdeliverypointgroupEntity":
					_alreadyFetchedPosdeliverypointgroupEntity = true;
					this.PosdeliverypointgroupEntity = (PosdeliverypointgroupEntity)entity;
					break;



				default:

					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "PosdeliverypointgroupEntity":
					SetupSyncPosdeliverypointgroupEntity(relatedEntity);
					break;


				default:

					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "PosdeliverypointgroupEntity":
					DesetupSyncPosdeliverypointgroupEntity(false, true);
					break;


				default:

					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These
		/// entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		public override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();


			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		public override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_posdeliverypointgroupEntity!=null)
			{
				toReturn.Add(_posdeliverypointgroupEntity);
			}


			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. The contents of the ArrayList is
		/// used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		public override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		

		

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupPosdeliverypointgroupId)
		{
			return FetchUsingPK(deliverypointgroupPosdeliverypointgroupId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupPosdeliverypointgroupId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deliverypointgroupPosdeliverypointgroupId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupPosdeliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return Fetch(deliverypointgroupPosdeliverypointgroupId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupPosdeliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deliverypointgroupPosdeliverypointgroupId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. 
		/// Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeliverypointgroupPosdeliverypointgroupId, null, null, null);
		}

		/// <summary> Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(DeliverypointgroupPosdeliverypointgroupIdFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(DeliverypointgroupPosdeliverypointgroupIdFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new DeliverypointgroupPosdeliverypointgroupIdRelations().GetAllRelations();
		}




		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !base.IsSerializing && !base.IsDeserializing  && !base.InDesignMode)			
			{
				bool performLazyLoading = base.CheckIfLazyLoadingShouldOccur(DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				if(base.ParticipatesInTransaction)
				{
					base.Transaction.Add(newEntity);
				}
				bool fetchResult = false;
				if(performLazyLoading)
				{
					fetchResult = newEntity.FetchUsingPK(this.DeliverypointgroupId);
				}
				if(fetchResult)
				{
					if(base.ActiveContext!=null)
					{
						newEntity = (DeliverypointgroupEntity)base.ActiveContext.Get(newEntity);
					}
					this.DeliverypointgroupEntity = newEntity;
				}
				else
				{
					if(_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						if(performLazyLoading || (!performLazyLoading && (_deliverypointgroupEntity == null)))
						{
							this.DeliverypointgroupEntity = newEntity;
						}
					}
					else
					{
						this.DeliverypointgroupEntity = null;
					}
				}
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
				if(base.ParticipatesInTransaction && !fetchResult)
				{
					base.Transaction.Remove(newEntity);
				}
			}
			return _deliverypointgroupEntity;
		}

		/// <summary> Retrieves the related entity of type 'PosdeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosdeliverypointgroupEntity' which is related to this entity.</returns>
		public PosdeliverypointgroupEntity GetSinglePosdeliverypointgroupEntity()
		{
			return GetSinglePosdeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosdeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosdeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual PosdeliverypointgroupEntity GetSinglePosdeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosdeliverypointgroupEntity || forceFetch || _alwaysFetchPosdeliverypointgroupEntity) && !base.IsSerializing && !base.IsDeserializing  && !base.InDesignMode)			
			{
				bool performLazyLoading = base.CheckIfLazyLoadingShouldOccur(DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId);

				PosdeliverypointgroupEntity newEntity = new PosdeliverypointgroupEntity();
				if(base.ParticipatesInTransaction)
				{
					base.Transaction.Add(newEntity);
				}
				bool fetchResult = false;
				if(performLazyLoading)
				{
					fetchResult = newEntity.FetchUsingPK(this.PosdeliverypointgroupId);
				}
				if(fetchResult)
				{
					if(base.ActiveContext!=null)
					{
						newEntity = (PosdeliverypointgroupEntity)base.ActiveContext.Get(newEntity);
					}
					this.PosdeliverypointgroupEntity = newEntity;
				}
				else
				{
					if(_posdeliverypointgroupEntityReturnsNewIfNotFound)
					{
						if(performLazyLoading || (!performLazyLoading && (_posdeliverypointgroupEntity == null)))
						{
							this.PosdeliverypointgroupEntity = newEntity;
						}
					}
					else
					{
						this.PosdeliverypointgroupEntity = null;
					}
				}
				_alreadyFetchedPosdeliverypointgroupEntity = fetchResult;
				if(base.ParticipatesInTransaction && !fetchResult)
				{
					base.Transaction.Remove(newEntity);
				}
			}
			return _posdeliverypointgroupEntity;
		}


		/// <summary> Performs the insert action of a new Entity to the persistent storage.</summary>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool InsertEntity()
		{
			DeliverypointgroupPosdeliverypointgroupIdDAO dao = (DeliverypointgroupPosdeliverypointgroupIdDAO)CreateDAOInstance();
			return dao.AddNew(base.Fields, base.Transaction);
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{


			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.ActiveContext = base.ActiveContext;
			}
			if(_posdeliverypointgroupEntity!=null)
			{
				_posdeliverypointgroupEntity.ActiveContext = base.ActiveContext;
			}


		}


		/// <summary> Performs the update action of an existing Entity to the persistent storage.</summary>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool UpdateEntity()
		{
			DeliverypointgroupPosdeliverypointgroupIdDAO dao = (DeliverypointgroupPosdeliverypointgroupIdDAO)CreateDAOInstance();
			return dao.UpdateExisting(base.Fields, base.Transaction);
		}
		
		/// <summary> Performs the update action of an existing Entity to the persistent storage.</summary>
		/// <param name="updateRestriction">Predicate expression, meant for concurrency checks in an Update query</param>
		/// <returns>true if succeeded, false otherwise</returns>
		protected override bool UpdateEntity(IPredicate updateRestriction)
		{
			DeliverypointgroupPosdeliverypointgroupIdDAO dao = (DeliverypointgroupPosdeliverypointgroupIdDAO)CreateDAOInstance();
			return dao.UpdateExisting(base.Fields, base.Transaction, updateRestriction);
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		protected virtual void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			base.Fields = CreateFields();
			base.IsNew=true;
			base.Validator = validatorToUse;

			InitClassMembers();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();
		}
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Obymobi.Data.EntityType.DeliverypointgroupPosdeliverypointgroupIdEntity);
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("PosdeliverypointgroupEntity", _posdeliverypointgroupEntity);



			return toReturn;
		}
		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <param name="validator">The validator object for this DeliverypointgroupPosdeliverypointgroupIdEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected virtual void InitClassFetch(System.Int32 deliverypointgroupPosdeliverypointgroupId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			base.Validator = validator;
			InitClassMembers();
			base.Fields = CreateFields();
			bool wasSuccesful = Fetch(deliverypointgroupPosdeliverypointgroupId, prefetchPathToUse, null, null);
			base.IsNew = !wasSuccesful;

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{


			_deliverypointgroupEntity = null;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_alwaysFetchDeliverypointgroupEntity = false;
			_alreadyFetchedDeliverypointgroupEntity = false;
			_posdeliverypointgroupEntity = null;
			_posdeliverypointgroupEntityReturnsNewIfNotFound = true;
			_alwaysFetchPosdeliverypointgroupEntity = false;
			_alreadyFetchedPosdeliverypointgroupEntity = false;


			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("DeliverypointgroupPosdeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("PosdeliverypointgroupId", fieldHashtable);
		}
		#endregion


		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, true, signalRelatedEntity, "DeliverypointgroupPosdeliverypointgroupIdCollection", resetFKFields, new int[] { (int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.DeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntity relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posdeliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosdeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _posdeliverypointgroupEntity, new PropertyChangedEventHandler( OnPosdeliverypointgroupEntityPropertyChanged ), "PosdeliverypointgroupEntity", DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, true, signalRelatedEntity, "DeliverypointgroupPosdeliverypointgroupIdCollection", resetFKFields, new int[] { (int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.PosdeliverypointgroupId } );		
			_posdeliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posdeliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosdeliverypointgroupEntity(IEntity relatedEntity)
		{
			if(_posdeliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncPosdeliverypointgroupEntity(true, true);
				_posdeliverypointgroupEntity = (PosdeliverypointgroupEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _posdeliverypointgroupEntity, new PropertyChangedEventHandler( OnPosdeliverypointgroupEntityPropertyChanged ), "PosdeliverypointgroupEntity", DeliverypointgroupPosdeliverypointgroupIdEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, true, ref _alreadyFetchedPosdeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosdeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}


		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deliverypointgroupPosdeliverypointgroupId">PK value for DeliverypointgroupPosdeliverypointgroupId which data should be fetched into this DeliverypointgroupPosdeliverypointgroupId object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deliverypointgroupPosdeliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				IDao dao = this.CreateDAOInstance();
				base.Fields[(int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.DeliverypointgroupPosdeliverypointgroupId].ForcedCurrentValueWrite(deliverypointgroupPosdeliverypointgroupId);
				dao.FetchExisting(this, base.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (base.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliverypointgroupPosdeliverypointgroupIdDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeliverypointgroupPosdeliverypointgroupIdEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeliverypointgroupPosdeliverypointgroupIdRelations Relations
		{
			get	{ return new DeliverypointgroupPosdeliverypointgroupIdRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}




		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get
			{
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(),
					(IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupPosdeliverypointgroupIdEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupEntity
		{
			get
			{
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(),
					(IEntityRelation)GetRelationsForField("PosdeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupPosdeliverypointgroupIdEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, null, "PosdeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}


		/// <summary>Returns the full name for this entity, which is important for the DAO to find back persistence info for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override string LLBLGenProEntityName
		{
			get { return "DeliverypointgroupPosdeliverypointgroupIdEntity";}
		}

		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return DeliverypointgroupPosdeliverypointgroupIdEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return DeliverypointgroupPosdeliverypointgroupIdEntity.FieldsCustomProperties;}
		}

		/// <summary> The DeliverypointgroupPosdeliverypointgroupId property of the Entity DeliverypointgroupPosdeliverypointgroupId<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupPosdeliverypointgroupId"."DeliverypointgroupPosdeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointgroupPosdeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.DeliverypointgroupPosdeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.DeliverypointgroupPosdeliverypointgroupId, value, true); }
		}
		/// <summary> The DeliverypointgroupId property of the Entity DeliverypointgroupPosdeliverypointgroupId<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupPosdeliverypointgroupId"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.DeliverypointgroupId, value, true); }
		}
		/// <summary> The PosdeliverypointgroupId property of the Entity DeliverypointgroupPosdeliverypointgroupId<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "DeliverypointgroupPosdeliverypointgroupId"."PosdeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PosdeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.PosdeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointgroupPosdeliverypointgroupIdFieldIndex.PosdeliverypointgroupId, value, true); }
		}



		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.</summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					if(value==null)
					{
						if(_deliverypointgroupEntity != null)
						{
							_deliverypointgroupEntity.UnsetRelatedEntity(this, "DeliverypointgroupPosdeliverypointgroupIdCollection");
						}
					}
					else
					{
						if(_deliverypointgroupEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "DeliverypointgroupPosdeliverypointgroupIdCollection");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute
		/// a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'PosdeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.</summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosdeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PosdeliverypointgroupEntity PosdeliverypointgroupEntity
		{
			get	{ return GetSinglePosdeliverypointgroupEntity(false); }
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncPosdeliverypointgroupEntity(value);
				}
				else
				{
					if(value==null)
					{
						if(_posdeliverypointgroupEntity != null)
						{
							_posdeliverypointgroupEntity.UnsetRelatedEntity(this, "DeliverypointgroupPosdeliverypointgroupIdCollection");
						}
					}
					else
					{
						if(_posdeliverypointgroupEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "DeliverypointgroupPosdeliverypointgroupIdCollection");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupEntity. When set to true, PosdeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupEntity is accessed. You can always execute
		/// a forced fetch by calling GetSinglePosdeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupEntity
		{
			get	{ return _alwaysFetchPosdeliverypointgroupEntity; }
			set	{ _alwaysFetchPosdeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupEntity already has been fetched. Setting this property to false when PosdeliverypointgroupEntity has been fetched
		/// will set PosdeliverypointgroupEntity to null as well. Setting this property to true while PosdeliverypointgroupEntity hasn't been fetched disables lazy loading for PosdeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupEntity
		{
			get { return _alreadyFetchedPosdeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupEntity && !value)
				{
					this.PosdeliverypointgroupEntity = null;
				}
				_alreadyFetchedPosdeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosdeliverypointgroupEntity is not found
		/// in the database. When set to true, PosdeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosdeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _posdeliverypointgroupEntityReturnsNewIfNotFound; }
			set { _posdeliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeliverypointgroupPosdeliverypointgroupIdEntity; }
		}
		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}
