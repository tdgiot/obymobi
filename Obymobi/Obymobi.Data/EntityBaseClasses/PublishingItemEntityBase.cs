﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PublishingItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PublishingItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PublishingItemEntity"; }
		}
	
		#region Class Member Declarations
		private PublishingEntity _publishingEntity;
		private bool	_alwaysFetchPublishingEntity, _alreadyFetchedPublishingEntity, _publishingEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PublishingEntity</summary>
			public static readonly string PublishingEntity = "PublishingEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PublishingItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PublishingItemEntityBase() :base("PublishingItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		protected PublishingItemEntityBase(System.Int32 publishingItemId):base("PublishingItemEntity")
		{
			InitClassFetch(publishingItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PublishingItemEntityBase(System.Int32 publishingItemId, IPrefetchPath prefetchPathToUse): base("PublishingItemEntity")
		{
			InitClassFetch(publishingItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <param name="validator">The custom validator object for this PublishingItemEntity</param>
		protected PublishingItemEntityBase(System.Int32 publishingItemId, IValidator validator):base("PublishingItemEntity")
		{
			InitClassFetch(publishingItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PublishingItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_publishingEntity = (PublishingEntity)info.GetValue("_publishingEntity", typeof(PublishingEntity));
			if(_publishingEntity!=null)
			{
				_publishingEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_publishingEntityReturnsNewIfNotFound = info.GetBoolean("_publishingEntityReturnsNewIfNotFound");
			_alwaysFetchPublishingEntity = info.GetBoolean("_alwaysFetchPublishingEntity");
			_alreadyFetchedPublishingEntity = info.GetBoolean("_alreadyFetchedPublishingEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PublishingItemFieldIndex)fieldIndex)
			{
				case PublishingItemFieldIndex.PublishingId:
					DesetupSyncPublishingEntity(true, false);
					_alreadyFetchedPublishingEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPublishingEntity = (_publishingEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PublishingEntity":
					toReturn.Add(Relations.PublishingEntityUsingPublishingId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_publishingEntity", (!this.MarkedForDeletion?_publishingEntity:null));
			info.AddValue("_publishingEntityReturnsNewIfNotFound", _publishingEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPublishingEntity", _alwaysFetchPublishingEntity);
			info.AddValue("_alreadyFetchedPublishingEntity", _alreadyFetchedPublishingEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PublishingEntity":
					_alreadyFetchedPublishingEntity = true;
					this.PublishingEntity = (PublishingEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PublishingEntity":
					SetupSyncPublishingEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PublishingEntity":
					DesetupSyncPublishingEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_publishingEntity!=null)
			{
				toReturn.Add(_publishingEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 publishingItemId)
		{
			return FetchUsingPK(publishingItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 publishingItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(publishingItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 publishingItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(publishingItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 publishingItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(publishingItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PublishingItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PublishingItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PublishingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PublishingEntity' which is related to this entity.</returns>
		public PublishingEntity GetSinglePublishingEntity()
		{
			return GetSinglePublishingEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PublishingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PublishingEntity' which is related to this entity.</returns>
		public virtual PublishingEntity GetSinglePublishingEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPublishingEntity || forceFetch || _alwaysFetchPublishingEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PublishingEntityUsingPublishingId);
				PublishingEntity newEntity = new PublishingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PublishingId);
				}
				if(fetchResult)
				{
					newEntity = (PublishingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_publishingEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PublishingEntity = newEntity;
				_alreadyFetchedPublishingEntity = fetchResult;
			}
			return _publishingEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PublishingEntity", _publishingEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <param name="validator">The validator object for this PublishingItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 publishingItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(publishingItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_publishingEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishingItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishingId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApiOperation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Log", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter1Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter1Value", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter2Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Parameter2Value", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublishedTicks", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _publishingEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPublishingEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _publishingEntity, new PropertyChangedEventHandler( OnPublishingEntityPropertyChanged ), "PublishingEntity", Obymobi.Data.RelationClasses.StaticPublishingItemRelations.PublishingEntityUsingPublishingIdStatic, true, signalRelatedEntity, "PublishingItemCollection", resetFKFields, new int[] { (int)PublishingItemFieldIndex.PublishingId } );		
			_publishingEntity = null;
		}
		
		/// <summary> setups the sync logic for member _publishingEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPublishingEntity(IEntityCore relatedEntity)
		{
			if(_publishingEntity!=relatedEntity)
			{		
				DesetupSyncPublishingEntity(true, true);
				_publishingEntity = (PublishingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _publishingEntity, new PropertyChangedEventHandler( OnPublishingEntityPropertyChanged ), "PublishingEntity", Obymobi.Data.RelationClasses.StaticPublishingItemRelations.PublishingEntityUsingPublishingIdStatic, true, ref _alreadyFetchedPublishingEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPublishingEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="publishingItemId">PK value for PublishingItem which data should be fetched into this PublishingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 publishingItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PublishingItemFieldIndex.PublishingItemId].ForcedCurrentValueWrite(publishingItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePublishingItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PublishingItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PublishingItemRelations Relations
		{
			get	{ return new PublishingItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Publishing'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPublishingEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PublishingCollection(), (IEntityRelation)GetRelationsForField("PublishingEntity")[0], (int)Obymobi.Data.EntityType.PublishingItemEntity, (int)Obymobi.Data.EntityType.PublishingEntity, 0, null, null, null, "PublishingEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PublishingItemId property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."PublishingItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PublishingItemId
		{
			get { return (System.Int32)GetValue((int)PublishingItemFieldIndex.PublishingItemId, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.PublishingItemId, value, true); }
		}

		/// <summary> The PublishingId property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."PublishingId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PublishingId
		{
			get { return (System.Int32)GetValue((int)PublishingItemFieldIndex.PublishingId, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.PublishingId, value, true); }
		}

		/// <summary> The ApiOperation property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."ApiOperation"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ApiOperation
		{
			get { return (System.String)GetValue((int)PublishingItemFieldIndex.ApiOperation, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.ApiOperation, value, true); }
		}

		/// <summary> The Status property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."Status"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PublishingItemStatus Status
		{
			get { return (Obymobi.Enums.PublishingItemStatus)GetValue((int)PublishingItemFieldIndex.Status, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.Status, value, true); }
		}

		/// <summary> The StatusText property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."StatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)PublishingItemFieldIndex.StatusText, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.StatusText, value, true); }
		}

		/// <summary> The Log property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."Log"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Log
		{
			get { return (System.String)GetValue((int)PublishingItemFieldIndex.Log, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.Log, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PublishingItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PublishingItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PublishingItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)PublishingItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PublishingItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PublishingItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)PublishingItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)PublishingItemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Parameter1Name property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."Parameter1Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter1Name
		{
			get { return (System.String)GetValue((int)PublishingItemFieldIndex.Parameter1Name, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.Parameter1Name, value, true); }
		}

		/// <summary> The Parameter1Value property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."Parameter1Value"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter1Value
		{
			get { return (System.String)GetValue((int)PublishingItemFieldIndex.Parameter1Value, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.Parameter1Value, value, true); }
		}

		/// <summary> The Parameter2Name property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."Parameter2Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter2Name
		{
			get { return (System.String)GetValue((int)PublishingItemFieldIndex.Parameter2Name, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.Parameter2Name, value, true); }
		}

		/// <summary> The Parameter2Value property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."Parameter2Value"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Parameter2Value
		{
			get { return (System.String)GetValue((int)PublishingItemFieldIndex.Parameter2Value, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.Parameter2Value, value, true); }
		}

		/// <summary> The PublishedUTC property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."PublishedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime PublishedUTC
		{
			get { return (System.DateTime)GetValue((int)PublishingItemFieldIndex.PublishedUTC, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.PublishedUTC, value, true); }
		}

		/// <summary> The PublishedTicks property of the Entity PublishingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PublishingItem"."PublishedTicks"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PublishedTicks
		{
			get { return (System.Int64)GetValue((int)PublishingItemFieldIndex.PublishedTicks, true); }
			set	{ SetValue((int)PublishingItemFieldIndex.PublishedTicks, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PublishingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePublishingEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PublishingEntity PublishingEntity
		{
			get	{ return GetSinglePublishingEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPublishingEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PublishingItemCollection", "PublishingEntity", _publishingEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PublishingEntity. When set to true, PublishingEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PublishingEntity is accessed. You can always execute a forced fetch by calling GetSinglePublishingEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPublishingEntity
		{
			get	{ return _alwaysFetchPublishingEntity; }
			set	{ _alwaysFetchPublishingEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PublishingEntity already has been fetched. Setting this property to false when PublishingEntity has been fetched
		/// will set PublishingEntity to null as well. Setting this property to true while PublishingEntity hasn't been fetched disables lazy loading for PublishingEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPublishingEntity
		{
			get { return _alreadyFetchedPublishingEntity;}
			set 
			{
				if(_alreadyFetchedPublishingEntity && !value)
				{
					this.PublishingEntity = null;
				}
				_alreadyFetchedPublishingEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PublishingEntity is not found
		/// in the database. When set to true, PublishingEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PublishingEntityReturnsNewIfNotFound
		{
			get	{ return _publishingEntityReturnsNewIfNotFound; }
			set { _publishingEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PublishingItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
