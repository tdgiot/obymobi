﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'FeatureFlag'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class FeatureFlagEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "FeatureFlagEntity"; }
		}
	
		#region Class Member Declarations
		private ApplicationConfigurationEntity _applicationConfigurationEntity;
		private bool	_alwaysFetchApplicationConfigurationEntity, _alreadyFetchedApplicationConfigurationEntity, _applicationConfigurationEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ApplicationConfigurationEntity</summary>
			public static readonly string ApplicationConfigurationEntity = "ApplicationConfigurationEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FeatureFlagEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected FeatureFlagEntityBase() :base("FeatureFlagEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		protected FeatureFlagEntityBase(System.Int32 featureFlagId):base("FeatureFlagEntity")
		{
			InitClassFetch(featureFlagId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected FeatureFlagEntityBase(System.Int32 featureFlagId, IPrefetchPath prefetchPathToUse): base("FeatureFlagEntity")
		{
			InitClassFetch(featureFlagId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="validator">The custom validator object for this FeatureFlagEntity</param>
		protected FeatureFlagEntityBase(System.Int32 featureFlagId, IValidator validator):base("FeatureFlagEntity")
		{
			InitClassFetch(featureFlagId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FeatureFlagEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_applicationConfigurationEntity = (ApplicationConfigurationEntity)info.GetValue("_applicationConfigurationEntity", typeof(ApplicationConfigurationEntity));
			if(_applicationConfigurationEntity!=null)
			{
				_applicationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_applicationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_applicationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchApplicationConfigurationEntity = info.GetBoolean("_alwaysFetchApplicationConfigurationEntity");
			_alreadyFetchedApplicationConfigurationEntity = info.GetBoolean("_alreadyFetchedApplicationConfigurationEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FeatureFlagFieldIndex)fieldIndex)
			{
				case FeatureFlagFieldIndex.ApplicationConfigurationId:
					DesetupSyncApplicationConfigurationEntity(true, false);
					_alreadyFetchedApplicationConfigurationEntity = false;
					break;
				case FeatureFlagFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApplicationConfigurationEntity = (_applicationConfigurationEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					toReturn.Add(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_applicationConfigurationEntity", (!this.MarkedForDeletion?_applicationConfigurationEntity:null));
			info.AddValue("_applicationConfigurationEntityReturnsNewIfNotFound", _applicationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApplicationConfigurationEntity", _alwaysFetchApplicationConfigurationEntity);
			info.AddValue("_alreadyFetchedApplicationConfigurationEntity", _alreadyFetchedApplicationConfigurationEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ApplicationConfigurationEntity":
					_alreadyFetchedApplicationConfigurationEntity = true;
					this.ApplicationConfigurationEntity = (ApplicationConfigurationEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					SetupSyncApplicationConfigurationEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					DesetupSyncApplicationConfigurationEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_applicationConfigurationEntity!=null)
			{
				toReturn.Add(_applicationConfigurationEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 featureFlagId)
		{
			return FetchUsingPK(featureFlagId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 featureFlagId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(featureFlagId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 featureFlagId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(featureFlagId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 featureFlagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(featureFlagId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FeatureFlagId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FeatureFlagRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity()
		{
			return GetSingleApplicationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public virtual ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedApplicationConfigurationEntity || forceFetch || _alwaysFetchApplicationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
				ApplicationConfigurationEntity newEntity = new ApplicationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApplicationConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (ApplicationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_applicationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ApplicationConfigurationEntity = newEntity;
				_alreadyFetchedApplicationConfigurationEntity = fetchResult;
			}
			return _applicationConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ApplicationConfigurationEntity", _applicationConfigurationEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="validator">The validator object for this FeatureFlagEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 featureFlagId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(featureFlagId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_applicationConfigurationEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FeatureFlagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplessFeatureFlags", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApplicationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticFeatureFlagRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, signalRelatedEntity, "FeatureFlagCollection", resetFKFields, new int[] { (int)FeatureFlagFieldIndex.ApplicationConfigurationId } );		
			_applicationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApplicationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_applicationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncApplicationConfigurationEntity(true, true);
				_applicationConfigurationEntity = (ApplicationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticFeatureFlagRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, ref _alreadyFetchedApplicationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApplicationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticFeatureFlagRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "FeatureFlagCollection", resetFKFields, new int[] { (int)FeatureFlagFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticFeatureFlagRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="featureFlagId">PK value for FeatureFlag which data should be fetched into this FeatureFlag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 featureFlagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FeatureFlagFieldIndex.FeatureFlagId].ForcedCurrentValueWrite(featureFlagId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFeatureFlagDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FeatureFlagEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FeatureFlagRelations Relations
		{
			get	{ return new FeatureFlagRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationConfigurationCollection(), (IEntityRelation)GetRelationsForField("ApplicationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.FeatureFlagEntity, (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, 0, null, null, null, "ApplicationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.FeatureFlagEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The FeatureFlagId property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."FeatureFlagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 FeatureFlagId
		{
			get { return (System.Int32)GetValue((int)FeatureFlagFieldIndex.FeatureFlagId, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.FeatureFlagId, value, true); }
		}

		/// <summary> The ApplicationConfigurationId property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."ApplicationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApplicationConfigurationId
		{
			get { return (System.Int32)GetValue((int)FeatureFlagFieldIndex.ApplicationConfigurationId, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.ApplicationConfigurationId, value, true); }
		}

		/// <summary> The ApplessFeatureFlags property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."ApplessFeatureFlags"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ApplessFeatureFlags ApplessFeatureFlags
		{
			get { return (Obymobi.Enums.ApplessFeatureFlags)GetValue((int)FeatureFlagFieldIndex.ApplessFeatureFlags, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.ApplessFeatureFlags, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)FeatureFlagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)FeatureFlagFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)FeatureFlagFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FeatureFlagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)FeatureFlagFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)FeatureFlagFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)FeatureFlagFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CompanyId property of the Entity FeatureFlag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "FeatureFlag"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)FeatureFlagFieldIndex.CompanyId, true); }
			set	{ SetValue((int)FeatureFlagFieldIndex.CompanyId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ApplicationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApplicationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ApplicationConfigurationEntity ApplicationConfigurationEntity
		{
			get	{ return GetSingleApplicationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApplicationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FeatureFlagCollection", "ApplicationConfigurationEntity", _applicationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationConfigurationEntity. When set to true, ApplicationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleApplicationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationConfigurationEntity
		{
			get	{ return _alwaysFetchApplicationConfigurationEntity; }
			set	{ _alwaysFetchApplicationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationConfigurationEntity already has been fetched. Setting this property to false when ApplicationConfigurationEntity has been fetched
		/// will set ApplicationConfigurationEntity to null as well. Setting this property to true while ApplicationConfigurationEntity hasn't been fetched disables lazy loading for ApplicationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationConfigurationEntity
		{
			get { return _alreadyFetchedApplicationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedApplicationConfigurationEntity && !value)
				{
					this.ApplicationConfigurationEntity = null;
				}
				_alreadyFetchedApplicationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ApplicationConfigurationEntity is not found
		/// in the database. When set to true, ApplicationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ApplicationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _applicationConfigurationEntityReturnsNewIfNotFound; }
			set { _applicationConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FeatureFlagCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.FeatureFlagEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
