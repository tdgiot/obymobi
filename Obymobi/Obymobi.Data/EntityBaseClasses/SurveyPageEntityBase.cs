﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SurveyPage'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SurveyPageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SurveyPageEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.SurveyQuestionCollection	_surveyQuestionCollection;
		private bool	_alwaysFetchSurveyQuestionCollection, _alreadyFetchedSurveyQuestionCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementCollection _advertisementCollectionViaMedium;
		private bool	_alwaysFetchAdvertisementCollectionViaMedium, _alreadyFetchedAdvertisementCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationCollection _alterationCollectionViaMedium;
		private bool	_alwaysFetchAlterationCollectionViaMedium, _alreadyFetchedAlterationCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium_;
		private bool	_alwaysFetchCategoryCollectionViaMedium_, _alreadyFetchedCategoryCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMedium;
		private bool	_alwaysFetchCompanyCollectionViaMedium, _alreadyFetchedCompanyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaMedium;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaMedium, _alreadyFetchedDeliverypointgroupCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium_;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium_, _alreadyFetchedEntertainmentCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection _genericcategoryCollectionViaMedium;
		private bool	_alwaysFetchGenericcategoryCollectionViaMedium, _alreadyFetchedGenericcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaMedium;
		private bool	_alwaysFetchGenericproductCollectionViaMedium, _alreadyFetchedGenericproductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium_;
		private bool	_alwaysFetchProductCollectionViaMedium_, _alreadyFetchedProductCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.SurveyCollection _surveyCollectionViaMedium;
		private bool	_alwaysFetchSurveyCollectionViaMedium, _alreadyFetchedSurveyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.SurveyQuestionCollection _surveyQuestionCollectionViaSurveyQuestion;
		private bool	_alwaysFetchSurveyQuestionCollectionViaSurveyQuestion, _alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion;
		private SurveyEntity _surveyEntity;
		private bool	_alwaysFetchSurveyEntity, _alreadyFetchedSurveyEntity, _surveyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SurveyEntity</summary>
			public static readonly string SurveyEntity = "SurveyEntity";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name SurveyQuestionCollection</summary>
			public static readonly string SurveyQuestionCollection = "SurveyQuestionCollection";
			/// <summary>Member name AdvertisementCollectionViaMedium</summary>
			public static readonly string AdvertisementCollectionViaMedium = "AdvertisementCollectionViaMedium";
			/// <summary>Member name AlterationCollectionViaMedium</summary>
			public static readonly string AlterationCollectionViaMedium = "AlterationCollectionViaMedium";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium_</summary>
			public static readonly string CategoryCollectionViaMedium_ = "CategoryCollectionViaMedium_";
			/// <summary>Member name CompanyCollectionViaMedium</summary>
			public static readonly string CompanyCollectionViaMedium = "CompanyCollectionViaMedium";
			/// <summary>Member name DeliverypointgroupCollectionViaMedium</summary>
			public static readonly string DeliverypointgroupCollectionViaMedium = "DeliverypointgroupCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium_</summary>
			public static readonly string EntertainmentCollectionViaMedium_ = "EntertainmentCollectionViaMedium_";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name GenericcategoryCollectionViaMedium</summary>
			public static readonly string GenericcategoryCollectionViaMedium = "GenericcategoryCollectionViaMedium";
			/// <summary>Member name GenericproductCollectionViaMedium</summary>
			public static readonly string GenericproductCollectionViaMedium = "GenericproductCollectionViaMedium";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium_</summary>
			public static readonly string ProductCollectionViaMedium_ = "ProductCollectionViaMedium_";
			/// <summary>Member name SurveyCollectionViaMedium</summary>
			public static readonly string SurveyCollectionViaMedium = "SurveyCollectionViaMedium";
			/// <summary>Member name SurveyQuestionCollectionViaSurveyQuestion</summary>
			public static readonly string SurveyQuestionCollectionViaSurveyQuestion = "SurveyQuestionCollectionViaSurveyQuestion";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyPageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SurveyPageEntityBase() :base("SurveyPageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		protected SurveyPageEntityBase(System.Int32 surveyPageId):base("SurveyPageEntity")
		{
			InitClassFetch(surveyPageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SurveyPageEntityBase(System.Int32 surveyPageId, IPrefetchPath prefetchPathToUse): base("SurveyPageEntity")
		{
			InitClassFetch(surveyPageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <param name="validator">The custom validator object for this SurveyPageEntity</param>
		protected SurveyPageEntityBase(System.Int32 surveyPageId, IValidator validator):base("SurveyPageEntity")
		{
			InitClassFetch(surveyPageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyPageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_surveyQuestionCollection = (Obymobi.Data.CollectionClasses.SurveyQuestionCollection)info.GetValue("_surveyQuestionCollection", typeof(Obymobi.Data.CollectionClasses.SurveyQuestionCollection));
			_alwaysFetchSurveyQuestionCollection = info.GetBoolean("_alwaysFetchSurveyQuestionCollection");
			_alreadyFetchedSurveyQuestionCollection = info.GetBoolean("_alreadyFetchedSurveyQuestionCollection");
			_advertisementCollectionViaMedium = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollectionViaMedium = info.GetBoolean("_alwaysFetchAdvertisementCollectionViaMedium");
			_alreadyFetchedAdvertisementCollectionViaMedium = info.GetBoolean("_alreadyFetchedAdvertisementCollectionViaMedium");

			_alterationCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationCollectionViaMedium");
			_alreadyFetchedAlterationCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationCollectionViaMedium");

			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_categoryCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium_ = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium_");
			_alreadyFetchedCategoryCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium_");

			_companyCollectionViaMedium = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMedium = info.GetBoolean("_alwaysFetchCompanyCollectionViaMedium");
			_alreadyFetchedCompanyCollectionViaMedium = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMedium");

			_deliverypointgroupCollectionViaMedium = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaMedium");
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaMedium");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium_");
			_alreadyFetchedEntertainmentCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium_");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_genericcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericcategoryCollectionViaMedium");
			_alreadyFetchedGenericcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericcategoryCollectionViaMedium");

			_genericproductCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericproductCollectionViaMedium");
			_alreadyFetchedGenericproductCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaMedium");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_productCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium_ = info.GetBoolean("_alwaysFetchProductCollectionViaMedium_");
			_alreadyFetchedProductCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium_");

			_surveyCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyCollection)info.GetValue("_surveyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyCollection));
			_alwaysFetchSurveyCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyCollectionViaMedium");
			_alreadyFetchedSurveyCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyCollectionViaMedium");

			_surveyQuestionCollectionViaSurveyQuestion = (Obymobi.Data.CollectionClasses.SurveyQuestionCollection)info.GetValue("_surveyQuestionCollectionViaSurveyQuestion", typeof(Obymobi.Data.CollectionClasses.SurveyQuestionCollection));
			_alwaysFetchSurveyQuestionCollectionViaSurveyQuestion = info.GetBoolean("_alwaysFetchSurveyQuestionCollectionViaSurveyQuestion");
			_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion = info.GetBoolean("_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion");
			_surveyEntity = (SurveyEntity)info.GetValue("_surveyEntity", typeof(SurveyEntity));
			if(_surveyEntity!=null)
			{
				_surveyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyEntityReturnsNewIfNotFound = info.GetBoolean("_surveyEntityReturnsNewIfNotFound");
			_alwaysFetchSurveyEntity = info.GetBoolean("_alwaysFetchSurveyEntity");
			_alreadyFetchedSurveyEntity = info.GetBoolean("_alreadyFetchedSurveyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyPageFieldIndex)fieldIndex)
			{
				case SurveyPageFieldIndex.SurveyId:
					DesetupSyncSurveyEntity(true, false);
					_alreadyFetchedSurveyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedSurveyQuestionCollection = (_surveyQuestionCollection.Count > 0);
			_alreadyFetchedAdvertisementCollectionViaMedium = (_advertisementCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationCollectionViaMedium = (_alterationCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium_ = (_categoryCollectionViaMedium_.Count > 0);
			_alreadyFetchedCompanyCollectionViaMedium = (_companyCollectionViaMedium.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = (_deliverypointgroupCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium_ = (_entertainmentCollectionViaMedium_.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericcategoryCollectionViaMedium = (_genericcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericproductCollectionViaMedium = (_genericproductCollectionViaMedium.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium_ = (_productCollectionViaMedium_.Count > 0);
			_alreadyFetchedSurveyCollectionViaMedium = (_surveyCollectionViaMedium.Count > 0);
			_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion = (_surveyQuestionCollectionViaSurveyQuestion.Count > 0);
			_alreadyFetchedSurveyEntity = (_surveyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SurveyEntity":
					toReturn.Add(Relations.SurveyEntityUsingSurveyId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId);
					break;
				case "SurveyQuestionCollection":
					toReturn.Add(Relations.SurveyQuestionEntityUsingSurveyPageId);
					break;
				case "AdvertisementCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CompanyEntityUsingCompanyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericcategoryEntityUsingGenericcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericproductEntityUsingGenericproductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingSurveyPageId, "SurveyPageEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyQuestionCollectionViaSurveyQuestion":
					toReturn.Add(Relations.SurveyQuestionEntityUsingSurveyPageId, "SurveyPageEntity__", "SurveyQuestion_", JoinHint.None);
					toReturn.Add(SurveyQuestionEntity.Relations.SurveyQuestionEntityUsingParentQuestionId, "SurveyQuestion_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_surveyQuestionCollection", (!this.MarkedForDeletion?_surveyQuestionCollection:null));
			info.AddValue("_alwaysFetchSurveyQuestionCollection", _alwaysFetchSurveyQuestionCollection);
			info.AddValue("_alreadyFetchedSurveyQuestionCollection", _alreadyFetchedSurveyQuestionCollection);
			info.AddValue("_advertisementCollectionViaMedium", (!this.MarkedForDeletion?_advertisementCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAdvertisementCollectionViaMedium", _alwaysFetchAdvertisementCollectionViaMedium);
			info.AddValue("_alreadyFetchedAdvertisementCollectionViaMedium", _alreadyFetchedAdvertisementCollectionViaMedium);
			info.AddValue("_alterationCollectionViaMedium", (!this.MarkedForDeletion?_alterationCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationCollectionViaMedium", _alwaysFetchAlterationCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationCollectionViaMedium", _alreadyFetchedAlterationCollectionViaMedium);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium_", (!this.MarkedForDeletion?_categoryCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium_", _alwaysFetchCategoryCollectionViaMedium_);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium_", _alreadyFetchedCategoryCollectionViaMedium_);
			info.AddValue("_companyCollectionViaMedium", (!this.MarkedForDeletion?_companyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMedium", _alwaysFetchCompanyCollectionViaMedium);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMedium", _alreadyFetchedCompanyCollectionViaMedium);
			info.AddValue("_deliverypointgroupCollectionViaMedium", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaMedium:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaMedium", _alwaysFetchDeliverypointgroupCollectionViaMedium);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaMedium", _alreadyFetchedDeliverypointgroupCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium_", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium_", _alwaysFetchEntertainmentCollectionViaMedium_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium_", _alreadyFetchedEntertainmentCollectionViaMedium_);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_genericcategoryCollectionViaMedium", (!this.MarkedForDeletion?_genericcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericcategoryCollectionViaMedium", _alwaysFetchGenericcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericcategoryCollectionViaMedium", _alreadyFetchedGenericcategoryCollectionViaMedium);
			info.AddValue("_genericproductCollectionViaMedium", (!this.MarkedForDeletion?_genericproductCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaMedium", _alwaysFetchGenericproductCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaMedium", _alreadyFetchedGenericproductCollectionViaMedium);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium_", (!this.MarkedForDeletion?_productCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium_", _alwaysFetchProductCollectionViaMedium_);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium_", _alreadyFetchedProductCollectionViaMedium_);
			info.AddValue("_surveyCollectionViaMedium", (!this.MarkedForDeletion?_surveyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyCollectionViaMedium", _alwaysFetchSurveyCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyCollectionViaMedium", _alreadyFetchedSurveyCollectionViaMedium);
			info.AddValue("_surveyQuestionCollectionViaSurveyQuestion", (!this.MarkedForDeletion?_surveyQuestionCollectionViaSurveyQuestion:null));
			info.AddValue("_alwaysFetchSurveyQuestionCollectionViaSurveyQuestion", _alwaysFetchSurveyQuestionCollectionViaSurveyQuestion);
			info.AddValue("_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion", _alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion);
			info.AddValue("_surveyEntity", (!this.MarkedForDeletion?_surveyEntity:null));
			info.AddValue("_surveyEntityReturnsNewIfNotFound", _surveyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyEntity", _alwaysFetchSurveyEntity);
			info.AddValue("_alreadyFetchedSurveyEntity", _alreadyFetchedSurveyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SurveyEntity":
					_alreadyFetchedSurveyEntity = true;
					this.SurveyEntity = (SurveyEntity)entity;
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "SurveyQuestionCollection":
					_alreadyFetchedSurveyQuestionCollection = true;
					if(entity!=null)
					{
						this.SurveyQuestionCollection.Add((SurveyQuestionEntity)entity);
					}
					break;
				case "AdvertisementCollectionViaMedium":
					_alreadyFetchedAdvertisementCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AdvertisementCollectionViaMedium.Add((AdvertisementEntity)entity);
					}
					break;
				case "AlterationCollectionViaMedium":
					_alreadyFetchedAlterationCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationCollectionViaMedium.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium_":
					_alreadyFetchedCategoryCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium_.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaMedium":
					_alreadyFetchedCompanyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMedium.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaMedium":
					_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaMedium.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium_":
					_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "GenericcategoryCollectionViaMedium":
					_alreadyFetchedGenericcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericcategoryCollectionViaMedium.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericproductCollectionViaMedium":
					_alreadyFetchedGenericproductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaMedium.Add((GenericproductEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium_":
					_alreadyFetchedProductCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium_.Add((ProductEntity)entity);
					}
					break;
				case "SurveyCollectionViaMedium":
					_alreadyFetchedSurveyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyCollectionViaMedium.Add((SurveyEntity)entity);
					}
					break;
				case "SurveyQuestionCollectionViaSurveyQuestion":
					_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion = true;
					if(entity!=null)
					{
						this.SurveyQuestionCollectionViaSurveyQuestion.Add((SurveyQuestionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SurveyEntity":
					SetupSyncSurveyEntity(relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "SurveyQuestionCollection":
					_surveyQuestionCollection.Add((SurveyQuestionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SurveyEntity":
					DesetupSyncSurveyEntity(false, true);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyQuestionCollection":
					this.PerformRelatedEntityRemoval(_surveyQuestionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_surveyEntity!=null)
			{
				toReturn.Add(_surveyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_mediaCollection);
			toReturn.Add(_surveyQuestionCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyPageId)
		{
			return FetchUsingPK(surveyPageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyPageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyPageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyPageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyPageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyPageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyPageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyPageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyPageRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch)
		{
			return GetMultiSurveyQuestionCollection(forceFetch, _surveyQuestionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyQuestionCollection(forceFetch, _surveyQuestionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyQuestionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyQuestionCollection || forceFetch || _alwaysFetchSurveyQuestionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestionCollection);
				_surveyQuestionCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestionCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestionCollection.GetMultiManyToOne(this, null, filter);
				_surveyQuestionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestionCollection = true;
			}
			return _surveyQuestionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestionCollection'. These settings will be taken into account
		/// when the property SurveyQuestionCollection is requested or GetMultiSurveyQuestionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestionCollection.SortClauses=sortClauses;
			_surveyQuestionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAdvertisementCollectionViaMedium(forceFetch, _advertisementCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAdvertisementCollectionViaMedium || forceFetch || _alwaysFetchAdvertisementCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollectionViaMedium.GetMulti(filter, GetRelationsForField("AdvertisementCollectionViaMedium"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollectionViaMedium = true;
			}
			return _advertisementCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollectionViaMedium'. These settings will be taken into account
		/// when the property AdvertisementCollectionViaMedium is requested or GetMultiAdvertisementCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollectionViaMedium.SortClauses=sortClauses;
			_advertisementCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationCollectionViaMedium(forceFetch, _alterationCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationCollectionViaMedium || forceFetch || _alwaysFetchAlterationCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationCollectionViaMedium"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollectionViaMedium = true;
			}
			return _alterationCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationCollectionViaMedium is requested or GetMultiAlterationCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollectionViaMedium.SortClauses=sortClauses;
			_alterationCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium_(forceFetch, _categoryCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium_ || forceFetch || _alwaysFetchCategoryCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium_.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium_"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium_ = true;
			}
			return _categoryCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium_'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium_ is requested or GetMultiCategoryCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium_.SortClauses=sortClauses;
			_categoryCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMedium(forceFetch, _companyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMedium || forceFetch || _alwaysFetchCompanyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMedium.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMedium"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMedium = true;
			}
			return _companyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMedium'. These settings will be taken into account
		/// when the property CompanyCollectionViaMedium is requested or GetMultiCompanyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMedium.SortClauses=sortClauses;
			_companyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaMedium(forceFetch, _deliverypointgroupCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaMedium || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaMedium.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaMedium"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
			}
			return _deliverypointgroupCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaMedium'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaMedium is requested or GetMultiDeliverypointgroupCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaMedium.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium_(forceFetch, _entertainmentCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium_ || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium_"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
			}
			return _entertainmentCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium_ is requested or GetMultiEntertainmentCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium_.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericcategoryCollectionViaMedium(forceFetch, _genericcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollectionViaMedium || forceFetch || _alwaysFetchGenericcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericcategoryCollectionViaMedium"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollectionViaMedium = true;
			}
			return _genericcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericcategoryCollectionViaMedium is requested or GetMultiGenericcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollectionViaMedium.SortClauses=sortClauses;
			_genericcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaMedium(forceFetch, _genericproductCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaMedium || forceFetch || _alwaysFetchGenericproductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaMedium"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaMedium = true;
			}
			return _genericproductCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericproductCollectionViaMedium is requested or GetMultiGenericproductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaMedium.SortClauses=sortClauses;
			_genericproductCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium_(forceFetch, _productCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium_ || forceFetch || _alwaysFetchProductCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium_.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium_"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium_ = true;
			}
			return _productCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium_'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium_ is requested or GetMultiProductCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium_.SortClauses=sortClauses;
			_productCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyCollectionViaMedium(forceFetch, _surveyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyCollectionViaMedium || forceFetch || _alwaysFetchSurveyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyCollectionViaMedium"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyCollectionViaMedium = true;
			}
			return _surveyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyCollectionViaMedium is requested or GetMultiSurveyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyCollectionViaMedium.SortClauses=sortClauses;
			_surveyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollectionViaSurveyQuestion(bool forceFetch)
		{
			return GetMultiSurveyQuestionCollectionViaSurveyQuestion(forceFetch, _surveyQuestionCollectionViaSurveyQuestion.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollectionViaSurveyQuestion(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion || forceFetch || _alwaysFetchSurveyQuestionCollectionViaSurveyQuestion) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestionCollectionViaSurveyQuestion);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyPageFields.SurveyPageId, ComparisonOperator.Equal, this.SurveyPageId, "SurveyPageEntity__"));
				_surveyQuestionCollectionViaSurveyQuestion.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestionCollectionViaSurveyQuestion.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestionCollectionViaSurveyQuestion.GetMulti(filter, GetRelationsForField("SurveyQuestionCollectionViaSurveyQuestion"));
				_surveyQuestionCollectionViaSurveyQuestion.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion = true;
			}
			return _surveyQuestionCollectionViaSurveyQuestion;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestionCollectionViaSurveyQuestion'. These settings will be taken into account
		/// when the property SurveyQuestionCollectionViaSurveyQuestion is requested or GetMultiSurveyQuestionCollectionViaSurveyQuestion is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestionCollectionViaSurveyQuestion(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestionCollectionViaSurveyQuestion.SortClauses=sortClauses;
			_surveyQuestionCollectionViaSurveyQuestion.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public SurveyEntity GetSingleSurveyEntity()
		{
			return GetSingleSurveyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public virtual SurveyEntity GetSingleSurveyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyEntity || forceFetch || _alwaysFetchSurveyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyEntityUsingSurveyId);
				SurveyEntity newEntity = new SurveyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyId);
				}
				if(fetchResult)
				{
					newEntity = (SurveyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyEntity = newEntity;
				_alreadyFetchedSurveyEntity = fetchResult;
			}
			return _surveyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SurveyEntity", _surveyEntity);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("SurveyQuestionCollection", _surveyQuestionCollection);
			toReturn.Add("AdvertisementCollectionViaMedium", _advertisementCollectionViaMedium);
			toReturn.Add("AlterationCollectionViaMedium", _alterationCollectionViaMedium);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium_", _categoryCollectionViaMedium_);
			toReturn.Add("CompanyCollectionViaMedium", _companyCollectionViaMedium);
			toReturn.Add("DeliverypointgroupCollectionViaMedium", _deliverypointgroupCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium_", _entertainmentCollectionViaMedium_);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("GenericcategoryCollectionViaMedium", _genericcategoryCollectionViaMedium);
			toReturn.Add("GenericproductCollectionViaMedium", _genericproductCollectionViaMedium);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium_", _productCollectionViaMedium_);
			toReturn.Add("SurveyCollectionViaMedium", _surveyCollectionViaMedium);
			toReturn.Add("SurveyQuestionCollectionViaSurveyQuestion", _surveyQuestionCollectionViaSurveyQuestion);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <param name="validator">The validator object for this SurveyPageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 surveyPageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyPageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "SurveyPageEntity");

			_surveyQuestionCollection = new Obymobi.Data.CollectionClasses.SurveyQuestionCollection();
			_surveyQuestionCollection.SetContainingEntityInfo(this, "SurveyPageEntity");
			_advertisementCollectionViaMedium = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_alterationCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaMedium = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointgroupCollectionViaMedium = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_genericcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericproductCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_surveyCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyCollection();
			_surveyQuestionCollectionViaSurveyQuestion = new Obymobi.Data.CollectionClasses.SurveyQuestionCollection();
			_surveyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _surveyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyEntity, new PropertyChangedEventHandler( OnSurveyEntityPropertyChanged ), "SurveyEntity", Obymobi.Data.RelationClasses.StaticSurveyPageRelations.SurveyEntityUsingSurveyIdStatic, true, signalRelatedEntity, "SurveyPageCollection", resetFKFields, new int[] { (int)SurveyPageFieldIndex.SurveyId } );		
			_surveyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _surveyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyEntity(IEntityCore relatedEntity)
		{
			if(_surveyEntity!=relatedEntity)
			{		
				DesetupSyncSurveyEntity(true, true);
				_surveyEntity = (SurveyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyEntity, new PropertyChangedEventHandler( OnSurveyEntityPropertyChanged ), "SurveyEntity", Obymobi.Data.RelationClasses.StaticSurveyPageRelations.SurveyEntityUsingSurveyIdStatic, true, ref _alreadyFetchedSurveyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyPageId">PK value for SurveyPage which data should be fetched into this SurveyPage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 surveyPageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyPageFieldIndex.SurveyPageId].ForcedCurrentValueWrite(surveyPageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyPageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyPageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyPageRelations Relations
		{
			get	{ return new SurveyPageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestionCollection")[0], (int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, null, "SurveyQuestionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, GetRelationsForField("AdvertisementCollectionViaMedium"), "AdvertisementCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, GetRelationsForField("AlterationCollectionViaMedium"), "AlterationCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium_"), "CategoryCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMedium"), "CompanyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaMedium"), "DeliverypointgroupCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium_"), "EntertainmentCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, GetRelationsForField("GenericcategoryCollectionViaMedium"), "GenericcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaMedium"), "GenericproductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium_"), "ProductCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.SurveyEntity, 0, null, null, GetRelationsForField("SurveyCollectionViaMedium"), "SurveyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionCollectionViaSurveyQuestion
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SurveyQuestionEntityUsingSurveyPageId;
				intermediateRelation.SetAliases(string.Empty, "SurveyQuestion_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, GetRelationsForField("SurveyQuestionCollectionViaSurveyQuestion"), "SurveyQuestionCollectionViaSurveyQuestion", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyCollection(), (IEntityRelation)GetRelationsForField("SurveyEntity")[0], (int)Obymobi.Data.EntityType.SurveyPageEntity, (int)Obymobi.Data.EntityType.SurveyEntity, 0, null, null, null, "SurveyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SurveyPageId property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."SurveyPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SurveyPageId
		{
			get { return (System.Int32)GetValue((int)SurveyPageFieldIndex.SurveyPageId, true); }
			set	{ SetValue((int)SurveyPageFieldIndex.SurveyPageId, value, true); }
		}

		/// <summary> The SurveyId property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."SurveyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SurveyId
		{
			get { return (System.Int32)GetValue((int)SurveyPageFieldIndex.SurveyId, true); }
			set	{ SetValue((int)SurveyPageFieldIndex.SurveyId, value, true); }
		}

		/// <summary> The Name property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SurveyPageFieldIndex.Name, true); }
			set	{ SetValue((int)SurveyPageFieldIndex.Name, value, true); }
		}

		/// <summary> The SortOrder property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)SurveyPageFieldIndex.SortOrder, true); }
			set	{ SetValue((int)SurveyPageFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyPageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SurveyPageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyPageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SurveyPageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)SurveyPageFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)SurveyPageFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyPageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SurveyPageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SurveyPage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyPage"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyPageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SurveyPageFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionCollection SurveyQuestionCollection
		{
			get	{ return GetMultiSurveyQuestionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionCollection. When set to true, SurveyQuestionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyQuestionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionCollection
		{
			get	{ return _alwaysFetchSurveyQuestionCollection; }
			set	{ _alwaysFetchSurveyQuestionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionCollection already has been fetched. Setting this property to false when SurveyQuestionCollection has been fetched
		/// will clear the SurveyQuestionCollection collection well. Setting this property to true while SurveyQuestionCollection hasn't been fetched disables lazy loading for SurveyQuestionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionCollection
		{
			get { return _alreadyFetchedSurveyQuestionCollection;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionCollection && !value && (_surveyQuestionCollection != null))
				{
					_surveyQuestionCollection.Clear();
				}
				_alreadyFetchedSurveyQuestionCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollectionViaMedium
		{
			get { return GetMultiAdvertisementCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollectionViaMedium. When set to true, AdvertisementCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAdvertisementCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollectionViaMedium
		{
			get	{ return _alwaysFetchAdvertisementCollectionViaMedium; }
			set	{ _alwaysFetchAdvertisementCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollectionViaMedium already has been fetched. Setting this property to false when AdvertisementCollectionViaMedium has been fetched
		/// will clear the AdvertisementCollectionViaMedium collection well. Setting this property to true while AdvertisementCollectionViaMedium hasn't been fetched disables lazy loading for AdvertisementCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollectionViaMedium
		{
			get { return _alreadyFetchedAdvertisementCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollectionViaMedium && !value && (_advertisementCollectionViaMedium != null))
				{
					_advertisementCollectionViaMedium.Clear();
				}
				_alreadyFetchedAdvertisementCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollectionViaMedium
		{
			get { return GetMultiAlterationCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollectionViaMedium. When set to true, AlterationCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationCollectionViaMedium; }
			set	{ _alwaysFetchAlterationCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollectionViaMedium already has been fetched. Setting this property to false when AlterationCollectionViaMedium has been fetched
		/// will clear the AlterationCollectionViaMedium collection well. Setting this property to true while AlterationCollectionViaMedium hasn't been fetched disables lazy loading for AlterationCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationCollectionViaMedium && !value && (_alterationCollectionViaMedium != null))
				{
					_alterationCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium_
		{
			get { return GetMultiCategoryCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium_. When set to true, CategoryCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium_
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium_; }
			set	{ _alwaysFetchCategoryCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium_ already has been fetched. Setting this property to false when CategoryCollectionViaMedium_ has been fetched
		/// will clear the CategoryCollectionViaMedium_ collection well. Setting this property to true while CategoryCollectionViaMedium_ hasn't been fetched disables lazy loading for CategoryCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium_
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium_ && !value && (_categoryCollectionViaMedium_ != null))
				{
					_categoryCollectionViaMedium_.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMedium
		{
			get { return GetMultiCompanyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMedium. When set to true, CompanyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMedium
		{
			get	{ return _alwaysFetchCompanyCollectionViaMedium; }
			set	{ _alwaysFetchCompanyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMedium already has been fetched. Setting this property to false when CompanyCollectionViaMedium has been fetched
		/// will clear the CompanyCollectionViaMedium collection well. Setting this property to true while CompanyCollectionViaMedium hasn't been fetched disables lazy loading for CompanyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMedium
		{
			get { return _alreadyFetchedCompanyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMedium && !value && (_companyCollectionViaMedium != null))
				{
					_companyCollectionViaMedium.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaMedium
		{
			get { return GetMultiDeliverypointgroupCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaMedium. When set to true, DeliverypointgroupCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaMedium
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaMedium; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaMedium already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaMedium has been fetched
		/// will clear the DeliverypointgroupCollectionViaMedium collection well. Setting this property to true while DeliverypointgroupCollectionViaMedium hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaMedium
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaMedium && !value && (_deliverypointgroupCollectionViaMedium != null))
				{
					_deliverypointgroupCollectionViaMedium.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium_
		{
			get { return GetMultiEntertainmentCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium_. When set to true, EntertainmentCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium_; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium_ already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium_ has been fetched
		/// will clear the EntertainmentCollectionViaMedium_ collection well. Setting this property to true while EntertainmentCollectionViaMedium_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium_ && !value && (_entertainmentCollectionViaMedium_ != null))
				{
					_entertainmentCollectionViaMedium_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollectionViaMedium
		{
			get { return GetMultiGenericcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollectionViaMedium. When set to true, GenericcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericcategoryCollectionViaMedium; }
			set	{ _alwaysFetchGenericcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollectionViaMedium already has been fetched. Setting this property to false when GenericcategoryCollectionViaMedium has been fetched
		/// will clear the GenericcategoryCollectionViaMedium collection well. Setting this property to true while GenericcategoryCollectionViaMedium hasn't been fetched disables lazy loading for GenericcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedGenericcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollectionViaMedium && !value && (_genericcategoryCollectionViaMedium != null))
				{
					_genericcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaMedium
		{
			get { return GetMultiGenericproductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaMedium. When set to true, GenericproductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericproductCollectionViaMedium; }
			set	{ _alwaysFetchGenericproductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaMedium already has been fetched. Setting this property to false when GenericproductCollectionViaMedium has been fetched
		/// will clear the GenericproductCollectionViaMedium collection well. Setting this property to true while GenericproductCollectionViaMedium hasn't been fetched disables lazy loading for GenericproductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaMedium
		{
			get { return _alreadyFetchedGenericproductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaMedium && !value && (_genericproductCollectionViaMedium != null))
				{
					_genericproductCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium_
		{
			get { return GetMultiProductCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium_. When set to true, ProductCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium_
		{
			get	{ return _alwaysFetchProductCollectionViaMedium_; }
			set	{ _alwaysFetchProductCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium_ already has been fetched. Setting this property to false when ProductCollectionViaMedium_ has been fetched
		/// will clear the ProductCollectionViaMedium_ collection well. Setting this property to true while ProductCollectionViaMedium_ hasn't been fetched disables lazy loading for ProductCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium_
		{
			get { return _alreadyFetchedProductCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium_ && !value && (_productCollectionViaMedium_ != null))
				{
					_productCollectionViaMedium_.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyCollection SurveyCollectionViaMedium
		{
			get { return GetMultiSurveyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyCollectionViaMedium. When set to true, SurveyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyCollectionViaMedium; }
			set	{ _alwaysFetchSurveyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyCollectionViaMedium already has been fetched. Setting this property to false when SurveyCollectionViaMedium has been fetched
		/// will clear the SurveyCollectionViaMedium collection well. Setting this property to true while SurveyCollectionViaMedium hasn't been fetched disables lazy loading for SurveyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyCollectionViaMedium && !value && (_surveyCollectionViaMedium != null))
				{
					_surveyCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestionCollectionViaSurveyQuestion()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionCollection SurveyQuestionCollectionViaSurveyQuestion
		{
			get { return GetMultiSurveyQuestionCollectionViaSurveyQuestion(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionCollectionViaSurveyQuestion. When set to true, SurveyQuestionCollectionViaSurveyQuestion is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionCollectionViaSurveyQuestion is accessed. You can always execute a forced fetch by calling GetMultiSurveyQuestionCollectionViaSurveyQuestion(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionCollectionViaSurveyQuestion
		{
			get	{ return _alwaysFetchSurveyQuestionCollectionViaSurveyQuestion; }
			set	{ _alwaysFetchSurveyQuestionCollectionViaSurveyQuestion = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionCollectionViaSurveyQuestion already has been fetched. Setting this property to false when SurveyQuestionCollectionViaSurveyQuestion has been fetched
		/// will clear the SurveyQuestionCollectionViaSurveyQuestion collection well. Setting this property to true while SurveyQuestionCollectionViaSurveyQuestion hasn't been fetched disables lazy loading for SurveyQuestionCollectionViaSurveyQuestion</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionCollectionViaSurveyQuestion
		{
			get { return _alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion && !value && (_surveyQuestionCollectionViaSurveyQuestion != null))
				{
					_surveyQuestionCollectionViaSurveyQuestion.Clear();
				}
				_alreadyFetchedSurveyQuestionCollectionViaSurveyQuestion = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SurveyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyEntity SurveyEntity
		{
			get	{ return GetSingleSurveyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyPageCollection", "SurveyEntity", _surveyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyEntity. When set to true, SurveyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyEntity is accessed. You can always execute a forced fetch by calling GetSingleSurveyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyEntity
		{
			get	{ return _alwaysFetchSurveyEntity; }
			set	{ _alwaysFetchSurveyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyEntity already has been fetched. Setting this property to false when SurveyEntity has been fetched
		/// will set SurveyEntity to null as well. Setting this property to true while SurveyEntity hasn't been fetched disables lazy loading for SurveyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyEntity
		{
			get { return _alreadyFetchedSurveyEntity;}
			set 
			{
				if(_alreadyFetchedSurveyEntity && !value)
				{
					this.SurveyEntity = null;
				}
				_alreadyFetchedSurveyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyEntity is not found
		/// in the database. When set to true, SurveyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SurveyEntityReturnsNewIfNotFound
		{
			get	{ return _surveyEntityReturnsNewIfNotFound; }
			set { _surveyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SurveyPageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
