﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'OrderitemAlterationitem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class OrderitemAlterationitemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OrderitemAlterationitemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection	_orderitemAlterationitemTagCollection;
		private bool	_alwaysFetchOrderitemAlterationitemTagCollection, _alreadyFetchedOrderitemAlterationitemTagCollection;
		private AlterationEntity _alterationEntity;
		private bool	_alwaysFetchAlterationEntity, _alreadyFetchedAlterationEntity, _alterationEntityReturnsNewIfNotFound;
		private AlterationitemEntity _alterationitemEntity;
		private bool	_alwaysFetchAlterationitemEntity, _alreadyFetchedAlterationitemEntity, _alterationitemEntityReturnsNewIfNotFound;
		private OrderitemEntity _orderitemEntity;
		private bool	_alwaysFetchOrderitemEntity, _alreadyFetchedOrderitemEntity, _orderitemEntityReturnsNewIfNotFound;
		private PriceLevelItemEntity _priceLevelItemEntity;
		private bool	_alwaysFetchPriceLevelItemEntity, _alreadyFetchedPriceLevelItemEntity, _priceLevelItemEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private TaxTariffEntity _taxTariffEntity;
		private bool	_alwaysFetchTaxTariffEntity, _alreadyFetchedTaxTariffEntity, _taxTariffEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationEntity</summary>
			public static readonly string AlterationEntity = "AlterationEntity";
			/// <summary>Member name AlterationitemEntity</summary>
			public static readonly string AlterationitemEntity = "AlterationitemEntity";
			/// <summary>Member name OrderitemEntity</summary>
			public static readonly string OrderitemEntity = "OrderitemEntity";
			/// <summary>Member name PriceLevelItemEntity</summary>
			public static readonly string PriceLevelItemEntity = "PriceLevelItemEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name TaxTariffEntity</summary>
			public static readonly string TaxTariffEntity = "TaxTariffEntity";
			/// <summary>Member name OrderitemAlterationitemTagCollection</summary>
			public static readonly string OrderitemAlterationitemTagCollection = "OrderitemAlterationitemTagCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderitemAlterationitemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OrderitemAlterationitemEntityBase() :base("OrderitemAlterationitemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		protected OrderitemAlterationitemEntityBase(System.Int32 orderitemAlterationitemId):base("OrderitemAlterationitemEntity")
		{
			InitClassFetch(orderitemAlterationitemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OrderitemAlterationitemEntityBase(System.Int32 orderitemAlterationitemId, IPrefetchPath prefetchPathToUse): base("OrderitemAlterationitemEntity")
		{
			InitClassFetch(orderitemAlterationitemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="validator">The custom validator object for this OrderitemAlterationitemEntity</param>
		protected OrderitemAlterationitemEntityBase(System.Int32 orderitemAlterationitemId, IValidator validator):base("OrderitemAlterationitemEntity")
		{
			InitClassFetch(orderitemAlterationitemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemAlterationitemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_orderitemAlterationitemTagCollection = (Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection)info.GetValue("_orderitemAlterationitemTagCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection));
			_alwaysFetchOrderitemAlterationitemTagCollection = info.GetBoolean("_alwaysFetchOrderitemAlterationitemTagCollection");
			_alreadyFetchedOrderitemAlterationitemTagCollection = info.GetBoolean("_alreadyFetchedOrderitemAlterationitemTagCollection");
			_alterationEntity = (AlterationEntity)info.GetValue("_alterationEntity", typeof(AlterationEntity));
			if(_alterationEntity!=null)
			{
				_alterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationEntityReturnsNewIfNotFound = info.GetBoolean("_alterationEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationEntity = info.GetBoolean("_alwaysFetchAlterationEntity");
			_alreadyFetchedAlterationEntity = info.GetBoolean("_alreadyFetchedAlterationEntity");

			_alterationitemEntity = (AlterationitemEntity)info.GetValue("_alterationitemEntity", typeof(AlterationitemEntity));
			if(_alterationitemEntity!=null)
			{
				_alterationitemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationitemEntityReturnsNewIfNotFound = info.GetBoolean("_alterationitemEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationitemEntity = info.GetBoolean("_alwaysFetchAlterationitemEntity");
			_alreadyFetchedAlterationitemEntity = info.GetBoolean("_alreadyFetchedAlterationitemEntity");

			_orderitemEntity = (OrderitemEntity)info.GetValue("_orderitemEntity", typeof(OrderitemEntity));
			if(_orderitemEntity!=null)
			{
				_orderitemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderitemEntityReturnsNewIfNotFound = info.GetBoolean("_orderitemEntityReturnsNewIfNotFound");
			_alwaysFetchOrderitemEntity = info.GetBoolean("_alwaysFetchOrderitemEntity");
			_alreadyFetchedOrderitemEntity = info.GetBoolean("_alreadyFetchedOrderitemEntity");

			_priceLevelItemEntity = (PriceLevelItemEntity)info.GetValue("_priceLevelItemEntity", typeof(PriceLevelItemEntity));
			if(_priceLevelItemEntity!=null)
			{
				_priceLevelItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceLevelItemEntityReturnsNewIfNotFound = info.GetBoolean("_priceLevelItemEntityReturnsNewIfNotFound");
			_alwaysFetchPriceLevelItemEntity = info.GetBoolean("_alwaysFetchPriceLevelItemEntity");
			_alreadyFetchedPriceLevelItemEntity = info.GetBoolean("_alreadyFetchedPriceLevelItemEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_taxTariffEntity = (TaxTariffEntity)info.GetValue("_taxTariffEntity", typeof(TaxTariffEntity));
			if(_taxTariffEntity!=null)
			{
				_taxTariffEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_taxTariffEntityReturnsNewIfNotFound = info.GetBoolean("_taxTariffEntityReturnsNewIfNotFound");
			_alwaysFetchTaxTariffEntity = info.GetBoolean("_alwaysFetchTaxTariffEntity");
			_alreadyFetchedTaxTariffEntity = info.GetBoolean("_alreadyFetchedTaxTariffEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderitemAlterationitemFieldIndex)fieldIndex)
			{
				case OrderitemAlterationitemFieldIndex.OrderitemId:
					DesetupSyncOrderitemEntity(true, false);
					_alreadyFetchedOrderitemEntity = false;
					break;
				case OrderitemAlterationitemFieldIndex.AlterationitemId:
					DesetupSyncAlterationitemEntity(true, false);
					_alreadyFetchedAlterationitemEntity = false;
					break;
				case OrderitemAlterationitemFieldIndex.PriceLevelItemId:
					DesetupSyncPriceLevelItemEntity(true, false);
					_alreadyFetchedPriceLevelItemEntity = false;
					break;
				case OrderitemAlterationitemFieldIndex.AlterationId:
					DesetupSyncAlterationEntity(true, false);
					_alreadyFetchedAlterationEntity = false;
					break;
				case OrderitemAlterationitemFieldIndex.TaxTariffId:
					DesetupSyncTaxTariffEntity(true, false);
					_alreadyFetchedTaxTariffEntity = false;
					break;
				case OrderitemAlterationitemFieldIndex.AlterationoptionProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrderitemAlterationitemTagCollection = (_orderitemAlterationitemTagCollection.Count > 0);
			_alreadyFetchedAlterationEntity = (_alterationEntity != null);
			_alreadyFetchedAlterationitemEntity = (_alterationitemEntity != null);
			_alreadyFetchedOrderitemEntity = (_orderitemEntity != null);
			_alreadyFetchedPriceLevelItemEntity = (_priceLevelItemEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedTaxTariffEntity = (_taxTariffEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationId);
					break;
				case "AlterationitemEntity":
					toReturn.Add(Relations.AlterationitemEntityUsingAlterationitemId);
					break;
				case "OrderitemEntity":
					toReturn.Add(Relations.OrderitemEntityUsingOrderitemId);
					break;
				case "PriceLevelItemEntity":
					toReturn.Add(Relations.PriceLevelItemEntityUsingPriceLevelItemId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingAlterationoptionProductId);
					break;
				case "TaxTariffEntity":
					toReturn.Add(Relations.TaxTariffEntityUsingTaxTariffId);
					break;
				case "OrderitemAlterationitemTagCollection":
					toReturn.Add(Relations.OrderitemAlterationitemTagEntityUsingOrderitemAlterationitemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_orderitemAlterationitemTagCollection", (!this.MarkedForDeletion?_orderitemAlterationitemTagCollection:null));
			info.AddValue("_alwaysFetchOrderitemAlterationitemTagCollection", _alwaysFetchOrderitemAlterationitemTagCollection);
			info.AddValue("_alreadyFetchedOrderitemAlterationitemTagCollection", _alreadyFetchedOrderitemAlterationitemTagCollection);
			info.AddValue("_alterationEntity", (!this.MarkedForDeletion?_alterationEntity:null));
			info.AddValue("_alterationEntityReturnsNewIfNotFound", _alterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationEntity", _alwaysFetchAlterationEntity);
			info.AddValue("_alreadyFetchedAlterationEntity", _alreadyFetchedAlterationEntity);
			info.AddValue("_alterationitemEntity", (!this.MarkedForDeletion?_alterationitemEntity:null));
			info.AddValue("_alterationitemEntityReturnsNewIfNotFound", _alterationitemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationitemEntity", _alwaysFetchAlterationitemEntity);
			info.AddValue("_alreadyFetchedAlterationitemEntity", _alreadyFetchedAlterationitemEntity);
			info.AddValue("_orderitemEntity", (!this.MarkedForDeletion?_orderitemEntity:null));
			info.AddValue("_orderitemEntityReturnsNewIfNotFound", _orderitemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderitemEntity", _alwaysFetchOrderitemEntity);
			info.AddValue("_alreadyFetchedOrderitemEntity", _alreadyFetchedOrderitemEntity);
			info.AddValue("_priceLevelItemEntity", (!this.MarkedForDeletion?_priceLevelItemEntity:null));
			info.AddValue("_priceLevelItemEntityReturnsNewIfNotFound", _priceLevelItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceLevelItemEntity", _alwaysFetchPriceLevelItemEntity);
			info.AddValue("_alreadyFetchedPriceLevelItemEntity", _alreadyFetchedPriceLevelItemEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_taxTariffEntity", (!this.MarkedForDeletion?_taxTariffEntity:null));
			info.AddValue("_taxTariffEntityReturnsNewIfNotFound", _taxTariffEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTaxTariffEntity", _alwaysFetchTaxTariffEntity);
			info.AddValue("_alreadyFetchedTaxTariffEntity", _alreadyFetchedTaxTariffEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationEntity":
					_alreadyFetchedAlterationEntity = true;
					this.AlterationEntity = (AlterationEntity)entity;
					break;
				case "AlterationitemEntity":
					_alreadyFetchedAlterationitemEntity = true;
					this.AlterationitemEntity = (AlterationitemEntity)entity;
					break;
				case "OrderitemEntity":
					_alreadyFetchedOrderitemEntity = true;
					this.OrderitemEntity = (OrderitemEntity)entity;
					break;
				case "PriceLevelItemEntity":
					_alreadyFetchedPriceLevelItemEntity = true;
					this.PriceLevelItemEntity = (PriceLevelItemEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "TaxTariffEntity":
					_alreadyFetchedTaxTariffEntity = true;
					this.TaxTariffEntity = (TaxTariffEntity)entity;
					break;
				case "OrderitemAlterationitemTagCollection":
					_alreadyFetchedOrderitemAlterationitemTagCollection = true;
					if(entity!=null)
					{
						this.OrderitemAlterationitemTagCollection.Add((OrderitemAlterationitemTagEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					SetupSyncAlterationEntity(relatedEntity);
					break;
				case "AlterationitemEntity":
					SetupSyncAlterationitemEntity(relatedEntity);
					break;
				case "OrderitemEntity":
					SetupSyncOrderitemEntity(relatedEntity);
					break;
				case "PriceLevelItemEntity":
					SetupSyncPriceLevelItemEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "TaxTariffEntity":
					SetupSyncTaxTariffEntity(relatedEntity);
					break;
				case "OrderitemAlterationitemTagCollection":
					_orderitemAlterationitemTagCollection.Add((OrderitemAlterationitemTagEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationEntity":
					DesetupSyncAlterationEntity(false, true);
					break;
				case "AlterationitemEntity":
					DesetupSyncAlterationitemEntity(false, true);
					break;
				case "OrderitemEntity":
					DesetupSyncOrderitemEntity(false, true);
					break;
				case "PriceLevelItemEntity":
					DesetupSyncPriceLevelItemEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "TaxTariffEntity":
					DesetupSyncTaxTariffEntity(false, true);
					break;
				case "OrderitemAlterationitemTagCollection":
					this.PerformRelatedEntityRemoval(_orderitemAlterationitemTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_alterationEntity!=null)
			{
				toReturn.Add(_alterationEntity);
			}
			if(_alterationitemEntity!=null)
			{
				toReturn.Add(_alterationitemEntity);
			}
			if(_orderitemEntity!=null)
			{
				toReturn.Add(_orderitemEntity);
			}
			if(_priceLevelItemEntity!=null)
			{
				toReturn.Add(_priceLevelItemEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_taxTariffEntity!=null)
			{
				toReturn.Add(_taxTariffEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_orderitemAlterationitemTagCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemAlterationitemId)
		{
			return FetchUsingPK(orderitemAlterationitemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemAlterationitemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderitemAlterationitemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemAlterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderitemAlterationitemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemAlterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderitemAlterationitemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderitemAlterationitemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderitemAlterationitemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, _orderitemAlterationitemTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, _orderitemAlterationitemTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemAlterationitemTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection GetMultiOrderitemAlterationitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemAlterationitemTagCollection || forceFetch || _alwaysFetchOrderitemAlterationitemTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemAlterationitemTagCollection);
				_orderitemAlterationitemTagCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemAlterationitemTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemAlterationitemTagCollection.GetMultiManyToOne(null, this, null, filter);
				_orderitemAlterationitemTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemAlterationitemTagCollection = true;
			}
			return _orderitemAlterationitemTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemAlterationitemTagCollection'. These settings will be taken into account
		/// when the property OrderitemAlterationitemTagCollection is requested or GetMultiOrderitemAlterationitemTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemAlterationitemTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemAlterationitemTagCollection.SortClauses=sortClauses;
			_orderitemAlterationitemTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleAlterationEntity()
		{
			return GetSingleAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationEntity || forceFetch || _alwaysFetchAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationEntity = newEntity;
				_alreadyFetchedAlterationEntity = fetchResult;
			}
			return _alterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'AlterationitemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationitemEntity' which is related to this entity.</returns>
		public AlterationitemEntity GetSingleAlterationitemEntity()
		{
			return GetSingleAlterationitemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationitemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationitemEntity' which is related to this entity.</returns>
		public virtual AlterationitemEntity GetSingleAlterationitemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationitemEntity || forceFetch || _alwaysFetchAlterationitemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationitemEntityUsingAlterationitemId);
				AlterationitemEntity newEntity = new AlterationitemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationitemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationitemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationitemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationitemEntity = newEntity;
				_alreadyFetchedAlterationitemEntity = fetchResult;
			}
			return _alterationitemEntity;
		}


		/// <summary> Retrieves the related entity of type 'OrderitemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderitemEntity' which is related to this entity.</returns>
		public OrderitemEntity GetSingleOrderitemEntity()
		{
			return GetSingleOrderitemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderitemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderitemEntity' which is related to this entity.</returns>
		public virtual OrderitemEntity GetSingleOrderitemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderitemEntity || forceFetch || _alwaysFetchOrderitemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderitemEntityUsingOrderitemId);
				OrderitemEntity newEntity = new OrderitemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderitemId);
				}
				if(fetchResult)
				{
					newEntity = (OrderitemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderitemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderitemEntity = newEntity;
				_alreadyFetchedOrderitemEntity = fetchResult;
			}
			return _orderitemEntity;
		}


		/// <summary> Retrieves the related entity of type 'PriceLevelItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceLevelItemEntity' which is related to this entity.</returns>
		public PriceLevelItemEntity GetSinglePriceLevelItemEntity()
		{
			return GetSinglePriceLevelItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceLevelItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceLevelItemEntity' which is related to this entity.</returns>
		public virtual PriceLevelItemEntity GetSinglePriceLevelItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceLevelItemEntity || forceFetch || _alwaysFetchPriceLevelItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceLevelItemEntityUsingPriceLevelItemId);
				PriceLevelItemEntity newEntity = new PriceLevelItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceLevelItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PriceLevelItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceLevelItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceLevelItemEntity = newEntity;
				_alreadyFetchedPriceLevelItemEntity = fetchResult;
			}
			return _priceLevelItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingAlterationoptionProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationoptionProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'TaxTariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TaxTariffEntity' which is related to this entity.</returns>
		public TaxTariffEntity GetSingleTaxTariffEntity()
		{
			return GetSingleTaxTariffEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TaxTariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TaxTariffEntity' which is related to this entity.</returns>
		public virtual TaxTariffEntity GetSingleTaxTariffEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTaxTariffEntity || forceFetch || _alwaysFetchTaxTariffEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TaxTariffEntityUsingTaxTariffId);
				TaxTariffEntity newEntity = new TaxTariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TaxTariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TaxTariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_taxTariffEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TaxTariffEntity = newEntity;
				_alreadyFetchedTaxTariffEntity = fetchResult;
			}
			return _taxTariffEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationEntity", _alterationEntity);
			toReturn.Add("AlterationitemEntity", _alterationitemEntity);
			toReturn.Add("OrderitemEntity", _orderitemEntity);
			toReturn.Add("PriceLevelItemEntity", _priceLevelItemEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("TaxTariffEntity", _taxTariffEntity);
			toReturn.Add("OrderitemAlterationitemTagCollection", _orderitemAlterationitemTagCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="validator">The validator object for this OrderitemAlterationitemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 orderitemAlterationitemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderitemAlterationitemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_orderitemAlterationitemTagCollection = new Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection();
			_orderitemAlterationitemTagCollection.SetContainingEntityInfo(this, "OrderitemAlterationitemEntity");
			_alterationEntityReturnsNewIfNotFound = true;
			_alterationitemEntityReturnsNewIfNotFound = true;
			_orderitemEntityReturnsNewIfNotFound = true;
			_priceLevelItemEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_taxTariffEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderitemAlterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionPriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormerAlterationitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Guid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Time", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Value", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelLog", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderLevelEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceInTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceExTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceTotalInTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceTotalExTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxPercentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Tax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxTariffId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxTotal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalIdentifier", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _alterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.AlterationEntityUsingAlterationIdStatic, true, signalRelatedEntity, "OrderitemAlterationitemCollection", resetFKFields, new int[] { (int)OrderitemAlterationitemFieldIndex.AlterationId } );		
			_alterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationEntity(IEntityCore relatedEntity)
		{
			if(_alterationEntity!=relatedEntity)
			{		
				DesetupSyncAlterationEntity(true, true);
				_alterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationEntity, new PropertyChangedEventHandler( OnAlterationEntityPropertyChanged ), "AlterationEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.AlterationEntityUsingAlterationIdStatic, true, ref _alreadyFetchedAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alterationitemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationitemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationitemEntity, new PropertyChangedEventHandler( OnAlterationitemEntityPropertyChanged ), "AlterationitemEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.AlterationitemEntityUsingAlterationitemIdStatic, true, signalRelatedEntity, "OrderitemAlterationitemCollection", resetFKFields, new int[] { (int)OrderitemAlterationitemFieldIndex.AlterationitemId } );		
			_alterationitemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationitemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationitemEntity(IEntityCore relatedEntity)
		{
			if(_alterationitemEntity!=relatedEntity)
			{		
				DesetupSyncAlterationitemEntity(true, true);
				_alterationitemEntity = (AlterationitemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationitemEntity, new PropertyChangedEventHandler( OnAlterationitemEntityPropertyChanged ), "AlterationitemEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.AlterationitemEntityUsingAlterationitemIdStatic, true, ref _alreadyFetchedAlterationitemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationitemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderitemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderitemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderitemEntity, new PropertyChangedEventHandler( OnOrderitemEntityPropertyChanged ), "OrderitemEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.OrderitemEntityUsingOrderitemIdStatic, true, signalRelatedEntity, "OrderitemAlterationitemCollection", resetFKFields, new int[] { (int)OrderitemAlterationitemFieldIndex.OrderitemId } );		
			_orderitemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderitemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderitemEntity(IEntityCore relatedEntity)
		{
			if(_orderitemEntity!=relatedEntity)
			{		
				DesetupSyncOrderitemEntity(true, true);
				_orderitemEntity = (OrderitemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderitemEntity, new PropertyChangedEventHandler( OnOrderitemEntityPropertyChanged ), "OrderitemEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.OrderitemEntityUsingOrderitemIdStatic, true, ref _alreadyFetchedOrderitemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderitemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceLevelItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceLevelItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceLevelItemEntity, new PropertyChangedEventHandler( OnPriceLevelItemEntityPropertyChanged ), "PriceLevelItemEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.PriceLevelItemEntityUsingPriceLevelItemIdStatic, true, signalRelatedEntity, "OrderitemAlterationitemCollection", resetFKFields, new int[] { (int)OrderitemAlterationitemFieldIndex.PriceLevelItemId } );		
			_priceLevelItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceLevelItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceLevelItemEntity(IEntityCore relatedEntity)
		{
			if(_priceLevelItemEntity!=relatedEntity)
			{		
				DesetupSyncPriceLevelItemEntity(true, true);
				_priceLevelItemEntity = (PriceLevelItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceLevelItemEntity, new PropertyChangedEventHandler( OnPriceLevelItemEntityPropertyChanged ), "PriceLevelItemEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.PriceLevelItemEntityUsingPriceLevelItemIdStatic, true, ref _alreadyFetchedPriceLevelItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceLevelItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.ProductEntityUsingAlterationoptionProductIdStatic, true, signalRelatedEntity, "OrderitemAlterationitemCollection", resetFKFields, new int[] { (int)OrderitemAlterationitemFieldIndex.AlterationoptionProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.ProductEntityUsingAlterationoptionProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _taxTariffEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTaxTariffEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _taxTariffEntity, new PropertyChangedEventHandler( OnTaxTariffEntityPropertyChanged ), "TaxTariffEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.TaxTariffEntityUsingTaxTariffIdStatic, true, signalRelatedEntity, "OrderitemAlterationitemCollection", resetFKFields, new int[] { (int)OrderitemAlterationitemFieldIndex.TaxTariffId } );		
			_taxTariffEntity = null;
		}
		
		/// <summary> setups the sync logic for member _taxTariffEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTaxTariffEntity(IEntityCore relatedEntity)
		{
			if(_taxTariffEntity!=relatedEntity)
			{		
				DesetupSyncTaxTariffEntity(true, true);
				_taxTariffEntity = (TaxTariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _taxTariffEntity, new PropertyChangedEventHandler( OnTaxTariffEntityPropertyChanged ), "TaxTariffEntity", Obymobi.Data.RelationClasses.StaticOrderitemAlterationitemRelations.TaxTariffEntityUsingTaxTariffIdStatic, true, ref _alreadyFetchedTaxTariffEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTaxTariffEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 orderitemAlterationitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderitemAlterationitemFieldIndex.OrderitemAlterationitemId].ForcedCurrentValueWrite(orderitemAlterationitemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderitemAlterationitemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderitemAlterationitemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderitemAlterationitemRelations Relations
		{
			get	{ return new OrderitemAlterationitemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemAlterationitemTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection(), (IEntityRelation)GetRelationsForField("OrderitemAlterationitemTagCollection")[0], (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, (int)Obymobi.Data.EntityType.OrderitemAlterationitemTagEntity, 0, null, null, null, "OrderitemAlterationitemTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationEntity")[0], (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationitem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationitemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationitemCollection(), (IEntityRelation)GetRelationsForField("AlterationitemEntity")[0], (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, (int)Obymobi.Data.EntityType.AlterationitemEntity, 0, null, null, null, "AlterationitemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Orderitem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemEntity")[0], (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, (int)Obymobi.Data.EntityType.OrderitemEntity, 0, null, null, null, "OrderitemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceLevelItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceLevelItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceLevelItemCollection(), (IEntityRelation)GetRelationsForField("PriceLevelItemEntity")[0], (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, (int)Obymobi.Data.EntityType.PriceLevelItemEntity, 0, null, null, null, "PriceLevelItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TaxTariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTaxTariffEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TaxTariffCollection(), (IEntityRelation)GetRelationsForField("TaxTariffEntity")[0], (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, (int)Obymobi.Data.EntityType.TaxTariffEntity, 0, null, null, null, "TaxTariffEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OrderitemAlterationitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."OrderitemAlterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderitemAlterationitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.OrderitemAlterationitemId, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.OrderitemAlterationitemId, value, true); }
		}

		/// <summary> The OrderitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."OrderitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.OrderitemId, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.OrderitemId, value, true); }
		}

		/// <summary> The AlterationitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationitemId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationitemId, value, true); }
		}

		/// <summary> The AlterationName property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlterationName
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationName, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationName, value, true); }
		}

		/// <summary> The AlterationoptionName property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlterationoptionName
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionName, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionName, value, true); }
		}

		/// <summary> The AlterationoptionPriceIn property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionPriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal AlterationoptionPriceIn
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionPriceIn, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionPriceIn, value, true); }
		}

		/// <summary> The FormerAlterationitemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."FormerAlterationitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FormerAlterationitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.FormerAlterationitemId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.FormerAlterationitemId, value, true); }
		}

		/// <summary> The Guid property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Guid"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Guid, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Time property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Time"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Time
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.Time, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Time, value, true); }
		}

		/// <summary> The AlterationType property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationType
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationType, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationType, value, true); }
		}

		/// <summary> The Value property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Value"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Value
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.Value, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Value, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The AlterationoptionType property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationoptionType
		{
			get { return (System.Int32)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionType, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionType, value, true); }
		}

		/// <summary> The PriceLevelItemId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceLevelItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceLevelItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelItemId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelItemId, value, true); }
		}

		/// <summary> The PriceLevelLog property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceLevelLog"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PriceLevelLog
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelLog, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceLevelLog, value, true); }
		}

		/// <summary> The AlterationId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The TimeUTC property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemAlterationitemFieldIndex.TimeUTC, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TimeUTC, value, true); }
		}

		/// <summary> The OrderLevelEnabled property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."OrderLevelEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderLevelEnabled
		{
			get { return (System.Boolean)GetValue((int)OrderitemAlterationitemFieldIndex.OrderLevelEnabled, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.OrderLevelEnabled, value, true); }
		}

		/// <summary> The PriceInTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceInTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceInTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceInTax, value, true); }
		}

		/// <summary> The PriceExTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceExTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceExTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceExTax, value, true); }
		}

		/// <summary> The PriceTotalInTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceTotalInTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalInTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalInTax, value, true); }
		}

		/// <summary> The PriceTotalExTax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."PriceTotalExTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalExTax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.PriceTotalExTax, value, true); }
		}

		/// <summary> The TaxPercentage property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxPercentage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double TaxPercentage
		{
			get { return (System.Double)GetValue((int)OrderitemAlterationitemFieldIndex.TaxPercentage, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxPercentage, value, true); }
		}

		/// <summary> The Tax property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."Tax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Tax
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.Tax, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.Tax, value, true); }
		}

		/// <summary> The TaxTariffId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxTariffId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxTariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.TaxTariffId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxTariffId, value, true); }
		}

		/// <summary> The TaxTotal property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxTotal"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TaxTotal
		{
			get { return (System.Decimal)GetValue((int)OrderitemAlterationitemFieldIndex.TaxTotal, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxTotal, value, true); }
		}

		/// <summary> The TaxName property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."TaxName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxName
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.TaxName, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.TaxName, value, true); }
		}

		/// <summary> The AlterationoptionProductId property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."AlterationoptionProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AlterationoptionProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionProductId, false); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.AlterationoptionProductId, value, true); }
		}

		/// <summary> The ExternalIdentifier property of the Entity OrderitemAlterationitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderitemAlterationitem"."ExternalIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalIdentifier
		{
			get { return (System.String)GetValue((int)OrderitemAlterationitemFieldIndex.ExternalIdentifier, true); }
			set	{ SetValue((int)OrderitemAlterationitemFieldIndex.ExternalIdentifier, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemAlterationitemTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemTagCollection OrderitemAlterationitemTagCollection
		{
			get	{ return GetMultiOrderitemAlterationitemTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemAlterationitemTagCollection. When set to true, OrderitemAlterationitemTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemAlterationitemTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemAlterationitemTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemAlterationitemTagCollection
		{
			get	{ return _alwaysFetchOrderitemAlterationitemTagCollection; }
			set	{ _alwaysFetchOrderitemAlterationitemTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemAlterationitemTagCollection already has been fetched. Setting this property to false when OrderitemAlterationitemTagCollection has been fetched
		/// will clear the OrderitemAlterationitemTagCollection collection well. Setting this property to true while OrderitemAlterationitemTagCollection hasn't been fetched disables lazy loading for OrderitemAlterationitemTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemAlterationitemTagCollection
		{
			get { return _alreadyFetchedOrderitemAlterationitemTagCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemAlterationitemTagCollection && !value && (_orderitemAlterationitemTagCollection != null))
				{
					_orderitemAlterationitemTagCollection.Clear();
				}
				_alreadyFetchedOrderitemAlterationitemTagCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity AlterationEntity
		{
			get	{ return GetSingleAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemAlterationitemCollection", "AlterationEntity", _alterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationEntity. When set to true, AlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationEntity
		{
			get	{ return _alwaysFetchAlterationEntity; }
			set	{ _alwaysFetchAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationEntity already has been fetched. Setting this property to false when AlterationEntity has been fetched
		/// will set AlterationEntity to null as well. Setting this property to true while AlterationEntity hasn't been fetched disables lazy loading for AlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationEntity
		{
			get { return _alreadyFetchedAlterationEntity;}
			set 
			{
				if(_alreadyFetchedAlterationEntity && !value)
				{
					this.AlterationEntity = null;
				}
				_alreadyFetchedAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationEntity is not found
		/// in the database. When set to true, AlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationEntityReturnsNewIfNotFound
		{
			get	{ return _alterationEntityReturnsNewIfNotFound; }
			set { _alterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AlterationitemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationitemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationitemEntity AlterationitemEntity
		{
			get	{ return GetSingleAlterationitemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationitemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemAlterationitemCollection", "AlterationitemEntity", _alterationitemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationitemEntity. When set to true, AlterationitemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationitemEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationitemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationitemEntity
		{
			get	{ return _alwaysFetchAlterationitemEntity; }
			set	{ _alwaysFetchAlterationitemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationitemEntity already has been fetched. Setting this property to false when AlterationitemEntity has been fetched
		/// will set AlterationitemEntity to null as well. Setting this property to true while AlterationitemEntity hasn't been fetched disables lazy loading for AlterationitemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationitemEntity
		{
			get { return _alreadyFetchedAlterationitemEntity;}
			set 
			{
				if(_alreadyFetchedAlterationitemEntity && !value)
				{
					this.AlterationitemEntity = null;
				}
				_alreadyFetchedAlterationitemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationitemEntity is not found
		/// in the database. When set to true, AlterationitemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationitemEntityReturnsNewIfNotFound
		{
			get	{ return _alterationitemEntityReturnsNewIfNotFound; }
			set { _alterationitemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderitemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderitemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderitemEntity OrderitemEntity
		{
			get	{ return GetSingleOrderitemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderitemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemAlterationitemCollection", "OrderitemEntity", _orderitemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemEntity. When set to true, OrderitemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderitemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemEntity
		{
			get	{ return _alwaysFetchOrderitemEntity; }
			set	{ _alwaysFetchOrderitemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemEntity already has been fetched. Setting this property to false when OrderitemEntity has been fetched
		/// will set OrderitemEntity to null as well. Setting this property to true while OrderitemEntity hasn't been fetched disables lazy loading for OrderitemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemEntity
		{
			get { return _alreadyFetchedOrderitemEntity;}
			set 
			{
				if(_alreadyFetchedOrderitemEntity && !value)
				{
					this.OrderitemEntity = null;
				}
				_alreadyFetchedOrderitemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderitemEntity is not found
		/// in the database. When set to true, OrderitemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderitemEntityReturnsNewIfNotFound
		{
			get	{ return _orderitemEntityReturnsNewIfNotFound; }
			set { _orderitemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PriceLevelItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceLevelItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceLevelItemEntity PriceLevelItemEntity
		{
			get	{ return GetSinglePriceLevelItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceLevelItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemAlterationitemCollection", "PriceLevelItemEntity", _priceLevelItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceLevelItemEntity. When set to true, PriceLevelItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceLevelItemEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceLevelItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceLevelItemEntity
		{
			get	{ return _alwaysFetchPriceLevelItemEntity; }
			set	{ _alwaysFetchPriceLevelItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceLevelItemEntity already has been fetched. Setting this property to false when PriceLevelItemEntity has been fetched
		/// will set PriceLevelItemEntity to null as well. Setting this property to true while PriceLevelItemEntity hasn't been fetched disables lazy loading for PriceLevelItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceLevelItemEntity
		{
			get { return _alreadyFetchedPriceLevelItemEntity;}
			set 
			{
				if(_alreadyFetchedPriceLevelItemEntity && !value)
				{
					this.PriceLevelItemEntity = null;
				}
				_alreadyFetchedPriceLevelItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceLevelItemEntity is not found
		/// in the database. When set to true, PriceLevelItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceLevelItemEntityReturnsNewIfNotFound
		{
			get	{ return _priceLevelItemEntityReturnsNewIfNotFound; }
			set { _priceLevelItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemAlterationitemCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TaxTariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTaxTariffEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TaxTariffEntity TaxTariffEntity
		{
			get	{ return GetSingleTaxTariffEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTaxTariffEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemAlterationitemCollection", "TaxTariffEntity", _taxTariffEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TaxTariffEntity. When set to true, TaxTariffEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TaxTariffEntity is accessed. You can always execute a forced fetch by calling GetSingleTaxTariffEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTaxTariffEntity
		{
			get	{ return _alwaysFetchTaxTariffEntity; }
			set	{ _alwaysFetchTaxTariffEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TaxTariffEntity already has been fetched. Setting this property to false when TaxTariffEntity has been fetched
		/// will set TaxTariffEntity to null as well. Setting this property to true while TaxTariffEntity hasn't been fetched disables lazy loading for TaxTariffEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTaxTariffEntity
		{
			get { return _alreadyFetchedTaxTariffEntity;}
			set 
			{
				if(_alreadyFetchedTaxTariffEntity && !value)
				{
					this.TaxTariffEntity = null;
				}
				_alreadyFetchedTaxTariffEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TaxTariffEntity is not found
		/// in the database. When set to true, TaxTariffEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TaxTariffEntityReturnsNewIfNotFound
		{
			get	{ return _taxTariffEntityReturnsNewIfNotFound; }
			set { _taxTariffEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
