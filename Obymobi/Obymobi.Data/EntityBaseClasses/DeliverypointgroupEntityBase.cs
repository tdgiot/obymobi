﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Deliverypointgroup'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeliverypointgroupEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "DeliverypointgroupEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection	_announcementCollection;
		private bool	_alwaysFetchAnnouncementCollection, _alreadyFetchedAnnouncementCollection;
		private Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection	_checkoutMethodDeliverypointgroupCollection;
		private bool	_alwaysFetchCheckoutMethodDeliverypointgroupCollection, _alreadyFetchedCheckoutMethodDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.ClientCollection	_clientCollection;
		private bool	_alwaysFetchClientCollection, _alreadyFetchedClientCollection;
		private Obymobi.Data.CollectionClasses.ClientConfigurationCollection	_clientConfigurationCollection;
		private bool	_alwaysFetchClientConfigurationCollection, _alreadyFetchedClientConfigurationCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection	_deliverypointCollection;
		private bool	_alwaysFetchDeliverypointCollection, _alreadyFetchedDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection	_deliverypointgroupAdvertisementCollection;
		private bool	_alwaysFetchDeliverypointgroupAdvertisementCollection, _alreadyFetchedDeliverypointgroupAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection	_deliverypointgroupAnnouncementCollection;
		private bool	_alwaysFetchDeliverypointgroupAnnouncementCollection, _alreadyFetchedDeliverypointgroupAnnouncementCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection	_deliverypointgroupEntertainmentCollection;
		private bool	_alwaysFetchDeliverypointgroupEntertainmentCollection, _alreadyFetchedDeliverypointgroupEntertainmentCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection	_deliverypointgroupLanguageCollection;
		private bool	_alwaysFetchDeliverypointgroupLanguageCollection, _alreadyFetchedDeliverypointgroupLanguageCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection	_deliverypointgroupOccupancyCollection;
		private bool	_alwaysFetchDeliverypointgroupOccupancyCollection, _alreadyFetchedDeliverypointgroupOccupancyCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection	_deliverypointgroupProductCollection;
		private bool	_alwaysFetchDeliverypointgroupProductCollection, _alreadyFetchedDeliverypointgroupProductCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_netmessageCollection;
		private bool	_alwaysFetchNetmessageCollection, _alreadyFetchedNetmessageCollection;
		private Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection	_serviceMethodDeliverypointgroupCollection;
		private bool	_alwaysFetchServiceMethodDeliverypointgroupCollection, _alreadyFetchedServiceMethodDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.SetupCodeCollection	_setupCodeCollection;
		private bool	_alwaysFetchSetupCodeCollection, _alreadyFetchedSetupCodeCollection;
		private Obymobi.Data.CollectionClasses.TerminalCollection	_terminalCollection;
		private bool	_alwaysFetchTerminalCollection, _alreadyFetchedTerminalCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleCollection	_uIScheduleCollection;
		private bool	_alwaysFetchUIScheduleCollection, _alreadyFetchedUIScheduleCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementCollection _advertisementCollectionViaDeliverypointgroupAdvertisement;
		private bool	_alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement, _alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAdvertisement;
		private bool	_alwaysFetchCategoryCollectionViaAdvertisement, _alreadyFetchedCategoryCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAnnouncement;
		private bool	_alwaysFetchCategoryCollectionViaAnnouncement, _alreadyFetchedCategoryCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAnnouncement_;
		private bool	_alwaysFetchCategoryCollectionViaAnnouncement_, _alreadyFetchedCategoryCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ClientCollection _clientCollectionViaNetmessage;
		private bool	_alwaysFetchClientCollectionViaNetmessage, _alreadyFetchedClientCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaAdvertisement;
		private bool	_alwaysFetchCompanyCollectionViaAdvertisement, _alreadyFetchedCompanyCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaAnnouncement;
		private bool	_alwaysFetchCompanyCollectionViaAnnouncement, _alreadyFetchedCompanyCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaNetmessage;
		private bool	_alwaysFetchCompanyCollectionViaNetmessage, _alreadyFetchedCompanyCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaNetmessage_;
		private bool	_alwaysFetchCompanyCollectionViaNetmessage_, _alreadyFetchedCompanyCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaTerminal;
		private bool	_alwaysFetchCompanyCollectionViaTerminal, _alreadyFetchedCompanyCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaNetmessage;
		private bool	_alwaysFetchCustomerCollectionViaNetmessage, _alreadyFetchedCustomerCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.CustomerCollection _customerCollectionViaNetmessage_;
		private bool	_alwaysFetchCustomerCollectionViaNetmessage_, _alreadyFetchedCustomerCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaClient;
		private bool	_alwaysFetchDeliverypointCollectionViaClient, _alreadyFetchedDeliverypointCollectionViaClient;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaClient_;
		private bool	_alwaysFetchDeliverypointCollectionViaClient_, _alreadyFetchedDeliverypointCollectionViaClient_;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaNetmessage;
		private bool	_alwaysFetchDeliverypointCollectionViaNetmessage, _alreadyFetchedDeliverypointCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaNetmessage_;
		private bool	_alwaysFetchDeliverypointCollectionViaNetmessage_, _alreadyFetchedDeliverypointCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal, _alreadyFetchedDeliverypointCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal_;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal_, _alreadyFetchedDeliverypointCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaClient;
		private bool	_alwaysFetchDeviceCollectionViaClient, _alreadyFetchedDeviceCollectionViaClient;
		private Obymobi.Data.CollectionClasses.DeviceCollection _deviceCollectionViaTerminal;
		private bool	_alwaysFetchDeviceCollectionViaTerminal, _alreadyFetchedDeviceCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal, _alreadyFetchedEntertainmentCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal_;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal_, _alreadyFetchedEntertainmentCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal__;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal__, _alreadyFetchedEntertainmentCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement, _alreadyFetchedEntertainmentCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement_;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement_, _alreadyFetchedEntertainmentCollectionViaAdvertisement_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAnnouncement;
		private bool	_alwaysFetchEntertainmentCollectionViaAnnouncement, _alreadyFetchedEntertainmentCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaDeliverypointgroupEntertainment;
		private bool	_alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment, _alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement, _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAnnouncement;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement, _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAnnouncement_;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_, _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaAdvertisement;
		private bool	_alwaysFetchGenericproductCollectionViaAdvertisement, _alreadyFetchedGenericproductCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection _icrtouchprintermappingCollectionViaTerminal;
		private bool	_alwaysFetchIcrtouchprintermappingCollectionViaTerminal, _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaAnnouncement;
		private bool	_alwaysFetchMediaCollectionViaAnnouncement, _alreadyFetchedMediaCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAnnouncement;
		private bool	_alwaysFetchProductCollectionViaAnnouncement, _alreadyFetchedProductCollectionViaAnnouncement;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaDeliverypointgroupProduct;
		private bool	_alwaysFetchProductCollectionViaDeliverypointgroupProduct, _alreadyFetchedProductCollectionViaDeliverypointgroupProduct;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal;
		private bool	_alwaysFetchProductCollectionViaTerminal, _alreadyFetchedProductCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal_;
		private bool	_alwaysFetchProductCollectionViaTerminal_, _alreadyFetchedProductCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal__;
		private bool	_alwaysFetchProductCollectionViaTerminal__, _alreadyFetchedProductCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal___;
		private bool	_alwaysFetchProductCollectionViaTerminal___, _alreadyFetchedProductCollectionViaTerminal___;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaAdvertisement;
		private bool	_alwaysFetchProductCollectionViaAdvertisement, _alreadyFetchedProductCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.SupplierCollection _supplierCollectionViaAdvertisement;
		private bool	_alwaysFetchSupplierCollectionViaAdvertisement, _alreadyFetchedSupplierCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage, _alreadyFetchedTerminalCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage_;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage_, _alreadyFetchedTerminalCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaTerminal;
		private bool	_alwaysFetchTerminalCollectionViaTerminal, _alreadyFetchedTerminalCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaTerminal;
		private bool	_alwaysFetchUIModeCollectionViaTerminal, _alreadyFetchedUIModeCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UserCollection _userCollectionViaTerminal;
		private bool	_alwaysFetchUserCollectionViaTerminal, _alreadyFetchedUserCollectionViaTerminal;
		private AddressEntity _addressEntity;
		private bool	_alwaysFetchAddressEntity, _alreadyFetchedAddressEntity, _addressEntityReturnsNewIfNotFound;
		private AffiliateCampaignEntity _affiliateCampaignEntity;
		private bool	_alwaysFetchAffiliateCampaignEntity, _alreadyFetchedAffiliateCampaignEntity, _affiliateCampaignEntityReturnsNewIfNotFound;
		private AnnouncementEntity _announcementEntity;
		private bool	_alwaysFetchAnnouncementEntity, _alreadyFetchedAnnouncementEntity, _announcementEntityReturnsNewIfNotFound;
		private ClientConfigurationEntity _clientConfigurationEntity;
		private bool	_alwaysFetchClientConfigurationEntity, _alreadyFetchedClientConfigurationEntity, _clientConfigurationEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private MenuEntity _menuEntity;
		private bool	_alwaysFetchMenuEntity, _alreadyFetchedMenuEntity, _menuEntityReturnsNewIfNotFound;
		private PosdeliverypointgroupEntity _posdeliverypointgroupEntity;
		private bool	_alwaysFetchPosdeliverypointgroupEntity, _alreadyFetchedPosdeliverypointgroupEntity, _posdeliverypointgroupEntityReturnsNewIfNotFound;
		private PriceScheduleEntity _priceScheduleEntity;
		private bool	_alwaysFetchPriceScheduleEntity, _alreadyFetchedPriceScheduleEntity, _priceScheduleEntityReturnsNewIfNotFound;
		private ProductEntity _hotSOSBatteryLowProductEntity;
		private bool	_alwaysFetchHotSOSBatteryLowProductEntity, _alreadyFetchedHotSOSBatteryLowProductEntity, _hotSOSBatteryLowProductEntityReturnsNewIfNotFound;
		private RouteEntity _emailDocumentRouteEntity;
		private bool	_alwaysFetchEmailDocumentRouteEntity, _alreadyFetchedEmailDocumentRouteEntity, _emailDocumentRouteEntityReturnsNewIfNotFound;
		private RouteEntity _routeEntity;
		private bool	_alwaysFetchRouteEntity, _alreadyFetchedRouteEntity, _routeEntityReturnsNewIfNotFound;
		private RouteEntity _routeEntity_;
		private bool	_alwaysFetchRouteEntity_, _alreadyFetchedRouteEntity_, _routeEntity_ReturnsNewIfNotFound;
		private RouteEntity _systemMessageRouteEntity;
		private bool	_alwaysFetchSystemMessageRouteEntity, _alreadyFetchedSystemMessageRouteEntity, _systemMessageRouteEntityReturnsNewIfNotFound;
		private TerminalEntity _terminalEntity;
		private bool	_alwaysFetchTerminalEntity, _alreadyFetchedTerminalEntity, _terminalEntityReturnsNewIfNotFound;
		private UIModeEntity _mobileUIModeEntity;
		private bool	_alwaysFetchMobileUIModeEntity, _alreadyFetchedMobileUIModeEntity, _mobileUIModeEntityReturnsNewIfNotFound;
		private UIModeEntity _tabletUIModeEntity;
		private bool	_alwaysFetchTabletUIModeEntity, _alreadyFetchedTabletUIModeEntity, _tabletUIModeEntityReturnsNewIfNotFound;
		private UIModeEntity _uIModeEntity;
		private bool	_alwaysFetchUIModeEntity, _alreadyFetchedUIModeEntity, _uIModeEntityReturnsNewIfNotFound;
		private UIScheduleEntity _uIScheduleEntity;
		private bool	_alwaysFetchUIScheduleEntity, _alreadyFetchedUIScheduleEntity, _uIScheduleEntityReturnsNewIfNotFound;
		private UIThemeEntity _uIThemeEntity;
		private bool	_alwaysFetchUIThemeEntity, _alreadyFetchedUIThemeEntity, _uIThemeEntityReturnsNewIfNotFound;
		private TimestampEntity _timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection, _timestampCollectionReturnsNewIfNotFound;

        // __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AddressEntity</summary>
			public static readonly string AddressEntity = "AddressEntity";
			/// <summary>Member name AffiliateCampaignEntity</summary>
			public static readonly string AffiliateCampaignEntity = "AffiliateCampaignEntity";
			/// <summary>Member name AnnouncementEntity</summary>
			public static readonly string AnnouncementEntity = "AnnouncementEntity";
			/// <summary>Member name ClientConfigurationEntity</summary>
			public static readonly string ClientConfigurationEntity = "ClientConfigurationEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name MenuEntity</summary>
			public static readonly string MenuEntity = "MenuEntity";
			/// <summary>Member name PosdeliverypointgroupEntity</summary>
			public static readonly string PosdeliverypointgroupEntity = "PosdeliverypointgroupEntity";
			/// <summary>Member name PriceScheduleEntity</summary>
			public static readonly string PriceScheduleEntity = "PriceScheduleEntity";
			/// <summary>Member name HotSOSBatteryLowProductEntity</summary>
			public static readonly string HotSOSBatteryLowProductEntity = "HotSOSBatteryLowProductEntity";
			/// <summary>Member name EmailDocumentRouteEntity</summary>
			public static readonly string EmailDocumentRouteEntity = "EmailDocumentRouteEntity";
			/// <summary>Member name RouteEntity</summary>
			public static readonly string RouteEntity = "RouteEntity";
			/// <summary>Member name RouteEntity_</summary>
			public static readonly string RouteEntity_ = "RouteEntity_";
			/// <summary>Member name SystemMessageRouteEntity</summary>
			public static readonly string SystemMessageRouteEntity = "SystemMessageRouteEntity";
			/// <summary>Member name TerminalEntity</summary>
			public static readonly string TerminalEntity = "TerminalEntity";
			/// <summary>Member name MobileUIModeEntity</summary>
			public static readonly string MobileUIModeEntity = "MobileUIModeEntity";
			/// <summary>Member name TabletUIModeEntity</summary>
			public static readonly string TabletUIModeEntity = "TabletUIModeEntity";
			/// <summary>Member name UIModeEntity</summary>
			public static readonly string UIModeEntity = "UIModeEntity";
			/// <summary>Member name UIScheduleEntity</summary>
			public static readonly string UIScheduleEntity = "UIScheduleEntity";
			/// <summary>Member name UIThemeEntity</summary>
			public static readonly string UIThemeEntity = "UIThemeEntity";
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name AnnouncementCollection</summary>
			public static readonly string AnnouncementCollection = "AnnouncementCollection";
			/// <summary>Member name CheckoutMethodDeliverypointgroupCollection</summary>
			public static readonly string CheckoutMethodDeliverypointgroupCollection = "CheckoutMethodDeliverypointgroupCollection";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name DeliverypointgroupAdvertisementCollection</summary>
			public static readonly string DeliverypointgroupAdvertisementCollection = "DeliverypointgroupAdvertisementCollection";
			/// <summary>Member name DeliverypointgroupAnnouncementCollection</summary>
			public static readonly string DeliverypointgroupAnnouncementCollection = "DeliverypointgroupAnnouncementCollection";
			/// <summary>Member name DeliverypointgroupEntertainmentCollection</summary>
			public static readonly string DeliverypointgroupEntertainmentCollection = "DeliverypointgroupEntertainmentCollection";
			/// <summary>Member name DeliverypointgroupLanguageCollection</summary>
			public static readonly string DeliverypointgroupLanguageCollection = "DeliverypointgroupLanguageCollection";
			/// <summary>Member name DeliverypointgroupOccupancyCollection</summary>
			public static readonly string DeliverypointgroupOccupancyCollection = "DeliverypointgroupOccupancyCollection";
			/// <summary>Member name DeliverypointgroupProductCollection</summary>
			public static readonly string DeliverypointgroupProductCollection = "DeliverypointgroupProductCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name NetmessageCollection</summary>
			public static readonly string NetmessageCollection = "NetmessageCollection";
			/// <summary>Member name ServiceMethodDeliverypointgroupCollection</summary>
			public static readonly string ServiceMethodDeliverypointgroupCollection = "ServiceMethodDeliverypointgroupCollection";
			/// <summary>Member name SetupCodeCollection</summary>
			public static readonly string SetupCodeCollection = "SetupCodeCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name UIScheduleCollection</summary>
			public static readonly string UIScheduleCollection = "UIScheduleCollection";
			/// <summary>Member name AdvertisementCollectionViaDeliverypointgroupAdvertisement</summary>
			public static readonly string AdvertisementCollectionViaDeliverypointgroupAdvertisement = "AdvertisementCollectionViaDeliverypointgroupAdvertisement";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaAdvertisement</summary>
			public static readonly string CategoryCollectionViaAdvertisement = "CategoryCollectionViaAdvertisement";
			/// <summary>Member name CategoryCollectionViaAnnouncement</summary>
			public static readonly string CategoryCollectionViaAnnouncement = "CategoryCollectionViaAnnouncement";
			/// <summary>Member name CategoryCollectionViaAnnouncement_</summary>
			public static readonly string CategoryCollectionViaAnnouncement_ = "CategoryCollectionViaAnnouncement_";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name ClientCollectionViaNetmessage</summary>
			public static readonly string ClientCollectionViaNetmessage = "ClientCollectionViaNetmessage";
			/// <summary>Member name CompanyCollectionViaAdvertisement</summary>
			public static readonly string CompanyCollectionViaAdvertisement = "CompanyCollectionViaAdvertisement";
			/// <summary>Member name CompanyCollectionViaAnnouncement</summary>
			public static readonly string CompanyCollectionViaAnnouncement = "CompanyCollectionViaAnnouncement";
			/// <summary>Member name CompanyCollectionViaNetmessage</summary>
			public static readonly string CompanyCollectionViaNetmessage = "CompanyCollectionViaNetmessage";
			/// <summary>Member name CompanyCollectionViaNetmessage_</summary>
			public static readonly string CompanyCollectionViaNetmessage_ = "CompanyCollectionViaNetmessage_";
			/// <summary>Member name CompanyCollectionViaTerminal</summary>
			public static readonly string CompanyCollectionViaTerminal = "CompanyCollectionViaTerminal";
			/// <summary>Member name CustomerCollectionViaNetmessage</summary>
			public static readonly string CustomerCollectionViaNetmessage = "CustomerCollectionViaNetmessage";
			/// <summary>Member name CustomerCollectionViaNetmessage_</summary>
			public static readonly string CustomerCollectionViaNetmessage_ = "CustomerCollectionViaNetmessage_";
			/// <summary>Member name DeliverypointCollectionViaClient</summary>
			public static readonly string DeliverypointCollectionViaClient = "DeliverypointCollectionViaClient";
			/// <summary>Member name DeliverypointCollectionViaClient_</summary>
			public static readonly string DeliverypointCollectionViaClient_ = "DeliverypointCollectionViaClient_";
			/// <summary>Member name DeliverypointCollectionViaNetmessage</summary>
			public static readonly string DeliverypointCollectionViaNetmessage = "DeliverypointCollectionViaNetmessage";
			/// <summary>Member name DeliverypointCollectionViaNetmessage_</summary>
			public static readonly string DeliverypointCollectionViaNetmessage_ = "DeliverypointCollectionViaNetmessage_";
			/// <summary>Member name DeliverypointCollectionViaTerminal</summary>
			public static readonly string DeliverypointCollectionViaTerminal = "DeliverypointCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaTerminal_</summary>
			public static readonly string DeliverypointCollectionViaTerminal_ = "DeliverypointCollectionViaTerminal_";
			/// <summary>Member name DeviceCollectionViaClient</summary>
			public static readonly string DeviceCollectionViaClient = "DeviceCollectionViaClient";
			/// <summary>Member name DeviceCollectionViaTerminal</summary>
			public static readonly string DeviceCollectionViaTerminal = "DeviceCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaTerminal</summary>
			public static readonly string EntertainmentCollectionViaTerminal = "EntertainmentCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal_</summary>
			public static readonly string EntertainmentCollectionViaTerminal_ = "EntertainmentCollectionViaTerminal_";
			/// <summary>Member name EntertainmentCollectionViaTerminal__</summary>
			public static readonly string EntertainmentCollectionViaTerminal__ = "EntertainmentCollectionViaTerminal__";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement = "EntertainmentCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement_</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement_ = "EntertainmentCollectionViaAdvertisement_";
			/// <summary>Member name EntertainmentCollectionViaAnnouncement</summary>
			public static readonly string EntertainmentCollectionViaAnnouncement = "EntertainmentCollectionViaAnnouncement";
			/// <summary>Member name EntertainmentCollectionViaDeliverypointgroupEntertainment</summary>
			public static readonly string EntertainmentCollectionViaDeliverypointgroupEntertainment = "EntertainmentCollectionViaDeliverypointgroupEntertainment";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name EntertainmentcategoryCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentcategoryCollectionViaAdvertisement = "EntertainmentcategoryCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentcategoryCollectionViaAnnouncement</summary>
			public static readonly string EntertainmentcategoryCollectionViaAnnouncement = "EntertainmentcategoryCollectionViaAnnouncement";
			/// <summary>Member name EntertainmentcategoryCollectionViaAnnouncement_</summary>
			public static readonly string EntertainmentcategoryCollectionViaAnnouncement_ = "EntertainmentcategoryCollectionViaAnnouncement_";
			/// <summary>Member name GenericproductCollectionViaAdvertisement</summary>
			public static readonly string GenericproductCollectionViaAdvertisement = "GenericproductCollectionViaAdvertisement";
			/// <summary>Member name IcrtouchprintermappingCollectionViaTerminal</summary>
			public static readonly string IcrtouchprintermappingCollectionViaTerminal = "IcrtouchprintermappingCollectionViaTerminal";
			/// <summary>Member name MediaCollectionViaAnnouncement</summary>
			public static readonly string MediaCollectionViaAnnouncement = "MediaCollectionViaAnnouncement";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaAnnouncement</summary>
			public static readonly string ProductCollectionViaAnnouncement = "ProductCollectionViaAnnouncement";
			/// <summary>Member name ProductCollectionViaDeliverypointgroupProduct</summary>
			public static readonly string ProductCollectionViaDeliverypointgroupProduct = "ProductCollectionViaDeliverypointgroupProduct";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaTerminal</summary>
			public static readonly string ProductCollectionViaTerminal = "ProductCollectionViaTerminal";
			/// <summary>Member name ProductCollectionViaTerminal_</summary>
			public static readonly string ProductCollectionViaTerminal_ = "ProductCollectionViaTerminal_";
			/// <summary>Member name ProductCollectionViaTerminal__</summary>
			public static readonly string ProductCollectionViaTerminal__ = "ProductCollectionViaTerminal__";
			/// <summary>Member name ProductCollectionViaTerminal___</summary>
			public static readonly string ProductCollectionViaTerminal___ = "ProductCollectionViaTerminal___";
			/// <summary>Member name ProductCollectionViaAdvertisement</summary>
			public static readonly string ProductCollectionViaAdvertisement = "ProductCollectionViaAdvertisement";
			/// <summary>Member name SupplierCollectionViaAdvertisement</summary>
			public static readonly string SupplierCollectionViaAdvertisement = "SupplierCollectionViaAdvertisement";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
			/// <summary>Member name TerminalCollectionViaNetmessage</summary>
			public static readonly string TerminalCollectionViaNetmessage = "TerminalCollectionViaNetmessage";
			/// <summary>Member name TerminalCollectionViaNetmessage_</summary>
			public static readonly string TerminalCollectionViaNetmessage_ = "TerminalCollectionViaNetmessage_";
			/// <summary>Member name TerminalCollectionViaTerminal</summary>
			public static readonly string TerminalCollectionViaTerminal = "TerminalCollectionViaTerminal";
			/// <summary>Member name UIModeCollectionViaTerminal</summary>
			public static readonly string UIModeCollectionViaTerminal = "UIModeCollectionViaTerminal";
			/// <summary>Member name UserCollectionViaTerminal</summary>
			public static readonly string UserCollectionViaTerminal = "UserCollectionViaTerminal";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeliverypointgroupEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected DeliverypointgroupEntityBase() :base("DeliverypointgroupEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		protected DeliverypointgroupEntityBase(System.Int32 deliverypointgroupId):base("DeliverypointgroupEntity")
		{
			InitClassFetch(deliverypointgroupId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected DeliverypointgroupEntityBase(System.Int32 deliverypointgroupId, IPrefetchPath prefetchPathToUse): base("DeliverypointgroupEntity")
		{
			InitClassFetch(deliverypointgroupId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this DeliverypointgroupEntity</param>
		protected DeliverypointgroupEntityBase(System.Int32 deliverypointgroupId, IValidator validator):base("DeliverypointgroupEntity")
		{
			InitClassFetch(deliverypointgroupId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointgroupEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_announcementCollection = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollection", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollection = info.GetBoolean("_alwaysFetchAnnouncementCollection");
			_alreadyFetchedAnnouncementCollection = info.GetBoolean("_alreadyFetchedAnnouncementCollection");

			_checkoutMethodDeliverypointgroupCollection = (Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection)info.GetValue("_checkoutMethodDeliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection));
			_alwaysFetchCheckoutMethodDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchCheckoutMethodDeliverypointgroupCollection");
			_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedCheckoutMethodDeliverypointgroupCollection");

			_clientCollection = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollection", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollection = info.GetBoolean("_alwaysFetchClientCollection");
			_alreadyFetchedClientCollection = info.GetBoolean("_alreadyFetchedClientCollection");

			_clientConfigurationCollection = (Obymobi.Data.CollectionClasses.ClientConfigurationCollection)info.GetValue("_clientConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.ClientConfigurationCollection));
			_alwaysFetchClientConfigurationCollection = info.GetBoolean("_alwaysFetchClientConfigurationCollection");
			_alreadyFetchedClientConfigurationCollection = info.GetBoolean("_alreadyFetchedClientConfigurationCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_deliverypointCollection = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollection = info.GetBoolean("_alwaysFetchDeliverypointCollection");
			_alreadyFetchedDeliverypointCollection = info.GetBoolean("_alreadyFetchedDeliverypointCollection");

			_deliverypointgroupAdvertisementCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection)info.GetValue("_deliverypointgroupAdvertisementCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection));
			_alwaysFetchDeliverypointgroupAdvertisementCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupAdvertisementCollection");
			_alreadyFetchedDeliverypointgroupAdvertisementCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupAdvertisementCollection");

			_deliverypointgroupAnnouncementCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection)info.GetValue("_deliverypointgroupAnnouncementCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection));
			_alwaysFetchDeliverypointgroupAnnouncementCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupAnnouncementCollection");
			_alreadyFetchedDeliverypointgroupAnnouncementCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupAnnouncementCollection");

			_deliverypointgroupEntertainmentCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection)info.GetValue("_deliverypointgroupEntertainmentCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection));
			_alwaysFetchDeliverypointgroupEntertainmentCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupEntertainmentCollection");
			_alreadyFetchedDeliverypointgroupEntertainmentCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntertainmentCollection");

			_deliverypointgroupLanguageCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection)info.GetValue("_deliverypointgroupLanguageCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection));
			_alwaysFetchDeliverypointgroupLanguageCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupLanguageCollection");
			_alreadyFetchedDeliverypointgroupLanguageCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupLanguageCollection");

			_deliverypointgroupOccupancyCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection)info.GetValue("_deliverypointgroupOccupancyCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection));
			_alwaysFetchDeliverypointgroupOccupancyCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupOccupancyCollection");
			_alreadyFetchedDeliverypointgroupOccupancyCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupOccupancyCollection");

			_deliverypointgroupProductCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection)info.GetValue("_deliverypointgroupProductCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection));
			_alwaysFetchDeliverypointgroupProductCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupProductCollection");
			_alreadyFetchedDeliverypointgroupProductCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupProductCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_netmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_netmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchNetmessageCollection = info.GetBoolean("_alwaysFetchNetmessageCollection");
			_alreadyFetchedNetmessageCollection = info.GetBoolean("_alreadyFetchedNetmessageCollection");

			_serviceMethodDeliverypointgroupCollection = (Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection)info.GetValue("_serviceMethodDeliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection));
			_alwaysFetchServiceMethodDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchServiceMethodDeliverypointgroupCollection");
			_alreadyFetchedServiceMethodDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedServiceMethodDeliverypointgroupCollection");

			_setupCodeCollection = (Obymobi.Data.CollectionClasses.SetupCodeCollection)info.GetValue("_setupCodeCollection", typeof(Obymobi.Data.CollectionClasses.SetupCodeCollection));
			_alwaysFetchSetupCodeCollection = info.GetBoolean("_alwaysFetchSetupCodeCollection");
			_alreadyFetchedSetupCodeCollection = info.GetBoolean("_alreadyFetchedSetupCodeCollection");

			_terminalCollection = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollection", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollection = info.GetBoolean("_alwaysFetchTerminalCollection");
			_alreadyFetchedTerminalCollection = info.GetBoolean("_alreadyFetchedTerminalCollection");

			_uIScheduleCollection = (Obymobi.Data.CollectionClasses.UIScheduleCollection)info.GetValue("_uIScheduleCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleCollection));
			_alwaysFetchUIScheduleCollection = info.GetBoolean("_alwaysFetchUIScheduleCollection");
			_alreadyFetchedUIScheduleCollection = info.GetBoolean("_alreadyFetchedUIScheduleCollection");
			_advertisementCollectionViaDeliverypointgroupAdvertisement = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollectionViaDeliverypointgroupAdvertisement", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement = info.GetBoolean("_alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement");
			_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement = info.GetBoolean("_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement");

			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_categoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchCategoryCollectionViaAdvertisement");
			_alreadyFetchedCategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAdvertisement");

			_categoryCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchCategoryCollectionViaAnnouncement");
			_alreadyFetchedCategoryCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAnnouncement");

			_categoryCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchCategoryCollectionViaAnnouncement_");
			_alreadyFetchedCategoryCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAnnouncement_");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_clientCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollectionViaNetmessage = info.GetBoolean("_alwaysFetchClientCollectionViaNetmessage");
			_alreadyFetchedClientCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedClientCollectionViaNetmessage");

			_companyCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchCompanyCollectionViaAdvertisement");
			_alreadyFetchedCompanyCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedCompanyCollectionViaAdvertisement");

			_companyCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchCompanyCollectionViaAnnouncement");
			_alreadyFetchedCompanyCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedCompanyCollectionViaAnnouncement");

			_companyCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaNetmessage = info.GetBoolean("_alwaysFetchCompanyCollectionViaNetmessage");
			_alreadyFetchedCompanyCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedCompanyCollectionViaNetmessage");

			_companyCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchCompanyCollectionViaNetmessage_");
			_alreadyFetchedCompanyCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaNetmessage_");

			_companyCollectionViaTerminal = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaTerminal = info.GetBoolean("_alwaysFetchCompanyCollectionViaTerminal");
			_alreadyFetchedCompanyCollectionViaTerminal = info.GetBoolean("_alreadyFetchedCompanyCollectionViaTerminal");

			_customerCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaNetmessage = info.GetBoolean("_alwaysFetchCustomerCollectionViaNetmessage");
			_alreadyFetchedCustomerCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedCustomerCollectionViaNetmessage");

			_customerCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.CustomerCollection)info.GetValue("_customerCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.CustomerCollection));
			_alwaysFetchCustomerCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchCustomerCollectionViaNetmessage_");
			_alreadyFetchedCustomerCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedCustomerCollectionViaNetmessage_");

			_deliverypointCollectionViaClient = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaClient = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaClient");
			_alreadyFetchedDeliverypointCollectionViaClient = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaClient");

			_deliverypointCollectionViaClient_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaClient_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaClient_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaClient_");
			_alreadyFetchedDeliverypointCollectionViaClient_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaClient_");

			_deliverypointCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaNetmessage = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaNetmessage");
			_alreadyFetchedDeliverypointCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaNetmessage");

			_deliverypointCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaNetmessage_");
			_alreadyFetchedDeliverypointCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaNetmessage_");

			_deliverypointCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal");
			_alreadyFetchedDeliverypointCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal");

			_deliverypointCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal_");
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal_");

			_deviceCollectionViaClient = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaClient = info.GetBoolean("_alwaysFetchDeviceCollectionViaClient");
			_alreadyFetchedDeviceCollectionViaClient = info.GetBoolean("_alreadyFetchedDeviceCollectionViaClient");

			_deviceCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeviceCollectionViaTerminal");
			_alreadyFetchedDeviceCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeviceCollectionViaTerminal");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentCollectionViaTerminal = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal");
			_alreadyFetchedEntertainmentCollectionViaTerminal = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal");

			_entertainmentCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal_");
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal_");

			_entertainmentCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal__");
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal__");

			_entertainmentCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement");

			_entertainmentCollectionViaAdvertisement_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement_");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement_");

			_entertainmentCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAnnouncement");
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAnnouncement");

			_entertainmentCollectionViaDeliverypointgroupEntertainment = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaDeliverypointgroupEntertainment", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment");
			_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_entertainmentcategoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement");

			_entertainmentcategoryCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement");
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement");

			_entertainmentcategoryCollectionViaAnnouncement_ = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAnnouncement_", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_ = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_");
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_");

			_genericproductCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchGenericproductCollectionViaAdvertisement");
			_alreadyFetchedGenericproductCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaAdvertisement");

			_icrtouchprintermappingCollectionViaTerminal = (Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection)info.GetValue("_icrtouchprintermappingCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection));
			_alwaysFetchIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal");
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal");

			_mediaCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchMediaCollectionViaAnnouncement");
			_alreadyFetchedMediaCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedMediaCollectionViaAnnouncement");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_productCollectionViaAnnouncement = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAnnouncement", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAnnouncement = info.GetBoolean("_alwaysFetchProductCollectionViaAnnouncement");
			_alreadyFetchedProductCollectionViaAnnouncement = info.GetBoolean("_alreadyFetchedProductCollectionViaAnnouncement");

			_productCollectionViaDeliverypointgroupProduct = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaDeliverypointgroupProduct", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaDeliverypointgroupProduct = info.GetBoolean("_alwaysFetchProductCollectionViaDeliverypointgroupProduct");
			_alreadyFetchedProductCollectionViaDeliverypointgroupProduct = info.GetBoolean("_alreadyFetchedProductCollectionViaDeliverypointgroupProduct");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_productCollectionViaTerminal = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal");
			_alreadyFetchedProductCollectionViaTerminal = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal");

			_productCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal_");
			_alreadyFetchedProductCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal_");

			_productCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal__");
			_alreadyFetchedProductCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal__");

			_productCollectionViaTerminal___ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal___", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal___ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal___");
			_alreadyFetchedProductCollectionViaTerminal___ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal___");

			_productCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchProductCollectionViaAdvertisement");
			_alreadyFetchedProductCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedProductCollectionViaAdvertisement");

			_supplierCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.SupplierCollection)info.GetValue("_supplierCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.SupplierCollection));
			_alwaysFetchSupplierCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchSupplierCollectionViaAdvertisement");
			_alreadyFetchedSupplierCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedSupplierCollectionViaAdvertisement");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");

			_terminalCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage");
			_alreadyFetchedTerminalCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage");

			_terminalCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage_");
			_alreadyFetchedTerminalCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage_");

			_terminalCollectionViaTerminal = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaTerminal = info.GetBoolean("_alwaysFetchTerminalCollectionViaTerminal");
			_alreadyFetchedTerminalCollectionViaTerminal = info.GetBoolean("_alreadyFetchedTerminalCollectionViaTerminal");

			_uIModeCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaTerminal = info.GetBoolean("_alwaysFetchUIModeCollectionViaTerminal");
			_alreadyFetchedUIModeCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUIModeCollectionViaTerminal");

			_userCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_userCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchUserCollectionViaTerminal = info.GetBoolean("_alwaysFetchUserCollectionViaTerminal");
			_alreadyFetchedUserCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUserCollectionViaTerminal");
			_addressEntity = (AddressEntity)info.GetValue("_addressEntity", typeof(AddressEntity));
			if(_addressEntity!=null)
			{
				_addressEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_addressEntityReturnsNewIfNotFound = info.GetBoolean("_addressEntityReturnsNewIfNotFound");
			_alwaysFetchAddressEntity = info.GetBoolean("_alwaysFetchAddressEntity");
			_alreadyFetchedAddressEntity = info.GetBoolean("_alreadyFetchedAddressEntity");

			_affiliateCampaignEntity = (AffiliateCampaignEntity)info.GetValue("_affiliateCampaignEntity", typeof(AffiliateCampaignEntity));
			if(_affiliateCampaignEntity!=null)
			{
				_affiliateCampaignEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_affiliateCampaignEntityReturnsNewIfNotFound = info.GetBoolean("_affiliateCampaignEntityReturnsNewIfNotFound");
			_alwaysFetchAffiliateCampaignEntity = info.GetBoolean("_alwaysFetchAffiliateCampaignEntity");
			_alreadyFetchedAffiliateCampaignEntity = info.GetBoolean("_alreadyFetchedAffiliateCampaignEntity");

			_announcementEntity = (AnnouncementEntity)info.GetValue("_announcementEntity", typeof(AnnouncementEntity));
			if(_announcementEntity!=null)
			{
				_announcementEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_announcementEntityReturnsNewIfNotFound = info.GetBoolean("_announcementEntityReturnsNewIfNotFound");
			_alwaysFetchAnnouncementEntity = info.GetBoolean("_alwaysFetchAnnouncementEntity");
			_alreadyFetchedAnnouncementEntity = info.GetBoolean("_alreadyFetchedAnnouncementEntity");

			_clientConfigurationEntity = (ClientConfigurationEntity)info.GetValue("_clientConfigurationEntity", typeof(ClientConfigurationEntity));
			if(_clientConfigurationEntity!=null)
			{
				_clientConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_clientConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchClientConfigurationEntity = info.GetBoolean("_alwaysFetchClientConfigurationEntity");
			_alreadyFetchedClientConfigurationEntity = info.GetBoolean("_alreadyFetchedClientConfigurationEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_menuEntity = (MenuEntity)info.GetValue("_menuEntity", typeof(MenuEntity));
			if(_menuEntity!=null)
			{
				_menuEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_menuEntityReturnsNewIfNotFound = info.GetBoolean("_menuEntityReturnsNewIfNotFound");
			_alwaysFetchMenuEntity = info.GetBoolean("_alwaysFetchMenuEntity");
			_alreadyFetchedMenuEntity = info.GetBoolean("_alreadyFetchedMenuEntity");

			_posdeliverypointgroupEntity = (PosdeliverypointgroupEntity)info.GetValue("_posdeliverypointgroupEntity", typeof(PosdeliverypointgroupEntity));
			if(_posdeliverypointgroupEntity!=null)
			{
				_posdeliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posdeliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_posdeliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchPosdeliverypointgroupEntity = info.GetBoolean("_alwaysFetchPosdeliverypointgroupEntity");
			_alreadyFetchedPosdeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupEntity");

			_priceScheduleEntity = (PriceScheduleEntity)info.GetValue("_priceScheduleEntity", typeof(PriceScheduleEntity));
			if(_priceScheduleEntity!=null)
			{
				_priceScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_priceScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchPriceScheduleEntity = info.GetBoolean("_alwaysFetchPriceScheduleEntity");
			_alreadyFetchedPriceScheduleEntity = info.GetBoolean("_alreadyFetchedPriceScheduleEntity");

			_hotSOSBatteryLowProductEntity = (ProductEntity)info.GetValue("_hotSOSBatteryLowProductEntity", typeof(ProductEntity));
			if(_hotSOSBatteryLowProductEntity!=null)
			{
				_hotSOSBatteryLowProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_hotSOSBatteryLowProductEntityReturnsNewIfNotFound = info.GetBoolean("_hotSOSBatteryLowProductEntityReturnsNewIfNotFound");
			_alwaysFetchHotSOSBatteryLowProductEntity = info.GetBoolean("_alwaysFetchHotSOSBatteryLowProductEntity");
			_alreadyFetchedHotSOSBatteryLowProductEntity = info.GetBoolean("_alreadyFetchedHotSOSBatteryLowProductEntity");

			_emailDocumentRouteEntity = (RouteEntity)info.GetValue("_emailDocumentRouteEntity", typeof(RouteEntity));
			if(_emailDocumentRouteEntity!=null)
			{
				_emailDocumentRouteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_emailDocumentRouteEntityReturnsNewIfNotFound = info.GetBoolean("_emailDocumentRouteEntityReturnsNewIfNotFound");
			_alwaysFetchEmailDocumentRouteEntity = info.GetBoolean("_alwaysFetchEmailDocumentRouteEntity");
			_alreadyFetchedEmailDocumentRouteEntity = info.GetBoolean("_alreadyFetchedEmailDocumentRouteEntity");

			_routeEntity = (RouteEntity)info.GetValue("_routeEntity", typeof(RouteEntity));
			if(_routeEntity!=null)
			{
				_routeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routeEntityReturnsNewIfNotFound = info.GetBoolean("_routeEntityReturnsNewIfNotFound");
			_alwaysFetchRouteEntity = info.GetBoolean("_alwaysFetchRouteEntity");
			_alreadyFetchedRouteEntity = info.GetBoolean("_alreadyFetchedRouteEntity");

			_routeEntity_ = (RouteEntity)info.GetValue("_routeEntity_", typeof(RouteEntity));
			if(_routeEntity_!=null)
			{
				_routeEntity_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routeEntity_ReturnsNewIfNotFound = info.GetBoolean("_routeEntity_ReturnsNewIfNotFound");
			_alwaysFetchRouteEntity_ = info.GetBoolean("_alwaysFetchRouteEntity_");
			_alreadyFetchedRouteEntity_ = info.GetBoolean("_alreadyFetchedRouteEntity_");

			_systemMessageRouteEntity = (RouteEntity)info.GetValue("_systemMessageRouteEntity", typeof(RouteEntity));
			if(_systemMessageRouteEntity!=null)
			{
				_systemMessageRouteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_systemMessageRouteEntityReturnsNewIfNotFound = info.GetBoolean("_systemMessageRouteEntityReturnsNewIfNotFound");
			_alwaysFetchSystemMessageRouteEntity = info.GetBoolean("_alwaysFetchSystemMessageRouteEntity");
			_alreadyFetchedSystemMessageRouteEntity = info.GetBoolean("_alreadyFetchedSystemMessageRouteEntity");

			_terminalEntity = (TerminalEntity)info.GetValue("_terminalEntity", typeof(TerminalEntity));
			if(_terminalEntity!=null)
			{
				_terminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalEntityReturnsNewIfNotFound = info.GetBoolean("_terminalEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalEntity = info.GetBoolean("_alwaysFetchTerminalEntity");
			_alreadyFetchedTerminalEntity = info.GetBoolean("_alreadyFetchedTerminalEntity");

			_mobileUIModeEntity = (UIModeEntity)info.GetValue("_mobileUIModeEntity", typeof(UIModeEntity));
			if(_mobileUIModeEntity!=null)
			{
				_mobileUIModeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mobileUIModeEntityReturnsNewIfNotFound = info.GetBoolean("_mobileUIModeEntityReturnsNewIfNotFound");
			_alwaysFetchMobileUIModeEntity = info.GetBoolean("_alwaysFetchMobileUIModeEntity");
			_alreadyFetchedMobileUIModeEntity = info.GetBoolean("_alreadyFetchedMobileUIModeEntity");

			_tabletUIModeEntity = (UIModeEntity)info.GetValue("_tabletUIModeEntity", typeof(UIModeEntity));
			if(_tabletUIModeEntity!=null)
			{
				_tabletUIModeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tabletUIModeEntityReturnsNewIfNotFound = info.GetBoolean("_tabletUIModeEntityReturnsNewIfNotFound");
			_alwaysFetchTabletUIModeEntity = info.GetBoolean("_alwaysFetchTabletUIModeEntity");
			_alreadyFetchedTabletUIModeEntity = info.GetBoolean("_alreadyFetchedTabletUIModeEntity");

			_uIModeEntity = (UIModeEntity)info.GetValue("_uIModeEntity", typeof(UIModeEntity));
			if(_uIModeEntity!=null)
			{
				_uIModeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIModeEntityReturnsNewIfNotFound = info.GetBoolean("_uIModeEntityReturnsNewIfNotFound");
			_alwaysFetchUIModeEntity = info.GetBoolean("_alwaysFetchUIModeEntity");
			_alreadyFetchedUIModeEntity = info.GetBoolean("_alreadyFetchedUIModeEntity");

			_uIScheduleEntity = (UIScheduleEntity)info.GetValue("_uIScheduleEntity", typeof(UIScheduleEntity));
			if(_uIScheduleEntity!=null)
			{
				_uIScheduleEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIScheduleEntityReturnsNewIfNotFound = info.GetBoolean("_uIScheduleEntityReturnsNewIfNotFound");
			_alwaysFetchUIScheduleEntity = info.GetBoolean("_alwaysFetchUIScheduleEntity");
			_alreadyFetchedUIScheduleEntity = info.GetBoolean("_alreadyFetchedUIScheduleEntity");

			_uIThemeEntity = (UIThemeEntity)info.GetValue("_uIThemeEntity", typeof(UIThemeEntity));
			if(_uIThemeEntity!=null)
			{
				_uIThemeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIThemeEntityReturnsNewIfNotFound = info.GetBoolean("_uIThemeEntityReturnsNewIfNotFound");
			_alwaysFetchUIThemeEntity = info.GetBoolean("_alwaysFetchUIThemeEntity");
			_alreadyFetchedUIThemeEntity = info.GetBoolean("_alreadyFetchedUIThemeEntity");
			_timestampCollection = (TimestampEntity)info.GetValue("_timestampCollection", typeof(TimestampEntity));
			if(_timestampCollection!=null)
			{
				_timestampCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timestampCollectionReturnsNewIfNotFound = info.GetBoolean("_timestampCollectionReturnsNewIfNotFound");
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeliverypointgroupFieldIndex)fieldIndex)
			{
				case DeliverypointgroupFieldIndex.ReorderNotificationAnnouncementId:
					DesetupSyncAnnouncementEntity(true, false);
					_alreadyFetchedAnnouncementEntity = false;
					break;
				case DeliverypointgroupFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case DeliverypointgroupFieldIndex.MenuId:
					DesetupSyncMenuEntity(true, false);
					_alreadyFetchedMenuEntity = false;
					break;
				case DeliverypointgroupFieldIndex.PosdeliverypointgroupId:
					DesetupSyncPosdeliverypointgroupEntity(true, false);
					_alreadyFetchedPosdeliverypointgroupEntity = false;
					break;
				case DeliverypointgroupFieldIndex.RouteId:
					DesetupSyncRouteEntity(true, false);
					_alreadyFetchedRouteEntity = false;
					break;
				case DeliverypointgroupFieldIndex.SystemMessageRouteId:
					DesetupSyncSystemMessageRouteEntity(true, false);
					_alreadyFetchedSystemMessageRouteEntity = false;
					break;
				case DeliverypointgroupFieldIndex.XTerminalId:
					DesetupSyncTerminalEntity(true, false);
					_alreadyFetchedTerminalEntity = false;
					break;
				case DeliverypointgroupFieldIndex.UIModeId:
					DesetupSyncUIModeEntity(true, false);
					_alreadyFetchedUIModeEntity = false;
					break;
				case DeliverypointgroupFieldIndex.MobileUIModeId:
					DesetupSyncMobileUIModeEntity(true, false);
					_alreadyFetchedMobileUIModeEntity = false;
					break;
				case DeliverypointgroupFieldIndex.TabletUIModeId:
					DesetupSyncTabletUIModeEntity(true, false);
					_alreadyFetchedTabletUIModeEntity = false;
					break;
				case DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId:
					DesetupSyncHotSOSBatteryLowProductEntity(true, false);
					_alreadyFetchedHotSOSBatteryLowProductEntity = false;
					break;
				case DeliverypointgroupFieldIndex.EmailDocumentRouteId:
					DesetupSyncEmailDocumentRouteEntity(true, false);
					_alreadyFetchedEmailDocumentRouteEntity = false;
					break;
				case DeliverypointgroupFieldIndex.UIThemeId:
					DesetupSyncUIThemeEntity(true, false);
					_alreadyFetchedUIThemeEntity = false;
					break;
				case DeliverypointgroupFieldIndex.OrderNotesRouteId:
					DesetupSyncRouteEntity_(true, false);
					_alreadyFetchedRouteEntity_ = false;
					break;
				case DeliverypointgroupFieldIndex.UIScheduleId:
					DesetupSyncUIScheduleEntity(true, false);
					_alreadyFetchedUIScheduleEntity = false;
					break;
				case DeliverypointgroupFieldIndex.PriceScheduleId:
					DesetupSyncPriceScheduleEntity(true, false);
					_alreadyFetchedPriceScheduleEntity = false;
					break;
				case DeliverypointgroupFieldIndex.ClientConfigurationId:
					DesetupSyncClientConfigurationEntity(true, false);
					_alreadyFetchedClientConfigurationEntity = false;
					break;
				case DeliverypointgroupFieldIndex.AddressId:
					DesetupSyncAddressEntity(true, false);
					_alreadyFetchedAddressEntity = false;
					break;
				case DeliverypointgroupFieldIndex.AffiliateCampaignId:
					DesetupSyncAffiliateCampaignEntity(true, false);
					_alreadyFetchedAffiliateCampaignEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedAnnouncementCollection = (_announcementCollection.Count > 0);
			_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = (_checkoutMethodDeliverypointgroupCollection.Count > 0);
			_alreadyFetchedClientCollection = (_clientCollection.Count > 0);
			_alreadyFetchedClientConfigurationCollection = (_clientConfigurationCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedDeliverypointCollection = (_deliverypointCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupAdvertisementCollection = (_deliverypointgroupAdvertisementCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupAnnouncementCollection = (_deliverypointgroupAnnouncementCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupEntertainmentCollection = (_deliverypointgroupEntertainmentCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupLanguageCollection = (_deliverypointgroupLanguageCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupOccupancyCollection = (_deliverypointgroupOccupancyCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupProductCollection = (_deliverypointgroupProductCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedNetmessageCollection = (_netmessageCollection.Count > 0);
			_alreadyFetchedServiceMethodDeliverypointgroupCollection = (_serviceMethodDeliverypointgroupCollection.Count > 0);
			_alreadyFetchedSetupCodeCollection = (_setupCodeCollection.Count > 0);
			_alreadyFetchedTerminalCollection = (_terminalCollection.Count > 0);
			_alreadyFetchedUIScheduleCollection = (_uIScheduleCollection.Count > 0);
			_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement = (_advertisementCollectionViaDeliverypointgroupAdvertisement.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaAdvertisement = (_categoryCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedCategoryCollectionViaAnnouncement = (_categoryCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedCategoryCollectionViaAnnouncement_ = (_categoryCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedClientCollectionViaNetmessage = (_clientCollectionViaNetmessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaAdvertisement = (_companyCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedCompanyCollectionViaAnnouncement = (_companyCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedCompanyCollectionViaNetmessage = (_companyCollectionViaNetmessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaNetmessage_ = (_companyCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedCompanyCollectionViaTerminal = (_companyCollectionViaTerminal.Count > 0);
			_alreadyFetchedCustomerCollectionViaNetmessage = (_customerCollectionViaNetmessage.Count > 0);
			_alreadyFetchedCustomerCollectionViaNetmessage_ = (_customerCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaClient = (_deliverypointCollectionViaClient.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaClient_ = (_deliverypointCollectionViaClient_.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaNetmessage = (_deliverypointCollectionViaNetmessage.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaNetmessage_ = (_deliverypointCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal = (_deliverypointCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = (_deliverypointCollectionViaTerminal_.Count > 0);
			_alreadyFetchedDeviceCollectionViaClient = (_deviceCollectionViaClient.Count > 0);
			_alreadyFetchedDeviceCollectionViaTerminal = (_deviceCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal = (_entertainmentCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = (_entertainmentCollectionViaTerminal_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = (_entertainmentCollectionViaTerminal__.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = (_entertainmentCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = (_entertainmentCollectionViaAdvertisement_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAnnouncement = (_entertainmentCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment = (_entertainmentCollectionViaDeliverypointgroupEntertainment.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = (_entertainmentcategoryCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = (_entertainmentcategoryCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = (_entertainmentcategoryCollectionViaAnnouncement_.Count > 0);
			_alreadyFetchedGenericproductCollectionViaAdvertisement = (_genericproductCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = (_icrtouchprintermappingCollectionViaTerminal.Count > 0);
			_alreadyFetchedMediaCollectionViaAnnouncement = (_mediaCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaAnnouncement = (_productCollectionViaAnnouncement.Count > 0);
			_alreadyFetchedProductCollectionViaDeliverypointgroupProduct = (_productCollectionViaDeliverypointgroupProduct.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal = (_productCollectionViaTerminal.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal_ = (_productCollectionViaTerminal_.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal__ = (_productCollectionViaTerminal__.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal___ = (_productCollectionViaTerminal___.Count > 0);
			_alreadyFetchedProductCollectionViaAdvertisement = (_productCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedSupplierCollectionViaAdvertisement = (_supplierCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage = (_terminalCollectionViaNetmessage.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage_ = (_terminalCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedTerminalCollectionViaTerminal = (_terminalCollectionViaTerminal.Count > 0);
			_alreadyFetchedUIModeCollectionViaTerminal = (_uIModeCollectionViaTerminal.Count > 0);
			_alreadyFetchedUserCollectionViaTerminal = (_userCollectionViaTerminal.Count > 0);
			_alreadyFetchedAddressEntity = (_addressEntity != null);
			_alreadyFetchedAffiliateCampaignEntity = (_affiliateCampaignEntity != null);
			_alreadyFetchedAnnouncementEntity = (_announcementEntity != null);
			_alreadyFetchedClientConfigurationEntity = (_clientConfigurationEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedMenuEntity = (_menuEntity != null);
			_alreadyFetchedPosdeliverypointgroupEntity = (_posdeliverypointgroupEntity != null);
			_alreadyFetchedPriceScheduleEntity = (_priceScheduleEntity != null);
			_alreadyFetchedHotSOSBatteryLowProductEntity = (_hotSOSBatteryLowProductEntity != null);
			_alreadyFetchedEmailDocumentRouteEntity = (_emailDocumentRouteEntity != null);
			_alreadyFetchedRouteEntity = (_routeEntity != null);
			_alreadyFetchedRouteEntity_ = (_routeEntity_ != null);
			_alreadyFetchedSystemMessageRouteEntity = (_systemMessageRouteEntity != null);
			_alreadyFetchedTerminalEntity = (_terminalEntity != null);
			_alreadyFetchedMobileUIModeEntity = (_mobileUIModeEntity != null);
			_alreadyFetchedTabletUIModeEntity = (_tabletUIModeEntity != null);
			_alreadyFetchedUIModeEntity = (_uIModeEntity != null);
			_alreadyFetchedUIScheduleEntity = (_uIScheduleEntity != null);
			_alreadyFetchedUIThemeEntity = (_uIThemeEntity != null);
			_alreadyFetchedTimestampCollection = (_timestampCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AddressEntity":
					toReturn.Add(Relations.AddressEntityUsingAddressId);
					break;
				case "AffiliateCampaignEntity":
					toReturn.Add(Relations.AffiliateCampaignEntityUsingAffiliateCampaignId);
					break;
				case "AnnouncementEntity":
					toReturn.Add(Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId);
					break;
				case "ClientConfigurationEntity":
					toReturn.Add(Relations.ClientConfigurationEntityUsingClientConfigurationId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "MenuEntity":
					toReturn.Add(Relations.MenuEntityUsingMenuId);
					break;
				case "PosdeliverypointgroupEntity":
					toReturn.Add(Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId);
					break;
				case "PriceScheduleEntity":
					toReturn.Add(Relations.PriceScheduleEntityUsingPriceScheduleId);
					break;
				case "HotSOSBatteryLowProductEntity":
					toReturn.Add(Relations.ProductEntityUsingHotSOSBatteryLowProductId);
					break;
				case "EmailDocumentRouteEntity":
					toReturn.Add(Relations.RouteEntityUsingEmailDocumentRouteId);
					break;
				case "RouteEntity":
					toReturn.Add(Relations.RouteEntityUsingRouteId);
					break;
				case "RouteEntity_":
					toReturn.Add(Relations.RouteEntityUsingOrderNotesRouteId);
					break;
				case "SystemMessageRouteEntity":
					toReturn.Add(Relations.RouteEntityUsingSystemMessageRouteId);
					break;
				case "TerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingXTerminalId);
					break;
				case "MobileUIModeEntity":
					toReturn.Add(Relations.UIModeEntityUsingMobileUIModeId);
					break;
				case "TabletUIModeEntity":
					toReturn.Add(Relations.UIModeEntityUsingTabletUIModeId);
					break;
				case "UIModeEntity":
					toReturn.Add(Relations.UIModeEntityUsingUIModeId);
					break;
				case "UIScheduleEntity":
					toReturn.Add(Relations.UIScheduleEntityUsingUIScheduleId);
					break;
				case "UIThemeEntity":
					toReturn.Add(Relations.UIThemeEntityUsingUIThemeId);
					break;
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId);
					break;
				case "AnnouncementCollection":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId);
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					toReturn.Add(Relations.CheckoutMethodDeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "ClientCollection":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointGroupId);
					break;
				case "ClientConfigurationCollection":
					toReturn.Add(Relations.ClientConfigurationEntityUsingDeliverypointgroupId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingDeliverypointgroupId);
					break;
				case "DeliverypointCollection":
					toReturn.Add(Relations.DeliverypointEntityUsingDeliverypointgroupId);
					break;
				case "DeliverypointgroupAdvertisementCollection":
					toReturn.Add(Relations.DeliverypointgroupAdvertisementEntityUsingDeliverypointgroupId);
					break;
				case "DeliverypointgroupAnnouncementCollection":
					toReturn.Add(Relations.DeliverypointgroupAnnouncementEntityUsingDeliverypointgroupId);
					break;
				case "DeliverypointgroupEntertainmentCollection":
					toReturn.Add(Relations.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId);
					break;
				case "DeliverypointgroupLanguageCollection":
					toReturn.Add(Relations.DeliverypointgroupLanguageEntityUsingDeliverypointgroupId);
					break;
				case "DeliverypointgroupOccupancyCollection":
					toReturn.Add(Relations.DeliverypointgroupOccupancyEntityUsingDeliverypointgroupId);
					break;
				case "DeliverypointgroupProductCollection":
					toReturn.Add(Relations.DeliverypointgroupProductEntityUsingDeliverypointgroupId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId);
					break;
				case "NetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId);
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					toReturn.Add(Relations.ServiceMethodDeliverypointgroupEntityUsingDeliverypointgroupId);
					break;
				case "SetupCodeCollection":
					toReturn.Add(Relations.SetupCodeEntityUsingDeliverypointgroupId);
					break;
				case "TerminalCollection":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId);
					break;
				case "UIScheduleCollection":
					toReturn.Add(Relations.UIScheduleEntityUsingDeliverypointgroupId);
					break;
				case "AdvertisementCollectionViaDeliverypointgroupAdvertisement":
					toReturn.Add(Relations.DeliverypointgroupAdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "DeliverypointgroupAdvertisement_", JoinHint.None);
					toReturn.Add(DeliverypointgroupAdvertisementEntity.Relations.AdvertisementEntityUsingAdvertisementId, "DeliverypointgroupAdvertisement_", string.Empty, JoinHint.None);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.CategoryEntityUsingActionCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.CategoryEntityUsingOnNoCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.CategoryEntityUsingOnYesCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ClientCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.ClientEntityUsingReceiverClientId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.CompanyEntityUsingCompanyId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.CompanyEntityUsingCompanyId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CompanyEntityUsingReceiverCompanyId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CompanyEntityUsingSenderCompanyId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CustomerEntityUsingReceiverCustomerId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "CustomerCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.CustomerEntityUsingSenderCustomerId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointGroupId, "DeliverypointgroupEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaClient_":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointGroupId, "DeliverypointgroupEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointEntityUsingLastDeliverypointId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointEntityUsingReceiverDeliverypointId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointEntityUsingSenderDeliverypointId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingAltSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeliverypointGroupId, "DeliverypointgroupEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeviceCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser1, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser2, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingCmsPage, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement_":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentEntityUsingOnYesEntertainment, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaDeliverypointgroupEntertainment":
					toReturn.Add(Relations.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "DeliverypointgroupEntertainment_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntertainmentEntity.Relations.EntertainmentEntityUsingEntertainmentId, "DeliverypointgroupEntertainment_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentcategoryEntityUsingOnYesEntertainmentCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement_":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.EntertainmentcategoryEntityUsingOnNoEntertainmentCategory, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.GenericproductEntityUsingGenericproductId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.MediaEntityUsingMediaId, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAnnouncement":
					toReturn.Add(Relations.AnnouncementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Announcement_", JoinHint.None);
					toReturn.Add(AnnouncementEntity.Relations.ProductEntityUsingOnYesProduct, "Announcement_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaDeliverypointgroupProduct":
					toReturn.Add(Relations.DeliverypointgroupProductEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "DeliverypointgroupProduct_", JoinHint.None);
					toReturn.Add(DeliverypointgroupProductEntity.Relations.ProductEntityUsingProductId, "DeliverypointgroupProduct_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingBatteryLowProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingClientDisconnectedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingOrderFailedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal___":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingUnlockDeliverypointProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.ProductEntityUsingProductId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "SupplierCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.SupplierEntityUsingSupplierId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingReceiverTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverDeliverypointgroupId, "DeliverypointgroupEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingSenderTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.TerminalEntityUsingForwardToTerminalId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UIModeEntityUsingUIModeId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UserCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeliverypointgroupId, "DeliverypointgroupEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UserEntityUsingAutomaticSignOnUserId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingDeliverypointgroupId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_announcementCollection", (!this.MarkedForDeletion?_announcementCollection:null));
			info.AddValue("_alwaysFetchAnnouncementCollection", _alwaysFetchAnnouncementCollection);
			info.AddValue("_alreadyFetchedAnnouncementCollection", _alreadyFetchedAnnouncementCollection);
			info.AddValue("_checkoutMethodDeliverypointgroupCollection", (!this.MarkedForDeletion?_checkoutMethodDeliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchCheckoutMethodDeliverypointgroupCollection", _alwaysFetchCheckoutMethodDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedCheckoutMethodDeliverypointgroupCollection", _alreadyFetchedCheckoutMethodDeliverypointgroupCollection);
			info.AddValue("_clientCollection", (!this.MarkedForDeletion?_clientCollection:null));
			info.AddValue("_alwaysFetchClientCollection", _alwaysFetchClientCollection);
			info.AddValue("_alreadyFetchedClientCollection", _alreadyFetchedClientCollection);
			info.AddValue("_clientConfigurationCollection", (!this.MarkedForDeletion?_clientConfigurationCollection:null));
			info.AddValue("_alwaysFetchClientConfigurationCollection", _alwaysFetchClientConfigurationCollection);
			info.AddValue("_alreadyFetchedClientConfigurationCollection", _alreadyFetchedClientConfigurationCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_deliverypointCollection", (!this.MarkedForDeletion?_deliverypointCollection:null));
			info.AddValue("_alwaysFetchDeliverypointCollection", _alwaysFetchDeliverypointCollection);
			info.AddValue("_alreadyFetchedDeliverypointCollection", _alreadyFetchedDeliverypointCollection);
			info.AddValue("_deliverypointgroupAdvertisementCollection", (!this.MarkedForDeletion?_deliverypointgroupAdvertisementCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupAdvertisementCollection", _alwaysFetchDeliverypointgroupAdvertisementCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupAdvertisementCollection", _alreadyFetchedDeliverypointgroupAdvertisementCollection);
			info.AddValue("_deliverypointgroupAnnouncementCollection", (!this.MarkedForDeletion?_deliverypointgroupAnnouncementCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupAnnouncementCollection", _alwaysFetchDeliverypointgroupAnnouncementCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupAnnouncementCollection", _alreadyFetchedDeliverypointgroupAnnouncementCollection);
			info.AddValue("_deliverypointgroupEntertainmentCollection", (!this.MarkedForDeletion?_deliverypointgroupEntertainmentCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupEntertainmentCollection", _alwaysFetchDeliverypointgroupEntertainmentCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntertainmentCollection", _alreadyFetchedDeliverypointgroupEntertainmentCollection);
			info.AddValue("_deliverypointgroupLanguageCollection", (!this.MarkedForDeletion?_deliverypointgroupLanguageCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupLanguageCollection", _alwaysFetchDeliverypointgroupLanguageCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupLanguageCollection", _alreadyFetchedDeliverypointgroupLanguageCollection);
			info.AddValue("_deliverypointgroupOccupancyCollection", (!this.MarkedForDeletion?_deliverypointgroupOccupancyCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupOccupancyCollection", _alwaysFetchDeliverypointgroupOccupancyCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupOccupancyCollection", _alreadyFetchedDeliverypointgroupOccupancyCollection);
			info.AddValue("_deliverypointgroupProductCollection", (!this.MarkedForDeletion?_deliverypointgroupProductCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupProductCollection", _alwaysFetchDeliverypointgroupProductCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupProductCollection", _alreadyFetchedDeliverypointgroupProductCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_netmessageCollection", (!this.MarkedForDeletion?_netmessageCollection:null));
			info.AddValue("_alwaysFetchNetmessageCollection", _alwaysFetchNetmessageCollection);
			info.AddValue("_alreadyFetchedNetmessageCollection", _alreadyFetchedNetmessageCollection);
			info.AddValue("_serviceMethodDeliverypointgroupCollection", (!this.MarkedForDeletion?_serviceMethodDeliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchServiceMethodDeliverypointgroupCollection", _alwaysFetchServiceMethodDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedServiceMethodDeliverypointgroupCollection", _alreadyFetchedServiceMethodDeliverypointgroupCollection);
			info.AddValue("_setupCodeCollection", (!this.MarkedForDeletion?_setupCodeCollection:null));
			info.AddValue("_alwaysFetchSetupCodeCollection", _alwaysFetchSetupCodeCollection);
			info.AddValue("_alreadyFetchedSetupCodeCollection", _alreadyFetchedSetupCodeCollection);
			info.AddValue("_terminalCollection", (!this.MarkedForDeletion?_terminalCollection:null));
			info.AddValue("_alwaysFetchTerminalCollection", _alwaysFetchTerminalCollection);
			info.AddValue("_alreadyFetchedTerminalCollection", _alreadyFetchedTerminalCollection);
			info.AddValue("_uIScheduleCollection", (!this.MarkedForDeletion?_uIScheduleCollection:null));
			info.AddValue("_alwaysFetchUIScheduleCollection", _alwaysFetchUIScheduleCollection);
			info.AddValue("_alreadyFetchedUIScheduleCollection", _alreadyFetchedUIScheduleCollection);
			info.AddValue("_advertisementCollectionViaDeliverypointgroupAdvertisement", (!this.MarkedForDeletion?_advertisementCollectionViaDeliverypointgroupAdvertisement:null));
			info.AddValue("_alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement", _alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement);
			info.AddValue("_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement", _alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_categoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_categoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAdvertisement", _alwaysFetchCategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAdvertisement", _alreadyFetchedCategoryCollectionViaAdvertisement);
			info.AddValue("_categoryCollectionViaAnnouncement", (!this.MarkedForDeletion?_categoryCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAnnouncement", _alwaysFetchCategoryCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAnnouncement", _alreadyFetchedCategoryCollectionViaAnnouncement);
			info.AddValue("_categoryCollectionViaAnnouncement_", (!this.MarkedForDeletion?_categoryCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAnnouncement_", _alwaysFetchCategoryCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAnnouncement_", _alreadyFetchedCategoryCollectionViaAnnouncement_);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_clientCollectionViaNetmessage", (!this.MarkedForDeletion?_clientCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchClientCollectionViaNetmessage", _alwaysFetchClientCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedClientCollectionViaNetmessage", _alreadyFetchedClientCollectionViaNetmessage);
			info.AddValue("_companyCollectionViaAdvertisement", (!this.MarkedForDeletion?_companyCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaAdvertisement", _alwaysFetchCompanyCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedCompanyCollectionViaAdvertisement", _alreadyFetchedCompanyCollectionViaAdvertisement);
			info.AddValue("_companyCollectionViaAnnouncement", (!this.MarkedForDeletion?_companyCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaAnnouncement", _alwaysFetchCompanyCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedCompanyCollectionViaAnnouncement", _alreadyFetchedCompanyCollectionViaAnnouncement);
			info.AddValue("_companyCollectionViaNetmessage", (!this.MarkedForDeletion?_companyCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaNetmessage", _alwaysFetchCompanyCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedCompanyCollectionViaNetmessage", _alreadyFetchedCompanyCollectionViaNetmessage);
			info.AddValue("_companyCollectionViaNetmessage_", (!this.MarkedForDeletion?_companyCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaNetmessage_", _alwaysFetchCompanyCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedCompanyCollectionViaNetmessage_", _alreadyFetchedCompanyCollectionViaNetmessage_);
			info.AddValue("_companyCollectionViaTerminal", (!this.MarkedForDeletion?_companyCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaTerminal", _alwaysFetchCompanyCollectionViaTerminal);
			info.AddValue("_alreadyFetchedCompanyCollectionViaTerminal", _alreadyFetchedCompanyCollectionViaTerminal);
			info.AddValue("_customerCollectionViaNetmessage", (!this.MarkedForDeletion?_customerCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaNetmessage", _alwaysFetchCustomerCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedCustomerCollectionViaNetmessage", _alreadyFetchedCustomerCollectionViaNetmessage);
			info.AddValue("_customerCollectionViaNetmessage_", (!this.MarkedForDeletion?_customerCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchCustomerCollectionViaNetmessage_", _alwaysFetchCustomerCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedCustomerCollectionViaNetmessage_", _alreadyFetchedCustomerCollectionViaNetmessage_);
			info.AddValue("_deliverypointCollectionViaClient", (!this.MarkedForDeletion?_deliverypointCollectionViaClient:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaClient", _alwaysFetchDeliverypointCollectionViaClient);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaClient", _alreadyFetchedDeliverypointCollectionViaClient);
			info.AddValue("_deliverypointCollectionViaClient_", (!this.MarkedForDeletion?_deliverypointCollectionViaClient_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaClient_", _alwaysFetchDeliverypointCollectionViaClient_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaClient_", _alreadyFetchedDeliverypointCollectionViaClient_);
			info.AddValue("_deliverypointCollectionViaNetmessage", (!this.MarkedForDeletion?_deliverypointCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaNetmessage", _alwaysFetchDeliverypointCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaNetmessage", _alreadyFetchedDeliverypointCollectionViaNetmessage);
			info.AddValue("_deliverypointCollectionViaNetmessage_", (!this.MarkedForDeletion?_deliverypointCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaNetmessage_", _alwaysFetchDeliverypointCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaNetmessage_", _alreadyFetchedDeliverypointCollectionViaNetmessage_);
			info.AddValue("_deliverypointCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal", _alwaysFetchDeliverypointCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal", _alreadyFetchedDeliverypointCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaTerminal_", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal_", _alwaysFetchDeliverypointCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal_", _alreadyFetchedDeliverypointCollectionViaTerminal_);
			info.AddValue("_deviceCollectionViaClient", (!this.MarkedForDeletion?_deviceCollectionViaClient:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaClient", _alwaysFetchDeviceCollectionViaClient);
			info.AddValue("_alreadyFetchedDeviceCollectionViaClient", _alreadyFetchedDeviceCollectionViaClient);
			info.AddValue("_deviceCollectionViaTerminal", (!this.MarkedForDeletion?_deviceCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeviceCollectionViaTerminal", _alwaysFetchDeviceCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeviceCollectionViaTerminal", _alreadyFetchedDeviceCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaTerminal", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal", _alwaysFetchEntertainmentCollectionViaTerminal);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal", _alreadyFetchedEntertainmentCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal_", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal_", _alwaysFetchEntertainmentCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal_", _alreadyFetchedEntertainmentCollectionViaTerminal_);
			info.AddValue("_entertainmentCollectionViaTerminal__", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal__", _alwaysFetchEntertainmentCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal__", _alreadyFetchedEntertainmentCollectionViaTerminal__);
			info.AddValue("_entertainmentCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement", _alwaysFetchEntertainmentCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement", _alreadyFetchedEntertainmentCollectionViaAdvertisement);
			info.AddValue("_entertainmentCollectionViaAdvertisement_", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement_", _alwaysFetchEntertainmentCollectionViaAdvertisement_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement_", _alreadyFetchedEntertainmentCollectionViaAdvertisement_);
			info.AddValue("_entertainmentCollectionViaAnnouncement", (!this.MarkedForDeletion?_entertainmentCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAnnouncement", _alwaysFetchEntertainmentCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAnnouncement", _alreadyFetchedEntertainmentCollectionViaAnnouncement);
			info.AddValue("_entertainmentCollectionViaDeliverypointgroupEntertainment", (!this.MarkedForDeletion?_entertainmentCollectionViaDeliverypointgroupEntertainment:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment", _alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment", _alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_entertainmentcategoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement", _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement", _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement);
			info.AddValue("_entertainmentcategoryCollectionViaAnnouncement", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement", _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement", _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement);
			info.AddValue("_entertainmentcategoryCollectionViaAnnouncement_", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAnnouncement_:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_", _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_", _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_);
			info.AddValue("_genericproductCollectionViaAdvertisement", (!this.MarkedForDeletion?_genericproductCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaAdvertisement", _alwaysFetchGenericproductCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaAdvertisement", _alreadyFetchedGenericproductCollectionViaAdvertisement);
			info.AddValue("_icrtouchprintermappingCollectionViaTerminal", (!this.MarkedForDeletion?_icrtouchprintermappingCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal", _alwaysFetchIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal", _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_mediaCollectionViaAnnouncement", (!this.MarkedForDeletion?_mediaCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchMediaCollectionViaAnnouncement", _alwaysFetchMediaCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedMediaCollectionViaAnnouncement", _alreadyFetchedMediaCollectionViaAnnouncement);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_productCollectionViaAnnouncement", (!this.MarkedForDeletion?_productCollectionViaAnnouncement:null));
			info.AddValue("_alwaysFetchProductCollectionViaAnnouncement", _alwaysFetchProductCollectionViaAnnouncement);
			info.AddValue("_alreadyFetchedProductCollectionViaAnnouncement", _alreadyFetchedProductCollectionViaAnnouncement);
			info.AddValue("_productCollectionViaDeliverypointgroupProduct", (!this.MarkedForDeletion?_productCollectionViaDeliverypointgroupProduct:null));
			info.AddValue("_alwaysFetchProductCollectionViaDeliverypointgroupProduct", _alwaysFetchProductCollectionViaDeliverypointgroupProduct);
			info.AddValue("_alreadyFetchedProductCollectionViaDeliverypointgroupProduct", _alreadyFetchedProductCollectionViaDeliverypointgroupProduct);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_productCollectionViaTerminal", (!this.MarkedForDeletion?_productCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal", _alwaysFetchProductCollectionViaTerminal);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal", _alreadyFetchedProductCollectionViaTerminal);
			info.AddValue("_productCollectionViaTerminal_", (!this.MarkedForDeletion?_productCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal_", _alwaysFetchProductCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal_", _alreadyFetchedProductCollectionViaTerminal_);
			info.AddValue("_productCollectionViaTerminal__", (!this.MarkedForDeletion?_productCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal__", _alwaysFetchProductCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal__", _alreadyFetchedProductCollectionViaTerminal__);
			info.AddValue("_productCollectionViaTerminal___", (!this.MarkedForDeletion?_productCollectionViaTerminal___:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal___", _alwaysFetchProductCollectionViaTerminal___);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal___", _alreadyFetchedProductCollectionViaTerminal___);
			info.AddValue("_productCollectionViaAdvertisement", (!this.MarkedForDeletion?_productCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchProductCollectionViaAdvertisement", _alwaysFetchProductCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedProductCollectionViaAdvertisement", _alreadyFetchedProductCollectionViaAdvertisement);
			info.AddValue("_supplierCollectionViaAdvertisement", (!this.MarkedForDeletion?_supplierCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchSupplierCollectionViaAdvertisement", _alwaysFetchSupplierCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedSupplierCollectionViaAdvertisement", _alreadyFetchedSupplierCollectionViaAdvertisement);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);
			info.AddValue("_terminalCollectionViaNetmessage", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage", _alwaysFetchTerminalCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage", _alreadyFetchedTerminalCollectionViaNetmessage);
			info.AddValue("_terminalCollectionViaNetmessage_", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage_", _alwaysFetchTerminalCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage_", _alreadyFetchedTerminalCollectionViaNetmessage_);
			info.AddValue("_terminalCollectionViaTerminal", (!this.MarkedForDeletion?_terminalCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaTerminal", _alwaysFetchTerminalCollectionViaTerminal);
			info.AddValue("_alreadyFetchedTerminalCollectionViaTerminal", _alreadyFetchedTerminalCollectionViaTerminal);
			info.AddValue("_uIModeCollectionViaTerminal", (!this.MarkedForDeletion?_uIModeCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaTerminal", _alwaysFetchUIModeCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUIModeCollectionViaTerminal", _alreadyFetchedUIModeCollectionViaTerminal);
			info.AddValue("_userCollectionViaTerminal", (!this.MarkedForDeletion?_userCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUserCollectionViaTerminal", _alwaysFetchUserCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUserCollectionViaTerminal", _alreadyFetchedUserCollectionViaTerminal);
			info.AddValue("_addressEntity", (!this.MarkedForDeletion?_addressEntity:null));
			info.AddValue("_addressEntityReturnsNewIfNotFound", _addressEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAddressEntity", _alwaysFetchAddressEntity);
			info.AddValue("_alreadyFetchedAddressEntity", _alreadyFetchedAddressEntity);
			info.AddValue("_affiliateCampaignEntity", (!this.MarkedForDeletion?_affiliateCampaignEntity:null));
			info.AddValue("_affiliateCampaignEntityReturnsNewIfNotFound", _affiliateCampaignEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAffiliateCampaignEntity", _alwaysFetchAffiliateCampaignEntity);
			info.AddValue("_alreadyFetchedAffiliateCampaignEntity", _alreadyFetchedAffiliateCampaignEntity);
			info.AddValue("_announcementEntity", (!this.MarkedForDeletion?_announcementEntity:null));
			info.AddValue("_announcementEntityReturnsNewIfNotFound", _announcementEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAnnouncementEntity", _alwaysFetchAnnouncementEntity);
			info.AddValue("_alreadyFetchedAnnouncementEntity", _alreadyFetchedAnnouncementEntity);
			info.AddValue("_clientConfigurationEntity", (!this.MarkedForDeletion?_clientConfigurationEntity:null));
			info.AddValue("_clientConfigurationEntityReturnsNewIfNotFound", _clientConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientConfigurationEntity", _alwaysFetchClientConfigurationEntity);
			info.AddValue("_alreadyFetchedClientConfigurationEntity", _alreadyFetchedClientConfigurationEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_menuEntity", (!this.MarkedForDeletion?_menuEntity:null));
			info.AddValue("_menuEntityReturnsNewIfNotFound", _menuEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMenuEntity", _alwaysFetchMenuEntity);
			info.AddValue("_alreadyFetchedMenuEntity", _alreadyFetchedMenuEntity);
			info.AddValue("_posdeliverypointgroupEntity", (!this.MarkedForDeletion?_posdeliverypointgroupEntity:null));
			info.AddValue("_posdeliverypointgroupEntityReturnsNewIfNotFound", _posdeliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosdeliverypointgroupEntity", _alwaysFetchPosdeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupEntity", _alreadyFetchedPosdeliverypointgroupEntity);
			info.AddValue("_priceScheduleEntity", (!this.MarkedForDeletion?_priceScheduleEntity:null));
			info.AddValue("_priceScheduleEntityReturnsNewIfNotFound", _priceScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceScheduleEntity", _alwaysFetchPriceScheduleEntity);
			info.AddValue("_alreadyFetchedPriceScheduleEntity", _alreadyFetchedPriceScheduleEntity);
			info.AddValue("_hotSOSBatteryLowProductEntity", (!this.MarkedForDeletion?_hotSOSBatteryLowProductEntity:null));
			info.AddValue("_hotSOSBatteryLowProductEntityReturnsNewIfNotFound", _hotSOSBatteryLowProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchHotSOSBatteryLowProductEntity", _alwaysFetchHotSOSBatteryLowProductEntity);
			info.AddValue("_alreadyFetchedHotSOSBatteryLowProductEntity", _alreadyFetchedHotSOSBatteryLowProductEntity);
			info.AddValue("_emailDocumentRouteEntity", (!this.MarkedForDeletion?_emailDocumentRouteEntity:null));
			info.AddValue("_emailDocumentRouteEntityReturnsNewIfNotFound", _emailDocumentRouteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEmailDocumentRouteEntity", _alwaysFetchEmailDocumentRouteEntity);
			info.AddValue("_alreadyFetchedEmailDocumentRouteEntity", _alreadyFetchedEmailDocumentRouteEntity);
			info.AddValue("_routeEntity", (!this.MarkedForDeletion?_routeEntity:null));
			info.AddValue("_routeEntityReturnsNewIfNotFound", _routeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRouteEntity", _alwaysFetchRouteEntity);
			info.AddValue("_alreadyFetchedRouteEntity", _alreadyFetchedRouteEntity);
			info.AddValue("_routeEntity_", (!this.MarkedForDeletion?_routeEntity_:null));
			info.AddValue("_routeEntity_ReturnsNewIfNotFound", _routeEntity_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRouteEntity_", _alwaysFetchRouteEntity_);
			info.AddValue("_alreadyFetchedRouteEntity_", _alreadyFetchedRouteEntity_);
			info.AddValue("_systemMessageRouteEntity", (!this.MarkedForDeletion?_systemMessageRouteEntity:null));
			info.AddValue("_systemMessageRouteEntityReturnsNewIfNotFound", _systemMessageRouteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSystemMessageRouteEntity", _alwaysFetchSystemMessageRouteEntity);
			info.AddValue("_alreadyFetchedSystemMessageRouteEntity", _alreadyFetchedSystemMessageRouteEntity);
			info.AddValue("_terminalEntity", (!this.MarkedForDeletion?_terminalEntity:null));
			info.AddValue("_terminalEntityReturnsNewIfNotFound", _terminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalEntity", _alwaysFetchTerminalEntity);
			info.AddValue("_alreadyFetchedTerminalEntity", _alreadyFetchedTerminalEntity);
			info.AddValue("_mobileUIModeEntity", (!this.MarkedForDeletion?_mobileUIModeEntity:null));
			info.AddValue("_mobileUIModeEntityReturnsNewIfNotFound", _mobileUIModeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMobileUIModeEntity", _alwaysFetchMobileUIModeEntity);
			info.AddValue("_alreadyFetchedMobileUIModeEntity", _alreadyFetchedMobileUIModeEntity);
			info.AddValue("_tabletUIModeEntity", (!this.MarkedForDeletion?_tabletUIModeEntity:null));
			info.AddValue("_tabletUIModeEntityReturnsNewIfNotFound", _tabletUIModeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTabletUIModeEntity", _alwaysFetchTabletUIModeEntity);
			info.AddValue("_alreadyFetchedTabletUIModeEntity", _alreadyFetchedTabletUIModeEntity);
			info.AddValue("_uIModeEntity", (!this.MarkedForDeletion?_uIModeEntity:null));
			info.AddValue("_uIModeEntityReturnsNewIfNotFound", _uIModeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIModeEntity", _alwaysFetchUIModeEntity);
			info.AddValue("_alreadyFetchedUIModeEntity", _alreadyFetchedUIModeEntity);
			info.AddValue("_uIScheduleEntity", (!this.MarkedForDeletion?_uIScheduleEntity:null));
			info.AddValue("_uIScheduleEntityReturnsNewIfNotFound", _uIScheduleEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIScheduleEntity", _alwaysFetchUIScheduleEntity);
			info.AddValue("_alreadyFetchedUIScheduleEntity", _alreadyFetchedUIScheduleEntity);
			info.AddValue("_uIThemeEntity", (!this.MarkedForDeletion?_uIThemeEntity:null));
			info.AddValue("_uIThemeEntityReturnsNewIfNotFound", _uIThemeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIThemeEntity", _alwaysFetchUIThemeEntity);
			info.AddValue("_alreadyFetchedUIThemeEntity", _alreadyFetchedUIThemeEntity);

			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_timestampCollectionReturnsNewIfNotFound", _timestampCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AddressEntity":
					_alreadyFetchedAddressEntity = true;
					this.AddressEntity = (AddressEntity)entity;
					break;
				case "AffiliateCampaignEntity":
					_alreadyFetchedAffiliateCampaignEntity = true;
					this.AffiliateCampaignEntity = (AffiliateCampaignEntity)entity;
					break;
				case "AnnouncementEntity":
					_alreadyFetchedAnnouncementEntity = true;
					this.AnnouncementEntity = (AnnouncementEntity)entity;
					break;
				case "ClientConfigurationEntity":
					_alreadyFetchedClientConfigurationEntity = true;
					this.ClientConfigurationEntity = (ClientConfigurationEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "MenuEntity":
					_alreadyFetchedMenuEntity = true;
					this.MenuEntity = (MenuEntity)entity;
					break;
				case "PosdeliverypointgroupEntity":
					_alreadyFetchedPosdeliverypointgroupEntity = true;
					this.PosdeliverypointgroupEntity = (PosdeliverypointgroupEntity)entity;
					break;
				case "PriceScheduleEntity":
					_alreadyFetchedPriceScheduleEntity = true;
					this.PriceScheduleEntity = (PriceScheduleEntity)entity;
					break;
				case "HotSOSBatteryLowProductEntity":
					_alreadyFetchedHotSOSBatteryLowProductEntity = true;
					this.HotSOSBatteryLowProductEntity = (ProductEntity)entity;
					break;
				case "EmailDocumentRouteEntity":
					_alreadyFetchedEmailDocumentRouteEntity = true;
					this.EmailDocumentRouteEntity = (RouteEntity)entity;
					break;
				case "RouteEntity":
					_alreadyFetchedRouteEntity = true;
					this.RouteEntity = (RouteEntity)entity;
					break;
				case "RouteEntity_":
					_alreadyFetchedRouteEntity_ = true;
					this.RouteEntity_ = (RouteEntity)entity;
					break;
				case "SystemMessageRouteEntity":
					_alreadyFetchedSystemMessageRouteEntity = true;
					this.SystemMessageRouteEntity = (RouteEntity)entity;
					break;
				case "TerminalEntity":
					_alreadyFetchedTerminalEntity = true;
					this.TerminalEntity = (TerminalEntity)entity;
					break;
				case "MobileUIModeEntity":
					_alreadyFetchedMobileUIModeEntity = true;
					this.MobileUIModeEntity = (UIModeEntity)entity;
					break;
				case "TabletUIModeEntity":
					_alreadyFetchedTabletUIModeEntity = true;
					this.TabletUIModeEntity = (UIModeEntity)entity;
					break;
				case "UIModeEntity":
					_alreadyFetchedUIModeEntity = true;
					this.UIModeEntity = (UIModeEntity)entity;
					break;
				case "UIScheduleEntity":
					_alreadyFetchedUIScheduleEntity = true;
					this.UIScheduleEntity = (UIScheduleEntity)entity;
					break;
				case "UIThemeEntity":
					_alreadyFetchedUIThemeEntity = true;
					this.UIThemeEntity = (UIThemeEntity)entity;
					break;
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "AnnouncementCollection":
					_alreadyFetchedAnnouncementCollection = true;
					if(entity!=null)
					{
						this.AnnouncementCollection.Add((AnnouncementEntity)entity);
					}
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.CheckoutMethodDeliverypointgroupCollection.Add((CheckoutMethodDeliverypointgroupEntity)entity);
					}
					break;
				case "ClientCollection":
					_alreadyFetchedClientCollection = true;
					if(entity!=null)
					{
						this.ClientCollection.Add((ClientEntity)entity);
					}
					break;
				case "ClientConfigurationCollection":
					_alreadyFetchedClientConfigurationCollection = true;
					if(entity!=null)
					{
						this.ClientConfigurationCollection.Add((ClientConfigurationEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "DeliverypointCollection":
					_alreadyFetchedDeliverypointCollection = true;
					if(entity!=null)
					{
						this.DeliverypointCollection.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupAdvertisementCollection":
					_alreadyFetchedDeliverypointgroupAdvertisementCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupAdvertisementCollection.Add((DeliverypointgroupAdvertisementEntity)entity);
					}
					break;
				case "DeliverypointgroupAnnouncementCollection":
					_alreadyFetchedDeliverypointgroupAnnouncementCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupAnnouncementCollection.Add((DeliverypointgroupAnnouncementEntity)entity);
					}
					break;
				case "DeliverypointgroupEntertainmentCollection":
					_alreadyFetchedDeliverypointgroupEntertainmentCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupEntertainmentCollection.Add((DeliverypointgroupEntertainmentEntity)entity);
					}
					break;
				case "DeliverypointgroupLanguageCollection":
					_alreadyFetchedDeliverypointgroupLanguageCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupLanguageCollection.Add((DeliverypointgroupLanguageEntity)entity);
					}
					break;
				case "DeliverypointgroupOccupancyCollection":
					_alreadyFetchedDeliverypointgroupOccupancyCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupOccupancyCollection.Add((DeliverypointgroupOccupancyEntity)entity);
					}
					break;
				case "DeliverypointgroupProductCollection":
					_alreadyFetchedDeliverypointgroupProductCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupProductCollection.Add((DeliverypointgroupProductEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "NetmessageCollection":
					_alreadyFetchedNetmessageCollection = true;
					if(entity!=null)
					{
						this.NetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					_alreadyFetchedServiceMethodDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.ServiceMethodDeliverypointgroupCollection.Add((ServiceMethodDeliverypointgroupEntity)entity);
					}
					break;
				case "SetupCodeCollection":
					_alreadyFetchedSetupCodeCollection = true;
					if(entity!=null)
					{
						this.SetupCodeCollection.Add((SetupCodeEntity)entity);
					}
					break;
				case "TerminalCollection":
					_alreadyFetchedTerminalCollection = true;
					if(entity!=null)
					{
						this.TerminalCollection.Add((TerminalEntity)entity);
					}
					break;
				case "UIScheduleCollection":
					_alreadyFetchedUIScheduleCollection = true;
					if(entity!=null)
					{
						this.UIScheduleCollection.Add((UIScheduleEntity)entity);
					}
					break;
				case "AdvertisementCollectionViaDeliverypointgroupAdvertisement":
					_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement = true;
					if(entity!=null)
					{
						this.AdvertisementCollectionViaDeliverypointgroupAdvertisement.Add((AdvertisementEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaAdvertisement":
					_alreadyFetchedCategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAdvertisement.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaAnnouncement":
					_alreadyFetchedCategoryCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAnnouncement.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaAnnouncement_":
					_alreadyFetchedCategoryCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAnnouncement_.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "ClientCollectionViaNetmessage":
					_alreadyFetchedClientCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.ClientCollectionViaNetmessage.Add((ClientEntity)entity);
					}
					break;
				case "CompanyCollectionViaAdvertisement":
					_alreadyFetchedCompanyCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaAdvertisement.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaAnnouncement":
					_alreadyFetchedCompanyCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaAnnouncement.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaNetmessage":
					_alreadyFetchedCompanyCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaNetmessage.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaNetmessage_":
					_alreadyFetchedCompanyCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaNetmessage_.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaTerminal":
					_alreadyFetchedCompanyCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaTerminal.Add((CompanyEntity)entity);
					}
					break;
				case "CustomerCollectionViaNetmessage":
					_alreadyFetchedCustomerCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaNetmessage.Add((CustomerEntity)entity);
					}
					break;
				case "CustomerCollectionViaNetmessage_":
					_alreadyFetchedCustomerCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.CustomerCollectionViaNetmessage_.Add((CustomerEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaClient":
					_alreadyFetchedDeliverypointCollectionViaClient = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaClient.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaClient_":
					_alreadyFetchedDeliverypointCollectionViaClient_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaClient_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaNetmessage":
					_alreadyFetchedDeliverypointCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaNetmessage.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaNetmessage_":
					_alreadyFetchedDeliverypointCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaNetmessage_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal":
					_alreadyFetchedDeliverypointCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal_":
					_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeviceCollectionViaClient":
					_alreadyFetchedDeviceCollectionViaClient = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaClient.Add((DeviceEntity)entity);
					}
					break;
				case "DeviceCollectionViaTerminal":
					_alreadyFetchedDeviceCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeviceCollectionViaTerminal.Add((DeviceEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal":
					_alreadyFetchedEntertainmentCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal_":
					_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal__":
					_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal__.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement_":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAnnouncement":
					_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAnnouncement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaDeliverypointgroupEntertainment":
					_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaDeliverypointgroupEntertainment.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAdvertisement.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement":
					_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAnnouncement.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAnnouncement_":
					_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAnnouncement_.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "GenericproductCollectionViaAdvertisement":
					_alreadyFetchedGenericproductCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaAdvertisement.Add((GenericproductEntity)entity);
					}
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.IcrtouchprintermappingCollectionViaTerminal.Add((IcrtouchprintermappingEntity)entity);
					}
					break;
				case "MediaCollectionViaAnnouncement":
					_alreadyFetchedMediaCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.MediaCollectionViaAnnouncement.Add((MediaEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaAnnouncement":
					_alreadyFetchedProductCollectionViaAnnouncement = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAnnouncement.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaDeliverypointgroupProduct":
					_alreadyFetchedProductCollectionViaDeliverypointgroupProduct = true;
					if(entity!=null)
					{
						this.ProductCollectionViaDeliverypointgroupProduct.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal":
					_alreadyFetchedProductCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal_":
					_alreadyFetchedProductCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal__":
					_alreadyFetchedProductCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal__.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal___":
					_alreadyFetchedProductCollectionViaTerminal___ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal___.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaAdvertisement":
					_alreadyFetchedProductCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.ProductCollectionViaAdvertisement.Add((ProductEntity)entity);
					}
					break;
				case "SupplierCollectionViaAdvertisement":
					_alreadyFetchedSupplierCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.SupplierCollectionViaAdvertisement.Add((SupplierEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage":
					_alreadyFetchedTerminalCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage_":
					_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaTerminal":
					_alreadyFetchedTerminalCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaTerminal.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaTerminal":
					_alreadyFetchedUIModeCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaTerminal.Add((UIModeEntity)entity);
					}
					break;
				case "UserCollectionViaTerminal":
					_alreadyFetchedUserCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UserCollectionViaTerminal.Add((UserEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					this.TimestampCollection = (TimestampEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AddressEntity":
					SetupSyncAddressEntity(relatedEntity);
					break;
				case "AffiliateCampaignEntity":
					SetupSyncAffiliateCampaignEntity(relatedEntity);
					break;
				case "AnnouncementEntity":
					SetupSyncAnnouncementEntity(relatedEntity);
					break;
				case "ClientConfigurationEntity":
					SetupSyncClientConfigurationEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "MenuEntity":
					SetupSyncMenuEntity(relatedEntity);
					break;
				case "PosdeliverypointgroupEntity":
					SetupSyncPosdeliverypointgroupEntity(relatedEntity);
					break;
				case "PriceScheduleEntity":
					SetupSyncPriceScheduleEntity(relatedEntity);
					break;
				case "HotSOSBatteryLowProductEntity":
					SetupSyncHotSOSBatteryLowProductEntity(relatedEntity);
					break;
				case "EmailDocumentRouteEntity":
					SetupSyncEmailDocumentRouteEntity(relatedEntity);
					break;
				case "RouteEntity":
					SetupSyncRouteEntity(relatedEntity);
					break;
				case "RouteEntity_":
					SetupSyncRouteEntity_(relatedEntity);
					break;
				case "SystemMessageRouteEntity":
					SetupSyncSystemMessageRouteEntity(relatedEntity);
					break;
				case "TerminalEntity":
					SetupSyncTerminalEntity(relatedEntity);
					break;
				case "MobileUIModeEntity":
					SetupSyncMobileUIModeEntity(relatedEntity);
					break;
				case "TabletUIModeEntity":
					SetupSyncTabletUIModeEntity(relatedEntity);
					break;
				case "UIModeEntity":
					SetupSyncUIModeEntity(relatedEntity);
					break;
				case "UIScheduleEntity":
					SetupSyncUIScheduleEntity(relatedEntity);
					break;
				case "UIThemeEntity":
					SetupSyncUIThemeEntity(relatedEntity);
					break;
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "AnnouncementCollection":
					_announcementCollection.Add((AnnouncementEntity)relatedEntity);
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					_checkoutMethodDeliverypointgroupCollection.Add((CheckoutMethodDeliverypointgroupEntity)relatedEntity);
					break;
				case "ClientCollection":
					_clientCollection.Add((ClientEntity)relatedEntity);
					break;
				case "ClientConfigurationCollection":
					_clientConfigurationCollection.Add((ClientConfigurationEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "DeliverypointCollection":
					_deliverypointCollection.Add((DeliverypointEntity)relatedEntity);
					break;
				case "DeliverypointgroupAdvertisementCollection":
					_deliverypointgroupAdvertisementCollection.Add((DeliverypointgroupAdvertisementEntity)relatedEntity);
					break;
				case "DeliverypointgroupAnnouncementCollection":
					_deliverypointgroupAnnouncementCollection.Add((DeliverypointgroupAnnouncementEntity)relatedEntity);
					break;
				case "DeliverypointgroupEntertainmentCollection":
					_deliverypointgroupEntertainmentCollection.Add((DeliverypointgroupEntertainmentEntity)relatedEntity);
					break;
				case "DeliverypointgroupLanguageCollection":
					_deliverypointgroupLanguageCollection.Add((DeliverypointgroupLanguageEntity)relatedEntity);
					break;
				case "DeliverypointgroupOccupancyCollection":
					_deliverypointgroupOccupancyCollection.Add((DeliverypointgroupOccupancyEntity)relatedEntity);
					break;
				case "DeliverypointgroupProductCollection":
					_deliverypointgroupProductCollection.Add((DeliverypointgroupProductEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "NetmessageCollection":
					_netmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					_serviceMethodDeliverypointgroupCollection.Add((ServiceMethodDeliverypointgroupEntity)relatedEntity);
					break;
				case "SetupCodeCollection":
					_setupCodeCollection.Add((SetupCodeEntity)relatedEntity);
					break;
				case "TerminalCollection":
					_terminalCollection.Add((TerminalEntity)relatedEntity);
					break;
				case "UIScheduleCollection":
					_uIScheduleCollection.Add((UIScheduleEntity)relatedEntity);
					break;
				case "TimestampCollection":
					SetupSyncTimestampCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AddressEntity":
					DesetupSyncAddressEntity(false, true);
					break;
				case "AffiliateCampaignEntity":
					DesetupSyncAffiliateCampaignEntity(false, true);
					break;
				case "AnnouncementEntity":
					DesetupSyncAnnouncementEntity(false, true);
					break;
				case "ClientConfigurationEntity":
					DesetupSyncClientConfigurationEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "MenuEntity":
					DesetupSyncMenuEntity(false, true);
					break;
				case "PosdeliverypointgroupEntity":
					DesetupSyncPosdeliverypointgroupEntity(false, true);
					break;
				case "PriceScheduleEntity":
					DesetupSyncPriceScheduleEntity(false, true);
					break;
				case "HotSOSBatteryLowProductEntity":
					DesetupSyncHotSOSBatteryLowProductEntity(false, true);
					break;
				case "EmailDocumentRouteEntity":
					DesetupSyncEmailDocumentRouteEntity(false, true);
					break;
				case "RouteEntity":
					DesetupSyncRouteEntity(false, true);
					break;
				case "RouteEntity_":
					DesetupSyncRouteEntity_(false, true);
					break;
				case "SystemMessageRouteEntity":
					DesetupSyncSystemMessageRouteEntity(false, true);
					break;
				case "TerminalEntity":
					DesetupSyncTerminalEntity(false, true);
					break;
				case "MobileUIModeEntity":
					DesetupSyncMobileUIModeEntity(false, true);
					break;
				case "TabletUIModeEntity":
					DesetupSyncTabletUIModeEntity(false, true);
					break;
				case "UIModeEntity":
					DesetupSyncUIModeEntity(false, true);
					break;
				case "UIScheduleEntity":
					DesetupSyncUIScheduleEntity(false, true);
					break;
				case "UIThemeEntity":
					DesetupSyncUIThemeEntity(false, true);
					break;
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AnnouncementCollection":
					this.PerformRelatedEntityRemoval(_announcementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CheckoutMethodDeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_checkoutMethodDeliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientCollection":
					this.PerformRelatedEntityRemoval(_clientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientConfigurationCollection":
					this.PerformRelatedEntityRemoval(_clientConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointCollection":
					this.PerformRelatedEntityRemoval(_deliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupAdvertisementCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupAdvertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupAnnouncementCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupAnnouncementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupEntertainmentCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupEntertainmentCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupLanguageCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupOccupancyCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupOccupancyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupProductCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupProductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NetmessageCollection":
					this.PerformRelatedEntityRemoval(_netmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ServiceMethodDeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_serviceMethodDeliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SetupCodeCollection":
					this.PerformRelatedEntityRemoval(_setupCodeCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalCollection":
					this.PerformRelatedEntityRemoval(_terminalCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					DesetupSyncTimestampCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_timestampCollection!=null)
			{
				toReturn.Add(_timestampCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_addressEntity!=null)
			{
				toReturn.Add(_addressEntity);
			}
			if(_affiliateCampaignEntity!=null)
			{
				toReturn.Add(_affiliateCampaignEntity);
			}
			if(_announcementEntity!=null)
			{
				toReturn.Add(_announcementEntity);
			}
			if(_clientConfigurationEntity!=null)
			{
				toReturn.Add(_clientConfigurationEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_menuEntity!=null)
			{
				toReturn.Add(_menuEntity);
			}
			if(_posdeliverypointgroupEntity!=null)
			{
				toReturn.Add(_posdeliverypointgroupEntity);
			}
			if(_priceScheduleEntity!=null)
			{
				toReturn.Add(_priceScheduleEntity);
			}
			if(_hotSOSBatteryLowProductEntity!=null)
			{
				toReturn.Add(_hotSOSBatteryLowProductEntity);
			}
			if(_emailDocumentRouteEntity!=null)
			{
				toReturn.Add(_emailDocumentRouteEntity);
			}
			if(_routeEntity!=null)
			{
				toReturn.Add(_routeEntity);
			}
			if(_routeEntity_!=null)
			{
				toReturn.Add(_routeEntity_);
			}
			if(_systemMessageRouteEntity!=null)
			{
				toReturn.Add(_systemMessageRouteEntity);
			}
			if(_terminalEntity!=null)
			{
				toReturn.Add(_terminalEntity);
			}
			if(_mobileUIModeEntity!=null)
			{
				toReturn.Add(_mobileUIModeEntity);
			}
			if(_tabletUIModeEntity!=null)
			{
				toReturn.Add(_tabletUIModeEntity);
			}
			if(_uIModeEntity!=null)
			{
				toReturn.Add(_uIModeEntity);
			}
			if(_uIScheduleEntity!=null)
			{
				toReturn.Add(_uIScheduleEntity);
			}
			if(_uIThemeEntity!=null)
			{
				toReturn.Add(_uIThemeEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_announcementCollection);
			toReturn.Add(_checkoutMethodDeliverypointgroupCollection);
			toReturn.Add(_clientCollection);
			toReturn.Add(_clientConfigurationCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_deliverypointCollection);
			toReturn.Add(_deliverypointgroupAdvertisementCollection);
			toReturn.Add(_deliverypointgroupAnnouncementCollection);
			toReturn.Add(_deliverypointgroupEntertainmentCollection);
			toReturn.Add(_deliverypointgroupLanguageCollection);
			toReturn.Add(_deliverypointgroupOccupancyCollection);
			toReturn.Add(_deliverypointgroupProductCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_netmessageCollection);
			toReturn.Add(_serviceMethodDeliverypointgroupCollection);
			toReturn.Add(_setupCodeCollection);
			toReturn.Add(_terminalCollection);
			toReturn.Add(_uIScheduleCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupId)
		{
			return FetchUsingPK(deliverypointgroupId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deliverypointgroupId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deliverypointgroupId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deliverypointgroupId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeliverypointgroupId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeliverypointgroupRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementCollection(forceFetch, _announcementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementCollection || forceFetch || _alwaysFetchAnnouncementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollection);
				_announcementCollection.SuppressClearInGetMulti=!forceFetch;
				_announcementCollection.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, filter);
				_announcementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollection = true;
			}
			return _announcementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollection'. These settings will be taken into account
		/// when the property AnnouncementCollection is requested or GetMultiAnnouncementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollection.SortClauses=sortClauses;
			_announcementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiCheckoutMethodDeliverypointgroupCollection(forceFetch, _checkoutMethodDeliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CheckoutMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCheckoutMethodDeliverypointgroupCollection(forceFetch, _checkoutMethodDeliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCheckoutMethodDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection GetMultiCheckoutMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCheckoutMethodDeliverypointgroupCollection || forceFetch || _alwaysFetchCheckoutMethodDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_checkoutMethodDeliverypointgroupCollection);
				_checkoutMethodDeliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_checkoutMethodDeliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_checkoutMethodDeliverypointgroupCollection.GetMultiManyToOne(null, null, this, filter);
				_checkoutMethodDeliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = true;
			}
			return _checkoutMethodDeliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CheckoutMethodDeliverypointgroupCollection'. These settings will be taken into account
		/// when the property CheckoutMethodDeliverypointgroupCollection is requested or GetMultiCheckoutMethodDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCheckoutMethodDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_checkoutMethodDeliverypointgroupCollection.SortClauses=sortClauses;
			_checkoutMethodDeliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientCollection || forceFetch || _alwaysFetchClientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollection);
				_clientCollection.SuppressClearInGetMulti=!forceFetch;
				_clientCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientCollection.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_clientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollection = true;
			}
			return _clientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollection'. These settings will be taken into account
		/// when the property ClientCollection is requested or GetMultiClientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollection.SortClauses=sortClauses;
			_clientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientConfigurationCollection || forceFetch || _alwaysFetchClientConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientConfigurationCollection);
				_clientConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_clientConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientConfigurationCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, filter);
				_clientConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientConfigurationCollection = true;
			}
			return _clientConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientConfigurationCollection'. These settings will be taken into account
		/// when the property ClientConfigurationCollection is requested or GetMultiClientConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientConfigurationCollection.SortClauses=sortClauses;
			_clientConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointCollection || forceFetch || _alwaysFetchDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollection);
				_deliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollection.GetMultiManyToOne(null, null, this, null, null, null, null, filter);
				_deliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollection = true;
			}
			return _deliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollection'. These settings will be taken into account
		/// when the property DeliverypointCollection is requested or GetMultiDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollection.SortClauses=sortClauses;
			_deliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupAdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection GetMultiDeliverypointgroupAdvertisementCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupAdvertisementCollection(forceFetch, _deliverypointgroupAdvertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupAdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection GetMultiDeliverypointgroupAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupAdvertisementCollection(forceFetch, _deliverypointgroupAdvertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection GetMultiDeliverypointgroupAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection GetMultiDeliverypointgroupAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupAdvertisementCollection || forceFetch || _alwaysFetchDeliverypointgroupAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupAdvertisementCollection);
				_deliverypointgroupAdvertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupAdvertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupAdvertisementCollection.GetMultiManyToOne(null, this, filter);
				_deliverypointgroupAdvertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupAdvertisementCollection = true;
			}
			return _deliverypointgroupAdvertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupAdvertisementCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupAdvertisementCollection is requested or GetMultiDeliverypointgroupAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupAdvertisementCollection.SortClauses=sortClauses;
			_deliverypointgroupAdvertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupAnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupAnnouncementCollection(forceFetch, _deliverypointgroupAnnouncementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupAnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupAnnouncementCollection(forceFetch, _deliverypointgroupAnnouncementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupAnnouncementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection GetMultiDeliverypointgroupAnnouncementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupAnnouncementCollection || forceFetch || _alwaysFetchDeliverypointgroupAnnouncementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupAnnouncementCollection);
				_deliverypointgroupAnnouncementCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupAnnouncementCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupAnnouncementCollection.GetMultiManyToOne(null, this, filter);
				_deliverypointgroupAnnouncementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupAnnouncementCollection = true;
			}
			return _deliverypointgroupAnnouncementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupAnnouncementCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupAnnouncementCollection is requested or GetMultiDeliverypointgroupAnnouncementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupAnnouncementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupAnnouncementCollection.SortClauses=sortClauses;
			_deliverypointgroupAnnouncementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection GetMultiDeliverypointgroupEntertainmentCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupEntertainmentCollection(forceFetch, _deliverypointgroupEntertainmentCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection GetMultiDeliverypointgroupEntertainmentCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupEntertainmentCollection(forceFetch, _deliverypointgroupEntertainmentCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection GetMultiDeliverypointgroupEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupEntertainmentCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntertainmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection GetMultiDeliverypointgroupEntertainmentCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupEntertainmentCollection || forceFetch || _alwaysFetchDeliverypointgroupEntertainmentCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupEntertainmentCollection);
				_deliverypointgroupEntertainmentCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupEntertainmentCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupEntertainmentCollection.GetMultiManyToOne(this, null, filter);
				_deliverypointgroupEntertainmentCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupEntertainmentCollection = true;
			}
			return _deliverypointgroupEntertainmentCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupEntertainmentCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupEntertainmentCollection is requested or GetMultiDeliverypointgroupEntertainmentCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupEntertainmentCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupEntertainmentCollection.SortClauses=sortClauses;
			_deliverypointgroupEntertainmentCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupLanguageCollection(forceFetch, _deliverypointgroupLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupLanguageCollection(forceFetch, _deliverypointgroupLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupLanguageCollection || forceFetch || _alwaysFetchDeliverypointgroupLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupLanguageCollection);
				_deliverypointgroupLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupLanguageCollection.GetMultiManyToOne(this, null, filter);
				_deliverypointgroupLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupLanguageCollection = true;
			}
			return _deliverypointgroupLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupLanguageCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupLanguageCollection is requested or GetMultiDeliverypointgroupLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupLanguageCollection.SortClauses=sortClauses;
			_deliverypointgroupLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupOccupancyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupOccupancyEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection GetMultiDeliverypointgroupOccupancyCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupOccupancyCollection(forceFetch, _deliverypointgroupOccupancyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupOccupancyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupOccupancyEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection GetMultiDeliverypointgroupOccupancyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupOccupancyCollection(forceFetch, _deliverypointgroupOccupancyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupOccupancyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection GetMultiDeliverypointgroupOccupancyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupOccupancyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupOccupancyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection GetMultiDeliverypointgroupOccupancyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupOccupancyCollection || forceFetch || _alwaysFetchDeliverypointgroupOccupancyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupOccupancyCollection);
				_deliverypointgroupOccupancyCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupOccupancyCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupOccupancyCollection.GetMultiManyToOne(this, filter);
				_deliverypointgroupOccupancyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupOccupancyCollection = true;
			}
			return _deliverypointgroupOccupancyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupOccupancyCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupOccupancyCollection is requested or GetMultiDeliverypointgroupOccupancyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupOccupancyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupOccupancyCollection.SortClauses=sortClauses;
			_deliverypointgroupOccupancyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection GetMultiDeliverypointgroupProductCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupProductCollection(forceFetch, _deliverypointgroupProductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection GetMultiDeliverypointgroupProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupProductCollection(forceFetch, _deliverypointgroupProductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection GetMultiDeliverypointgroupProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection GetMultiDeliverypointgroupProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupProductCollection || forceFetch || _alwaysFetchDeliverypointgroupProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupProductCollection);
				_deliverypointgroupProductCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupProductCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupProductCollection.GetMultiManyToOne(this, null, filter);
				_deliverypointgroupProductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupProductCollection = true;
			}
			return _deliverypointgroupProductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupProductCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupProductCollection is requested or GetMultiDeliverypointgroupProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupProductCollection.SortClauses=sortClauses;
			_deliverypointgroupProductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiNetmessageCollection(bool forceFetch)
		{
			return GetMultiNetmessageCollection(forceFetch, _netmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNetmessageCollection(forceFetch, _netmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNetmessageCollection || forceFetch || _alwaysFetchNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_netmessageCollection);
				_netmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_netmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_netmessageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, filter);
				_netmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedNetmessageCollection = true;
			}
			return _netmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'NetmessageCollection'. These settings will be taken into account
		/// when the property NetmessageCollection is requested or GetMultiNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_netmessageCollection.SortClauses=sortClauses;
			_netmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ServiceMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiServiceMethodDeliverypointgroupCollection(forceFetch, _serviceMethodDeliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ServiceMethodDeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiServiceMethodDeliverypointgroupCollection(forceFetch, _serviceMethodDeliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiServiceMethodDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection GetMultiServiceMethodDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedServiceMethodDeliverypointgroupCollection || forceFetch || _alwaysFetchServiceMethodDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_serviceMethodDeliverypointgroupCollection);
				_serviceMethodDeliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_serviceMethodDeliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_serviceMethodDeliverypointgroupCollection.GetMultiManyToOne(null, this, null, filter);
				_serviceMethodDeliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedServiceMethodDeliverypointgroupCollection = true;
			}
			return _serviceMethodDeliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ServiceMethodDeliverypointgroupCollection'. These settings will be taken into account
		/// when the property ServiceMethodDeliverypointgroupCollection is requested or GetMultiServiceMethodDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersServiceMethodDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_serviceMethodDeliverypointgroupCollection.SortClauses=sortClauses;
			_serviceMethodDeliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SetupCodeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SetupCodeEntity'</returns>
		public Obymobi.Data.CollectionClasses.SetupCodeCollection GetMultiSetupCodeCollection(bool forceFetch)
		{
			return GetMultiSetupCodeCollection(forceFetch, _setupCodeCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SetupCodeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SetupCodeEntity'</returns>
		public Obymobi.Data.CollectionClasses.SetupCodeCollection GetMultiSetupCodeCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSetupCodeCollection(forceFetch, _setupCodeCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SetupCodeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SetupCodeCollection GetMultiSetupCodeCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSetupCodeCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SetupCodeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SetupCodeCollection GetMultiSetupCodeCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSetupCodeCollection || forceFetch || _alwaysFetchSetupCodeCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_setupCodeCollection);
				_setupCodeCollection.SuppressClearInGetMulti=!forceFetch;
				_setupCodeCollection.EntityFactoryToUse = entityFactoryToUse;
				_setupCodeCollection.GetMultiManyToOne(null, this, filter);
				_setupCodeCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSetupCodeCollection = true;
			}
			return _setupCodeCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SetupCodeCollection'. These settings will be taken into account
		/// when the property SetupCodeCollection is requested or GetMultiSetupCodeCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSetupCodeCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_setupCodeCollection.SortClauses=sortClauses;
			_setupCodeCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalCollection || forceFetch || _alwaysFetchTerminalCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollection);
				_terminalCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_terminalCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollection = true;
			}
			return _terminalCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollection'. These settings will be taken into account
		/// when the property TerminalCollection is requested or GetMultiTerminalCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollection.SortClauses=sortClauses;
			_terminalCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleCollection GetMultiUIScheduleCollection(bool forceFetch)
		{
			return GetMultiUIScheduleCollection(forceFetch, _uIScheduleCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleCollection GetMultiUIScheduleCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleCollection(forceFetch, _uIScheduleCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleCollection GetMultiUIScheduleCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleCollection GetMultiUIScheduleCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleCollection || forceFetch || _alwaysFetchUIScheduleCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleCollection);
				_uIScheduleCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleCollection.GetMultiManyToOne(null, this, filter);
				_uIScheduleCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleCollection = true;
			}
			return _uIScheduleCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleCollection'. These settings will be taken into account
		/// when the property UIScheduleCollection is requested or GetMultiUIScheduleCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleCollection.SortClauses=sortClauses;
			_uIScheduleCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaDeliverypointgroupAdvertisement(bool forceFetch)
		{
			return GetMultiAdvertisementCollectionViaDeliverypointgroupAdvertisement(forceFetch, _advertisementCollectionViaDeliverypointgroupAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaDeliverypointgroupAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement || forceFetch || _alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollectionViaDeliverypointgroupAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_advertisementCollectionViaDeliverypointgroupAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollectionViaDeliverypointgroupAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollectionViaDeliverypointgroupAdvertisement.GetMulti(filter, GetRelationsForField("AdvertisementCollectionViaDeliverypointgroupAdvertisement"));
				_advertisementCollectionViaDeliverypointgroupAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement = true;
			}
			return _advertisementCollectionViaDeliverypointgroupAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollectionViaDeliverypointgroupAdvertisement'. These settings will be taken into account
		/// when the property AdvertisementCollectionViaDeliverypointgroupAdvertisement is requested or GetMultiAdvertisementCollectionViaDeliverypointgroupAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollectionViaDeliverypointgroupAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollectionViaDeliverypointgroupAdvertisement.SortClauses=sortClauses;
			_advertisementCollectionViaDeliverypointgroupAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAdvertisement(forceFetch, _categoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchCategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAdvertisement"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAdvertisement = true;
			}
			return _categoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property CategoryCollectionViaAdvertisement is requested or GetMultiCategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_categoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAnnouncement(forceFetch, _categoryCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAnnouncement || forceFetch || _alwaysFetchCategoryCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_categoryCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAnnouncement"));
				_categoryCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAnnouncement = true;
			}
			return _categoryCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property CategoryCollectionViaAnnouncement is requested or GetMultiCategoryCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAnnouncement.SortClauses=sortClauses;
			_categoryCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAnnouncement_(forceFetch, _categoryCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAnnouncement_ || forceFetch || _alwaysFetchCategoryCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_categoryCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAnnouncement_"));
				_categoryCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAnnouncement_ = true;
			}
			return _categoryCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property CategoryCollectionViaAnnouncement_ is requested or GetMultiCategoryCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAnnouncement_.SortClauses=sortClauses;
			_categoryCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiClientCollectionViaNetmessage(forceFetch, _clientCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedClientCollectionViaNetmessage || forceFetch || _alwaysFetchClientCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_clientCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_clientCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_clientCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("ClientCollectionViaNetmessage"));
				_clientCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollectionViaNetmessage = true;
			}
			return _clientCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollectionViaNetmessage'. These settings will be taken into account
		/// when the property ClientCollectionViaNetmessage is requested or GetMultiClientCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollectionViaNetmessage.SortClauses=sortClauses;
			_clientCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaAdvertisement(forceFetch, _companyCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaAdvertisement || forceFetch || _alwaysFetchCompanyCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_companyCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("CompanyCollectionViaAdvertisement"));
				_companyCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaAdvertisement = true;
			}
			return _companyCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property CompanyCollectionViaAdvertisement is requested or GetMultiCompanyCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaAdvertisement.SortClauses=sortClauses;
			_companyCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaAnnouncement(forceFetch, _companyCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaAnnouncement || forceFetch || _alwaysFetchCompanyCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_companyCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("CompanyCollectionViaAnnouncement"));
				_companyCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaAnnouncement = true;
			}
			return _companyCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property CompanyCollectionViaAnnouncement is requested or GetMultiCompanyCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaAnnouncement.SortClauses=sortClauses;
			_companyCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaNetmessage(forceFetch, _companyCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaNetmessage || forceFetch || _alwaysFetchCompanyCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_companyCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("CompanyCollectionViaNetmessage"));
				_companyCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaNetmessage = true;
			}
			return _companyCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaNetmessage'. These settings will be taken into account
		/// when the property CompanyCollectionViaNetmessage is requested or GetMultiCompanyCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaNetmessage.SortClauses=sortClauses;
			_companyCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaNetmessage_(forceFetch, _companyCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaNetmessage_ || forceFetch || _alwaysFetchCompanyCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_companyCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("CompanyCollectionViaNetmessage_"));
				_companyCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaNetmessage_ = true;
			}
			return _companyCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property CompanyCollectionViaNetmessage_ is requested or GetMultiCompanyCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaNetmessage_.SortClauses=sortClauses;
			_companyCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaTerminal(forceFetch, _companyCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaTerminal || forceFetch || _alwaysFetchCompanyCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaTerminal.GetMulti(filter, GetRelationsForField("CompanyCollectionViaTerminal"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaTerminal = true;
			}
			return _companyCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaTerminal'. These settings will be taken into account
		/// when the property CompanyCollectionViaTerminal is requested or GetMultiCompanyCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaTerminal.SortClauses=sortClauses;
			_companyCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaNetmessage(forceFetch, _customerCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaNetmessage || forceFetch || _alwaysFetchCustomerCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_customerCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("CustomerCollectionViaNetmessage"));
				_customerCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaNetmessage = true;
			}
			return _customerCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaNetmessage'. These settings will be taken into account
		/// when the property CustomerCollectionViaNetmessage is requested or GetMultiCustomerCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaNetmessage.SortClauses=sortClauses;
			_customerCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiCustomerCollectionViaNetmessage_(forceFetch, _customerCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomerCollection GetMultiCustomerCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCustomerCollectionViaNetmessage_ || forceFetch || _alwaysFetchCustomerCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_customerCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_customerCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_customerCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("CustomerCollectionViaNetmessage_"));
				_customerCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerCollectionViaNetmessage_ = true;
			}
			return _customerCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property CustomerCollectionViaNetmessage_ is requested or GetMultiCustomerCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerCollectionViaNetmessage_.SortClauses=sortClauses;
			_customerCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaClient(forceFetch, _deliverypointCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaClient || forceFetch || _alwaysFetchDeliverypointCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deliverypointCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaClient.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaClient"));
				_deliverypointCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaClient = true;
			}
			return _deliverypointCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaClient'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaClient is requested or GetMultiDeliverypointCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaClient.SortClauses=sortClauses;
			_deliverypointCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaClient_(forceFetch, _deliverypointCollectionViaClient_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaClient_ || forceFetch || _alwaysFetchDeliverypointCollectionViaClient_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaClient_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deliverypointCollectionViaClient_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaClient_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaClient_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaClient_"));
				_deliverypointCollectionViaClient_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaClient_ = true;
			}
			return _deliverypointCollectionViaClient_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaClient_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaClient_ is requested or GetMultiDeliverypointCollectionViaClient_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaClient_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaClient_.SortClauses=sortClauses;
			_deliverypointCollectionViaClient_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaNetmessage(forceFetch, _deliverypointCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaNetmessage || forceFetch || _alwaysFetchDeliverypointCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deliverypointCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaNetmessage"));
				_deliverypointCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaNetmessage = true;
			}
			return _deliverypointCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaNetmessage'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaNetmessage is requested or GetMultiDeliverypointCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaNetmessage.SortClauses=sortClauses;
			_deliverypointCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaNetmessage_(forceFetch, _deliverypointCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaNetmessage_ || forceFetch || _alwaysFetchDeliverypointCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deliverypointCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaNetmessage_"));
				_deliverypointCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaNetmessage_ = true;
			}
			return _deliverypointCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaNetmessage_ is requested or GetMultiDeliverypointCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaNetmessage_.SortClauses=sortClauses;
			_deliverypointCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal(forceFetch, _deliverypointCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal = true;
			}
			return _deliverypointCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal is requested or GetMultiDeliverypointCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal_(forceFetch, _deliverypointCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal_ || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal_"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
			}
			return _deliverypointCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal_ is requested or GetMultiDeliverypointCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal_.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaClient(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaClient(forceFetch, _deviceCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaClient || forceFetch || _alwaysFetchDeviceCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deviceCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaClient.GetMulti(filter, GetRelationsForField("DeviceCollectionViaClient"));
				_deviceCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaClient = true;
			}
			return _deviceCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaClient'. These settings will be taken into account
		/// when the property DeviceCollectionViaClient is requested or GetMultiDeviceCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaClient.SortClauses=sortClauses;
			_deviceCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeviceCollectionViaTerminal(forceFetch, _deviceCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeviceCollectionViaTerminal || forceFetch || _alwaysFetchDeviceCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deviceCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeviceCollectionViaTerminal"));
				_deviceCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollectionViaTerminal = true;
			}
			return _deviceCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeviceCollectionViaTerminal is requested or GetMultiDeviceCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollectionViaTerminal.SortClauses=sortClauses;
			_deviceCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal(forceFetch, _entertainmentCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal = true;
			}
			return _entertainmentCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal is requested or GetMultiEntertainmentCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal_(forceFetch, _entertainmentCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal_ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal_"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
			}
			return _entertainmentCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal_ is requested or GetMultiEntertainmentCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal_.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal__(forceFetch, _entertainmentCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal__ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
			}
			return _entertainmentCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal__'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal__ is requested or GetMultiEntertainmentCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal__.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement(forceFetch, _entertainmentCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
			}
			return _entertainmentCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement is requested or GetMultiEntertainmentCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement_(forceFetch, _entertainmentCollectionViaAdvertisement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement_ || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaAdvertisement_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement_"));
				_entertainmentCollectionViaAdvertisement_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = true;
			}
			return _entertainmentCollectionViaAdvertisement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement_ is requested or GetMultiEntertainmentCollectionViaAdvertisement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement_.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAnnouncement(forceFetch, _entertainmentCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAnnouncement || forceFetch || _alwaysFetchEntertainmentCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAnnouncement"));
				_entertainmentCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = true;
			}
			return _entertainmentCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAnnouncement is requested or GetMultiEntertainmentCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAnnouncement.SortClauses=sortClauses;
			_entertainmentCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaDeliverypointgroupEntertainment(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaDeliverypointgroupEntertainment(forceFetch, _entertainmentCollectionViaDeliverypointgroupEntertainment.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaDeliverypointgroupEntertainment(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment || forceFetch || _alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaDeliverypointgroupEntertainment);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentCollectionViaDeliverypointgroupEntertainment.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaDeliverypointgroupEntertainment.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaDeliverypointgroupEntertainment.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaDeliverypointgroupEntertainment"));
				_entertainmentCollectionViaDeliverypointgroupEntertainment.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment = true;
			}
			return _entertainmentCollectionViaDeliverypointgroupEntertainment;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaDeliverypointgroupEntertainment'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaDeliverypointgroupEntertainment is requested or GetMultiEntertainmentCollectionViaDeliverypointgroupEntertainment is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaDeliverypointgroupEntertainment(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaDeliverypointgroupEntertainment.SortClauses=sortClauses;
			_entertainmentCollectionViaDeliverypointgroupEntertainment.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAdvertisement(forceFetch, _entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
			}
			return _entertainmentcategoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAdvertisement is requested or GetMultiEntertainmentcategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAnnouncement(forceFetch, _entertainmentcategoryCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentcategoryCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement"));
				_entertainmentcategoryCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = true;
			}
			return _entertainmentcategoryCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAnnouncement is requested or GetMultiEntertainmentcategoryCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAnnouncement.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement_(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAnnouncement_(forceFetch, _entertainmentcategoryCollectionViaAnnouncement_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAnnouncement_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAnnouncement_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_entertainmentcategoryCollectionViaAnnouncement_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAnnouncement_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAnnouncement_.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement_"));
				_entertainmentcategoryCollectionViaAnnouncement_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = true;
			}
			return _entertainmentcategoryCollectionViaAnnouncement_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAnnouncement_'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAnnouncement_ is requested or GetMultiEntertainmentcategoryCollectionViaAnnouncement_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAnnouncement_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAnnouncement_.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAnnouncement_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaAdvertisement(forceFetch, _genericproductCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaAdvertisement || forceFetch || _alwaysFetchGenericproductCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_genericproductCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaAdvertisement"));
				_genericproductCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaAdvertisement = true;
			}
			return _genericproductCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property GenericproductCollectionViaAdvertisement is requested or GetMultiGenericproductCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaAdvertisement.SortClauses=sortClauses;
			_genericproductCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiIcrtouchprintermappingCollectionViaTerminal(forceFetch, _icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal || forceFetch || _alwaysFetchIcrtouchprintermappingCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_icrtouchprintermappingCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_icrtouchprintermappingCollectionViaTerminal.GetMulti(filter, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
			}
			return _icrtouchprintermappingCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'IcrtouchprintermappingCollectionViaTerminal'. These settings will be taken into account
		/// when the property IcrtouchprintermappingCollectionViaTerminal is requested or GetMultiIcrtouchprintermappingCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIcrtouchprintermappingCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_icrtouchprintermappingCollectionViaTerminal.SortClauses=sortClauses;
			_icrtouchprintermappingCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiMediaCollectionViaAnnouncement(forceFetch, _mediaCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaAnnouncement || forceFetch || _alwaysFetchMediaCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_mediaCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("MediaCollectionViaAnnouncement"));
				_mediaCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaAnnouncement = true;
			}
			return _mediaCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property MediaCollectionViaAnnouncement is requested or GetMultiMediaCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaAnnouncement.SortClauses=sortClauses;
			_mediaCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch)
		{
			return GetMultiProductCollectionViaAnnouncement(forceFetch, _productCollectionViaAnnouncement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAnnouncement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAnnouncement || forceFetch || _alwaysFetchProductCollectionViaAnnouncement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAnnouncement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAnnouncement.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAnnouncement.GetMulti(filter, GetRelationsForField("ProductCollectionViaAnnouncement"));
				_productCollectionViaAnnouncement.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAnnouncement = true;
			}
			return _productCollectionViaAnnouncement;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAnnouncement'. These settings will be taken into account
		/// when the property ProductCollectionViaAnnouncement is requested or GetMultiProductCollectionViaAnnouncement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAnnouncement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAnnouncement.SortClauses=sortClauses;
			_productCollectionViaAnnouncement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaDeliverypointgroupProduct(bool forceFetch)
		{
			return GetMultiProductCollectionViaDeliverypointgroupProduct(forceFetch, _productCollectionViaDeliverypointgroupProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaDeliverypointgroupProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaDeliverypointgroupProduct || forceFetch || _alwaysFetchProductCollectionViaDeliverypointgroupProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaDeliverypointgroupProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaDeliverypointgroupProduct.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaDeliverypointgroupProduct.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaDeliverypointgroupProduct.GetMulti(filter, GetRelationsForField("ProductCollectionViaDeliverypointgroupProduct"));
				_productCollectionViaDeliverypointgroupProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaDeliverypointgroupProduct = true;
			}
			return _productCollectionViaDeliverypointgroupProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaDeliverypointgroupProduct'. These settings will be taken into account
		/// when the property ProductCollectionViaDeliverypointgroupProduct is requested or GetMultiProductCollectionViaDeliverypointgroupProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaDeliverypointgroupProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaDeliverypointgroupProduct.SortClauses=sortClauses;
			_productCollectionViaDeliverypointgroupProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal(forceFetch, _productCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal || forceFetch || _alwaysFetchProductCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal = true;
			}
			return _productCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal is requested or GetMultiProductCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal.SortClauses=sortClauses;
			_productCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal_(forceFetch, _productCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal_ || forceFetch || _alwaysFetchProductCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal_"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal_ = true;
			}
			return _productCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal_'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal_ is requested or GetMultiProductCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal_.SortClauses=sortClauses;
			_productCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal__(forceFetch, _productCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal__ || forceFetch || _alwaysFetchProductCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal__ = true;
			}
			return _productCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal__'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal__ is requested or GetMultiProductCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal__.SortClauses=sortClauses;
			_productCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal___(forceFetch, _productCollectionViaTerminal___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal___ || forceFetch || _alwaysFetchProductCollectionViaTerminal___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal___.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal___.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal___"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal___ = true;
			}
			return _productCollectionViaTerminal___;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal___'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal___ is requested or GetMultiProductCollectionViaTerminal___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal___.SortClauses=sortClauses;
			_productCollectionViaTerminal___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiProductCollectionViaAdvertisement(forceFetch, _productCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaAdvertisement || forceFetch || _alwaysFetchProductCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_productCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("ProductCollectionViaAdvertisement"));
				_productCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaAdvertisement = true;
			}
			return _productCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property ProductCollectionViaAdvertisement is requested or GetMultiProductCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaAdvertisement.SortClauses=sortClauses;
			_productCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupplierEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupplierCollection GetMultiSupplierCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiSupplierCollectionViaAdvertisement(forceFetch, _supplierCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupplierCollection GetMultiSupplierCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupplierCollectionViaAdvertisement || forceFetch || _alwaysFetchSupplierCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supplierCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_supplierCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_supplierCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_supplierCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("SupplierCollectionViaAdvertisement"));
				_supplierCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedSupplierCollectionViaAdvertisement = true;
			}
			return _supplierCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupplierCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property SupplierCollectionViaAdvertisement is requested or GetMultiSupplierCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupplierCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supplierCollectionViaAdvertisement.SortClauses=sortClauses;
			_supplierCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage(forceFetch, _terminalCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage = true;
			}
			return _terminalCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage is requested or GetMultiTerminalCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage_(forceFetch, _terminalCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage_ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage_"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
			}
			return _terminalCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage_ is requested or GetMultiTerminalCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage_.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaTerminal(forceFetch, _terminalCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaTerminal || forceFetch || _alwaysFetchTerminalCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaTerminal.GetMulti(filter, GetRelationsForField("TerminalCollectionViaTerminal"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaTerminal = true;
			}
			return _terminalCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaTerminal'. These settings will be taken into account
		/// when the property TerminalCollectionViaTerminal is requested or GetMultiTerminalCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaTerminal.SortClauses=sortClauses;
			_terminalCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaTerminal(forceFetch, _uIModeCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaTerminal || forceFetch || _alwaysFetchUIModeCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UIModeCollectionViaTerminal"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaTerminal = true;
			}
			return _uIModeCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaTerminal'. These settings will be taken into account
		/// when the property UIModeCollectionViaTerminal is requested or GetMultiUIModeCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaTerminal.SortClauses=sortClauses;
			_uIModeCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUserCollectionViaTerminal(forceFetch, _userCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUserCollectionViaTerminal || forceFetch || _alwaysFetchUserCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeliverypointgroupFields.DeliverypointgroupId, ComparisonOperator.Equal, this.DeliverypointgroupId, "DeliverypointgroupEntity__"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_userCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_userCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UserCollectionViaTerminal"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUserCollectionViaTerminal = true;
			}
			return _userCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserCollectionViaTerminal'. These settings will be taken into account
		/// when the property UserCollectionViaTerminal is requested or GetMultiUserCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userCollectionViaTerminal.SortClauses=sortClauses;
			_userCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleAddressEntity()
		{
			return GetSingleAddressEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleAddressEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAddressEntity || forceFetch || _alwaysFetchAddressEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingAddressId);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AddressId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_addressEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AddressEntity = newEntity;
				_alreadyFetchedAddressEntity = fetchResult;
			}
			return _addressEntity;
		}


		/// <summary> Retrieves the related entity of type 'AffiliateCampaignEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AffiliateCampaignEntity' which is related to this entity.</returns>
		public AffiliateCampaignEntity GetSingleAffiliateCampaignEntity()
		{
			return GetSingleAffiliateCampaignEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AffiliateCampaignEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AffiliateCampaignEntity' which is related to this entity.</returns>
		public virtual AffiliateCampaignEntity GetSingleAffiliateCampaignEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAffiliateCampaignEntity || forceFetch || _alwaysFetchAffiliateCampaignEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AffiliateCampaignEntityUsingAffiliateCampaignId);
				AffiliateCampaignEntity newEntity = new AffiliateCampaignEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AffiliateCampaignId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AffiliateCampaignEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_affiliateCampaignEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AffiliateCampaignEntity = newEntity;
				_alreadyFetchedAffiliateCampaignEntity = fetchResult;
			}
			return _affiliateCampaignEntity;
		}


		/// <summary> Retrieves the related entity of type 'AnnouncementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AnnouncementEntity' which is related to this entity.</returns>
		public AnnouncementEntity GetSingleAnnouncementEntity()
		{
			return GetSingleAnnouncementEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AnnouncementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AnnouncementEntity' which is related to this entity.</returns>
		public virtual AnnouncementEntity GetSingleAnnouncementEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAnnouncementEntity || forceFetch || _alwaysFetchAnnouncementEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId);
				AnnouncementEntity newEntity = new AnnouncementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReorderNotificationAnnouncementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AnnouncementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_announcementEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AnnouncementEntity = newEntity;
				_alreadyFetchedAnnouncementEntity = fetchResult;
			}
			return _announcementEntity;
		}


		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public ClientConfigurationEntity GetSingleClientConfigurationEntity()
		{
			return GetSingleClientConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientConfigurationEntity' which is related to this entity.</returns>
		public virtual ClientConfigurationEntity GetSingleClientConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientConfigurationEntity || forceFetch || _alwaysFetchClientConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientConfigurationEntityUsingClientConfigurationId);
				ClientConfigurationEntity newEntity = new ClientConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientConfigurationEntity = newEntity;
				_alreadyFetchedClientConfigurationEntity = fetchResult;
			}
			return _clientConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public MenuEntity GetSingleMenuEntity()
		{
			return GetSingleMenuEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MenuEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MenuEntity' which is related to this entity.</returns>
		public virtual MenuEntity GetSingleMenuEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMenuEntity || forceFetch || _alwaysFetchMenuEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MenuEntityUsingMenuId);
				MenuEntity newEntity = new MenuEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MenuId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MenuEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_menuEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MenuEntity = newEntity;
				_alreadyFetchedMenuEntity = fetchResult;
			}
			return _menuEntity;
		}


		/// <summary> Retrieves the related entity of type 'PosdeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosdeliverypointgroupEntity' which is related to this entity.</returns>
		public PosdeliverypointgroupEntity GetSinglePosdeliverypointgroupEntity()
		{
			return GetSinglePosdeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosdeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosdeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual PosdeliverypointgroupEntity GetSinglePosdeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosdeliverypointgroupEntity || forceFetch || _alwaysFetchPosdeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId);
				PosdeliverypointgroupEntity newEntity = new PosdeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PosdeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PosdeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_posdeliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PosdeliverypointgroupEntity = newEntity;
				_alreadyFetchedPosdeliverypointgroupEntity = fetchResult;
			}
			return _posdeliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public PriceScheduleEntity GetSinglePriceScheduleEntity()
		{
			return GetSinglePriceScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceScheduleEntity' which is related to this entity.</returns>
		public virtual PriceScheduleEntity GetSinglePriceScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceScheduleEntity || forceFetch || _alwaysFetchPriceScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceScheduleEntityUsingPriceScheduleId);
				PriceScheduleEntity newEntity = new PriceScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PriceScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceScheduleEntity = newEntity;
				_alreadyFetchedPriceScheduleEntity = fetchResult;
			}
			return _priceScheduleEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleHotSOSBatteryLowProductEntity()
		{
			return GetSingleHotSOSBatteryLowProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleHotSOSBatteryLowProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedHotSOSBatteryLowProductEntity || forceFetch || _alwaysFetchHotSOSBatteryLowProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingHotSOSBatteryLowProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.HotSOSBatteryLowProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_hotSOSBatteryLowProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.HotSOSBatteryLowProductEntity = newEntity;
				_alreadyFetchedHotSOSBatteryLowProductEntity = fetchResult;
			}
			return _hotSOSBatteryLowProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleEmailDocumentRouteEntity()
		{
			return GetSingleEmailDocumentRouteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleEmailDocumentRouteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEmailDocumentRouteEntity || forceFetch || _alwaysFetchEmailDocumentRouteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingEmailDocumentRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EmailDocumentRouteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_emailDocumentRouteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EmailDocumentRouteEntity = newEntity;
				_alreadyFetchedEmailDocumentRouteEntity = fetchResult;
			}
			return _emailDocumentRouteEntity;
		}


		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleRouteEntity()
		{
			return GetSingleRouteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleRouteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRouteEntity || forceFetch || _alwaysFetchRouteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RouteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RouteEntity = newEntity;
				_alreadyFetchedRouteEntity = fetchResult;
			}
			return _routeEntity;
		}


		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleRouteEntity_()
		{
			return GetSingleRouteEntity_(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleRouteEntity_(bool forceFetch)
		{
			if( ( !_alreadyFetchedRouteEntity_ || forceFetch || _alwaysFetchRouteEntity_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingOrderNotesRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderNotesRouteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routeEntity_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RouteEntity_ = newEntity;
				_alreadyFetchedRouteEntity_ = fetchResult;
			}
			return _routeEntity_;
		}


		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleSystemMessageRouteEntity()
		{
			return GetSingleSystemMessageRouteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleSystemMessageRouteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSystemMessageRouteEntity || forceFetch || _alwaysFetchSystemMessageRouteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingSystemMessageRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SystemMessageRouteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_systemMessageRouteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SystemMessageRouteEntity = newEntity;
				_alreadyFetchedSystemMessageRouteEntity = fetchResult;
			}
			return _systemMessageRouteEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleTerminalEntity()
		{
			return GetSingleTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalEntity || forceFetch || _alwaysFetchTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingXTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.XTerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalEntity = newEntity;
				_alreadyFetchedTerminalEntity = fetchResult;
			}
			return _terminalEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public UIModeEntity GetSingleMobileUIModeEntity()
		{
			return GetSingleMobileUIModeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public virtual UIModeEntity GetSingleMobileUIModeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMobileUIModeEntity || forceFetch || _alwaysFetchMobileUIModeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIModeEntityUsingMobileUIModeId);
				UIModeEntity newEntity = new UIModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MobileUIModeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mobileUIModeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MobileUIModeEntity = newEntity;
				_alreadyFetchedMobileUIModeEntity = fetchResult;
			}
			return _mobileUIModeEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public UIModeEntity GetSingleTabletUIModeEntity()
		{
			return GetSingleTabletUIModeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public virtual UIModeEntity GetSingleTabletUIModeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTabletUIModeEntity || forceFetch || _alwaysFetchTabletUIModeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIModeEntityUsingTabletUIModeId);
				UIModeEntity newEntity = new UIModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TabletUIModeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tabletUIModeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TabletUIModeEntity = newEntity;
				_alreadyFetchedTabletUIModeEntity = fetchResult;
			}
			return _tabletUIModeEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public UIModeEntity GetSingleUIModeEntity()
		{
			return GetSingleUIModeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public virtual UIModeEntity GetSingleUIModeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIModeEntity || forceFetch || _alwaysFetchUIModeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIModeEntityUsingUIModeId);
				UIModeEntity newEntity = new UIModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIModeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIModeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIModeEntity = newEntity;
				_alreadyFetchedUIModeEntity = fetchResult;
			}
			return _uIModeEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public UIScheduleEntity GetSingleUIScheduleEntity()
		{
			return GetSingleUIScheduleEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIScheduleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIScheduleEntity' which is related to this entity.</returns>
		public virtual UIScheduleEntity GetSingleUIScheduleEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIScheduleEntity || forceFetch || _alwaysFetchUIScheduleEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIScheduleEntityUsingUIScheduleId);
				UIScheduleEntity newEntity = new UIScheduleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIScheduleId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIScheduleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIScheduleEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIScheduleEntity = newEntity;
				_alreadyFetchedUIScheduleEntity = fetchResult;
			}
			return _uIScheduleEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public UIThemeEntity GetSingleUIThemeEntity()
		{
			return GetSingleUIThemeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIThemeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIThemeEntity' which is related to this entity.</returns>
		public virtual UIThemeEntity GetSingleUIThemeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIThemeEntity || forceFetch || _alwaysFetchUIThemeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIThemeEntityUsingUIThemeId);
				UIThemeEntity newEntity = new UIThemeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIThemeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UIThemeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIThemeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIThemeEntity = newEntity;
				_alreadyFetchedUIThemeEntity = fetchResult;
			}
			return _uIThemeEntity;
		}

		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public TimestampEntity GetSingleTimestampCollection()
		{
			return GetSingleTimestampCollection(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public virtual TimestampEntity GetSingleTimestampCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimestampEntityUsingDeliverypointgroupId);
				TimestampEntity newEntity = new TimestampEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCDeliverypointgroupId(this.DeliverypointgroupId);
				}
				if(fetchResult)
				{
					newEntity = (TimestampEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timestampCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimestampCollection = newEntity;
				_alreadyFetchedTimestampCollection = fetchResult;
			}
			return _timestampCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AddressEntity", _addressEntity);
			toReturn.Add("AffiliateCampaignEntity", _affiliateCampaignEntity);
			toReturn.Add("AnnouncementEntity", _announcementEntity);
			toReturn.Add("ClientConfigurationEntity", _clientConfigurationEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("MenuEntity", _menuEntity);
			toReturn.Add("PosdeliverypointgroupEntity", _posdeliverypointgroupEntity);
			toReturn.Add("PriceScheduleEntity", _priceScheduleEntity);
			toReturn.Add("HotSOSBatteryLowProductEntity", _hotSOSBatteryLowProductEntity);
			toReturn.Add("EmailDocumentRouteEntity", _emailDocumentRouteEntity);
			toReturn.Add("RouteEntity", _routeEntity);
			toReturn.Add("RouteEntity_", _routeEntity_);
			toReturn.Add("SystemMessageRouteEntity", _systemMessageRouteEntity);
			toReturn.Add("TerminalEntity", _terminalEntity);
			toReturn.Add("MobileUIModeEntity", _mobileUIModeEntity);
			toReturn.Add("TabletUIModeEntity", _tabletUIModeEntity);
			toReturn.Add("UIModeEntity", _uIModeEntity);
			toReturn.Add("UIScheduleEntity", _uIScheduleEntity);
			toReturn.Add("UIThemeEntity", _uIThemeEntity);
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("AnnouncementCollection", _announcementCollection);
			toReturn.Add("CheckoutMethodDeliverypointgroupCollection", _checkoutMethodDeliverypointgroupCollection);
			toReturn.Add("ClientCollection", _clientCollection);
			toReturn.Add("ClientConfigurationCollection", _clientConfigurationCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("DeliverypointCollection", _deliverypointCollection);
			toReturn.Add("DeliverypointgroupAdvertisementCollection", _deliverypointgroupAdvertisementCollection);
			toReturn.Add("DeliverypointgroupAnnouncementCollection", _deliverypointgroupAnnouncementCollection);
			toReturn.Add("DeliverypointgroupEntertainmentCollection", _deliverypointgroupEntertainmentCollection);
			toReturn.Add("DeliverypointgroupLanguageCollection", _deliverypointgroupLanguageCollection);
			toReturn.Add("DeliverypointgroupOccupancyCollection", _deliverypointgroupOccupancyCollection);
			toReturn.Add("DeliverypointgroupProductCollection", _deliverypointgroupProductCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("NetmessageCollection", _netmessageCollection);
			toReturn.Add("ServiceMethodDeliverypointgroupCollection", _serviceMethodDeliverypointgroupCollection);
			toReturn.Add("SetupCodeCollection", _setupCodeCollection);
			toReturn.Add("TerminalCollection", _terminalCollection);
			toReturn.Add("UIScheduleCollection", _uIScheduleCollection);
			toReturn.Add("AdvertisementCollectionViaDeliverypointgroupAdvertisement", _advertisementCollectionViaDeliverypointgroupAdvertisement);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaAdvertisement", _categoryCollectionViaAdvertisement);
			toReturn.Add("CategoryCollectionViaAnnouncement", _categoryCollectionViaAnnouncement);
			toReturn.Add("CategoryCollectionViaAnnouncement_", _categoryCollectionViaAnnouncement_);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("ClientCollectionViaNetmessage", _clientCollectionViaNetmessage);
			toReturn.Add("CompanyCollectionViaAdvertisement", _companyCollectionViaAdvertisement);
			toReturn.Add("CompanyCollectionViaAnnouncement", _companyCollectionViaAnnouncement);
			toReturn.Add("CompanyCollectionViaNetmessage", _companyCollectionViaNetmessage);
			toReturn.Add("CompanyCollectionViaNetmessage_", _companyCollectionViaNetmessage_);
			toReturn.Add("CompanyCollectionViaTerminal", _companyCollectionViaTerminal);
			toReturn.Add("CustomerCollectionViaNetmessage", _customerCollectionViaNetmessage);
			toReturn.Add("CustomerCollectionViaNetmessage_", _customerCollectionViaNetmessage_);
			toReturn.Add("DeliverypointCollectionViaClient", _deliverypointCollectionViaClient);
			toReturn.Add("DeliverypointCollectionViaClient_", _deliverypointCollectionViaClient_);
			toReturn.Add("DeliverypointCollectionViaNetmessage", _deliverypointCollectionViaNetmessage);
			toReturn.Add("DeliverypointCollectionViaNetmessage_", _deliverypointCollectionViaNetmessage_);
			toReturn.Add("DeliverypointCollectionViaTerminal", _deliverypointCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaTerminal_", _deliverypointCollectionViaTerminal_);
			toReturn.Add("DeviceCollectionViaClient", _deviceCollectionViaClient);
			toReturn.Add("DeviceCollectionViaTerminal", _deviceCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaTerminal", _entertainmentCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal_", _entertainmentCollectionViaTerminal_);
			toReturn.Add("EntertainmentCollectionViaTerminal__", _entertainmentCollectionViaTerminal__);
			toReturn.Add("EntertainmentCollectionViaAdvertisement", _entertainmentCollectionViaAdvertisement);
			toReturn.Add("EntertainmentCollectionViaAdvertisement_", _entertainmentCollectionViaAdvertisement_);
			toReturn.Add("EntertainmentCollectionViaAnnouncement", _entertainmentCollectionViaAnnouncement);
			toReturn.Add("EntertainmentCollectionViaDeliverypointgroupEntertainment", _entertainmentCollectionViaDeliverypointgroupEntertainment);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("EntertainmentcategoryCollectionViaAdvertisement", _entertainmentcategoryCollectionViaAdvertisement);
			toReturn.Add("EntertainmentcategoryCollectionViaAnnouncement", _entertainmentcategoryCollectionViaAnnouncement);
			toReturn.Add("EntertainmentcategoryCollectionViaAnnouncement_", _entertainmentcategoryCollectionViaAnnouncement_);
			toReturn.Add("GenericproductCollectionViaAdvertisement", _genericproductCollectionViaAdvertisement);
			toReturn.Add("IcrtouchprintermappingCollectionViaTerminal", _icrtouchprintermappingCollectionViaTerminal);
			toReturn.Add("MediaCollectionViaAnnouncement", _mediaCollectionViaAnnouncement);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("ProductCollectionViaAnnouncement", _productCollectionViaAnnouncement);
			toReturn.Add("ProductCollectionViaDeliverypointgroupProduct", _productCollectionViaDeliverypointgroupProduct);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("ProductCollectionViaTerminal", _productCollectionViaTerminal);
			toReturn.Add("ProductCollectionViaTerminal_", _productCollectionViaTerminal_);
			toReturn.Add("ProductCollectionViaTerminal__", _productCollectionViaTerminal__);
			toReturn.Add("ProductCollectionViaTerminal___", _productCollectionViaTerminal___);
			toReturn.Add("ProductCollectionViaAdvertisement", _productCollectionViaAdvertisement);
			toReturn.Add("SupplierCollectionViaAdvertisement", _supplierCollectionViaAdvertisement);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			toReturn.Add("TerminalCollectionViaNetmessage", _terminalCollectionViaNetmessage);
			toReturn.Add("TerminalCollectionViaNetmessage_", _terminalCollectionViaNetmessage_);
			toReturn.Add("TerminalCollectionViaTerminal", _terminalCollectionViaTerminal);
			toReturn.Add("UIModeCollectionViaTerminal", _uIModeCollectionViaTerminal);
			toReturn.Add("UserCollectionViaTerminal", _userCollectionViaTerminal);
			toReturn.Add("TimestampCollection", _timestampCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
            // __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="validator">The validator object for this DeliverypointgroupEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 deliverypointgroupId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deliverypointgroupId, prefetchPathToUse, null, null);

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
            // __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_announcementCollection = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_checkoutMethodDeliverypointgroupCollection = new Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection();
			_checkoutMethodDeliverypointgroupCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_clientCollection = new Obymobi.Data.CollectionClasses.ClientCollection();
			_clientCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_clientConfigurationCollection = new Obymobi.Data.CollectionClasses.ClientConfigurationCollection();
			_clientConfigurationCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_deliverypointCollection = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_deliverypointgroupAdvertisementCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection();
			_deliverypointgroupAdvertisementCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_deliverypointgroupAnnouncementCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection();
			_deliverypointgroupAnnouncementCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_deliverypointgroupEntertainmentCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection();
			_deliverypointgroupEntertainmentCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_deliverypointgroupLanguageCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection();
			_deliverypointgroupLanguageCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_deliverypointgroupOccupancyCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection();
			_deliverypointgroupOccupancyCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_deliverypointgroupProductCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection();
			_deliverypointgroupProductCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_netmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_netmessageCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_serviceMethodDeliverypointgroupCollection = new Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection();
			_serviceMethodDeliverypointgroupCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_setupCodeCollection = new Obymobi.Data.CollectionClasses.SetupCodeCollection();
			_setupCodeCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_terminalCollection = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");

			_uIScheduleCollection = new Obymobi.Data.CollectionClasses.UIScheduleCollection();
			_uIScheduleCollection.SetContainingEntityInfo(this, "DeliverypointgroupEntity");
			_advertisementCollectionViaDeliverypointgroupAdvertisement = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_clientCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.ClientCollection();
			_companyCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaTerminal = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_customerCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_customerCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.CustomerCollection();
			_deliverypointCollectionViaClient = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaClient_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deviceCollectionViaClient = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_deviceCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAdvertisement_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaDeliverypointgroupEntertainment = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_entertainmentcategoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_entertainmentcategoryCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_entertainmentcategoryCollectionViaAnnouncement_ = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_genericproductCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_icrtouchprintermappingCollectionViaTerminal = new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection();
			_mediaCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.MediaCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaAnnouncement = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaDeliverypointgroupProduct = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal___ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.ProductCollection();
			_supplierCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.SupplierCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_terminalCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaTerminal = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_userCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UserCollection();
			_addressEntityReturnsNewIfNotFound = true;
			_affiliateCampaignEntityReturnsNewIfNotFound = true;
			_announcementEntityReturnsNewIfNotFound = true;
			_clientConfigurationEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_menuEntityReturnsNewIfNotFound = true;
			_posdeliverypointgroupEntityReturnsNewIfNotFound = true;
			_priceScheduleEntityReturnsNewIfNotFound = true;
			_hotSOSBatteryLowProductEntityReturnsNewIfNotFound = true;
			_emailDocumentRouteEntityReturnsNewIfNotFound = true;
			_routeEntityReturnsNewIfNotFound = true;
			_routeEntity_ReturnsNewIfNotFound = true;
			_systemMessageRouteEntityReturnsNewIfNotFound = true;
			_terminalEntityReturnsNewIfNotFound = true;
			_mobileUIModeEntityReturnsNewIfNotFound = true;
			_tabletUIModeEntityReturnsNewIfNotFound = true;
			_uIModeEntityReturnsNewIfNotFound = true;
			_uIScheduleEntityReturnsNewIfNotFound = true;
			_uIThemeEntityReturnsNewIfNotFound = true;
			_timestampCollectionReturnsNewIfNotFound = true;
			PerformDependencyInjection();

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
            // __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfirmationCodeDeliveryType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseAgeCheckInOtoucho", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseManualDeliverypointsEntryInOtoucho", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseStatelessInOtoucho", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SkipOrderOverviewAfterOrderSubmitInOtoucho", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiSsid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiPassword", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Locale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Pincode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseManualDeliverypoint", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseManualDeliverypointEncryption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddDeliverypointToOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EncryptionSalt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HideDeliverypointNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HidePrices", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearSessionOnTimeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResetTimeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReorderNotificationEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReorderNotificationInterval", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReorderNotificationAnnouncementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreenTimeoutInterval", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceActivationRequired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultProductFullView", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MarketingSurveyUrl", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl1Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl1Zoom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl2Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl2Zoom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl3Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotelUrl3Zoom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DimLevelDull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DimLevelMedium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DimLevelBright", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerSaveTimeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerSaveLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderFailedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderFailedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompletedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompletedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderCompletedEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderSavingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderSavingMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceSavingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceSavingMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingSavingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingSavingMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingProcessedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RatingProcessedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DailyOrderReset", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SleepTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WakeUpTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessingNotificationEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessingNotificationTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessingNotification", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessedNotificationEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessedNotificationTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderProcessedNotification", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FreeformMessageTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessingNotificationTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessingNotification", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessedNotificationTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceProcessedNotification", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PreorderingEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PreorderingActiveMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnalyticsOrderingVisible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnalyticsBestsellersVisible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PincodeSU", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PincodeGM", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DockedDimLevelDull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DockedDimLevelMedium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DockedDimLevelBright", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointCaption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderingEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HomepageSlideshowInterval", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderHistoryDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderingNotAvailableMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GooglePrinterId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsIntegration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsAllowShowBill", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsAllowExpressCheckout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsLockClientWhenNotCheckedIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiAnonymousIdentify", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiEapMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiIdentity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiPhase2Authentication", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiSecurityMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosdeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemMessageRouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("XTerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutOfChargeNotificationAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsChargerRemovedDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedDialogTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedDialogText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsChargerRemovedReminderDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedReminderDialogTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargerRemovedReminderDialogText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsDeviceLockedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsDeviceLockedText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckinFailedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckinFailedText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsRestartTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsRestartText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsWelcomeTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsWelcomeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutApproveText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsWelcomeTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutCompleteTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutCompleteText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsCheckoutCompleteTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailableOnObymobi", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobileUIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabletUIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerButtonHardBehaviour", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PowerButtonSoftBehaviour", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreenOffMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseHardKeyboard", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GooglePrinterName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomserviceCharge", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HotSOSBatteryLowProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailDocumentRouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnableBatteryLowConsoleNotifications", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIThemeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreensaverMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PmsAllowShowGuestName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TurnOffPrivacyTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TurnOffPrivacyText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EstimatedDeliveryTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNotesRouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ItemCurrentlyUnavailableText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceFailedTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceFailedMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlarmSetWhileNotChargingTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlarmSetWhileNotChargingText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceScheduleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsOrderitemAddedDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrowserAgeVerificationEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrowserAgeVerificationLayout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RestartApplicationDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RebootDeviceDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RestartTimeoutSeconds", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CraveAnalytics", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GeoFencingEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GeoFencingRadius", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HideCompanyDetails", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsClearBasketDialogEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearBasketTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearBasketText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApiVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagingVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AffiliateCampaignId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _addressEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAddressEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _addressEntity, new PropertyChangedEventHandler( OnAddressEntityPropertyChanged ), "AddressEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.AddressEntityUsingAddressIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.AddressId } );		
			_addressEntity = null;
		}
		
		/// <summary> setups the sync logic for member _addressEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAddressEntity(IEntityCore relatedEntity)
		{
			if(_addressEntity!=relatedEntity)
			{		
				DesetupSyncAddressEntity(true, true);
				_addressEntity = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _addressEntity, new PropertyChangedEventHandler( OnAddressEntityPropertyChanged ), "AddressEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.AddressEntityUsingAddressIdStatic, true, ref _alreadyFetchedAddressEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAddressEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _affiliateCampaignEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAffiliateCampaignEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _affiliateCampaignEntity, new PropertyChangedEventHandler( OnAffiliateCampaignEntityPropertyChanged ), "AffiliateCampaignEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.AffiliateCampaignEntityUsingAffiliateCampaignIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.AffiliateCampaignId } );		
			_affiliateCampaignEntity = null;
		}
		
		/// <summary> setups the sync logic for member _affiliateCampaignEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAffiliateCampaignEntity(IEntityCore relatedEntity)
		{
			if(_affiliateCampaignEntity!=relatedEntity)
			{		
				DesetupSyncAffiliateCampaignEntity(true, true);
				_affiliateCampaignEntity = (AffiliateCampaignEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _affiliateCampaignEntity, new PropertyChangedEventHandler( OnAffiliateCampaignEntityPropertyChanged ), "AffiliateCampaignEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.AffiliateCampaignEntityUsingAffiliateCampaignIdStatic, true, ref _alreadyFetchedAffiliateCampaignEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAffiliateCampaignEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _announcementEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAnnouncementEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _announcementEntity, new PropertyChangedEventHandler( OnAnnouncementEntityPropertyChanged ), "AnnouncementEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.AnnouncementEntityUsingReorderNotificationAnnouncementIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.ReorderNotificationAnnouncementId } );		
			_announcementEntity = null;
		}
		
		/// <summary> setups the sync logic for member _announcementEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAnnouncementEntity(IEntityCore relatedEntity)
		{
			if(_announcementEntity!=relatedEntity)
			{		
				DesetupSyncAnnouncementEntity(true, true);
				_announcementEntity = (AnnouncementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _announcementEntity, new PropertyChangedEventHandler( OnAnnouncementEntityPropertyChanged ), "AnnouncementEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.AnnouncementEntityUsingReorderNotificationAnnouncementIdStatic, true, ref _alreadyFetchedAnnouncementEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAnnouncementEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.ClientConfigurationId } );		
			_clientConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_clientConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncClientConfigurationEntity(true, true);
				_clientConfigurationEntity = (ClientConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientConfigurationEntity, new PropertyChangedEventHandler( OnClientConfigurationEntityPropertyChanged ), "ClientConfigurationEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.ClientConfigurationEntityUsingClientConfigurationIdStatic, true, ref _alreadyFetchedClientConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _menuEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMenuEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.MenuEntityUsingMenuIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.MenuId } );		
			_menuEntity = null;
		}
		
		/// <summary> setups the sync logic for member _menuEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMenuEntity(IEntityCore relatedEntity)
		{
			if(_menuEntity!=relatedEntity)
			{		
				DesetupSyncMenuEntity(true, true);
				_menuEntity = (MenuEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _menuEntity, new PropertyChangedEventHandler( OnMenuEntityPropertyChanged ), "MenuEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.MenuEntityUsingMenuIdStatic, true, ref _alreadyFetchedMenuEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMenuEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posdeliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosdeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _posdeliverypointgroupEntity, new PropertyChangedEventHandler( OnPosdeliverypointgroupEntityPropertyChanged ), "PosdeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.PosdeliverypointgroupId } );		
			_posdeliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posdeliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosdeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_posdeliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncPosdeliverypointgroupEntity(true, true);
				_posdeliverypointgroupEntity = (PosdeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _posdeliverypointgroupEntity, new PropertyChangedEventHandler( OnPosdeliverypointgroupEntityPropertyChanged ), "PosdeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupIdStatic, true, ref _alreadyFetchedPosdeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosdeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.PriceScheduleId } );		
			_priceScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceScheduleEntity(IEntityCore relatedEntity)
		{
			if(_priceScheduleEntity!=relatedEntity)
			{		
				DesetupSyncPriceScheduleEntity(true, true);
				_priceScheduleEntity = (PriceScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceScheduleEntity, new PropertyChangedEventHandler( OnPriceScheduleEntityPropertyChanged ), "PriceScheduleEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.PriceScheduleEntityUsingPriceScheduleIdStatic, true, ref _alreadyFetchedPriceScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _hotSOSBatteryLowProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncHotSOSBatteryLowProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _hotSOSBatteryLowProductEntity, new PropertyChangedEventHandler( OnHotSOSBatteryLowProductEntityPropertyChanged ), "HotSOSBatteryLowProductEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.ProductEntityUsingHotSOSBatteryLowProductIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId } );		
			_hotSOSBatteryLowProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _hotSOSBatteryLowProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncHotSOSBatteryLowProductEntity(IEntityCore relatedEntity)
		{
			if(_hotSOSBatteryLowProductEntity!=relatedEntity)
			{		
				DesetupSyncHotSOSBatteryLowProductEntity(true, true);
				_hotSOSBatteryLowProductEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _hotSOSBatteryLowProductEntity, new PropertyChangedEventHandler( OnHotSOSBatteryLowProductEntityPropertyChanged ), "HotSOSBatteryLowProductEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.ProductEntityUsingHotSOSBatteryLowProductIdStatic, true, ref _alreadyFetchedHotSOSBatteryLowProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnHotSOSBatteryLowProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _emailDocumentRouteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEmailDocumentRouteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _emailDocumentRouteEntity, new PropertyChangedEventHandler( OnEmailDocumentRouteEntityPropertyChanged ), "EmailDocumentRouteEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingEmailDocumentRouteIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection__", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.EmailDocumentRouteId } );		
			_emailDocumentRouteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _emailDocumentRouteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEmailDocumentRouteEntity(IEntityCore relatedEntity)
		{
			if(_emailDocumentRouteEntity!=relatedEntity)
			{		
				DesetupSyncEmailDocumentRouteEntity(true, true);
				_emailDocumentRouteEntity = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _emailDocumentRouteEntity, new PropertyChangedEventHandler( OnEmailDocumentRouteEntityPropertyChanged ), "EmailDocumentRouteEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingEmailDocumentRouteIdStatic, true, ref _alreadyFetchedEmailDocumentRouteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEmailDocumentRouteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRouteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingRouteIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.RouteId } );		
			_routeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _routeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRouteEntity(IEntityCore relatedEntity)
		{
			if(_routeEntity!=relatedEntity)
			{		
				DesetupSyncRouteEntity(true, true);
				_routeEntity = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routeEntity, new PropertyChangedEventHandler( OnRouteEntityPropertyChanged ), "RouteEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingRouteIdStatic, true, ref _alreadyFetchedRouteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRouteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routeEntity_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRouteEntity_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routeEntity_, new PropertyChangedEventHandler( OnRouteEntity_PropertyChanged ), "RouteEntity_", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingOrderNotesRouteIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection___", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.OrderNotesRouteId } );		
			_routeEntity_ = null;
		}
		
		/// <summary> setups the sync logic for member _routeEntity_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRouteEntity_(IEntityCore relatedEntity)
		{
			if(_routeEntity_!=relatedEntity)
			{		
				DesetupSyncRouteEntity_(true, true);
				_routeEntity_ = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routeEntity_, new PropertyChangedEventHandler( OnRouteEntity_PropertyChanged ), "RouteEntity_", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingOrderNotesRouteIdStatic, true, ref _alreadyFetchedRouteEntity_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRouteEntity_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _systemMessageRouteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSystemMessageRouteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _systemMessageRouteEntity, new PropertyChangedEventHandler( OnSystemMessageRouteEntityPropertyChanged ), "SystemMessageRouteEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingSystemMessageRouteIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection_", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.SystemMessageRouteId } );		
			_systemMessageRouteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _systemMessageRouteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSystemMessageRouteEntity(IEntityCore relatedEntity)
		{
			if(_systemMessageRouteEntity!=relatedEntity)
			{		
				DesetupSyncSystemMessageRouteEntity(true, true);
				_systemMessageRouteEntity = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _systemMessageRouteEntity, new PropertyChangedEventHandler( OnSystemMessageRouteEntityPropertyChanged ), "SystemMessageRouteEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.RouteEntityUsingSystemMessageRouteIdStatic, true, ref _alreadyFetchedSystemMessageRouteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSystemMessageRouteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _terminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.TerminalEntityUsingXTerminalIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.XTerminalId } );		
			_terminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _terminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalEntity(IEntityCore relatedEntity)
		{
			if(_terminalEntity!=relatedEntity)
			{		
				DesetupSyncTerminalEntity(true, true);
				_terminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.TerminalEntityUsingXTerminalIdStatic, true, ref _alreadyFetchedTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mobileUIModeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMobileUIModeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mobileUIModeEntity, new PropertyChangedEventHandler( OnMobileUIModeEntityPropertyChanged ), "MobileUIModeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIModeEntityUsingMobileUIModeIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection_", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.MobileUIModeId } );		
			_mobileUIModeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mobileUIModeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMobileUIModeEntity(IEntityCore relatedEntity)
		{
			if(_mobileUIModeEntity!=relatedEntity)
			{		
				DesetupSyncMobileUIModeEntity(true, true);
				_mobileUIModeEntity = (UIModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mobileUIModeEntity, new PropertyChangedEventHandler( OnMobileUIModeEntityPropertyChanged ), "MobileUIModeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIModeEntityUsingMobileUIModeIdStatic, true, ref _alreadyFetchedMobileUIModeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMobileUIModeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tabletUIModeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTabletUIModeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tabletUIModeEntity, new PropertyChangedEventHandler( OnTabletUIModeEntityPropertyChanged ), "TabletUIModeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIModeEntityUsingTabletUIModeIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection__", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.TabletUIModeId } );		
			_tabletUIModeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _tabletUIModeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTabletUIModeEntity(IEntityCore relatedEntity)
		{
			if(_tabletUIModeEntity!=relatedEntity)
			{		
				DesetupSyncTabletUIModeEntity(true, true);
				_tabletUIModeEntity = (UIModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tabletUIModeEntity, new PropertyChangedEventHandler( OnTabletUIModeEntityPropertyChanged ), "TabletUIModeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIModeEntityUsingTabletUIModeIdStatic, true, ref _alreadyFetchedTabletUIModeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTabletUIModeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIModeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIModeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIModeEntityUsingUIModeIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.UIModeId } );		
			_uIModeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIModeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIModeEntity(IEntityCore relatedEntity)
		{
			if(_uIModeEntity!=relatedEntity)
			{		
				DesetupSyncUIModeEntity(true, true);
				_uIModeEntity = (UIModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIModeEntityUsingUIModeIdStatic, true, ref _alreadyFetchedUIModeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIModeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIScheduleEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIScheduleEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.UIScheduleId } );		
			_uIScheduleEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIScheduleEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIScheduleEntity(IEntityCore relatedEntity)
		{
			if(_uIScheduleEntity!=relatedEntity)
			{		
				DesetupSyncUIScheduleEntity(true, true);
				_uIScheduleEntity = (UIScheduleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIScheduleEntity, new PropertyChangedEventHandler( OnUIScheduleEntityPropertyChanged ), "UIScheduleEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIScheduleEntityUsingUIScheduleIdStatic, true, ref _alreadyFetchedUIScheduleEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIScheduleEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIThemeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIThemeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIThemeEntityUsingUIThemeIdStatic, true, signalRelatedEntity, "DeliverypointgroupCollection", resetFKFields, new int[] { (int)DeliverypointgroupFieldIndex.UIThemeId } );		
			_uIThemeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIThemeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIThemeEntity(IEntityCore relatedEntity)
		{
			if(_uIThemeEntity!=relatedEntity)
			{		
				DesetupSyncUIThemeEntity(true, true);
				_uIThemeEntity = (UIThemeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIThemeEntity, new PropertyChangedEventHandler( OnUIThemeEntityPropertyChanged ), "UIThemeEntity", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.UIThemeEntityUsingUIThemeIdStatic, true, ref _alreadyFetchedUIThemeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIThemeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timestampCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimestampCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.TimestampEntityUsingDeliverypointgroupIdStatic, false, signalRelatedEntity, "DeliverypointgroupEntity", false, new int[] { (int)DeliverypointgroupFieldIndex.DeliverypointgroupId } );
			_timestampCollection = null;
		}
	
		/// <summary> setups the sync logic for member _timestampCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimestampCollection(IEntityCore relatedEntity)
		{
			if(_timestampCollection!=relatedEntity)
			{
				DesetupSyncTimestampCollection(true, true);
				_timestampCollection = (TimestampEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticDeliverypointgroupRelations.TimestampEntityUsingDeliverypointgroupIdStatic, false, ref _alreadyFetchedTimestampCollection, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimestampCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deliverypointgroupId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeliverypointgroupFieldIndex.DeliverypointgroupId].ForcedCurrentValueWrite(deliverypointgroupId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliverypointgroupDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeliverypointgroupEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeliverypointgroupRelations Relations
		{
			get	{ return new DeliverypointgroupRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CheckoutMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCheckoutMethodDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("CheckoutMethodDeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CheckoutMethodDeliverypointgroupEntity, 0, null, null, null, "CheckoutMethodDeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupAdvertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupAdvertisementCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupAdvertisementEntity, 0, null, null, null, "DeliverypointgroupAdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupAnnouncement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupAnnouncementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupAnnouncementCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity, 0, null, null, null, "DeliverypointgroupAnnouncementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupEntertainment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntertainmentCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntertainmentCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntertainmentEntity, 0, null, null, null, "DeliverypointgroupEntertainmentCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupLanguageCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity, 0, null, null, null, "DeliverypointgroupLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupOccupancy' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupOccupancyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupOccupancyCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupOccupancyEntity, 0, null, null, null, "DeliverypointgroupOccupancyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupProductCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupProductEntity, 0, null, null, null, "DeliverypointgroupProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("NetmessageCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "NetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceMethodDeliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceMethodDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("ServiceMethodDeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ServiceMethodDeliverypointgroupEntity, 0, null, null, null, "ServiceMethodDeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SetupCode' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSetupCodeCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SetupCodeCollection(), (IEntityRelation)GetRelationsForField("SetupCodeCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.SetupCodeEntity, 0, null, null, null, "SetupCodeCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UISchedule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleCollection(), (IEntityRelation)GetRelationsForField("UIScheduleCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UIScheduleEntity, 0, null, null, null, "UIScheduleCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollectionViaDeliverypointgroupAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupAdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "DeliverypointgroupAdvertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, GetRelationsForField("AdvertisementCollectionViaDeliverypointgroupAdvertisement"), "AdvertisementCollectionViaDeliverypointgroupAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAdvertisement"), "CategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAnnouncement"), "CategoryCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAnnouncement_"), "CategoryCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, GetRelationsForField("ClientCollectionViaNetmessage"), "ClientCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaAdvertisement"), "CompanyCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaAnnouncement"), "CompanyCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaNetmessage"), "CompanyCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaNetmessage_"), "CompanyCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaTerminal"), "CompanyCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaNetmessage"), "CustomerCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, GetRelationsForField("CustomerCollectionViaNetmessage_"), "CustomerCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointGroupId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaClient"), "DeliverypointCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaClient_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointGroupId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaClient_"), "DeliverypointCollectionViaClient_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaNetmessage"), "DeliverypointCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaNetmessage_"), "DeliverypointCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal"), "DeliverypointCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal_"), "DeliverypointCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeliverypointGroupId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaClient"), "DeviceCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, GetRelationsForField("DeviceCollectionViaTerminal"), "DeviceCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal"), "EntertainmentCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal_"), "EntertainmentCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal__"), "EntertainmentCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement"), "EntertainmentCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement_"), "EntertainmentCollectionViaAdvertisement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAnnouncement"), "EntertainmentCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaDeliverypointgroupEntertainment
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "DeliverypointgroupEntertainment_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaDeliverypointgroupEntertainment"), "EntertainmentCollectionViaDeliverypointgroupEntertainment", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"), "EntertainmentcategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement"), "EntertainmentcategoryCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAnnouncement_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAnnouncement_"), "EntertainmentcategoryCollectionViaAnnouncement_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaAdvertisement"), "GenericproductCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"), "IcrtouchprintermappingCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaAnnouncement"), "MediaCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAnnouncement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AnnouncementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Announcement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAnnouncement"), "ProductCollectionViaAnnouncement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaDeliverypointgroupProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupProductEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "DeliverypointgroupProduct_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaDeliverypointgroupProduct"), "ProductCollectionViaDeliverypointgroupProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal"), "ProductCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal_"), "ProductCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal__"), "ProductCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal___"), "ProductCollectionViaTerminal___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaAdvertisement"), "ProductCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supplier'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupplierCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupplierCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.SupplierEntity, 0, null, null, GetRelationsForField("SupplierCollectionViaAdvertisement"), "SupplierCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage"), "TerminalCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage_"), "TerminalCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaTerminal"), "TerminalCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaTerminal"), "UIModeCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeliverypointgroupId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, GetRelationsForField("UserCollectionViaTerminal"), "UserCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddressEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("AddressEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.AddressEntity, 0, null, null, null, "AddressEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AffiliateCampaign'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAffiliateCampaignEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AffiliateCampaignCollection(), (IEntityRelation)GetRelationsForField("AffiliateCampaignEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.AffiliateCampaignEntity, 0, null, null, null, "AffiliateCampaignEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), (IEntityRelation)GetRelationsForField("AnnouncementEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, null, "AnnouncementEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), (IEntityRelation)GetRelationsForField("MenuEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, null, "MenuEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("PosdeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, null, "PosdeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceSchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceScheduleCollection(), (IEntityRelation)GetRelationsForField("PriceScheduleEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.PriceScheduleEntity, 0, null, null, null, "PriceScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathHotSOSBatteryLowProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("HotSOSBatteryLowProductEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "HotSOSBatteryLowProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailDocumentRouteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("EmailDocumentRouteEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "EmailDocumentRouteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RouteEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "RouteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteEntity_
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RouteEntity_")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "RouteEntity_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSystemMessageRouteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("SystemMessageRouteEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "SystemMessageRouteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMobileUIModeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("MobileUIModeEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "MobileUIModeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTabletUIModeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("TabletUIModeEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "TabletUIModeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("UIModeEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "UIModeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UISchedule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleCollection(), (IEntityRelation)GetRelationsForField("UIScheduleEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UIScheduleEntity, 0, null, null, null, "UIScheduleEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITheme'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIThemeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIThemeCollection(), (IEntityRelation)GetRelationsForField("UIThemeEntity")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.UIThemeEntity, 0, null, null, null, "UIThemeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeliverypointgroupId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeliverypointgroupId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DeliverypointgroupId, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DeliverypointgroupId, value, true); }
		}

		/// <summary> The Name property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.Name, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Name, value, true); }
		}

		/// <summary> The ConfirmationCodeDeliveryType property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ConfirmationCodeDeliveryType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ConfirmationCodeDeliveryType
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.ConfirmationCodeDeliveryType, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ConfirmationCodeDeliveryType, value, true); }
		}

		/// <summary> The Active property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Active"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.Active, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Active, value, true); }
		}

		/// <summary> The UseAgeCheckInOtoucho property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseAgeCheckInOtoucho"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseAgeCheckInOtoucho
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.UseAgeCheckInOtoucho, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseAgeCheckInOtoucho, value, true); }
		}

		/// <summary> The UseManualDeliverypointsEntryInOtoucho property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseManualDeliverypointsEntryInOtoucho"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseManualDeliverypointsEntryInOtoucho
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypointsEntryInOtoucho, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypointsEntryInOtoucho, value, true); }
		}

		/// <summary> The UseStatelessInOtoucho property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseStatelessInOtoucho"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseStatelessInOtoucho
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.UseStatelessInOtoucho, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseStatelessInOtoucho, value, true); }
		}

		/// <summary> The SkipOrderOverviewAfterOrderSubmitInOtoucho property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."SkipOrderOverviewAfterOrderSubmitInOtoucho"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SkipOrderOverviewAfterOrderSubmitInOtoucho
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.SkipOrderOverviewAfterOrderSubmitInOtoucho, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.SkipOrderOverviewAfterOrderSubmitInOtoucho, value, true); }
		}

		/// <summary> The WifiSsid property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiSsid"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiSsid
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiSsid, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiSsid, value, true); }
		}

		/// <summary> The WifiPassword property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiPassword"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiPassword
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiPassword, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiPassword, value, true); }
		}

		/// <summary> The Locale property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Locale"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Locale
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.Locale, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Locale, value, true); }
		}

		/// <summary> The Pincode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."Pincode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Pincode
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.Pincode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.Pincode, value, true); }
		}

		/// <summary> The UseManualDeliverypoint property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseManualDeliverypoint"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseManualDeliverypoint
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypoint, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypoint, value, true); }
		}

		/// <summary> The UseManualDeliverypointEncryption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseManualDeliverypointEncryption"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseManualDeliverypointEncryption
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypointEncryption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseManualDeliverypointEncryption, value, true); }
		}

		/// <summary> The AddDeliverypointToOrder property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AddDeliverypointToOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AddDeliverypointToOrder
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AddDeliverypointToOrder, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AddDeliverypointToOrder, value, true); }
		}

		/// <summary> The EncryptionSalt property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EncryptionSalt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EncryptionSalt
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.EncryptionSalt, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EncryptionSalt, value, true); }
		}

		/// <summary> The HideDeliverypointNumber property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HideDeliverypointNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HideDeliverypointNumber
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.HideDeliverypointNumber, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HideDeliverypointNumber, value, true); }
		}

		/// <summary> The HidePrices property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HidePrices"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HidePrices
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.HidePrices, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HidePrices, value, true); }
		}

		/// <summary> The ClearSessionOnTimeout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClearSessionOnTimeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ClearSessionOnTimeout
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.ClearSessionOnTimeout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClearSessionOnTimeout, value, true); }
		}

		/// <summary> The ResetTimeout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ResetTimeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResetTimeout
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ResetTimeout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ResetTimeout, value, true); }
		}

		/// <summary> The ReorderNotificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ReorderNotificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ReorderNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationEnabled, value, true); }
		}

		/// <summary> The ReorderNotificationInterval property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ReorderNotificationInterval"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ReorderNotificationInterval
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationInterval, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationInterval, value, true); }
		}

		/// <summary> The ReorderNotificationAnnouncementId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ReorderNotificationAnnouncementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReorderNotificationAnnouncementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationAnnouncementId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ReorderNotificationAnnouncementId, value, true); }
		}

		/// <summary> The ScreenTimeoutInterval property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ScreenTimeoutInterval"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenTimeoutInterval
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ScreenTimeoutInterval, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ScreenTimeoutInterval, value, true); }
		}

		/// <summary> The DeviceActivationRequired property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DeviceActivationRequired"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DeviceActivationRequired
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.DeviceActivationRequired, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DeviceActivationRequired, value, true); }
		}

		/// <summary> The DefaultProductFullView property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DefaultProductFullView"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DefaultProductFullView
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.DefaultProductFullView, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DefaultProductFullView, value, true); }
		}

		/// <summary> The MarketingSurveyUrl property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MarketingSurveyUrl"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MarketingSurveyUrl
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.MarketingSurveyUrl, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MarketingSurveyUrl, value, true); }
		}

		/// <summary> The HotelUrl1 property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl1
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl1, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl1, value, true); }
		}

		/// <summary> The HotelUrl1Caption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl1Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl1Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Caption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Caption, value, true); }
		}

		/// <summary> The HotelUrl1Zoom property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl1Zoom"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl1Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl1Zoom, value, true); }
		}

		/// <summary> The HotelUrl2 property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl2
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl2, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl2, value, true); }
		}

		/// <summary> The HotelUrl2Caption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl2Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl2Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Caption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Caption, value, true); }
		}

		/// <summary> The HotelUrl2Zoom property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl2Zoom"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl2Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl2Zoom, value, true); }
		}

		/// <summary> The HotelUrl3 property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl3
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl3, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl3, value, true); }
		}

		/// <summary> The HotelUrl3Caption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl3Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HotelUrl3Caption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Caption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Caption, value, true); }
		}

		/// <summary> The HotelUrl3Zoom property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotelUrl3Zoom"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotelUrl3Zoom
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Zoom, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotelUrl3Zoom, value, true); }
		}

		/// <summary> The DimLevelDull property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DimLevelDull"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelDull
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DimLevelDull, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DimLevelDull, value, true); }
		}

		/// <summary> The DimLevelMedium property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DimLevelMedium"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelMedium
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DimLevelMedium, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DimLevelMedium, value, true); }
		}

		/// <summary> The DimLevelBright property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DimLevelBright"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DimLevelBright
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DimLevelBright, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DimLevelBright, value, true); }
		}

		/// <summary> The PowerSaveTimeout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerSaveTimeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveTimeout
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerSaveTimeout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerSaveTimeout, value, true); }
		}

		/// <summary> The PowerSaveLevel property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerSaveLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerSaveLevel
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerSaveLevel, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerSaveLevel, value, true); }
		}

		/// <summary> The OutOfChargeLevel property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeLevel
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeLevel, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeLevel, value, true); }
		}

		/// <summary> The OutOfChargeTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OutOfChargeTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeTitle, value, true); }
		}

		/// <summary> The OutOfChargeMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OutOfChargeMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeMessage, value, true); }
		}

		/// <summary> The OrderProcessedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedTitle, value, true); }
		}

		/// <summary> The OrderProcessedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedMessage, value, true); }
		}

		/// <summary> The OrderFailedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderFailedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderFailedTitle, value, true); }
		}

		/// <summary> The OrderFailedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderFailedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderFailedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderFailedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderFailedMessage, value, true); }
		}

		/// <summary> The OrderCompletedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderCompletedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompletedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderCompletedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderCompletedTitle, value, true); }
		}

		/// <summary> The OrderCompletedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderCompletedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderCompletedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderCompletedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderCompletedMessage, value, true); }
		}

		/// <summary> The OrderCompletedEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderCompletedEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderCompletedEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderCompletedEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderCompletedEnabled, value, true); }
		}

		/// <summary> The OrderSavingTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderSavingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderSavingTitle, value, true); }
		}

		/// <summary> The OrderSavingMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderSavingMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderSavingMessage, value, true); }
		}

		/// <summary> The ServiceSavingTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceSavingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceSavingTitle, value, true); }
		}

		/// <summary> The ServiceSavingMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceSavingMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceSavingMessage, value, true); }
		}

		/// <summary> The ServiceProcessedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedTitle, value, true); }
		}

		/// <summary> The ServiceProcessedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedMessage, value, true); }
		}

		/// <summary> The RatingSavingTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RatingSavingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingSavingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.RatingSavingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RatingSavingTitle, value, true); }
		}

		/// <summary> The RatingSavingMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RatingSavingMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingSavingMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.RatingSavingMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RatingSavingMessage, value, true); }
		}

		/// <summary> The RatingProcessedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RatingProcessedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingProcessedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.RatingProcessedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RatingProcessedTitle, value, true); }
		}

		/// <summary> The RatingProcessedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RatingProcessedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RatingProcessedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.RatingProcessedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RatingProcessedMessage, value, true); }
		}

		/// <summary> The DailyOrderReset property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DailyOrderReset"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DailyOrderReset
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.DailyOrderReset, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DailyOrderReset, value, true); }
		}

		/// <summary> The SleepTime property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."SleepTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SleepTime
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.SleepTime, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.SleepTime, value, true); }
		}

		/// <summary> The WakeUpTime property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WakeUpTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String WakeUpTime
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WakeUpTime, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WakeUpTime, value, true); }
		}

		/// <summary> The OrderProcessingNotificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessingNotificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderProcessingNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationEnabled, value, true); }
		}

		/// <summary> The OrderProcessingNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessingNotificationTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessingNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotificationTitle, value, true); }
		}

		/// <summary> The OrderProcessingNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessingNotification"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessingNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessingNotification, value, true); }
		}

		/// <summary> The OrderProcessedNotificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedNotificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderProcessedNotificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationEnabled, value, true); }
		}

		/// <summary> The OrderProcessedNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedNotificationTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotificationTitle, value, true); }
		}

		/// <summary> The OrderProcessedNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderProcessedNotification"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderProcessedNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderProcessedNotification, value, true); }
		}

		/// <summary> The FreeformMessageTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."FreeformMessageTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FreeformMessageTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.FreeformMessageTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.FreeformMessageTitle, value, true); }
		}

		/// <summary> The ServiceProcessingNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessingNotificationTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessingNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotificationTitle, value, true); }
		}

		/// <summary> The ServiceProcessingNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessingNotification"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessingNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessingNotification, value, true); }
		}

		/// <summary> The ServiceProcessedNotificationTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedNotificationTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedNotificationTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotificationTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotificationTitle, value, true); }
		}

		/// <summary> The ServiceProcessedNotification property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceProcessedNotification"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceProcessedNotification
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotification, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceProcessedNotification, value, true); }
		}

		/// <summary> The PreorderingEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PreorderingEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PreorderingEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PreorderingEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PreorderingEnabled, value, true); }
		}

		/// <summary> The PreorderingActiveMinutes property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PreorderingActiveMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PreorderingActiveMinutes
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PreorderingActiveMinutes, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PreorderingActiveMinutes, value, true); }
		}

		/// <summary> The AnalyticsOrderingVisible property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AnalyticsOrderingVisible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnalyticsOrderingVisible
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AnalyticsOrderingVisible, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AnalyticsOrderingVisible, value, true); }
		}

		/// <summary> The AnalyticsBestsellersVisible property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AnalyticsBestsellersVisible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnalyticsBestsellersVisible
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AnalyticsBestsellersVisible, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AnalyticsBestsellersVisible, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The PincodeSU property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PincodeSU"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeSU
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PincodeSU, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PincodeSU, value, true); }
		}

		/// <summary> The PincodeGM property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PincodeGM"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PincodeGM
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PincodeGM, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PincodeGM, value, true); }
		}

		/// <summary> The DockedDimLevelDull property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DockedDimLevelDull"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelDull
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelDull, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelDull, value, true); }
		}

		/// <summary> The DockedDimLevelMedium property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DockedDimLevelMedium"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelMedium
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelMedium, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelMedium, value, true); }
		}

		/// <summary> The DockedDimLevelBright property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DockedDimLevelBright"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DockedDimLevelBright
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelBright, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DockedDimLevelBright, value, true); }
		}

		/// <summary> The DeliverypointCaption property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."DeliverypointCaption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointCaption
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.DeliverypointCaption, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.DeliverypointCaption, value, true); }
		}

		/// <summary> The OrderingEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderingEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderingEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderingEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderingEnabled, value, true); }
		}

		/// <summary> The HomepageSlideshowInterval property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HomepageSlideshowInterval"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HomepageSlideshowInterval
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.HomepageSlideshowInterval, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HomepageSlideshowInterval, value, true); }
		}

		/// <summary> The OrderHistoryDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderHistoryDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderHistoryDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.OrderHistoryDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderHistoryDialogEnabled, value, true); }
		}

		/// <summary> The OrderingNotAvailableMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderingNotAvailableMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderingNotAvailableMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.OrderingNotAvailableMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderingNotAvailableMessage, value, true); }
		}

		/// <summary> The GooglePrinterId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GooglePrinterId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterId
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.GooglePrinterId, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GooglePrinterId, value, true); }
		}

		/// <summary> The PmsIntegration property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsIntegration"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsIntegration
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsIntegration, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsIntegration, value, true); }
		}

		/// <summary> The PmsAllowShowBill property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsAllowShowBill"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowBill
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowBill, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowBill, value, true); }
		}

		/// <summary> The PmsAllowExpressCheckout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsAllowExpressCheckout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowExpressCheckout
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsAllowExpressCheckout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsAllowExpressCheckout, value, true); }
		}

		/// <summary> The PmsLockClientWhenNotCheckedIn property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsLockClientWhenNotCheckedIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsLockClientWhenNotCheckedIn
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsLockClientWhenNotCheckedIn, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsLockClientWhenNotCheckedIn, value, true); }
		}

		/// <summary> The WifiAnonymousIdentify property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiAnonymousIdentify"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiAnonymousIdentify
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiAnonymousIdentify, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiAnonymousIdentify, value, true); }
		}

		/// <summary> The WifiEapMode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiEapMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WifiEapMode
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.WifiEapMode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiEapMode, value, true); }
		}

		/// <summary> The WifiIdentity property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiIdentity"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WifiIdentity
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.WifiIdentity, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiIdentity, value, true); }
		}

		/// <summary> The WifiPhase2Authentication property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiPhase2Authentication"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WifiPhase2Authentication
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.WifiPhase2Authentication, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiPhase2Authentication, value, true); }
		}

		/// <summary> The WifiSecurityMethod property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."WifiSecurityMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WifiSecurityMethod
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.WifiSecurityMethod, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.WifiSecurityMethod, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.CompanyId, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The MenuId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.MenuId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MenuId, value, true); }
		}

		/// <summary> The PosdeliverypointgroupId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PosdeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosdeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.PosdeliverypointgroupId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PosdeliverypointgroupId, value, true); }
		}

		/// <summary> The RouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.RouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RouteId, value, true); }
		}

		/// <summary> The SystemMessageRouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."SystemMessageRouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SystemMessageRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.SystemMessageRouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.SystemMessageRouteId, value, true); }
		}

		/// <summary> The XTerminalId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> XTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.XTerminalId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.XTerminalId, value, true); }
		}

		/// <summary> The UIModeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.UIModeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UIModeId, value, true); }
		}

		/// <summary> The OutOfChargeNotificationAmount property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OutOfChargeNotificationAmount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OutOfChargeNotificationAmount
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.OutOfChargeNotificationAmount, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OutOfChargeNotificationAmount, value, true); }
		}

		/// <summary> The IsChargerRemovedDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsChargerRemovedDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedDialogEnabled, value, true); }
		}

		/// <summary> The ChargerRemovedDialogTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedDialogTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedDialogTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogTitle, value, true); }
		}

		/// <summary> The ChargerRemovedDialogText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedDialogText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedDialogText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedDialogText, value, true); }
		}

		/// <summary> The IsChargerRemovedReminderDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsChargerRemovedReminderDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsChargerRemovedReminderDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedReminderDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsChargerRemovedReminderDialogEnabled, value, true); }
		}

		/// <summary> The ChargerRemovedReminderDialogTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedReminderDialogTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedReminderDialogTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogTitle, value, true); }
		}

		/// <summary> The ChargerRemovedReminderDialogText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ChargerRemovedReminderDialogText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargerRemovedReminderDialogText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogText, value, true); }
		}

		/// <summary> The PmsDeviceLockedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsDeviceLockedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsDeviceLockedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedTitle, value, true); }
		}

		/// <summary> The PmsDeviceLockedText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsDeviceLockedText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsDeviceLockedText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsDeviceLockedText, value, true); }
		}

		/// <summary> The PmsCheckinFailedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckinFailedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckinFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedTitle, value, true); }
		}

		/// <summary> The PmsCheckinFailedText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckinFailedText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckinFailedText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckinFailedText, value, true); }
		}

		/// <summary> The PmsRestartTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsRestartTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsRestartTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsRestartTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsRestartTitle, value, true); }
		}

		/// <summary> The PmsRestartText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsRestartText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsRestartText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsRestartText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsRestartText, value, true); }
		}

		/// <summary> The PmsWelcomeTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsWelcomeTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsWelcomeTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTitle, value, true); }
		}

		/// <summary> The PmsWelcomeText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsWelcomeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsWelcomeText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeText, value, true); }
		}

		/// <summary> The PmsCheckoutApproveText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutApproveText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutApproveText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutApproveText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutApproveText, value, true); }
		}

		/// <summary> The PmsWelcomeTimeoutMinutes property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsWelcomeTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsWelcomeTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTimeoutMinutes, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsWelcomeTimeoutMinutes, value, true); }
		}

		/// <summary> The PmsCheckoutCompleteTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutCompleteTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutCompleteTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTitle, value, true); }
		}

		/// <summary> The PmsCheckoutCompleteText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutCompleteText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PmsCheckoutCompleteText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteText, value, true); }
		}

		/// <summary> The PmsCheckoutCompleteTimeoutMinutes property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsCheckoutCompleteTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PmsCheckoutCompleteTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTimeoutMinutes, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTimeoutMinutes, value, true); }
		}

		/// <summary> The AvailableOnObymobi property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AvailableOnObymobi"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AvailableOnObymobi, value, true); }
		}

		/// <summary> The MobileUIModeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MobileUIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MobileUIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.MobileUIModeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MobileUIModeId, value, true); }
		}

		/// <summary> The TabletUIModeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TabletUIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TabletUIModeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.TabletUIModeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.TabletUIModeId, value, true); }
		}

		/// <summary> The PowerButtonHardBehaviour property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerButtonHardBehaviour"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerButtonHardBehaviour
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerButtonHardBehaviour, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerButtonHardBehaviour, value, true); }
		}

		/// <summary> The PowerButtonSoftBehaviour property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PowerButtonSoftBehaviour"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PowerButtonSoftBehaviour
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.PowerButtonSoftBehaviour, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PowerButtonSoftBehaviour, value, true); }
		}

		/// <summary> The ScreenOffMode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ScreenOffMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenOffMode
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ScreenOffMode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ScreenOffMode, value, true); }
		}

		/// <summary> The UseHardKeyboard property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UseHardKeyboard"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.HardwareKeyboardType UseHardKeyboard
		{
			get { return (Obymobi.Enums.HardwareKeyboardType)GetValue((int)DeliverypointgroupFieldIndex.UseHardKeyboard, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UseHardKeyboard, value, true); }
		}

		/// <summary> The GooglePrinterName property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GooglePrinterName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GooglePrinterName
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.GooglePrinterName, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GooglePrinterName, value, true); }
		}

		/// <summary> The RoomserviceCharge property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RoomserviceCharge"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> RoomserviceCharge
		{
			get { return (Nullable<System.Decimal>)GetValue((int)DeliverypointgroupFieldIndex.RoomserviceCharge, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RoomserviceCharge, value, true); }
		}

		/// <summary> The HotSOSBatteryLowProductId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HotSOSBatteryLowProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> HotSOSBatteryLowProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HotSOSBatteryLowProductId, value, true); }
		}

		/// <summary> The EmailDocumentRouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EmailDocumentRouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EmailDocumentRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.EmailDocumentRouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EmailDocumentRouteId, value, true); }
		}

		/// <summary> The EnableBatteryLowConsoleNotifications property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EnableBatteryLowConsoleNotifications"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EnableBatteryLowConsoleNotifications
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.EnableBatteryLowConsoleNotifications, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EnableBatteryLowConsoleNotifications, value, true); }
		}

		/// <summary> The UIThemeId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UIThemeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIThemeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.UIThemeId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UIThemeId, value, true); }
		}

		/// <summary> The ScreensaverMode property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ScreensaverMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ScreensaverMode ScreensaverMode
		{
			get { return (Obymobi.Enums.ScreensaverMode)GetValue((int)DeliverypointgroupFieldIndex.ScreensaverMode, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ScreensaverMode, value, true); }
		}

		/// <summary> The PmsAllowShowGuestName property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PmsAllowShowGuestName"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PmsAllowShowGuestName
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowGuestName, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PmsAllowShowGuestName, value, true); }
		}

		/// <summary> The TurnOffPrivacyTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TurnOffPrivacyTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TurnOffPrivacyTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyTitle, value, true); }
		}

		/// <summary> The TurnOffPrivacyText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."TurnOffPrivacyText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TurnOffPrivacyText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.TurnOffPrivacyText, value, true); }
		}

		/// <summary> The EstimatedDeliveryTime property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."EstimatedDeliveryTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EstimatedDeliveryTime
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.EstimatedDeliveryTime, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.EstimatedDeliveryTime, value, true); }
		}

		/// <summary> The OrderNotesRouteId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."OrderNotesRouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderNotesRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.OrderNotesRouteId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.OrderNotesRouteId, value, true); }
		}

		/// <summary> The UIScheduleId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UIScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UIScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.UIScheduleId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UIScheduleId, value, true); }
		}

		/// <summary> The ItemCurrentlyUnavailableText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ItemCurrentlyUnavailableText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ItemCurrentlyUnavailableText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ItemCurrentlyUnavailableText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ItemCurrentlyUnavailableText, value, true); }
		}

		/// <summary> The ServiceFailedTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceFailedTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceFailedTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceFailedTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceFailedTitle, value, true); }
		}

		/// <summary> The ServiceFailedMessage property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ServiceFailedMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceFailedMessage
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ServiceFailedMessage, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ServiceFailedMessage, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeliverypointgroupFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The AlarmSetWhileNotChargingTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AlarmSetWhileNotChargingTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlarmSetWhileNotChargingTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingTitle, value, true); }
		}

		/// <summary> The AlarmSetWhileNotChargingText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AlarmSetWhileNotChargingText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlarmSetWhileNotChargingText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingText, value, true); }
		}

		/// <summary> The PriceScheduleId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."PriceScheduleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceScheduleId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.PriceScheduleId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.PriceScheduleId, value, true); }
		}

		/// <summary> The ClientConfigurationId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClientConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.ClientConfigurationId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClientConfigurationId, value, true); }
		}

		/// <summary> The IsOrderitemAddedDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsOrderitemAddedDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsOrderitemAddedDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsOrderitemAddedDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsOrderitemAddedDialogEnabled, value, true); }
		}

		/// <summary> The BrowserAgeVerificationEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."BrowserAgeVerificationEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BrowserAgeVerificationEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationEnabled, value, true); }
		}

		/// <summary> The BrowserAgeVerificationLayout property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."BrowserAgeVerificationLayout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.BrowserAgeVerificationLayoutType BrowserAgeVerificationLayout
		{
			get { return (Obymobi.Enums.BrowserAgeVerificationLayoutType)GetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationLayout, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.BrowserAgeVerificationLayout, value, true); }
		}

		/// <summary> The RestartApplicationDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RestartApplicationDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RestartApplicationDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.RestartApplicationDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RestartApplicationDialogEnabled, value, true); }
		}

		/// <summary> The RebootDeviceDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RebootDeviceDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RebootDeviceDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.RebootDeviceDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RebootDeviceDialogEnabled, value, true); }
		}

		/// <summary> The RestartTimeoutSeconds property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."RestartTimeoutSeconds"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RestartTimeoutSeconds
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.RestartTimeoutSeconds, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.RestartTimeoutSeconds, value, true); }
		}

		/// <summary> The CraveAnalytics property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."CraveAnalytics"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CraveAnalytics
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.CraveAnalytics, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.CraveAnalytics, value, true); }
		}

		/// <summary> The GeoFencingEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GeoFencingEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean GeoFencingEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.GeoFencingEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GeoFencingEnabled, value, true); }
		}

		/// <summary> The GeoFencingRadius property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."GeoFencingRadius"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GeoFencingRadius
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.GeoFencingRadius, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.GeoFencingRadius, value, true); }
		}

		/// <summary> The HideCompanyDetails property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."HideCompanyDetails"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HideCompanyDetails
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.HideCompanyDetails, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.HideCompanyDetails, value, true); }
		}

		/// <summary> The IsClearBasketDialogEnabled property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."IsClearBasketDialogEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsClearBasketDialogEnabled
		{
			get { return (System.Boolean)GetValue((int)DeliverypointgroupFieldIndex.IsClearBasketDialogEnabled, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.IsClearBasketDialogEnabled, value, true); }
		}

		/// <summary> The ClearBasketTitle property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClearBasketTitle"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClearBasketTitle
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ClearBasketTitle, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClearBasketTitle, value, true); }
		}

		/// <summary> The ClearBasketText property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ClearBasketText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClearBasketText
		{
			get { return (System.String)GetValue((int)DeliverypointgroupFieldIndex.ClearBasketText, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ClearBasketText, value, true); }
		}

		/// <summary> The ApiVersion property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."ApiVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApiVersion
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.ApiVersion, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.ApiVersion, value, true); }
		}

		/// <summary> The MessagingVersion property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."MessagingVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagingVersion
		{
			get { return (System.Int32)GetValue((int)DeliverypointgroupFieldIndex.MessagingVersion, true); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.MessagingVersion, value, true); }
		}

		/// <summary> The AddressId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AddressId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AddressId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.AddressId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AddressId, value, true); }
		}

		/// <summary> The AffiliateCampaignId property of the Entity Deliverypointgroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Deliverypointgroup"."AffiliateCampaignId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AffiliateCampaignId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeliverypointgroupFieldIndex.AffiliateCampaignId, false); }
			set	{ SetValue((int)DeliverypointgroupFieldIndex.AffiliateCampaignId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollection
		{
			get	{ return GetMultiAnnouncementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollection. When set to true, AnnouncementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollection
		{
			get	{ return _alwaysFetchAnnouncementCollection; }
			set	{ _alwaysFetchAnnouncementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollection already has been fetched. Setting this property to false when AnnouncementCollection has been fetched
		/// will clear the AnnouncementCollection collection well. Setting this property to true while AnnouncementCollection hasn't been fetched disables lazy loading for AnnouncementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollection
		{
			get { return _alreadyFetchedAnnouncementCollection;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollection && !value && (_announcementCollection != null))
				{
					_announcementCollection.Clear();
				}
				_alreadyFetchedAnnouncementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CheckoutMethodDeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCheckoutMethodDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CheckoutMethodDeliverypointgroupCollection CheckoutMethodDeliverypointgroupCollection
		{
			get	{ return GetMultiCheckoutMethodDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CheckoutMethodDeliverypointgroupCollection. When set to true, CheckoutMethodDeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CheckoutMethodDeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCheckoutMethodDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCheckoutMethodDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchCheckoutMethodDeliverypointgroupCollection; }
			set	{ _alwaysFetchCheckoutMethodDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CheckoutMethodDeliverypointgroupCollection already has been fetched. Setting this property to false when CheckoutMethodDeliverypointgroupCollection has been fetched
		/// will clear the CheckoutMethodDeliverypointgroupCollection collection well. Setting this property to true while CheckoutMethodDeliverypointgroupCollection hasn't been fetched disables lazy loading for CheckoutMethodDeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCheckoutMethodDeliverypointgroupCollection
		{
			get { return _alreadyFetchedCheckoutMethodDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedCheckoutMethodDeliverypointgroupCollection && !value && (_checkoutMethodDeliverypointgroupCollection != null))
				{
					_checkoutMethodDeliverypointgroupCollection.Clear();
				}
				_alreadyFetchedCheckoutMethodDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollection
		{
			get	{ return GetMultiClientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollection. When set to true, ClientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollection
		{
			get	{ return _alwaysFetchClientCollection; }
			set	{ _alwaysFetchClientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollection already has been fetched. Setting this property to false when ClientCollection has been fetched
		/// will clear the ClientCollection collection well. Setting this property to true while ClientCollection hasn't been fetched disables lazy loading for ClientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollection
		{
			get { return _alreadyFetchedClientCollection;}
			set 
			{
				if(_alreadyFetchedClientCollection && !value && (_clientCollection != null))
				{
					_clientCollection.Clear();
				}
				_alreadyFetchedClientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection ClientConfigurationCollection
		{
			get	{ return GetMultiClientConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationCollection. When set to true, ClientConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationCollection
		{
			get	{ return _alwaysFetchClientConfigurationCollection; }
			set	{ _alwaysFetchClientConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationCollection already has been fetched. Setting this property to false when ClientConfigurationCollection has been fetched
		/// will clear the ClientConfigurationCollection collection well. Setting this property to true while ClientConfigurationCollection hasn't been fetched disables lazy loading for ClientConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationCollection
		{
			get { return _alreadyFetchedClientConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedClientConfigurationCollection && !value && (_clientConfigurationCollection != null))
				{
					_clientConfigurationCollection.Clear();
				}
				_alreadyFetchedClientConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollection
		{
			get	{ return GetMultiDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollection. When set to true, DeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollection
		{
			get	{ return _alwaysFetchDeliverypointCollection; }
			set	{ _alwaysFetchDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollection already has been fetched. Setting this property to false when DeliverypointCollection has been fetched
		/// will clear the DeliverypointCollection collection well. Setting this property to true while DeliverypointCollection hasn't been fetched disables lazy loading for DeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollection
		{
			get { return _alreadyFetchedDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollection && !value && (_deliverypointCollection != null))
				{
					_deliverypointCollection.Clear();
				}
				_alreadyFetchedDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupAdvertisementCollection DeliverypointgroupAdvertisementCollection
		{
			get	{ return GetMultiDeliverypointgroupAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupAdvertisementCollection. When set to true, DeliverypointgroupAdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupAdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupAdvertisementCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupAdvertisementCollection; }
			set	{ _alwaysFetchDeliverypointgroupAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupAdvertisementCollection already has been fetched. Setting this property to false when DeliverypointgroupAdvertisementCollection has been fetched
		/// will clear the DeliverypointgroupAdvertisementCollection collection well. Setting this property to true while DeliverypointgroupAdvertisementCollection hasn't been fetched disables lazy loading for DeliverypointgroupAdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupAdvertisementCollection
		{
			get { return _alreadyFetchedDeliverypointgroupAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupAdvertisementCollection && !value && (_deliverypointgroupAdvertisementCollection != null))
				{
					_deliverypointgroupAdvertisementCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupAnnouncementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupAnnouncementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupAnnouncementCollection DeliverypointgroupAnnouncementCollection
		{
			get	{ return GetMultiDeliverypointgroupAnnouncementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupAnnouncementCollection. When set to true, DeliverypointgroupAnnouncementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupAnnouncementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupAnnouncementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupAnnouncementCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupAnnouncementCollection; }
			set	{ _alwaysFetchDeliverypointgroupAnnouncementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupAnnouncementCollection already has been fetched. Setting this property to false when DeliverypointgroupAnnouncementCollection has been fetched
		/// will clear the DeliverypointgroupAnnouncementCollection collection well. Setting this property to true while DeliverypointgroupAnnouncementCollection hasn't been fetched disables lazy loading for DeliverypointgroupAnnouncementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupAnnouncementCollection
		{
			get { return _alreadyFetchedDeliverypointgroupAnnouncementCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupAnnouncementCollection && !value && (_deliverypointgroupAnnouncementCollection != null))
				{
					_deliverypointgroupAnnouncementCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupAnnouncementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntertainmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupEntertainmentCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupEntertainmentCollection DeliverypointgroupEntertainmentCollection
		{
			get	{ return GetMultiDeliverypointgroupEntertainmentCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntertainmentCollection. When set to true, DeliverypointgroupEntertainmentCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntertainmentCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupEntertainmentCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntertainmentCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupEntertainmentCollection; }
			set	{ _alwaysFetchDeliverypointgroupEntertainmentCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntertainmentCollection already has been fetched. Setting this property to false when DeliverypointgroupEntertainmentCollection has been fetched
		/// will clear the DeliverypointgroupEntertainmentCollection collection well. Setting this property to true while DeliverypointgroupEntertainmentCollection hasn't been fetched disables lazy loading for DeliverypointgroupEntertainmentCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntertainmentCollection
		{
			get { return _alreadyFetchedDeliverypointgroupEntertainmentCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntertainmentCollection && !value && (_deliverypointgroupEntertainmentCollection != null))
				{
					_deliverypointgroupEntertainmentCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupEntertainmentCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection DeliverypointgroupLanguageCollection
		{
			get	{ return GetMultiDeliverypointgroupLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupLanguageCollection. When set to true, DeliverypointgroupLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupLanguageCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupLanguageCollection; }
			set	{ _alwaysFetchDeliverypointgroupLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupLanguageCollection already has been fetched. Setting this property to false when DeliverypointgroupLanguageCollection has been fetched
		/// will clear the DeliverypointgroupLanguageCollection collection well. Setting this property to true while DeliverypointgroupLanguageCollection hasn't been fetched disables lazy loading for DeliverypointgroupLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupLanguageCollection
		{
			get { return _alreadyFetchedDeliverypointgroupLanguageCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupLanguageCollection && !value && (_deliverypointgroupLanguageCollection != null))
				{
					_deliverypointgroupLanguageCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupOccupancyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupOccupancyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupOccupancyCollection DeliverypointgroupOccupancyCollection
		{
			get	{ return GetMultiDeliverypointgroupOccupancyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupOccupancyCollection. When set to true, DeliverypointgroupOccupancyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupOccupancyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupOccupancyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupOccupancyCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupOccupancyCollection; }
			set	{ _alwaysFetchDeliverypointgroupOccupancyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupOccupancyCollection already has been fetched. Setting this property to false when DeliverypointgroupOccupancyCollection has been fetched
		/// will clear the DeliverypointgroupOccupancyCollection collection well. Setting this property to true while DeliverypointgroupOccupancyCollection hasn't been fetched disables lazy loading for DeliverypointgroupOccupancyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupOccupancyCollection
		{
			get { return _alreadyFetchedDeliverypointgroupOccupancyCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupOccupancyCollection && !value && (_deliverypointgroupOccupancyCollection != null))
				{
					_deliverypointgroupOccupancyCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupOccupancyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupProductCollection DeliverypointgroupProductCollection
		{
			get	{ return GetMultiDeliverypointgroupProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupProductCollection. When set to true, DeliverypointgroupProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupProductCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupProductCollection; }
			set	{ _alwaysFetchDeliverypointgroupProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupProductCollection already has been fetched. Setting this property to false when DeliverypointgroupProductCollection has been fetched
		/// will clear the DeliverypointgroupProductCollection collection well. Setting this property to true while DeliverypointgroupProductCollection hasn't been fetched disables lazy loading for DeliverypointgroupProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupProductCollection
		{
			get { return _alreadyFetchedDeliverypointgroupProductCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupProductCollection && !value && (_deliverypointgroupProductCollection != null))
				{
					_deliverypointgroupProductCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupProductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection NetmessageCollection
		{
			get	{ return GetMultiNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NetmessageCollection. When set to true, NetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNetmessageCollection
		{
			get	{ return _alwaysFetchNetmessageCollection; }
			set	{ _alwaysFetchNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NetmessageCollection already has been fetched. Setting this property to false when NetmessageCollection has been fetched
		/// will clear the NetmessageCollection collection well. Setting this property to true while NetmessageCollection hasn't been fetched disables lazy loading for NetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNetmessageCollection
		{
			get { return _alreadyFetchedNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedNetmessageCollection && !value && (_netmessageCollection != null))
				{
					_netmessageCollection.Clear();
				}
				_alreadyFetchedNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ServiceMethodDeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiServiceMethodDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ServiceMethodDeliverypointgroupCollection ServiceMethodDeliverypointgroupCollection
		{
			get	{ return GetMultiServiceMethodDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceMethodDeliverypointgroupCollection. When set to true, ServiceMethodDeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceMethodDeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiServiceMethodDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceMethodDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchServiceMethodDeliverypointgroupCollection; }
			set	{ _alwaysFetchServiceMethodDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceMethodDeliverypointgroupCollection already has been fetched. Setting this property to false when ServiceMethodDeliverypointgroupCollection has been fetched
		/// will clear the ServiceMethodDeliverypointgroupCollection collection well. Setting this property to true while ServiceMethodDeliverypointgroupCollection hasn't been fetched disables lazy loading for ServiceMethodDeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceMethodDeliverypointgroupCollection
		{
			get { return _alreadyFetchedServiceMethodDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedServiceMethodDeliverypointgroupCollection && !value && (_serviceMethodDeliverypointgroupCollection != null))
				{
					_serviceMethodDeliverypointgroupCollection.Clear();
				}
				_alreadyFetchedServiceMethodDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SetupCodeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSetupCodeCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SetupCodeCollection SetupCodeCollection
		{
			get	{ return GetMultiSetupCodeCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SetupCodeCollection. When set to true, SetupCodeCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SetupCodeCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSetupCodeCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSetupCodeCollection
		{
			get	{ return _alwaysFetchSetupCodeCollection; }
			set	{ _alwaysFetchSetupCodeCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SetupCodeCollection already has been fetched. Setting this property to false when SetupCodeCollection has been fetched
		/// will clear the SetupCodeCollection collection well. Setting this property to true while SetupCodeCollection hasn't been fetched disables lazy loading for SetupCodeCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSetupCodeCollection
		{
			get { return _alreadyFetchedSetupCodeCollection;}
			set 
			{
				if(_alreadyFetchedSetupCodeCollection && !value && (_setupCodeCollection != null))
				{
					_setupCodeCollection.Clear();
				}
				_alreadyFetchedSetupCodeCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollection
		{
			get	{ return GetMultiTerminalCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollection. When set to true, TerminalCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollection
		{
			get	{ return _alwaysFetchTerminalCollection; }
			set	{ _alwaysFetchTerminalCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollection already has been fetched. Setting this property to false when TerminalCollection has been fetched
		/// will clear the TerminalCollection collection well. Setting this property to true while TerminalCollection hasn't been fetched disables lazy loading for TerminalCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollection
		{
			get { return _alreadyFetchedTerminalCollection;}
			set 
			{
				if(_alreadyFetchedTerminalCollection && !value && (_terminalCollection != null))
				{
					_terminalCollection.Clear();
				}
				_alreadyFetchedTerminalCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleCollection UIScheduleCollection
		{
			get	{ return GetMultiUIScheduleCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleCollection. When set to true, UIScheduleCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleCollection
		{
			get	{ return _alwaysFetchUIScheduleCollection; }
			set	{ _alwaysFetchUIScheduleCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleCollection already has been fetched. Setting this property to false when UIScheduleCollection has been fetched
		/// will clear the UIScheduleCollection collection well. Setting this property to true while UIScheduleCollection hasn't been fetched disables lazy loading for UIScheduleCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleCollection
		{
			get { return _alreadyFetchedUIScheduleCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleCollection && !value && (_uIScheduleCollection != null))
				{
					_uIScheduleCollection.Clear();
				}
				_alreadyFetchedUIScheduleCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollectionViaDeliverypointgroupAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollectionViaDeliverypointgroupAdvertisement
		{
			get { return GetMultiAdvertisementCollectionViaDeliverypointgroupAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollectionViaDeliverypointgroupAdvertisement. When set to true, AdvertisementCollectionViaDeliverypointgroupAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollectionViaDeliverypointgroupAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiAdvertisementCollectionViaDeliverypointgroupAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement
		{
			get	{ return _alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement; }
			set	{ _alwaysFetchAdvertisementCollectionViaDeliverypointgroupAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollectionViaDeliverypointgroupAdvertisement already has been fetched. Setting this property to false when AdvertisementCollectionViaDeliverypointgroupAdvertisement has been fetched
		/// will clear the AdvertisementCollectionViaDeliverypointgroupAdvertisement collection well. Setting this property to true while AdvertisementCollectionViaDeliverypointgroupAdvertisement hasn't been fetched disables lazy loading for AdvertisementCollectionViaDeliverypointgroupAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement
		{
			get { return _alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement && !value && (_advertisementCollectionViaDeliverypointgroupAdvertisement != null))
				{
					_advertisementCollectionViaDeliverypointgroupAdvertisement.Clear();
				}
				_alreadyFetchedAdvertisementCollectionViaDeliverypointgroupAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAdvertisement
		{
			get { return GetMultiCategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAdvertisement. When set to true, CategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchCategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchCategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when CategoryCollectionViaAdvertisement has been fetched
		/// will clear the CategoryCollectionViaAdvertisement collection well. Setting this property to true while CategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for CategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedCategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAdvertisement && !value && (_categoryCollectionViaAdvertisement != null))
				{
					_categoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAnnouncement
		{
			get { return GetMultiCategoryCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAnnouncement. When set to true, CategoryCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchCategoryCollectionViaAnnouncement; }
			set	{ _alwaysFetchCategoryCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAnnouncement already has been fetched. Setting this property to false when CategoryCollectionViaAnnouncement has been fetched
		/// will clear the CategoryCollectionViaAnnouncement collection well. Setting this property to true while CategoryCollectionViaAnnouncement hasn't been fetched disables lazy loading for CategoryCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAnnouncement
		{
			get { return _alreadyFetchedCategoryCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAnnouncement && !value && (_categoryCollectionViaAnnouncement != null))
				{
					_categoryCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAnnouncement_
		{
			get { return GetMultiCategoryCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAnnouncement_. When set to true, CategoryCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchCategoryCollectionViaAnnouncement_; }
			set	{ _alwaysFetchCategoryCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAnnouncement_ already has been fetched. Setting this property to false when CategoryCollectionViaAnnouncement_ has been fetched
		/// will clear the CategoryCollectionViaAnnouncement_ collection well. Setting this property to true while CategoryCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for CategoryCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedCategoryCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAnnouncement_ && !value && (_categoryCollectionViaAnnouncement_ != null))
				{
					_categoryCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollectionViaNetmessage
		{
			get { return GetMultiClientCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollectionViaNetmessage. When set to true, ClientCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiClientCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollectionViaNetmessage
		{
			get	{ return _alwaysFetchClientCollectionViaNetmessage; }
			set	{ _alwaysFetchClientCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollectionViaNetmessage already has been fetched. Setting this property to false when ClientCollectionViaNetmessage has been fetched
		/// will clear the ClientCollectionViaNetmessage collection well. Setting this property to true while ClientCollectionViaNetmessage hasn't been fetched disables lazy loading for ClientCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollectionViaNetmessage
		{
			get { return _alreadyFetchedClientCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedClientCollectionViaNetmessage && !value && (_clientCollectionViaNetmessage != null))
				{
					_clientCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedClientCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaAdvertisement
		{
			get { return GetMultiCompanyCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaAdvertisement. When set to true, CompanyCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchCompanyCollectionViaAdvertisement; }
			set	{ _alwaysFetchCompanyCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaAdvertisement already has been fetched. Setting this property to false when CompanyCollectionViaAdvertisement has been fetched
		/// will clear the CompanyCollectionViaAdvertisement collection well. Setting this property to true while CompanyCollectionViaAdvertisement hasn't been fetched disables lazy loading for CompanyCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaAdvertisement
		{
			get { return _alreadyFetchedCompanyCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaAdvertisement && !value && (_companyCollectionViaAdvertisement != null))
				{
					_companyCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedCompanyCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaAnnouncement
		{
			get { return GetMultiCompanyCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaAnnouncement. When set to true, CompanyCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchCompanyCollectionViaAnnouncement; }
			set	{ _alwaysFetchCompanyCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaAnnouncement already has been fetched. Setting this property to false when CompanyCollectionViaAnnouncement has been fetched
		/// will clear the CompanyCollectionViaAnnouncement collection well. Setting this property to true while CompanyCollectionViaAnnouncement hasn't been fetched disables lazy loading for CompanyCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaAnnouncement
		{
			get { return _alreadyFetchedCompanyCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaAnnouncement && !value && (_companyCollectionViaAnnouncement != null))
				{
					_companyCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedCompanyCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaNetmessage
		{
			get { return GetMultiCompanyCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaNetmessage. When set to true, CompanyCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaNetmessage
		{
			get	{ return _alwaysFetchCompanyCollectionViaNetmessage; }
			set	{ _alwaysFetchCompanyCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaNetmessage already has been fetched. Setting this property to false when CompanyCollectionViaNetmessage has been fetched
		/// will clear the CompanyCollectionViaNetmessage collection well. Setting this property to true while CompanyCollectionViaNetmessage hasn't been fetched disables lazy loading for CompanyCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaNetmessage
		{
			get { return _alreadyFetchedCompanyCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaNetmessage && !value && (_companyCollectionViaNetmessage != null))
				{
					_companyCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedCompanyCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaNetmessage_
		{
			get { return GetMultiCompanyCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaNetmessage_. When set to true, CompanyCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchCompanyCollectionViaNetmessage_; }
			set	{ _alwaysFetchCompanyCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaNetmessage_ already has been fetched. Setting this property to false when CompanyCollectionViaNetmessage_ has been fetched
		/// will clear the CompanyCollectionViaNetmessage_ collection well. Setting this property to true while CompanyCollectionViaNetmessage_ hasn't been fetched disables lazy loading for CompanyCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaNetmessage_
		{
			get { return _alreadyFetchedCompanyCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaNetmessage_ && !value && (_companyCollectionViaNetmessage_ != null))
				{
					_companyCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedCompanyCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaTerminal
		{
			get { return GetMultiCompanyCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaTerminal. When set to true, CompanyCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaTerminal
		{
			get	{ return _alwaysFetchCompanyCollectionViaTerminal; }
			set	{ _alwaysFetchCompanyCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaTerminal already has been fetched. Setting this property to false when CompanyCollectionViaTerminal has been fetched
		/// will clear the CompanyCollectionViaTerminal collection well. Setting this property to true while CompanyCollectionViaTerminal hasn't been fetched disables lazy loading for CompanyCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaTerminal
		{
			get { return _alreadyFetchedCompanyCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaTerminal && !value && (_companyCollectionViaTerminal != null))
				{
					_companyCollectionViaTerminal.Clear();
				}
				_alreadyFetchedCompanyCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaNetmessage
		{
			get { return GetMultiCustomerCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaNetmessage. When set to true, CustomerCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaNetmessage
		{
			get	{ return _alwaysFetchCustomerCollectionViaNetmessage; }
			set	{ _alwaysFetchCustomerCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaNetmessage already has been fetched. Setting this property to false when CustomerCollectionViaNetmessage has been fetched
		/// will clear the CustomerCollectionViaNetmessage collection well. Setting this property to true while CustomerCollectionViaNetmessage hasn't been fetched disables lazy loading for CustomerCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaNetmessage
		{
			get { return _alreadyFetchedCustomerCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaNetmessage && !value && (_customerCollectionViaNetmessage != null))
				{
					_customerCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedCustomerCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CustomerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomerCollection CustomerCollectionViaNetmessage_
		{
			get { return GetMultiCustomerCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerCollectionViaNetmessage_. When set to true, CustomerCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiCustomerCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchCustomerCollectionViaNetmessage_; }
			set	{ _alwaysFetchCustomerCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerCollectionViaNetmessage_ already has been fetched. Setting this property to false when CustomerCollectionViaNetmessage_ has been fetched
		/// will clear the CustomerCollectionViaNetmessage_ collection well. Setting this property to true while CustomerCollectionViaNetmessage_ hasn't been fetched disables lazy loading for CustomerCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerCollectionViaNetmessage_
		{
			get { return _alreadyFetchedCustomerCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedCustomerCollectionViaNetmessage_ && !value && (_customerCollectionViaNetmessage_ != null))
				{
					_customerCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedCustomerCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaClient
		{
			get { return GetMultiDeliverypointCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaClient. When set to true, DeliverypointCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaClient
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaClient; }
			set	{ _alwaysFetchDeliverypointCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaClient already has been fetched. Setting this property to false when DeliverypointCollectionViaClient has been fetched
		/// will clear the DeliverypointCollectionViaClient collection well. Setting this property to true while DeliverypointCollectionViaClient hasn't been fetched disables lazy loading for DeliverypointCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaClient
		{
			get { return _alreadyFetchedDeliverypointCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaClient && !value && (_deliverypointCollectionViaClient != null))
				{
					_deliverypointCollectionViaClient.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaClient_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaClient_
		{
			get { return GetMultiDeliverypointCollectionViaClient_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaClient_. When set to true, DeliverypointCollectionViaClient_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaClient_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaClient_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaClient_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaClient_; }
			set	{ _alwaysFetchDeliverypointCollectionViaClient_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaClient_ already has been fetched. Setting this property to false when DeliverypointCollectionViaClient_ has been fetched
		/// will clear the DeliverypointCollectionViaClient_ collection well. Setting this property to true while DeliverypointCollectionViaClient_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaClient_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaClient_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaClient_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaClient_ && !value && (_deliverypointCollectionViaClient_ != null))
				{
					_deliverypointCollectionViaClient_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaClient_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaNetmessage
		{
			get { return GetMultiDeliverypointCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaNetmessage. When set to true, DeliverypointCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaNetmessage
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaNetmessage; }
			set	{ _alwaysFetchDeliverypointCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaNetmessage already has been fetched. Setting this property to false when DeliverypointCollectionViaNetmessage has been fetched
		/// will clear the DeliverypointCollectionViaNetmessage collection well. Setting this property to true while DeliverypointCollectionViaNetmessage hasn't been fetched disables lazy loading for DeliverypointCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaNetmessage
		{
			get { return _alreadyFetchedDeliverypointCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaNetmessage && !value && (_deliverypointCollectionViaNetmessage != null))
				{
					_deliverypointCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaNetmessage_
		{
			get { return GetMultiDeliverypointCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaNetmessage_. When set to true, DeliverypointCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaNetmessage_; }
			set	{ _alwaysFetchDeliverypointCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaNetmessage_ already has been fetched. Setting this property to false when DeliverypointCollectionViaNetmessage_ has been fetched
		/// will clear the DeliverypointCollectionViaNetmessage_ collection well. Setting this property to true while DeliverypointCollectionViaNetmessage_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaNetmessage_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaNetmessage_ && !value && (_deliverypointCollectionViaNetmessage_ != null))
				{
					_deliverypointCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal
		{
			get { return GetMultiDeliverypointCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal. When set to true, DeliverypointCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal has been fetched
		/// will clear the DeliverypointCollectionViaTerminal collection well. Setting this property to true while DeliverypointCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal && !value && (_deliverypointCollectionViaTerminal != null))
				{
					_deliverypointCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal_
		{
			get { return GetMultiDeliverypointCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal_. When set to true, DeliverypointCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal_; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal_ already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal_ has been fetched
		/// will clear the DeliverypointCollectionViaTerminal_ collection well. Setting this property to true while DeliverypointCollectionViaTerminal_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal_ && !value && (_deliverypointCollectionViaTerminal_ != null))
				{
					_deliverypointCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaClient
		{
			get { return GetMultiDeviceCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaClient. When set to true, DeviceCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaClient
		{
			get	{ return _alwaysFetchDeviceCollectionViaClient; }
			set	{ _alwaysFetchDeviceCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaClient already has been fetched. Setting this property to false when DeviceCollectionViaClient has been fetched
		/// will clear the DeviceCollectionViaClient collection well. Setting this property to true while DeviceCollectionViaClient hasn't been fetched disables lazy loading for DeviceCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaClient
		{
			get { return _alreadyFetchedDeviceCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaClient && !value && (_deviceCollectionViaClient != null))
				{
					_deviceCollectionViaClient.Clear();
				}
				_alreadyFetchedDeviceCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollectionViaTerminal
		{
			get { return GetMultiDeviceCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollectionViaTerminal. When set to true, DeviceCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeviceCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeviceCollectionViaTerminal; }
			set	{ _alwaysFetchDeviceCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollectionViaTerminal already has been fetched. Setting this property to false when DeviceCollectionViaTerminal has been fetched
		/// will clear the DeviceCollectionViaTerminal collection well. Setting this property to true while DeviceCollectionViaTerminal hasn't been fetched disables lazy loading for DeviceCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollectionViaTerminal
		{
			get { return _alreadyFetchedDeviceCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeviceCollectionViaTerminal && !value && (_deviceCollectionViaTerminal != null))
				{
					_deviceCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeviceCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal
		{
			get { return GetMultiEntertainmentCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal. When set to true, EntertainmentCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal has been fetched
		/// will clear the EntertainmentCollectionViaTerminal collection well. Setting this property to true while EntertainmentCollectionViaTerminal hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal && !value && (_entertainmentCollectionViaTerminal != null))
				{
					_entertainmentCollectionViaTerminal.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal_
		{
			get { return GetMultiEntertainmentCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal_. When set to true, EntertainmentCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal_; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal_ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal_ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal_ collection well. Setting this property to true while EntertainmentCollectionViaTerminal_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal_ && !value && (_entertainmentCollectionViaTerminal_ != null))
				{
					_entertainmentCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal__
		{
			get { return GetMultiEntertainmentCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal__. When set to true, EntertainmentCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal__
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal__; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal__ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal__ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal__ collection well. Setting this property to true while EntertainmentCollectionViaTerminal__ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal__
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal__ && !value && (_entertainmentCollectionViaTerminal__ != null))
				{
					_entertainmentCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement. When set to true, EntertainmentCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement && !value && (_entertainmentCollectionViaAdvertisement != null))
				{
					_entertainmentCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement_
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement_. When set to true, EntertainmentCollectionViaAdvertisement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement_; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement_ already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement_ has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement_ collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement_ && !value && (_entertainmentCollectionViaAdvertisement_ != null))
				{
					_entertainmentCollectionViaAdvertisement_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAnnouncement
		{
			get { return GetMultiEntertainmentCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAnnouncement. When set to true, EntertainmentCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAnnouncement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAnnouncement already has been fetched. Setting this property to false when EntertainmentCollectionViaAnnouncement has been fetched
		/// will clear the EntertainmentCollectionViaAnnouncement collection well. Setting this property to true while EntertainmentCollectionViaAnnouncement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAnnouncement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAnnouncement && !value && (_entertainmentCollectionViaAnnouncement != null))
				{
					_entertainmentCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaDeliverypointgroupEntertainment()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaDeliverypointgroupEntertainment
		{
			get { return GetMultiEntertainmentCollectionViaDeliverypointgroupEntertainment(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaDeliverypointgroupEntertainment. When set to true, EntertainmentCollectionViaDeliverypointgroupEntertainment is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaDeliverypointgroupEntertainment is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaDeliverypointgroupEntertainment(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment; }
			set	{ _alwaysFetchEntertainmentCollectionViaDeliverypointgroupEntertainment = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaDeliverypointgroupEntertainment already has been fetched. Setting this property to false when EntertainmentCollectionViaDeliverypointgroupEntertainment has been fetched
		/// will clear the EntertainmentCollectionViaDeliverypointgroupEntertainment collection well. Setting this property to true while EntertainmentCollectionViaDeliverypointgroupEntertainment hasn't been fetched disables lazy loading for EntertainmentCollectionViaDeliverypointgroupEntertainment</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment
		{
			get { return _alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment && !value && (_entertainmentCollectionViaDeliverypointgroupEntertainment != null))
				{
					_entertainmentCollectionViaDeliverypointgroupEntertainment.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaDeliverypointgroupEntertainment = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAdvertisement. When set to true, EntertainmentcategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentcategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement && !value && (_entertainmentcategoryCollectionViaAdvertisement != null))
				{
					_entertainmentcategoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAnnouncement
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAnnouncement. When set to true, EntertainmentcategoryCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAnnouncement already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAnnouncement has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAnnouncement collection well. Setting this property to true while EntertainmentcategoryCollectionViaAnnouncement hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAnnouncement
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement && !value && (_entertainmentcategoryCollectionViaAnnouncement != null))
				{
					_entertainmentcategoryCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAnnouncement_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAnnouncement_
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAnnouncement_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAnnouncement_. When set to true, EntertainmentcategoryCollectionViaAnnouncement_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAnnouncement_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAnnouncement_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAnnouncement_
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAnnouncement_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAnnouncement_ already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAnnouncement_ has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAnnouncement_ collection well. Setting this property to true while EntertainmentcategoryCollectionViaAnnouncement_ hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAnnouncement_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ && !value && (_entertainmentcategoryCollectionViaAnnouncement_ != null))
				{
					_entertainmentcategoryCollectionViaAnnouncement_.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAnnouncement_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaAdvertisement
		{
			get { return GetMultiGenericproductCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaAdvertisement. When set to true, GenericproductCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchGenericproductCollectionViaAdvertisement; }
			set	{ _alwaysFetchGenericproductCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaAdvertisement already has been fetched. Setting this property to false when GenericproductCollectionViaAdvertisement has been fetched
		/// will clear the GenericproductCollectionViaAdvertisement collection well. Setting this property to true while GenericproductCollectionViaAdvertisement hasn't been fetched disables lazy loading for GenericproductCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaAdvertisement
		{
			get { return _alreadyFetchedGenericproductCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaAdvertisement && !value && (_genericproductCollectionViaAdvertisement != null))
				{
					_genericproductCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIcrtouchprintermappingCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection IcrtouchprintermappingCollectionViaTerminal
		{
			get { return GetMultiIcrtouchprintermappingCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingCollectionViaTerminal. When set to true, IcrtouchprintermappingCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiIcrtouchprintermappingCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingCollectionViaTerminal
		{
			get	{ return _alwaysFetchIcrtouchprintermappingCollectionViaTerminal; }
			set	{ _alwaysFetchIcrtouchprintermappingCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingCollectionViaTerminal already has been fetched. Setting this property to false when IcrtouchprintermappingCollectionViaTerminal has been fetched
		/// will clear the IcrtouchprintermappingCollectionViaTerminal collection well. Setting this property to true while IcrtouchprintermappingCollectionViaTerminal hasn't been fetched disables lazy loading for IcrtouchprintermappingCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingCollectionViaTerminal
		{
			get { return _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal && !value && (_icrtouchprintermappingCollectionViaTerminal != null))
				{
					_icrtouchprintermappingCollectionViaTerminal.Clear();
				}
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaAnnouncement
		{
			get { return GetMultiMediaCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaAnnouncement. When set to true, MediaCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchMediaCollectionViaAnnouncement; }
			set	{ _alwaysFetchMediaCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaAnnouncement already has been fetched. Setting this property to false when MediaCollectionViaAnnouncement has been fetched
		/// will clear the MediaCollectionViaAnnouncement collection well. Setting this property to true while MediaCollectionViaAnnouncement hasn't been fetched disables lazy loading for MediaCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaAnnouncement
		{
			get { return _alreadyFetchedMediaCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaAnnouncement && !value && (_mediaCollectionViaAnnouncement != null))
				{
					_mediaCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedMediaCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAnnouncement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAnnouncement
		{
			get { return GetMultiProductCollectionViaAnnouncement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAnnouncement. When set to true, ProductCollectionViaAnnouncement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAnnouncement is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAnnouncement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAnnouncement
		{
			get	{ return _alwaysFetchProductCollectionViaAnnouncement; }
			set	{ _alwaysFetchProductCollectionViaAnnouncement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAnnouncement already has been fetched. Setting this property to false when ProductCollectionViaAnnouncement has been fetched
		/// will clear the ProductCollectionViaAnnouncement collection well. Setting this property to true while ProductCollectionViaAnnouncement hasn't been fetched disables lazy loading for ProductCollectionViaAnnouncement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAnnouncement
		{
			get { return _alreadyFetchedProductCollectionViaAnnouncement;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAnnouncement && !value && (_productCollectionViaAnnouncement != null))
				{
					_productCollectionViaAnnouncement.Clear();
				}
				_alreadyFetchedProductCollectionViaAnnouncement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaDeliverypointgroupProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaDeliverypointgroupProduct
		{
			get { return GetMultiProductCollectionViaDeliverypointgroupProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaDeliverypointgroupProduct. When set to true, ProductCollectionViaDeliverypointgroupProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaDeliverypointgroupProduct is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaDeliverypointgroupProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaDeliverypointgroupProduct
		{
			get	{ return _alwaysFetchProductCollectionViaDeliverypointgroupProduct; }
			set	{ _alwaysFetchProductCollectionViaDeliverypointgroupProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaDeliverypointgroupProduct already has been fetched. Setting this property to false when ProductCollectionViaDeliverypointgroupProduct has been fetched
		/// will clear the ProductCollectionViaDeliverypointgroupProduct collection well. Setting this property to true while ProductCollectionViaDeliverypointgroupProduct hasn't been fetched disables lazy loading for ProductCollectionViaDeliverypointgroupProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaDeliverypointgroupProduct
		{
			get { return _alreadyFetchedProductCollectionViaDeliverypointgroupProduct;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaDeliverypointgroupProduct && !value && (_productCollectionViaDeliverypointgroupProduct != null))
				{
					_productCollectionViaDeliverypointgroupProduct.Clear();
				}
				_alreadyFetchedProductCollectionViaDeliverypointgroupProduct = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal
		{
			get { return GetMultiProductCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal. When set to true, ProductCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal; }
			set	{ _alwaysFetchProductCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal already has been fetched. Setting this property to false when ProductCollectionViaTerminal has been fetched
		/// will clear the ProductCollectionViaTerminal collection well. Setting this property to true while ProductCollectionViaTerminal hasn't been fetched disables lazy loading for ProductCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal
		{
			get { return _alreadyFetchedProductCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal && !value && (_productCollectionViaTerminal != null))
				{
					_productCollectionViaTerminal.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal_
		{
			get { return GetMultiProductCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal_. When set to true, ProductCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal_
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal_; }
			set	{ _alwaysFetchProductCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal_ already has been fetched. Setting this property to false when ProductCollectionViaTerminal_ has been fetched
		/// will clear the ProductCollectionViaTerminal_ collection well. Setting this property to true while ProductCollectionViaTerminal_ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal_
		{
			get { return _alreadyFetchedProductCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal_ && !value && (_productCollectionViaTerminal_ != null))
				{
					_productCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal__
		{
			get { return GetMultiProductCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal__. When set to true, ProductCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal__
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal__; }
			set	{ _alwaysFetchProductCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal__ already has been fetched. Setting this property to false when ProductCollectionViaTerminal__ has been fetched
		/// will clear the ProductCollectionViaTerminal__ collection well. Setting this property to true while ProductCollectionViaTerminal__ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal__
		{
			get { return _alreadyFetchedProductCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal__ && !value && (_productCollectionViaTerminal__ != null))
				{
					_productCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal___
		{
			get { return GetMultiProductCollectionViaTerminal___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal___. When set to true, ProductCollectionViaTerminal___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal___ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal___
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal___; }
			set	{ _alwaysFetchProductCollectionViaTerminal___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal___ already has been fetched. Setting this property to false when ProductCollectionViaTerminal___ has been fetched
		/// will clear the ProductCollectionViaTerminal___ collection well. Setting this property to true while ProductCollectionViaTerminal___ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal___
		{
			get { return _alreadyFetchedProductCollectionViaTerminal___;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal___ && !value && (_productCollectionViaTerminal___ != null))
				{
					_productCollectionViaTerminal___.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaAdvertisement
		{
			get { return GetMultiProductCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaAdvertisement. When set to true, ProductCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchProductCollectionViaAdvertisement; }
			set	{ _alwaysFetchProductCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaAdvertisement already has been fetched. Setting this property to false when ProductCollectionViaAdvertisement has been fetched
		/// will clear the ProductCollectionViaAdvertisement collection well. Setting this property to true while ProductCollectionViaAdvertisement hasn't been fetched disables lazy loading for ProductCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaAdvertisement
		{
			get { return _alreadyFetchedProductCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaAdvertisement && !value && (_productCollectionViaAdvertisement != null))
				{
					_productCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedProductCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupplierEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupplierCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupplierCollection SupplierCollectionViaAdvertisement
		{
			get { return GetMultiSupplierCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupplierCollectionViaAdvertisement. When set to true, SupplierCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupplierCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiSupplierCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupplierCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchSupplierCollectionViaAdvertisement; }
			set	{ _alwaysFetchSupplierCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupplierCollectionViaAdvertisement already has been fetched. Setting this property to false when SupplierCollectionViaAdvertisement has been fetched
		/// will clear the SupplierCollectionViaAdvertisement collection well. Setting this property to true while SupplierCollectionViaAdvertisement hasn't been fetched disables lazy loading for SupplierCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupplierCollectionViaAdvertisement
		{
			get { return _alreadyFetchedSupplierCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedSupplierCollectionViaAdvertisement && !value && (_supplierCollectionViaAdvertisement != null))
				{
					_supplierCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedSupplierCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage
		{
			get { return GetMultiTerminalCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage. When set to true, TerminalCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage has been fetched
		/// will clear the TerminalCollectionViaNetmessage collection well. Setting this property to true while TerminalCollectionViaNetmessage hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage && !value && (_terminalCollectionViaNetmessage != null))
				{
					_terminalCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage_
		{
			get { return GetMultiTerminalCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage_. When set to true, TerminalCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage_; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage_ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage_ has been fetched
		/// will clear the TerminalCollectionViaNetmessage_ collection well. Setting this property to true while TerminalCollectionViaNetmessage_ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage_
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage_ && !value && (_terminalCollectionViaNetmessage_ != null))
				{
					_terminalCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaTerminal
		{
			get { return GetMultiTerminalCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaTerminal. When set to true, TerminalCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaTerminal
		{
			get	{ return _alwaysFetchTerminalCollectionViaTerminal; }
			set	{ _alwaysFetchTerminalCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaTerminal already has been fetched. Setting this property to false when TerminalCollectionViaTerminal has been fetched
		/// will clear the TerminalCollectionViaTerminal collection well. Setting this property to true while TerminalCollectionViaTerminal hasn't been fetched disables lazy loading for TerminalCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaTerminal
		{
			get { return _alreadyFetchedTerminalCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaTerminal && !value && (_terminalCollectionViaTerminal != null))
				{
					_terminalCollectionViaTerminal.Clear();
				}
				_alreadyFetchedTerminalCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaTerminal
		{
			get { return GetMultiUIModeCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaTerminal. When set to true, UIModeCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaTerminal
		{
			get	{ return _alwaysFetchUIModeCollectionViaTerminal; }
			set	{ _alwaysFetchUIModeCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaTerminal already has been fetched. Setting this property to false when UIModeCollectionViaTerminal has been fetched
		/// will clear the UIModeCollectionViaTerminal collection well. Setting this property to true while UIModeCollectionViaTerminal hasn't been fetched disables lazy loading for UIModeCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaTerminal
		{
			get { return _alreadyFetchedUIModeCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaTerminal && !value && (_uIModeCollectionViaTerminal != null))
				{
					_uIModeCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUIModeCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection UserCollectionViaTerminal
		{
			get { return GetMultiUserCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserCollectionViaTerminal. When set to true, UserCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUserCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserCollectionViaTerminal
		{
			get	{ return _alwaysFetchUserCollectionViaTerminal; }
			set	{ _alwaysFetchUserCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserCollectionViaTerminal already has been fetched. Setting this property to false when UserCollectionViaTerminal has been fetched
		/// will clear the UserCollectionViaTerminal collection well. Setting this property to true while UserCollectionViaTerminal hasn't been fetched disables lazy loading for UserCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserCollectionViaTerminal
		{
			get { return _alreadyFetchedUserCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUserCollectionViaTerminal && !value && (_userCollectionViaTerminal != null))
				{
					_userCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUserCollectionViaTerminal = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAddressEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AddressEntity AddressEntity
		{
			get	{ return GetSingleAddressEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAddressEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "AddressEntity", _addressEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AddressEntity. When set to true, AddressEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AddressEntity is accessed. You can always execute a forced fetch by calling GetSingleAddressEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddressEntity
		{
			get	{ return _alwaysFetchAddressEntity; }
			set	{ _alwaysFetchAddressEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AddressEntity already has been fetched. Setting this property to false when AddressEntity has been fetched
		/// will set AddressEntity to null as well. Setting this property to true while AddressEntity hasn't been fetched disables lazy loading for AddressEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddressEntity
		{
			get { return _alreadyFetchedAddressEntity;}
			set 
			{
				if(_alreadyFetchedAddressEntity && !value)
				{
					this.AddressEntity = null;
				}
				_alreadyFetchedAddressEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AddressEntity is not found
		/// in the database. When set to true, AddressEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AddressEntityReturnsNewIfNotFound
		{
			get	{ return _addressEntityReturnsNewIfNotFound; }
			set { _addressEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AffiliateCampaignEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAffiliateCampaignEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AffiliateCampaignEntity AffiliateCampaignEntity
		{
			get	{ return GetSingleAffiliateCampaignEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAffiliateCampaignEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "AffiliateCampaignEntity", _affiliateCampaignEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AffiliateCampaignEntity. When set to true, AffiliateCampaignEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AffiliateCampaignEntity is accessed. You can always execute a forced fetch by calling GetSingleAffiliateCampaignEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAffiliateCampaignEntity
		{
			get	{ return _alwaysFetchAffiliateCampaignEntity; }
			set	{ _alwaysFetchAffiliateCampaignEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AffiliateCampaignEntity already has been fetched. Setting this property to false when AffiliateCampaignEntity has been fetched
		/// will set AffiliateCampaignEntity to null as well. Setting this property to true while AffiliateCampaignEntity hasn't been fetched disables lazy loading for AffiliateCampaignEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAffiliateCampaignEntity
		{
			get { return _alreadyFetchedAffiliateCampaignEntity;}
			set 
			{
				if(_alreadyFetchedAffiliateCampaignEntity && !value)
				{
					this.AffiliateCampaignEntity = null;
				}
				_alreadyFetchedAffiliateCampaignEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AffiliateCampaignEntity is not found
		/// in the database. When set to true, AffiliateCampaignEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AffiliateCampaignEntityReturnsNewIfNotFound
		{
			get	{ return _affiliateCampaignEntityReturnsNewIfNotFound; }
			set { _affiliateCampaignEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AnnouncementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAnnouncementEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AnnouncementEntity AnnouncementEntity
		{
			get	{ return GetSingleAnnouncementEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAnnouncementEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "AnnouncementEntity", _announcementEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementEntity. When set to true, AnnouncementEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementEntity is accessed. You can always execute a forced fetch by calling GetSingleAnnouncementEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementEntity
		{
			get	{ return _alwaysFetchAnnouncementEntity; }
			set	{ _alwaysFetchAnnouncementEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementEntity already has been fetched. Setting this property to false when AnnouncementEntity has been fetched
		/// will set AnnouncementEntity to null as well. Setting this property to true while AnnouncementEntity hasn't been fetched disables lazy loading for AnnouncementEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementEntity
		{
			get { return _alreadyFetchedAnnouncementEntity;}
			set 
			{
				if(_alreadyFetchedAnnouncementEntity && !value)
				{
					this.AnnouncementEntity = null;
				}
				_alreadyFetchedAnnouncementEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AnnouncementEntity is not found
		/// in the database. When set to true, AnnouncementEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AnnouncementEntityReturnsNewIfNotFound
		{
			get	{ return _announcementEntityReturnsNewIfNotFound; }
			set { _announcementEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientConfigurationEntity ClientConfigurationEntity
		{
			get	{ return GetSingleClientConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "ClientConfigurationEntity", _clientConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationEntity. When set to true, ClientConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleClientConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationEntity
		{
			get	{ return _alwaysFetchClientConfigurationEntity; }
			set	{ _alwaysFetchClientConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationEntity already has been fetched. Setting this property to false when ClientConfigurationEntity has been fetched
		/// will set ClientConfigurationEntity to null as well. Setting this property to true while ClientConfigurationEntity hasn't been fetched disables lazy loading for ClientConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationEntity
		{
			get { return _alreadyFetchedClientConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedClientConfigurationEntity && !value)
				{
					this.ClientConfigurationEntity = null;
				}
				_alreadyFetchedClientConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientConfigurationEntity is not found
		/// in the database. When set to true, ClientConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _clientConfigurationEntityReturnsNewIfNotFound; }
			set { _clientConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MenuEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMenuEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MenuEntity MenuEntity
		{
			get	{ return GetSingleMenuEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMenuEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "MenuEntity", _menuEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MenuEntity. When set to true, MenuEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuEntity is accessed. You can always execute a forced fetch by calling GetSingleMenuEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuEntity
		{
			get	{ return _alwaysFetchMenuEntity; }
			set	{ _alwaysFetchMenuEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuEntity already has been fetched. Setting this property to false when MenuEntity has been fetched
		/// will set MenuEntity to null as well. Setting this property to true while MenuEntity hasn't been fetched disables lazy loading for MenuEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuEntity
		{
			get { return _alreadyFetchedMenuEntity;}
			set 
			{
				if(_alreadyFetchedMenuEntity && !value)
				{
					this.MenuEntity = null;
				}
				_alreadyFetchedMenuEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MenuEntity is not found
		/// in the database. When set to true, MenuEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MenuEntityReturnsNewIfNotFound
		{
			get	{ return _menuEntityReturnsNewIfNotFound; }
			set { _menuEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PosdeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosdeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PosdeliverypointgroupEntity PosdeliverypointgroupEntity
		{
			get	{ return GetSinglePosdeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPosdeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "PosdeliverypointgroupEntity", _posdeliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupEntity. When set to true, PosdeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSinglePosdeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupEntity
		{
			get	{ return _alwaysFetchPosdeliverypointgroupEntity; }
			set	{ _alwaysFetchPosdeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupEntity already has been fetched. Setting this property to false when PosdeliverypointgroupEntity has been fetched
		/// will set PosdeliverypointgroupEntity to null as well. Setting this property to true while PosdeliverypointgroupEntity hasn't been fetched disables lazy loading for PosdeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupEntity
		{
			get { return _alreadyFetchedPosdeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupEntity && !value)
				{
					this.PosdeliverypointgroupEntity = null;
				}
				_alreadyFetchedPosdeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosdeliverypointgroupEntity is not found
		/// in the database. When set to true, PosdeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosdeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _posdeliverypointgroupEntityReturnsNewIfNotFound; }
			set { _posdeliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PriceScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceScheduleEntity PriceScheduleEntity
		{
			get	{ return GetSinglePriceScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "PriceScheduleEntity", _priceScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceScheduleEntity. When set to true, PriceScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceScheduleEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceScheduleEntity
		{
			get	{ return _alwaysFetchPriceScheduleEntity; }
			set	{ _alwaysFetchPriceScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceScheduleEntity already has been fetched. Setting this property to false when PriceScheduleEntity has been fetched
		/// will set PriceScheduleEntity to null as well. Setting this property to true while PriceScheduleEntity hasn't been fetched disables lazy loading for PriceScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceScheduleEntity
		{
			get { return _alreadyFetchedPriceScheduleEntity;}
			set 
			{
				if(_alreadyFetchedPriceScheduleEntity && !value)
				{
					this.PriceScheduleEntity = null;
				}
				_alreadyFetchedPriceScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceScheduleEntity is not found
		/// in the database. When set to true, PriceScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _priceScheduleEntityReturnsNewIfNotFound; }
			set { _priceScheduleEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleHotSOSBatteryLowProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity HotSOSBatteryLowProductEntity
		{
			get	{ return GetSingleHotSOSBatteryLowProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncHotSOSBatteryLowProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "HotSOSBatteryLowProductEntity", _hotSOSBatteryLowProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for HotSOSBatteryLowProductEntity. When set to true, HotSOSBatteryLowProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time HotSOSBatteryLowProductEntity is accessed. You can always execute a forced fetch by calling GetSingleHotSOSBatteryLowProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchHotSOSBatteryLowProductEntity
		{
			get	{ return _alwaysFetchHotSOSBatteryLowProductEntity; }
			set	{ _alwaysFetchHotSOSBatteryLowProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property HotSOSBatteryLowProductEntity already has been fetched. Setting this property to false when HotSOSBatteryLowProductEntity has been fetched
		/// will set HotSOSBatteryLowProductEntity to null as well. Setting this property to true while HotSOSBatteryLowProductEntity hasn't been fetched disables lazy loading for HotSOSBatteryLowProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedHotSOSBatteryLowProductEntity
		{
			get { return _alreadyFetchedHotSOSBatteryLowProductEntity;}
			set 
			{
				if(_alreadyFetchedHotSOSBatteryLowProductEntity && !value)
				{
					this.HotSOSBatteryLowProductEntity = null;
				}
				_alreadyFetchedHotSOSBatteryLowProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property HotSOSBatteryLowProductEntity is not found
		/// in the database. When set to true, HotSOSBatteryLowProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool HotSOSBatteryLowProductEntityReturnsNewIfNotFound
		{
			get	{ return _hotSOSBatteryLowProductEntityReturnsNewIfNotFound; }
			set { _hotSOSBatteryLowProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEmailDocumentRouteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity EmailDocumentRouteEntity
		{
			get	{ return GetSingleEmailDocumentRouteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEmailDocumentRouteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection__", "EmailDocumentRouteEntity", _emailDocumentRouteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EmailDocumentRouteEntity. When set to true, EmailDocumentRouteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailDocumentRouteEntity is accessed. You can always execute a forced fetch by calling GetSingleEmailDocumentRouteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailDocumentRouteEntity
		{
			get	{ return _alwaysFetchEmailDocumentRouteEntity; }
			set	{ _alwaysFetchEmailDocumentRouteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailDocumentRouteEntity already has been fetched. Setting this property to false when EmailDocumentRouteEntity has been fetched
		/// will set EmailDocumentRouteEntity to null as well. Setting this property to true while EmailDocumentRouteEntity hasn't been fetched disables lazy loading for EmailDocumentRouteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailDocumentRouteEntity
		{
			get { return _alreadyFetchedEmailDocumentRouteEntity;}
			set 
			{
				if(_alreadyFetchedEmailDocumentRouteEntity && !value)
				{
					this.EmailDocumentRouteEntity = null;
				}
				_alreadyFetchedEmailDocumentRouteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EmailDocumentRouteEntity is not found
		/// in the database. When set to true, EmailDocumentRouteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EmailDocumentRouteEntityReturnsNewIfNotFound
		{
			get	{ return _emailDocumentRouteEntityReturnsNewIfNotFound; }
			set { _emailDocumentRouteEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRouteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity RouteEntity
		{
			get	{ return GetSingleRouteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRouteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "RouteEntity", _routeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RouteEntity. When set to true, RouteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteEntity is accessed. You can always execute a forced fetch by calling GetSingleRouteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteEntity
		{
			get	{ return _alwaysFetchRouteEntity; }
			set	{ _alwaysFetchRouteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteEntity already has been fetched. Setting this property to false when RouteEntity has been fetched
		/// will set RouteEntity to null as well. Setting this property to true while RouteEntity hasn't been fetched disables lazy loading for RouteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteEntity
		{
			get { return _alreadyFetchedRouteEntity;}
			set 
			{
				if(_alreadyFetchedRouteEntity && !value)
				{
					this.RouteEntity = null;
				}
				_alreadyFetchedRouteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RouteEntity is not found
		/// in the database. When set to true, RouteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RouteEntityReturnsNewIfNotFound
		{
			get	{ return _routeEntityReturnsNewIfNotFound; }
			set { _routeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRouteEntity_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity RouteEntity_
		{
			get	{ return GetSingleRouteEntity_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRouteEntity_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection___", "RouteEntity_", _routeEntity_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RouteEntity_. When set to true, RouteEntity_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteEntity_ is accessed. You can always execute a forced fetch by calling GetSingleRouteEntity_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteEntity_
		{
			get	{ return _alwaysFetchRouteEntity_; }
			set	{ _alwaysFetchRouteEntity_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteEntity_ already has been fetched. Setting this property to false when RouteEntity_ has been fetched
		/// will set RouteEntity_ to null as well. Setting this property to true while RouteEntity_ hasn't been fetched disables lazy loading for RouteEntity_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteEntity_
		{
			get { return _alreadyFetchedRouteEntity_;}
			set 
			{
				if(_alreadyFetchedRouteEntity_ && !value)
				{
					this.RouteEntity_ = null;
				}
				_alreadyFetchedRouteEntity_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RouteEntity_ is not found
		/// in the database. When set to true, RouteEntity_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RouteEntity_ReturnsNewIfNotFound
		{
			get	{ return _routeEntity_ReturnsNewIfNotFound; }
			set { _routeEntity_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSystemMessageRouteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity SystemMessageRouteEntity
		{
			get	{ return GetSingleSystemMessageRouteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSystemMessageRouteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection_", "SystemMessageRouteEntity", _systemMessageRouteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SystemMessageRouteEntity. When set to true, SystemMessageRouteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SystemMessageRouteEntity is accessed. You can always execute a forced fetch by calling GetSingleSystemMessageRouteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSystemMessageRouteEntity
		{
			get	{ return _alwaysFetchSystemMessageRouteEntity; }
			set	{ _alwaysFetchSystemMessageRouteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SystemMessageRouteEntity already has been fetched. Setting this property to false when SystemMessageRouteEntity has been fetched
		/// will set SystemMessageRouteEntity to null as well. Setting this property to true while SystemMessageRouteEntity hasn't been fetched disables lazy loading for SystemMessageRouteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSystemMessageRouteEntity
		{
			get { return _alreadyFetchedSystemMessageRouteEntity;}
			set 
			{
				if(_alreadyFetchedSystemMessageRouteEntity && !value)
				{
					this.SystemMessageRouteEntity = null;
				}
				_alreadyFetchedSystemMessageRouteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SystemMessageRouteEntity is not found
		/// in the database. When set to true, SystemMessageRouteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SystemMessageRouteEntityReturnsNewIfNotFound
		{
			get	{ return _systemMessageRouteEntityReturnsNewIfNotFound; }
			set { _systemMessageRouteEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity TerminalEntity
		{
			get	{ return GetSingleTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "TerminalEntity", _terminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalEntity. When set to true, TerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalEntity
		{
			get	{ return _alwaysFetchTerminalEntity; }
			set	{ _alwaysFetchTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalEntity already has been fetched. Setting this property to false when TerminalEntity has been fetched
		/// will set TerminalEntity to null as well. Setting this property to true while TerminalEntity hasn't been fetched disables lazy loading for TerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalEntity
		{
			get { return _alreadyFetchedTerminalEntity;}
			set 
			{
				if(_alreadyFetchedTerminalEntity && !value)
				{
					this.TerminalEntity = null;
				}
				_alreadyFetchedTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalEntity is not found
		/// in the database. When set to true, TerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalEntityReturnsNewIfNotFound
		{
			get	{ return _terminalEntityReturnsNewIfNotFound; }
			set { _terminalEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMobileUIModeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIModeEntity MobileUIModeEntity
		{
			get	{ return GetSingleMobileUIModeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMobileUIModeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection_", "MobileUIModeEntity", _mobileUIModeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MobileUIModeEntity. When set to true, MobileUIModeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MobileUIModeEntity is accessed. You can always execute a forced fetch by calling GetSingleMobileUIModeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMobileUIModeEntity
		{
			get	{ return _alwaysFetchMobileUIModeEntity; }
			set	{ _alwaysFetchMobileUIModeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MobileUIModeEntity already has been fetched. Setting this property to false when MobileUIModeEntity has been fetched
		/// will set MobileUIModeEntity to null as well. Setting this property to true while MobileUIModeEntity hasn't been fetched disables lazy loading for MobileUIModeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMobileUIModeEntity
		{
			get { return _alreadyFetchedMobileUIModeEntity;}
			set 
			{
				if(_alreadyFetchedMobileUIModeEntity && !value)
				{
					this.MobileUIModeEntity = null;
				}
				_alreadyFetchedMobileUIModeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MobileUIModeEntity is not found
		/// in the database. When set to true, MobileUIModeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MobileUIModeEntityReturnsNewIfNotFound
		{
			get	{ return _mobileUIModeEntityReturnsNewIfNotFound; }
			set { _mobileUIModeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTabletUIModeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIModeEntity TabletUIModeEntity
		{
			get	{ return GetSingleTabletUIModeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTabletUIModeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection__", "TabletUIModeEntity", _tabletUIModeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TabletUIModeEntity. When set to true, TabletUIModeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TabletUIModeEntity is accessed. You can always execute a forced fetch by calling GetSingleTabletUIModeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTabletUIModeEntity
		{
			get	{ return _alwaysFetchTabletUIModeEntity; }
			set	{ _alwaysFetchTabletUIModeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TabletUIModeEntity already has been fetched. Setting this property to false when TabletUIModeEntity has been fetched
		/// will set TabletUIModeEntity to null as well. Setting this property to true while TabletUIModeEntity hasn't been fetched disables lazy loading for TabletUIModeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTabletUIModeEntity
		{
			get { return _alreadyFetchedTabletUIModeEntity;}
			set 
			{
				if(_alreadyFetchedTabletUIModeEntity && !value)
				{
					this.TabletUIModeEntity = null;
				}
				_alreadyFetchedTabletUIModeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TabletUIModeEntity is not found
		/// in the database. When set to true, TabletUIModeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TabletUIModeEntityReturnsNewIfNotFound
		{
			get	{ return _tabletUIModeEntityReturnsNewIfNotFound; }
			set { _tabletUIModeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIModeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIModeEntity UIModeEntity
		{
			get	{ return GetSingleUIModeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIModeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "UIModeEntity", _uIModeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeEntity. When set to true, UIModeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIModeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeEntity
		{
			get	{ return _alwaysFetchUIModeEntity; }
			set	{ _alwaysFetchUIModeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeEntity already has been fetched. Setting this property to false when UIModeEntity has been fetched
		/// will set UIModeEntity to null as well. Setting this property to true while UIModeEntity hasn't been fetched disables lazy loading for UIModeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeEntity
		{
			get { return _alreadyFetchedUIModeEntity;}
			set 
			{
				if(_alreadyFetchedUIModeEntity && !value)
				{
					this.UIModeEntity = null;
				}
				_alreadyFetchedUIModeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIModeEntity is not found
		/// in the database. When set to true, UIModeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIModeEntityReturnsNewIfNotFound
		{
			get	{ return _uIModeEntityReturnsNewIfNotFound; }
			set { _uIModeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIScheduleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIScheduleEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIScheduleEntity UIScheduleEntity
		{
			get	{ return GetSingleUIScheduleEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIScheduleEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "UIScheduleEntity", _uIScheduleEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleEntity. When set to true, UIScheduleEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleEntity is accessed. You can always execute a forced fetch by calling GetSingleUIScheduleEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleEntity
		{
			get	{ return _alwaysFetchUIScheduleEntity; }
			set	{ _alwaysFetchUIScheduleEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleEntity already has been fetched. Setting this property to false when UIScheduleEntity has been fetched
		/// will set UIScheduleEntity to null as well. Setting this property to true while UIScheduleEntity hasn't been fetched disables lazy loading for UIScheduleEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleEntity
		{
			get { return _alreadyFetchedUIScheduleEntity;}
			set 
			{
				if(_alreadyFetchedUIScheduleEntity && !value)
				{
					this.UIScheduleEntity = null;
				}
				_alreadyFetchedUIScheduleEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIScheduleEntity is not found
		/// in the database. When set to true, UIScheduleEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIScheduleEntityReturnsNewIfNotFound
		{
			get	{ return _uIScheduleEntityReturnsNewIfNotFound; }
			set { _uIScheduleEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIThemeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIThemeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIThemeEntity UIThemeEntity
		{
			get	{ return GetSingleUIThemeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIThemeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeliverypointgroupCollection", "UIThemeEntity", _uIThemeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIThemeEntity. When set to true, UIThemeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIThemeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIThemeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIThemeEntity
		{
			get	{ return _alwaysFetchUIThemeEntity; }
			set	{ _alwaysFetchUIThemeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIThemeEntity already has been fetched. Setting this property to false when UIThemeEntity has been fetched
		/// will set UIThemeEntity to null as well. Setting this property to true while UIThemeEntity hasn't been fetched disables lazy loading for UIThemeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIThemeEntity
		{
			get { return _alreadyFetchedUIThemeEntity;}
			set 
			{
				if(_alreadyFetchedUIThemeEntity && !value)
				{
					this.UIThemeEntity = null;
				}
				_alreadyFetchedUIThemeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIThemeEntity is not found
		/// in the database. When set to true, UIThemeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIThemeEntityReturnsNewIfNotFound
		{
			get	{ return _uIThemeEntityReturnsNewIfNotFound; }
			set { _uIThemeEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimestampEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimestampEntity TimestampCollection
		{
			get	{ return GetSingleTimestampCollection(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTimestampCollection(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_timestampCollection !=null);
						DesetupSyncTimestampCollection(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TimestampCollection");
						}
					}
					else
					{
						if(_timestampCollection!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "DeliverypointgroupEntity");
							SetupSyncTimestampCollection(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute a forced fetch by calling GetSingleTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will set TimestampCollection to null as well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value)
				{
					this.TimestampCollection = null;
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimestampCollection is not found
		/// in the database. When set to true, TimestampCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimestampCollectionReturnsNewIfNotFound
		{
			get	{ return _timestampCollectionReturnsNewIfNotFound; }
			set	{ _timestampCollectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeliverypointgroupEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
