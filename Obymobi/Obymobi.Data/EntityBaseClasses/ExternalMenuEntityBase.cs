﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ExternalMenu'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ExternalMenuEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ExternalMenuEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ExternalProductCollection	_externalProductCollection;
		private bool	_alwaysFetchExternalProductCollection, _alreadyFetchedExternalProductCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private ExternalSystemEntity _externalSystemEntity;
		private bool	_alwaysFetchExternalSystemEntity, _alreadyFetchedExternalSystemEntity, _externalSystemEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name ExternalSystemEntity</summary>
			public static readonly string ExternalSystemEntity = "ExternalSystemEntity";
			/// <summary>Member name ExternalProductCollection</summary>
			public static readonly string ExternalProductCollection = "ExternalProductCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ExternalMenuEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ExternalMenuEntityBase() :base("ExternalMenuEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		protected ExternalMenuEntityBase(System.Int32 externalMenuId):base("ExternalMenuEntity")
		{
			InitClassFetch(externalMenuId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ExternalMenuEntityBase(System.Int32 externalMenuId, IPrefetchPath prefetchPathToUse): base("ExternalMenuEntity")
		{
			InitClassFetch(externalMenuId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="validator">The custom validator object for this ExternalMenuEntity</param>
		protected ExternalMenuEntityBase(System.Int32 externalMenuId, IValidator validator):base("ExternalMenuEntity")
		{
			InitClassFetch(externalMenuId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalMenuEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_externalProductCollection = (Obymobi.Data.CollectionClasses.ExternalProductCollection)info.GetValue("_externalProductCollection", typeof(Obymobi.Data.CollectionClasses.ExternalProductCollection));
			_alwaysFetchExternalProductCollection = info.GetBoolean("_alwaysFetchExternalProductCollection");
			_alreadyFetchedExternalProductCollection = info.GetBoolean("_alreadyFetchedExternalProductCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_externalSystemEntity = (ExternalSystemEntity)info.GetValue("_externalSystemEntity", typeof(ExternalSystemEntity));
			if(_externalSystemEntity!=null)
			{
				_externalSystemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalSystemEntityReturnsNewIfNotFound = info.GetBoolean("_externalSystemEntityReturnsNewIfNotFound");
			_alwaysFetchExternalSystemEntity = info.GetBoolean("_alwaysFetchExternalSystemEntity");
			_alreadyFetchedExternalSystemEntity = info.GetBoolean("_alreadyFetchedExternalSystemEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ExternalMenuFieldIndex)fieldIndex)
			{
				case ExternalMenuFieldIndex.ExternalSystemId:
					DesetupSyncExternalSystemEntity(true, false);
					_alreadyFetchedExternalSystemEntity = false;
					break;
				case ExternalMenuFieldIndex.ParentCompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedExternalProductCollection = (_externalProductCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedExternalSystemEntity = (_externalSystemEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingParentCompanyId);
					break;
				case "ExternalSystemEntity":
					toReturn.Add(Relations.ExternalSystemEntityUsingExternalSystemId);
					break;
				case "ExternalProductCollection":
					toReturn.Add(Relations.ExternalProductEntityUsingExternalMenuId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_externalProductCollection", (!this.MarkedForDeletion?_externalProductCollection:null));
			info.AddValue("_alwaysFetchExternalProductCollection", _alwaysFetchExternalProductCollection);
			info.AddValue("_alreadyFetchedExternalProductCollection", _alreadyFetchedExternalProductCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_externalSystemEntity", (!this.MarkedForDeletion?_externalSystemEntity:null));
			info.AddValue("_externalSystemEntityReturnsNewIfNotFound", _externalSystemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalSystemEntity", _alwaysFetchExternalSystemEntity);
			info.AddValue("_alreadyFetchedExternalSystemEntity", _alreadyFetchedExternalSystemEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "ExternalSystemEntity":
					_alreadyFetchedExternalSystemEntity = true;
					this.ExternalSystemEntity = (ExternalSystemEntity)entity;
					break;
				case "ExternalProductCollection":
					_alreadyFetchedExternalProductCollection = true;
					if(entity!=null)
					{
						this.ExternalProductCollection.Add((ExternalProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "ExternalSystemEntity":
					SetupSyncExternalSystemEntity(relatedEntity);
					break;
				case "ExternalProductCollection":
					_externalProductCollection.Add((ExternalProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "ExternalSystemEntity":
					DesetupSyncExternalSystemEntity(false, true);
					break;
				case "ExternalProductCollection":
					this.PerformRelatedEntityRemoval(_externalProductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_externalSystemEntity!=null)
			{
				toReturn.Add(_externalSystemEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_externalProductCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalMenuId)
		{
			return FetchUsingPK(externalMenuId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalMenuId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(externalMenuId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalMenuId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(externalMenuId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 externalMenuId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(externalMenuId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ExternalMenuId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ExternalMenuRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch)
		{
			return GetMultiExternalProductCollection(forceFetch, _externalProductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalProductCollection(forceFetch, _externalProductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ExternalProductCollection GetMultiExternalProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalProductCollection || forceFetch || _alwaysFetchExternalProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalProductCollection);
				_externalProductCollection.SuppressClearInGetMulti=!forceFetch;
				_externalProductCollection.EntityFactoryToUse = entityFactoryToUse;
				_externalProductCollection.GetMultiManyToOne(this, null, filter);
				_externalProductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalProductCollection = true;
			}
			return _externalProductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalProductCollection'. These settings will be taken into account
		/// when the property ExternalProductCollection is requested or GetMultiExternalProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalProductCollection.SortClauses=sortClauses;
			_externalProductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingParentCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentCompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public ExternalSystemEntity GetSingleExternalSystemEntity()
		{
			return GetSingleExternalSystemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public virtual ExternalSystemEntity GetSingleExternalSystemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalSystemEntity || forceFetch || _alwaysFetchExternalSystemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalSystemEntityUsingExternalSystemId);
				ExternalSystemEntity newEntity = new ExternalSystemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalSystemId);
				}
				if(fetchResult)
				{
					newEntity = (ExternalSystemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalSystemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalSystemEntity = newEntity;
				_alreadyFetchedExternalSystemEntity = fetchResult;
			}
			return _externalSystemEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("ExternalSystemEntity", _externalSystemEntity);
			toReturn.Add("ExternalProductCollection", _externalProductCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="validator">The validator object for this ExternalMenuEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 externalMenuId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(externalMenuId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_externalProductCollection = new Obymobi.Data.CollectionClasses.ExternalProductCollection();
			_externalProductCollection.SetContainingEntityInfo(this, "ExternalMenuEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_externalSystemEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalMenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalSystemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticExternalMenuRelations.CompanyEntityUsingParentCompanyIdStatic, true, signalRelatedEntity, "ExternalMenuCollection", resetFKFields, new int[] { (int)ExternalMenuFieldIndex.ParentCompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticExternalMenuRelations.CompanyEntityUsingParentCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalSystemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalSystemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticExternalMenuRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, signalRelatedEntity, "ExternalMenuCollection", resetFKFields, new int[] { (int)ExternalMenuFieldIndex.ExternalSystemId } );		
			_externalSystemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalSystemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalSystemEntity(IEntityCore relatedEntity)
		{
			if(_externalSystemEntity!=relatedEntity)
			{		
				DesetupSyncExternalSystemEntity(true, true);
				_externalSystemEntity = (ExternalSystemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticExternalMenuRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, ref _alreadyFetchedExternalSystemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalSystemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="externalMenuId">PK value for ExternalMenu which data should be fetched into this ExternalMenu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 externalMenuId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ExternalMenuFieldIndex.ExternalMenuId].ForcedCurrentValueWrite(externalMenuId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateExternalMenuDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ExternalMenuEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ExternalMenuRelations Relations
		{
			get	{ return new ExternalMenuRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalProductCollection(), (IEntityRelation)GetRelationsForField("ExternalProductCollection")[0], (int)Obymobi.Data.EntityType.ExternalMenuEntity, (int)Obymobi.Data.EntityType.ExternalProductEntity, 0, null, null, null, "ExternalProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ExternalMenuEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSystem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSystemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSystemCollection(), (IEntityRelation)GetRelationsForField("ExternalSystemEntity")[0], (int)Obymobi.Data.EntityType.ExternalMenuEntity, (int)Obymobi.Data.EntityType.ExternalSystemEntity, 0, null, null, null, "ExternalSystemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ExternalMenuId property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."ExternalMenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ExternalMenuId
		{
			get { return (System.Int32)GetValue((int)ExternalMenuFieldIndex.ExternalMenuId, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.ExternalMenuId, value, true); }
		}

		/// <summary> The ExternalSystemId property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."ExternalSystemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ExternalSystemId
		{
			get { return (System.Int32)GetValue((int)ExternalMenuFieldIndex.ExternalSystemId, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.ExternalSystemId, value, true); }
		}

		/// <summary> The Id property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Id
		{
			get { return (System.String)GetValue((int)ExternalMenuFieldIndex.Id, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.Id, value, true); }
		}

		/// <summary> The Name property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ExternalMenuFieldIndex.Name, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ExternalMenuFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdateUTC property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."UpdateUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdateUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ExternalMenuFieldIndex.UpdateUTC, false); }
			set	{ SetValue((int)ExternalMenuFieldIndex.UpdateUTC, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ExternalMenu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ExternalMenu"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ExternalMenuFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ExternalMenuFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ExternalProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ExternalProductCollection ExternalProductCollection
		{
			get	{ return GetMultiExternalProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalProductCollection. When set to true, ExternalProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiExternalProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalProductCollection
		{
			get	{ return _alwaysFetchExternalProductCollection; }
			set	{ _alwaysFetchExternalProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalProductCollection already has been fetched. Setting this property to false when ExternalProductCollection has been fetched
		/// will clear the ExternalProductCollection collection well. Setting this property to true while ExternalProductCollection hasn't been fetched disables lazy loading for ExternalProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalProductCollection
		{
			get { return _alreadyFetchedExternalProductCollection;}
			set 
			{
				if(_alreadyFetchedExternalProductCollection && !value && (_externalProductCollection != null))
				{
					_externalProductCollection.Clear();
				}
				_alreadyFetchedExternalProductCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalMenuCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalSystemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalSystemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalSystemEntity ExternalSystemEntity
		{
			get	{ return GetSingleExternalSystemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalSystemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalMenuCollection", "ExternalSystemEntity", _externalSystemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSystemEntity. When set to true, ExternalSystemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSystemEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalSystemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSystemEntity
		{
			get	{ return _alwaysFetchExternalSystemEntity; }
			set	{ _alwaysFetchExternalSystemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSystemEntity already has been fetched. Setting this property to false when ExternalSystemEntity has been fetched
		/// will set ExternalSystemEntity to null as well. Setting this property to true while ExternalSystemEntity hasn't been fetched disables lazy loading for ExternalSystemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSystemEntity
		{
			get { return _alreadyFetchedExternalSystemEntity;}
			set 
			{
				if(_alreadyFetchedExternalSystemEntity && !value)
				{
					this.ExternalSystemEntity = null;
				}
				_alreadyFetchedExternalSystemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalSystemEntity is not found
		/// in the database. When set to true, ExternalSystemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalSystemEntityReturnsNewIfNotFound
		{
			get	{ return _externalSystemEntityReturnsNewIfNotFound; }
			set { _externalSystemEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ExternalMenuEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
