﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Language'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class LanguageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "LanguageEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection	_actionButtonLanguageCollection;
		private bool	_alwaysFetchActionButtonLanguageCollection, _alreadyFetchedActionButtonLanguageCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection	_advertisementLanguageCollection;
		private bool	_alwaysFetchAdvertisementLanguageCollection, _alreadyFetchedAdvertisementLanguageCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection	_advertisementTagLanguageCollection;
		private bool	_alwaysFetchAdvertisementTagLanguageCollection, _alreadyFetchedAdvertisementTagLanguageCollection;
		private Obymobi.Data.CollectionClasses.AlterationLanguageCollection	_alterationLanguageCollection;
		private bool	_alwaysFetchAlterationLanguageCollection, _alreadyFetchedAlterationLanguageCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection	_alterationoptionLanguageCollection;
		private bool	_alwaysFetchAlterationoptionLanguageCollection, _alreadyFetchedAlterationoptionLanguageCollection;
		private Obymobi.Data.CollectionClasses.AmenityLanguageCollection	_amenityLanguageCollection;
		private bool	_alwaysFetchAmenityLanguageCollection, _alreadyFetchedAmenityLanguageCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection	_announcementLanguageCollection;
		private bool	_alwaysFetchAnnouncementLanguageCollection, _alreadyFetchedAnnouncementLanguageCollection;
		private Obymobi.Data.CollectionClasses.AttachmentLanguageCollection	_attachmentLanguageCollection;
		private bool	_alwaysFetchAttachmentLanguageCollection, _alreadyFetchedAttachmentLanguageCollection;
		private Obymobi.Data.CollectionClasses.CategoryLanguageCollection	_categoryLanguageCollection;
		private bool	_alwaysFetchCategoryLanguageCollection, _alreadyFetchedCategoryLanguageCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection	_companyCollection;
		private bool	_alwaysFetchCompanyCollection, _alreadyFetchedCompanyCollection;
		private Obymobi.Data.CollectionClasses.CompanyLanguageCollection	_companyLanguageCollection;
		private bool	_alwaysFetchCompanyLanguageCollection, _alreadyFetchedCompanyLanguageCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection	_deliverypointgroupLanguageCollection;
		private bool	_alwaysFetchDeliverypointgroupLanguageCollection, _alreadyFetchedDeliverypointgroupLanguageCollection;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection	_entertainmentcategoryLanguageCollection;
		private bool	_alwaysFetchEntertainmentcategoryLanguageCollection, _alreadyFetchedEntertainmentcategoryLanguageCollection;
		private Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection	_genericcategoryLanguageCollection;
		private bool	_alwaysFetchGenericcategoryLanguageCollection, _alreadyFetchedGenericcategoryLanguageCollection;
		private Obymobi.Data.CollectionClasses.GenericproductLanguageCollection	_genericproductLanguageCollection;
		private bool	_alwaysFetchGenericproductLanguageCollection, _alreadyFetchedGenericproductLanguageCollection;
		private Obymobi.Data.CollectionClasses.MediaLanguageCollection	_mediaLanguageCollection;
		private bool	_alwaysFetchMediaLanguageCollection, _alreadyFetchedMediaLanguageCollection;
		private Obymobi.Data.CollectionClasses.PageElementCollection	_pageElementCollection;
		private bool	_alwaysFetchPageElementCollection, _alreadyFetchedPageElementCollection;
		private Obymobi.Data.CollectionClasses.PageLanguageCollection	_pageLanguageCollection;
		private bool	_alwaysFetchPageLanguageCollection, _alreadyFetchedPageLanguageCollection;
		private Obymobi.Data.CollectionClasses.PageTemplateElementCollection	_pageTemplateElementCollection;
		private bool	_alwaysFetchPageTemplateElementCollection, _alreadyFetchedPageTemplateElementCollection;
		private Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection	_pageTemplateLanguageCollection;
		private bool	_alwaysFetchPageTemplateLanguageCollection, _alreadyFetchedPageTemplateLanguageCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection	_pointOfInterestLanguageCollection;
		private bool	_alwaysFetchPointOfInterestLanguageCollection, _alreadyFetchedPointOfInterestLanguageCollection;
		private Obymobi.Data.CollectionClasses.ProductLanguageCollection	_productLanguageCollection;
		private bool	_alwaysFetchProductLanguageCollection, _alreadyFetchedProductLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection	_roomControlAreaLanguageCollection;
		private bool	_alwaysFetchRoomControlAreaLanguageCollection, _alreadyFetchedRoomControlAreaLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection	_roomControlComponentLanguageCollection;
		private bool	_alwaysFetchRoomControlComponentLanguageCollection, _alreadyFetchedRoomControlComponentLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection	_roomControlSectionItemLanguageCollection;
		private bool	_alwaysFetchRoomControlSectionItemLanguageCollection, _alreadyFetchedRoomControlSectionItemLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection	_roomControlSectionLanguageCollection;
		private bool	_alwaysFetchRoomControlSectionLanguageCollection, _alreadyFetchedRoomControlSectionLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection	_roomControlWidgetLanguageCollection;
		private bool	_alwaysFetchRoomControlWidgetLanguageCollection, _alreadyFetchedRoomControlWidgetLanguageCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection	_scheduledMessageLanguageCollection;
		private bool	_alwaysFetchScheduledMessageLanguageCollection, _alreadyFetchedScheduledMessageLanguageCollection;
		private Obymobi.Data.CollectionClasses.SiteLanguageCollection	_siteLanguageCollection;
		private bool	_alwaysFetchSiteLanguageCollection, _alreadyFetchedSiteLanguageCollection;
		private Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection	_siteTemplateLanguageCollection;
		private bool	_alwaysFetchSiteTemplateLanguageCollection, _alreadyFetchedSiteTemplateLanguageCollection;
		private Obymobi.Data.CollectionClasses.StationLanguageCollection	_stationLanguageCollection;
		private bool	_alwaysFetchStationLanguageCollection, _alreadyFetchedStationLanguageCollection;
		private Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection	_surveyAnswerLanguageCollection;
		private bool	_alwaysFetchSurveyAnswerLanguageCollection, _alreadyFetchedSurveyAnswerLanguageCollection;
		private Obymobi.Data.CollectionClasses.SurveyLanguageCollection	_surveyLanguageCollection;
		private bool	_alwaysFetchSurveyLanguageCollection, _alreadyFetchedSurveyLanguageCollection;
		private Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection	_surveyQuestionLanguageCollection;
		private bool	_alwaysFetchSurveyQuestionLanguageCollection, _alreadyFetchedSurveyQuestionLanguageCollection;
		private Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection	_uIFooterItemLanguageCollection;
		private bool	_alwaysFetchUIFooterItemLanguageCollection, _alreadyFetchedUIFooterItemLanguageCollection;
		private Obymobi.Data.CollectionClasses.UITabLanguageCollection	_uITabLanguageCollection;
		private bool	_alwaysFetchUITabLanguageCollection, _alreadyFetchedUITabLanguageCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection	_uIWidgetLanguageCollection;
		private bool	_alwaysFetchUIWidgetLanguageCollection, _alreadyFetchedUIWidgetLanguageCollection;
		private Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection	_venueCategoryLanguageCollection;
		private bool	_alwaysFetchVenueCategoryLanguageCollection, _alreadyFetchedVenueCategoryLanguageCollection;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaEntertainmentcategoryLanguage;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage, _alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCompany;
		private bool	_alwaysFetchRouteCollectionViaCompany, _alreadyFetchedRouteCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaCompany;
		private bool	_alwaysFetchSupportpoolCollectionViaCompany, _alreadyFetchedSupportpoolCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.UITabCollection _uITabCollectionViaUITabLanguage;
		private bool	_alwaysFetchUITabCollectionViaUITabLanguage, _alreadyFetchedUITabCollectionViaUITabLanguage;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ActionButtonLanguageCollection</summary>
			public static readonly string ActionButtonLanguageCollection = "ActionButtonLanguageCollection";
			/// <summary>Member name AdvertisementLanguageCollection</summary>
			public static readonly string AdvertisementLanguageCollection = "AdvertisementLanguageCollection";
			/// <summary>Member name AdvertisementTagLanguageCollection</summary>
			public static readonly string AdvertisementTagLanguageCollection = "AdvertisementTagLanguageCollection";
			/// <summary>Member name AlterationLanguageCollection</summary>
			public static readonly string AlterationLanguageCollection = "AlterationLanguageCollection";
			/// <summary>Member name AlterationoptionLanguageCollection</summary>
			public static readonly string AlterationoptionLanguageCollection = "AlterationoptionLanguageCollection";
			/// <summary>Member name AmenityLanguageCollection</summary>
			public static readonly string AmenityLanguageCollection = "AmenityLanguageCollection";
			/// <summary>Member name AnnouncementLanguageCollection</summary>
			public static readonly string AnnouncementLanguageCollection = "AnnouncementLanguageCollection";
			/// <summary>Member name AttachmentLanguageCollection</summary>
			public static readonly string AttachmentLanguageCollection = "AttachmentLanguageCollection";
			/// <summary>Member name CategoryLanguageCollection</summary>
			public static readonly string CategoryLanguageCollection = "CategoryLanguageCollection";
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name CompanyLanguageCollection</summary>
			public static readonly string CompanyLanguageCollection = "CompanyLanguageCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name DeliverypointgroupLanguageCollection</summary>
			public static readonly string DeliverypointgroupLanguageCollection = "DeliverypointgroupLanguageCollection";
			/// <summary>Member name EntertainmentcategoryLanguageCollection</summary>
			public static readonly string EntertainmentcategoryLanguageCollection = "EntertainmentcategoryLanguageCollection";
			/// <summary>Member name GenericcategoryLanguageCollection</summary>
			public static readonly string GenericcategoryLanguageCollection = "GenericcategoryLanguageCollection";
			/// <summary>Member name GenericproductLanguageCollection</summary>
			public static readonly string GenericproductLanguageCollection = "GenericproductLanguageCollection";
			/// <summary>Member name MediaLanguageCollection</summary>
			public static readonly string MediaLanguageCollection = "MediaLanguageCollection";
			/// <summary>Member name PageElementCollection</summary>
			public static readonly string PageElementCollection = "PageElementCollection";
			/// <summary>Member name PageLanguageCollection</summary>
			public static readonly string PageLanguageCollection = "PageLanguageCollection";
			/// <summary>Member name PageTemplateElementCollection</summary>
			public static readonly string PageTemplateElementCollection = "PageTemplateElementCollection";
			/// <summary>Member name PageTemplateLanguageCollection</summary>
			public static readonly string PageTemplateLanguageCollection = "PageTemplateLanguageCollection";
			/// <summary>Member name PointOfInterestLanguageCollection</summary>
			public static readonly string PointOfInterestLanguageCollection = "PointOfInterestLanguageCollection";
			/// <summary>Member name ProductLanguageCollection</summary>
			public static readonly string ProductLanguageCollection = "ProductLanguageCollection";
			/// <summary>Member name RoomControlAreaLanguageCollection</summary>
			public static readonly string RoomControlAreaLanguageCollection = "RoomControlAreaLanguageCollection";
			/// <summary>Member name RoomControlComponentLanguageCollection</summary>
			public static readonly string RoomControlComponentLanguageCollection = "RoomControlComponentLanguageCollection";
			/// <summary>Member name RoomControlSectionItemLanguageCollection</summary>
			public static readonly string RoomControlSectionItemLanguageCollection = "RoomControlSectionItemLanguageCollection";
			/// <summary>Member name RoomControlSectionLanguageCollection</summary>
			public static readonly string RoomControlSectionLanguageCollection = "RoomControlSectionLanguageCollection";
			/// <summary>Member name RoomControlWidgetLanguageCollection</summary>
			public static readonly string RoomControlWidgetLanguageCollection = "RoomControlWidgetLanguageCollection";
			/// <summary>Member name ScheduledMessageLanguageCollection</summary>
			public static readonly string ScheduledMessageLanguageCollection = "ScheduledMessageLanguageCollection";
			/// <summary>Member name SiteLanguageCollection</summary>
			public static readonly string SiteLanguageCollection = "SiteLanguageCollection";
			/// <summary>Member name SiteTemplateLanguageCollection</summary>
			public static readonly string SiteTemplateLanguageCollection = "SiteTemplateLanguageCollection";
			/// <summary>Member name StationLanguageCollection</summary>
			public static readonly string StationLanguageCollection = "StationLanguageCollection";
			/// <summary>Member name SurveyAnswerLanguageCollection</summary>
			public static readonly string SurveyAnswerLanguageCollection = "SurveyAnswerLanguageCollection";
			/// <summary>Member name SurveyLanguageCollection</summary>
			public static readonly string SurveyLanguageCollection = "SurveyLanguageCollection";
			/// <summary>Member name SurveyQuestionLanguageCollection</summary>
			public static readonly string SurveyQuestionLanguageCollection = "SurveyQuestionLanguageCollection";
			/// <summary>Member name UIFooterItemLanguageCollection</summary>
			public static readonly string UIFooterItemLanguageCollection = "UIFooterItemLanguageCollection";
			/// <summary>Member name UITabLanguageCollection</summary>
			public static readonly string UITabLanguageCollection = "UITabLanguageCollection";
			/// <summary>Member name UIWidgetLanguageCollection</summary>
			public static readonly string UIWidgetLanguageCollection = "UIWidgetLanguageCollection";
			/// <summary>Member name VenueCategoryLanguageCollection</summary>
			public static readonly string VenueCategoryLanguageCollection = "VenueCategoryLanguageCollection";
			/// <summary>Member name EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage</summary>
			public static readonly string EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = "EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage";
			/// <summary>Member name RouteCollectionViaCompany</summary>
			public static readonly string RouteCollectionViaCompany = "RouteCollectionViaCompany";
			/// <summary>Member name SupportpoolCollectionViaCompany</summary>
			public static readonly string SupportpoolCollectionViaCompany = "SupportpoolCollectionViaCompany";
			/// <summary>Member name UITabCollectionViaUITabLanguage</summary>
			public static readonly string UITabCollectionViaUITabLanguage = "UITabCollectionViaUITabLanguage";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static LanguageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected LanguageEntityBase() :base("LanguageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		protected LanguageEntityBase(System.Int32 languageId):base("LanguageEntity")
		{
			InitClassFetch(languageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected LanguageEntityBase(System.Int32 languageId, IPrefetchPath prefetchPathToUse): base("LanguageEntity")
		{
			InitClassFetch(languageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <param name="validator">The custom validator object for this LanguageEntity</param>
		protected LanguageEntityBase(System.Int32 languageId, IValidator validator):base("LanguageEntity")
		{
			InitClassFetch(languageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LanguageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_actionButtonLanguageCollection = (Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection)info.GetValue("_actionButtonLanguageCollection", typeof(Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection));
			_alwaysFetchActionButtonLanguageCollection = info.GetBoolean("_alwaysFetchActionButtonLanguageCollection");
			_alreadyFetchedActionButtonLanguageCollection = info.GetBoolean("_alreadyFetchedActionButtonLanguageCollection");

			_advertisementLanguageCollection = (Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection)info.GetValue("_advertisementLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection));
			_alwaysFetchAdvertisementLanguageCollection = info.GetBoolean("_alwaysFetchAdvertisementLanguageCollection");
			_alreadyFetchedAdvertisementLanguageCollection = info.GetBoolean("_alreadyFetchedAdvertisementLanguageCollection");

			_advertisementTagLanguageCollection = (Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection)info.GetValue("_advertisementTagLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection));
			_alwaysFetchAdvertisementTagLanguageCollection = info.GetBoolean("_alwaysFetchAdvertisementTagLanguageCollection");
			_alreadyFetchedAdvertisementTagLanguageCollection = info.GetBoolean("_alreadyFetchedAdvertisementTagLanguageCollection");

			_alterationLanguageCollection = (Obymobi.Data.CollectionClasses.AlterationLanguageCollection)info.GetValue("_alterationLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AlterationLanguageCollection));
			_alwaysFetchAlterationLanguageCollection = info.GetBoolean("_alwaysFetchAlterationLanguageCollection");
			_alreadyFetchedAlterationLanguageCollection = info.GetBoolean("_alreadyFetchedAlterationLanguageCollection");

			_alterationoptionLanguageCollection = (Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection)info.GetValue("_alterationoptionLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection));
			_alwaysFetchAlterationoptionLanguageCollection = info.GetBoolean("_alwaysFetchAlterationoptionLanguageCollection");
			_alreadyFetchedAlterationoptionLanguageCollection = info.GetBoolean("_alreadyFetchedAlterationoptionLanguageCollection");

			_amenityLanguageCollection = (Obymobi.Data.CollectionClasses.AmenityLanguageCollection)info.GetValue("_amenityLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AmenityLanguageCollection));
			_alwaysFetchAmenityLanguageCollection = info.GetBoolean("_alwaysFetchAmenityLanguageCollection");
			_alreadyFetchedAmenityLanguageCollection = info.GetBoolean("_alreadyFetchedAmenityLanguageCollection");

			_announcementLanguageCollection = (Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection)info.GetValue("_announcementLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection));
			_alwaysFetchAnnouncementLanguageCollection = info.GetBoolean("_alwaysFetchAnnouncementLanguageCollection");
			_alreadyFetchedAnnouncementLanguageCollection = info.GetBoolean("_alreadyFetchedAnnouncementLanguageCollection");

			_attachmentLanguageCollection = (Obymobi.Data.CollectionClasses.AttachmentLanguageCollection)info.GetValue("_attachmentLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AttachmentLanguageCollection));
			_alwaysFetchAttachmentLanguageCollection = info.GetBoolean("_alwaysFetchAttachmentLanguageCollection");
			_alreadyFetchedAttachmentLanguageCollection = info.GetBoolean("_alreadyFetchedAttachmentLanguageCollection");

			_categoryLanguageCollection = (Obymobi.Data.CollectionClasses.CategoryLanguageCollection)info.GetValue("_categoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.CategoryLanguageCollection));
			_alwaysFetchCategoryLanguageCollection = info.GetBoolean("_alwaysFetchCategoryLanguageCollection");
			_alreadyFetchedCategoryLanguageCollection = info.GetBoolean("_alreadyFetchedCategoryLanguageCollection");

			_companyCollection = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollection", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollection = info.GetBoolean("_alwaysFetchCompanyCollection");
			_alreadyFetchedCompanyCollection = info.GetBoolean("_alreadyFetchedCompanyCollection");

			_companyLanguageCollection = (Obymobi.Data.CollectionClasses.CompanyLanguageCollection)info.GetValue("_companyLanguageCollection", typeof(Obymobi.Data.CollectionClasses.CompanyLanguageCollection));
			_alwaysFetchCompanyLanguageCollection = info.GetBoolean("_alwaysFetchCompanyLanguageCollection");
			_alreadyFetchedCompanyLanguageCollection = info.GetBoolean("_alreadyFetchedCompanyLanguageCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_deliverypointgroupLanguageCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection)info.GetValue("_deliverypointgroupLanguageCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection));
			_alwaysFetchDeliverypointgroupLanguageCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupLanguageCollection");
			_alreadyFetchedDeliverypointgroupLanguageCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupLanguageCollection");

			_entertainmentcategoryLanguageCollection = (Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection)info.GetValue("_entertainmentcategoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection));
			_alwaysFetchEntertainmentcategoryLanguageCollection = info.GetBoolean("_alwaysFetchEntertainmentcategoryLanguageCollection");
			_alreadyFetchedEntertainmentcategoryLanguageCollection = info.GetBoolean("_alreadyFetchedEntertainmentcategoryLanguageCollection");

			_genericcategoryLanguageCollection = (Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection)info.GetValue("_genericcategoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection));
			_alwaysFetchGenericcategoryLanguageCollection = info.GetBoolean("_alwaysFetchGenericcategoryLanguageCollection");
			_alreadyFetchedGenericcategoryLanguageCollection = info.GetBoolean("_alreadyFetchedGenericcategoryLanguageCollection");

			_genericproductLanguageCollection = (Obymobi.Data.CollectionClasses.GenericproductLanguageCollection)info.GetValue("_genericproductLanguageCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductLanguageCollection));
			_alwaysFetchGenericproductLanguageCollection = info.GetBoolean("_alwaysFetchGenericproductLanguageCollection");
			_alreadyFetchedGenericproductLanguageCollection = info.GetBoolean("_alreadyFetchedGenericproductLanguageCollection");

			_mediaLanguageCollection = (Obymobi.Data.CollectionClasses.MediaLanguageCollection)info.GetValue("_mediaLanguageCollection", typeof(Obymobi.Data.CollectionClasses.MediaLanguageCollection));
			_alwaysFetchMediaLanguageCollection = info.GetBoolean("_alwaysFetchMediaLanguageCollection");
			_alreadyFetchedMediaLanguageCollection = info.GetBoolean("_alreadyFetchedMediaLanguageCollection");

			_pageElementCollection = (Obymobi.Data.CollectionClasses.PageElementCollection)info.GetValue("_pageElementCollection", typeof(Obymobi.Data.CollectionClasses.PageElementCollection));
			_alwaysFetchPageElementCollection = info.GetBoolean("_alwaysFetchPageElementCollection");
			_alreadyFetchedPageElementCollection = info.GetBoolean("_alreadyFetchedPageElementCollection");

			_pageLanguageCollection = (Obymobi.Data.CollectionClasses.PageLanguageCollection)info.GetValue("_pageLanguageCollection", typeof(Obymobi.Data.CollectionClasses.PageLanguageCollection));
			_alwaysFetchPageLanguageCollection = info.GetBoolean("_alwaysFetchPageLanguageCollection");
			_alreadyFetchedPageLanguageCollection = info.GetBoolean("_alreadyFetchedPageLanguageCollection");

			_pageTemplateElementCollection = (Obymobi.Data.CollectionClasses.PageTemplateElementCollection)info.GetValue("_pageTemplateElementCollection", typeof(Obymobi.Data.CollectionClasses.PageTemplateElementCollection));
			_alwaysFetchPageTemplateElementCollection = info.GetBoolean("_alwaysFetchPageTemplateElementCollection");
			_alreadyFetchedPageTemplateElementCollection = info.GetBoolean("_alreadyFetchedPageTemplateElementCollection");

			_pageTemplateLanguageCollection = (Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection)info.GetValue("_pageTemplateLanguageCollection", typeof(Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection));
			_alwaysFetchPageTemplateLanguageCollection = info.GetBoolean("_alwaysFetchPageTemplateLanguageCollection");
			_alreadyFetchedPageTemplateLanguageCollection = info.GetBoolean("_alreadyFetchedPageTemplateLanguageCollection");

			_pointOfInterestLanguageCollection = (Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection)info.GetValue("_pointOfInterestLanguageCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection));
			_alwaysFetchPointOfInterestLanguageCollection = info.GetBoolean("_alwaysFetchPointOfInterestLanguageCollection");
			_alreadyFetchedPointOfInterestLanguageCollection = info.GetBoolean("_alreadyFetchedPointOfInterestLanguageCollection");

			_productLanguageCollection = (Obymobi.Data.CollectionClasses.ProductLanguageCollection)info.GetValue("_productLanguageCollection", typeof(Obymobi.Data.CollectionClasses.ProductLanguageCollection));
			_alwaysFetchProductLanguageCollection = info.GetBoolean("_alwaysFetchProductLanguageCollection");
			_alreadyFetchedProductLanguageCollection = info.GetBoolean("_alreadyFetchedProductLanguageCollection");

			_roomControlAreaLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection)info.GetValue("_roomControlAreaLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection));
			_alwaysFetchRoomControlAreaLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlAreaLanguageCollection");
			_alreadyFetchedRoomControlAreaLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlAreaLanguageCollection");

			_roomControlComponentLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection)info.GetValue("_roomControlComponentLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection));
			_alwaysFetchRoomControlComponentLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlComponentLanguageCollection");
			_alreadyFetchedRoomControlComponentLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlComponentLanguageCollection");

			_roomControlSectionItemLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection)info.GetValue("_roomControlSectionItemLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection));
			_alwaysFetchRoomControlSectionItemLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlSectionItemLanguageCollection");
			_alreadyFetchedRoomControlSectionItemLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlSectionItemLanguageCollection");

			_roomControlSectionLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection)info.GetValue("_roomControlSectionLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection));
			_alwaysFetchRoomControlSectionLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlSectionLanguageCollection");
			_alreadyFetchedRoomControlSectionLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlSectionLanguageCollection");

			_roomControlWidgetLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection)info.GetValue("_roomControlWidgetLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection));
			_alwaysFetchRoomControlWidgetLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlWidgetLanguageCollection");
			_alreadyFetchedRoomControlWidgetLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlWidgetLanguageCollection");

			_scheduledMessageLanguageCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection)info.GetValue("_scheduledMessageLanguageCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection));
			_alwaysFetchScheduledMessageLanguageCollection = info.GetBoolean("_alwaysFetchScheduledMessageLanguageCollection");
			_alreadyFetchedScheduledMessageLanguageCollection = info.GetBoolean("_alreadyFetchedScheduledMessageLanguageCollection");

			_siteLanguageCollection = (Obymobi.Data.CollectionClasses.SiteLanguageCollection)info.GetValue("_siteLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SiteLanguageCollection));
			_alwaysFetchSiteLanguageCollection = info.GetBoolean("_alwaysFetchSiteLanguageCollection");
			_alreadyFetchedSiteLanguageCollection = info.GetBoolean("_alreadyFetchedSiteLanguageCollection");

			_siteTemplateLanguageCollection = (Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection)info.GetValue("_siteTemplateLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection));
			_alwaysFetchSiteTemplateLanguageCollection = info.GetBoolean("_alwaysFetchSiteTemplateLanguageCollection");
			_alreadyFetchedSiteTemplateLanguageCollection = info.GetBoolean("_alreadyFetchedSiteTemplateLanguageCollection");

			_stationLanguageCollection = (Obymobi.Data.CollectionClasses.StationLanguageCollection)info.GetValue("_stationLanguageCollection", typeof(Obymobi.Data.CollectionClasses.StationLanguageCollection));
			_alwaysFetchStationLanguageCollection = info.GetBoolean("_alwaysFetchStationLanguageCollection");
			_alreadyFetchedStationLanguageCollection = info.GetBoolean("_alreadyFetchedStationLanguageCollection");

			_surveyAnswerLanguageCollection = (Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection)info.GetValue("_surveyAnswerLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection));
			_alwaysFetchSurveyAnswerLanguageCollection = info.GetBoolean("_alwaysFetchSurveyAnswerLanguageCollection");
			_alreadyFetchedSurveyAnswerLanguageCollection = info.GetBoolean("_alreadyFetchedSurveyAnswerLanguageCollection");

			_surveyLanguageCollection = (Obymobi.Data.CollectionClasses.SurveyLanguageCollection)info.GetValue("_surveyLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SurveyLanguageCollection));
			_alwaysFetchSurveyLanguageCollection = info.GetBoolean("_alwaysFetchSurveyLanguageCollection");
			_alreadyFetchedSurveyLanguageCollection = info.GetBoolean("_alreadyFetchedSurveyLanguageCollection");

			_surveyQuestionLanguageCollection = (Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection)info.GetValue("_surveyQuestionLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection));
			_alwaysFetchSurveyQuestionLanguageCollection = info.GetBoolean("_alwaysFetchSurveyQuestionLanguageCollection");
			_alreadyFetchedSurveyQuestionLanguageCollection = info.GetBoolean("_alreadyFetchedSurveyQuestionLanguageCollection");

			_uIFooterItemLanguageCollection = (Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection)info.GetValue("_uIFooterItemLanguageCollection", typeof(Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection));
			_alwaysFetchUIFooterItemLanguageCollection = info.GetBoolean("_alwaysFetchUIFooterItemLanguageCollection");
			_alreadyFetchedUIFooterItemLanguageCollection = info.GetBoolean("_alreadyFetchedUIFooterItemLanguageCollection");

			_uITabLanguageCollection = (Obymobi.Data.CollectionClasses.UITabLanguageCollection)info.GetValue("_uITabLanguageCollection", typeof(Obymobi.Data.CollectionClasses.UITabLanguageCollection));
			_alwaysFetchUITabLanguageCollection = info.GetBoolean("_alwaysFetchUITabLanguageCollection");
			_alreadyFetchedUITabLanguageCollection = info.GetBoolean("_alreadyFetchedUITabLanguageCollection");

			_uIWidgetLanguageCollection = (Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection)info.GetValue("_uIWidgetLanguageCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection));
			_alwaysFetchUIWidgetLanguageCollection = info.GetBoolean("_alwaysFetchUIWidgetLanguageCollection");
			_alreadyFetchedUIWidgetLanguageCollection = info.GetBoolean("_alreadyFetchedUIWidgetLanguageCollection");

			_venueCategoryLanguageCollection = (Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection)info.GetValue("_venueCategoryLanguageCollection", typeof(Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection));
			_alwaysFetchVenueCategoryLanguageCollection = info.GetBoolean("_alwaysFetchVenueCategoryLanguageCollection");
			_alreadyFetchedVenueCategoryLanguageCollection = info.GetBoolean("_alreadyFetchedVenueCategoryLanguageCollection");
			_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage");
			_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage");

			_routeCollectionViaCompany = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCompany = info.GetBoolean("_alwaysFetchRouteCollectionViaCompany");
			_alreadyFetchedRouteCollectionViaCompany = info.GetBoolean("_alreadyFetchedRouteCollectionViaCompany");

			_supportpoolCollectionViaCompany = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaCompany = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaCompany");
			_alreadyFetchedSupportpoolCollectionViaCompany = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaCompany");

			_uITabCollectionViaUITabLanguage = (Obymobi.Data.CollectionClasses.UITabCollection)info.GetValue("_uITabCollectionViaUITabLanguage", typeof(Obymobi.Data.CollectionClasses.UITabCollection));
			_alwaysFetchUITabCollectionViaUITabLanguage = info.GetBoolean("_alwaysFetchUITabCollectionViaUITabLanguage");
			_alreadyFetchedUITabCollectionViaUITabLanguage = info.GetBoolean("_alreadyFetchedUITabCollectionViaUITabLanguage");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedActionButtonLanguageCollection = (_actionButtonLanguageCollection.Count > 0);
			_alreadyFetchedAdvertisementLanguageCollection = (_advertisementLanguageCollection.Count > 0);
			_alreadyFetchedAdvertisementTagLanguageCollection = (_advertisementTagLanguageCollection.Count > 0);
			_alreadyFetchedAlterationLanguageCollection = (_alterationLanguageCollection.Count > 0);
			_alreadyFetchedAlterationoptionLanguageCollection = (_alterationoptionLanguageCollection.Count > 0);
			_alreadyFetchedAmenityLanguageCollection = (_amenityLanguageCollection.Count > 0);
			_alreadyFetchedAnnouncementLanguageCollection = (_announcementLanguageCollection.Count > 0);
			_alreadyFetchedAttachmentLanguageCollection = (_attachmentLanguageCollection.Count > 0);
			_alreadyFetchedCategoryLanguageCollection = (_categoryLanguageCollection.Count > 0);
			_alreadyFetchedCompanyCollection = (_companyCollection.Count > 0);
			_alreadyFetchedCompanyLanguageCollection = (_companyLanguageCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupLanguageCollection = (_deliverypointgroupLanguageCollection.Count > 0);
			_alreadyFetchedEntertainmentcategoryLanguageCollection = (_entertainmentcategoryLanguageCollection.Count > 0);
			_alreadyFetchedGenericcategoryLanguageCollection = (_genericcategoryLanguageCollection.Count > 0);
			_alreadyFetchedGenericproductLanguageCollection = (_genericproductLanguageCollection.Count > 0);
			_alreadyFetchedMediaLanguageCollection = (_mediaLanguageCollection.Count > 0);
			_alreadyFetchedPageElementCollection = (_pageElementCollection.Count > 0);
			_alreadyFetchedPageLanguageCollection = (_pageLanguageCollection.Count > 0);
			_alreadyFetchedPageTemplateElementCollection = (_pageTemplateElementCollection.Count > 0);
			_alreadyFetchedPageTemplateLanguageCollection = (_pageTemplateLanguageCollection.Count > 0);
			_alreadyFetchedPointOfInterestLanguageCollection = (_pointOfInterestLanguageCollection.Count > 0);
			_alreadyFetchedProductLanguageCollection = (_productLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlAreaLanguageCollection = (_roomControlAreaLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlComponentLanguageCollection = (_roomControlComponentLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlSectionItemLanguageCollection = (_roomControlSectionItemLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlSectionLanguageCollection = (_roomControlSectionLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlWidgetLanguageCollection = (_roomControlWidgetLanguageCollection.Count > 0);
			_alreadyFetchedScheduledMessageLanguageCollection = (_scheduledMessageLanguageCollection.Count > 0);
			_alreadyFetchedSiteLanguageCollection = (_siteLanguageCollection.Count > 0);
			_alreadyFetchedSiteTemplateLanguageCollection = (_siteTemplateLanguageCollection.Count > 0);
			_alreadyFetchedStationLanguageCollection = (_stationLanguageCollection.Count > 0);
			_alreadyFetchedSurveyAnswerLanguageCollection = (_surveyAnswerLanguageCollection.Count > 0);
			_alreadyFetchedSurveyLanguageCollection = (_surveyLanguageCollection.Count > 0);
			_alreadyFetchedSurveyQuestionLanguageCollection = (_surveyQuestionLanguageCollection.Count > 0);
			_alreadyFetchedUIFooterItemLanguageCollection = (_uIFooterItemLanguageCollection.Count > 0);
			_alreadyFetchedUITabLanguageCollection = (_uITabLanguageCollection.Count > 0);
			_alreadyFetchedUIWidgetLanguageCollection = (_uIWidgetLanguageCollection.Count > 0);
			_alreadyFetchedVenueCategoryLanguageCollection = (_venueCategoryLanguageCollection.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = (_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.Count > 0);
			_alreadyFetchedRouteCollectionViaCompany = (_routeCollectionViaCompany.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaCompany = (_supportpoolCollectionViaCompany.Count > 0);
			_alreadyFetchedUITabCollectionViaUITabLanguage = (_uITabCollectionViaUITabLanguage.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ActionButtonLanguageCollection":
					toReturn.Add(Relations.ActionButtonLanguageEntityUsingLanguageId);
					break;
				case "AdvertisementLanguageCollection":
					toReturn.Add(Relations.AdvertisementLanguageEntityUsingLanguageId);
					break;
				case "AdvertisementTagLanguageCollection":
					toReturn.Add(Relations.AdvertisementTagLanguageEntityUsingLanguageId);
					break;
				case "AlterationLanguageCollection":
					toReturn.Add(Relations.AlterationLanguageEntityUsingLanguageId);
					break;
				case "AlterationoptionLanguageCollection":
					toReturn.Add(Relations.AlterationoptionLanguageEntityUsingLanguageId);
					break;
				case "AmenityLanguageCollection":
					toReturn.Add(Relations.AmenityLanguageEntityUsingLanguageId);
					break;
				case "AnnouncementLanguageCollection":
					toReturn.Add(Relations.AnnouncementLanguageEntityUsingLanguageId);
					break;
				case "AttachmentLanguageCollection":
					toReturn.Add(Relations.AttachmentLanguageEntityUsingLanguageId);
					break;
				case "CategoryLanguageCollection":
					toReturn.Add(Relations.CategoryLanguageEntityUsingLanguageId);
					break;
				case "CompanyCollection":
					toReturn.Add(Relations.CompanyEntityUsingLanguageId);
					break;
				case "CompanyLanguageCollection":
					toReturn.Add(Relations.CompanyLanguageEntityUsingLanguageId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingLanguageId);
					break;
				case "DeliverypointgroupLanguageCollection":
					toReturn.Add(Relations.DeliverypointgroupLanguageEntityUsingLanguageId);
					break;
				case "EntertainmentcategoryLanguageCollection":
					toReturn.Add(Relations.EntertainmentcategoryLanguageEntityUsingLanguageId);
					break;
				case "GenericcategoryLanguageCollection":
					toReturn.Add(Relations.GenericcategoryLanguageEntityUsingLanguageId);
					break;
				case "GenericproductLanguageCollection":
					toReturn.Add(Relations.GenericproductLanguageEntityUsingLanguageId);
					break;
				case "MediaLanguageCollection":
					toReturn.Add(Relations.MediaLanguageEntityUsingLanguageId);
					break;
				case "PageElementCollection":
					toReturn.Add(Relations.PageElementEntityUsingLanguageId);
					break;
				case "PageLanguageCollection":
					toReturn.Add(Relations.PageLanguageEntityUsingLanguageId);
					break;
				case "PageTemplateElementCollection":
					toReturn.Add(Relations.PageTemplateElementEntityUsingLanguageId);
					break;
				case "PageTemplateLanguageCollection":
					toReturn.Add(Relations.PageTemplateLanguageEntityUsingLanguageId);
					break;
				case "PointOfInterestLanguageCollection":
					toReturn.Add(Relations.PointOfInterestLanguageEntityUsingLanguageId);
					break;
				case "ProductLanguageCollection":
					toReturn.Add(Relations.ProductLanguageEntityUsingLanguageId);
					break;
				case "RoomControlAreaLanguageCollection":
					toReturn.Add(Relations.RoomControlAreaLanguageEntityUsingLanguageId);
					break;
				case "RoomControlComponentLanguageCollection":
					toReturn.Add(Relations.RoomControlComponentLanguageEntityUsingLanguageId);
					break;
				case "RoomControlSectionItemLanguageCollection":
					toReturn.Add(Relations.RoomControlSectionItemLanguageEntityUsingLanguageId);
					break;
				case "RoomControlSectionLanguageCollection":
					toReturn.Add(Relations.RoomControlSectionLanguageEntityUsingLanguageId);
					break;
				case "RoomControlWidgetLanguageCollection":
					toReturn.Add(Relations.RoomControlWidgetLanguageEntityUsingLanguageId);
					break;
				case "ScheduledMessageLanguageCollection":
					toReturn.Add(Relations.ScheduledMessageLanguageEntityUsingLanguageId);
					break;
				case "SiteLanguageCollection":
					toReturn.Add(Relations.SiteLanguageEntityUsingLanguageId);
					break;
				case "SiteTemplateLanguageCollection":
					toReturn.Add(Relations.SiteTemplateLanguageEntityUsingLanguageId);
					break;
				case "StationLanguageCollection":
					toReturn.Add(Relations.StationLanguageEntityUsingLanguageId);
					break;
				case "SurveyAnswerLanguageCollection":
					toReturn.Add(Relations.SurveyAnswerLanguageEntityUsingLanguageId);
					break;
				case "SurveyLanguageCollection":
					toReturn.Add(Relations.SurveyLanguageEntityUsingLanguageId);
					break;
				case "SurveyQuestionLanguageCollection":
					toReturn.Add(Relations.SurveyQuestionLanguageEntityUsingLanguageId);
					break;
				case "UIFooterItemLanguageCollection":
					toReturn.Add(Relations.UIFooterItemLanguageEntityUsingLanguageId);
					break;
				case "UITabLanguageCollection":
					toReturn.Add(Relations.UITabLanguageEntityUsingLanguageId);
					break;
				case "UIWidgetLanguageCollection":
					toReturn.Add(Relations.UIWidgetLanguageEntityUsingLanguageId);
					break;
				case "VenueCategoryLanguageCollection":
					toReturn.Add(Relations.VenueCategoryLanguageEntityUsingLanguageId);
					break;
				case "EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage":
					toReturn.Add(Relations.EntertainmentcategoryLanguageEntityUsingLanguageId, "LanguageEntity__", "EntertainmentcategoryLanguage_", JoinHint.None);
					toReturn.Add(EntertainmentcategoryLanguageEntity.Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId, "EntertainmentcategoryLanguage_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingLanguageId, "LanguageEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.RouteEntityUsingRouteId, "Company_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingLanguageId, "LanguageEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.SupportpoolEntityUsingSupportpoolId, "Company_", string.Empty, JoinHint.None);
					break;
				case "UITabCollectionViaUITabLanguage":
					toReturn.Add(Relations.UITabLanguageEntityUsingLanguageId, "LanguageEntity__", "UITabLanguage_", JoinHint.None);
					toReturn.Add(UITabLanguageEntity.Relations.UITabEntityUsingUITabId, "UITabLanguage_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_actionButtonLanguageCollection", (!this.MarkedForDeletion?_actionButtonLanguageCollection:null));
			info.AddValue("_alwaysFetchActionButtonLanguageCollection", _alwaysFetchActionButtonLanguageCollection);
			info.AddValue("_alreadyFetchedActionButtonLanguageCollection", _alreadyFetchedActionButtonLanguageCollection);
			info.AddValue("_advertisementLanguageCollection", (!this.MarkedForDeletion?_advertisementLanguageCollection:null));
			info.AddValue("_alwaysFetchAdvertisementLanguageCollection", _alwaysFetchAdvertisementLanguageCollection);
			info.AddValue("_alreadyFetchedAdvertisementLanguageCollection", _alreadyFetchedAdvertisementLanguageCollection);
			info.AddValue("_advertisementTagLanguageCollection", (!this.MarkedForDeletion?_advertisementTagLanguageCollection:null));
			info.AddValue("_alwaysFetchAdvertisementTagLanguageCollection", _alwaysFetchAdvertisementTagLanguageCollection);
			info.AddValue("_alreadyFetchedAdvertisementTagLanguageCollection", _alreadyFetchedAdvertisementTagLanguageCollection);
			info.AddValue("_alterationLanguageCollection", (!this.MarkedForDeletion?_alterationLanguageCollection:null));
			info.AddValue("_alwaysFetchAlterationLanguageCollection", _alwaysFetchAlterationLanguageCollection);
			info.AddValue("_alreadyFetchedAlterationLanguageCollection", _alreadyFetchedAlterationLanguageCollection);
			info.AddValue("_alterationoptionLanguageCollection", (!this.MarkedForDeletion?_alterationoptionLanguageCollection:null));
			info.AddValue("_alwaysFetchAlterationoptionLanguageCollection", _alwaysFetchAlterationoptionLanguageCollection);
			info.AddValue("_alreadyFetchedAlterationoptionLanguageCollection", _alreadyFetchedAlterationoptionLanguageCollection);
			info.AddValue("_amenityLanguageCollection", (!this.MarkedForDeletion?_amenityLanguageCollection:null));
			info.AddValue("_alwaysFetchAmenityLanguageCollection", _alwaysFetchAmenityLanguageCollection);
			info.AddValue("_alreadyFetchedAmenityLanguageCollection", _alreadyFetchedAmenityLanguageCollection);
			info.AddValue("_announcementLanguageCollection", (!this.MarkedForDeletion?_announcementLanguageCollection:null));
			info.AddValue("_alwaysFetchAnnouncementLanguageCollection", _alwaysFetchAnnouncementLanguageCollection);
			info.AddValue("_alreadyFetchedAnnouncementLanguageCollection", _alreadyFetchedAnnouncementLanguageCollection);
			info.AddValue("_attachmentLanguageCollection", (!this.MarkedForDeletion?_attachmentLanguageCollection:null));
			info.AddValue("_alwaysFetchAttachmentLanguageCollection", _alwaysFetchAttachmentLanguageCollection);
			info.AddValue("_alreadyFetchedAttachmentLanguageCollection", _alreadyFetchedAttachmentLanguageCollection);
			info.AddValue("_categoryLanguageCollection", (!this.MarkedForDeletion?_categoryLanguageCollection:null));
			info.AddValue("_alwaysFetchCategoryLanguageCollection", _alwaysFetchCategoryLanguageCollection);
			info.AddValue("_alreadyFetchedCategoryLanguageCollection", _alreadyFetchedCategoryLanguageCollection);
			info.AddValue("_companyCollection", (!this.MarkedForDeletion?_companyCollection:null));
			info.AddValue("_alwaysFetchCompanyCollection", _alwaysFetchCompanyCollection);
			info.AddValue("_alreadyFetchedCompanyCollection", _alreadyFetchedCompanyCollection);
			info.AddValue("_companyLanguageCollection", (!this.MarkedForDeletion?_companyLanguageCollection:null));
			info.AddValue("_alwaysFetchCompanyLanguageCollection", _alwaysFetchCompanyLanguageCollection);
			info.AddValue("_alreadyFetchedCompanyLanguageCollection", _alreadyFetchedCompanyLanguageCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_deliverypointgroupLanguageCollection", (!this.MarkedForDeletion?_deliverypointgroupLanguageCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupLanguageCollection", _alwaysFetchDeliverypointgroupLanguageCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupLanguageCollection", _alreadyFetchedDeliverypointgroupLanguageCollection);
			info.AddValue("_entertainmentcategoryLanguageCollection", (!this.MarkedForDeletion?_entertainmentcategoryLanguageCollection:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryLanguageCollection", _alwaysFetchEntertainmentcategoryLanguageCollection);
			info.AddValue("_alreadyFetchedEntertainmentcategoryLanguageCollection", _alreadyFetchedEntertainmentcategoryLanguageCollection);
			info.AddValue("_genericcategoryLanguageCollection", (!this.MarkedForDeletion?_genericcategoryLanguageCollection:null));
			info.AddValue("_alwaysFetchGenericcategoryLanguageCollection", _alwaysFetchGenericcategoryLanguageCollection);
			info.AddValue("_alreadyFetchedGenericcategoryLanguageCollection", _alreadyFetchedGenericcategoryLanguageCollection);
			info.AddValue("_genericproductLanguageCollection", (!this.MarkedForDeletion?_genericproductLanguageCollection:null));
			info.AddValue("_alwaysFetchGenericproductLanguageCollection", _alwaysFetchGenericproductLanguageCollection);
			info.AddValue("_alreadyFetchedGenericproductLanguageCollection", _alreadyFetchedGenericproductLanguageCollection);
			info.AddValue("_mediaLanguageCollection", (!this.MarkedForDeletion?_mediaLanguageCollection:null));
			info.AddValue("_alwaysFetchMediaLanguageCollection", _alwaysFetchMediaLanguageCollection);
			info.AddValue("_alreadyFetchedMediaLanguageCollection", _alreadyFetchedMediaLanguageCollection);
			info.AddValue("_pageElementCollection", (!this.MarkedForDeletion?_pageElementCollection:null));
			info.AddValue("_alwaysFetchPageElementCollection", _alwaysFetchPageElementCollection);
			info.AddValue("_alreadyFetchedPageElementCollection", _alreadyFetchedPageElementCollection);
			info.AddValue("_pageLanguageCollection", (!this.MarkedForDeletion?_pageLanguageCollection:null));
			info.AddValue("_alwaysFetchPageLanguageCollection", _alwaysFetchPageLanguageCollection);
			info.AddValue("_alreadyFetchedPageLanguageCollection", _alreadyFetchedPageLanguageCollection);
			info.AddValue("_pageTemplateElementCollection", (!this.MarkedForDeletion?_pageTemplateElementCollection:null));
			info.AddValue("_alwaysFetchPageTemplateElementCollection", _alwaysFetchPageTemplateElementCollection);
			info.AddValue("_alreadyFetchedPageTemplateElementCollection", _alreadyFetchedPageTemplateElementCollection);
			info.AddValue("_pageTemplateLanguageCollection", (!this.MarkedForDeletion?_pageTemplateLanguageCollection:null));
			info.AddValue("_alwaysFetchPageTemplateLanguageCollection", _alwaysFetchPageTemplateLanguageCollection);
			info.AddValue("_alreadyFetchedPageTemplateLanguageCollection", _alreadyFetchedPageTemplateLanguageCollection);
			info.AddValue("_pointOfInterestLanguageCollection", (!this.MarkedForDeletion?_pointOfInterestLanguageCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestLanguageCollection", _alwaysFetchPointOfInterestLanguageCollection);
			info.AddValue("_alreadyFetchedPointOfInterestLanguageCollection", _alreadyFetchedPointOfInterestLanguageCollection);
			info.AddValue("_productLanguageCollection", (!this.MarkedForDeletion?_productLanguageCollection:null));
			info.AddValue("_alwaysFetchProductLanguageCollection", _alwaysFetchProductLanguageCollection);
			info.AddValue("_alreadyFetchedProductLanguageCollection", _alreadyFetchedProductLanguageCollection);
			info.AddValue("_roomControlAreaLanguageCollection", (!this.MarkedForDeletion?_roomControlAreaLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlAreaLanguageCollection", _alwaysFetchRoomControlAreaLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlAreaLanguageCollection", _alreadyFetchedRoomControlAreaLanguageCollection);
			info.AddValue("_roomControlComponentLanguageCollection", (!this.MarkedForDeletion?_roomControlComponentLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlComponentLanguageCollection", _alwaysFetchRoomControlComponentLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlComponentLanguageCollection", _alreadyFetchedRoomControlComponentLanguageCollection);
			info.AddValue("_roomControlSectionItemLanguageCollection", (!this.MarkedForDeletion?_roomControlSectionItemLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlSectionItemLanguageCollection", _alwaysFetchRoomControlSectionItemLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlSectionItemLanguageCollection", _alreadyFetchedRoomControlSectionItemLanguageCollection);
			info.AddValue("_roomControlSectionLanguageCollection", (!this.MarkedForDeletion?_roomControlSectionLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlSectionLanguageCollection", _alwaysFetchRoomControlSectionLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlSectionLanguageCollection", _alreadyFetchedRoomControlSectionLanguageCollection);
			info.AddValue("_roomControlWidgetLanguageCollection", (!this.MarkedForDeletion?_roomControlWidgetLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlWidgetLanguageCollection", _alwaysFetchRoomControlWidgetLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlWidgetLanguageCollection", _alreadyFetchedRoomControlWidgetLanguageCollection);
			info.AddValue("_scheduledMessageLanguageCollection", (!this.MarkedForDeletion?_scheduledMessageLanguageCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageLanguageCollection", _alwaysFetchScheduledMessageLanguageCollection);
			info.AddValue("_alreadyFetchedScheduledMessageLanguageCollection", _alreadyFetchedScheduledMessageLanguageCollection);
			info.AddValue("_siteLanguageCollection", (!this.MarkedForDeletion?_siteLanguageCollection:null));
			info.AddValue("_alwaysFetchSiteLanguageCollection", _alwaysFetchSiteLanguageCollection);
			info.AddValue("_alreadyFetchedSiteLanguageCollection", _alreadyFetchedSiteLanguageCollection);
			info.AddValue("_siteTemplateLanguageCollection", (!this.MarkedForDeletion?_siteTemplateLanguageCollection:null));
			info.AddValue("_alwaysFetchSiteTemplateLanguageCollection", _alwaysFetchSiteTemplateLanguageCollection);
			info.AddValue("_alreadyFetchedSiteTemplateLanguageCollection", _alreadyFetchedSiteTemplateLanguageCollection);
			info.AddValue("_stationLanguageCollection", (!this.MarkedForDeletion?_stationLanguageCollection:null));
			info.AddValue("_alwaysFetchStationLanguageCollection", _alwaysFetchStationLanguageCollection);
			info.AddValue("_alreadyFetchedStationLanguageCollection", _alreadyFetchedStationLanguageCollection);
			info.AddValue("_surveyAnswerLanguageCollection", (!this.MarkedForDeletion?_surveyAnswerLanguageCollection:null));
			info.AddValue("_alwaysFetchSurveyAnswerLanguageCollection", _alwaysFetchSurveyAnswerLanguageCollection);
			info.AddValue("_alreadyFetchedSurveyAnswerLanguageCollection", _alreadyFetchedSurveyAnswerLanguageCollection);
			info.AddValue("_surveyLanguageCollection", (!this.MarkedForDeletion?_surveyLanguageCollection:null));
			info.AddValue("_alwaysFetchSurveyLanguageCollection", _alwaysFetchSurveyLanguageCollection);
			info.AddValue("_alreadyFetchedSurveyLanguageCollection", _alreadyFetchedSurveyLanguageCollection);
			info.AddValue("_surveyQuestionLanguageCollection", (!this.MarkedForDeletion?_surveyQuestionLanguageCollection:null));
			info.AddValue("_alwaysFetchSurveyQuestionLanguageCollection", _alwaysFetchSurveyQuestionLanguageCollection);
			info.AddValue("_alreadyFetchedSurveyQuestionLanguageCollection", _alreadyFetchedSurveyQuestionLanguageCollection);
			info.AddValue("_uIFooterItemLanguageCollection", (!this.MarkedForDeletion?_uIFooterItemLanguageCollection:null));
			info.AddValue("_alwaysFetchUIFooterItemLanguageCollection", _alwaysFetchUIFooterItemLanguageCollection);
			info.AddValue("_alreadyFetchedUIFooterItemLanguageCollection", _alreadyFetchedUIFooterItemLanguageCollection);
			info.AddValue("_uITabLanguageCollection", (!this.MarkedForDeletion?_uITabLanguageCollection:null));
			info.AddValue("_alwaysFetchUITabLanguageCollection", _alwaysFetchUITabLanguageCollection);
			info.AddValue("_alreadyFetchedUITabLanguageCollection", _alreadyFetchedUITabLanguageCollection);
			info.AddValue("_uIWidgetLanguageCollection", (!this.MarkedForDeletion?_uIWidgetLanguageCollection:null));
			info.AddValue("_alwaysFetchUIWidgetLanguageCollection", _alwaysFetchUIWidgetLanguageCollection);
			info.AddValue("_alreadyFetchedUIWidgetLanguageCollection", _alreadyFetchedUIWidgetLanguageCollection);
			info.AddValue("_venueCategoryLanguageCollection", (!this.MarkedForDeletion?_venueCategoryLanguageCollection:null));
			info.AddValue("_alwaysFetchVenueCategoryLanguageCollection", _alwaysFetchVenueCategoryLanguageCollection);
			info.AddValue("_alreadyFetchedVenueCategoryLanguageCollection", _alreadyFetchedVenueCategoryLanguageCollection);
			info.AddValue("_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage", _alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage", _alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage);
			info.AddValue("_routeCollectionViaCompany", (!this.MarkedForDeletion?_routeCollectionViaCompany:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCompany", _alwaysFetchRouteCollectionViaCompany);
			info.AddValue("_alreadyFetchedRouteCollectionViaCompany", _alreadyFetchedRouteCollectionViaCompany);
			info.AddValue("_supportpoolCollectionViaCompany", (!this.MarkedForDeletion?_supportpoolCollectionViaCompany:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaCompany", _alwaysFetchSupportpoolCollectionViaCompany);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaCompany", _alreadyFetchedSupportpoolCollectionViaCompany);
			info.AddValue("_uITabCollectionViaUITabLanguage", (!this.MarkedForDeletion?_uITabCollectionViaUITabLanguage:null));
			info.AddValue("_alwaysFetchUITabCollectionViaUITabLanguage", _alwaysFetchUITabCollectionViaUITabLanguage);
			info.AddValue("_alreadyFetchedUITabCollectionViaUITabLanguage", _alreadyFetchedUITabCollectionViaUITabLanguage);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ActionButtonLanguageCollection":
					_alreadyFetchedActionButtonLanguageCollection = true;
					if(entity!=null)
					{
						this.ActionButtonLanguageCollection.Add((ActionButtonLanguageEntity)entity);
					}
					break;
				case "AdvertisementLanguageCollection":
					_alreadyFetchedAdvertisementLanguageCollection = true;
					if(entity!=null)
					{
						this.AdvertisementLanguageCollection.Add((AdvertisementLanguageEntity)entity);
					}
					break;
				case "AdvertisementTagLanguageCollection":
					_alreadyFetchedAdvertisementTagLanguageCollection = true;
					if(entity!=null)
					{
						this.AdvertisementTagLanguageCollection.Add((AdvertisementTagLanguageEntity)entity);
					}
					break;
				case "AlterationLanguageCollection":
					_alreadyFetchedAlterationLanguageCollection = true;
					if(entity!=null)
					{
						this.AlterationLanguageCollection.Add((AlterationLanguageEntity)entity);
					}
					break;
				case "AlterationoptionLanguageCollection":
					_alreadyFetchedAlterationoptionLanguageCollection = true;
					if(entity!=null)
					{
						this.AlterationoptionLanguageCollection.Add((AlterationoptionLanguageEntity)entity);
					}
					break;
				case "AmenityLanguageCollection":
					_alreadyFetchedAmenityLanguageCollection = true;
					if(entity!=null)
					{
						this.AmenityLanguageCollection.Add((AmenityLanguageEntity)entity);
					}
					break;
				case "AnnouncementLanguageCollection":
					_alreadyFetchedAnnouncementLanguageCollection = true;
					if(entity!=null)
					{
						this.AnnouncementLanguageCollection.Add((AnnouncementLanguageEntity)entity);
					}
					break;
				case "AttachmentLanguageCollection":
					_alreadyFetchedAttachmentLanguageCollection = true;
					if(entity!=null)
					{
						this.AttachmentLanguageCollection.Add((AttachmentLanguageEntity)entity);
					}
					break;
				case "CategoryLanguageCollection":
					_alreadyFetchedCategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.CategoryLanguageCollection.Add((CategoryLanguageEntity)entity);
					}
					break;
				case "CompanyCollection":
					_alreadyFetchedCompanyCollection = true;
					if(entity!=null)
					{
						this.CompanyCollection.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyLanguageCollection":
					_alreadyFetchedCompanyLanguageCollection = true;
					if(entity!=null)
					{
						this.CompanyLanguageCollection.Add((CompanyLanguageEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "DeliverypointgroupLanguageCollection":
					_alreadyFetchedDeliverypointgroupLanguageCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupLanguageCollection.Add((DeliverypointgroupLanguageEntity)entity);
					}
					break;
				case "EntertainmentcategoryLanguageCollection":
					_alreadyFetchedEntertainmentcategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryLanguageCollection.Add((EntertainmentcategoryLanguageEntity)entity);
					}
					break;
				case "GenericcategoryLanguageCollection":
					_alreadyFetchedGenericcategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.GenericcategoryLanguageCollection.Add((GenericcategoryLanguageEntity)entity);
					}
					break;
				case "GenericproductLanguageCollection":
					_alreadyFetchedGenericproductLanguageCollection = true;
					if(entity!=null)
					{
						this.GenericproductLanguageCollection.Add((GenericproductLanguageEntity)entity);
					}
					break;
				case "MediaLanguageCollection":
					_alreadyFetchedMediaLanguageCollection = true;
					if(entity!=null)
					{
						this.MediaLanguageCollection.Add((MediaLanguageEntity)entity);
					}
					break;
				case "PageElementCollection":
					_alreadyFetchedPageElementCollection = true;
					if(entity!=null)
					{
						this.PageElementCollection.Add((PageElementEntity)entity);
					}
					break;
				case "PageLanguageCollection":
					_alreadyFetchedPageLanguageCollection = true;
					if(entity!=null)
					{
						this.PageLanguageCollection.Add((PageLanguageEntity)entity);
					}
					break;
				case "PageTemplateElementCollection":
					_alreadyFetchedPageTemplateElementCollection = true;
					if(entity!=null)
					{
						this.PageTemplateElementCollection.Add((PageTemplateElementEntity)entity);
					}
					break;
				case "PageTemplateLanguageCollection":
					_alreadyFetchedPageTemplateLanguageCollection = true;
					if(entity!=null)
					{
						this.PageTemplateLanguageCollection.Add((PageTemplateLanguageEntity)entity);
					}
					break;
				case "PointOfInterestLanguageCollection":
					_alreadyFetchedPointOfInterestLanguageCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestLanguageCollection.Add((PointOfInterestLanguageEntity)entity);
					}
					break;
				case "ProductLanguageCollection":
					_alreadyFetchedProductLanguageCollection = true;
					if(entity!=null)
					{
						this.ProductLanguageCollection.Add((ProductLanguageEntity)entity);
					}
					break;
				case "RoomControlAreaLanguageCollection":
					_alreadyFetchedRoomControlAreaLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlAreaLanguageCollection.Add((RoomControlAreaLanguageEntity)entity);
					}
					break;
				case "RoomControlComponentLanguageCollection":
					_alreadyFetchedRoomControlComponentLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlComponentLanguageCollection.Add((RoomControlComponentLanguageEntity)entity);
					}
					break;
				case "RoomControlSectionItemLanguageCollection":
					_alreadyFetchedRoomControlSectionItemLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlSectionItemLanguageCollection.Add((RoomControlSectionItemLanguageEntity)entity);
					}
					break;
				case "RoomControlSectionLanguageCollection":
					_alreadyFetchedRoomControlSectionLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlSectionLanguageCollection.Add((RoomControlSectionLanguageEntity)entity);
					}
					break;
				case "RoomControlWidgetLanguageCollection":
					_alreadyFetchedRoomControlWidgetLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlWidgetLanguageCollection.Add((RoomControlWidgetLanguageEntity)entity);
					}
					break;
				case "ScheduledMessageLanguageCollection":
					_alreadyFetchedScheduledMessageLanguageCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageLanguageCollection.Add((ScheduledMessageLanguageEntity)entity);
					}
					break;
				case "SiteLanguageCollection":
					_alreadyFetchedSiteLanguageCollection = true;
					if(entity!=null)
					{
						this.SiteLanguageCollection.Add((SiteLanguageEntity)entity);
					}
					break;
				case "SiteTemplateLanguageCollection":
					_alreadyFetchedSiteTemplateLanguageCollection = true;
					if(entity!=null)
					{
						this.SiteTemplateLanguageCollection.Add((SiteTemplateLanguageEntity)entity);
					}
					break;
				case "StationLanguageCollection":
					_alreadyFetchedStationLanguageCollection = true;
					if(entity!=null)
					{
						this.StationLanguageCollection.Add((StationLanguageEntity)entity);
					}
					break;
				case "SurveyAnswerLanguageCollection":
					_alreadyFetchedSurveyAnswerLanguageCollection = true;
					if(entity!=null)
					{
						this.SurveyAnswerLanguageCollection.Add((SurveyAnswerLanguageEntity)entity);
					}
					break;
				case "SurveyLanguageCollection":
					_alreadyFetchedSurveyLanguageCollection = true;
					if(entity!=null)
					{
						this.SurveyLanguageCollection.Add((SurveyLanguageEntity)entity);
					}
					break;
				case "SurveyQuestionLanguageCollection":
					_alreadyFetchedSurveyQuestionLanguageCollection = true;
					if(entity!=null)
					{
						this.SurveyQuestionLanguageCollection.Add((SurveyQuestionLanguageEntity)entity);
					}
					break;
				case "UIFooterItemLanguageCollection":
					_alreadyFetchedUIFooterItemLanguageCollection = true;
					if(entity!=null)
					{
						this.UIFooterItemLanguageCollection.Add((UIFooterItemLanguageEntity)entity);
					}
					break;
				case "UITabLanguageCollection":
					_alreadyFetchedUITabLanguageCollection = true;
					if(entity!=null)
					{
						this.UITabLanguageCollection.Add((UITabLanguageEntity)entity);
					}
					break;
				case "UIWidgetLanguageCollection":
					_alreadyFetchedUIWidgetLanguageCollection = true;
					if(entity!=null)
					{
						this.UIWidgetLanguageCollection.Add((UIWidgetLanguageEntity)entity);
					}
					break;
				case "VenueCategoryLanguageCollection":
					_alreadyFetchedVenueCategoryLanguageCollection = true;
					if(entity!=null)
					{
						this.VenueCategoryLanguageCollection.Add((VenueCategoryLanguageEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage":
					_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "RouteCollectionViaCompany":
					_alreadyFetchedRouteCollectionViaCompany = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCompany.Add((RouteEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaCompany":
					_alreadyFetchedSupportpoolCollectionViaCompany = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaCompany.Add((SupportpoolEntity)entity);
					}
					break;
				case "UITabCollectionViaUITabLanguage":
					_alreadyFetchedUITabCollectionViaUITabLanguage = true;
					if(entity!=null)
					{
						this.UITabCollectionViaUITabLanguage.Add((UITabEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ActionButtonLanguageCollection":
					_actionButtonLanguageCollection.Add((ActionButtonLanguageEntity)relatedEntity);
					break;
				case "AdvertisementLanguageCollection":
					_advertisementLanguageCollection.Add((AdvertisementLanguageEntity)relatedEntity);
					break;
				case "AdvertisementTagLanguageCollection":
					_advertisementTagLanguageCollection.Add((AdvertisementTagLanguageEntity)relatedEntity);
					break;
				case "AlterationLanguageCollection":
					_alterationLanguageCollection.Add((AlterationLanguageEntity)relatedEntity);
					break;
				case "AlterationoptionLanguageCollection":
					_alterationoptionLanguageCollection.Add((AlterationoptionLanguageEntity)relatedEntity);
					break;
				case "AmenityLanguageCollection":
					_amenityLanguageCollection.Add((AmenityLanguageEntity)relatedEntity);
					break;
				case "AnnouncementLanguageCollection":
					_announcementLanguageCollection.Add((AnnouncementLanguageEntity)relatedEntity);
					break;
				case "AttachmentLanguageCollection":
					_attachmentLanguageCollection.Add((AttachmentLanguageEntity)relatedEntity);
					break;
				case "CategoryLanguageCollection":
					_categoryLanguageCollection.Add((CategoryLanguageEntity)relatedEntity);
					break;
				case "CompanyCollection":
					_companyCollection.Add((CompanyEntity)relatedEntity);
					break;
				case "CompanyLanguageCollection":
					_companyLanguageCollection.Add((CompanyLanguageEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "DeliverypointgroupLanguageCollection":
					_deliverypointgroupLanguageCollection.Add((DeliverypointgroupLanguageEntity)relatedEntity);
					break;
				case "EntertainmentcategoryLanguageCollection":
					_entertainmentcategoryLanguageCollection.Add((EntertainmentcategoryLanguageEntity)relatedEntity);
					break;
				case "GenericcategoryLanguageCollection":
					_genericcategoryLanguageCollection.Add((GenericcategoryLanguageEntity)relatedEntity);
					break;
				case "GenericproductLanguageCollection":
					_genericproductLanguageCollection.Add((GenericproductLanguageEntity)relatedEntity);
					break;
				case "MediaLanguageCollection":
					_mediaLanguageCollection.Add((MediaLanguageEntity)relatedEntity);
					break;
				case "PageElementCollection":
					_pageElementCollection.Add((PageElementEntity)relatedEntity);
					break;
				case "PageLanguageCollection":
					_pageLanguageCollection.Add((PageLanguageEntity)relatedEntity);
					break;
				case "PageTemplateElementCollection":
					_pageTemplateElementCollection.Add((PageTemplateElementEntity)relatedEntity);
					break;
				case "PageTemplateLanguageCollection":
					_pageTemplateLanguageCollection.Add((PageTemplateLanguageEntity)relatedEntity);
					break;
				case "PointOfInterestLanguageCollection":
					_pointOfInterestLanguageCollection.Add((PointOfInterestLanguageEntity)relatedEntity);
					break;
				case "ProductLanguageCollection":
					_productLanguageCollection.Add((ProductLanguageEntity)relatedEntity);
					break;
				case "RoomControlAreaLanguageCollection":
					_roomControlAreaLanguageCollection.Add((RoomControlAreaLanguageEntity)relatedEntity);
					break;
				case "RoomControlComponentLanguageCollection":
					_roomControlComponentLanguageCollection.Add((RoomControlComponentLanguageEntity)relatedEntity);
					break;
				case "RoomControlSectionItemLanguageCollection":
					_roomControlSectionItemLanguageCollection.Add((RoomControlSectionItemLanguageEntity)relatedEntity);
					break;
				case "RoomControlSectionLanguageCollection":
					_roomControlSectionLanguageCollection.Add((RoomControlSectionLanguageEntity)relatedEntity);
					break;
				case "RoomControlWidgetLanguageCollection":
					_roomControlWidgetLanguageCollection.Add((RoomControlWidgetLanguageEntity)relatedEntity);
					break;
				case "ScheduledMessageLanguageCollection":
					_scheduledMessageLanguageCollection.Add((ScheduledMessageLanguageEntity)relatedEntity);
					break;
				case "SiteLanguageCollection":
					_siteLanguageCollection.Add((SiteLanguageEntity)relatedEntity);
					break;
				case "SiteTemplateLanguageCollection":
					_siteTemplateLanguageCollection.Add((SiteTemplateLanguageEntity)relatedEntity);
					break;
				case "StationLanguageCollection":
					_stationLanguageCollection.Add((StationLanguageEntity)relatedEntity);
					break;
				case "SurveyAnswerLanguageCollection":
					_surveyAnswerLanguageCollection.Add((SurveyAnswerLanguageEntity)relatedEntity);
					break;
				case "SurveyLanguageCollection":
					_surveyLanguageCollection.Add((SurveyLanguageEntity)relatedEntity);
					break;
				case "SurveyQuestionLanguageCollection":
					_surveyQuestionLanguageCollection.Add((SurveyQuestionLanguageEntity)relatedEntity);
					break;
				case "UIFooterItemLanguageCollection":
					_uIFooterItemLanguageCollection.Add((UIFooterItemLanguageEntity)relatedEntity);
					break;
				case "UITabLanguageCollection":
					_uITabLanguageCollection.Add((UITabLanguageEntity)relatedEntity);
					break;
				case "UIWidgetLanguageCollection":
					_uIWidgetLanguageCollection.Add((UIWidgetLanguageEntity)relatedEntity);
					break;
				case "VenueCategoryLanguageCollection":
					_venueCategoryLanguageCollection.Add((VenueCategoryLanguageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ActionButtonLanguageCollection":
					this.PerformRelatedEntityRemoval(_actionButtonLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementLanguageCollection":
					this.PerformRelatedEntityRemoval(_advertisementLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AdvertisementTagLanguageCollection":
					this.PerformRelatedEntityRemoval(_advertisementTagLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationLanguageCollection":
					this.PerformRelatedEntityRemoval(_alterationLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationoptionLanguageCollection":
					this.PerformRelatedEntityRemoval(_alterationoptionLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AmenityLanguageCollection":
					this.PerformRelatedEntityRemoval(_amenityLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AnnouncementLanguageCollection":
					this.PerformRelatedEntityRemoval(_announcementLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttachmentLanguageCollection":
					this.PerformRelatedEntityRemoval(_attachmentLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_categoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CompanyCollection":
					this.PerformRelatedEntityRemoval(_companyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CompanyLanguageCollection":
					this.PerformRelatedEntityRemoval(_companyLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupLanguageCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EntertainmentcategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_entertainmentcategoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericcategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_genericcategoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericproductLanguageCollection":
					this.PerformRelatedEntityRemoval(_genericproductLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaLanguageCollection":
					this.PerformRelatedEntityRemoval(_mediaLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageElementCollection":
					this.PerformRelatedEntityRemoval(_pageElementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageLanguageCollection":
					this.PerformRelatedEntityRemoval(_pageLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageTemplateElementCollection":
					this.PerformRelatedEntityRemoval(_pageTemplateElementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageTemplateLanguageCollection":
					this.PerformRelatedEntityRemoval(_pageTemplateLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestLanguageCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductLanguageCollection":
					this.PerformRelatedEntityRemoval(_productLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlAreaLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlAreaLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlComponentLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlComponentLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlSectionItemLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlSectionItemLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlSectionLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlSectionLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlWidgetLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageLanguageCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SiteLanguageCollection":
					this.PerformRelatedEntityRemoval(_siteLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SiteTemplateLanguageCollection":
					this.PerformRelatedEntityRemoval(_siteTemplateLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "StationLanguageCollection":
					this.PerformRelatedEntityRemoval(_stationLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyAnswerLanguageCollection":
					this.PerformRelatedEntityRemoval(_surveyAnswerLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyLanguageCollection":
					this.PerformRelatedEntityRemoval(_surveyLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyQuestionLanguageCollection":
					this.PerformRelatedEntityRemoval(_surveyQuestionLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIFooterItemLanguageCollection":
					this.PerformRelatedEntityRemoval(_uIFooterItemLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UITabLanguageCollection":
					this.PerformRelatedEntityRemoval(_uITabLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetLanguageCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VenueCategoryLanguageCollection":
					this.PerformRelatedEntityRemoval(_venueCategoryLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_actionButtonLanguageCollection);
			toReturn.Add(_advertisementLanguageCollection);
			toReturn.Add(_advertisementTagLanguageCollection);
			toReturn.Add(_alterationLanguageCollection);
			toReturn.Add(_alterationoptionLanguageCollection);
			toReturn.Add(_amenityLanguageCollection);
			toReturn.Add(_announcementLanguageCollection);
			toReturn.Add(_attachmentLanguageCollection);
			toReturn.Add(_categoryLanguageCollection);
			toReturn.Add(_companyCollection);
			toReturn.Add(_companyLanguageCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_deliverypointgroupLanguageCollection);
			toReturn.Add(_entertainmentcategoryLanguageCollection);
			toReturn.Add(_genericcategoryLanguageCollection);
			toReturn.Add(_genericproductLanguageCollection);
			toReturn.Add(_mediaLanguageCollection);
			toReturn.Add(_pageElementCollection);
			toReturn.Add(_pageLanguageCollection);
			toReturn.Add(_pageTemplateElementCollection);
			toReturn.Add(_pageTemplateLanguageCollection);
			toReturn.Add(_pointOfInterestLanguageCollection);
			toReturn.Add(_productLanguageCollection);
			toReturn.Add(_roomControlAreaLanguageCollection);
			toReturn.Add(_roomControlComponentLanguageCollection);
			toReturn.Add(_roomControlSectionItemLanguageCollection);
			toReturn.Add(_roomControlSectionLanguageCollection);
			toReturn.Add(_roomControlWidgetLanguageCollection);
			toReturn.Add(_scheduledMessageLanguageCollection);
			toReturn.Add(_siteLanguageCollection);
			toReturn.Add(_siteTemplateLanguageCollection);
			toReturn.Add(_stationLanguageCollection);
			toReturn.Add(_surveyAnswerLanguageCollection);
			toReturn.Add(_surveyLanguageCollection);
			toReturn.Add(_surveyQuestionLanguageCollection);
			toReturn.Add(_uIFooterItemLanguageCollection);
			toReturn.Add(_uITabLanguageCollection);
			toReturn.Add(_uIWidgetLanguageCollection);
			toReturn.Add(_venueCategoryLanguageCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 languageId)
		{
			return FetchUsingPK(languageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 languageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(languageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 languageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(languageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 languageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(languageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LanguageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new LanguageRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ActionButtonLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ActionButtonLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection GetMultiActionButtonLanguageCollection(bool forceFetch)
		{
			return GetMultiActionButtonLanguageCollection(forceFetch, _actionButtonLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionButtonLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ActionButtonLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection GetMultiActionButtonLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiActionButtonLanguageCollection(forceFetch, _actionButtonLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ActionButtonLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection GetMultiActionButtonLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiActionButtonLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionButtonLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection GetMultiActionButtonLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedActionButtonLanguageCollection || forceFetch || _alwaysFetchActionButtonLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_actionButtonLanguageCollection);
				_actionButtonLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_actionButtonLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_actionButtonLanguageCollection.GetMultiManyToOne(null, this, filter);
				_actionButtonLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedActionButtonLanguageCollection = true;
			}
			return _actionButtonLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ActionButtonLanguageCollection'. These settings will be taken into account
		/// when the property ActionButtonLanguageCollection is requested or GetMultiActionButtonLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersActionButtonLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_actionButtonLanguageCollection.SortClauses=sortClauses;
			_actionButtonLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection GetMultiAdvertisementLanguageCollection(bool forceFetch)
		{
			return GetMultiAdvertisementLanguageCollection(forceFetch, _advertisementLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection GetMultiAdvertisementLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementLanguageCollection(forceFetch, _advertisementLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection GetMultiAdvertisementLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection GetMultiAdvertisementLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementLanguageCollection || forceFetch || _alwaysFetchAdvertisementLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementLanguageCollection);
				_advertisementLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementLanguageCollection.GetMultiManyToOne(null, this, filter);
				_advertisementLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementLanguageCollection = true;
			}
			return _advertisementLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementLanguageCollection'. These settings will be taken into account
		/// when the property AdvertisementLanguageCollection is requested or GetMultiAdvertisementLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementLanguageCollection.SortClauses=sortClauses;
			_advertisementLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch)
		{
			return GetMultiAdvertisementTagLanguageCollection(forceFetch, _advertisementTagLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementTagLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementTagLanguageCollection(forceFetch, _advertisementTagLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementTagLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection GetMultiAdvertisementTagLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementTagLanguageCollection || forceFetch || _alwaysFetchAdvertisementTagLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementTagLanguageCollection);
				_advertisementTagLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementTagLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementTagLanguageCollection.GetMultiManyToOne(null, this, filter);
				_advertisementTagLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementTagLanguageCollection = true;
			}
			return _advertisementTagLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementTagLanguageCollection'. These settings will be taken into account
		/// when the property AdvertisementTagLanguageCollection is requested or GetMultiAdvertisementTagLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementTagLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementTagLanguageCollection.SortClauses=sortClauses;
			_advertisementTagLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch)
		{
			return GetMultiAlterationLanguageCollection(forceFetch, _alterationLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationLanguageCollection(forceFetch, _alterationLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationLanguageCollection || forceFetch || _alwaysFetchAlterationLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationLanguageCollection);
				_alterationLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationLanguageCollection.GetMultiManyToOne(null, this, filter);
				_alterationLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationLanguageCollection = true;
			}
			return _alterationLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationLanguageCollection'. These settings will be taken into account
		/// when the property AlterationLanguageCollection is requested or GetMultiAlterationLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationLanguageCollection.SortClauses=sortClauses;
			_alterationLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch)
		{
			return GetMultiAlterationoptionLanguageCollection(forceFetch, _alterationoptionLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationoptionLanguageCollection(forceFetch, _alterationoptionLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationoptionLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection GetMultiAlterationoptionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationoptionLanguageCollection || forceFetch || _alwaysFetchAlterationoptionLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionLanguageCollection);
				_alterationoptionLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionLanguageCollection.GetMultiManyToOne(null, this, filter);
				_alterationoptionLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionLanguageCollection = true;
			}
			return _alterationoptionLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionLanguageCollection'. These settings will be taken into account
		/// when the property AlterationoptionLanguageCollection is requested or GetMultiAlterationoptionLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionLanguageCollection.SortClauses=sortClauses;
			_alterationoptionLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AmenityLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch)
		{
			return GetMultiAmenityLanguageCollection(forceFetch, _amenityLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AmenityLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAmenityLanguageCollection(forceFetch, _amenityLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAmenityLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAmenityLanguageCollection || forceFetch || _alwaysFetchAmenityLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_amenityLanguageCollection);
				_amenityLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_amenityLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_amenityLanguageCollection.GetMultiManyToOne(null, this, filter);
				_amenityLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAmenityLanguageCollection = true;
			}
			return _amenityLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AmenityLanguageCollection'. These settings will be taken into account
		/// when the property AmenityLanguageCollection is requested or GetMultiAmenityLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAmenityLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_amenityLanguageCollection.SortClauses=sortClauses;
			_amenityLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch)
		{
			return GetMultiAnnouncementLanguageCollection(forceFetch, _announcementLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAnnouncementLanguageCollection(forceFetch, _announcementLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAnnouncementLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection GetMultiAnnouncementLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAnnouncementLanguageCollection || forceFetch || _alwaysFetchAnnouncementLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementLanguageCollection);
				_announcementLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_announcementLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_announcementLanguageCollection.GetMultiManyToOne(null, this, filter);
				_announcementLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementLanguageCollection = true;
			}
			return _announcementLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementLanguageCollection'. These settings will be taken into account
		/// when the property AnnouncementLanguageCollection is requested or GetMultiAnnouncementLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementLanguageCollection.SortClauses=sortClauses;
			_announcementLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttachmentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentLanguageCollection GetMultiAttachmentLanguageCollection(bool forceFetch)
		{
			return GetMultiAttachmentLanguageCollection(forceFetch, _attachmentLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttachmentLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AttachmentLanguageCollection GetMultiAttachmentLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttachmentLanguageCollection(forceFetch, _attachmentLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AttachmentLanguageCollection GetMultiAttachmentLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttachmentLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttachmentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AttachmentLanguageCollection GetMultiAttachmentLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttachmentLanguageCollection || forceFetch || _alwaysFetchAttachmentLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attachmentLanguageCollection);
				_attachmentLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_attachmentLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_attachmentLanguageCollection.GetMultiManyToOne(null, this, filter);
				_attachmentLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAttachmentLanguageCollection = true;
			}
			return _attachmentLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttachmentLanguageCollection'. These settings will be taken into account
		/// when the property AttachmentLanguageCollection is requested or GetMultiAttachmentLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttachmentLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attachmentLanguageCollection.SortClauses=sortClauses;
			_attachmentLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiCategoryLanguageCollection(forceFetch, _categoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryLanguageCollection(forceFetch, _categoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryLanguageCollection GetMultiCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryLanguageCollection || forceFetch || _alwaysFetchCategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryLanguageCollection);
				_categoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryLanguageCollection.GetMultiManyToOne(null, this, filter);
				_categoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryLanguageCollection = true;
			}
			return _categoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryLanguageCollection'. These settings will be taken into account
		/// when the property CategoryLanguageCollection is requested or GetMultiCategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryLanguageCollection.SortClauses=sortClauses;
			_categoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyCollection || forceFetch || _alwaysFetchCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollection);
				_companyCollection.SuppressClearInGetMulti=!forceFetch;
				_companyCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, filter);
				_companyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollection = true;
			}
			return _companyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollection'. These settings will be taken into account
		/// when the property CompanyCollection is requested or GetMultiCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollection.SortClauses=sortClauses;
			_companyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyLanguageCollection GetMultiCompanyLanguageCollection(bool forceFetch)
		{
			return GetMultiCompanyLanguageCollection(forceFetch, _companyLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyLanguageCollection GetMultiCompanyLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyLanguageCollection(forceFetch, _companyLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyLanguageCollection GetMultiCompanyLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyLanguageCollection GetMultiCompanyLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyLanguageCollection || forceFetch || _alwaysFetchCompanyLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyLanguageCollection);
				_companyLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_companyLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyLanguageCollection.GetMultiManyToOne(null, this, filter);
				_companyLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyLanguageCollection = true;
			}
			return _companyLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyLanguageCollection'. These settings will be taken into account
		/// when the property CompanyLanguageCollection is requested or GetMultiCompanyLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyLanguageCollection.SortClauses=sortClauses;
			_companyLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupLanguageCollection(forceFetch, _deliverypointgroupLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupLanguageCollection(forceFetch, _deliverypointgroupLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection GetMultiDeliverypointgroupLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupLanguageCollection || forceFetch || _alwaysFetchDeliverypointgroupLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupLanguageCollection);
				_deliverypointgroupLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupLanguageCollection.GetMultiManyToOne(null, this, filter);
				_deliverypointgroupLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupLanguageCollection = true;
			}
			return _deliverypointgroupLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupLanguageCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupLanguageCollection is requested or GetMultiDeliverypointgroupLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupLanguageCollection.SortClauses=sortClauses;
			_deliverypointgroupLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryLanguageCollection(forceFetch, _entertainmentcategoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntertainmentcategoryLanguageCollection(forceFetch, _entertainmentcategoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntertainmentcategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection GetMultiEntertainmentcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryLanguageCollection || forceFetch || _alwaysFetchEntertainmentcategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryLanguageCollection);
				_entertainmentcategoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryLanguageCollection.GetMultiManyToOne(null, this, filter);
				_entertainmentcategoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryLanguageCollection = true;
			}
			return _entertainmentcategoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryLanguageCollection'. These settings will be taken into account
		/// when the property EntertainmentcategoryLanguageCollection is requested or GetMultiEntertainmentcategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryLanguageCollection.SortClauses=sortClauses;
			_entertainmentcategoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiGenericcategoryLanguageCollection(forceFetch, _genericcategoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericcategoryLanguageCollection(forceFetch, _genericcategoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericcategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GetMultiGenericcategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericcategoryLanguageCollection || forceFetch || _alwaysFetchGenericcategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryLanguageCollection);
				_genericcategoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryLanguageCollection.GetMultiManyToOne(null, this, filter);
				_genericcategoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryLanguageCollection = true;
			}
			return _genericcategoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryLanguageCollection'. These settings will be taken into account
		/// when the property GenericcategoryLanguageCollection is requested or GetMultiGenericcategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryLanguageCollection.SortClauses=sortClauses;
			_genericcategoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch)
		{
			return GetMultiGenericproductLanguageCollection(forceFetch, _genericproductLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductLanguageCollection(forceFetch, _genericproductLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GetMultiGenericproductLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductLanguageCollection || forceFetch || _alwaysFetchGenericproductLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductLanguageCollection);
				_genericproductLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductLanguageCollection.GetMultiManyToOne(null, this, filter);
				_genericproductLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductLanguageCollection = true;
			}
			return _genericproductLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductLanguageCollection'. These settings will be taken into account
		/// when the property GenericproductLanguageCollection is requested or GetMultiGenericproductLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductLanguageCollection.SortClauses=sortClauses;
			_genericproductLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch)
		{
			return GetMultiMediaLanguageCollection(forceFetch, _mediaLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaLanguageCollection(forceFetch, _mediaLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaLanguageCollection GetMultiMediaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaLanguageCollection || forceFetch || _alwaysFetchMediaLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaLanguageCollection);
				_mediaLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaLanguageCollection.GetMultiManyToOne(this, null, filter);
				_mediaLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaLanguageCollection = true;
			}
			return _mediaLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaLanguageCollection'. These settings will be taken into account
		/// when the property MediaLanguageCollection is requested or GetMultiMediaLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaLanguageCollection.SortClauses=sortClauses;
			_mediaLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch)
		{
			return GetMultiPageElementCollection(forceFetch, _pageElementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageElementCollection(forceFetch, _pageElementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageElementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageElementCollection GetMultiPageElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageElementCollection || forceFetch || _alwaysFetchPageElementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageElementCollection);
				_pageElementCollection.SuppressClearInGetMulti=!forceFetch;
				_pageElementCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageElementCollection.GetMultiManyToOne(this, null, filter);
				_pageElementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageElementCollection = true;
			}
			return _pageElementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageElementCollection'. These settings will be taken into account
		/// when the property PageElementCollection is requested or GetMultiPageElementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageElementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageElementCollection.SortClauses=sortClauses;
			_pageElementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch)
		{
			return GetMultiPageLanguageCollection(forceFetch, _pageLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageLanguageCollection(forceFetch, _pageLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageLanguageCollection GetMultiPageLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageLanguageCollection || forceFetch || _alwaysFetchPageLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageLanguageCollection);
				_pageLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_pageLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageLanguageCollection.GetMultiManyToOne(this, null, filter);
				_pageLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageLanguageCollection = true;
			}
			return _pageLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageLanguageCollection'. These settings will be taken into account
		/// when the property PageLanguageCollection is requested or GetMultiPageLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageLanguageCollection.SortClauses=sortClauses;
			_pageLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch)
		{
			return GetMultiPageTemplateElementCollection(forceFetch, _pageTemplateElementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateElementEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageTemplateElementCollection(forceFetch, _pageTemplateElementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageTemplateElementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateElementCollection GetMultiPageTemplateElementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageTemplateElementCollection || forceFetch || _alwaysFetchPageTemplateElementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageTemplateElementCollection);
				_pageTemplateElementCollection.SuppressClearInGetMulti=!forceFetch;
				_pageTemplateElementCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageTemplateElementCollection.GetMultiManyToOne(this, null, filter);
				_pageTemplateElementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageTemplateElementCollection = true;
			}
			return _pageTemplateElementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageTemplateElementCollection'. These settings will be taken into account
		/// when the property PageTemplateElementCollection is requested or GetMultiPageTemplateElementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageTemplateElementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageTemplateElementCollection.SortClauses=sortClauses;
			_pageTemplateElementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch)
		{
			return GetMultiPageTemplateLanguageCollection(forceFetch, _pageTemplateLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageTemplateLanguageCollection(forceFetch, _pageTemplateLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageTemplateLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection GetMultiPageTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageTemplateLanguageCollection || forceFetch || _alwaysFetchPageTemplateLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageTemplateLanguageCollection);
				_pageTemplateLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_pageTemplateLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pageTemplateLanguageCollection.GetMultiManyToOne(this, null, filter);
				_pageTemplateLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPageTemplateLanguageCollection = true;
			}
			return _pageTemplateLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageTemplateLanguageCollection'. These settings will be taken into account
		/// when the property PageTemplateLanguageCollection is requested or GetMultiPageTemplateLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageTemplateLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageTemplateLanguageCollection.SortClauses=sortClauses;
			_pageTemplateLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestLanguageCollection(forceFetch, _pointOfInterestLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestLanguageCollection(forceFetch, _pointOfInterestLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestLanguageCollection || forceFetch || _alwaysFetchPointOfInterestLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestLanguageCollection);
				_pointOfInterestLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestLanguageCollection.GetMultiManyToOne(this, null, filter);
				_pointOfInterestLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestLanguageCollection = true;
			}
			return _pointOfInterestLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestLanguageCollection'. These settings will be taken into account
		/// when the property PointOfInterestLanguageCollection is requested or GetMultiPointOfInterestLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestLanguageCollection.SortClauses=sortClauses;
			_pointOfInterestLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductLanguageCollection GetMultiProductLanguageCollection(bool forceFetch)
		{
			return GetMultiProductLanguageCollection(forceFetch, _productLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductLanguageCollection GetMultiProductLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductLanguageCollection(forceFetch, _productLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductLanguageCollection GetMultiProductLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductLanguageCollection GetMultiProductLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductLanguageCollection || forceFetch || _alwaysFetchProductLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productLanguageCollection);
				_productLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_productLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_productLanguageCollection.GetMultiManyToOne(this, null, filter);
				_productLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductLanguageCollection = true;
			}
			return _productLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductLanguageCollection'. These settings will be taken into account
		/// when the property ProductLanguageCollection is requested or GetMultiProductLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productLanguageCollection.SortClauses=sortClauses;
			_productLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlAreaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlAreaLanguageCollection(forceFetch, _roomControlAreaLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlAreaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlAreaLanguageCollection(forceFetch, _roomControlAreaLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlAreaLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlAreaLanguageCollection || forceFetch || _alwaysFetchRoomControlAreaLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlAreaLanguageCollection);
				_roomControlAreaLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlAreaLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlAreaLanguageCollection.GetMultiManyToOne(this, null, filter);
				_roomControlAreaLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlAreaLanguageCollection = true;
			}
			return _roomControlAreaLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlAreaLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlAreaLanguageCollection is requested or GetMultiRoomControlAreaLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlAreaLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlAreaLanguageCollection.SortClauses=sortClauses;
			_roomControlAreaLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlComponentLanguageCollection(forceFetch, _roomControlComponentLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlComponentLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlComponentLanguageCollection(forceFetch, _roomControlComponentLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlComponentLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection GetMultiRoomControlComponentLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlComponentLanguageCollection || forceFetch || _alwaysFetchRoomControlComponentLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlComponentLanguageCollection);
				_roomControlComponentLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlComponentLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlComponentLanguageCollection.GetMultiManyToOne(this, null, filter);
				_roomControlComponentLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlComponentLanguageCollection = true;
			}
			return _roomControlComponentLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlComponentLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlComponentLanguageCollection is requested or GetMultiRoomControlComponentLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlComponentLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlComponentLanguageCollection.SortClauses=sortClauses;
			_roomControlComponentLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlSectionItemLanguageCollection(forceFetch, _roomControlSectionItemLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionItemLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlSectionItemLanguageCollection(forceFetch, _roomControlSectionItemLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlSectionItemLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection GetMultiRoomControlSectionItemLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlSectionItemLanguageCollection || forceFetch || _alwaysFetchRoomControlSectionItemLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlSectionItemLanguageCollection);
				_roomControlSectionItemLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlSectionItemLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlSectionItemLanguageCollection.GetMultiManyToOne(this, null, filter);
				_roomControlSectionItemLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlSectionItemLanguageCollection = true;
			}
			return _roomControlSectionItemLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlSectionItemLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlSectionItemLanguageCollection is requested or GetMultiRoomControlSectionItemLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlSectionItemLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlSectionItemLanguageCollection.SortClauses=sortClauses;
			_roomControlSectionItemLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlSectionLanguageCollection(forceFetch, _roomControlSectionLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlSectionLanguageCollection(forceFetch, _roomControlSectionLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlSectionLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection GetMultiRoomControlSectionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlSectionLanguageCollection || forceFetch || _alwaysFetchRoomControlSectionLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlSectionLanguageCollection);
				_roomControlSectionLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlSectionLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlSectionLanguageCollection.GetMultiManyToOne(this, null, filter);
				_roomControlSectionLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlSectionLanguageCollection = true;
			}
			return _roomControlSectionLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlSectionLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlSectionLanguageCollection is requested or GetMultiRoomControlSectionLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlSectionLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlSectionLanguageCollection.SortClauses=sortClauses;
			_roomControlSectionLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlWidgetLanguageCollection(forceFetch, _roomControlWidgetLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetLanguageCollection(forceFetch, _roomControlWidgetLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetLanguageCollection || forceFetch || _alwaysFetchRoomControlWidgetLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetLanguageCollection);
				_roomControlWidgetLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetLanguageCollection.GetMultiManyToOne(this, null, filter);
				_roomControlWidgetLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetLanguageCollection = true;
			}
			return _roomControlWidgetLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlWidgetLanguageCollection is requested or GetMultiRoomControlWidgetLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetLanguageCollection.SortClauses=sortClauses;
			_roomControlWidgetLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection GetMultiScheduledMessageLanguageCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageLanguageCollection(forceFetch, _scheduledMessageLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection GetMultiScheduledMessageLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageLanguageCollection(forceFetch, _scheduledMessageLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection GetMultiScheduledMessageLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection GetMultiScheduledMessageLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageLanguageCollection || forceFetch || _alwaysFetchScheduledMessageLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageLanguageCollection);
				_scheduledMessageLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageLanguageCollection.GetMultiManyToOne(this, null, filter);
				_scheduledMessageLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageLanguageCollection = true;
			}
			return _scheduledMessageLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageLanguageCollection'. These settings will be taken into account
		/// when the property ScheduledMessageLanguageCollection is requested or GetMultiScheduledMessageLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageLanguageCollection.SortClauses=sortClauses;
			_scheduledMessageLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SiteLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch)
		{
			return GetMultiSiteLanguageCollection(forceFetch, _siteLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SiteLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSiteLanguageCollection(forceFetch, _siteLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSiteLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SiteLanguageCollection GetMultiSiteLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSiteLanguageCollection || forceFetch || _alwaysFetchSiteLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_siteLanguageCollection);
				_siteLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_siteLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_siteLanguageCollection.GetMultiManyToOne(this, null, filter);
				_siteLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSiteLanguageCollection = true;
			}
			return _siteLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SiteLanguageCollection'. These settings will be taken into account
		/// when the property SiteLanguageCollection is requested or GetMultiSiteLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSiteLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_siteLanguageCollection.SortClauses=sortClauses;
			_siteLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SiteTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch)
		{
			return GetMultiSiteTemplateLanguageCollection(forceFetch, _siteTemplateLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SiteTemplateLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSiteTemplateLanguageCollection(forceFetch, _siteTemplateLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSiteTemplateLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection GetMultiSiteTemplateLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSiteTemplateLanguageCollection || forceFetch || _alwaysFetchSiteTemplateLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_siteTemplateLanguageCollection);
				_siteTemplateLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_siteTemplateLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_siteTemplateLanguageCollection.GetMultiManyToOne(this, null, filter);
				_siteTemplateLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSiteTemplateLanguageCollection = true;
			}
			return _siteTemplateLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SiteTemplateLanguageCollection'. These settings will be taken into account
		/// when the property SiteTemplateLanguageCollection is requested or GetMultiSiteTemplateLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSiteTemplateLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_siteTemplateLanguageCollection.SortClauses=sortClauses;
			_siteTemplateLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StationLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.StationLanguageCollection GetMultiStationLanguageCollection(bool forceFetch)
		{
			return GetMultiStationLanguageCollection(forceFetch, _stationLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StationLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.StationLanguageCollection GetMultiStationLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiStationLanguageCollection(forceFetch, _stationLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.StationLanguageCollection GetMultiStationLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiStationLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.StationLanguageCollection GetMultiStationLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedStationLanguageCollection || forceFetch || _alwaysFetchStationLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_stationLanguageCollection);
				_stationLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_stationLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_stationLanguageCollection.GetMultiManyToOne(this, null, filter);
				_stationLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedStationLanguageCollection = true;
			}
			return _stationLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'StationLanguageCollection'. These settings will be taken into account
		/// when the property StationLanguageCollection is requested or GetMultiStationLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersStationLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_stationLanguageCollection.SortClauses=sortClauses;
			_stationLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch)
		{
			return GetMultiSurveyAnswerLanguageCollection(forceFetch, _surveyAnswerLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyAnswerLanguageCollection(forceFetch, _surveyAnswerLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyAnswerLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection GetMultiSurveyAnswerLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyAnswerLanguageCollection || forceFetch || _alwaysFetchSurveyAnswerLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyAnswerLanguageCollection);
				_surveyAnswerLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyAnswerLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyAnswerLanguageCollection.GetMultiManyToOne(this, null, filter);
				_surveyAnswerLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyAnswerLanguageCollection = true;
			}
			return _surveyAnswerLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyAnswerLanguageCollection'. These settings will be taken into account
		/// when the property SurveyAnswerLanguageCollection is requested or GetMultiSurveyAnswerLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyAnswerLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyAnswerLanguageCollection.SortClauses=sortClauses;
			_surveyAnswerLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyLanguageCollection GetMultiSurveyLanguageCollection(bool forceFetch)
		{
			return GetMultiSurveyLanguageCollection(forceFetch, _surveyLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyLanguageCollection GetMultiSurveyLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyLanguageCollection(forceFetch, _surveyLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyLanguageCollection GetMultiSurveyLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyLanguageCollection GetMultiSurveyLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyLanguageCollection || forceFetch || _alwaysFetchSurveyLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyLanguageCollection);
				_surveyLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyLanguageCollection.GetMultiManyToOne(this, null, filter);
				_surveyLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyLanguageCollection = true;
			}
			return _surveyLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyLanguageCollection'. These settings will be taken into account
		/// when the property SurveyLanguageCollection is requested or GetMultiSurveyLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyLanguageCollection.SortClauses=sortClauses;
			_surveyLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch)
		{
			return GetMultiSurveyQuestionLanguageCollection(forceFetch, _surveyQuestionLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyQuestionLanguageCollection(forceFetch, _surveyQuestionLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyQuestionLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyQuestionLanguageCollection || forceFetch || _alwaysFetchSurveyQuestionLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestionLanguageCollection);
				_surveyQuestionLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestionLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestionLanguageCollection.GetMultiManyToOne(this, null, filter);
				_surveyQuestionLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestionLanguageCollection = true;
			}
			return _surveyQuestionLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestionLanguageCollection'. These settings will be taken into account
		/// when the property SurveyQuestionLanguageCollection is requested or GetMultiSurveyQuestionLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestionLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestionLanguageCollection.SortClauses=sortClauses;
			_surveyQuestionLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIFooterItemLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection GetMultiUIFooterItemLanguageCollection(bool forceFetch)
		{
			return GetMultiUIFooterItemLanguageCollection(forceFetch, _uIFooterItemLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIFooterItemLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection GetMultiUIFooterItemLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIFooterItemLanguageCollection(forceFetch, _uIFooterItemLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection GetMultiUIFooterItemLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIFooterItemLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIFooterItemLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection GetMultiUIFooterItemLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIFooterItemLanguageCollection || forceFetch || _alwaysFetchUIFooterItemLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIFooterItemLanguageCollection);
				_uIFooterItemLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_uIFooterItemLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIFooterItemLanguageCollection.GetMultiManyToOne(this, null, filter);
				_uIFooterItemLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIFooterItemLanguageCollection = true;
			}
			return _uIFooterItemLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIFooterItemLanguageCollection'. These settings will be taken into account
		/// when the property UIFooterItemLanguageCollection is requested or GetMultiUIFooterItemLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIFooterItemLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIFooterItemLanguageCollection.SortClauses=sortClauses;
			_uIFooterItemLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UITabLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch)
		{
			return GetMultiUITabLanguageCollection(forceFetch, _uITabLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UITabLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUITabLanguageCollection(forceFetch, _uITabLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUITabLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUITabLanguageCollection || forceFetch || _alwaysFetchUITabLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uITabLanguageCollection);
				_uITabLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_uITabLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_uITabLanguageCollection.GetMultiManyToOne(this, null, filter);
				_uITabLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUITabLanguageCollection = true;
			}
			return _uITabLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UITabLanguageCollection'. These settings will be taken into account
		/// when the property UITabLanguageCollection is requested or GetMultiUITabLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUITabLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uITabLanguageCollection.SortClauses=sortClauses;
			_uITabLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch)
		{
			return GetMultiUIWidgetLanguageCollection(forceFetch, _uIWidgetLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetLanguageCollection(forceFetch, _uIWidgetLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection GetMultiUIWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetLanguageCollection || forceFetch || _alwaysFetchUIWidgetLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetLanguageCollection);
				_uIWidgetLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetLanguageCollection.GetMultiManyToOne(this, null, filter);
				_uIWidgetLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetLanguageCollection = true;
			}
			return _uIWidgetLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetLanguageCollection'. These settings will be taken into account
		/// when the property UIWidgetLanguageCollection is requested or GetMultiUIWidgetLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetLanguageCollection.SortClauses=sortClauses;
			_uIWidgetLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VenueCategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch)
		{
			return GetMultiVenueCategoryLanguageCollection(forceFetch, _venueCategoryLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VenueCategoryLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVenueCategoryLanguageCollection(forceFetch, _venueCategoryLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVenueCategoryLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection GetMultiVenueCategoryLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVenueCategoryLanguageCollection || forceFetch || _alwaysFetchVenueCategoryLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_venueCategoryLanguageCollection);
				_venueCategoryLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_venueCategoryLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_venueCategoryLanguageCollection.GetMultiManyToOne(this, null, filter);
				_venueCategoryLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedVenueCategoryLanguageCollection = true;
			}
			return _venueCategoryLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'VenueCategoryLanguageCollection'. These settings will be taken into account
		/// when the property VenueCategoryLanguageCollection is requested or GetMultiVenueCategoryLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVenueCategoryLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_venueCategoryLanguageCollection.SortClauses=sortClauses;
			_venueCategoryLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage(forceFetch, _entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(LanguageFields.LanguageId, ComparisonOperator.Equal, this.LanguageId, "LanguageEntity__"));
				_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage"));
				_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = true;
			}
			return _entertainmentcategoryCollectionViaEntertainmentcategoryLanguage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage is requested or GetMultiEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCompany(forceFetch, _routeCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCompany || forceFetch || _alwaysFetchRouteCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(LanguageFields.LanguageId, ComparisonOperator.Equal, this.LanguageId, "LanguageEntity__"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCompany.GetMulti(filter, GetRelationsForField("RouteCollectionViaCompany"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCompany = true;
			}
			return _routeCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCompany'. These settings will be taken into account
		/// when the property RouteCollectionViaCompany is requested or GetMultiRouteCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCompany.SortClauses=sortClauses;
			_routeCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaCompany(forceFetch, _supportpoolCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaCompany || forceFetch || _alwaysFetchSupportpoolCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(LanguageFields.LanguageId, ComparisonOperator.Equal, this.LanguageId, "LanguageEntity__"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaCompany.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaCompany"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaCompany = true;
			}
			return _supportpoolCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaCompany'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaCompany is requested or GetMultiSupportpoolCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaCompany.SortClauses=sortClauses;
			_supportpoolCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UITabEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollectionViaUITabLanguage(bool forceFetch)
		{
			return GetMultiUITabCollectionViaUITabLanguage(forceFetch, _uITabCollectionViaUITabLanguage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UITabCollection GetMultiUITabCollectionViaUITabLanguage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUITabCollectionViaUITabLanguage || forceFetch || _alwaysFetchUITabCollectionViaUITabLanguage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uITabCollectionViaUITabLanguage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(LanguageFields.LanguageId, ComparisonOperator.Equal, this.LanguageId, "LanguageEntity__"));
				_uITabCollectionViaUITabLanguage.SuppressClearInGetMulti=!forceFetch;
				_uITabCollectionViaUITabLanguage.EntityFactoryToUse = entityFactoryToUse;
				_uITabCollectionViaUITabLanguage.GetMulti(filter, GetRelationsForField("UITabCollectionViaUITabLanguage"));
				_uITabCollectionViaUITabLanguage.SuppressClearInGetMulti=false;
				_alreadyFetchedUITabCollectionViaUITabLanguage = true;
			}
			return _uITabCollectionViaUITabLanguage;
		}

		/// <summary> Sets the collection parameters for the collection for 'UITabCollectionViaUITabLanguage'. These settings will be taken into account
		/// when the property UITabCollectionViaUITabLanguage is requested or GetMultiUITabCollectionViaUITabLanguage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUITabCollectionViaUITabLanguage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uITabCollectionViaUITabLanguage.SortClauses=sortClauses;
			_uITabCollectionViaUITabLanguage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ActionButtonLanguageCollection", _actionButtonLanguageCollection);
			toReturn.Add("AdvertisementLanguageCollection", _advertisementLanguageCollection);
			toReturn.Add("AdvertisementTagLanguageCollection", _advertisementTagLanguageCollection);
			toReturn.Add("AlterationLanguageCollection", _alterationLanguageCollection);
			toReturn.Add("AlterationoptionLanguageCollection", _alterationoptionLanguageCollection);
			toReturn.Add("AmenityLanguageCollection", _amenityLanguageCollection);
			toReturn.Add("AnnouncementLanguageCollection", _announcementLanguageCollection);
			toReturn.Add("AttachmentLanguageCollection", _attachmentLanguageCollection);
			toReturn.Add("CategoryLanguageCollection", _categoryLanguageCollection);
			toReturn.Add("CompanyCollection", _companyCollection);
			toReturn.Add("CompanyLanguageCollection", _companyLanguageCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("DeliverypointgroupLanguageCollection", _deliverypointgroupLanguageCollection);
			toReturn.Add("EntertainmentcategoryLanguageCollection", _entertainmentcategoryLanguageCollection);
			toReturn.Add("GenericcategoryLanguageCollection", _genericcategoryLanguageCollection);
			toReturn.Add("GenericproductLanguageCollection", _genericproductLanguageCollection);
			toReturn.Add("MediaLanguageCollection", _mediaLanguageCollection);
			toReturn.Add("PageElementCollection", _pageElementCollection);
			toReturn.Add("PageLanguageCollection", _pageLanguageCollection);
			toReturn.Add("PageTemplateElementCollection", _pageTemplateElementCollection);
			toReturn.Add("PageTemplateLanguageCollection", _pageTemplateLanguageCollection);
			toReturn.Add("PointOfInterestLanguageCollection", _pointOfInterestLanguageCollection);
			toReturn.Add("ProductLanguageCollection", _productLanguageCollection);
			toReturn.Add("RoomControlAreaLanguageCollection", _roomControlAreaLanguageCollection);
			toReturn.Add("RoomControlComponentLanguageCollection", _roomControlComponentLanguageCollection);
			toReturn.Add("RoomControlSectionItemLanguageCollection", _roomControlSectionItemLanguageCollection);
			toReturn.Add("RoomControlSectionLanguageCollection", _roomControlSectionLanguageCollection);
			toReturn.Add("RoomControlWidgetLanguageCollection", _roomControlWidgetLanguageCollection);
			toReturn.Add("ScheduledMessageLanguageCollection", _scheduledMessageLanguageCollection);
			toReturn.Add("SiteLanguageCollection", _siteLanguageCollection);
			toReturn.Add("SiteTemplateLanguageCollection", _siteTemplateLanguageCollection);
			toReturn.Add("StationLanguageCollection", _stationLanguageCollection);
			toReturn.Add("SurveyAnswerLanguageCollection", _surveyAnswerLanguageCollection);
			toReturn.Add("SurveyLanguageCollection", _surveyLanguageCollection);
			toReturn.Add("SurveyQuestionLanguageCollection", _surveyQuestionLanguageCollection);
			toReturn.Add("UIFooterItemLanguageCollection", _uIFooterItemLanguageCollection);
			toReturn.Add("UITabLanguageCollection", _uITabLanguageCollection);
			toReturn.Add("UIWidgetLanguageCollection", _uIWidgetLanguageCollection);
			toReturn.Add("VenueCategoryLanguageCollection", _venueCategoryLanguageCollection);
			toReturn.Add("EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage", _entertainmentcategoryCollectionViaEntertainmentcategoryLanguage);
			toReturn.Add("RouteCollectionViaCompany", _routeCollectionViaCompany);
			toReturn.Add("SupportpoolCollectionViaCompany", _supportpoolCollectionViaCompany);
			toReturn.Add("UITabCollectionViaUITabLanguage", _uITabCollectionViaUITabLanguage);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <param name="validator">The validator object for this LanguageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 languageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(languageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_actionButtonLanguageCollection = new Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection();
			_actionButtonLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_advertisementLanguageCollection = new Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection();
			_advertisementLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_advertisementTagLanguageCollection = new Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection();
			_advertisementTagLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_alterationLanguageCollection = new Obymobi.Data.CollectionClasses.AlterationLanguageCollection();
			_alterationLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_alterationoptionLanguageCollection = new Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection();
			_alterationoptionLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_amenityLanguageCollection = new Obymobi.Data.CollectionClasses.AmenityLanguageCollection();
			_amenityLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_announcementLanguageCollection = new Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection();
			_announcementLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_attachmentLanguageCollection = new Obymobi.Data.CollectionClasses.AttachmentLanguageCollection();
			_attachmentLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_categoryLanguageCollection = new Obymobi.Data.CollectionClasses.CategoryLanguageCollection();
			_categoryLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_companyCollection = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_companyLanguageCollection = new Obymobi.Data.CollectionClasses.CompanyLanguageCollection();
			_companyLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_deliverypointgroupLanguageCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection();
			_deliverypointgroupLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_entertainmentcategoryLanguageCollection = new Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection();
			_entertainmentcategoryLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_genericcategoryLanguageCollection = new Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection();
			_genericcategoryLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_genericproductLanguageCollection = new Obymobi.Data.CollectionClasses.GenericproductLanguageCollection();
			_genericproductLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_mediaLanguageCollection = new Obymobi.Data.CollectionClasses.MediaLanguageCollection();
			_mediaLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_pageElementCollection = new Obymobi.Data.CollectionClasses.PageElementCollection();
			_pageElementCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_pageLanguageCollection = new Obymobi.Data.CollectionClasses.PageLanguageCollection();
			_pageLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_pageTemplateElementCollection = new Obymobi.Data.CollectionClasses.PageTemplateElementCollection();
			_pageTemplateElementCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_pageTemplateLanguageCollection = new Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection();
			_pageTemplateLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_pointOfInterestLanguageCollection = new Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection();
			_pointOfInterestLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_productLanguageCollection = new Obymobi.Data.CollectionClasses.ProductLanguageCollection();
			_productLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_roomControlAreaLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection();
			_roomControlAreaLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_roomControlComponentLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection();
			_roomControlComponentLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_roomControlSectionItemLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection();
			_roomControlSectionItemLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_roomControlSectionLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection();
			_roomControlSectionLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_roomControlWidgetLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection();
			_roomControlWidgetLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_scheduledMessageLanguageCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection();
			_scheduledMessageLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_siteLanguageCollection = new Obymobi.Data.CollectionClasses.SiteLanguageCollection();
			_siteLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_siteTemplateLanguageCollection = new Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection();
			_siteTemplateLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_stationLanguageCollection = new Obymobi.Data.CollectionClasses.StationLanguageCollection();
			_stationLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_surveyAnswerLanguageCollection = new Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection();
			_surveyAnswerLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_surveyLanguageCollection = new Obymobi.Data.CollectionClasses.SurveyLanguageCollection();
			_surveyLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_surveyQuestionLanguageCollection = new Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection();
			_surveyQuestionLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_uIFooterItemLanguageCollection = new Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection();
			_uIFooterItemLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_uITabLanguageCollection = new Obymobi.Data.CollectionClasses.UITabLanguageCollection();
			_uITabLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_uIWidgetLanguageCollection = new Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection();
			_uIWidgetLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");

			_venueCategoryLanguageCollection = new Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection();
			_venueCategoryLanguageCollection.SetContainingEntityInfo(this, "LanguageEntity");
			_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_routeCollectionViaCompany = new Obymobi.Data.CollectionClasses.RouteCollection();
			_supportpoolCollectionViaCompany = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_uITabCollectionViaUITabLanguage = new Obymobi.Data.CollectionClasses.UITabCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocalizedName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodeAlpha3", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="languageId">PK value for Language which data should be fetched into this Language object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 languageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)LanguageFieldIndex.LanguageId].ForcedCurrentValueWrite(languageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateLanguageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new LanguageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static LanguageRelations Relations
		{
			get	{ return new LanguageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ActionButtonLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionButtonLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection(), (IEntityRelation)GetRelationsForField("ActionButtonLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.ActionButtonLanguageEntity, 0, null, null, null, "ActionButtonLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection(), (IEntityRelation)GetRelationsForField("AdvertisementLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.AdvertisementLanguageEntity, 0, null, null, null, "AdvertisementLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTagLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.AdvertisementTagLanguageEntity, 0, null, null, null, "AdvertisementTagLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationLanguageCollection(), (IEntityRelation)GetRelationsForField("AlterationLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.AlterationLanguageEntity, 0, null, null, null, "AlterationLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationoptionLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.AlterationoptionLanguageEntity, 0, null, null, null, "AlterationoptionLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AmenityLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAmenityLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AmenityLanguageCollection(), (IEntityRelation)GetRelationsForField("AmenityLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.AmenityLanguageEntity, 0, null, null, null, "AmenityLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AnnouncementLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection(), (IEntityRelation)GetRelationsForField("AnnouncementLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.AnnouncementLanguageEntity, 0, null, null, null, "AnnouncementLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttachmentLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttachmentLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AttachmentLanguageCollection(), (IEntityRelation)GetRelationsForField("AttachmentLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.AttachmentLanguageEntity, 0, null, null, null, "AttachmentLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("CategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.CategoryLanguageEntity, 0, null, null, null, "CategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyLanguageCollection(), (IEntityRelation)GetRelationsForField("CompanyLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.CompanyLanguageEntity, 0, null, null, null, "CompanyLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeliverypointgroupLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity, 0, null, null, null, "DeliverypointgroupLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntertainmentcategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("EntertainmentcategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity, 0, null, null, null, "EntertainmentcategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GenericcategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("GenericcategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.GenericcategoryLanguageEntity, 0, null, null, null, "GenericcategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GenericproductLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductLanguageCollection(), (IEntityRelation)GetRelationsForField("GenericproductLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.GenericproductLanguageEntity, 0, null, null, null, "GenericproductLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaLanguageCollection(), (IEntityRelation)GetRelationsForField("MediaLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.MediaLanguageEntity, 0, null, null, null, "MediaLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageElementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageElementCollection(), (IEntityRelation)GetRelationsForField("PageElementCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.PageElementEntity, 0, null, null, null, "PageElementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageLanguageCollection(), (IEntityRelation)GetRelationsForField("PageLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.PageLanguageEntity, 0, null, null, null, "PageLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplateElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateElementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateElementCollection(), (IEntityRelation)GetRelationsForField("PageTemplateElementCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.PageTemplateElementEntity, 0, null, null, null, "PageTemplateElementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageTemplateLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageTemplateLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection(), (IEntityRelation)GetRelationsForField("PageTemplateLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.PageTemplateLanguageEntity, 0, null, null, null, "PageTemplateLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterestLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.PointOfInterestLanguageEntity, 0, null, null, null, "PointOfInterestLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductLanguageCollection(), (IEntityRelation)GetRelationsForField("ProductLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.ProductLanguageEntity, 0, null, null, null, "ProductLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlAreaLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlAreaLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlAreaLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.RoomControlAreaLanguageEntity, 0, null, null, null, "RoomControlAreaLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponentLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.RoomControlComponentLanguageEntity, 0, null, null, null, "RoomControlComponentLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionItemLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionItemLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionItemLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.RoomControlSectionItemLanguageEntity, 0, null, null, null, "RoomControlSectionItemLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.RoomControlSectionLanguageEntity, 0, null, null, null, "RoomControlSectionLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidgetLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetLanguageEntity, 0, null, null, null, "RoomControlWidgetLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessageLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.ScheduledMessageLanguageEntity, 0, null, null, null, "ScheduledMessageLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteLanguageCollection(), (IEntityRelation)GetRelationsForField("SiteLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.SiteLanguageEntity, 0, null, null, null, "SiteLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SiteTemplateLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteTemplateLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection(), (IEntityRelation)GetRelationsForField("SiteTemplateLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.SiteTemplateLanguageEntity, 0, null, null, null, "SiteTemplateLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StationLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStationLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.StationLanguageCollection(), (IEntityRelation)GetRelationsForField("StationLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.StationLanguageEntity, 0, null, null, null, "StationLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswerLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswerLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswerLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.SurveyAnswerLanguageEntity, 0, null, null, null, "SurveyAnswerLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyLanguageCollection(), (IEntityRelation)GetRelationsForField("SurveyLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.SurveyLanguageEntity, 0, null, null, null, "SurveyLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestionLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestionLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.SurveyQuestionLanguageEntity, 0, null, null, null, "SurveyQuestionLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIFooterItemLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIFooterItemLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection(), (IEntityRelation)GetRelationsForField("UIFooterItemLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.UIFooterItemLanguageEntity, 0, null, null, null, "UIFooterItemLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITabLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabLanguageCollection(), (IEntityRelation)GetRelationsForField("UITabLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.UITabLanguageEntity, 0, null, null, null, "UITabLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidgetLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection(), (IEntityRelation)GetRelationsForField("UIWidgetLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.UIWidgetLanguageEntity, 0, null, null, null, "UIWidgetLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VenueCategoryLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVenueCategoryLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection(), (IEntityRelation)GetRelationsForField("VenueCategoryLanguageCollection")[0], (int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.VenueCategoryLanguageEntity, 0, null, null, null, "VenueCategoryLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.EntertainmentcategoryLanguageEntityUsingLanguageId;
				intermediateRelation.SetAliases(string.Empty, "EntertainmentcategoryLanguage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage"), "EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingLanguageId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCompany"), "RouteCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingLanguageId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaCompany"), "SupportpoolCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITab'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabCollectionViaUITabLanguage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UITabLanguageEntityUsingLanguageId;
				intermediateRelation.SetAliases(string.Empty, "UITabLanguage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.LanguageEntity, (int)Obymobi.Data.EntityType.UITabEntity, 0, null, null, GetRelationsForField("UITabCollectionViaUITabLanguage"), "UITabCollectionViaUITabLanguage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LanguageId property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."LanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 LanguageId
		{
			get { return (System.Int32)GetValue((int)LanguageFieldIndex.LanguageId, true); }
			set	{ SetValue((int)LanguageFieldIndex.LanguageId, value, true); }
		}

		/// <summary> The Code property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."Code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)LanguageFieldIndex.Code, true); }
			set	{ SetValue((int)LanguageFieldIndex.Code, value, true); }
		}

		/// <summary> The Name property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)LanguageFieldIndex.Name, true); }
			set	{ SetValue((int)LanguageFieldIndex.Name, value, true); }
		}

		/// <summary> The LocalizedName property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."LocalizedName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LocalizedName
		{
			get { return (System.String)GetValue((int)LanguageFieldIndex.LocalizedName, true); }
			set	{ SetValue((int)LanguageFieldIndex.LocalizedName, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)LanguageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)LanguageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)LanguageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)LanguageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)LanguageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)LanguageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)LanguageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)LanguageFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CodeAlpha3 property of the Entity Language<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Language"."CodeAlpha3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CodeAlpha3
		{
			get { return (System.String)GetValue((int)LanguageFieldIndex.CodeAlpha3, true); }
			set	{ SetValue((int)LanguageFieldIndex.CodeAlpha3, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ActionButtonLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiActionButtonLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ActionButtonLanguageCollection ActionButtonLanguageCollection
		{
			get	{ return GetMultiActionButtonLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ActionButtonLanguageCollection. When set to true, ActionButtonLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionButtonLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiActionButtonLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionButtonLanguageCollection
		{
			get	{ return _alwaysFetchActionButtonLanguageCollection; }
			set	{ _alwaysFetchActionButtonLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionButtonLanguageCollection already has been fetched. Setting this property to false when ActionButtonLanguageCollection has been fetched
		/// will clear the ActionButtonLanguageCollection collection well. Setting this property to true while ActionButtonLanguageCollection hasn't been fetched disables lazy loading for ActionButtonLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionButtonLanguageCollection
		{
			get { return _alreadyFetchedActionButtonLanguageCollection;}
			set 
			{
				if(_alreadyFetchedActionButtonLanguageCollection && !value && (_actionButtonLanguageCollection != null))
				{
					_actionButtonLanguageCollection.Clear();
				}
				_alreadyFetchedActionButtonLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementLanguageCollection AdvertisementLanguageCollection
		{
			get	{ return GetMultiAdvertisementLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementLanguageCollection. When set to true, AdvertisementLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementLanguageCollection
		{
			get	{ return _alwaysFetchAdvertisementLanguageCollection; }
			set	{ _alwaysFetchAdvertisementLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementLanguageCollection already has been fetched. Setting this property to false when AdvertisementLanguageCollection has been fetched
		/// will clear the AdvertisementLanguageCollection collection well. Setting this property to true while AdvertisementLanguageCollection hasn't been fetched disables lazy loading for AdvertisementLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementLanguageCollection
		{
			get { return _alreadyFetchedAdvertisementLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementLanguageCollection && !value && (_advertisementLanguageCollection != null))
				{
					_advertisementLanguageCollection.Clear();
				}
				_alreadyFetchedAdvertisementLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AdvertisementTagLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementTagLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementTagLanguageCollection AdvertisementTagLanguageCollection
		{
			get	{ return GetMultiAdvertisementTagLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagLanguageCollection. When set to true, AdvertisementTagLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementTagLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagLanguageCollection
		{
			get	{ return _alwaysFetchAdvertisementTagLanguageCollection; }
			set	{ _alwaysFetchAdvertisementTagLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagLanguageCollection already has been fetched. Setting this property to false when AdvertisementTagLanguageCollection has been fetched
		/// will clear the AdvertisementTagLanguageCollection collection well. Setting this property to true while AdvertisementTagLanguageCollection hasn't been fetched disables lazy loading for AdvertisementTagLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagLanguageCollection
		{
			get { return _alreadyFetchedAdvertisementTagLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagLanguageCollection && !value && (_advertisementTagLanguageCollection != null))
				{
					_advertisementTagLanguageCollection.Clear();
				}
				_alreadyFetchedAdvertisementTagLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationLanguageCollection AlterationLanguageCollection
		{
			get	{ return GetMultiAlterationLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationLanguageCollection. When set to true, AlterationLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationLanguageCollection
		{
			get	{ return _alwaysFetchAlterationLanguageCollection; }
			set	{ _alwaysFetchAlterationLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationLanguageCollection already has been fetched. Setting this property to false when AlterationLanguageCollection has been fetched
		/// will clear the AlterationLanguageCollection collection well. Setting this property to true while AlterationLanguageCollection hasn't been fetched disables lazy loading for AlterationLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationLanguageCollection
		{
			get { return _alreadyFetchedAlterationLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAlterationLanguageCollection && !value && (_alterationLanguageCollection != null))
				{
					_alterationLanguageCollection.Clear();
				}
				_alreadyFetchedAlterationLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationoptionLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionLanguageCollection AlterationoptionLanguageCollection
		{
			get	{ return GetMultiAlterationoptionLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionLanguageCollection. When set to true, AlterationoptionLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationoptionLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionLanguageCollection
		{
			get	{ return _alwaysFetchAlterationoptionLanguageCollection; }
			set	{ _alwaysFetchAlterationoptionLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionLanguageCollection already has been fetched. Setting this property to false when AlterationoptionLanguageCollection has been fetched
		/// will clear the AlterationoptionLanguageCollection collection well. Setting this property to true while AlterationoptionLanguageCollection hasn't been fetched disables lazy loading for AlterationoptionLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionLanguageCollection
		{
			get { return _alreadyFetchedAlterationoptionLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAlterationoptionLanguageCollection && !value && (_alterationoptionLanguageCollection != null))
				{
					_alterationoptionLanguageCollection.Clear();
				}
				_alreadyFetchedAlterationoptionLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAmenityLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AmenityLanguageCollection AmenityLanguageCollection
		{
			get	{ return GetMultiAmenityLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AmenityLanguageCollection. When set to true, AmenityLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AmenityLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAmenityLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAmenityLanguageCollection
		{
			get	{ return _alwaysFetchAmenityLanguageCollection; }
			set	{ _alwaysFetchAmenityLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AmenityLanguageCollection already has been fetched. Setting this property to false when AmenityLanguageCollection has been fetched
		/// will clear the AmenityLanguageCollection collection well. Setting this property to true while AmenityLanguageCollection hasn't been fetched disables lazy loading for AmenityLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAmenityLanguageCollection
		{
			get { return _alreadyFetchedAmenityLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAmenityLanguageCollection && !value && (_amenityLanguageCollection != null))
				{
					_amenityLanguageCollection.Clear();
				}
				_alreadyFetchedAmenityLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AnnouncementLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementLanguageCollection AnnouncementLanguageCollection
		{
			get	{ return GetMultiAnnouncementLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementLanguageCollection. When set to true, AnnouncementLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAnnouncementLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementLanguageCollection
		{
			get	{ return _alwaysFetchAnnouncementLanguageCollection; }
			set	{ _alwaysFetchAnnouncementLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementLanguageCollection already has been fetched. Setting this property to false when AnnouncementLanguageCollection has been fetched
		/// will clear the AnnouncementLanguageCollection collection well. Setting this property to true while AnnouncementLanguageCollection hasn't been fetched disables lazy loading for AnnouncementLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementLanguageCollection
		{
			get { return _alreadyFetchedAnnouncementLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAnnouncementLanguageCollection && !value && (_announcementLanguageCollection != null))
				{
					_announcementLanguageCollection.Clear();
				}
				_alreadyFetchedAnnouncementLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttachmentLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttachmentLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AttachmentLanguageCollection AttachmentLanguageCollection
		{
			get	{ return GetMultiAttachmentLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttachmentLanguageCollection. When set to true, AttachmentLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttachmentLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAttachmentLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttachmentLanguageCollection
		{
			get	{ return _alwaysFetchAttachmentLanguageCollection; }
			set	{ _alwaysFetchAttachmentLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttachmentLanguageCollection already has been fetched. Setting this property to false when AttachmentLanguageCollection has been fetched
		/// will clear the AttachmentLanguageCollection collection well. Setting this property to true while AttachmentLanguageCollection hasn't been fetched disables lazy loading for AttachmentLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttachmentLanguageCollection
		{
			get { return _alreadyFetchedAttachmentLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAttachmentLanguageCollection && !value && (_attachmentLanguageCollection != null))
				{
					_attachmentLanguageCollection.Clear();
				}
				_alreadyFetchedAttachmentLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryLanguageCollection CategoryLanguageCollection
		{
			get	{ return GetMultiCategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryLanguageCollection. When set to true, CategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryLanguageCollection
		{
			get	{ return _alwaysFetchCategoryLanguageCollection; }
			set	{ _alwaysFetchCategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryLanguageCollection already has been fetched. Setting this property to false when CategoryLanguageCollection has been fetched
		/// will clear the CategoryLanguageCollection collection well. Setting this property to true while CategoryLanguageCollection hasn't been fetched disables lazy loading for CategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryLanguageCollection
		{
			get { return _alreadyFetchedCategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedCategoryLanguageCollection && !value && (_categoryLanguageCollection != null))
				{
					_categoryLanguageCollection.Clear();
				}
				_alreadyFetchedCategoryLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollection
		{
			get	{ return GetMultiCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollection. When set to true, CompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollection
		{
			get	{ return _alwaysFetchCompanyCollection; }
			set	{ _alwaysFetchCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollection already has been fetched. Setting this property to false when CompanyCollection has been fetched
		/// will clear the CompanyCollection collection well. Setting this property to true while CompanyCollection hasn't been fetched disables lazy loading for CompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollection
		{
			get { return _alreadyFetchedCompanyCollection;}
			set 
			{
				if(_alreadyFetchedCompanyCollection && !value && (_companyCollection != null))
				{
					_companyCollection.Clear();
				}
				_alreadyFetchedCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CompanyLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyLanguageCollection CompanyLanguageCollection
		{
			get	{ return GetMultiCompanyLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyLanguageCollection. When set to true, CompanyLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyLanguageCollection
		{
			get	{ return _alwaysFetchCompanyLanguageCollection; }
			set	{ _alwaysFetchCompanyLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyLanguageCollection already has been fetched. Setting this property to false when CompanyLanguageCollection has been fetched
		/// will clear the CompanyLanguageCollection collection well. Setting this property to true while CompanyLanguageCollection hasn't been fetched disables lazy loading for CompanyLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyLanguageCollection
		{
			get { return _alreadyFetchedCompanyLanguageCollection;}
			set 
			{
				if(_alreadyFetchedCompanyLanguageCollection && !value && (_companyLanguageCollection != null))
				{
					_companyLanguageCollection.Clear();
				}
				_alreadyFetchedCompanyLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupLanguageCollection DeliverypointgroupLanguageCollection
		{
			get	{ return GetMultiDeliverypointgroupLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupLanguageCollection. When set to true, DeliverypointgroupLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupLanguageCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupLanguageCollection; }
			set	{ _alwaysFetchDeliverypointgroupLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupLanguageCollection already has been fetched. Setting this property to false when DeliverypointgroupLanguageCollection has been fetched
		/// will clear the DeliverypointgroupLanguageCollection collection well. Setting this property to true while DeliverypointgroupLanguageCollection hasn't been fetched disables lazy loading for DeliverypointgroupLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupLanguageCollection
		{
			get { return _alreadyFetchedDeliverypointgroupLanguageCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupLanguageCollection && !value && (_deliverypointgroupLanguageCollection != null))
				{
					_deliverypointgroupLanguageCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryLanguageCollection EntertainmentcategoryLanguageCollection
		{
			get	{ return GetMultiEntertainmentcategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryLanguageCollection. When set to true, EntertainmentcategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiEntertainmentcategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryLanguageCollection
		{
			get	{ return _alwaysFetchEntertainmentcategoryLanguageCollection; }
			set	{ _alwaysFetchEntertainmentcategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryLanguageCollection already has been fetched. Setting this property to false when EntertainmentcategoryLanguageCollection has been fetched
		/// will clear the EntertainmentcategoryLanguageCollection collection well. Setting this property to true while EntertainmentcategoryLanguageCollection hasn't been fetched disables lazy loading for EntertainmentcategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryLanguageCollection
		{
			get { return _alreadyFetchedEntertainmentcategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryLanguageCollection && !value && (_entertainmentcategoryLanguageCollection != null))
				{
					_entertainmentcategoryLanguageCollection.Clear();
				}
				_alreadyFetchedEntertainmentcategoryLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericcategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryLanguageCollection GenericcategoryLanguageCollection
		{
			get	{ return GetMultiGenericcategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryLanguageCollection. When set to true, GenericcategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericcategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryLanguageCollection
		{
			get	{ return _alwaysFetchGenericcategoryLanguageCollection; }
			set	{ _alwaysFetchGenericcategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryLanguageCollection already has been fetched. Setting this property to false when GenericcategoryLanguageCollection has been fetched
		/// will clear the GenericcategoryLanguageCollection collection well. Setting this property to true while GenericcategoryLanguageCollection hasn't been fetched disables lazy loading for GenericcategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryLanguageCollection
		{
			get { return _alreadyFetchedGenericcategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedGenericcategoryLanguageCollection && !value && (_genericcategoryLanguageCollection != null))
				{
					_genericcategoryLanguageCollection.Clear();
				}
				_alreadyFetchedGenericcategoryLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericproductLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductLanguageCollection GenericproductLanguageCollection
		{
			get	{ return GetMultiGenericproductLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductLanguageCollection. When set to true, GenericproductLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductLanguageCollection
		{
			get	{ return _alwaysFetchGenericproductLanguageCollection; }
			set	{ _alwaysFetchGenericproductLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductLanguageCollection already has been fetched. Setting this property to false when GenericproductLanguageCollection has been fetched
		/// will clear the GenericproductLanguageCollection collection well. Setting this property to true while GenericproductLanguageCollection hasn't been fetched disables lazy loading for GenericproductLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductLanguageCollection
		{
			get { return _alreadyFetchedGenericproductLanguageCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductLanguageCollection && !value && (_genericproductLanguageCollection != null))
				{
					_genericproductLanguageCollection.Clear();
				}
				_alreadyFetchedGenericproductLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaLanguageCollection MediaLanguageCollection
		{
			get	{ return GetMultiMediaLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaLanguageCollection. When set to true, MediaLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaLanguageCollection
		{
			get	{ return _alwaysFetchMediaLanguageCollection; }
			set	{ _alwaysFetchMediaLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaLanguageCollection already has been fetched. Setting this property to false when MediaLanguageCollection has been fetched
		/// will clear the MediaLanguageCollection collection well. Setting this property to true while MediaLanguageCollection hasn't been fetched disables lazy loading for MediaLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaLanguageCollection
		{
			get { return _alreadyFetchedMediaLanguageCollection;}
			set 
			{
				if(_alreadyFetchedMediaLanguageCollection && !value && (_mediaLanguageCollection != null))
				{
					_mediaLanguageCollection.Clear();
				}
				_alreadyFetchedMediaLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageElementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageElementCollection PageElementCollection
		{
			get	{ return GetMultiPageElementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageElementCollection. When set to true, PageElementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageElementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageElementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageElementCollection
		{
			get	{ return _alwaysFetchPageElementCollection; }
			set	{ _alwaysFetchPageElementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageElementCollection already has been fetched. Setting this property to false when PageElementCollection has been fetched
		/// will clear the PageElementCollection collection well. Setting this property to true while PageElementCollection hasn't been fetched disables lazy loading for PageElementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageElementCollection
		{
			get { return _alreadyFetchedPageElementCollection;}
			set 
			{
				if(_alreadyFetchedPageElementCollection && !value && (_pageElementCollection != null))
				{
					_pageElementCollection.Clear();
				}
				_alreadyFetchedPageElementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageLanguageCollection PageLanguageCollection
		{
			get	{ return GetMultiPageLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageLanguageCollection. When set to true, PageLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageLanguageCollection
		{
			get	{ return _alwaysFetchPageLanguageCollection; }
			set	{ _alwaysFetchPageLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageLanguageCollection already has been fetched. Setting this property to false when PageLanguageCollection has been fetched
		/// will clear the PageLanguageCollection collection well. Setting this property to true while PageLanguageCollection hasn't been fetched disables lazy loading for PageLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageLanguageCollection
		{
			get { return _alreadyFetchedPageLanguageCollection;}
			set 
			{
				if(_alreadyFetchedPageLanguageCollection && !value && (_pageLanguageCollection != null))
				{
					_pageLanguageCollection.Clear();
				}
				_alreadyFetchedPageLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageTemplateElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageTemplateElementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateElementCollection PageTemplateElementCollection
		{
			get	{ return GetMultiPageTemplateElementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateElementCollection. When set to true, PageTemplateElementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateElementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageTemplateElementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateElementCollection
		{
			get	{ return _alwaysFetchPageTemplateElementCollection; }
			set	{ _alwaysFetchPageTemplateElementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateElementCollection already has been fetched. Setting this property to false when PageTemplateElementCollection has been fetched
		/// will clear the PageTemplateElementCollection collection well. Setting this property to true while PageTemplateElementCollection hasn't been fetched disables lazy loading for PageTemplateElementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateElementCollection
		{
			get { return _alreadyFetchedPageTemplateElementCollection;}
			set 
			{
				if(_alreadyFetchedPageTemplateElementCollection && !value && (_pageTemplateElementCollection != null))
				{
					_pageTemplateElementCollection.Clear();
				}
				_alreadyFetchedPageTemplateElementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageTemplateLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageTemplateLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PageTemplateLanguageCollection PageTemplateLanguageCollection
		{
			get	{ return GetMultiPageTemplateLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageTemplateLanguageCollection. When set to true, PageTemplateLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageTemplateLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPageTemplateLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageTemplateLanguageCollection
		{
			get	{ return _alwaysFetchPageTemplateLanguageCollection; }
			set	{ _alwaysFetchPageTemplateLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageTemplateLanguageCollection already has been fetched. Setting this property to false when PageTemplateLanguageCollection has been fetched
		/// will clear the PageTemplateLanguageCollection collection well. Setting this property to true while PageTemplateLanguageCollection hasn't been fetched disables lazy loading for PageTemplateLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageTemplateLanguageCollection
		{
			get { return _alreadyFetchedPageTemplateLanguageCollection;}
			set 
			{
				if(_alreadyFetchedPageTemplateLanguageCollection && !value && (_pageTemplateLanguageCollection != null))
				{
					_pageTemplateLanguageCollection.Clear();
				}
				_alreadyFetchedPageTemplateLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection PointOfInterestLanguageCollection
		{
			get	{ return GetMultiPointOfInterestLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestLanguageCollection. When set to true, PointOfInterestLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestLanguageCollection
		{
			get	{ return _alwaysFetchPointOfInterestLanguageCollection; }
			set	{ _alwaysFetchPointOfInterestLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestLanguageCollection already has been fetched. Setting this property to false when PointOfInterestLanguageCollection has been fetched
		/// will clear the PointOfInterestLanguageCollection collection well. Setting this property to true while PointOfInterestLanguageCollection hasn't been fetched disables lazy loading for PointOfInterestLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestLanguageCollection
		{
			get { return _alreadyFetchedPointOfInterestLanguageCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestLanguageCollection && !value && (_pointOfInterestLanguageCollection != null))
				{
					_pointOfInterestLanguageCollection.Clear();
				}
				_alreadyFetchedPointOfInterestLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductLanguageCollection ProductLanguageCollection
		{
			get	{ return GetMultiProductLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductLanguageCollection. When set to true, ProductLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductLanguageCollection
		{
			get	{ return _alwaysFetchProductLanguageCollection; }
			set	{ _alwaysFetchProductLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductLanguageCollection already has been fetched. Setting this property to false when ProductLanguageCollection has been fetched
		/// will clear the ProductLanguageCollection collection well. Setting this property to true while ProductLanguageCollection hasn't been fetched disables lazy loading for ProductLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductLanguageCollection
		{
			get { return _alreadyFetchedProductLanguageCollection;}
			set 
			{
				if(_alreadyFetchedProductLanguageCollection && !value && (_productLanguageCollection != null))
				{
					_productLanguageCollection.Clear();
				}
				_alreadyFetchedProductLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlAreaLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection RoomControlAreaLanguageCollection
		{
			get	{ return GetMultiRoomControlAreaLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlAreaLanguageCollection. When set to true, RoomControlAreaLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlAreaLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlAreaLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlAreaLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlAreaLanguageCollection; }
			set	{ _alwaysFetchRoomControlAreaLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlAreaLanguageCollection already has been fetched. Setting this property to false when RoomControlAreaLanguageCollection has been fetched
		/// will clear the RoomControlAreaLanguageCollection collection well. Setting this property to true while RoomControlAreaLanguageCollection hasn't been fetched disables lazy loading for RoomControlAreaLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlAreaLanguageCollection
		{
			get { return _alreadyFetchedRoomControlAreaLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlAreaLanguageCollection && !value && (_roomControlAreaLanguageCollection != null))
				{
					_roomControlAreaLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlAreaLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlComponentLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlComponentLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlComponentLanguageCollection RoomControlComponentLanguageCollection
		{
			get	{ return GetMultiRoomControlComponentLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentLanguageCollection. When set to true, RoomControlComponentLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlComponentLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlComponentLanguageCollection; }
			set	{ _alwaysFetchRoomControlComponentLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentLanguageCollection already has been fetched. Setting this property to false when RoomControlComponentLanguageCollection has been fetched
		/// will clear the RoomControlComponentLanguageCollection collection well. Setting this property to true while RoomControlComponentLanguageCollection hasn't been fetched disables lazy loading for RoomControlComponentLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentLanguageCollection
		{
			get { return _alreadyFetchedRoomControlComponentLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentLanguageCollection && !value && (_roomControlComponentLanguageCollection != null))
				{
					_roomControlComponentLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlComponentLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlSectionItemLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlSectionItemLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionItemLanguageCollection RoomControlSectionItemLanguageCollection
		{
			get	{ return GetMultiRoomControlSectionItemLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionItemLanguageCollection. When set to true, RoomControlSectionItemLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionItemLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlSectionItemLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionItemLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlSectionItemLanguageCollection; }
			set	{ _alwaysFetchRoomControlSectionItemLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionItemLanguageCollection already has been fetched. Setting this property to false when RoomControlSectionItemLanguageCollection has been fetched
		/// will clear the RoomControlSectionItemLanguageCollection collection well. Setting this property to true while RoomControlSectionItemLanguageCollection hasn't been fetched disables lazy loading for RoomControlSectionItemLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionItemLanguageCollection
		{
			get { return _alreadyFetchedRoomControlSectionItemLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionItemLanguageCollection && !value && (_roomControlSectionItemLanguageCollection != null))
				{
					_roomControlSectionItemLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlSectionItemLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlSectionLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlSectionLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionLanguageCollection RoomControlSectionLanguageCollection
		{
			get	{ return GetMultiRoomControlSectionLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionLanguageCollection. When set to true, RoomControlSectionLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlSectionLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlSectionLanguageCollection; }
			set	{ _alwaysFetchRoomControlSectionLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionLanguageCollection already has been fetched. Setting this property to false when RoomControlSectionLanguageCollection has been fetched
		/// will clear the RoomControlSectionLanguageCollection collection well. Setting this property to true while RoomControlSectionLanguageCollection hasn't been fetched disables lazy loading for RoomControlSectionLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionLanguageCollection
		{
			get { return _alreadyFetchedRoomControlSectionLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionLanguageCollection && !value && (_roomControlSectionLanguageCollection != null))
				{
					_roomControlSectionLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlSectionLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection RoomControlWidgetLanguageCollection
		{
			get	{ return GetMultiRoomControlWidgetLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetLanguageCollection. When set to true, RoomControlWidgetLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlWidgetLanguageCollection; }
			set	{ _alwaysFetchRoomControlWidgetLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetLanguageCollection already has been fetched. Setting this property to false when RoomControlWidgetLanguageCollection has been fetched
		/// will clear the RoomControlWidgetLanguageCollection collection well. Setting this property to true while RoomControlWidgetLanguageCollection hasn't been fetched disables lazy loading for RoomControlWidgetLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetLanguageCollection
		{
			get { return _alreadyFetchedRoomControlWidgetLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetLanguageCollection && !value && (_roomControlWidgetLanguageCollection != null))
				{
					_roomControlWidgetLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlWidgetLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageLanguageCollection ScheduledMessageLanguageCollection
		{
			get	{ return GetMultiScheduledMessageLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageLanguageCollection. When set to true, ScheduledMessageLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageLanguageCollection
		{
			get	{ return _alwaysFetchScheduledMessageLanguageCollection; }
			set	{ _alwaysFetchScheduledMessageLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageLanguageCollection already has been fetched. Setting this property to false when ScheduledMessageLanguageCollection has been fetched
		/// will clear the ScheduledMessageLanguageCollection collection well. Setting this property to true while ScheduledMessageLanguageCollection hasn't been fetched disables lazy loading for ScheduledMessageLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageLanguageCollection
		{
			get { return _alreadyFetchedScheduledMessageLanguageCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageLanguageCollection && !value && (_scheduledMessageLanguageCollection != null))
				{
					_scheduledMessageLanguageCollection.Clear();
				}
				_alreadyFetchedScheduledMessageLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SiteLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSiteLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SiteLanguageCollection SiteLanguageCollection
		{
			get	{ return GetMultiSiteLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SiteLanguageCollection. When set to true, SiteLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSiteLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteLanguageCollection
		{
			get	{ return _alwaysFetchSiteLanguageCollection; }
			set	{ _alwaysFetchSiteLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteLanguageCollection already has been fetched. Setting this property to false when SiteLanguageCollection has been fetched
		/// will clear the SiteLanguageCollection collection well. Setting this property to true while SiteLanguageCollection hasn't been fetched disables lazy loading for SiteLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteLanguageCollection
		{
			get { return _alreadyFetchedSiteLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSiteLanguageCollection && !value && (_siteLanguageCollection != null))
				{
					_siteLanguageCollection.Clear();
				}
				_alreadyFetchedSiteLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SiteTemplateLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSiteTemplateLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SiteTemplateLanguageCollection SiteTemplateLanguageCollection
		{
			get	{ return GetMultiSiteTemplateLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SiteTemplateLanguageCollection. When set to true, SiteTemplateLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteTemplateLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSiteTemplateLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteTemplateLanguageCollection
		{
			get	{ return _alwaysFetchSiteTemplateLanguageCollection; }
			set	{ _alwaysFetchSiteTemplateLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteTemplateLanguageCollection already has been fetched. Setting this property to false when SiteTemplateLanguageCollection has been fetched
		/// will clear the SiteTemplateLanguageCollection collection well. Setting this property to true while SiteTemplateLanguageCollection hasn't been fetched disables lazy loading for SiteTemplateLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteTemplateLanguageCollection
		{
			get { return _alreadyFetchedSiteTemplateLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSiteTemplateLanguageCollection && !value && (_siteTemplateLanguageCollection != null))
				{
					_siteTemplateLanguageCollection.Clear();
				}
				_alreadyFetchedSiteTemplateLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StationLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiStationLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.StationLanguageCollection StationLanguageCollection
		{
			get	{ return GetMultiStationLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for StationLanguageCollection. When set to true, StationLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StationLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiStationLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStationLanguageCollection
		{
			get	{ return _alwaysFetchStationLanguageCollection; }
			set	{ _alwaysFetchStationLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property StationLanguageCollection already has been fetched. Setting this property to false when StationLanguageCollection has been fetched
		/// will clear the StationLanguageCollection collection well. Setting this property to true while StationLanguageCollection hasn't been fetched disables lazy loading for StationLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStationLanguageCollection
		{
			get { return _alreadyFetchedStationLanguageCollection;}
			set 
			{
				if(_alreadyFetchedStationLanguageCollection && !value && (_stationLanguageCollection != null))
				{
					_stationLanguageCollection.Clear();
				}
				_alreadyFetchedStationLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyAnswerLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyAnswerLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerLanguageCollection SurveyAnswerLanguageCollection
		{
			get	{ return GetMultiSurveyAnswerLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswerLanguageCollection. When set to true, SurveyAnswerLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswerLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyAnswerLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswerLanguageCollection
		{
			get	{ return _alwaysFetchSurveyAnswerLanguageCollection; }
			set	{ _alwaysFetchSurveyAnswerLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswerLanguageCollection already has been fetched. Setting this property to false when SurveyAnswerLanguageCollection has been fetched
		/// will clear the SurveyAnswerLanguageCollection collection well. Setting this property to true while SurveyAnswerLanguageCollection hasn't been fetched disables lazy loading for SurveyAnswerLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswerLanguageCollection
		{
			get { return _alreadyFetchedSurveyAnswerLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSurveyAnswerLanguageCollection && !value && (_surveyAnswerLanguageCollection != null))
				{
					_surveyAnswerLanguageCollection.Clear();
				}
				_alreadyFetchedSurveyAnswerLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyLanguageCollection SurveyLanguageCollection
		{
			get	{ return GetMultiSurveyLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyLanguageCollection. When set to true, SurveyLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyLanguageCollection
		{
			get	{ return _alwaysFetchSurveyLanguageCollection; }
			set	{ _alwaysFetchSurveyLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyLanguageCollection already has been fetched. Setting this property to false when SurveyLanguageCollection has been fetched
		/// will clear the SurveyLanguageCollection collection well. Setting this property to true while SurveyLanguageCollection hasn't been fetched disables lazy loading for SurveyLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyLanguageCollection
		{
			get { return _alreadyFetchedSurveyLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSurveyLanguageCollection && !value && (_surveyLanguageCollection != null))
				{
					_surveyLanguageCollection.Clear();
				}
				_alreadyFetchedSurveyLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestionLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection SurveyQuestionLanguageCollection
		{
			get	{ return GetMultiSurveyQuestionLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionLanguageCollection. When set to true, SurveyQuestionLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyQuestionLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionLanguageCollection
		{
			get	{ return _alwaysFetchSurveyQuestionLanguageCollection; }
			set	{ _alwaysFetchSurveyQuestionLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionLanguageCollection already has been fetched. Setting this property to false when SurveyQuestionLanguageCollection has been fetched
		/// will clear the SurveyQuestionLanguageCollection collection well. Setting this property to true while SurveyQuestionLanguageCollection hasn't been fetched disables lazy loading for SurveyQuestionLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionLanguageCollection
		{
			get { return _alreadyFetchedSurveyQuestionLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionLanguageCollection && !value && (_surveyQuestionLanguageCollection != null))
				{
					_surveyQuestionLanguageCollection.Clear();
				}
				_alreadyFetchedSurveyQuestionLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIFooterItemLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIFooterItemLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIFooterItemLanguageCollection UIFooterItemLanguageCollection
		{
			get	{ return GetMultiUIFooterItemLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIFooterItemLanguageCollection. When set to true, UIFooterItemLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIFooterItemLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIFooterItemLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIFooterItemLanguageCollection
		{
			get	{ return _alwaysFetchUIFooterItemLanguageCollection; }
			set	{ _alwaysFetchUIFooterItemLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIFooterItemLanguageCollection already has been fetched. Setting this property to false when UIFooterItemLanguageCollection has been fetched
		/// will clear the UIFooterItemLanguageCollection collection well. Setting this property to true while UIFooterItemLanguageCollection hasn't been fetched disables lazy loading for UIFooterItemLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIFooterItemLanguageCollection
		{
			get { return _alreadyFetchedUIFooterItemLanguageCollection;}
			set 
			{
				if(_alreadyFetchedUIFooterItemLanguageCollection && !value && (_uIFooterItemLanguageCollection != null))
				{
					_uIFooterItemLanguageCollection.Clear();
				}
				_alreadyFetchedUIFooterItemLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUITabLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UITabLanguageCollection UITabLanguageCollection
		{
			get	{ return GetMultiUITabLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UITabLanguageCollection. When set to true, UITabLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUITabLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabLanguageCollection
		{
			get	{ return _alwaysFetchUITabLanguageCollection; }
			set	{ _alwaysFetchUITabLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabLanguageCollection already has been fetched. Setting this property to false when UITabLanguageCollection has been fetched
		/// will clear the UITabLanguageCollection collection well. Setting this property to true while UITabLanguageCollection hasn't been fetched disables lazy loading for UITabLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabLanguageCollection
		{
			get { return _alreadyFetchedUITabLanguageCollection;}
			set 
			{
				if(_alreadyFetchedUITabLanguageCollection && !value && (_uITabLanguageCollection != null))
				{
					_uITabLanguageCollection.Clear();
				}
				_alreadyFetchedUITabLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetLanguageCollection UIWidgetLanguageCollection
		{
			get	{ return GetMultiUIWidgetLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetLanguageCollection. When set to true, UIWidgetLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetLanguageCollection
		{
			get	{ return _alwaysFetchUIWidgetLanguageCollection; }
			set	{ _alwaysFetchUIWidgetLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetLanguageCollection already has been fetched. Setting this property to false when UIWidgetLanguageCollection has been fetched
		/// will clear the UIWidgetLanguageCollection collection well. Setting this property to true while UIWidgetLanguageCollection hasn't been fetched disables lazy loading for UIWidgetLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetLanguageCollection
		{
			get { return _alreadyFetchedUIWidgetLanguageCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetLanguageCollection && !value && (_uIWidgetLanguageCollection != null))
				{
					_uIWidgetLanguageCollection.Clear();
				}
				_alreadyFetchedUIWidgetLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VenueCategoryLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVenueCategoryLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.VenueCategoryLanguageCollection VenueCategoryLanguageCollection
		{
			get	{ return GetMultiVenueCategoryLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VenueCategoryLanguageCollection. When set to true, VenueCategoryLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VenueCategoryLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiVenueCategoryLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVenueCategoryLanguageCollection
		{
			get	{ return _alwaysFetchVenueCategoryLanguageCollection; }
			set	{ _alwaysFetchVenueCategoryLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VenueCategoryLanguageCollection already has been fetched. Setting this property to false when VenueCategoryLanguageCollection has been fetched
		/// will clear the VenueCategoryLanguageCollection collection well. Setting this property to true while VenueCategoryLanguageCollection hasn't been fetched disables lazy loading for VenueCategoryLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVenueCategoryLanguageCollection
		{
			get { return _alreadyFetchedVenueCategoryLanguageCollection;}
			set 
			{
				if(_alreadyFetchedVenueCategoryLanguageCollection && !value && (_venueCategoryLanguageCollection != null))
				{
					_venueCategoryLanguageCollection.Clear();
				}
				_alreadyFetchedVenueCategoryLanguageCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage
		{
			get { return GetMultiEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage. When set to true, EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage has been fetched
		/// will clear the EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage collection well. Setting this property to true while EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaEntertainmentcategoryLanguage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage && !value && (_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage != null))
				{
					_entertainmentcategoryCollectionViaEntertainmentcategoryLanguage.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaEntertainmentcategoryLanguage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCompany
		{
			get { return GetMultiRouteCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCompany. When set to true, RouteCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCompany
		{
			get	{ return _alwaysFetchRouteCollectionViaCompany; }
			set	{ _alwaysFetchRouteCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCompany already has been fetched. Setting this property to false when RouteCollectionViaCompany has been fetched
		/// will clear the RouteCollectionViaCompany collection well. Setting this property to true while RouteCollectionViaCompany hasn't been fetched disables lazy loading for RouteCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCompany
		{
			get { return _alreadyFetchedRouteCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCompany && !value && (_routeCollectionViaCompany != null))
				{
					_routeCollectionViaCompany.Clear();
				}
				_alreadyFetchedRouteCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaCompany
		{
			get { return GetMultiSupportpoolCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaCompany. When set to true, SupportpoolCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaCompany
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaCompany; }
			set	{ _alwaysFetchSupportpoolCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaCompany already has been fetched. Setting this property to false when SupportpoolCollectionViaCompany has been fetched
		/// will clear the SupportpoolCollectionViaCompany collection well. Setting this property to true while SupportpoolCollectionViaCompany hasn't been fetched disables lazy loading for SupportpoolCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaCompany
		{
			get { return _alreadyFetchedSupportpoolCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaCompany && !value && (_supportpoolCollectionViaCompany != null))
				{
					_supportpoolCollectionViaCompany.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UITabEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUITabCollectionViaUITabLanguage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UITabCollection UITabCollectionViaUITabLanguage
		{
			get { return GetMultiUITabCollectionViaUITabLanguage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UITabCollectionViaUITabLanguage. When set to true, UITabCollectionViaUITabLanguage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabCollectionViaUITabLanguage is accessed. You can always execute a forced fetch by calling GetMultiUITabCollectionViaUITabLanguage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabCollectionViaUITabLanguage
		{
			get	{ return _alwaysFetchUITabCollectionViaUITabLanguage; }
			set	{ _alwaysFetchUITabCollectionViaUITabLanguage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabCollectionViaUITabLanguage already has been fetched. Setting this property to false when UITabCollectionViaUITabLanguage has been fetched
		/// will clear the UITabCollectionViaUITabLanguage collection well. Setting this property to true while UITabCollectionViaUITabLanguage hasn't been fetched disables lazy loading for UITabCollectionViaUITabLanguage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabCollectionViaUITabLanguage
		{
			get { return _alreadyFetchedUITabCollectionViaUITabLanguage;}
			set 
			{
				if(_alreadyFetchedUITabCollectionViaUITabLanguage && !value && (_uITabCollectionViaUITabLanguage != null))
				{
					_uITabCollectionViaUITabLanguage.Clear();
				}
				_alreadyFetchedUITabCollectionViaUITabLanguage = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.LanguageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
