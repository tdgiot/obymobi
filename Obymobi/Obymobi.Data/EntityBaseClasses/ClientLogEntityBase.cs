﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ClientLog'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ClientLogEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ClientLogEntity"; }
		}
	
		#region Class Member Declarations
		private ClientEntity _clientEntity;
		private bool	_alwaysFetchClientEntity, _alreadyFetchedClientEntity, _clientEntityReturnsNewIfNotFound;
		private ClientLogFileEntity _clientLogFileEntity;
		private bool	_alwaysFetchClientLogFileEntity, _alreadyFetchedClientLogFileEntity, _clientLogFileEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientEntity</summary>
			public static readonly string ClientEntity = "ClientEntity";
			/// <summary>Member name ClientLogFileEntity</summary>
			public static readonly string ClientLogFileEntity = "ClientLogFileEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClientLogEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ClientLogEntityBase() :base("ClientLogEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		protected ClientLogEntityBase(System.Int32 clientLogId):base("ClientLogEntity")
		{
			InitClassFetch(clientLogId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ClientLogEntityBase(System.Int32 clientLogId, IPrefetchPath prefetchPathToUse): base("ClientLogEntity")
		{
			InitClassFetch(clientLogId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="validator">The custom validator object for this ClientLogEntity</param>
		protected ClientLogEntityBase(System.Int32 clientLogId, IValidator validator):base("ClientLogEntity")
		{
			InitClassFetch(clientLogId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientLogEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientEntity = (ClientEntity)info.GetValue("_clientEntity", typeof(ClientEntity));
			if(_clientEntity!=null)
			{
				_clientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientEntityReturnsNewIfNotFound = info.GetBoolean("_clientEntityReturnsNewIfNotFound");
			_alwaysFetchClientEntity = info.GetBoolean("_alwaysFetchClientEntity");
			_alreadyFetchedClientEntity = info.GetBoolean("_alreadyFetchedClientEntity");

			_clientLogFileEntity = (ClientLogFileEntity)info.GetValue("_clientLogFileEntity", typeof(ClientLogFileEntity));
			if(_clientLogFileEntity!=null)
			{
				_clientLogFileEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientLogFileEntityReturnsNewIfNotFound = info.GetBoolean("_clientLogFileEntityReturnsNewIfNotFound");
			_alwaysFetchClientLogFileEntity = info.GetBoolean("_alwaysFetchClientLogFileEntity");
			_alreadyFetchedClientLogFileEntity = info.GetBoolean("_alreadyFetchedClientLogFileEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ClientLogFieldIndex)fieldIndex)
			{
				case ClientLogFieldIndex.ClientId:
					DesetupSyncClientEntity(true, false);
					_alreadyFetchedClientEntity = false;
					break;
				case ClientLogFieldIndex.ClientLogFileId:
					DesetupSyncClientLogFileEntity(true, false);
					_alreadyFetchedClientLogFileEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientEntity = (_clientEntity != null);
			_alreadyFetchedClientLogFileEntity = (_clientLogFileEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientEntity":
					toReturn.Add(Relations.ClientEntityUsingClientId);
					break;
				case "ClientLogFileEntity":
					toReturn.Add(Relations.ClientLogFileEntityUsingClientLogFileId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientEntity", (!this.MarkedForDeletion?_clientEntity:null));
			info.AddValue("_clientEntityReturnsNewIfNotFound", _clientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientEntity", _alwaysFetchClientEntity);
			info.AddValue("_alreadyFetchedClientEntity", _alreadyFetchedClientEntity);
			info.AddValue("_clientLogFileEntity", (!this.MarkedForDeletion?_clientLogFileEntity:null));
			info.AddValue("_clientLogFileEntityReturnsNewIfNotFound", _clientLogFileEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientLogFileEntity", _alwaysFetchClientLogFileEntity);
			info.AddValue("_alreadyFetchedClientLogFileEntity", _alreadyFetchedClientLogFileEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientEntity":
					_alreadyFetchedClientEntity = true;
					this.ClientEntity = (ClientEntity)entity;
					break;
				case "ClientLogFileEntity":
					_alreadyFetchedClientLogFileEntity = true;
					this.ClientLogFileEntity = (ClientLogFileEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					SetupSyncClientEntity(relatedEntity);
					break;
				case "ClientLogFileEntity":
					SetupSyncClientLogFileEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					DesetupSyncClientEntity(false, true);
					break;
				case "ClientLogFileEntity":
					DesetupSyncClientLogFileEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_clientEntity!=null)
			{
				toReturn.Add(_clientEntity);
			}
			if(_clientLogFileEntity!=null)
			{
				toReturn.Add(_clientLogFileEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogId)
		{
			return FetchUsingPK(clientLogId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(clientLogId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(clientLogId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 clientLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(clientLogId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ClientLogId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClientLogRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientEntity()
		{
			return GetSingleClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientEntity || forceFetch || _alwaysFetchClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientEntity = newEntity;
				_alreadyFetchedClientEntity = fetchResult;
			}
			return _clientEntity;
		}


		/// <summary> Retrieves the related entity of type 'ClientLogFileEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientLogFileEntity' which is related to this entity.</returns>
		public ClientLogFileEntity GetSingleClientLogFileEntity()
		{
			return GetSingleClientLogFileEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientLogFileEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientLogFileEntity' which is related to this entity.</returns>
		public virtual ClientLogFileEntity GetSingleClientLogFileEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientLogFileEntity || forceFetch || _alwaysFetchClientLogFileEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientLogFileEntityUsingClientLogFileId);
				ClientLogFileEntity newEntity = new ClientLogFileEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientLogFileId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientLogFileEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientLogFileEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientLogFileEntity = newEntity;
				_alreadyFetchedClientLogFileEntity = fetchResult;
			}
			return _clientLogFileEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientEntity", _clientEntity);
			toReturn.Add("ClientLogFileEntity", _clientLogFileEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="validator">The validator object for this ClientLogEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 clientLogId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(clientLogId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_clientEntityReturnsNewIfNotFound = true;
			_clientLogFileEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientLogId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromOperationModeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromStatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToOperationModeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToStatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientLogFileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _clientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticClientLogRelations.ClientEntityUsingClientIdStatic, true, signalRelatedEntity, "ClientLogCollection", resetFKFields, new int[] { (int)ClientLogFieldIndex.ClientId } );		
			_clientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientEntity(IEntityCore relatedEntity)
		{
			if(_clientEntity!=relatedEntity)
			{		
				DesetupSyncClientEntity(true, true);
				_clientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticClientLogRelations.ClientEntityUsingClientIdStatic, true, ref _alreadyFetchedClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientLogFileEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientLogFileEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientLogFileEntity, new PropertyChangedEventHandler( OnClientLogFileEntityPropertyChanged ), "ClientLogFileEntity", Obymobi.Data.RelationClasses.StaticClientLogRelations.ClientLogFileEntityUsingClientLogFileIdStatic, true, signalRelatedEntity, "ClientLogCollection", resetFKFields, new int[] { (int)ClientLogFieldIndex.ClientLogFileId } );		
			_clientLogFileEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientLogFileEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientLogFileEntity(IEntityCore relatedEntity)
		{
			if(_clientLogFileEntity!=relatedEntity)
			{		
				DesetupSyncClientLogFileEntity(true, true);
				_clientLogFileEntity = (ClientLogFileEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientLogFileEntity, new PropertyChangedEventHandler( OnClientLogFileEntityPropertyChanged ), "ClientLogFileEntity", Obymobi.Data.RelationClasses.StaticClientLogRelations.ClientLogFileEntityUsingClientLogFileIdStatic, true, ref _alreadyFetchedClientLogFileEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientLogFileEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 clientLogId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ClientLogFieldIndex.ClientLogId].ForcedCurrentValueWrite(clientLogId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateClientLogDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ClientLogEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClientLogRelations Relations
		{
			get	{ return new ClientLogRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientEntity")[0], (int)Obymobi.Data.EntityType.ClientLogEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientLogFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientLogFileEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientLogFileCollection(), (IEntityRelation)GetRelationsForField("ClientLogFileEntity")[0], (int)Obymobi.Data.EntityType.ClientLogEntity, (int)Obymobi.Data.EntityType.ClientLogFileEntity, 0, null, null, null, "ClientLogFileEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientLogId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ClientLogId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ClientLogId
		{
			get { return (System.Int32)GetValue((int)ClientLogFieldIndex.ClientLogId, true); }
			set	{ SetValue((int)ClientLogFieldIndex.ClientLogId, value, true); }
		}

		/// <summary> The ClientId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ClientId, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ClientId, value, true); }
		}

		/// <summary> The Type property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)ClientLogFieldIndex.Type, true); }
			set	{ SetValue((int)ClientLogFieldIndex.Type, value, true); }
		}

		/// <summary> The Message property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."Message"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.Message, true); }
			set	{ SetValue((int)ClientLogFieldIndex.Message, value, true); }
		}

		/// <summary> The FromStatus property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FromStatus
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.FromStatus, false); }
			set	{ SetValue((int)ClientLogFieldIndex.FromStatus, value, true); }
		}

		/// <summary> The ToStatus property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ToStatus
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ToStatus, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ToStatus, value, true); }
		}

		/// <summary> The FromOperationMode property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FromOperationMode
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.FromOperationMode, false); }
			set	{ SetValue((int)ClientLogFieldIndex.FromOperationMode, value, true); }
		}

		/// <summary> The ToOperationMode property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ToOperationMode
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ToOperationMode, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ToOperationMode, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ClientLogFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ClientLogFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ClientLogFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ClientLogFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The FromOperationModeText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromOperationModeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FromOperationModeText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.FromOperationModeText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.FromOperationModeText, value, true); }
		}

		/// <summary> The FromStatusText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."FromStatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FromStatusText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.FromStatusText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.FromStatusText, value, true); }
		}

		/// <summary> The ToOperationModeText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToOperationModeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ToOperationModeText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.ToOperationModeText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.ToOperationModeText, value, true); }
		}

		/// <summary> The ToStatusText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ToStatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ToStatusText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.ToStatusText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.ToStatusText, value, true); }
		}

		/// <summary> The TypeText property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."TypeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TypeText
		{
			get { return (System.String)GetValue((int)ClientLogFieldIndex.TypeText, true); }
			set	{ SetValue((int)ClientLogFieldIndex.TypeText, value, true); }
		}

		/// <summary> The ClientLogFileId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ClientLogFileId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientLogFileId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ClientLogFileId, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ClientLogFileId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ClientLogFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ClientLogFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientLogFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ClientLogFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ClientLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ClientLog"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClientLogFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ClientLogFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ClientEntity
		{
			get	{ return GetSingleClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientLogCollection", "ClientEntity", _clientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntity. When set to true, ClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntity is accessed. You can always execute a forced fetch by calling GetSingleClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntity
		{
			get	{ return _alwaysFetchClientEntity; }
			set	{ _alwaysFetchClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntity already has been fetched. Setting this property to false when ClientEntity has been fetched
		/// will set ClientEntity to null as well. Setting this property to true while ClientEntity hasn't been fetched disables lazy loading for ClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntity
		{
			get { return _alreadyFetchedClientEntity;}
			set 
			{
				if(_alreadyFetchedClientEntity && !value)
				{
					this.ClientEntity = null;
				}
				_alreadyFetchedClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientEntity is not found
		/// in the database. When set to true, ClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientEntityReturnsNewIfNotFound
		{
			get	{ return _clientEntityReturnsNewIfNotFound; }
			set { _clientEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientLogFileEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientLogFileEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientLogFileEntity ClientLogFileEntity
		{
			get	{ return GetSingleClientLogFileEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientLogFileEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientLogCollection", "ClientLogFileEntity", _clientLogFileEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientLogFileEntity. When set to true, ClientLogFileEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientLogFileEntity is accessed. You can always execute a forced fetch by calling GetSingleClientLogFileEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientLogFileEntity
		{
			get	{ return _alwaysFetchClientLogFileEntity; }
			set	{ _alwaysFetchClientLogFileEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientLogFileEntity already has been fetched. Setting this property to false when ClientLogFileEntity has been fetched
		/// will set ClientLogFileEntity to null as well. Setting this property to true while ClientLogFileEntity hasn't been fetched disables lazy loading for ClientLogFileEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientLogFileEntity
		{
			get { return _alreadyFetchedClientLogFileEntity;}
			set 
			{
				if(_alreadyFetchedClientLogFileEntity && !value)
				{
					this.ClientLogFileEntity = null;
				}
				_alreadyFetchedClientLogFileEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientLogFileEntity is not found
		/// in the database. When set to true, ClientLogFileEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientLogFileEntityReturnsNewIfNotFound
		{
			get	{ return _clientLogFileEntityReturnsNewIfNotFound; }
			set { _clientLogFileEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ClientLogEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
