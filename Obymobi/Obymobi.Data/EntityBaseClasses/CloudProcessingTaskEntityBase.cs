﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'CloudProcessingTask'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CloudProcessingTaskEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CloudProcessingTaskEntity"; }
		}
	
		#region Class Member Declarations
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private EntertainmentFileEntity _entertainmentFileEntity;
		private bool	_alwaysFetchEntertainmentFileEntity, _alreadyFetchedEntertainmentFileEntity, _entertainmentFileEntityReturnsNewIfNotFound;
		private MediaRatioTypeMediaEntity _mediaRatioTypeMediaEntity;
		private bool	_alwaysFetchMediaRatioTypeMediaEntity, _alreadyFetchedMediaRatioTypeMediaEntity, _mediaRatioTypeMediaEntityReturnsNewIfNotFound;
		private ReleaseEntity _releaseEntity;
		private bool	_alwaysFetchReleaseEntity, _alreadyFetchedReleaseEntity, _releaseEntityReturnsNewIfNotFound;
		private WeatherEntity _weatherEntity;
		private bool	_alwaysFetchWeatherEntity, _alreadyFetchedWeatherEntity, _weatherEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name EntertainmentFileEntity</summary>
			public static readonly string EntertainmentFileEntity = "EntertainmentFileEntity";
			/// <summary>Member name MediaRatioTypeMediaEntity</summary>
			public static readonly string MediaRatioTypeMediaEntity = "MediaRatioTypeMediaEntity";
			/// <summary>Member name ReleaseEntity</summary>
			public static readonly string ReleaseEntity = "ReleaseEntity";
			/// <summary>Member name WeatherEntity</summary>
			public static readonly string WeatherEntity = "WeatherEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CloudProcessingTaskEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CloudProcessingTaskEntityBase() :base("CloudProcessingTaskEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		protected CloudProcessingTaskEntityBase(System.Int32 cloudProcessingTaskId):base("CloudProcessingTaskEntity")
		{
			InitClassFetch(cloudProcessingTaskId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CloudProcessingTaskEntityBase(System.Int32 cloudProcessingTaskId, IPrefetchPath prefetchPathToUse): base("CloudProcessingTaskEntity")
		{
			InitClassFetch(cloudProcessingTaskId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="validator">The custom validator object for this CloudProcessingTaskEntity</param>
		protected CloudProcessingTaskEntityBase(System.Int32 cloudProcessingTaskId, IValidator validator):base("CloudProcessingTaskEntity")
		{
			InitClassFetch(cloudProcessingTaskId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CloudProcessingTaskEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_entertainmentFileEntity = (EntertainmentFileEntity)info.GetValue("_entertainmentFileEntity", typeof(EntertainmentFileEntity));
			if(_entertainmentFileEntity!=null)
			{
				_entertainmentFileEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentFileEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentFileEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentFileEntity = info.GetBoolean("_alwaysFetchEntertainmentFileEntity");
			_alreadyFetchedEntertainmentFileEntity = info.GetBoolean("_alreadyFetchedEntertainmentFileEntity");

			_mediaRatioTypeMediaEntity = (MediaRatioTypeMediaEntity)info.GetValue("_mediaRatioTypeMediaEntity", typeof(MediaRatioTypeMediaEntity));
			if(_mediaRatioTypeMediaEntity!=null)
			{
				_mediaRatioTypeMediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaRatioTypeMediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaRatioTypeMediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaRatioTypeMediaEntity = info.GetBoolean("_alwaysFetchMediaRatioTypeMediaEntity");
			_alreadyFetchedMediaRatioTypeMediaEntity = info.GetBoolean("_alreadyFetchedMediaRatioTypeMediaEntity");

			_releaseEntity = (ReleaseEntity)info.GetValue("_releaseEntity", typeof(ReleaseEntity));
			if(_releaseEntity!=null)
			{
				_releaseEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_releaseEntityReturnsNewIfNotFound = info.GetBoolean("_releaseEntityReturnsNewIfNotFound");
			_alwaysFetchReleaseEntity = info.GetBoolean("_alwaysFetchReleaseEntity");
			_alreadyFetchedReleaseEntity = info.GetBoolean("_alreadyFetchedReleaseEntity");

			_weatherEntity = (WeatherEntity)info.GetValue("_weatherEntity", typeof(WeatherEntity));
			if(_weatherEntity!=null)
			{
				_weatherEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_weatherEntityReturnsNewIfNotFound = info.GetBoolean("_weatherEntityReturnsNewIfNotFound");
			_alwaysFetchWeatherEntity = info.GetBoolean("_alwaysFetchWeatherEntity");
			_alreadyFetchedWeatherEntity = info.GetBoolean("_alreadyFetchedWeatherEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CloudProcessingTaskFieldIndex)fieldIndex)
			{
				case CloudProcessingTaskFieldIndex.EntertainmentFileId:
					DesetupSyncEntertainmentFileEntity(true, false);
					_alreadyFetchedEntertainmentFileEntity = false;
					break;
				case CloudProcessingTaskFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case CloudProcessingTaskFieldIndex.WeatherId:
					DesetupSyncWeatherEntity(true, false);
					_alreadyFetchedWeatherEntity = false;
					break;
				case CloudProcessingTaskFieldIndex.ReleaseId:
					DesetupSyncReleaseEntity(true, false);
					_alreadyFetchedReleaseEntity = false;
					break;
				case CloudProcessingTaskFieldIndex.MediaRatioTypeMediaId:
					DesetupSyncMediaRatioTypeMediaEntity(true, false);
					_alreadyFetchedMediaRatioTypeMediaEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedEntertainmentFileEntity = (_entertainmentFileEntity != null);
			_alreadyFetchedMediaRatioTypeMediaEntity = (_mediaRatioTypeMediaEntity != null);
			_alreadyFetchedReleaseEntity = (_releaseEntity != null);
			_alreadyFetchedWeatherEntity = (_weatherEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				case "EntertainmentFileEntity":
					toReturn.Add(Relations.EntertainmentFileEntityUsingEntertainmentFileId);
					break;
				case "MediaRatioTypeMediaEntity":
					toReturn.Add(Relations.MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId);
					break;
				case "ReleaseEntity":
					toReturn.Add(Relations.ReleaseEntityUsingReleaseId);
					break;
				case "WeatherEntity":
					toReturn.Add(Relations.WeatherEntityUsingWeatherId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_entertainmentFileEntity", (!this.MarkedForDeletion?_entertainmentFileEntity:null));
			info.AddValue("_entertainmentFileEntityReturnsNewIfNotFound", _entertainmentFileEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentFileEntity", _alwaysFetchEntertainmentFileEntity);
			info.AddValue("_alreadyFetchedEntertainmentFileEntity", _alreadyFetchedEntertainmentFileEntity);
			info.AddValue("_mediaRatioTypeMediaEntity", (!this.MarkedForDeletion?_mediaRatioTypeMediaEntity:null));
			info.AddValue("_mediaRatioTypeMediaEntityReturnsNewIfNotFound", _mediaRatioTypeMediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaRatioTypeMediaEntity", _alwaysFetchMediaRatioTypeMediaEntity);
			info.AddValue("_alreadyFetchedMediaRatioTypeMediaEntity", _alreadyFetchedMediaRatioTypeMediaEntity);
			info.AddValue("_releaseEntity", (!this.MarkedForDeletion?_releaseEntity:null));
			info.AddValue("_releaseEntityReturnsNewIfNotFound", _releaseEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReleaseEntity", _alwaysFetchReleaseEntity);
			info.AddValue("_alreadyFetchedReleaseEntity", _alreadyFetchedReleaseEntity);
			info.AddValue("_weatherEntity", (!this.MarkedForDeletion?_weatherEntity:null));
			info.AddValue("_weatherEntityReturnsNewIfNotFound", _weatherEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWeatherEntity", _alwaysFetchWeatherEntity);
			info.AddValue("_alreadyFetchedWeatherEntity", _alreadyFetchedWeatherEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "EntertainmentFileEntity":
					_alreadyFetchedEntertainmentFileEntity = true;
					this.EntertainmentFileEntity = (EntertainmentFileEntity)entity;
					break;
				case "MediaRatioTypeMediaEntity":
					_alreadyFetchedMediaRatioTypeMediaEntity = true;
					this.MediaRatioTypeMediaEntity = (MediaRatioTypeMediaEntity)entity;
					break;
				case "ReleaseEntity":
					_alreadyFetchedReleaseEntity = true;
					this.ReleaseEntity = (ReleaseEntity)entity;
					break;
				case "WeatherEntity":
					_alreadyFetchedWeatherEntity = true;
					this.WeatherEntity = (WeatherEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "EntertainmentFileEntity":
					SetupSyncEntertainmentFileEntity(relatedEntity);
					break;
				case "MediaRatioTypeMediaEntity":
					SetupSyncMediaRatioTypeMediaEntity(relatedEntity);
					break;
				case "ReleaseEntity":
					SetupSyncReleaseEntity(relatedEntity);
					break;
				case "WeatherEntity":
					SetupSyncWeatherEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "EntertainmentFileEntity":
					DesetupSyncEntertainmentFileEntity(false, true);
					break;
				case "MediaRatioTypeMediaEntity":
					DesetupSyncMediaRatioTypeMediaEntity(false, true);
					break;
				case "ReleaseEntity":
					DesetupSyncReleaseEntity(false, true);
					break;
				case "WeatherEntity":
					DesetupSyncWeatherEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_entertainmentFileEntity!=null)
			{
				toReturn.Add(_entertainmentFileEntity);
			}
			if(_mediaRatioTypeMediaEntity!=null)
			{
				toReturn.Add(_mediaRatioTypeMediaEntity);
			}
			if(_releaseEntity!=null)
			{
				toReturn.Add(_releaseEntity);
			}
			if(_weatherEntity!=null)
			{
				toReturn.Add(_weatherEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 cloudProcessingTaskId)
		{
			return FetchUsingPK(cloudProcessingTaskId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 cloudProcessingTaskId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cloudProcessingTaskId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 cloudProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cloudProcessingTaskId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 cloudProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cloudProcessingTaskId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CloudProcessingTaskId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CloudProcessingTaskRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentFileEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentFileEntity' which is related to this entity.</returns>
		public EntertainmentFileEntity GetSingleEntertainmentFileEntity()
		{
			return GetSingleEntertainmentFileEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentFileEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentFileEntity' which is related to this entity.</returns>
		public virtual EntertainmentFileEntity GetSingleEntertainmentFileEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentFileEntity || forceFetch || _alwaysFetchEntertainmentFileEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentFileEntityUsingEntertainmentFileId);
				EntertainmentFileEntity newEntity = new EntertainmentFileEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentFileId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentFileEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentFileEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentFileEntity = newEntity;
				_alreadyFetchedEntertainmentFileEntity = fetchResult;
			}
			return _entertainmentFileEntity;
		}


		/// <summary> Retrieves the related entity of type 'MediaRatioTypeMediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaRatioTypeMediaEntity' which is related to this entity.</returns>
		public MediaRatioTypeMediaEntity GetSingleMediaRatioTypeMediaEntity()
		{
			return GetSingleMediaRatioTypeMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaRatioTypeMediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaRatioTypeMediaEntity' which is related to this entity.</returns>
		public virtual MediaRatioTypeMediaEntity GetSingleMediaRatioTypeMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaRatioTypeMediaEntity || forceFetch || _alwaysFetchMediaRatioTypeMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId);
				MediaRatioTypeMediaEntity newEntity = new MediaRatioTypeMediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MediaRatioTypeMediaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MediaRatioTypeMediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaRatioTypeMediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaRatioTypeMediaEntity = newEntity;
				_alreadyFetchedMediaRatioTypeMediaEntity = fetchResult;
			}
			return _mediaRatioTypeMediaEntity;
		}


		/// <summary> Retrieves the related entity of type 'ReleaseEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReleaseEntity' which is related to this entity.</returns>
		public ReleaseEntity GetSingleReleaseEntity()
		{
			return GetSingleReleaseEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReleaseEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReleaseEntity' which is related to this entity.</returns>
		public virtual ReleaseEntity GetSingleReleaseEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReleaseEntity || forceFetch || _alwaysFetchReleaseEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReleaseEntityUsingReleaseId);
				ReleaseEntity newEntity = new ReleaseEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReleaseId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReleaseEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_releaseEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReleaseEntity = newEntity;
				_alreadyFetchedReleaseEntity = fetchResult;
			}
			return _releaseEntity;
		}


		/// <summary> Retrieves the related entity of type 'WeatherEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WeatherEntity' which is related to this entity.</returns>
		public WeatherEntity GetSingleWeatherEntity()
		{
			return GetSingleWeatherEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WeatherEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WeatherEntity' which is related to this entity.</returns>
		public virtual WeatherEntity GetSingleWeatherEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWeatherEntity || forceFetch || _alwaysFetchWeatherEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WeatherEntityUsingWeatherId);
				WeatherEntity newEntity = new WeatherEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.WeatherId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (WeatherEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_weatherEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WeatherEntity = newEntity;
				_alreadyFetchedWeatherEntity = fetchResult;
			}
			return _weatherEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("EntertainmentFileEntity", _entertainmentFileEntity);
			toReturn.Add("MediaRatioTypeMediaEntity", _mediaRatioTypeMediaEntity);
			toReturn.Add("ReleaseEntity", _releaseEntity);
			toReturn.Add("WeatherEntity", _weatherEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="validator">The validator object for this CloudProcessingTaskEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 cloudProcessingTaskId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cloudProcessingTaskId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_entertainmentEntityReturnsNewIfNotFound = true;
			_entertainmentFileEntityReturnsNewIfNotFound = true;
			_mediaRatioTypeMediaEntityReturnsNewIfNotFound = true;
			_releaseEntityReturnsNewIfNotFound = true;
			_weatherEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloudProcessingTaskId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PathFormat", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Action", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Errors", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Attempts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentFileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Container", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WeatherId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastAttemptUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NextAttemptUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletedOnAmazonUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Priority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaRatioTypeMediaIdNonRelationCopy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelatedToCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaRatioTypeMediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QueuedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileContent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileContentTimestamp", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileContentTimestampKey", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "CloudProcessingTaskCollection", resetFKFields, new int[] { (int)CloudProcessingTaskFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentFileEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentFileEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentFileEntity, new PropertyChangedEventHandler( OnEntertainmentFileEntityPropertyChanged ), "EntertainmentFileEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.EntertainmentFileEntityUsingEntertainmentFileIdStatic, true, signalRelatedEntity, "CloudProcessingTaskCollection", resetFKFields, new int[] { (int)CloudProcessingTaskFieldIndex.EntertainmentFileId } );		
			_entertainmentFileEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentFileEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentFileEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentFileEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentFileEntity(true, true);
				_entertainmentFileEntity = (EntertainmentFileEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentFileEntity, new PropertyChangedEventHandler( OnEntertainmentFileEntityPropertyChanged ), "EntertainmentFileEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.EntertainmentFileEntityUsingEntertainmentFileIdStatic, true, ref _alreadyFetchedEntertainmentFileEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentFileEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mediaRatioTypeMediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaRatioTypeMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaRatioTypeMediaEntity, new PropertyChangedEventHandler( OnMediaRatioTypeMediaEntityPropertyChanged ), "MediaRatioTypeMediaEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaIdStatic, true, signalRelatedEntity, "CloudProcessingTaskCollection", resetFKFields, new int[] { (int)CloudProcessingTaskFieldIndex.MediaRatioTypeMediaId } );		
			_mediaRatioTypeMediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaRatioTypeMediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaRatioTypeMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaRatioTypeMediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaRatioTypeMediaEntity(true, true);
				_mediaRatioTypeMediaEntity = (MediaRatioTypeMediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaRatioTypeMediaEntity, new PropertyChangedEventHandler( OnMediaRatioTypeMediaEntityPropertyChanged ), "MediaRatioTypeMediaEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaIdStatic, true, ref _alreadyFetchedMediaRatioTypeMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaRatioTypeMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _releaseEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReleaseEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _releaseEntity, new PropertyChangedEventHandler( OnReleaseEntityPropertyChanged ), "ReleaseEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.ReleaseEntityUsingReleaseIdStatic, true, signalRelatedEntity, "CloudProcessingTaskCollection", resetFKFields, new int[] { (int)CloudProcessingTaskFieldIndex.ReleaseId } );		
			_releaseEntity = null;
		}
		
		/// <summary> setups the sync logic for member _releaseEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReleaseEntity(IEntityCore relatedEntity)
		{
			if(_releaseEntity!=relatedEntity)
			{		
				DesetupSyncReleaseEntity(true, true);
				_releaseEntity = (ReleaseEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _releaseEntity, new PropertyChangedEventHandler( OnReleaseEntityPropertyChanged ), "ReleaseEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.ReleaseEntityUsingReleaseIdStatic, true, ref _alreadyFetchedReleaseEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReleaseEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _weatherEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWeatherEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _weatherEntity, new PropertyChangedEventHandler( OnWeatherEntityPropertyChanged ), "WeatherEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.WeatherEntityUsingWeatherIdStatic, true, signalRelatedEntity, "CloudProcessingTaskCollection", resetFKFields, new int[] { (int)CloudProcessingTaskFieldIndex.WeatherId } );		
			_weatherEntity = null;
		}
		
		/// <summary> setups the sync logic for member _weatherEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWeatherEntity(IEntityCore relatedEntity)
		{
			if(_weatherEntity!=relatedEntity)
			{		
				DesetupSyncWeatherEntity(true, true);
				_weatherEntity = (WeatherEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _weatherEntity, new PropertyChangedEventHandler( OnWeatherEntityPropertyChanged ), "WeatherEntity", Obymobi.Data.RelationClasses.StaticCloudProcessingTaskRelations.WeatherEntityUsingWeatherIdStatic, true, ref _alreadyFetchedWeatherEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWeatherEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 cloudProcessingTaskId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CloudProcessingTaskFieldIndex.CloudProcessingTaskId].ForcedCurrentValueWrite(cloudProcessingTaskId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCloudProcessingTaskDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CloudProcessingTaskEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CloudProcessingTaskRelations Relations
		{
			get	{ return new CloudProcessingTaskRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntertainmentFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentFileEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentFileCollection(), (IEntityRelation)GetRelationsForField("EntertainmentFileEntity")[0], (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, (int)Obymobi.Data.EntityType.EntertainmentFileEntity, 0, null, null, null, "EntertainmentFileEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaRatioTypeMedia'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaRatioTypeMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection(), (IEntityRelation)GetRelationsForField("MediaRatioTypeMediaEntity")[0], (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, (int)Obymobi.Data.EntityType.MediaRatioTypeMediaEntity, 0, null, null, null, "MediaRatioTypeMediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Release'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReleaseEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReleaseCollection(), (IEntityRelation)GetRelationsForField("ReleaseEntity")[0], (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, (int)Obymobi.Data.EntityType.ReleaseEntity, 0, null, null, null, "ReleaseEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Weather'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWeatherEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WeatherCollection(), (IEntityRelation)GetRelationsForField("WeatherEntity")[0], (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, (int)Obymobi.Data.EntityType.WeatherEntity, 0, null, null, null, "WeatherEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CloudProcessingTaskId property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."CloudProcessingTaskId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CloudProcessingTaskId
		{
			get { return (System.Int32)GetValue((int)CloudProcessingTaskFieldIndex.CloudProcessingTaskId, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.CloudProcessingTaskId, value, true); }
		}

		/// <summary> The Type property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)CloudProcessingTaskFieldIndex.Type, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.Type, value, true); }
		}

		/// <summary> The PathFormat property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."PathFormat"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 260<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PathFormat
		{
			get { return (System.String)GetValue((int)CloudProcessingTaskFieldIndex.PathFormat, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.PathFormat, value, true); }
		}

		/// <summary> The Action property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."Action"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Action
		{
			get { return (System.Int32)GetValue((int)CloudProcessingTaskFieldIndex.Action, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.Action, value, true); }
		}

		/// <summary> The Errors property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."Errors"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Errors
		{
			get { return (System.String)GetValue((int)CloudProcessingTaskFieldIndex.Errors, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.Errors, value, true); }
		}

		/// <summary> The Attempts property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."Attempts"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Attempts
		{
			get { return (System.Int32)GetValue((int)CloudProcessingTaskFieldIndex.Attempts, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.Attempts, value, true); }
		}

		/// <summary> The EntertainmentFileId property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."EntertainmentFileId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentFileId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CloudProcessingTaskFieldIndex.EntertainmentFileId, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.EntertainmentFileId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CloudProcessingTaskFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CloudProcessingTaskFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Container property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."Container"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Container
		{
			get { return (System.String)GetValue((int)CloudProcessingTaskFieldIndex.Container, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.Container, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CloudProcessingTaskFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The WeatherId property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."WeatherId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WeatherId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CloudProcessingTaskFieldIndex.WeatherId, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.WeatherId, value, true); }
		}

		/// <summary> The ReleaseId property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."ReleaseId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReleaseId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CloudProcessingTaskFieldIndex.ReleaseId, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.ReleaseId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloudProcessingTaskFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloudProcessingTaskFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LastAttemptUTC property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."LastAttemptUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastAttemptUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloudProcessingTaskFieldIndex.LastAttemptUTC, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.LastAttemptUTC, value, true); }
		}

		/// <summary> The NextAttemptUTC property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."NextAttemptUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> NextAttemptUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloudProcessingTaskFieldIndex.NextAttemptUTC, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.NextAttemptUTC, value, true); }
		}

		/// <summary> The CompletedOnAmazonUTC property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."CompletedOnAmazonUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CompletedOnAmazonUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloudProcessingTaskFieldIndex.CompletedOnAmazonUTC, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.CompletedOnAmazonUTC, value, true); }
		}

		/// <summary> The Priority property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."Priority"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Priority
		{
			get { return (System.Int32)GetValue((int)CloudProcessingTaskFieldIndex.Priority, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.Priority, value, true); }
		}

		/// <summary> The MediaRatioTypeMediaIdNonRelationCopy property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."MediaRatioTypeMediaIdNonRelationCopy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaRatioTypeMediaIdNonRelationCopy
		{
			get { return (Nullable<System.Int32>)GetValue((int)CloudProcessingTaskFieldIndex.MediaRatioTypeMediaIdNonRelationCopy, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.MediaRatioTypeMediaIdNonRelationCopy, value, true); }
		}

		/// <summary> The RelatedToCompanyId property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."RelatedToCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RelatedToCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CloudProcessingTaskFieldIndex.RelatedToCompanyId, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.RelatedToCompanyId, value, true); }
		}

		/// <summary> The MediaRatioTypeMediaId property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."MediaRatioTypeMediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaRatioTypeMediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CloudProcessingTaskFieldIndex.MediaRatioTypeMediaId, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.MediaRatioTypeMediaId, value, true); }
		}

		/// <summary> The QueuedUTC property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."QueuedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> QueuedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloudProcessingTaskFieldIndex.QueuedUTC, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.QueuedUTC, value, true); }
		}

		/// <summary> The FileContent property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."FileContent"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FileContent
		{
			get { return (System.String)GetValue((int)CloudProcessingTaskFieldIndex.FileContent, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.FileContent, value, true); }
		}

		/// <summary> The FileContentTimestamp property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."FileContentTimestamp"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FileContentTimestamp
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloudProcessingTaskFieldIndex.FileContentTimestamp, false); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.FileContentTimestamp, value, true); }
		}

		/// <summary> The FileContentTimestampKey property of the Entity CloudProcessingTask<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CloudProcessingTask"."FileContentTimestampKey"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FileContentTimestampKey
		{
			get { return (System.String)GetValue((int)CloudProcessingTaskFieldIndex.FileContentTimestampKey, true); }
			set	{ SetValue((int)CloudProcessingTaskFieldIndex.FileContentTimestampKey, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CloudProcessingTaskCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentFileEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentFileEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentFileEntity EntertainmentFileEntity
		{
			get	{ return GetSingleEntertainmentFileEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentFileEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CloudProcessingTaskCollection", "EntertainmentFileEntity", _entertainmentFileEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentFileEntity. When set to true, EntertainmentFileEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentFileEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentFileEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentFileEntity
		{
			get	{ return _alwaysFetchEntertainmentFileEntity; }
			set	{ _alwaysFetchEntertainmentFileEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentFileEntity already has been fetched. Setting this property to false when EntertainmentFileEntity has been fetched
		/// will set EntertainmentFileEntity to null as well. Setting this property to true while EntertainmentFileEntity hasn't been fetched disables lazy loading for EntertainmentFileEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentFileEntity
		{
			get { return _alreadyFetchedEntertainmentFileEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentFileEntity && !value)
				{
					this.EntertainmentFileEntity = null;
				}
				_alreadyFetchedEntertainmentFileEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentFileEntity is not found
		/// in the database. When set to true, EntertainmentFileEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentFileEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentFileEntityReturnsNewIfNotFound; }
			set { _entertainmentFileEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MediaRatioTypeMediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaRatioTypeMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaRatioTypeMediaEntity MediaRatioTypeMediaEntity
		{
			get	{ return GetSingleMediaRatioTypeMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaRatioTypeMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CloudProcessingTaskCollection", "MediaRatioTypeMediaEntity", _mediaRatioTypeMediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaRatioTypeMediaEntity. When set to true, MediaRatioTypeMediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaRatioTypeMediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaRatioTypeMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaRatioTypeMediaEntity
		{
			get	{ return _alwaysFetchMediaRatioTypeMediaEntity; }
			set	{ _alwaysFetchMediaRatioTypeMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaRatioTypeMediaEntity already has been fetched. Setting this property to false when MediaRatioTypeMediaEntity has been fetched
		/// will set MediaRatioTypeMediaEntity to null as well. Setting this property to true while MediaRatioTypeMediaEntity hasn't been fetched disables lazy loading for MediaRatioTypeMediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaRatioTypeMediaEntity
		{
			get { return _alreadyFetchedMediaRatioTypeMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaRatioTypeMediaEntity && !value)
				{
					this.MediaRatioTypeMediaEntity = null;
				}
				_alreadyFetchedMediaRatioTypeMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaRatioTypeMediaEntity is not found
		/// in the database. When set to true, MediaRatioTypeMediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaRatioTypeMediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaRatioTypeMediaEntityReturnsNewIfNotFound; }
			set { _mediaRatioTypeMediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReleaseEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReleaseEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReleaseEntity ReleaseEntity
		{
			get	{ return GetSingleReleaseEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReleaseEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CloudProcessingTaskCollection", "ReleaseEntity", _releaseEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReleaseEntity. When set to true, ReleaseEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReleaseEntity is accessed. You can always execute a forced fetch by calling GetSingleReleaseEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReleaseEntity
		{
			get	{ return _alwaysFetchReleaseEntity; }
			set	{ _alwaysFetchReleaseEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReleaseEntity already has been fetched. Setting this property to false when ReleaseEntity has been fetched
		/// will set ReleaseEntity to null as well. Setting this property to true while ReleaseEntity hasn't been fetched disables lazy loading for ReleaseEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReleaseEntity
		{
			get { return _alreadyFetchedReleaseEntity;}
			set 
			{
				if(_alreadyFetchedReleaseEntity && !value)
				{
					this.ReleaseEntity = null;
				}
				_alreadyFetchedReleaseEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReleaseEntity is not found
		/// in the database. When set to true, ReleaseEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReleaseEntityReturnsNewIfNotFound
		{
			get	{ return _releaseEntityReturnsNewIfNotFound; }
			set { _releaseEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WeatherEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWeatherEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WeatherEntity WeatherEntity
		{
			get	{ return GetSingleWeatherEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWeatherEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CloudProcessingTaskCollection", "WeatherEntity", _weatherEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WeatherEntity. When set to true, WeatherEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WeatherEntity is accessed. You can always execute a forced fetch by calling GetSingleWeatherEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWeatherEntity
		{
			get	{ return _alwaysFetchWeatherEntity; }
			set	{ _alwaysFetchWeatherEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WeatherEntity already has been fetched. Setting this property to false when WeatherEntity has been fetched
		/// will set WeatherEntity to null as well. Setting this property to true while WeatherEntity hasn't been fetched disables lazy loading for WeatherEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWeatherEntity
		{
			get { return _alreadyFetchedWeatherEntity;}
			set 
			{
				if(_alreadyFetchedWeatherEntity && !value)
				{
					this.WeatherEntity = null;
				}
				_alreadyFetchedWeatherEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WeatherEntity is not found
		/// in the database. When set to true, WeatherEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WeatherEntityReturnsNewIfNotFound
		{
			get	{ return _weatherEntityReturnsNewIfNotFound; }
			set { _weatherEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
