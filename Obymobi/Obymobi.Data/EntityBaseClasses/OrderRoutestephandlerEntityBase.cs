﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'OrderRoutestephandler'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class OrderRoutestephandlerEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OrderRoutestephandlerEntity"; }
		}
	
		#region Class Member Declarations
		private ExternalSystemEntity _externalSystemEntity;
		private bool	_alwaysFetchExternalSystemEntity, _alreadyFetchedExternalSystemEntity, _externalSystemEntityReturnsNewIfNotFound;
		private OrderEntity _orderEntity;
		private bool	_alwaysFetchOrderEntity, _alreadyFetchedOrderEntity, _orderEntityReturnsNewIfNotFound;
		private SupportpoolEntity _supportpoolEntity;
		private bool	_alwaysFetchSupportpoolEntity, _alreadyFetchedSupportpoolEntity, _supportpoolEntityReturnsNewIfNotFound;
		private TerminalEntity _forwardedFromTerminalEntity;
		private bool	_alwaysFetchForwardedFromTerminalEntity, _alreadyFetchedForwardedFromTerminalEntity, _forwardedFromTerminalEntityReturnsNewIfNotFound;
		private TerminalEntity _terminalEntity;
		private bool	_alwaysFetchTerminalEntity, _alreadyFetchedTerminalEntity, _terminalEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalSystemEntity</summary>
			public static readonly string ExternalSystemEntity = "ExternalSystemEntity";
			/// <summary>Member name OrderEntity</summary>
			public static readonly string OrderEntity = "OrderEntity";
			/// <summary>Member name SupportpoolEntity</summary>
			public static readonly string SupportpoolEntity = "SupportpoolEntity";
			/// <summary>Member name ForwardedFromTerminalEntity</summary>
			public static readonly string ForwardedFromTerminalEntity = "ForwardedFromTerminalEntity";
			/// <summary>Member name TerminalEntity</summary>
			public static readonly string TerminalEntity = "TerminalEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderRoutestephandlerEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OrderRoutestephandlerEntityBase() :base("OrderRoutestephandlerEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		protected OrderRoutestephandlerEntityBase(System.Int32 orderRoutestephandlerId):base("OrderRoutestephandlerEntity")
		{
			InitClassFetch(orderRoutestephandlerId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OrderRoutestephandlerEntityBase(System.Int32 orderRoutestephandlerId, IPrefetchPath prefetchPathToUse): base("OrderRoutestephandlerEntity")
		{
			InitClassFetch(orderRoutestephandlerId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="validator">The custom validator object for this OrderRoutestephandlerEntity</param>
		protected OrderRoutestephandlerEntityBase(System.Int32 orderRoutestephandlerId, IValidator validator):base("OrderRoutestephandlerEntity")
		{
			InitClassFetch(orderRoutestephandlerId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderRoutestephandlerEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_externalSystemEntity = (ExternalSystemEntity)info.GetValue("_externalSystemEntity", typeof(ExternalSystemEntity));
			if(_externalSystemEntity!=null)
			{
				_externalSystemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalSystemEntityReturnsNewIfNotFound = info.GetBoolean("_externalSystemEntityReturnsNewIfNotFound");
			_alwaysFetchExternalSystemEntity = info.GetBoolean("_alwaysFetchExternalSystemEntity");
			_alreadyFetchedExternalSystemEntity = info.GetBoolean("_alreadyFetchedExternalSystemEntity");

			_orderEntity = (OrderEntity)info.GetValue("_orderEntity", typeof(OrderEntity));
			if(_orderEntity!=null)
			{
				_orderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderEntityReturnsNewIfNotFound = info.GetBoolean("_orderEntityReturnsNewIfNotFound");
			_alwaysFetchOrderEntity = info.GetBoolean("_alwaysFetchOrderEntity");
			_alreadyFetchedOrderEntity = info.GetBoolean("_alreadyFetchedOrderEntity");

			_supportpoolEntity = (SupportpoolEntity)info.GetValue("_supportpoolEntity", typeof(SupportpoolEntity));
			if(_supportpoolEntity!=null)
			{
				_supportpoolEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_supportpoolEntityReturnsNewIfNotFound = info.GetBoolean("_supportpoolEntityReturnsNewIfNotFound");
			_alwaysFetchSupportpoolEntity = info.GetBoolean("_alwaysFetchSupportpoolEntity");
			_alreadyFetchedSupportpoolEntity = info.GetBoolean("_alreadyFetchedSupportpoolEntity");

			_forwardedFromTerminalEntity = (TerminalEntity)info.GetValue("_forwardedFromTerminalEntity", typeof(TerminalEntity));
			if(_forwardedFromTerminalEntity!=null)
			{
				_forwardedFromTerminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_forwardedFromTerminalEntityReturnsNewIfNotFound = info.GetBoolean("_forwardedFromTerminalEntityReturnsNewIfNotFound");
			_alwaysFetchForwardedFromTerminalEntity = info.GetBoolean("_alwaysFetchForwardedFromTerminalEntity");
			_alreadyFetchedForwardedFromTerminalEntity = info.GetBoolean("_alreadyFetchedForwardedFromTerminalEntity");

			_terminalEntity = (TerminalEntity)info.GetValue("_terminalEntity", typeof(TerminalEntity));
			if(_terminalEntity!=null)
			{
				_terminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_terminalEntityReturnsNewIfNotFound = info.GetBoolean("_terminalEntityReturnsNewIfNotFound");
			_alwaysFetchTerminalEntity = info.GetBoolean("_alwaysFetchTerminalEntity");
			_alreadyFetchedTerminalEntity = info.GetBoolean("_alreadyFetchedTerminalEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderRoutestephandlerFieldIndex)fieldIndex)
			{
				case OrderRoutestephandlerFieldIndex.OrderId:
					DesetupSyncOrderEntity(true, false);
					_alreadyFetchedOrderEntity = false;
					break;
				case OrderRoutestephandlerFieldIndex.TerminalId:
					DesetupSyncTerminalEntity(true, false);
					_alreadyFetchedTerminalEntity = false;
					break;
				case OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId:
					DesetupSyncForwardedFromTerminalEntity(true, false);
					_alreadyFetchedForwardedFromTerminalEntity = false;
					break;
				case OrderRoutestephandlerFieldIndex.SupportpoolId:
					DesetupSyncSupportpoolEntity(true, false);
					_alreadyFetchedSupportpoolEntity = false;
					break;
				case OrderRoutestephandlerFieldIndex.ExternalSystemId:
					DesetupSyncExternalSystemEntity(true, false);
					_alreadyFetchedExternalSystemEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedExternalSystemEntity = (_externalSystemEntity != null);
			_alreadyFetchedOrderEntity = (_orderEntity != null);
			_alreadyFetchedSupportpoolEntity = (_supportpoolEntity != null);
			_alreadyFetchedForwardedFromTerminalEntity = (_forwardedFromTerminalEntity != null);
			_alreadyFetchedTerminalEntity = (_terminalEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ExternalSystemEntity":
					toReturn.Add(Relations.ExternalSystemEntityUsingExternalSystemId);
					break;
				case "OrderEntity":
					toReturn.Add(Relations.OrderEntityUsingOrderId);
					break;
				case "SupportpoolEntity":
					toReturn.Add(Relations.SupportpoolEntityUsingSupportpoolId);
					break;
				case "ForwardedFromTerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingForwardedFromTerminalId);
					break;
				case "TerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingTerminalId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_externalSystemEntity", (!this.MarkedForDeletion?_externalSystemEntity:null));
			info.AddValue("_externalSystemEntityReturnsNewIfNotFound", _externalSystemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalSystemEntity", _alwaysFetchExternalSystemEntity);
			info.AddValue("_alreadyFetchedExternalSystemEntity", _alreadyFetchedExternalSystemEntity);
			info.AddValue("_orderEntity", (!this.MarkedForDeletion?_orderEntity:null));
			info.AddValue("_orderEntityReturnsNewIfNotFound", _orderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderEntity", _alwaysFetchOrderEntity);
			info.AddValue("_alreadyFetchedOrderEntity", _alreadyFetchedOrderEntity);
			info.AddValue("_supportpoolEntity", (!this.MarkedForDeletion?_supportpoolEntity:null));
			info.AddValue("_supportpoolEntityReturnsNewIfNotFound", _supportpoolEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSupportpoolEntity", _alwaysFetchSupportpoolEntity);
			info.AddValue("_alreadyFetchedSupportpoolEntity", _alreadyFetchedSupportpoolEntity);
			info.AddValue("_forwardedFromTerminalEntity", (!this.MarkedForDeletion?_forwardedFromTerminalEntity:null));
			info.AddValue("_forwardedFromTerminalEntityReturnsNewIfNotFound", _forwardedFromTerminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchForwardedFromTerminalEntity", _alwaysFetchForwardedFromTerminalEntity);
			info.AddValue("_alreadyFetchedForwardedFromTerminalEntity", _alreadyFetchedForwardedFromTerminalEntity);
			info.AddValue("_terminalEntity", (!this.MarkedForDeletion?_terminalEntity:null));
			info.AddValue("_terminalEntityReturnsNewIfNotFound", _terminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTerminalEntity", _alwaysFetchTerminalEntity);
			info.AddValue("_alreadyFetchedTerminalEntity", _alreadyFetchedTerminalEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ExternalSystemEntity":
					_alreadyFetchedExternalSystemEntity = true;
					this.ExternalSystemEntity = (ExternalSystemEntity)entity;
					break;
				case "OrderEntity":
					_alreadyFetchedOrderEntity = true;
					this.OrderEntity = (OrderEntity)entity;
					break;
				case "SupportpoolEntity":
					_alreadyFetchedSupportpoolEntity = true;
					this.SupportpoolEntity = (SupportpoolEntity)entity;
					break;
				case "ForwardedFromTerminalEntity":
					_alreadyFetchedForwardedFromTerminalEntity = true;
					this.ForwardedFromTerminalEntity = (TerminalEntity)entity;
					break;
				case "TerminalEntity":
					_alreadyFetchedTerminalEntity = true;
					this.TerminalEntity = (TerminalEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ExternalSystemEntity":
					SetupSyncExternalSystemEntity(relatedEntity);
					break;
				case "OrderEntity":
					SetupSyncOrderEntity(relatedEntity);
					break;
				case "SupportpoolEntity":
					SetupSyncSupportpoolEntity(relatedEntity);
					break;
				case "ForwardedFromTerminalEntity":
					SetupSyncForwardedFromTerminalEntity(relatedEntity);
					break;
				case "TerminalEntity":
					SetupSyncTerminalEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ExternalSystemEntity":
					DesetupSyncExternalSystemEntity(false, true);
					break;
				case "OrderEntity":
					DesetupSyncOrderEntity(false, true);
					break;
				case "SupportpoolEntity":
					DesetupSyncSupportpoolEntity(false, true);
					break;
				case "ForwardedFromTerminalEntity":
					DesetupSyncForwardedFromTerminalEntity(false, true);
					break;
				case "TerminalEntity":
					DesetupSyncTerminalEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_externalSystemEntity!=null)
			{
				toReturn.Add(_externalSystemEntity);
			}
			if(_orderEntity!=null)
			{
				toReturn.Add(_orderEntity);
			}
			if(_supportpoolEntity!=null)
			{
				toReturn.Add(_supportpoolEntity);
			}
			if(_forwardedFromTerminalEntity!=null)
			{
				toReturn.Add(_forwardedFromTerminalEntity);
			}
			if(_terminalEntity!=null)
			{
				toReturn.Add(_terminalEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderRoutestephandlerId)
		{
			return FetchUsingPK(orderRoutestephandlerId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderRoutestephandlerId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderRoutestephandlerId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderRoutestephandlerId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderRoutestephandlerId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderRoutestephandlerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderRoutestephandlerId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderRoutestephandlerId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderRoutestephandlerRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public ExternalSystemEntity GetSingleExternalSystemEntity()
		{
			return GetSingleExternalSystemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalSystemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalSystemEntity' which is related to this entity.</returns>
		public virtual ExternalSystemEntity GetSingleExternalSystemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalSystemEntity || forceFetch || _alwaysFetchExternalSystemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalSystemEntityUsingExternalSystemId);
				ExternalSystemEntity newEntity = new ExternalSystemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalSystemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalSystemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalSystemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalSystemEntity = newEntity;
				_alreadyFetchedExternalSystemEntity = fetchResult;
			}
			return _externalSystemEntity;
		}


		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrderEntity()
		{
			return GetSingleOrderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderEntity || forceFetch || _alwaysFetchOrderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderId);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderId);
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderEntity = newEntity;
				_alreadyFetchedOrderEntity = fetchResult;
			}
			return _orderEntity;
		}


		/// <summary> Retrieves the related entity of type 'SupportpoolEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SupportpoolEntity' which is related to this entity.</returns>
		public SupportpoolEntity GetSingleSupportpoolEntity()
		{
			return GetSingleSupportpoolEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SupportpoolEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SupportpoolEntity' which is related to this entity.</returns>
		public virtual SupportpoolEntity GetSingleSupportpoolEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSupportpoolEntity || forceFetch || _alwaysFetchSupportpoolEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SupportpoolEntityUsingSupportpoolId);
				SupportpoolEntity newEntity = new SupportpoolEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SupportpoolId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SupportpoolEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_supportpoolEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SupportpoolEntity = newEntity;
				_alreadyFetchedSupportpoolEntity = fetchResult;
			}
			return _supportpoolEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleForwardedFromTerminalEntity()
		{
			return GetSingleForwardedFromTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleForwardedFromTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedForwardedFromTerminalEntity || forceFetch || _alwaysFetchForwardedFromTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingForwardedFromTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ForwardedFromTerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_forwardedFromTerminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ForwardedFromTerminalEntity = newEntity;
				_alreadyFetchedForwardedFromTerminalEntity = fetchResult;
			}
			return _forwardedFromTerminalEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleTerminalEntity()
		{
			return GetSingleTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTerminalEntity || forceFetch || _alwaysFetchTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_terminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TerminalEntity = newEntity;
				_alreadyFetchedTerminalEntity = fetchResult;
			}
			return _terminalEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ExternalSystemEntity", _externalSystemEntity);
			toReturn.Add("OrderEntity", _orderEntity);
			toReturn.Add("SupportpoolEntity", _supportpoolEntity);
			toReturn.Add("ForwardedFromTerminalEntity", _forwardedFromTerminalEntity);
			toReturn.Add("TerminalEntity", _terminalEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="validator">The validator object for this OrderRoutestephandlerEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 orderRoutestephandlerId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderRoutestephandlerId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_externalSystemEntityReturnsNewIfNotFound = true;
			_orderEntityReturnsNewIfNotFound = true;
			_supportpoolEntityReturnsNewIfNotFound = true;
			_forwardedFromTerminalEntityReturnsNewIfNotFound = true;
			_terminalEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderRoutestephandlerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HandlerType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintReportType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Guid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Timeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogAlways", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ForwardedFromTerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EscalationStep", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContinueOnFailure", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompleteRouteOnComplete", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EscalationRouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupportpoolId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RetrievalSupportNotificationSent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BeingHandledSupportNotificationSent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RetrievalSupportNotificationTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BeingHandledSupportNotificationTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HandlerTypeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginatedFromRoutestepHandlerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeoutExpiresUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RetrievalSupportNotificationTimeoutUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BeingHandledSupportNotificationTimeoutUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WaitingToBeRetrievedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RetrievedByHandlerUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BeingHandledUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalSystemId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _externalSystemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalSystemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, signalRelatedEntity, "OrderRoutestephandlerCollection", resetFKFields, new int[] { (int)OrderRoutestephandlerFieldIndex.ExternalSystemId } );		
			_externalSystemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalSystemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalSystemEntity(IEntityCore relatedEntity)
		{
			if(_externalSystemEntity!=relatedEntity)
			{		
				DesetupSyncExternalSystemEntity(true, true);
				_externalSystemEntity = (ExternalSystemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalSystemEntity, new PropertyChangedEventHandler( OnExternalSystemEntityPropertyChanged ), "ExternalSystemEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.ExternalSystemEntityUsingExternalSystemIdStatic, true, ref _alreadyFetchedExternalSystemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalSystemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.OrderEntityUsingOrderIdStatic, true, signalRelatedEntity, "OrderRoutestephandlerCollection", resetFKFields, new int[] { (int)OrderRoutestephandlerFieldIndex.OrderId } );		
			_orderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderEntity(IEntityCore relatedEntity)
		{
			if(_orderEntity!=relatedEntity)
			{		
				DesetupSyncOrderEntity(true, true);
				_orderEntity = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.OrderEntityUsingOrderIdStatic, true, ref _alreadyFetchedOrderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _supportpoolEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSupportpoolEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _supportpoolEntity, new PropertyChangedEventHandler( OnSupportpoolEntityPropertyChanged ), "SupportpoolEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.SupportpoolEntityUsingSupportpoolIdStatic, true, signalRelatedEntity, "OrderRoutestephandlerCollection", resetFKFields, new int[] { (int)OrderRoutestephandlerFieldIndex.SupportpoolId } );		
			_supportpoolEntity = null;
		}
		
		/// <summary> setups the sync logic for member _supportpoolEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSupportpoolEntity(IEntityCore relatedEntity)
		{
			if(_supportpoolEntity!=relatedEntity)
			{		
				DesetupSyncSupportpoolEntity(true, true);
				_supportpoolEntity = (SupportpoolEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _supportpoolEntity, new PropertyChangedEventHandler( OnSupportpoolEntityPropertyChanged ), "SupportpoolEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.SupportpoolEntityUsingSupportpoolIdStatic, true, ref _alreadyFetchedSupportpoolEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSupportpoolEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _forwardedFromTerminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncForwardedFromTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _forwardedFromTerminalEntity, new PropertyChangedEventHandler( OnForwardedFromTerminalEntityPropertyChanged ), "ForwardedFromTerminalEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.TerminalEntityUsingForwardedFromTerminalIdStatic, true, signalRelatedEntity, "OrderRoutestephandlerCollection_", resetFKFields, new int[] { (int)OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId } );		
			_forwardedFromTerminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _forwardedFromTerminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncForwardedFromTerminalEntity(IEntityCore relatedEntity)
		{
			if(_forwardedFromTerminalEntity!=relatedEntity)
			{		
				DesetupSyncForwardedFromTerminalEntity(true, true);
				_forwardedFromTerminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _forwardedFromTerminalEntity, new PropertyChangedEventHandler( OnForwardedFromTerminalEntityPropertyChanged ), "ForwardedFromTerminalEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.TerminalEntityUsingForwardedFromTerminalIdStatic, true, ref _alreadyFetchedForwardedFromTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnForwardedFromTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _terminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.TerminalEntityUsingTerminalIdStatic, true, signalRelatedEntity, "OrderRoutestephandlerCollection", resetFKFields, new int[] { (int)OrderRoutestephandlerFieldIndex.TerminalId } );		
			_terminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _terminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTerminalEntity(IEntityCore relatedEntity)
		{
			if(_terminalEntity!=relatedEntity)
			{		
				DesetupSyncTerminalEntity(true, true);
				_terminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _terminalEntity, new PropertyChangedEventHandler( OnTerminalEntityPropertyChanged ), "TerminalEntity", Obymobi.Data.RelationClasses.StaticOrderRoutestephandlerRelations.TerminalEntityUsingTerminalIdStatic, true, ref _alreadyFetchedTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 orderRoutestephandlerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderRoutestephandlerFieldIndex.OrderRoutestephandlerId].ForcedCurrentValueWrite(orderRoutestephandlerId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderRoutestephandlerDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderRoutestephandlerEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderRoutestephandlerRelations Relations
		{
			get	{ return new OrderRoutestephandlerRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalSystem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalSystemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalSystemCollection(), (IEntityRelation)GetRelationsForField("ExternalSystemEntity")[0], (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, (int)Obymobi.Data.EntityType.ExternalSystemEntity, 0, null, null, null, "ExternalSystemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderEntity")[0], (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), (IEntityRelation)GetRelationsForField("SupportpoolEntity")[0], (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, null, "SupportpoolEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForwardedFromTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("ForwardedFromTerminalEntity")[0], (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "ForwardedFromTerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalEntity")[0], (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OrderRoutestephandlerId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."OrderRoutestephandlerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderRoutestephandlerId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.OrderRoutestephandlerId, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.OrderRoutestephandlerId, value, true); }
		}

		/// <summary> The OrderId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.OrderId, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.OrderId, value, true); }
		}

		/// <summary> The TerminalId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."TerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.TerminalId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.TerminalId, value, true); }
		}

		/// <summary> The Number property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Number"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Number
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.Number, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Number, value, true); }
		}

		/// <summary> The HandlerType property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."HandlerType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HandlerType
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.HandlerType, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.HandlerType, value, true); }
		}

		/// <summary> The PrintReportType property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."PrintReportType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PrintReportType
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.PrintReportType, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.PrintReportType, value, true); }
		}

		/// <summary> The Status property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Status"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Status
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.Status, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Status, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ErrorCode property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ErrorCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ErrorCode
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.ErrorCode, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ErrorCode, value, true); }
		}

		/// <summary> The ErrorText property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ErrorText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.ErrorText, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ErrorText, value, true); }
		}

		/// <summary> The Guid property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Guid"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Guid, value, true); }
		}

		/// <summary> The Timeout property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."Timeout"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Timeout
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.Timeout, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.Timeout, value, true); }
		}

		/// <summary> The LogAlways property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."LogAlways"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LogAlways
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.LogAlways, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.LogAlways, value, true); }
		}

		/// <summary> The ForwardedFromTerminalId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ForwardedFromTerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ForwardedFromTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ForwardedFromTerminalId, value, true); }
		}

		/// <summary> The EscalationStep property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."EscalationStep"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EscalationStep
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.EscalationStep, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.EscalationStep, value, true); }
		}

		/// <summary> The ContinueOnFailure property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ContinueOnFailure"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ContinueOnFailure
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.ContinueOnFailure, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ContinueOnFailure, value, true); }
		}

		/// <summary> The CompleteRouteOnComplete property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CompleteRouteOnComplete"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CompleteRouteOnComplete
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.CompleteRouteOnComplete, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CompleteRouteOnComplete, value, true); }
		}

		/// <summary> The EscalationRouteId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."EscalationRouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EscalationRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.EscalationRouteId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.EscalationRouteId, value, true); }
		}

		/// <summary> The SupportpoolId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."SupportpoolId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupportpoolId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.SupportpoolId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.SupportpoolId, value, true); }
		}

		/// <summary> The RetrievalSupportNotificationSent property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievalSupportNotificationSent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RetrievalSupportNotificationSent
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationSent, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationSent, value, true); }
		}

		/// <summary> The BeingHandledSupportNotificationSent property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledSupportNotificationSent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BeingHandledSupportNotificationSent
		{
			get { return (System.Boolean)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationSent, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationSent, value, true); }
		}

		/// <summary> The RetrievalSupportNotificationTimeoutMinutes property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievalSupportNotificationTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RetrievalSupportNotificationTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutMinutes, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutMinutes, value, true); }
		}

		/// <summary> The BeingHandledSupportNotificationTimeoutMinutes property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledSupportNotificationTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BeingHandledSupportNotificationTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, value, true); }
		}

		/// <summary> The HandlerTypeText property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."HandlerTypeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HandlerTypeText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.HandlerTypeText, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.HandlerTypeText, value, true); }
		}

		/// <summary> The StatusText property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."StatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)OrderRoutestephandlerFieldIndex.StatusText, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.StatusText, value, true); }
		}

		/// <summary> The OriginatedFromRoutestepHandlerId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."OriginatedFromRoutestepHandlerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OriginatedFromRoutestepHandlerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.OriginatedFromRoutestepHandlerId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.OriginatedFromRoutestepHandlerId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderRoutestephandlerFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The TimeoutExpiresUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."TimeoutExpiresUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TimeoutExpiresUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.TimeoutExpiresUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.TimeoutExpiresUTC, value, true); }
		}

		/// <summary> The RetrievalSupportNotificationTimeoutUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievalSupportNotificationTimeoutUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RetrievalSupportNotificationTimeoutUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievalSupportNotificationTimeoutUTC, value, true); }
		}

		/// <summary> The BeingHandledSupportNotificationTimeoutUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledSupportNotificationTimeoutUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BeingHandledSupportNotificationTimeoutUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledSupportNotificationTimeoutUTC, value, true); }
		}

		/// <summary> The WaitingToBeRetrievedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."WaitingToBeRetrievedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> WaitingToBeRetrievedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.WaitingToBeRetrievedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.WaitingToBeRetrievedUTC, value, true); }
		}

		/// <summary> The RetrievedByHandlerUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."RetrievedByHandlerUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RetrievedByHandlerUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.RetrievedByHandlerUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.RetrievedByHandlerUTC, value, true); }
		}

		/// <summary> The BeingHandledUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."BeingHandledUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BeingHandledUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.BeingHandledUTC, value, true); }
		}

		/// <summary> The CompletedUTC property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."CompletedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CompletedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderRoutestephandlerFieldIndex.CompletedUTC, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.CompletedUTC, value, true); }
		}

		/// <summary> The ExternalSystemId property of the Entity OrderRoutestephandler<br/><br/></summary>
		/// <remarks>Mapped on  table field: "OrderRoutestephandler"."ExternalSystemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalSystemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderRoutestephandlerFieldIndex.ExternalSystemId, false); }
			set	{ SetValue((int)OrderRoutestephandlerFieldIndex.ExternalSystemId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ExternalSystemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalSystemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalSystemEntity ExternalSystemEntity
		{
			get	{ return GetSingleExternalSystemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalSystemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderRoutestephandlerCollection", "ExternalSystemEntity", _externalSystemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalSystemEntity. When set to true, ExternalSystemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalSystemEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalSystemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalSystemEntity
		{
			get	{ return _alwaysFetchExternalSystemEntity; }
			set	{ _alwaysFetchExternalSystemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalSystemEntity already has been fetched. Setting this property to false when ExternalSystemEntity has been fetched
		/// will set ExternalSystemEntity to null as well. Setting this property to true while ExternalSystemEntity hasn't been fetched disables lazy loading for ExternalSystemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalSystemEntity
		{
			get { return _alreadyFetchedExternalSystemEntity;}
			set 
			{
				if(_alreadyFetchedExternalSystemEntity && !value)
				{
					this.ExternalSystemEntity = null;
				}
				_alreadyFetchedExternalSystemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalSystemEntity is not found
		/// in the database. When set to true, ExternalSystemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalSystemEntityReturnsNewIfNotFound
		{
			get	{ return _externalSystemEntityReturnsNewIfNotFound; }
			set { _externalSystemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderEntity OrderEntity
		{
			get	{ return GetSingleOrderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderRoutestephandlerCollection", "OrderEntity", _orderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderEntity. When set to true, OrderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderEntity
		{
			get	{ return _alwaysFetchOrderEntity; }
			set	{ _alwaysFetchOrderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderEntity already has been fetched. Setting this property to false when OrderEntity has been fetched
		/// will set OrderEntity to null as well. Setting this property to true while OrderEntity hasn't been fetched disables lazy loading for OrderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderEntity
		{
			get { return _alreadyFetchedOrderEntity;}
			set 
			{
				if(_alreadyFetchedOrderEntity && !value)
				{
					this.OrderEntity = null;
				}
				_alreadyFetchedOrderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderEntity is not found
		/// in the database. When set to true, OrderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderEntityReturnsNewIfNotFound
		{
			get	{ return _orderEntityReturnsNewIfNotFound; }
			set { _orderEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SupportpoolEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSupportpoolEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SupportpoolEntity SupportpoolEntity
		{
			get	{ return GetSingleSupportpoolEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSupportpoolEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderRoutestephandlerCollection", "SupportpoolEntity", _supportpoolEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolEntity. When set to true, SupportpoolEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolEntity is accessed. You can always execute a forced fetch by calling GetSingleSupportpoolEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolEntity
		{
			get	{ return _alwaysFetchSupportpoolEntity; }
			set	{ _alwaysFetchSupportpoolEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolEntity already has been fetched. Setting this property to false when SupportpoolEntity has been fetched
		/// will set SupportpoolEntity to null as well. Setting this property to true while SupportpoolEntity hasn't been fetched disables lazy loading for SupportpoolEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolEntity
		{
			get { return _alreadyFetchedSupportpoolEntity;}
			set 
			{
				if(_alreadyFetchedSupportpoolEntity && !value)
				{
					this.SupportpoolEntity = null;
				}
				_alreadyFetchedSupportpoolEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SupportpoolEntity is not found
		/// in the database. When set to true, SupportpoolEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SupportpoolEntityReturnsNewIfNotFound
		{
			get	{ return _supportpoolEntityReturnsNewIfNotFound; }
			set { _supportpoolEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleForwardedFromTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity ForwardedFromTerminalEntity
		{
			get	{ return GetSingleForwardedFromTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncForwardedFromTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderRoutestephandlerCollection_", "ForwardedFromTerminalEntity", _forwardedFromTerminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ForwardedFromTerminalEntity. When set to true, ForwardedFromTerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ForwardedFromTerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleForwardedFromTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForwardedFromTerminalEntity
		{
			get	{ return _alwaysFetchForwardedFromTerminalEntity; }
			set	{ _alwaysFetchForwardedFromTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ForwardedFromTerminalEntity already has been fetched. Setting this property to false when ForwardedFromTerminalEntity has been fetched
		/// will set ForwardedFromTerminalEntity to null as well. Setting this property to true while ForwardedFromTerminalEntity hasn't been fetched disables lazy loading for ForwardedFromTerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForwardedFromTerminalEntity
		{
			get { return _alreadyFetchedForwardedFromTerminalEntity;}
			set 
			{
				if(_alreadyFetchedForwardedFromTerminalEntity && !value)
				{
					this.ForwardedFromTerminalEntity = null;
				}
				_alreadyFetchedForwardedFromTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ForwardedFromTerminalEntity is not found
		/// in the database. When set to true, ForwardedFromTerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ForwardedFromTerminalEntityReturnsNewIfNotFound
		{
			get	{ return _forwardedFromTerminalEntityReturnsNewIfNotFound; }
			set { _forwardedFromTerminalEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity TerminalEntity
		{
			get	{ return GetSingleTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderRoutestephandlerCollection", "TerminalEntity", _terminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalEntity. When set to true, TerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalEntity
		{
			get	{ return _alwaysFetchTerminalEntity; }
			set	{ _alwaysFetchTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalEntity already has been fetched. Setting this property to false when TerminalEntity has been fetched
		/// will set TerminalEntity to null as well. Setting this property to true while TerminalEntity hasn't been fetched disables lazy loading for TerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalEntity
		{
			get { return _alreadyFetchedTerminalEntity;}
			set 
			{
				if(_alreadyFetchedTerminalEntity && !value)
				{
					this.TerminalEntity = null;
				}
				_alreadyFetchedTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TerminalEntity is not found
		/// in the database. When set to true, TerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TerminalEntityReturnsNewIfNotFound
		{
			get	{ return _terminalEntityReturnsNewIfNotFound; }
			set { _terminalEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OrderRoutestephandlerEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
