﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ProductgroupItem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ProductgroupItemEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ProductgroupItemEntity"; }
		}
	
		#region Class Member Declarations
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private ProductgroupEntity _productgroupEntity;
		private bool	_alwaysFetchProductgroupEntity, _alreadyFetchedProductgroupEntity, _productgroupEntityReturnsNewIfNotFound;
		private ProductgroupEntity _productgroupEntity1;
		private bool	_alwaysFetchProductgroupEntity1, _alreadyFetchedProductgroupEntity1, _productgroupEntity1ReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name ProductgroupEntity</summary>
			public static readonly string ProductgroupEntity = "ProductgroupEntity";
			/// <summary>Member name ProductgroupEntity1</summary>
			public static readonly string ProductgroupEntity1 = "ProductgroupEntity1";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProductgroupItemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ProductgroupItemEntityBase() :base("ProductgroupItemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		protected ProductgroupItemEntityBase(System.Int32 productgroupItemId):base("ProductgroupItemEntity")
		{
			InitClassFetch(productgroupItemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ProductgroupItemEntityBase(System.Int32 productgroupItemId, IPrefetchPath prefetchPathToUse): base("ProductgroupItemEntity")
		{
			InitClassFetch(productgroupItemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <param name="validator">The custom validator object for this ProductgroupItemEntity</param>
		protected ProductgroupItemEntityBase(System.Int32 productgroupItemId, IValidator validator):base("ProductgroupItemEntity")
		{
			InitClassFetch(productgroupItemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductgroupItemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_productgroupEntity = (ProductgroupEntity)info.GetValue("_productgroupEntity", typeof(ProductgroupEntity));
			if(_productgroupEntity!=null)
			{
				_productgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productgroupEntityReturnsNewIfNotFound = info.GetBoolean("_productgroupEntityReturnsNewIfNotFound");
			_alwaysFetchProductgroupEntity = info.GetBoolean("_alwaysFetchProductgroupEntity");
			_alreadyFetchedProductgroupEntity = info.GetBoolean("_alreadyFetchedProductgroupEntity");

			_productgroupEntity1 = (ProductgroupEntity)info.GetValue("_productgroupEntity1", typeof(ProductgroupEntity));
			if(_productgroupEntity1!=null)
			{
				_productgroupEntity1.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productgroupEntity1ReturnsNewIfNotFound = info.GetBoolean("_productgroupEntity1ReturnsNewIfNotFound");
			_alwaysFetchProductgroupEntity1 = info.GetBoolean("_alwaysFetchProductgroupEntity1");
			_alreadyFetchedProductgroupEntity1 = info.GetBoolean("_alreadyFetchedProductgroupEntity1");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProductgroupItemFieldIndex)fieldIndex)
			{
				case ProductgroupItemFieldIndex.ProductgroupId:
					DesetupSyncProductgroupEntity(true, false);
					_alreadyFetchedProductgroupEntity = false;
					break;
				case ProductgroupItemFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case ProductgroupItemFieldIndex.NestedProductgroupId:
					DesetupSyncProductgroupEntity1(true, false);
					_alreadyFetchedProductgroupEntity1 = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedProductgroupEntity = (_productgroupEntity != null);
			_alreadyFetchedProductgroupEntity1 = (_productgroupEntity1 != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "ProductgroupEntity":
					toReturn.Add(Relations.ProductgroupEntityUsingProductgroupId);
					break;
				case "ProductgroupEntity1":
					toReturn.Add(Relations.ProductgroupEntityUsingNestedProductgroupId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_productgroupEntity", (!this.MarkedForDeletion?_productgroupEntity:null));
			info.AddValue("_productgroupEntityReturnsNewIfNotFound", _productgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductgroupEntity", _alwaysFetchProductgroupEntity);
			info.AddValue("_alreadyFetchedProductgroupEntity", _alreadyFetchedProductgroupEntity);
			info.AddValue("_productgroupEntity1", (!this.MarkedForDeletion?_productgroupEntity1:null));
			info.AddValue("_productgroupEntity1ReturnsNewIfNotFound", _productgroupEntity1ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductgroupEntity1", _alwaysFetchProductgroupEntity1);
			info.AddValue("_alreadyFetchedProductgroupEntity1", _alreadyFetchedProductgroupEntity1);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "ProductgroupEntity":
					_alreadyFetchedProductgroupEntity = true;
					this.ProductgroupEntity = (ProductgroupEntity)entity;
					break;
				case "ProductgroupEntity1":
					_alreadyFetchedProductgroupEntity1 = true;
					this.ProductgroupEntity1 = (ProductgroupEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "ProductgroupEntity":
					SetupSyncProductgroupEntity(relatedEntity);
					break;
				case "ProductgroupEntity1":
					SetupSyncProductgroupEntity1(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "ProductgroupEntity":
					DesetupSyncProductgroupEntity(false, true);
					break;
				case "ProductgroupEntity1":
					DesetupSyncProductgroupEntity1(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_productgroupEntity!=null)
			{
				toReturn.Add(_productgroupEntity);
			}
			if(_productgroupEntity1!=null)
			{
				toReturn.Add(_productgroupEntity1);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productgroupItemId)
		{
			return FetchUsingPK(productgroupItemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productgroupItemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(productgroupItemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productgroupItemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(productgroupItemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productgroupItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(productgroupItemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProductgroupItemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProductgroupItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public ProductgroupEntity GetSingleProductgroupEntity()
		{
			return GetSingleProductgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public virtual ProductgroupEntity GetSingleProductgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductgroupEntity || forceFetch || _alwaysFetchProductgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductgroupEntityUsingProductgroupId);
				ProductgroupEntity newEntity = new ProductgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductgroupId);
				}
				if(fetchResult)
				{
					newEntity = (ProductgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductgroupEntity = newEntity;
				_alreadyFetchedProductgroupEntity = fetchResult;
			}
			return _productgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public ProductgroupEntity GetSingleProductgroupEntity1()
		{
			return GetSingleProductgroupEntity1(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductgroupEntity' which is related to this entity.</returns>
		public virtual ProductgroupEntity GetSingleProductgroupEntity1(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductgroupEntity1 || forceFetch || _alwaysFetchProductgroupEntity1) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductgroupEntityUsingNestedProductgroupId);
				ProductgroupEntity newEntity = new ProductgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NestedProductgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productgroupEntity1ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductgroupEntity1 = newEntity;
				_alreadyFetchedProductgroupEntity1 = fetchResult;
			}
			return _productgroupEntity1;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("ProductgroupEntity", _productgroupEntity);
			toReturn.Add("ProductgroupEntity1", _productgroupEntity1);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <param name="validator">The validator object for this ProductgroupItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 productgroupItemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(productgroupItemId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_productEntityReturnsNewIfNotFound = true;
			_productgroupEntityReturnsNewIfNotFound = true;
			_productgroupEntity1ReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductgroupItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NestedProductgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticProductgroupItemRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "ProductgroupItemCollection", resetFKFields, new int[] { (int)ProductgroupItemFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticProductgroupItemRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productgroupEntity, new PropertyChangedEventHandler( OnProductgroupEntityPropertyChanged ), "ProductgroupEntity", Obymobi.Data.RelationClasses.StaticProductgroupItemRelations.ProductgroupEntityUsingProductgroupIdStatic, true, signalRelatedEntity, "ProductgroupItemCollection", resetFKFields, new int[] { (int)ProductgroupItemFieldIndex.ProductgroupId } );		
			_productgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductgroupEntity(IEntityCore relatedEntity)
		{
			if(_productgroupEntity!=relatedEntity)
			{		
				DesetupSyncProductgroupEntity(true, true);
				_productgroupEntity = (ProductgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productgroupEntity, new PropertyChangedEventHandler( OnProductgroupEntityPropertyChanged ), "ProductgroupEntity", Obymobi.Data.RelationClasses.StaticProductgroupItemRelations.ProductgroupEntityUsingProductgroupIdStatic, true, ref _alreadyFetchedProductgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productgroupEntity1</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductgroupEntity1(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productgroupEntity1, new PropertyChangedEventHandler( OnProductgroupEntity1PropertyChanged ), "ProductgroupEntity1", Obymobi.Data.RelationClasses.StaticProductgroupItemRelations.ProductgroupEntityUsingNestedProductgroupIdStatic, true, signalRelatedEntity, "ProductgroupItemCollection1", resetFKFields, new int[] { (int)ProductgroupItemFieldIndex.NestedProductgroupId } );		
			_productgroupEntity1 = null;
		}
		
		/// <summary> setups the sync logic for member _productgroupEntity1</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductgroupEntity1(IEntityCore relatedEntity)
		{
			if(_productgroupEntity1!=relatedEntity)
			{		
				DesetupSyncProductgroupEntity1(true, true);
				_productgroupEntity1 = (ProductgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productgroupEntity1, new PropertyChangedEventHandler( OnProductgroupEntity1PropertyChanged ), "ProductgroupEntity1", Obymobi.Data.RelationClasses.StaticProductgroupItemRelations.ProductgroupEntityUsingNestedProductgroupIdStatic, true, ref _alreadyFetchedProductgroupEntity1, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductgroupEntity1PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="productgroupItemId">PK value for ProductgroupItem which data should be fetched into this ProductgroupItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 productgroupItemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProductgroupItemFieldIndex.ProductgroupItemId].ForcedCurrentValueWrite(productgroupItemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProductgroupItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProductgroupItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProductgroupItemRelations Relations
		{
			get	{ return new ProductgroupItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.ProductgroupItemEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Productgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductgroupCollection(), (IEntityRelation)GetRelationsForField("ProductgroupEntity")[0], (int)Obymobi.Data.EntityType.ProductgroupItemEntity, (int)Obymobi.Data.EntityType.ProductgroupEntity, 0, null, null, null, "ProductgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Productgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductgroupEntity1
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductgroupCollection(), (IEntityRelation)GetRelationsForField("ProductgroupEntity1")[0], (int)Obymobi.Data.EntityType.ProductgroupItemEntity, (int)Obymobi.Data.EntityType.ProductgroupEntity, 0, null, null, null, "ProductgroupEntity1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ProductgroupItemId property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."ProductgroupItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductgroupItemId
		{
			get { return (System.Int32)GetValue((int)ProductgroupItemFieldIndex.ProductgroupItemId, true); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.ProductgroupItemId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ProductgroupItemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The ProductgroupId property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."ProductgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductgroupId
		{
			get { return (System.Int32)GetValue((int)ProductgroupItemFieldIndex.ProductgroupId, true); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.ProductgroupId, value, true); }
		}

		/// <summary> The ProductId property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductgroupItemFieldIndex.ProductId, false); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.ProductId, value, true); }
		}

		/// <summary> The NestedProductgroupId property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."NestedProductgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> NestedProductgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductgroupItemFieldIndex.NestedProductgroupId, false); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.NestedProductgroupId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ProductgroupItemFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductgroupItemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductgroupItemFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductgroupItemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ProductgroupItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductgroupItem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductgroupItemFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ProductgroupItemFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductgroupItemCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductgroupEntity ProductgroupEntity
		{
			get	{ return GetSingleProductgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductgroupItemCollection", "ProductgroupEntity", _productgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductgroupEntity. When set to true, ProductgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleProductgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductgroupEntity
		{
			get	{ return _alwaysFetchProductgroupEntity; }
			set	{ _alwaysFetchProductgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductgroupEntity already has been fetched. Setting this property to false when ProductgroupEntity has been fetched
		/// will set ProductgroupEntity to null as well. Setting this property to true while ProductgroupEntity hasn't been fetched disables lazy loading for ProductgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductgroupEntity
		{
			get { return _alreadyFetchedProductgroupEntity;}
			set 
			{
				if(_alreadyFetchedProductgroupEntity && !value)
				{
					this.ProductgroupEntity = null;
				}
				_alreadyFetchedProductgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductgroupEntity is not found
		/// in the database. When set to true, ProductgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductgroupEntityReturnsNewIfNotFound
		{
			get	{ return _productgroupEntityReturnsNewIfNotFound; }
			set { _productgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductgroupEntity1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductgroupEntity ProductgroupEntity1
		{
			get	{ return GetSingleProductgroupEntity1(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductgroupEntity1(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductgroupItemCollection1", "ProductgroupEntity1", _productgroupEntity1, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductgroupEntity1. When set to true, ProductgroupEntity1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductgroupEntity1 is accessed. You can always execute a forced fetch by calling GetSingleProductgroupEntity1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductgroupEntity1
		{
			get	{ return _alwaysFetchProductgroupEntity1; }
			set	{ _alwaysFetchProductgroupEntity1 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductgroupEntity1 already has been fetched. Setting this property to false when ProductgroupEntity1 has been fetched
		/// will set ProductgroupEntity1 to null as well. Setting this property to true while ProductgroupEntity1 hasn't been fetched disables lazy loading for ProductgroupEntity1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductgroupEntity1
		{
			get { return _alreadyFetchedProductgroupEntity1;}
			set 
			{
				if(_alreadyFetchedProductgroupEntity1 && !value)
				{
					this.ProductgroupEntity1 = null;
				}
				_alreadyFetchedProductgroupEntity1 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductgroupEntity1 is not found
		/// in the database. When set to true, ProductgroupEntity1 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductgroupEntity1ReturnsNewIfNotFound
		{
			get	{ return _productgroupEntity1ReturnsNewIfNotFound; }
			set { _productgroupEntity1ReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ProductgroupItemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
