﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'AdyenPaymentMethod'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AdyenPaymentMethodEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AdyenPaymentMethodEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection	_adyenPaymentMethodBrandCollection;
		private bool	_alwaysFetchAdyenPaymentMethodBrandCollection, _alreadyFetchedAdyenPaymentMethodBrandCollection;
		private PaymentIntegrationConfigurationEntity _paymentIntegrationConfigurationEntity;
		private bool	_alwaysFetchPaymentIntegrationConfigurationEntity, _alreadyFetchedPaymentIntegrationConfigurationEntity, _paymentIntegrationConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PaymentIntegrationConfigurationEntity</summary>
			public static readonly string PaymentIntegrationConfigurationEntity = "PaymentIntegrationConfigurationEntity";
			/// <summary>Member name AdyenPaymentMethodBrandCollection</summary>
			public static readonly string AdyenPaymentMethodBrandCollection = "AdyenPaymentMethodBrandCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AdyenPaymentMethodEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AdyenPaymentMethodEntityBase() :base("AdyenPaymentMethodEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		protected AdyenPaymentMethodEntityBase(System.Int32 adyenPaymentMethodId):base("AdyenPaymentMethodEntity")
		{
			InitClassFetch(adyenPaymentMethodId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AdyenPaymentMethodEntityBase(System.Int32 adyenPaymentMethodId, IPrefetchPath prefetchPathToUse): base("AdyenPaymentMethodEntity")
		{
			InitClassFetch(adyenPaymentMethodId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="validator">The custom validator object for this AdyenPaymentMethodEntity</param>
		protected AdyenPaymentMethodEntityBase(System.Int32 adyenPaymentMethodId, IValidator validator):base("AdyenPaymentMethodEntity")
		{
			InitClassFetch(adyenPaymentMethodId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AdyenPaymentMethodEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_adyenPaymentMethodBrandCollection = (Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection)info.GetValue("_adyenPaymentMethodBrandCollection", typeof(Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection));
			_alwaysFetchAdyenPaymentMethodBrandCollection = info.GetBoolean("_alwaysFetchAdyenPaymentMethodBrandCollection");
			_alreadyFetchedAdyenPaymentMethodBrandCollection = info.GetBoolean("_alreadyFetchedAdyenPaymentMethodBrandCollection");
			_paymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)info.GetValue("_paymentIntegrationConfigurationEntity", typeof(PaymentIntegrationConfigurationEntity));
			if(_paymentIntegrationConfigurationEntity!=null)
			{
				_paymentIntegrationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentIntegrationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_paymentIntegrationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchPaymentIntegrationConfigurationEntity = info.GetBoolean("_alwaysFetchPaymentIntegrationConfigurationEntity");
			_alreadyFetchedPaymentIntegrationConfigurationEntity = info.GetBoolean("_alreadyFetchedPaymentIntegrationConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AdyenPaymentMethodFieldIndex)fieldIndex)
			{
				case AdyenPaymentMethodFieldIndex.PaymentIntegrationConfigurationId:
					DesetupSyncPaymentIntegrationConfigurationEntity(true, false);
					_alreadyFetchedPaymentIntegrationConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdyenPaymentMethodBrandCollection = (_adyenPaymentMethodBrandCollection.Count > 0);
			_alreadyFetchedPaymentIntegrationConfigurationEntity = (_paymentIntegrationConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PaymentIntegrationConfigurationEntity":
					toReturn.Add(Relations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
					break;
				case "AdyenPaymentMethodBrandCollection":
					toReturn.Add(Relations.AdyenPaymentMethodBrandEntityUsingAdyenPaymentMethodId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_adyenPaymentMethodBrandCollection", (!this.MarkedForDeletion?_adyenPaymentMethodBrandCollection:null));
			info.AddValue("_alwaysFetchAdyenPaymentMethodBrandCollection", _alwaysFetchAdyenPaymentMethodBrandCollection);
			info.AddValue("_alreadyFetchedAdyenPaymentMethodBrandCollection", _alreadyFetchedAdyenPaymentMethodBrandCollection);
			info.AddValue("_paymentIntegrationConfigurationEntity", (!this.MarkedForDeletion?_paymentIntegrationConfigurationEntity:null));
			info.AddValue("_paymentIntegrationConfigurationEntityReturnsNewIfNotFound", _paymentIntegrationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentIntegrationConfigurationEntity", _alwaysFetchPaymentIntegrationConfigurationEntity);
			info.AddValue("_alreadyFetchedPaymentIntegrationConfigurationEntity", _alreadyFetchedPaymentIntegrationConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PaymentIntegrationConfigurationEntity":
					_alreadyFetchedPaymentIntegrationConfigurationEntity = true;
					this.PaymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)entity;
					break;
				case "AdyenPaymentMethodBrandCollection":
					_alreadyFetchedAdyenPaymentMethodBrandCollection = true;
					if(entity!=null)
					{
						this.AdyenPaymentMethodBrandCollection.Add((AdyenPaymentMethodBrandEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PaymentIntegrationConfigurationEntity":
					SetupSyncPaymentIntegrationConfigurationEntity(relatedEntity);
					break;
				case "AdyenPaymentMethodBrandCollection":
					_adyenPaymentMethodBrandCollection.Add((AdyenPaymentMethodBrandEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PaymentIntegrationConfigurationEntity":
					DesetupSyncPaymentIntegrationConfigurationEntity(false, true);
					break;
				case "AdyenPaymentMethodBrandCollection":
					this.PerformRelatedEntityRemoval(_adyenPaymentMethodBrandCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_paymentIntegrationConfigurationEntity!=null)
			{
				toReturn.Add(_paymentIntegrationConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_adyenPaymentMethodBrandCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 adyenPaymentMethodId)
		{
			return FetchUsingPK(adyenPaymentMethodId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 adyenPaymentMethodId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(adyenPaymentMethodId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 adyenPaymentMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(adyenPaymentMethodId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 adyenPaymentMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(adyenPaymentMethodId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AdyenPaymentMethodId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AdyenPaymentMethodRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdyenPaymentMethodBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection GetMultiAdyenPaymentMethodBrandCollection(bool forceFetch)
		{
			return GetMultiAdyenPaymentMethodBrandCollection(forceFetch, _adyenPaymentMethodBrandCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdyenPaymentMethodBrandEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection GetMultiAdyenPaymentMethodBrandCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdyenPaymentMethodBrandCollection(forceFetch, _adyenPaymentMethodBrandCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection GetMultiAdyenPaymentMethodBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdyenPaymentMethodBrandCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodBrandEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection GetMultiAdyenPaymentMethodBrandCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdyenPaymentMethodBrandCollection || forceFetch || _alwaysFetchAdyenPaymentMethodBrandCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_adyenPaymentMethodBrandCollection);
				_adyenPaymentMethodBrandCollection.SuppressClearInGetMulti=!forceFetch;
				_adyenPaymentMethodBrandCollection.EntityFactoryToUse = entityFactoryToUse;
				_adyenPaymentMethodBrandCollection.GetMultiManyToOne(this, filter);
				_adyenPaymentMethodBrandCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdyenPaymentMethodBrandCollection = true;
			}
			return _adyenPaymentMethodBrandCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdyenPaymentMethodBrandCollection'. These settings will be taken into account
		/// when the property AdyenPaymentMethodBrandCollection is requested or GetMultiAdyenPaymentMethodBrandCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdyenPaymentMethodBrandCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_adyenPaymentMethodBrandCollection.SortClauses=sortClauses;
			_adyenPaymentMethodBrandCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PaymentIntegrationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentIntegrationConfigurationEntity' which is related to this entity.</returns>
		public PaymentIntegrationConfigurationEntity GetSinglePaymentIntegrationConfigurationEntity()
		{
			return GetSinglePaymentIntegrationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentIntegrationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentIntegrationConfigurationEntity' which is related to this entity.</returns>
		public virtual PaymentIntegrationConfigurationEntity GetSinglePaymentIntegrationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentIntegrationConfigurationEntity || forceFetch || _alwaysFetchPaymentIntegrationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
				PaymentIntegrationConfigurationEntity newEntity = new PaymentIntegrationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentIntegrationConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (PaymentIntegrationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentIntegrationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentIntegrationConfigurationEntity = newEntity;
				_alreadyFetchedPaymentIntegrationConfigurationEntity = fetchResult;
			}
			return _paymentIntegrationConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PaymentIntegrationConfigurationEntity", _paymentIntegrationConfigurationEntity);
			toReturn.Add("AdyenPaymentMethodBrandCollection", _adyenPaymentMethodBrandCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="validator">The validator object for this AdyenPaymentMethodEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 adyenPaymentMethodId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(adyenPaymentMethodId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_adyenPaymentMethodBrandCollection = new Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection();
			_adyenPaymentMethodBrandCollection.SetContainingEntityInfo(this, "AdyenPaymentMethodEntity");
			_paymentIntegrationConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdyenPaymentMethodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentIntegrationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _paymentIntegrationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentIntegrationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentIntegrationConfigurationEntity, new PropertyChangedEventHandler( OnPaymentIntegrationConfigurationEntityPropertyChanged ), "PaymentIntegrationConfigurationEntity", Obymobi.Data.RelationClasses.StaticAdyenPaymentMethodRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, true, signalRelatedEntity, "AdyenPaymentMethodCollection", resetFKFields, new int[] { (int)AdyenPaymentMethodFieldIndex.PaymentIntegrationConfigurationId } );		
			_paymentIntegrationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _paymentIntegrationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentIntegrationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_paymentIntegrationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncPaymentIntegrationConfigurationEntity(true, true);
				_paymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentIntegrationConfigurationEntity, new PropertyChangedEventHandler( OnPaymentIntegrationConfigurationEntityPropertyChanged ), "PaymentIntegrationConfigurationEntity", Obymobi.Data.RelationClasses.StaticAdyenPaymentMethodRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, true, ref _alreadyFetchedPaymentIntegrationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentIntegrationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 adyenPaymentMethodId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AdyenPaymentMethodFieldIndex.AdyenPaymentMethodId].ForcedCurrentValueWrite(adyenPaymentMethodId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAdyenPaymentMethodDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AdyenPaymentMethodEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AdyenPaymentMethodRelations Relations
		{
			get	{ return new AdyenPaymentMethodRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdyenPaymentMethodBrand' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdyenPaymentMethodBrandCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection(), (IEntityRelation)GetRelationsForField("AdyenPaymentMethodBrandCollection")[0], (int)Obymobi.Data.EntityType.AdyenPaymentMethodEntity, (int)Obymobi.Data.EntityType.AdyenPaymentMethodBrandEntity, 0, null, null, null, "AdyenPaymentMethodBrandCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentIntegrationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection(), (IEntityRelation)GetRelationsForField("PaymentIntegrationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.AdyenPaymentMethodEntity, (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, 0, null, null, null, "PaymentIntegrationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AdyenPaymentMethodId property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."AdyenPaymentMethodId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AdyenPaymentMethodId
		{
			get { return (System.Int32)GetValue((int)AdyenPaymentMethodFieldIndex.AdyenPaymentMethodId, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.AdyenPaymentMethodId, value, true); }
		}

		/// <summary> The PaymentIntegrationConfigurationId property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."PaymentIntegrationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PaymentIntegrationConfigurationId
		{
			get { return (System.Int32)GetValue((int)AdyenPaymentMethodFieldIndex.PaymentIntegrationConfigurationId, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.PaymentIntegrationConfigurationId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)AdyenPaymentMethodFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The Type property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AdyenPaymentMethod Type
		{
			get { return (Obymobi.Enums.AdyenPaymentMethod)GetValue((int)AdyenPaymentMethodFieldIndex.Type, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.Type, value, true); }
		}

		/// <summary> The Active property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."Active"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)AdyenPaymentMethodFieldIndex.Active, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.Active, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AdyenPaymentMethodFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)AdyenPaymentMethodFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)AdyenPaymentMethodFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity AdyenPaymentMethod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdyenPaymentMethod"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)AdyenPaymentMethodFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)AdyenPaymentMethodFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdyenPaymentMethodBrandEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdyenPaymentMethodBrandCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdyenPaymentMethodBrandCollection AdyenPaymentMethodBrandCollection
		{
			get	{ return GetMultiAdyenPaymentMethodBrandCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdyenPaymentMethodBrandCollection. When set to true, AdyenPaymentMethodBrandCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdyenPaymentMethodBrandCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdyenPaymentMethodBrandCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdyenPaymentMethodBrandCollection
		{
			get	{ return _alwaysFetchAdyenPaymentMethodBrandCollection; }
			set	{ _alwaysFetchAdyenPaymentMethodBrandCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdyenPaymentMethodBrandCollection already has been fetched. Setting this property to false when AdyenPaymentMethodBrandCollection has been fetched
		/// will clear the AdyenPaymentMethodBrandCollection collection well. Setting this property to true while AdyenPaymentMethodBrandCollection hasn't been fetched disables lazy loading for AdyenPaymentMethodBrandCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdyenPaymentMethodBrandCollection
		{
			get { return _alreadyFetchedAdyenPaymentMethodBrandCollection;}
			set 
			{
				if(_alreadyFetchedAdyenPaymentMethodBrandCollection && !value && (_adyenPaymentMethodBrandCollection != null))
				{
					_adyenPaymentMethodBrandCollection.Clear();
				}
				_alreadyFetchedAdyenPaymentMethodBrandCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PaymentIntegrationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentIntegrationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PaymentIntegrationConfigurationEntity PaymentIntegrationConfigurationEntity
		{
			get	{ return GetSinglePaymentIntegrationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentIntegrationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AdyenPaymentMethodCollection", "PaymentIntegrationConfigurationEntity", _paymentIntegrationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentIntegrationConfigurationEntity. When set to true, PaymentIntegrationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentIntegrationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSinglePaymentIntegrationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentIntegrationConfigurationEntity
		{
			get	{ return _alwaysFetchPaymentIntegrationConfigurationEntity; }
			set	{ _alwaysFetchPaymentIntegrationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentIntegrationConfigurationEntity already has been fetched. Setting this property to false when PaymentIntegrationConfigurationEntity has been fetched
		/// will set PaymentIntegrationConfigurationEntity to null as well. Setting this property to true while PaymentIntegrationConfigurationEntity hasn't been fetched disables lazy loading for PaymentIntegrationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentIntegrationConfigurationEntity
		{
			get { return _alreadyFetchedPaymentIntegrationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedPaymentIntegrationConfigurationEntity && !value)
				{
					this.PaymentIntegrationConfigurationEntity = null;
				}
				_alreadyFetchedPaymentIntegrationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentIntegrationConfigurationEntity is not found
		/// in the database. When set to true, PaymentIntegrationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PaymentIntegrationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _paymentIntegrationConfigurationEntityReturnsNewIfNotFound; }
			set { _paymentIntegrationConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AdyenPaymentMethodEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
