﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Customer'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CustomerEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CustomerEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.DeviceCollection	_deviceCollection;
		private bool	_alwaysFetchDeviceCollection, _alreadyFetchedDeviceCollection;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessageRecipientCollection	_messageRecipientCollection;
		private bool	_alwaysFetchMessageRecipientCollection, _alreadyFetchedMessageRecipientCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_receivedNetmessageCollection;
		private bool	_alwaysFetchReceivedNetmessageCollection, _alreadyFetchedReceivedNetmessageCollection;
		private Obymobi.Data.CollectionClasses.NetmessageCollection	_sentNetmessageCollection;
		private bool	_alwaysFetchSentNetmessageCollection, _alreadyFetchedSentNetmessageCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private Obymobi.Data.CollectionClasses.SurveyResultCollection	_surveyResultCollection;
		private bool	_alwaysFetchSurveyResultCollection, _alreadyFetchedSurveyResultCollection;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMessage;
		private bool	_alwaysFetchCategoryCollectionViaMessage, _alreadyFetchedCategoryCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaOrder;
		private bool	_alwaysFetchCompanyCollectionViaOrder, _alreadyFetchedCompanyCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaMessage;
		private bool	_alwaysFetchDeliverypointCollectionViaMessage, _alreadyFetchedDeliverypointCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaNetmessage;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaNetmessage, _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaNetmessage_;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaNetmessage_, _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMessage;
		private bool	_alwaysFetchEntertainmentCollectionViaMessage, _alreadyFetchedEntertainmentCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.MediaCollection _mediaCollectionViaMessage;
		private bool	_alwaysFetchMediaCollectionViaMessage, _alreadyFetchedMediaCollectionViaMessage;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrder;
		private bool	_alwaysFetchOrderCollectionViaOrder, _alreadyFetchedOrderCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage, _alreadyFetchedTerminalCollectionViaNetmessage;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage_;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage_, _alreadyFetchedTerminalCollectionViaNetmessage_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage__;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage__, _alreadyFetchedTerminalCollectionViaNetmessage__;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaNetmessage___;
		private bool	_alwaysFetchTerminalCollectionViaNetmessage___, _alreadyFetchedTerminalCollectionViaNetmessage___;
		private ClientEntity _clientEntity;
		private bool	_alwaysFetchClientEntity, _alreadyFetchedClientEntity, _clientEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientEntity</summary>
			public static readonly string ClientEntity = "ClientEntity";
			/// <summary>Member name DeviceCollection</summary>
			public static readonly string DeviceCollection = "DeviceCollection";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessageRecipientCollection</summary>
			public static readonly string MessageRecipientCollection = "MessageRecipientCollection";
			/// <summary>Member name ReceivedNetmessageCollection</summary>
			public static readonly string ReceivedNetmessageCollection = "ReceivedNetmessageCollection";
			/// <summary>Member name SentNetmessageCollection</summary>
			public static readonly string SentNetmessageCollection = "SentNetmessageCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name SurveyResultCollection</summary>
			public static readonly string SurveyResultCollection = "SurveyResultCollection";
			/// <summary>Member name CategoryCollectionViaMessage</summary>
			public static readonly string CategoryCollectionViaMessage = "CategoryCollectionViaMessage";
			/// <summary>Member name CompanyCollectionViaOrder</summary>
			public static readonly string CompanyCollectionViaOrder = "CompanyCollectionViaOrder";
			/// <summary>Member name DeliverypointCollectionViaMessage</summary>
			public static readonly string DeliverypointCollectionViaMessage = "DeliverypointCollectionViaMessage";
			/// <summary>Member name DeliverypointgroupCollectionViaNetmessage</summary>
			public static readonly string DeliverypointgroupCollectionViaNetmessage = "DeliverypointgroupCollectionViaNetmessage";
			/// <summary>Member name DeliverypointgroupCollectionViaNetmessage_</summary>
			public static readonly string DeliverypointgroupCollectionViaNetmessage_ = "DeliverypointgroupCollectionViaNetmessage_";
			/// <summary>Member name EntertainmentCollectionViaMessage</summary>
			public static readonly string EntertainmentCollectionViaMessage = "EntertainmentCollectionViaMessage";
			/// <summary>Member name MediaCollectionViaMessage</summary>
			public static readonly string MediaCollectionViaMessage = "MediaCollectionViaMessage";
			/// <summary>Member name OrderCollectionViaOrder</summary>
			public static readonly string OrderCollectionViaOrder = "OrderCollectionViaOrder";
			/// <summary>Member name TerminalCollectionViaNetmessage</summary>
			public static readonly string TerminalCollectionViaNetmessage = "TerminalCollectionViaNetmessage";
			/// <summary>Member name TerminalCollectionViaNetmessage_</summary>
			public static readonly string TerminalCollectionViaNetmessage_ = "TerminalCollectionViaNetmessage_";
			/// <summary>Member name TerminalCollectionViaNetmessage__</summary>
			public static readonly string TerminalCollectionViaNetmessage__ = "TerminalCollectionViaNetmessage__";
			/// <summary>Member name TerminalCollectionViaNetmessage___</summary>
			public static readonly string TerminalCollectionViaNetmessage___ = "TerminalCollectionViaNetmessage___";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomerEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CustomerEntityBase() :base("CustomerEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		protected CustomerEntityBase(System.Int32 customerId):base("CustomerEntity")
		{
			InitClassFetch(customerId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CustomerEntityBase(System.Int32 customerId, IPrefetchPath prefetchPathToUse): base("CustomerEntity")
		{
			InitClassFetch(customerId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="validator">The custom validator object for this CustomerEntity</param>
		protected CustomerEntityBase(System.Int32 customerId, IValidator validator):base("CustomerEntity")
		{
			InitClassFetch(customerId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomerEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deviceCollection = (Obymobi.Data.CollectionClasses.DeviceCollection)info.GetValue("_deviceCollection", typeof(Obymobi.Data.CollectionClasses.DeviceCollection));
			_alwaysFetchDeviceCollection = info.GetBoolean("_alwaysFetchDeviceCollection");
			_alreadyFetchedDeviceCollection = info.GetBoolean("_alreadyFetchedDeviceCollection");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messageRecipientCollection = (Obymobi.Data.CollectionClasses.MessageRecipientCollection)info.GetValue("_messageRecipientCollection", typeof(Obymobi.Data.CollectionClasses.MessageRecipientCollection));
			_alwaysFetchMessageRecipientCollection = info.GetBoolean("_alwaysFetchMessageRecipientCollection");
			_alreadyFetchedMessageRecipientCollection = info.GetBoolean("_alreadyFetchedMessageRecipientCollection");

			_receivedNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_receivedNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchReceivedNetmessageCollection = info.GetBoolean("_alwaysFetchReceivedNetmessageCollection");
			_alreadyFetchedReceivedNetmessageCollection = info.GetBoolean("_alreadyFetchedReceivedNetmessageCollection");

			_sentNetmessageCollection = (Obymobi.Data.CollectionClasses.NetmessageCollection)info.GetValue("_sentNetmessageCollection", typeof(Obymobi.Data.CollectionClasses.NetmessageCollection));
			_alwaysFetchSentNetmessageCollection = info.GetBoolean("_alwaysFetchSentNetmessageCollection");
			_alreadyFetchedSentNetmessageCollection = info.GetBoolean("_alreadyFetchedSentNetmessageCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");

			_surveyResultCollection = (Obymobi.Data.CollectionClasses.SurveyResultCollection)info.GetValue("_surveyResultCollection", typeof(Obymobi.Data.CollectionClasses.SurveyResultCollection));
			_alwaysFetchSurveyResultCollection = info.GetBoolean("_alwaysFetchSurveyResultCollection");
			_alreadyFetchedSurveyResultCollection = info.GetBoolean("_alreadyFetchedSurveyResultCollection");
			_categoryCollectionViaMessage = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMessage = info.GetBoolean("_alwaysFetchCategoryCollectionViaMessage");
			_alreadyFetchedCategoryCollectionViaMessage = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMessage");

			_companyCollectionViaOrder = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaOrder = info.GetBoolean("_alwaysFetchCompanyCollectionViaOrder");
			_alreadyFetchedCompanyCollectionViaOrder = info.GetBoolean("_alreadyFetchedCompanyCollectionViaOrder");

			_deliverypointCollectionViaMessage = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaMessage = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaMessage");
			_alreadyFetchedDeliverypointCollectionViaMessage = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaMessage");

			_deliverypointgroupCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaNetmessage");
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage");

			_deliverypointgroupCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaNetmessage_");
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_");

			_entertainmentCollectionViaMessage = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMessage = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMessage");
			_alreadyFetchedEntertainmentCollectionViaMessage = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMessage");

			_mediaCollectionViaMessage = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollectionViaMessage", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollectionViaMessage = info.GetBoolean("_alwaysFetchMediaCollectionViaMessage");
			_alreadyFetchedMediaCollectionViaMessage = info.GetBoolean("_alreadyFetchedMediaCollectionViaMessage");

			_orderCollectionViaOrder = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrder = info.GetBoolean("_alwaysFetchOrderCollectionViaOrder");
			_alreadyFetchedOrderCollectionViaOrder = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrder");

			_terminalCollectionViaNetmessage = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage");
			_alreadyFetchedTerminalCollectionViaNetmessage = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage");

			_terminalCollectionViaNetmessage_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage_");
			_alreadyFetchedTerminalCollectionViaNetmessage_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage_");

			_terminalCollectionViaNetmessage__ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage__", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage__ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage__");
			_alreadyFetchedTerminalCollectionViaNetmessage__ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage__");

			_terminalCollectionViaNetmessage___ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaNetmessage___", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaNetmessage___ = info.GetBoolean("_alwaysFetchTerminalCollectionViaNetmessage___");
			_alreadyFetchedTerminalCollectionViaNetmessage___ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaNetmessage___");
			_clientEntity = (ClientEntity)info.GetValue("_clientEntity", typeof(ClientEntity));
			if(_clientEntity!=null)
			{
				_clientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientEntityReturnsNewIfNotFound = info.GetBoolean("_clientEntityReturnsNewIfNotFound");
			_alwaysFetchClientEntity = info.GetBoolean("_alwaysFetchClientEntity");
			_alreadyFetchedClientEntity = info.GetBoolean("_alreadyFetchedClientEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CustomerFieldIndex)fieldIndex)
			{
				case CustomerFieldIndex.ClientId:
					DesetupSyncClientEntity(true, false);
					_alreadyFetchedClientEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeviceCollection = (_deviceCollection.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessageRecipientCollection = (_messageRecipientCollection.Count > 0);
			_alreadyFetchedReceivedNetmessageCollection = (_receivedNetmessageCollection.Count > 0);
			_alreadyFetchedSentNetmessageCollection = (_sentNetmessageCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedSurveyResultCollection = (_surveyResultCollection.Count > 0);
			_alreadyFetchedCategoryCollectionViaMessage = (_categoryCollectionViaMessage.Count > 0);
			_alreadyFetchedCompanyCollectionViaOrder = (_companyCollectionViaOrder.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaMessage = (_deliverypointCollectionViaMessage.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = (_deliverypointgroupCollectionViaNetmessage.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = (_deliverypointgroupCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMessage = (_entertainmentCollectionViaMessage.Count > 0);
			_alreadyFetchedMediaCollectionViaMessage = (_mediaCollectionViaMessage.Count > 0);
			_alreadyFetchedOrderCollectionViaOrder = (_orderCollectionViaOrder.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage = (_terminalCollectionViaNetmessage.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage_ = (_terminalCollectionViaNetmessage_.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage__ = (_terminalCollectionViaNetmessage__.Count > 0);
			_alreadyFetchedTerminalCollectionViaNetmessage___ = (_terminalCollectionViaNetmessage___.Count > 0);
			_alreadyFetchedClientEntity = (_clientEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientEntity":
					toReturn.Add(Relations.ClientEntityUsingClientId);
					break;
				case "DeviceCollection":
					toReturn.Add(Relations.DeviceEntityUsingCustomerId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingCustomerId);
					break;
				case "MessageRecipientCollection":
					toReturn.Add(Relations.MessageRecipientEntityUsingCustomerId);
					break;
				case "ReceivedNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverCustomerId);
					break;
				case "SentNetmessageCollection":
					toReturn.Add(Relations.NetmessageEntityUsingSenderCustomerId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingCustomerId);
					break;
				case "SurveyResultCollection":
					toReturn.Add(Relations.SurveyResultEntityUsingCustomerId);
					break;
				case "CategoryCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCustomerId, "CustomerEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.CategoryEntityUsingCategoryId, "Message_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingCustomerId, "CustomerEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.CompanyEntityUsingCompanyId, "Order_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCustomerId, "CustomerEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Message_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverCustomerId, "CustomerEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingSenderCustomerId, "CustomerEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCustomerId, "CustomerEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Message_", string.Empty, JoinHint.None);
					break;
				case "MediaCollectionViaMessage":
					toReturn.Add(Relations.MessageEntityUsingCustomerId, "CustomerEntity__", "Message_", JoinHint.None);
					toReturn.Add(MessageEntity.Relations.MediaEntityUsingMediaId, "Message_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingCustomerId, "CustomerEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.OrderEntityUsingMasterOrderId, "Order_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage":
					toReturn.Add(Relations.NetmessageEntityUsingSenderCustomerId, "CustomerEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingSenderTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage_":
					toReturn.Add(Relations.NetmessageEntityUsingSenderCustomerId, "CustomerEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingReceiverTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage__":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverCustomerId, "CustomerEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingSenderTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaNetmessage___":
					toReturn.Add(Relations.NetmessageEntityUsingReceiverCustomerId, "CustomerEntity__", "Netmessage_", JoinHint.None);
					toReturn.Add(NetmessageEntity.Relations.TerminalEntityUsingReceiverTerminalId, "Netmessage_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deviceCollection", (!this.MarkedForDeletion?_deviceCollection:null));
			info.AddValue("_alwaysFetchDeviceCollection", _alwaysFetchDeviceCollection);
			info.AddValue("_alreadyFetchedDeviceCollection", _alreadyFetchedDeviceCollection);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messageRecipientCollection", (!this.MarkedForDeletion?_messageRecipientCollection:null));
			info.AddValue("_alwaysFetchMessageRecipientCollection", _alwaysFetchMessageRecipientCollection);
			info.AddValue("_alreadyFetchedMessageRecipientCollection", _alreadyFetchedMessageRecipientCollection);
			info.AddValue("_receivedNetmessageCollection", (!this.MarkedForDeletion?_receivedNetmessageCollection:null));
			info.AddValue("_alwaysFetchReceivedNetmessageCollection", _alwaysFetchReceivedNetmessageCollection);
			info.AddValue("_alreadyFetchedReceivedNetmessageCollection", _alreadyFetchedReceivedNetmessageCollection);
			info.AddValue("_sentNetmessageCollection", (!this.MarkedForDeletion?_sentNetmessageCollection:null));
			info.AddValue("_alwaysFetchSentNetmessageCollection", _alwaysFetchSentNetmessageCollection);
			info.AddValue("_alreadyFetchedSentNetmessageCollection", _alreadyFetchedSentNetmessageCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_surveyResultCollection", (!this.MarkedForDeletion?_surveyResultCollection:null));
			info.AddValue("_alwaysFetchSurveyResultCollection", _alwaysFetchSurveyResultCollection);
			info.AddValue("_alreadyFetchedSurveyResultCollection", _alreadyFetchedSurveyResultCollection);
			info.AddValue("_categoryCollectionViaMessage", (!this.MarkedForDeletion?_categoryCollectionViaMessage:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMessage", _alwaysFetchCategoryCollectionViaMessage);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMessage", _alreadyFetchedCategoryCollectionViaMessage);
			info.AddValue("_companyCollectionViaOrder", (!this.MarkedForDeletion?_companyCollectionViaOrder:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaOrder", _alwaysFetchCompanyCollectionViaOrder);
			info.AddValue("_alreadyFetchedCompanyCollectionViaOrder", _alreadyFetchedCompanyCollectionViaOrder);
			info.AddValue("_deliverypointCollectionViaMessage", (!this.MarkedForDeletion?_deliverypointCollectionViaMessage:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaMessage", _alwaysFetchDeliverypointCollectionViaMessage);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaMessage", _alreadyFetchedDeliverypointCollectionViaMessage);
			info.AddValue("_deliverypointgroupCollectionViaNetmessage", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaNetmessage", _alwaysFetchDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage", _alreadyFetchedDeliverypointgroupCollectionViaNetmessage);
			info.AddValue("_deliverypointgroupCollectionViaNetmessage_", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaNetmessage_", _alwaysFetchDeliverypointgroupCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_", _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_);
			info.AddValue("_entertainmentCollectionViaMessage", (!this.MarkedForDeletion?_entertainmentCollectionViaMessage:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMessage", _alwaysFetchEntertainmentCollectionViaMessage);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMessage", _alreadyFetchedEntertainmentCollectionViaMessage);
			info.AddValue("_mediaCollectionViaMessage", (!this.MarkedForDeletion?_mediaCollectionViaMessage:null));
			info.AddValue("_alwaysFetchMediaCollectionViaMessage", _alwaysFetchMediaCollectionViaMessage);
			info.AddValue("_alreadyFetchedMediaCollectionViaMessage", _alreadyFetchedMediaCollectionViaMessage);
			info.AddValue("_orderCollectionViaOrder", (!this.MarkedForDeletion?_orderCollectionViaOrder:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrder", _alwaysFetchOrderCollectionViaOrder);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrder", _alreadyFetchedOrderCollectionViaOrder);
			info.AddValue("_terminalCollectionViaNetmessage", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage", _alwaysFetchTerminalCollectionViaNetmessage);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage", _alreadyFetchedTerminalCollectionViaNetmessage);
			info.AddValue("_terminalCollectionViaNetmessage_", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage_", _alwaysFetchTerminalCollectionViaNetmessage_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage_", _alreadyFetchedTerminalCollectionViaNetmessage_);
			info.AddValue("_terminalCollectionViaNetmessage__", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage__:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage__", _alwaysFetchTerminalCollectionViaNetmessage__);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage__", _alreadyFetchedTerminalCollectionViaNetmessage__);
			info.AddValue("_terminalCollectionViaNetmessage___", (!this.MarkedForDeletion?_terminalCollectionViaNetmessage___:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaNetmessage___", _alwaysFetchTerminalCollectionViaNetmessage___);
			info.AddValue("_alreadyFetchedTerminalCollectionViaNetmessage___", _alreadyFetchedTerminalCollectionViaNetmessage___);
			info.AddValue("_clientEntity", (!this.MarkedForDeletion?_clientEntity:null));
			info.AddValue("_clientEntityReturnsNewIfNotFound", _clientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientEntity", _alwaysFetchClientEntity);
			info.AddValue("_alreadyFetchedClientEntity", _alreadyFetchedClientEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientEntity":
					_alreadyFetchedClientEntity = true;
					this.ClientEntity = (ClientEntity)entity;
					break;
				case "DeviceCollection":
					_alreadyFetchedDeviceCollection = true;
					if(entity!=null)
					{
						this.DeviceCollection.Add((DeviceEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessageRecipientCollection":
					_alreadyFetchedMessageRecipientCollection = true;
					if(entity!=null)
					{
						this.MessageRecipientCollection.Add((MessageRecipientEntity)entity);
					}
					break;
				case "ReceivedNetmessageCollection":
					_alreadyFetchedReceivedNetmessageCollection = true;
					if(entity!=null)
					{
						this.ReceivedNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "SentNetmessageCollection":
					_alreadyFetchedSentNetmessageCollection = true;
					if(entity!=null)
					{
						this.SentNetmessageCollection.Add((NetmessageEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				case "SurveyResultCollection":
					_alreadyFetchedSurveyResultCollection = true;
					if(entity!=null)
					{
						this.SurveyResultCollection.Add((SurveyResultEntity)entity);
					}
					break;
				case "CategoryCollectionViaMessage":
					_alreadyFetchedCategoryCollectionViaMessage = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMessage.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaOrder":
					_alreadyFetchedCompanyCollectionViaOrder = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaOrder.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaMessage":
					_alreadyFetchedDeliverypointCollectionViaMessage = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaMessage.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaNetmessage":
					_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaNetmessage.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaNetmessage_":
					_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaNetmessage_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMessage":
					_alreadyFetchedEntertainmentCollectionViaMessage = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMessage.Add((EntertainmentEntity)entity);
					}
					break;
				case "MediaCollectionViaMessage":
					_alreadyFetchedMediaCollectionViaMessage = true;
					if(entity!=null)
					{
						this.MediaCollectionViaMessage.Add((MediaEntity)entity);
					}
					break;
				case "OrderCollectionViaOrder":
					_alreadyFetchedOrderCollectionViaOrder = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrder.Add((OrderEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage":
					_alreadyFetchedTerminalCollectionViaNetmessage = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage_":
					_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage_.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage__":
					_alreadyFetchedTerminalCollectionViaNetmessage__ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage__.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaNetmessage___":
					_alreadyFetchedTerminalCollectionViaNetmessage___ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaNetmessage___.Add((TerminalEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					SetupSyncClientEntity(relatedEntity);
					break;
				case "DeviceCollection":
					_deviceCollection.Add((DeviceEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessageRecipientCollection":
					_messageRecipientCollection.Add((MessageRecipientEntity)relatedEntity);
					break;
				case "ReceivedNetmessageCollection":
					_receivedNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "SentNetmessageCollection":
					_sentNetmessageCollection.Add((NetmessageEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				case "SurveyResultCollection":
					_surveyResultCollection.Add((SurveyResultEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					DesetupSyncClientEntity(false, true);
					break;
				case "DeviceCollection":
					this.PerformRelatedEntityRemoval(_deviceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageRecipientCollection":
					this.PerformRelatedEntityRemoval(_messageRecipientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReceivedNetmessageCollection":
					this.PerformRelatedEntityRemoval(_receivedNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SentNetmessageCollection":
					this.PerformRelatedEntityRemoval(_sentNetmessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyResultCollection":
					this.PerformRelatedEntityRemoval(_surveyResultCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_clientEntity!=null)
			{
				toReturn.Add(_clientEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_deviceCollection);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messageRecipientCollection);
			toReturn.Add(_receivedNetmessageCollection);
			toReturn.Add(_sentNetmessageCollection);
			toReturn.Add(_orderCollection);
			toReturn.Add(_surveyResultCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customerId)
		{
			return FetchUsingPK(customerId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customerId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customerId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customerId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customerId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 customerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customerId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomerId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomerRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollection(bool forceFetch)
		{
			return GetMultiDeviceCollection(forceFetch, _deviceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceCollection(forceFetch, _deviceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection GetMultiDeviceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceCollection || forceFetch || _alwaysFetchDeviceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCollection);
				_deviceCollection.SuppressClearInGetMulti=!forceFetch;
				_deviceCollection.EntityFactoryToUse = entityFactoryToUse;
				_deviceCollection.GetMultiManyToOne(this, filter);
				_deviceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCollection = true;
			}
			return _deviceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCollection'. These settings will be taken into account
		/// when the property DeviceCollection is requested or GetMultiDeviceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCollection.SortClauses=sortClauses;
			_deviceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageRecipientEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageRecipientCollection(forceFetch, _messageRecipientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageRecipientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection GetMultiMessageRecipientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageRecipientCollection || forceFetch || _alwaysFetchMessageRecipientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageRecipientCollection);
				_messageRecipientCollection.SuppressClearInGetMulti=!forceFetch;
				_messageRecipientCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageRecipientCollection.GetMultiManyToOne(null, null, this, null, null, null, null, filter);
				_messageRecipientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageRecipientCollection = true;
			}
			return _messageRecipientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageRecipientCollection'. These settings will be taken into account
		/// when the property MessageRecipientCollection is requested or GetMultiMessageRecipientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageRecipientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageRecipientCollection.SortClauses=sortClauses;
			_messageRecipientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, _receivedNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReceivedNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiReceivedNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReceivedNetmessageCollection || forceFetch || _alwaysFetchReceivedNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_receivedNetmessageCollection);
				_receivedNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_receivedNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_receivedNetmessageCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, filter);
				_receivedNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedReceivedNetmessageCollection = true;
			}
			return _receivedNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReceivedNetmessageCollection'. These settings will be taken into account
		/// when the property ReceivedNetmessageCollection is requested or GetMultiReceivedNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReceivedNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_receivedNetmessageCollection.SortClauses=sortClauses;
			_receivedNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NetmessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSentNetmessageCollection(forceFetch, _sentNetmessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSentNetmessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection GetMultiSentNetmessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSentNetmessageCollection || forceFetch || _alwaysFetchSentNetmessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sentNetmessageCollection);
				_sentNetmessageCollection.SuppressClearInGetMulti=!forceFetch;
				_sentNetmessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_sentNetmessageCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, filter);
				_sentNetmessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSentNetmessageCollection = true;
			}
			return _sentNetmessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SentNetmessageCollection'. These settings will be taken into account
		/// when the property SentNetmessageCollection is requested or GetMultiSentNetmessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSentNetmessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sentNetmessageCollection.SortClauses=sortClauses;
			_sentNetmessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyResultCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyResultCollection || forceFetch || _alwaysFetchSurveyResultCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyResultCollection);
				_surveyResultCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyResultCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyResultCollection.GetMultiManyToOne(null, this, null, null, filter);
				_surveyResultCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyResultCollection = true;
			}
			return _surveyResultCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyResultCollection'. These settings will be taken into account
		/// when the property SurveyResultCollection is requested or GetMultiSurveyResultCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyResultCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyResultCollection.SortClauses=sortClauses;
			_surveyResultCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMessage(forceFetch, _categoryCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMessage || forceFetch || _alwaysFetchCategoryCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMessage.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMessage"));
				_categoryCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMessage = true;
			}
			return _categoryCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMessage'. These settings will be taken into account
		/// when the property CategoryCollectionViaMessage is requested or GetMultiCategoryCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMessage.SortClauses=sortClauses;
			_categoryCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaOrder(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaOrder(forceFetch, _companyCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaOrder || forceFetch || _alwaysFetchCompanyCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_companyCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaOrder.GetMulti(filter, GetRelationsForField("CompanyCollectionViaOrder"));
				_companyCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaOrder = true;
			}
			return _companyCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaOrder'. These settings will be taken into account
		/// when the property CompanyCollectionViaOrder is requested or GetMultiCompanyCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaOrder.SortClauses=sortClauses;
			_companyCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaMessage(forceFetch, _deliverypointCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaMessage || forceFetch || _alwaysFetchDeliverypointCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaMessage.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaMessage"));
				_deliverypointCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaMessage = true;
			}
			return _deliverypointCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaMessage'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaMessage is requested or GetMultiDeliverypointCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaMessage.SortClauses=sortClauses;
			_deliverypointCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaNetmessage(forceFetch, _deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaNetmessage || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"));
				_deliverypointgroupCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = true;
			}
			return _deliverypointgroupCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaNetmessage'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaNetmessage is requested or GetMultiDeliverypointgroupCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaNetmessage.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaNetmessage_(forceFetch, _deliverypointgroupCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_deliverypointgroupCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage_"));
				_deliverypointgroupCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = true;
			}
			return _deliverypointgroupCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaNetmessage_ is requested or GetMultiDeliverypointgroupCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaNetmessage_.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMessage(forceFetch, _entertainmentCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMessage || forceFetch || _alwaysFetchEntertainmentCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMessage.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMessage"));
				_entertainmentCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMessage = true;
			}
			return _entertainmentCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMessage'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMessage is requested or GetMultiEntertainmentCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMessage.SortClauses=sortClauses;
			_entertainmentCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch)
		{
			return GetMultiMediaCollectionViaMessage(forceFetch, _mediaCollectionViaMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollectionViaMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMediaCollectionViaMessage || forceFetch || _alwaysFetchMediaCollectionViaMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollectionViaMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=!forceFetch;
				_mediaCollectionViaMessage.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollectionViaMessage.GetMulti(filter, GetRelationsForField("MediaCollectionViaMessage"));
				_mediaCollectionViaMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollectionViaMessage = true;
			}
			return _mediaCollectionViaMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollectionViaMessage'. These settings will be taken into account
		/// when the property MediaCollectionViaMessage is requested or GetMultiMediaCollectionViaMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollectionViaMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollectionViaMessage.SortClauses=sortClauses;
			_mediaCollectionViaMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrder(forceFetch, _orderCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrder || forceFetch || _alwaysFetchOrderCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrder.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrder"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrder = true;
			}
			return _orderCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrder'. These settings will be taken into account
		/// when the property OrderCollectionViaOrder is requested or GetMultiOrderCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrder.SortClauses=sortClauses;
			_orderCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage(forceFetch, _terminalCollectionViaNetmessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage"));
				_terminalCollectionViaNetmessage.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage = true;
			}
			return _terminalCollectionViaNetmessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage is requested or GetMultiTerminalCollectionViaNetmessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage_(forceFetch, _terminalCollectionViaNetmessage_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage_ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage_"));
				_terminalCollectionViaNetmessage_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage_ = true;
			}
			return _terminalCollectionViaNetmessage_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage_'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage_ is requested or GetMultiTerminalCollectionViaNetmessage_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage_.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage__(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage__(forceFetch, _terminalCollectionViaNetmessage__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage__ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_terminalCollectionViaNetmessage__.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage__.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage__.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage__"));
				_terminalCollectionViaNetmessage__.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage__ = true;
			}
			return _terminalCollectionViaNetmessage__;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage__'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage__ is requested or GetMultiTerminalCollectionViaNetmessage__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage__.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage___(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaNetmessage___(forceFetch, _terminalCollectionViaNetmessage___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaNetmessage___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaNetmessage___ || forceFetch || _alwaysFetchTerminalCollectionViaNetmessage___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaNetmessage___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CustomerFields.CustomerId, ComparisonOperator.Equal, this.CustomerId, "CustomerEntity__"));
				_terminalCollectionViaNetmessage___.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaNetmessage___.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaNetmessage___.GetMulti(filter, GetRelationsForField("TerminalCollectionViaNetmessage___"));
				_terminalCollectionViaNetmessage___.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaNetmessage___ = true;
			}
			return _terminalCollectionViaNetmessage___;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaNetmessage___'. These settings will be taken into account
		/// when the property TerminalCollectionViaNetmessage___ is requested or GetMultiTerminalCollectionViaNetmessage___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaNetmessage___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaNetmessage___.SortClauses=sortClauses;
			_terminalCollectionViaNetmessage___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientEntity()
		{
			return GetSingleClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientEntity || forceFetch || _alwaysFetchClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientEntity = newEntity;
				_alreadyFetchedClientEntity = fetchResult;
			}
			return _clientEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientEntity", _clientEntity);
			toReturn.Add("DeviceCollection", _deviceCollection);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessageRecipientCollection", _messageRecipientCollection);
			toReturn.Add("ReceivedNetmessageCollection", _receivedNetmessageCollection);
			toReturn.Add("SentNetmessageCollection", _sentNetmessageCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			toReturn.Add("SurveyResultCollection", _surveyResultCollection);
			toReturn.Add("CategoryCollectionViaMessage", _categoryCollectionViaMessage);
			toReturn.Add("CompanyCollectionViaOrder", _companyCollectionViaOrder);
			toReturn.Add("DeliverypointCollectionViaMessage", _deliverypointCollectionViaMessage);
			toReturn.Add("DeliverypointgroupCollectionViaNetmessage", _deliverypointgroupCollectionViaNetmessage);
			toReturn.Add("DeliverypointgroupCollectionViaNetmessage_", _deliverypointgroupCollectionViaNetmessage_);
			toReturn.Add("EntertainmentCollectionViaMessage", _entertainmentCollectionViaMessage);
			toReturn.Add("MediaCollectionViaMessage", _mediaCollectionViaMessage);
			toReturn.Add("OrderCollectionViaOrder", _orderCollectionViaOrder);
			toReturn.Add("TerminalCollectionViaNetmessage", _terminalCollectionViaNetmessage);
			toReturn.Add("TerminalCollectionViaNetmessage_", _terminalCollectionViaNetmessage_);
			toReturn.Add("TerminalCollectionViaNetmessage__", _terminalCollectionViaNetmessage__);
			toReturn.Add("TerminalCollectionViaNetmessage___", _terminalCollectionViaNetmessage___);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="validator">The validator object for this CustomerEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 customerId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customerId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_deviceCollection = new Obymobi.Data.CollectionClasses.DeviceCollection();
			_deviceCollection.SetContainingEntityInfo(this, "CustomerEntity");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "CustomerEntity");

			_messageRecipientCollection = new Obymobi.Data.CollectionClasses.MessageRecipientCollection();
			_messageRecipientCollection.SetContainingEntityInfo(this, "CustomerEntity");

			_receivedNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_receivedNetmessageCollection.SetContainingEntityInfo(this, "RecieverCustomerEntity");

			_sentNetmessageCollection = new Obymobi.Data.CollectionClasses.NetmessageCollection();
			_sentNetmessageCollection.SetContainingEntityInfo(this, "SenderCustomerEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "CustomerEntity");

			_surveyResultCollection = new Obymobi.Data.CollectionClasses.SurveyResultCollection();
			_surveyResultCollection.SetContainingEntityInfo(this, "CustomerEntity");
			_categoryCollectionViaMessage = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaOrder = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointCollectionViaMessage = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaMessage = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_mediaCollectionViaMessage = new Obymobi.Data.CollectionClasses.MediaCollection();
			_orderCollectionViaOrder = new Obymobi.Data.CollectionClasses.OrderCollection();
			_terminalCollectionViaNetmessage = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage__ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaNetmessage___ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_clientEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Verified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Blacklisted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlacklistedNotes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Firstname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Lastname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastnamePrefix", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RandomValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Birthdate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Gender", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Zipcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmsRequestsSent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmsCreateRequests", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmsResetPincodeRequests", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmsUnlockAccountRequests", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmsNonProcessableRequests", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasChangedInititialPincode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InitialLinkIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordResetLinkIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FailedPasswordAttemptCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RememberOnMobileWebsite", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommunicationSalt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AnonymousAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordSalt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordResetLinkFailedAtttempts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CanSingleSignOn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GUID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NewEmailAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NewEmailAddressConfirmationGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NewEmailAddressVerified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CampaignContent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CampaignMedium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CampaignName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CampaignSource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PasswordResetLinkGeneratedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastFailedPasswordAttemptUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _clientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticCustomerRelations.ClientEntityUsingClientIdStatic, true, signalRelatedEntity, "CustomerCollection", resetFKFields, new int[] { (int)CustomerFieldIndex.ClientId } );		
			_clientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientEntity(IEntityCore relatedEntity)
		{
			if(_clientEntity!=relatedEntity)
			{		
				DesetupSyncClientEntity(true, true);
				_clientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticCustomerRelations.ClientEntityUsingClientIdStatic, true, ref _alreadyFetchedClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customerId">PK value for Customer which data should be fetched into this Customer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 customerId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomerFieldIndex.CustomerId].ForcedCurrentValueWrite(customerId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomerDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomerEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomerRelations Relations
		{
			get	{ return new CustomerRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceCollection(), (IEntityRelation)GetRelationsForField("DeviceCollection")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.DeviceEntity, 0, null, null, null, "DeviceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageRecipient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageRecipientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageRecipientCollection(), (IEntityRelation)GetRelationsForField("MessageRecipientCollection")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.MessageRecipientEntity, 0, null, null, null, "MessageRecipientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceivedNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("ReceivedNetmessageCollection")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "ReceivedNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Netmessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSentNetmessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NetmessageCollection(), (IEntityRelation)GetRelationsForField("SentNetmessageCollection")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.NetmessageEntity, 0, null, null, null, "SentNetmessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyResultCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyResultCollection(), (IEntityRelation)GetRelationsForField("SurveyResultCollection")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.SurveyResultEntity, 0, null, null, null, "SurveyResultCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMessage"), "CategoryCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaOrder"), "CompanyCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaMessage"), "DeliverypointCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage"), "DeliverypointgroupCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaNetmessage_"), "DeliverypointgroupCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMessage"), "EntertainmentCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollectionViaMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MessageEntityUsingCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Message_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, GetRelationsForField("MediaCollectionViaMessage"), "MediaCollectionViaMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrder"), "OrderCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage"), "TerminalCollectionViaNetmessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingSenderCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage_"), "TerminalCollectionViaNetmessage_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage__"), "TerminalCollectionViaNetmessage__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaNetmessage___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NetmessageEntityUsingReceiverCustomerId;
				intermediateRelation.SetAliases(string.Empty, "Netmessage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaNetmessage___"), "TerminalCollectionViaNetmessage___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientEntity")[0], (int)Obymobi.Data.EntityType.CustomerEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CustomerId property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CustomerId
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.CustomerId, true); }
			set	{ SetValue((int)CustomerFieldIndex.CustomerId, value, true); }
		}

		/// <summary> The Phonenumber property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Phonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)CustomerFieldIndex.Phonenumber, value, true); }
		}

		/// <summary> The Password property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Password"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 160<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Password, true); }
			set	{ SetValue((int)CustomerFieldIndex.Password, value, true); }
		}

		/// <summary> The Verified property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Verified"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Verified
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.Verified, true); }
			set	{ SetValue((int)CustomerFieldIndex.Verified, value, true); }
		}

		/// <summary> The Blacklisted property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Blacklisted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Blacklisted
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.Blacklisted, true); }
			set	{ SetValue((int)CustomerFieldIndex.Blacklisted, value, true); }
		}

		/// <summary> The BlacklistedNotes property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."BlacklistedNotes"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BlacklistedNotes
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.BlacklistedNotes, true); }
			set	{ SetValue((int)CustomerFieldIndex.BlacklistedNotes, value, true); }
		}

		/// <summary> The Firstname property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Firstname"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Firstname
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Firstname, true); }
			set	{ SetValue((int)CustomerFieldIndex.Firstname, value, true); }
		}

		/// <summary> The Lastname property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Lastname"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Lastname
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Lastname, true); }
			set	{ SetValue((int)CustomerFieldIndex.Lastname, value, true); }
		}

		/// <summary> The LastnamePrefix property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."LastnamePrefix"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastnamePrefix
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.LastnamePrefix, true); }
			set	{ SetValue((int)CustomerFieldIndex.LastnamePrefix, value, true); }
		}

		/// <summary> The RandomValue property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."RandomValue"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RandomValue
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.RandomValue, true); }
			set	{ SetValue((int)CustomerFieldIndex.RandomValue, value, true); }
		}

		/// <summary> The Birthdate property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Birthdate"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Birthdate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.Birthdate, false); }
			set	{ SetValue((int)CustomerFieldIndex.Birthdate, value, true); }
		}

		/// <summary> The Email property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Email, true); }
			set	{ SetValue((int)CustomerFieldIndex.Email, value, true); }
		}

		/// <summary> The Gender property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Gender"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Gender
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CustomerFieldIndex.Gender, false); }
			set	{ SetValue((int)CustomerFieldIndex.Gender, value, true); }
		}

		/// <summary> The Addressline1 property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Addressline1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline1
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Addressline1, true); }
			set	{ SetValue((int)CustomerFieldIndex.Addressline1, value, true); }
		}

		/// <summary> The Addressline2 property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Addressline2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline2
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Addressline2, true); }
			set	{ SetValue((int)CustomerFieldIndex.Addressline2, value, true); }
		}

		/// <summary> The Zipcode property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Zipcode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Zipcode
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Zipcode, true); }
			set	{ SetValue((int)CustomerFieldIndex.Zipcode, value, true); }
		}

		/// <summary> The City property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."City"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.City, true); }
			set	{ SetValue((int)CustomerFieldIndex.City, value, true); }
		}

		/// <summary> The Notes property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."Notes"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.Notes, true); }
			set	{ SetValue((int)CustomerFieldIndex.Notes, value, true); }
		}

		/// <summary> The SmsRequestsSent property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsRequestsSent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsRequestsSent
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsRequestsSent, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsRequestsSent, value, true); }
		}

		/// <summary> The SmsCreateRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsCreateRequests"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsCreateRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsCreateRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsCreateRequests, value, true); }
		}

		/// <summary> The SmsResetPincodeRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsResetPincodeRequests"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsResetPincodeRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsResetPincodeRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsResetPincodeRequests, value, true); }
		}

		/// <summary> The SmsUnlockAccountRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsUnlockAccountRequests"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsUnlockAccountRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsUnlockAccountRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsUnlockAccountRequests, value, true); }
		}

		/// <summary> The SmsNonProcessableRequests property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."SmsNonProcessableRequests"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SmsNonProcessableRequests
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.SmsNonProcessableRequests, true); }
			set	{ SetValue((int)CustomerFieldIndex.SmsNonProcessableRequests, value, true); }
		}

		/// <summary> The HasChangedInititialPincode property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."HasChangedInititialPincode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasChangedInititialPincode
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.HasChangedInititialPincode, true); }
			set	{ SetValue((int)CustomerFieldIndex.HasChangedInititialPincode, value, true); }
		}

		/// <summary> The InitialLinkIdentifier property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."InitialLinkIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InitialLinkIdentifier
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.InitialLinkIdentifier, true); }
			set	{ SetValue((int)CustomerFieldIndex.InitialLinkIdentifier, value, true); }
		}

		/// <summary> The PasswordResetLinkIdentifier property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordResetLinkIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PasswordResetLinkIdentifier
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.PasswordResetLinkIdentifier, true); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordResetLinkIdentifier, value, true); }
		}

		/// <summary> The FailedPasswordAttemptCount property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."FailedPasswordAttemptCount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FailedPasswordAttemptCount
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.FailedPasswordAttemptCount, true); }
			set	{ SetValue((int)CustomerFieldIndex.FailedPasswordAttemptCount, value, true); }
		}

		/// <summary> The RememberOnMobileWebsite property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."RememberOnMobileWebsite"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RememberOnMobileWebsite
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.RememberOnMobileWebsite, true); }
			set	{ SetValue((int)CustomerFieldIndex.RememberOnMobileWebsite, value, true); }
		}

		/// <summary> The ClientId property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)CustomerFieldIndex.ClientId, false); }
			set	{ SetValue((int)CustomerFieldIndex.ClientId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CustomerFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CustomerFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CommunicationSalt property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CommunicationSalt"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommunicationSalt
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CommunicationSalt, true); }
			set	{ SetValue((int)CustomerFieldIndex.CommunicationSalt, value, true); }
		}

		/// <summary> The AnonymousAccount property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."AnonymousAccount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AnonymousAccount
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.AnonymousAccount, true); }
			set	{ SetValue((int)CustomerFieldIndex.AnonymousAccount, value, true); }
		}

		/// <summary> The PasswordSalt property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordSalt"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 256<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PasswordSalt
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.PasswordSalt, true); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordSalt, value, true); }
		}

		/// <summary> The PasswordResetLinkFailedAtttempts property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordResetLinkFailedAtttempts"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PasswordResetLinkFailedAtttempts
		{
			get { return (System.Int32)GetValue((int)CustomerFieldIndex.PasswordResetLinkFailedAtttempts, true); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordResetLinkFailedAtttempts, value, true); }
		}

		/// <summary> The CountryCode property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CountryCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CountryCode, true); }
			set	{ SetValue((int)CustomerFieldIndex.CountryCode, value, true); }
		}

		/// <summary> The CanSingleSignOn property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CanSingleSignOn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CanSingleSignOn
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.CanSingleSignOn, true); }
			set	{ SetValue((int)CustomerFieldIndex.CanSingleSignOn, value, true); }
		}

		/// <summary> The GUID property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."GUID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GUID
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.GUID, true); }
			set	{ SetValue((int)CustomerFieldIndex.GUID, value, true); }
		}

		/// <summary> The NewEmailAddress property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."NewEmailAddress"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NewEmailAddress
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.NewEmailAddress, true); }
			set	{ SetValue((int)CustomerFieldIndex.NewEmailAddress, value, true); }
		}

		/// <summary> The NewEmailAddressConfirmationGuid property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."NewEmailAddressConfirmationGuid"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NewEmailAddressConfirmationGuid
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.NewEmailAddressConfirmationGuid, true); }
			set	{ SetValue((int)CustomerFieldIndex.NewEmailAddressConfirmationGuid, value, true); }
		}

		/// <summary> The NewEmailAddressVerified property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."NewEmailAddressVerified"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NewEmailAddressVerified
		{
			get { return (System.Boolean)GetValue((int)CustomerFieldIndex.NewEmailAddressVerified, true); }
			set	{ SetValue((int)CustomerFieldIndex.NewEmailAddressVerified, value, true); }
		}

		/// <summary> The CampaignContent property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignContent"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignContent
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignContent, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignContent, value, true); }
		}

		/// <summary> The CampaignMedium property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignMedium"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignMedium
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignMedium, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignMedium, value, true); }
		}

		/// <summary> The CampaignName property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignName"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignName
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignName, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignName, value, true); }
		}

		/// <summary> The CampaignSource property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CampaignSource"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CampaignSource
		{
			get { return (System.String)GetValue((int)CustomerFieldIndex.CampaignSource, true); }
			set	{ SetValue((int)CustomerFieldIndex.CampaignSource, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The PasswordResetLinkGeneratedUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."PasswordResetLinkGeneratedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PasswordResetLinkGeneratedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.PasswordResetLinkGeneratedUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.PasswordResetLinkGeneratedUTC, value, true); }
		}

		/// <summary> The LastFailedPasswordAttemptUTC property of the Entity Customer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Customer"."LastFailedPasswordAttemptUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastFailedPasswordAttemptUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomerFieldIndex.LastFailedPasswordAttemptUTC, false); }
			set	{ SetValue((int)CustomerFieldIndex.LastFailedPasswordAttemptUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceCollection DeviceCollection
		{
			get	{ return GetMultiDeviceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCollection. When set to true, DeviceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCollection
		{
			get	{ return _alwaysFetchDeviceCollection; }
			set	{ _alwaysFetchDeviceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCollection already has been fetched. Setting this property to false when DeviceCollection has been fetched
		/// will clear the DeviceCollection collection well. Setting this property to true while DeviceCollection hasn't been fetched disables lazy loading for DeviceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCollection
		{
			get { return _alreadyFetchedDeviceCollection;}
			set 
			{
				if(_alreadyFetchedDeviceCollection && !value && (_deviceCollection != null))
				{
					_deviceCollection.Clear();
				}
				_alreadyFetchedDeviceCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageRecipientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageRecipientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageRecipientCollection MessageRecipientCollection
		{
			get	{ return GetMultiMessageRecipientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageRecipientCollection. When set to true, MessageRecipientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageRecipientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageRecipientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageRecipientCollection
		{
			get	{ return _alwaysFetchMessageRecipientCollection; }
			set	{ _alwaysFetchMessageRecipientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageRecipientCollection already has been fetched. Setting this property to false when MessageRecipientCollection has been fetched
		/// will clear the MessageRecipientCollection collection well. Setting this property to true while MessageRecipientCollection hasn't been fetched disables lazy loading for MessageRecipientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageRecipientCollection
		{
			get { return _alreadyFetchedMessageRecipientCollection;}
			set 
			{
				if(_alreadyFetchedMessageRecipientCollection && !value && (_messageRecipientCollection != null))
				{
					_messageRecipientCollection.Clear();
				}
				_alreadyFetchedMessageRecipientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReceivedNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection ReceivedNetmessageCollection
		{
			get	{ return GetMultiReceivedNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReceivedNetmessageCollection. When set to true, ReceivedNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceivedNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiReceivedNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceivedNetmessageCollection
		{
			get	{ return _alwaysFetchReceivedNetmessageCollection; }
			set	{ _alwaysFetchReceivedNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceivedNetmessageCollection already has been fetched. Setting this property to false when ReceivedNetmessageCollection has been fetched
		/// will clear the ReceivedNetmessageCollection collection well. Setting this property to true while ReceivedNetmessageCollection hasn't been fetched disables lazy loading for ReceivedNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceivedNetmessageCollection
		{
			get { return _alreadyFetchedReceivedNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedReceivedNetmessageCollection && !value && (_receivedNetmessageCollection != null))
				{
					_receivedNetmessageCollection.Clear();
				}
				_alreadyFetchedReceivedNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NetmessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSentNetmessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NetmessageCollection SentNetmessageCollection
		{
			get	{ return GetMultiSentNetmessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SentNetmessageCollection. When set to true, SentNetmessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SentNetmessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSentNetmessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSentNetmessageCollection
		{
			get	{ return _alwaysFetchSentNetmessageCollection; }
			set	{ _alwaysFetchSentNetmessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SentNetmessageCollection already has been fetched. Setting this property to false when SentNetmessageCollection has been fetched
		/// will clear the SentNetmessageCollection collection well. Setting this property to true while SentNetmessageCollection hasn't been fetched disables lazy loading for SentNetmessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSentNetmessageCollection
		{
			get { return _alreadyFetchedSentNetmessageCollection;}
			set 
			{
				if(_alreadyFetchedSentNetmessageCollection && !value && (_sentNetmessageCollection != null))
				{
					_sentNetmessageCollection.Clear();
				}
				_alreadyFetchedSentNetmessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyResultCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection SurveyResultCollection
		{
			get	{ return GetMultiSurveyResultCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyResultCollection. When set to true, SurveyResultCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyResultCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyResultCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyResultCollection
		{
			get	{ return _alwaysFetchSurveyResultCollection; }
			set	{ _alwaysFetchSurveyResultCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyResultCollection already has been fetched. Setting this property to false when SurveyResultCollection has been fetched
		/// will clear the SurveyResultCollection collection well. Setting this property to true while SurveyResultCollection hasn't been fetched disables lazy loading for SurveyResultCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyResultCollection
		{
			get { return _alreadyFetchedSurveyResultCollection;}
			set 
			{
				if(_alreadyFetchedSurveyResultCollection && !value && (_surveyResultCollection != null))
				{
					_surveyResultCollection.Clear();
				}
				_alreadyFetchedSurveyResultCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMessage
		{
			get { return GetMultiCategoryCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMessage. When set to true, CategoryCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMessage
		{
			get	{ return _alwaysFetchCategoryCollectionViaMessage; }
			set	{ _alwaysFetchCategoryCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMessage already has been fetched. Setting this property to false when CategoryCollectionViaMessage has been fetched
		/// will clear the CategoryCollectionViaMessage collection well. Setting this property to true while CategoryCollectionViaMessage hasn't been fetched disables lazy loading for CategoryCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMessage
		{
			get { return _alreadyFetchedCategoryCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMessage && !value && (_categoryCollectionViaMessage != null))
				{
					_categoryCollectionViaMessage.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaOrder
		{
			get { return GetMultiCompanyCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaOrder. When set to true, CompanyCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaOrder
		{
			get	{ return _alwaysFetchCompanyCollectionViaOrder; }
			set	{ _alwaysFetchCompanyCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaOrder already has been fetched. Setting this property to false when CompanyCollectionViaOrder has been fetched
		/// will clear the CompanyCollectionViaOrder collection well. Setting this property to true while CompanyCollectionViaOrder hasn't been fetched disables lazy loading for CompanyCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaOrder
		{
			get { return _alreadyFetchedCompanyCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaOrder && !value && (_companyCollectionViaOrder != null))
				{
					_companyCollectionViaOrder.Clear();
				}
				_alreadyFetchedCompanyCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaMessage
		{
			get { return GetMultiDeliverypointCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaMessage. When set to true, DeliverypointCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaMessage
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaMessage; }
			set	{ _alwaysFetchDeliverypointCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaMessage already has been fetched. Setting this property to false when DeliverypointCollectionViaMessage has been fetched
		/// will clear the DeliverypointCollectionViaMessage collection well. Setting this property to true while DeliverypointCollectionViaMessage hasn't been fetched disables lazy loading for DeliverypointCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaMessage
		{
			get { return _alreadyFetchedDeliverypointCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaMessage && !value && (_deliverypointCollectionViaMessage != null))
				{
					_deliverypointCollectionViaMessage.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaNetmessage
		{
			get { return GetMultiDeliverypointgroupCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaNetmessage. When set to true, DeliverypointgroupCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaNetmessage
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaNetmessage; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaNetmessage already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaNetmessage has been fetched
		/// will clear the DeliverypointgroupCollectionViaNetmessage collection well. Setting this property to true while DeliverypointgroupCollectionViaNetmessage hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaNetmessage
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaNetmessage && !value && (_deliverypointgroupCollectionViaNetmessage != null))
				{
					_deliverypointgroupCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaNetmessage_
		{
			get { return GetMultiDeliverypointgroupCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaNetmessage_. When set to true, DeliverypointgroupCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaNetmessage_; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaNetmessage_ already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaNetmessage_ has been fetched
		/// will clear the DeliverypointgroupCollectionViaNetmessage_ collection well. Setting this property to true while DeliverypointgroupCollectionViaNetmessage_ hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaNetmessage_
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ && !value && (_deliverypointgroupCollectionViaNetmessage_ != null))
				{
					_deliverypointgroupCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMessage
		{
			get { return GetMultiEntertainmentCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMessage. When set to true, EntertainmentCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMessage
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMessage; }
			set	{ _alwaysFetchEntertainmentCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMessage already has been fetched. Setting this property to false when EntertainmentCollectionViaMessage has been fetched
		/// will clear the EntertainmentCollectionViaMessage collection well. Setting this property to true while EntertainmentCollectionViaMessage hasn't been fetched disables lazy loading for EntertainmentCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMessage
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMessage && !value && (_entertainmentCollectionViaMessage != null))
				{
					_entertainmentCollectionViaMessage.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollectionViaMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollectionViaMessage
		{
			get { return GetMultiMediaCollectionViaMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollectionViaMessage. When set to true, MediaCollectionViaMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollectionViaMessage is accessed. You can always execute a forced fetch by calling GetMultiMediaCollectionViaMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollectionViaMessage
		{
			get	{ return _alwaysFetchMediaCollectionViaMessage; }
			set	{ _alwaysFetchMediaCollectionViaMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollectionViaMessage already has been fetched. Setting this property to false when MediaCollectionViaMessage has been fetched
		/// will clear the MediaCollectionViaMessage collection well. Setting this property to true while MediaCollectionViaMessage hasn't been fetched disables lazy loading for MediaCollectionViaMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollectionViaMessage
		{
			get { return _alreadyFetchedMediaCollectionViaMessage;}
			set 
			{
				if(_alreadyFetchedMediaCollectionViaMessage && !value && (_mediaCollectionViaMessage != null))
				{
					_mediaCollectionViaMessage.Clear();
				}
				_alreadyFetchedMediaCollectionViaMessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrder
		{
			get { return GetMultiOrderCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrder. When set to true, OrderCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrder
		{
			get	{ return _alwaysFetchOrderCollectionViaOrder; }
			set	{ _alwaysFetchOrderCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrder already has been fetched. Setting this property to false when OrderCollectionViaOrder has been fetched
		/// will clear the OrderCollectionViaOrder collection well. Setting this property to true while OrderCollectionViaOrder hasn't been fetched disables lazy loading for OrderCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrder
		{
			get { return _alreadyFetchedOrderCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrder && !value && (_orderCollectionViaOrder != null))
				{
					_orderCollectionViaOrder.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage
		{
			get { return GetMultiTerminalCollectionViaNetmessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage. When set to true, TerminalCollectionViaNetmessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage has been fetched
		/// will clear the TerminalCollectionViaNetmessage collection well. Setting this property to true while TerminalCollectionViaNetmessage hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage && !value && (_terminalCollectionViaNetmessage != null))
				{
					_terminalCollectionViaNetmessage.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage_
		{
			get { return GetMultiTerminalCollectionViaNetmessage_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage_. When set to true, TerminalCollectionViaNetmessage_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage_
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage_; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage_ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage_ has been fetched
		/// will clear the TerminalCollectionViaNetmessage_ collection well. Setting this property to true while TerminalCollectionViaNetmessage_ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage_
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage_ && !value && (_terminalCollectionViaNetmessage_ != null))
				{
					_terminalCollectionViaNetmessage_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage__
		{
			get { return GetMultiTerminalCollectionViaNetmessage__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage__. When set to true, TerminalCollectionViaNetmessage__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage__ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage__
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage__; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage__ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage__ has been fetched
		/// will clear the TerminalCollectionViaNetmessage__ collection well. Setting this property to true while TerminalCollectionViaNetmessage__ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage__
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage__;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage__ && !value && (_terminalCollectionViaNetmessage__ != null))
				{
					_terminalCollectionViaNetmessage__.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaNetmessage___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaNetmessage___
		{
			get { return GetMultiTerminalCollectionViaNetmessage___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaNetmessage___. When set to true, TerminalCollectionViaNetmessage___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaNetmessage___ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaNetmessage___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaNetmessage___
		{
			get	{ return _alwaysFetchTerminalCollectionViaNetmessage___; }
			set	{ _alwaysFetchTerminalCollectionViaNetmessage___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaNetmessage___ already has been fetched. Setting this property to false when TerminalCollectionViaNetmessage___ has been fetched
		/// will clear the TerminalCollectionViaNetmessage___ collection well. Setting this property to true while TerminalCollectionViaNetmessage___ hasn't been fetched disables lazy loading for TerminalCollectionViaNetmessage___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaNetmessage___
		{
			get { return _alreadyFetchedTerminalCollectionViaNetmessage___;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaNetmessage___ && !value && (_terminalCollectionViaNetmessage___ != null))
				{
					_terminalCollectionViaNetmessage___.Clear();
				}
				_alreadyFetchedTerminalCollectionViaNetmessage___ = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ClientEntity
		{
			get	{ return GetSingleClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerCollection", "ClientEntity", _clientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntity. When set to true, ClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntity is accessed. You can always execute a forced fetch by calling GetSingleClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntity
		{
			get	{ return _alwaysFetchClientEntity; }
			set	{ _alwaysFetchClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntity already has been fetched. Setting this property to false when ClientEntity has been fetched
		/// will set ClientEntity to null as well. Setting this property to true while ClientEntity hasn't been fetched disables lazy loading for ClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntity
		{
			get { return _alreadyFetchedClientEntity;}
			set 
			{
				if(_alreadyFetchedClientEntity && !value)
				{
					this.ClientEntity = null;
				}
				_alreadyFetchedClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientEntity is not found
		/// in the database. When set to true, ClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientEntityReturnsNewIfNotFound
		{
			get	{ return _clientEntityReturnsNewIfNotFound; }
			set { _clientEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CustomerEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
