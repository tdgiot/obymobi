﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'RoomControlArea'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RoomControlAreaEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RoomControlAreaEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientCollection	_clientCollection;
		private bool	_alwaysFetchClientCollection, _alreadyFetchedClientCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection	_deliverypointCollection;
		private bool	_alwaysFetchDeliverypointCollection, _alreadyFetchedDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection	_roomControlAreaLanguageCollection;
		private bool	_alwaysFetchRoomControlAreaLanguageCollection, _alreadyFetchedRoomControlAreaLanguageCollection;
		private Obymobi.Data.CollectionClasses.RoomControlSectionCollection	_roomControlSectionCollection;
		private bool	_alwaysFetchRoomControlSectionCollection, _alreadyFetchedRoomControlSectionCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private RoomControlConfigurationEntity _roomControlConfigurationEntity;
		private bool	_alwaysFetchRoomControlConfigurationEntity, _alreadyFetchedRoomControlConfigurationEntity, _roomControlConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RoomControlConfigurationEntity</summary>
			public static readonly string RoomControlConfigurationEntity = "RoomControlConfigurationEntity";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name RoomControlAreaLanguageCollection</summary>
			public static readonly string RoomControlAreaLanguageCollection = "RoomControlAreaLanguageCollection";
			/// <summary>Member name RoomControlSectionCollection</summary>
			public static readonly string RoomControlSectionCollection = "RoomControlSectionCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RoomControlAreaEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RoomControlAreaEntityBase() :base("RoomControlAreaEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		protected RoomControlAreaEntityBase(System.Int32 roomControlAreaId):base("RoomControlAreaEntity")
		{
			InitClassFetch(roomControlAreaId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RoomControlAreaEntityBase(System.Int32 roomControlAreaId, IPrefetchPath prefetchPathToUse): base("RoomControlAreaEntity")
		{
			InitClassFetch(roomControlAreaId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <param name="validator">The custom validator object for this RoomControlAreaEntity</param>
		protected RoomControlAreaEntityBase(System.Int32 roomControlAreaId, IValidator validator):base("RoomControlAreaEntity")
		{
			InitClassFetch(roomControlAreaId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlAreaEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientCollection = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollection", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollection = info.GetBoolean("_alwaysFetchClientCollection");
			_alreadyFetchedClientCollection = info.GetBoolean("_alreadyFetchedClientCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_deliverypointCollection = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollection = info.GetBoolean("_alwaysFetchDeliverypointCollection");
			_alreadyFetchedDeliverypointCollection = info.GetBoolean("_alreadyFetchedDeliverypointCollection");

			_roomControlAreaLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection)info.GetValue("_roomControlAreaLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection));
			_alwaysFetchRoomControlAreaLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlAreaLanguageCollection");
			_alreadyFetchedRoomControlAreaLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlAreaLanguageCollection");

			_roomControlSectionCollection = (Obymobi.Data.CollectionClasses.RoomControlSectionCollection)info.GetValue("_roomControlSectionCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlSectionCollection));
			_alwaysFetchRoomControlSectionCollection = info.GetBoolean("_alwaysFetchRoomControlSectionCollection");
			_alreadyFetchedRoomControlSectionCollection = info.GetBoolean("_alreadyFetchedRoomControlSectionCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_roomControlConfigurationEntity = (RoomControlConfigurationEntity)info.GetValue("_roomControlConfigurationEntity", typeof(RoomControlConfigurationEntity));
			if(_roomControlConfigurationEntity!=null)
			{
				_roomControlConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlConfigurationEntity = info.GetBoolean("_alwaysFetchRoomControlConfigurationEntity");
			_alreadyFetchedRoomControlConfigurationEntity = info.GetBoolean("_alreadyFetchedRoomControlConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RoomControlAreaFieldIndex)fieldIndex)
			{
				case RoomControlAreaFieldIndex.RoomControlConfigurationId:
					DesetupSyncRoomControlConfigurationEntity(true, false);
					_alreadyFetchedRoomControlConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientCollection = (_clientCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedDeliverypointCollection = (_deliverypointCollection.Count > 0);
			_alreadyFetchedRoomControlAreaLanguageCollection = (_roomControlAreaLanguageCollection.Count > 0);
			_alreadyFetchedRoomControlSectionCollection = (_roomControlSectionCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedRoomControlConfigurationEntity = (_roomControlConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RoomControlConfigurationEntity":
					toReturn.Add(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
					break;
				case "ClientCollection":
					toReturn.Add(Relations.ClientEntityUsingRoomControlAreaId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingRoomControlAreaId);
					break;
				case "DeliverypointCollection":
					toReturn.Add(Relations.DeliverypointEntityUsingRoomControlAreaId);
					break;
				case "RoomControlAreaLanguageCollection":
					toReturn.Add(Relations.RoomControlAreaLanguageEntityUsingRoomControlAreaId);
					break;
				case "RoomControlSectionCollection":
					toReturn.Add(Relations.RoomControlSectionEntityUsingRoomControlAreaId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingRoomControlAreaId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientCollection", (!this.MarkedForDeletion?_clientCollection:null));
			info.AddValue("_alwaysFetchClientCollection", _alwaysFetchClientCollection);
			info.AddValue("_alreadyFetchedClientCollection", _alreadyFetchedClientCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_deliverypointCollection", (!this.MarkedForDeletion?_deliverypointCollection:null));
			info.AddValue("_alwaysFetchDeliverypointCollection", _alwaysFetchDeliverypointCollection);
			info.AddValue("_alreadyFetchedDeliverypointCollection", _alreadyFetchedDeliverypointCollection);
			info.AddValue("_roomControlAreaLanguageCollection", (!this.MarkedForDeletion?_roomControlAreaLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlAreaLanguageCollection", _alwaysFetchRoomControlAreaLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlAreaLanguageCollection", _alreadyFetchedRoomControlAreaLanguageCollection);
			info.AddValue("_roomControlSectionCollection", (!this.MarkedForDeletion?_roomControlSectionCollection:null));
			info.AddValue("_alwaysFetchRoomControlSectionCollection", _alwaysFetchRoomControlSectionCollection);
			info.AddValue("_alreadyFetchedRoomControlSectionCollection", _alreadyFetchedRoomControlSectionCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_roomControlConfigurationEntity", (!this.MarkedForDeletion?_roomControlConfigurationEntity:null));
			info.AddValue("_roomControlConfigurationEntityReturnsNewIfNotFound", _roomControlConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlConfigurationEntity", _alwaysFetchRoomControlConfigurationEntity);
			info.AddValue("_alreadyFetchedRoomControlConfigurationEntity", _alreadyFetchedRoomControlConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RoomControlConfigurationEntity":
					_alreadyFetchedRoomControlConfigurationEntity = true;
					this.RoomControlConfigurationEntity = (RoomControlConfigurationEntity)entity;
					break;
				case "ClientCollection":
					_alreadyFetchedClientCollection = true;
					if(entity!=null)
					{
						this.ClientCollection.Add((ClientEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "DeliverypointCollection":
					_alreadyFetchedDeliverypointCollection = true;
					if(entity!=null)
					{
						this.DeliverypointCollection.Add((DeliverypointEntity)entity);
					}
					break;
				case "RoomControlAreaLanguageCollection":
					_alreadyFetchedRoomControlAreaLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlAreaLanguageCollection.Add((RoomControlAreaLanguageEntity)entity);
					}
					break;
				case "RoomControlSectionCollection":
					_alreadyFetchedRoomControlSectionCollection = true;
					if(entity!=null)
					{
						this.RoomControlSectionCollection.Add((RoomControlSectionEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RoomControlConfigurationEntity":
					SetupSyncRoomControlConfigurationEntity(relatedEntity);
					break;
				case "ClientCollection":
					_clientCollection.Add((ClientEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "DeliverypointCollection":
					_deliverypointCollection.Add((DeliverypointEntity)relatedEntity);
					break;
				case "RoomControlAreaLanguageCollection":
					_roomControlAreaLanguageCollection.Add((RoomControlAreaLanguageEntity)relatedEntity);
					break;
				case "RoomControlSectionCollection":
					_roomControlSectionCollection.Add((RoomControlSectionEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RoomControlConfigurationEntity":
					DesetupSyncRoomControlConfigurationEntity(false, true);
					break;
				case "ClientCollection":
					this.PerformRelatedEntityRemoval(_clientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointCollection":
					this.PerformRelatedEntityRemoval(_deliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlAreaLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlAreaLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlSectionCollection":
					this.PerformRelatedEntityRemoval(_roomControlSectionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_roomControlConfigurationEntity!=null)
			{
				toReturn.Add(_roomControlConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_deliverypointCollection);
			toReturn.Add(_roomControlAreaLanguageCollection);
			toReturn.Add(_roomControlSectionCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlAreaId)
		{
			return FetchUsingPK(roomControlAreaId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlAreaId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(roomControlAreaId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlAreaId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(roomControlAreaId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlAreaId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(roomControlAreaId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RoomControlAreaId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RoomControlAreaRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientCollection || forceFetch || _alwaysFetchClientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollection);
				_clientCollection.SuppressClearInGetMulti=!forceFetch;
				_clientCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientCollection.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_clientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollection = true;
			}
			return _clientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollection'. These settings will be taken into account
		/// when the property ClientCollection is requested or GetMultiClientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollection.SortClauses=sortClauses;
			_clientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointCollection || forceFetch || _alwaysFetchDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollection);
				_deliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollection.GetMultiManyToOne(null, null, null, null, null, this, null, filter);
				_deliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollection = true;
			}
			return _deliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollection'. These settings will be taken into account
		/// when the property DeliverypointCollection is requested or GetMultiDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollection.SortClauses=sortClauses;
			_deliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlAreaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlAreaLanguageCollection(forceFetch, _roomControlAreaLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlAreaLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlAreaLanguageCollection(forceFetch, _roomControlAreaLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlAreaLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection GetMultiRoomControlAreaLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlAreaLanguageCollection || forceFetch || _alwaysFetchRoomControlAreaLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlAreaLanguageCollection);
				_roomControlAreaLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlAreaLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlAreaLanguageCollection.GetMultiManyToOne(null, this, filter);
				_roomControlAreaLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlAreaLanguageCollection = true;
			}
			return _roomControlAreaLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlAreaLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlAreaLanguageCollection is requested or GetMultiRoomControlAreaLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlAreaLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlAreaLanguageCollection.SortClauses=sortClauses;
			_roomControlAreaLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionCollection GetMultiRoomControlSectionCollection(bool forceFetch)
		{
			return GetMultiRoomControlSectionCollection(forceFetch, _roomControlSectionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlSectionEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionCollection GetMultiRoomControlSectionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlSectionCollection(forceFetch, _roomControlSectionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlSectionCollection GetMultiRoomControlSectionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlSectionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlSectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionCollection GetMultiRoomControlSectionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlSectionCollection || forceFetch || _alwaysFetchRoomControlSectionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlSectionCollection);
				_roomControlSectionCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlSectionCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlSectionCollection.GetMultiManyToOne(this, filter);
				_roomControlSectionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlSectionCollection = true;
			}
			return _roomControlSectionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlSectionCollection'. These settings will be taken into account
		/// when the property RoomControlSectionCollection is requested or GetMultiRoomControlSectionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlSectionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlSectionCollection.SortClauses=sortClauses;
			_roomControlSectionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity()
		{
			return GetSingleRoomControlConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlConfigurationEntity' which is related to this entity.</returns>
		public virtual RoomControlConfigurationEntity GetSingleRoomControlConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlConfigurationEntity || forceFetch || _alwaysFetchRoomControlConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
				RoomControlConfigurationEntity newEntity = new RoomControlConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (RoomControlConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlConfigurationEntity = newEntity;
				_alreadyFetchedRoomControlConfigurationEntity = fetchResult;
			}
			return _roomControlConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RoomControlConfigurationEntity", _roomControlConfigurationEntity);
			toReturn.Add("ClientCollection", _clientCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("DeliverypointCollection", _deliverypointCollection);
			toReturn.Add("RoomControlAreaLanguageCollection", _roomControlAreaLanguageCollection);
			toReturn.Add("RoomControlSectionCollection", _roomControlSectionCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <param name="validator">The validator object for this RoomControlAreaEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 roomControlAreaId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(roomControlAreaId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientCollection = new Obymobi.Data.CollectionClasses.ClientCollection();
			_clientCollection.SetContainingEntityInfo(this, "RoomControlAreaEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "RoomControlAreaEntity");

			_deliverypointCollection = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollection.SetContainingEntityInfo(this, "RoomControlAreaEntity");

			_roomControlAreaLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection();
			_roomControlAreaLanguageCollection.SetContainingEntityInfo(this, "RoomControlAreaEntity");

			_roomControlSectionCollection = new Obymobi.Data.CollectionClasses.RoomControlSectionCollection();
			_roomControlSectionCollection.SetContainingEntityInfo(this, "RoomControlAreaEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "RoomControlAreaEntity");
			_roomControlConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NameSystem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsNameSystemBaseId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlAreaRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, signalRelatedEntity, "RoomControlAreaCollection", resetFKFields, new int[] { (int)RoomControlAreaFieldIndex.RoomControlConfigurationId } );		
			_roomControlConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_roomControlConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlConfigurationEntity(true, true);
				_roomControlConfigurationEntity = (RoomControlConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlConfigurationEntity, new PropertyChangedEventHandler( OnRoomControlConfigurationEntityPropertyChanged ), "RoomControlConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlAreaRelations.RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic, true, ref _alreadyFetchedRoomControlConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="roomControlAreaId">PK value for RoomControlArea which data should be fetched into this RoomControlArea object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 roomControlAreaId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RoomControlAreaFieldIndex.RoomControlAreaId].ForcedCurrentValueWrite(roomControlAreaId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoomControlAreaDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RoomControlAreaEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RoomControlAreaRelations Relations
		{
			get	{ return new RoomControlAreaRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientCollection")[0], (int)Obymobi.Data.EntityType.RoomControlAreaEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.RoomControlAreaEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointCollection")[0], (int)Obymobi.Data.EntityType.RoomControlAreaEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlAreaLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlAreaLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlAreaLanguageCollection")[0], (int)Obymobi.Data.EntityType.RoomControlAreaEntity, (int)Obymobi.Data.EntityType.RoomControlAreaLanguageEntity, 0, null, null, null, "RoomControlAreaLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSection' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionCollection")[0], (int)Obymobi.Data.EntityType.RoomControlAreaEntity, (int)Obymobi.Data.EntityType.RoomControlSectionEntity, 0, null, null, null, "RoomControlSectionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.RoomControlAreaEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlConfigurationCollection(), (IEntityRelation)GetRelationsForField("RoomControlConfigurationEntity")[0], (int)Obymobi.Data.EntityType.RoomControlAreaEntity, (int)Obymobi.Data.EntityType.RoomControlConfigurationEntity, 0, null, null, null, "RoomControlConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RoomControlAreaId property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."RoomControlAreaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoomControlAreaId
		{
			get { return (System.Int32)GetValue((int)RoomControlAreaFieldIndex.RoomControlAreaId, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.RoomControlAreaId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlAreaFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The RoomControlConfigurationId property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."RoomControlConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlConfigurationId
		{
			get { return (System.Int32)GetValue((int)RoomControlAreaFieldIndex.RoomControlConfigurationId, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.RoomControlConfigurationId, value, true); }
		}

		/// <summary> The Name property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RoomControlAreaFieldIndex.Name, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.Name, value, true); }
		}

		/// <summary> The SortOrder property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)RoomControlAreaFieldIndex.SortOrder, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlAreaFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlAreaFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlAreaFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlAreaFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Visible property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)RoomControlAreaFieldIndex.Visible, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.Visible, value, true); }
		}

		/// <summary> The NameSystem property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."NameSystem"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NameSystem
		{
			get { return (System.String)GetValue((int)RoomControlAreaFieldIndex.NameSystem, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.NameSystem, value, true); }
		}

		/// <summary> The Type property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.RoomControlAreaType Type
		{
			get { return (Obymobi.Enums.RoomControlAreaType)GetValue((int)RoomControlAreaFieldIndex.Type, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.Type, value, true); }
		}

		/// <summary> The IsNameSystemBaseId property of the Entity RoomControlArea<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlArea"."IsNameSystemBaseId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsNameSystemBaseId
		{
			get { return (System.Boolean)GetValue((int)RoomControlAreaFieldIndex.IsNameSystemBaseId, true); }
			set	{ SetValue((int)RoomControlAreaFieldIndex.IsNameSystemBaseId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollection
		{
			get	{ return GetMultiClientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollection. When set to true, ClientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollection
		{
			get	{ return _alwaysFetchClientCollection; }
			set	{ _alwaysFetchClientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollection already has been fetched. Setting this property to false when ClientCollection has been fetched
		/// will clear the ClientCollection collection well. Setting this property to true while ClientCollection hasn't been fetched disables lazy loading for ClientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollection
		{
			get { return _alreadyFetchedClientCollection;}
			set 
			{
				if(_alreadyFetchedClientCollection && !value && (_clientCollection != null))
				{
					_clientCollection.Clear();
				}
				_alreadyFetchedClientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollection
		{
			get	{ return GetMultiDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollection. When set to true, DeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollection
		{
			get	{ return _alwaysFetchDeliverypointCollection; }
			set	{ _alwaysFetchDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollection already has been fetched. Setting this property to false when DeliverypointCollection has been fetched
		/// will clear the DeliverypointCollection collection well. Setting this property to true while DeliverypointCollection hasn't been fetched disables lazy loading for DeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollection
		{
			get { return _alreadyFetchedDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollection && !value && (_deliverypointCollection != null))
				{
					_deliverypointCollection.Clear();
				}
				_alreadyFetchedDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlAreaLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlAreaLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlAreaLanguageCollection RoomControlAreaLanguageCollection
		{
			get	{ return GetMultiRoomControlAreaLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlAreaLanguageCollection. When set to true, RoomControlAreaLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlAreaLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlAreaLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlAreaLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlAreaLanguageCollection; }
			set	{ _alwaysFetchRoomControlAreaLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlAreaLanguageCollection already has been fetched. Setting this property to false when RoomControlAreaLanguageCollection has been fetched
		/// will clear the RoomControlAreaLanguageCollection collection well. Setting this property to true while RoomControlAreaLanguageCollection hasn't been fetched disables lazy loading for RoomControlAreaLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlAreaLanguageCollection
		{
			get { return _alreadyFetchedRoomControlAreaLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlAreaLanguageCollection && !value && (_roomControlAreaLanguageCollection != null))
				{
					_roomControlAreaLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlAreaLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlSectionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlSectionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlSectionCollection RoomControlSectionCollection
		{
			get	{ return GetMultiRoomControlSectionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionCollection. When set to true, RoomControlSectionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlSectionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionCollection
		{
			get	{ return _alwaysFetchRoomControlSectionCollection; }
			set	{ _alwaysFetchRoomControlSectionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionCollection already has been fetched. Setting this property to false when RoomControlSectionCollection has been fetched
		/// will clear the RoomControlSectionCollection collection well. Setting this property to true while RoomControlSectionCollection hasn't been fetched disables lazy loading for RoomControlSectionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionCollection
		{
			get { return _alreadyFetchedRoomControlSectionCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionCollection && !value && (_roomControlSectionCollection != null))
				{
					_roomControlSectionCollection.Clear();
				}
				_alreadyFetchedRoomControlSectionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'RoomControlConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlConfigurationEntity RoomControlConfigurationEntity
		{
			get	{ return GetSingleRoomControlConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlAreaCollection", "RoomControlConfigurationEntity", _roomControlConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlConfigurationEntity. When set to true, RoomControlConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlConfigurationEntity
		{
			get	{ return _alwaysFetchRoomControlConfigurationEntity; }
			set	{ _alwaysFetchRoomControlConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlConfigurationEntity already has been fetched. Setting this property to false when RoomControlConfigurationEntity has been fetched
		/// will set RoomControlConfigurationEntity to null as well. Setting this property to true while RoomControlConfigurationEntity hasn't been fetched disables lazy loading for RoomControlConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlConfigurationEntity
		{
			get { return _alreadyFetchedRoomControlConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlConfigurationEntity && !value)
				{
					this.RoomControlConfigurationEntity = null;
				}
				_alreadyFetchedRoomControlConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlConfigurationEntity is not found
		/// in the database. When set to true, RoomControlConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlConfigurationEntityReturnsNewIfNotFound; }
			set { _roomControlConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RoomControlAreaEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
