﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Route'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RouteEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RouteEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CategoryCollection	_categoryCollection;
		private bool	_alwaysFetchCategoryCollection, _alreadyFetchedCategoryCollection;
		private Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection	_clientConfigurationRouteCollection;
		private bool	_alwaysFetchClientConfigurationRouteCollection, _alreadyFetchedClientConfigurationRouteCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection	_companyCollection;
		private bool	_alwaysFetchCompanyCollection, _alreadyFetchedCompanyCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection__;
		private bool	_alwaysFetchDeliverypointgroupCollection__, _alreadyFetchedDeliverypointgroupCollection__;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection___;
		private bool	_alwaysFetchDeliverypointgroupCollection___, _alreadyFetchedDeliverypointgroupCollection___;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection_;
		private bool	_alwaysFetchDeliverypointgroupCollection_, _alreadyFetchedDeliverypointgroupCollection_;
		private Obymobi.Data.CollectionClasses.ProductCollection	_productCollection;
		private bool	_alwaysFetchProductCollection, _alreadyFetchedProductCollection;
		private Obymobi.Data.CollectionClasses.RouteCollection	_escalationFromRouteCollection;
		private bool	_alwaysFetchEscalationFromRouteCollection, _alreadyFetchedEscalationFromRouteCollection;
		private Obymobi.Data.CollectionClasses.RoutestepCollection	_routestepCollection;
		private bool	_alwaysFetchRoutestepCollection, _alreadyFetchedRoutestepCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection _announcementCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchAnnouncementCollectionViaDeliverypointgroup, _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection _announcementCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_, _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaCategory;
		private bool	_alwaysFetchCategoryCollectionViaCategory, _alreadyFetchedCategoryCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaCategory;
		private bool	_alwaysFetchCompanyCollectionViaCategory, _alreadyFetchedCompanyCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup, _alreadyFetchedCompanyCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup_, _alreadyFetchedCompanyCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaProduct;
		private bool	_alwaysFetchCompanyCollectionViaProduct, _alreadyFetchedCompanyCollectionViaProduct;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaRoute;
		private bool	_alwaysFetchCompanyCollectionViaRoute, _alreadyFetchedCompanyCollectionViaRoute;
		private Obymobi.Data.CollectionClasses.CompanyOwnerCollection _companyOwnerCollectionViaCompany;
		private bool	_alwaysFetchCompanyOwnerCollectionViaCompany, _alreadyFetchedCompanyOwnerCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.CountryCollection _countryCollectionViaCompany;
		private bool	_alwaysFetchCountryCollectionViaCompany, _alreadyFetchedCountryCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.CurrencyCollection _currencyCollectionViaCompany;
		private bool	_alwaysFetchCurrencyCollectionViaCompany, _alreadyFetchedCurrencyCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection _genericcategoryCollectionViaCategory;
		private bool	_alwaysFetchGenericcategoryCollectionViaCategory, _alreadyFetchedGenericcategoryCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaProduct;
		private bool	_alwaysFetchGenericproductCollectionViaProduct, _alreadyFetchedGenericproductCollectionViaProduct;
		private Obymobi.Data.CollectionClasses.LanguageCollection _languageCollectionViaCompany;
		private bool	_alwaysFetchLanguageCollectionViaCompany, _alreadyFetchedLanguageCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaCategory;
		private bool	_alwaysFetchMenuCollectionViaCategory, _alreadyFetchedMenuCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchMenuCollectionViaDeliverypointgroup, _alreadyFetchedMenuCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.MenuCollection _menuCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchMenuCollectionViaDeliverypointgroup_, _alreadyFetchedMenuCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.PoscategoryCollection _poscategoryCollectionViaCategory;
		private bool	_alwaysFetchPoscategoryCollectionViaCategory, _alreadyFetchedPoscategoryCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.PosproductCollection _posproductCollectionViaProduct;
		private bool	_alwaysFetchPosproductCollectionViaProduct, _alreadyFetchedPosproductCollectionViaProduct;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaCategory;
		private bool	_alwaysFetchProductCollectionViaCategory, _alreadyFetchedProductCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup, _alreadyFetchedRouteCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup_, _alreadyFetchedRouteCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaCompany;
		private bool	_alwaysFetchSupportpoolCollectionViaCompany, _alreadyFetchedSupportpoolCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchTerminalCollectionViaDeliverypointgroup, _alreadyFetchedTerminalCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchTerminalCollectionViaDeliverypointgroup_, _alreadyFetchedTerminalCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup__, _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup___;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup___, _alreadyFetchedUIModeCollectionViaDeliverypointgroup___;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup____;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup____, _alreadyFetchedUIModeCollectionViaDeliverypointgroup____;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup_____;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup_____, _alreadyFetchedUIModeCollectionViaDeliverypointgroup_____;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup, _alreadyFetchedUIModeCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup_, _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.VattariffCollection _vattariffCollectionViaProduct;
		private bool	_alwaysFetchVattariffCollectionViaProduct, _alreadyFetchedVattariffCollectionViaProduct;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private RouteEntity _escalationRouteEntity;
		private bool	_alwaysFetchEscalationRouteEntity, _alreadyFetchedEscalationRouteEntity, _escalationRouteEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name EscalationRouteEntity</summary>
			public static readonly string EscalationRouteEntity = "EscalationRouteEntity";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name ClientConfigurationRouteCollection</summary>
			public static readonly string ClientConfigurationRouteCollection = "ClientConfigurationRouteCollection";
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name DeliverypointgroupCollection__</summary>
			public static readonly string DeliverypointgroupCollection__ = "DeliverypointgroupCollection__";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name DeliverypointgroupCollection___</summary>
			public static readonly string DeliverypointgroupCollection___ = "DeliverypointgroupCollection___";
			/// <summary>Member name DeliverypointgroupCollection_</summary>
			public static readonly string DeliverypointgroupCollection_ = "DeliverypointgroupCollection_";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name EscalationFromRouteCollection</summary>
			public static readonly string EscalationFromRouteCollection = "EscalationFromRouteCollection";
			/// <summary>Member name RoutestepCollection</summary>
			public static readonly string RoutestepCollection = "RoutestepCollection";
			/// <summary>Member name AnnouncementCollectionViaDeliverypointgroup</summary>
			public static readonly string AnnouncementCollectionViaDeliverypointgroup = "AnnouncementCollectionViaDeliverypointgroup";
			/// <summary>Member name AnnouncementCollectionViaDeliverypointgroup_</summary>
			public static readonly string AnnouncementCollectionViaDeliverypointgroup_ = "AnnouncementCollectionViaDeliverypointgroup_";
			/// <summary>Member name CategoryCollectionViaCategory</summary>
			public static readonly string CategoryCollectionViaCategory = "CategoryCollectionViaCategory";
			/// <summary>Member name CompanyCollectionViaCategory</summary>
			public static readonly string CompanyCollectionViaCategory = "CompanyCollectionViaCategory";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup = "CompanyCollectionViaDeliverypointgroup";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup_</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup_ = "CompanyCollectionViaDeliverypointgroup_";
			/// <summary>Member name CompanyCollectionViaProduct</summary>
			public static readonly string CompanyCollectionViaProduct = "CompanyCollectionViaProduct";
			/// <summary>Member name CompanyCollectionViaRoute</summary>
			public static readonly string CompanyCollectionViaRoute = "CompanyCollectionViaRoute";
			/// <summary>Member name CompanyOwnerCollectionViaCompany</summary>
			public static readonly string CompanyOwnerCollectionViaCompany = "CompanyOwnerCollectionViaCompany";
			/// <summary>Member name CountryCollectionViaCompany</summary>
			public static readonly string CountryCollectionViaCompany = "CountryCollectionViaCompany";
			/// <summary>Member name CurrencyCollectionViaCompany</summary>
			public static readonly string CurrencyCollectionViaCompany = "CurrencyCollectionViaCompany";
			/// <summary>Member name GenericcategoryCollectionViaCategory</summary>
			public static readonly string GenericcategoryCollectionViaCategory = "GenericcategoryCollectionViaCategory";
			/// <summary>Member name GenericproductCollectionViaProduct</summary>
			public static readonly string GenericproductCollectionViaProduct = "GenericproductCollectionViaProduct";
			/// <summary>Member name LanguageCollectionViaCompany</summary>
			public static readonly string LanguageCollectionViaCompany = "LanguageCollectionViaCompany";
			/// <summary>Member name MenuCollectionViaCategory</summary>
			public static readonly string MenuCollectionViaCategory = "MenuCollectionViaCategory";
			/// <summary>Member name MenuCollectionViaDeliverypointgroup</summary>
			public static readonly string MenuCollectionViaDeliverypointgroup = "MenuCollectionViaDeliverypointgroup";
			/// <summary>Member name MenuCollectionViaDeliverypointgroup_</summary>
			public static readonly string MenuCollectionViaDeliverypointgroup_ = "MenuCollectionViaDeliverypointgroup_";
			/// <summary>Member name PoscategoryCollectionViaCategory</summary>
			public static readonly string PoscategoryCollectionViaCategory = "PoscategoryCollectionViaCategory";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup = "PosdeliverypointgroupCollectionViaDeliverypointgroup";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup_</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup_ = "PosdeliverypointgroupCollectionViaDeliverypointgroup_";
			/// <summary>Member name PosproductCollectionViaProduct</summary>
			public static readonly string PosproductCollectionViaProduct = "PosproductCollectionViaProduct";
			/// <summary>Member name ProductCollectionViaCategory</summary>
			public static readonly string ProductCollectionViaCategory = "ProductCollectionViaCategory";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup = "RouteCollectionViaDeliverypointgroup";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup_</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup_ = "RouteCollectionViaDeliverypointgroup_";
			/// <summary>Member name SupportpoolCollectionViaCompany</summary>
			public static readonly string SupportpoolCollectionViaCompany = "SupportpoolCollectionViaCompany";
			/// <summary>Member name TerminalCollectionViaDeliverypointgroup</summary>
			public static readonly string TerminalCollectionViaDeliverypointgroup = "TerminalCollectionViaDeliverypointgroup";
			/// <summary>Member name TerminalCollectionViaDeliverypointgroup_</summary>
			public static readonly string TerminalCollectionViaDeliverypointgroup_ = "TerminalCollectionViaDeliverypointgroup_";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup__</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup__ = "UIModeCollectionViaDeliverypointgroup__";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup___</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup___ = "UIModeCollectionViaDeliverypointgroup___";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup____</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup____ = "UIModeCollectionViaDeliverypointgroup____";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup_____</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup_____ = "UIModeCollectionViaDeliverypointgroup_____";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup = "UIModeCollectionViaDeliverypointgroup";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup_</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup_ = "UIModeCollectionViaDeliverypointgroup_";
			/// <summary>Member name VattariffCollectionViaProduct</summary>
			public static readonly string VattariffCollectionViaProduct = "VattariffCollectionViaProduct";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RouteEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RouteEntityBase() :base("RouteEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		protected RouteEntityBase(System.Int32 routeId):base("RouteEntity")
		{
			InitClassFetch(routeId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RouteEntityBase(System.Int32 routeId, IPrefetchPath prefetchPathToUse): base("RouteEntity")
		{
			InitClassFetch(routeId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="validator">The custom validator object for this RouteEntity</param>
		protected RouteEntityBase(System.Int32 routeId, IValidator validator):base("RouteEntity")
		{
			InitClassFetch(routeId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RouteEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_categoryCollection = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollection", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollection = info.GetBoolean("_alwaysFetchCategoryCollection");
			_alreadyFetchedCategoryCollection = info.GetBoolean("_alreadyFetchedCategoryCollection");

			_clientConfigurationRouteCollection = (Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection)info.GetValue("_clientConfigurationRouteCollection", typeof(Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection));
			_alwaysFetchClientConfigurationRouteCollection = info.GetBoolean("_alwaysFetchClientConfigurationRouteCollection");
			_alreadyFetchedClientConfigurationRouteCollection = info.GetBoolean("_alreadyFetchedClientConfigurationRouteCollection");

			_companyCollection = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollection", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollection = info.GetBoolean("_alwaysFetchCompanyCollection");
			_alreadyFetchedCompanyCollection = info.GetBoolean("_alreadyFetchedCompanyCollection");

			_deliverypointgroupCollection__ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection__", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection__ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection__");
			_alreadyFetchedDeliverypointgroupCollection__ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection__");

			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");

			_deliverypointgroupCollection___ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection___", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection___ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection___");
			_alreadyFetchedDeliverypointgroupCollection___ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection___");

			_deliverypointgroupCollection_ = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection_", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection_ = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection_");
			_alreadyFetchedDeliverypointgroupCollection_ = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection_");

			_productCollection = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollection", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollection = info.GetBoolean("_alwaysFetchProductCollection");
			_alreadyFetchedProductCollection = info.GetBoolean("_alreadyFetchedProductCollection");

			_escalationFromRouteCollection = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_escalationFromRouteCollection", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchEscalationFromRouteCollection = info.GetBoolean("_alwaysFetchEscalationFromRouteCollection");
			_alreadyFetchedEscalationFromRouteCollection = info.GetBoolean("_alreadyFetchedEscalationFromRouteCollection");

			_routestepCollection = (Obymobi.Data.CollectionClasses.RoutestepCollection)info.GetValue("_routestepCollection", typeof(Obymobi.Data.CollectionClasses.RoutestepCollection));
			_alwaysFetchRoutestepCollection = info.GetBoolean("_alwaysFetchRoutestepCollection");
			_alreadyFetchedRoutestepCollection = info.GetBoolean("_alreadyFetchedRoutestepCollection");
			_announcementCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup");
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup");

			_announcementCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_");
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_");

			_categoryCollectionViaCategory = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaCategory = info.GetBoolean("_alwaysFetchCategoryCollectionViaCategory");
			_alreadyFetchedCategoryCollectionViaCategory = info.GetBoolean("_alreadyFetchedCategoryCollectionViaCategory");

			_companyCollectionViaCategory = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaCategory = info.GetBoolean("_alwaysFetchCompanyCollectionViaCategory");
			_alreadyFetchedCompanyCollectionViaCategory = info.GetBoolean("_alreadyFetchedCompanyCollectionViaCategory");

			_companyCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup");

			_companyCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup_");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup_");

			_companyCollectionViaProduct = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaProduct = info.GetBoolean("_alwaysFetchCompanyCollectionViaProduct");
			_alreadyFetchedCompanyCollectionViaProduct = info.GetBoolean("_alreadyFetchedCompanyCollectionViaProduct");

			_companyCollectionViaRoute = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaRoute", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaRoute = info.GetBoolean("_alwaysFetchCompanyCollectionViaRoute");
			_alreadyFetchedCompanyCollectionViaRoute = info.GetBoolean("_alreadyFetchedCompanyCollectionViaRoute");

			_companyOwnerCollectionViaCompany = (Obymobi.Data.CollectionClasses.CompanyOwnerCollection)info.GetValue("_companyOwnerCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CompanyOwnerCollection));
			_alwaysFetchCompanyOwnerCollectionViaCompany = info.GetBoolean("_alwaysFetchCompanyOwnerCollectionViaCompany");
			_alreadyFetchedCompanyOwnerCollectionViaCompany = info.GetBoolean("_alreadyFetchedCompanyOwnerCollectionViaCompany");

			_countryCollectionViaCompany = (Obymobi.Data.CollectionClasses.CountryCollection)info.GetValue("_countryCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CountryCollection));
			_alwaysFetchCountryCollectionViaCompany = info.GetBoolean("_alwaysFetchCountryCollectionViaCompany");
			_alreadyFetchedCountryCollectionViaCompany = info.GetBoolean("_alreadyFetchedCountryCollectionViaCompany");

			_currencyCollectionViaCompany = (Obymobi.Data.CollectionClasses.CurrencyCollection)info.GetValue("_currencyCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CurrencyCollection));
			_alwaysFetchCurrencyCollectionViaCompany = info.GetBoolean("_alwaysFetchCurrencyCollectionViaCompany");
			_alreadyFetchedCurrencyCollectionViaCompany = info.GetBoolean("_alreadyFetchedCurrencyCollectionViaCompany");

			_genericcategoryCollectionViaCategory = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollectionViaCategory = info.GetBoolean("_alwaysFetchGenericcategoryCollectionViaCategory");
			_alreadyFetchedGenericcategoryCollectionViaCategory = info.GetBoolean("_alreadyFetchedGenericcategoryCollectionViaCategory");

			_genericproductCollectionViaProduct = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaProduct = info.GetBoolean("_alwaysFetchGenericproductCollectionViaProduct");
			_alreadyFetchedGenericproductCollectionViaProduct = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaProduct");

			_languageCollectionViaCompany = (Obymobi.Data.CollectionClasses.LanguageCollection)info.GetValue("_languageCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.LanguageCollection));
			_alwaysFetchLanguageCollectionViaCompany = info.GetBoolean("_alwaysFetchLanguageCollectionViaCompany");
			_alreadyFetchedLanguageCollectionViaCompany = info.GetBoolean("_alreadyFetchedLanguageCollectionViaCompany");

			_menuCollectionViaCategory = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaCategory = info.GetBoolean("_alwaysFetchMenuCollectionViaCategory");
			_alreadyFetchedMenuCollectionViaCategory = info.GetBoolean("_alreadyFetchedMenuCollectionViaCategory");

			_menuCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchMenuCollectionViaDeliverypointgroup");
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedMenuCollectionViaDeliverypointgroup");

			_menuCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.MenuCollection)info.GetValue("_menuCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.MenuCollection));
			_alwaysFetchMenuCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchMenuCollectionViaDeliverypointgroup_");
			_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedMenuCollectionViaDeliverypointgroup_");

			_poscategoryCollectionViaCategory = (Obymobi.Data.CollectionClasses.PoscategoryCollection)info.GetValue("_poscategoryCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.PoscategoryCollection));
			_alwaysFetchPoscategoryCollectionViaCategory = info.GetBoolean("_alwaysFetchPoscategoryCollectionViaCategory");
			_alreadyFetchedPoscategoryCollectionViaCategory = info.GetBoolean("_alreadyFetchedPoscategoryCollectionViaCategory");

			_posdeliverypointgroupCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup");

			_posdeliverypointgroupCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_");

			_posproductCollectionViaProduct = (Obymobi.Data.CollectionClasses.PosproductCollection)info.GetValue("_posproductCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.PosproductCollection));
			_alwaysFetchPosproductCollectionViaProduct = info.GetBoolean("_alwaysFetchPosproductCollectionViaProduct");
			_alreadyFetchedPosproductCollectionViaProduct = info.GetBoolean("_alreadyFetchedPosproductCollectionViaProduct");

			_productCollectionViaCategory = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaCategory = info.GetBoolean("_alwaysFetchProductCollectionViaCategory");
			_alreadyFetchedProductCollectionViaCategory = info.GetBoolean("_alreadyFetchedProductCollectionViaCategory");

			_routeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup");

			_routeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup_");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup_");

			_supportpoolCollectionViaCompany = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaCompany = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaCompany");
			_alreadyFetchedSupportpoolCollectionViaCompany = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaCompany");

			_terminalCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchTerminalCollectionViaDeliverypointgroup");
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedTerminalCollectionViaDeliverypointgroup");

			_terminalCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchTerminalCollectionViaDeliverypointgroup_");
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedTerminalCollectionViaDeliverypointgroup_");

			_uIModeCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup__");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__");

			_uIModeCollectionViaDeliverypointgroup___ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup___", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup___ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup___");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup___");

			_uIModeCollectionViaDeliverypointgroup____ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup____", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup____ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup____");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup____");

			_uIModeCollectionViaDeliverypointgroup_____ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup_____", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup_____ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup_____");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____");

			_uIModeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup");

			_uIModeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup_");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_");

			_vattariffCollectionViaProduct = (Obymobi.Data.CollectionClasses.VattariffCollection)info.GetValue("_vattariffCollectionViaProduct", typeof(Obymobi.Data.CollectionClasses.VattariffCollection));
			_alwaysFetchVattariffCollectionViaProduct = info.GetBoolean("_alwaysFetchVattariffCollectionViaProduct");
			_alreadyFetchedVattariffCollectionViaProduct = info.GetBoolean("_alreadyFetchedVattariffCollectionViaProduct");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_escalationRouteEntity = (RouteEntity)info.GetValue("_escalationRouteEntity", typeof(RouteEntity));
			if(_escalationRouteEntity!=null)
			{
				_escalationRouteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_escalationRouteEntityReturnsNewIfNotFound = info.GetBoolean("_escalationRouteEntityReturnsNewIfNotFound");
			_alwaysFetchEscalationRouteEntity = info.GetBoolean("_alwaysFetchEscalationRouteEntity");
			_alreadyFetchedEscalationRouteEntity = info.GetBoolean("_alreadyFetchedEscalationRouteEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RouteFieldIndex)fieldIndex)
			{
				case RouteFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case RouteFieldIndex.EscalationRouteId:
					DesetupSyncEscalationRouteEntity(true, false);
					_alreadyFetchedEscalationRouteEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCategoryCollection = (_categoryCollection.Count > 0);
			_alreadyFetchedClientConfigurationRouteCollection = (_clientConfigurationRouteCollection.Count > 0);
			_alreadyFetchedCompanyCollection = (_companyCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection__ = (_deliverypointgroupCollection__.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection___ = (_deliverypointgroupCollection___.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection_ = (_deliverypointgroupCollection_.Count > 0);
			_alreadyFetchedProductCollection = (_productCollection.Count > 0);
			_alreadyFetchedEscalationFromRouteCollection = (_escalationFromRouteCollection.Count > 0);
			_alreadyFetchedRoutestepCollection = (_routestepCollection.Count > 0);
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = (_announcementCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = (_announcementCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedCategoryCollectionViaCategory = (_categoryCollectionViaCategory.Count > 0);
			_alreadyFetchedCompanyCollectionViaCategory = (_companyCollectionViaCategory.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = (_companyCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = (_companyCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedCompanyCollectionViaProduct = (_companyCollectionViaProduct.Count > 0);
			_alreadyFetchedCompanyCollectionViaRoute = (_companyCollectionViaRoute.Count > 0);
			_alreadyFetchedCompanyOwnerCollectionViaCompany = (_companyOwnerCollectionViaCompany.Count > 0);
			_alreadyFetchedCountryCollectionViaCompany = (_countryCollectionViaCompany.Count > 0);
			_alreadyFetchedCurrencyCollectionViaCompany = (_currencyCollectionViaCompany.Count > 0);
			_alreadyFetchedGenericcategoryCollectionViaCategory = (_genericcategoryCollectionViaCategory.Count > 0);
			_alreadyFetchedGenericproductCollectionViaProduct = (_genericproductCollectionViaProduct.Count > 0);
			_alreadyFetchedLanguageCollectionViaCompany = (_languageCollectionViaCompany.Count > 0);
			_alreadyFetchedMenuCollectionViaCategory = (_menuCollectionViaCategory.Count > 0);
			_alreadyFetchedMenuCollectionViaDeliverypointgroup = (_menuCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = (_menuCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedPoscategoryCollectionViaCategory = (_poscategoryCollectionViaCategory.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = (_posdeliverypointgroupCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = (_posdeliverypointgroupCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedPosproductCollectionViaProduct = (_posproductCollectionViaProduct.Count > 0);
			_alreadyFetchedProductCollectionViaCategory = (_productCollectionViaCategory.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = (_routeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = (_routeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaCompany = (_supportpoolCollectionViaCompany.Count > 0);
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = (_terminalCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = (_terminalCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = (_uIModeCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = (_uIModeCollectionViaDeliverypointgroup___.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = (_uIModeCollectionViaDeliverypointgroup____.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = (_uIModeCollectionViaDeliverypointgroup_____.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = (_uIModeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = (_uIModeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedVattariffCollectionViaProduct = (_vattariffCollectionViaProduct.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedEscalationRouteEntity = (_escalationRouteEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "EscalationRouteEntity":
					toReturn.Add(Relations.RouteEntityUsingRouteIdEscalationRouteId);
					break;
				case "CategoryCollection":
					toReturn.Add(Relations.CategoryEntityUsingRouteId);
					break;
				case "ClientConfigurationRouteCollection":
					toReturn.Add(Relations.ClientConfigurationRouteEntityUsingRouteId);
					break;
				case "CompanyCollection":
					toReturn.Add(Relations.CompanyEntityUsingRouteId);
					break;
				case "DeliverypointgroupCollection__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingEmailDocumentRouteId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId);
					break;
				case "DeliverypointgroupCollection___":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingOrderNotesRouteId);
					break;
				case "DeliverypointgroupCollection_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId);
					break;
				case "ProductCollection":
					toReturn.Add(Relations.ProductEntityUsingRouteId);
					break;
				case "EscalationFromRouteCollection":
					toReturn.Add(Relations.RouteEntityUsingEscalationRouteId);
					break;
				case "RoutestepCollection":
					toReturn.Add(Relations.RoutestepEntityUsingRouteId);
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "AnnouncementCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingRouteId, "RouteEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.CategoryEntityUsingParentCategoryId, "Category_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingRouteId, "RouteEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.CompanyEntityUsingCompanyId, "Category_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingRouteId, "RouteEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.CompanyEntityUsingCompanyId, "Product_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaRoute":
					toReturn.Add(Relations.RouteEntityUsingEscalationRouteId, "RouteEntity__", "Route_", JoinHint.None);
					toReturn.Add(RouteEntity.Relations.CompanyEntityUsingCompanyId, "Route_", string.Empty, JoinHint.None);
					break;
				case "CompanyOwnerCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingRouteId, "RouteEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CompanyOwnerEntityUsingCompanyOwnerId, "Company_", string.Empty, JoinHint.None);
					break;
				case "CountryCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingRouteId, "RouteEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CountryEntityUsingCountryId, "Company_", string.Empty, JoinHint.None);
					break;
				case "CurrencyCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingRouteId, "RouteEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CurrencyEntityUsingCurrencyId, "Company_", string.Empty, JoinHint.None);
					break;
				case "GenericcategoryCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingRouteId, "RouteEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.GenericcategoryEntityUsingGenericcategoryId, "Category_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingRouteId, "RouteEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.GenericproductEntityUsingGenericproductId, "Product_", string.Empty, JoinHint.None);
					break;
				case "LanguageCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingRouteId, "RouteEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.LanguageEntityUsingLanguageId, "Company_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingRouteId, "RouteEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.MenuEntityUsingMenuId, "Category_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "MenuCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.MenuEntityUsingMenuId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "PoscategoryCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingRouteId, "RouteEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.PoscategoryEntityUsingPoscategoryId, "Category_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "PosproductCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingRouteId, "RouteEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.PosproductEntityUsingPosproductId, "Product_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingRouteId, "RouteEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.ProductEntityUsingProductId, "Category_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingRouteId, "RouteEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.SupportpoolEntityUsingSupportpoolId, "Company_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup___":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup____":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup_____":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingSystemMessageRouteId, "RouteEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "VattariffCollectionViaProduct":
					toReturn.Add(Relations.ProductEntityUsingRouteId, "RouteEntity__", "Product_", JoinHint.None);
					toReturn.Add(ProductEntity.Relations.VattariffEntityUsingVattariffId, "Product_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_categoryCollection", (!this.MarkedForDeletion?_categoryCollection:null));
			info.AddValue("_alwaysFetchCategoryCollection", _alwaysFetchCategoryCollection);
			info.AddValue("_alreadyFetchedCategoryCollection", _alreadyFetchedCategoryCollection);
			info.AddValue("_clientConfigurationRouteCollection", (!this.MarkedForDeletion?_clientConfigurationRouteCollection:null));
			info.AddValue("_alwaysFetchClientConfigurationRouteCollection", _alwaysFetchClientConfigurationRouteCollection);
			info.AddValue("_alreadyFetchedClientConfigurationRouteCollection", _alreadyFetchedClientConfigurationRouteCollection);
			info.AddValue("_companyCollection", (!this.MarkedForDeletion?_companyCollection:null));
			info.AddValue("_alwaysFetchCompanyCollection", _alwaysFetchCompanyCollection);
			info.AddValue("_alreadyFetchedCompanyCollection", _alreadyFetchedCompanyCollection);
			info.AddValue("_deliverypointgroupCollection__", (!this.MarkedForDeletion?_deliverypointgroupCollection__:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection__", _alwaysFetchDeliverypointgroupCollection__);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection__", _alreadyFetchedDeliverypointgroupCollection__);
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_deliverypointgroupCollection___", (!this.MarkedForDeletion?_deliverypointgroupCollection___:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection___", _alwaysFetchDeliverypointgroupCollection___);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection___", _alreadyFetchedDeliverypointgroupCollection___);
			info.AddValue("_deliverypointgroupCollection_", (!this.MarkedForDeletion?_deliverypointgroupCollection_:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection_", _alwaysFetchDeliverypointgroupCollection_);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection_", _alreadyFetchedDeliverypointgroupCollection_);
			info.AddValue("_productCollection", (!this.MarkedForDeletion?_productCollection:null));
			info.AddValue("_alwaysFetchProductCollection", _alwaysFetchProductCollection);
			info.AddValue("_alreadyFetchedProductCollection", _alreadyFetchedProductCollection);
			info.AddValue("_escalationFromRouteCollection", (!this.MarkedForDeletion?_escalationFromRouteCollection:null));
			info.AddValue("_alwaysFetchEscalationFromRouteCollection", _alwaysFetchEscalationFromRouteCollection);
			info.AddValue("_alreadyFetchedEscalationFromRouteCollection", _alreadyFetchedEscalationFromRouteCollection);
			info.AddValue("_routestepCollection", (!this.MarkedForDeletion?_routestepCollection:null));
			info.AddValue("_alwaysFetchRoutestepCollection", _alwaysFetchRoutestepCollection);
			info.AddValue("_alreadyFetchedRoutestepCollection", _alreadyFetchedRoutestepCollection);
			info.AddValue("_announcementCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_announcementCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup", _alwaysFetchAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup", _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_announcementCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_announcementCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup_", _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_", _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_);
			info.AddValue("_categoryCollectionViaCategory", (!this.MarkedForDeletion?_categoryCollectionViaCategory:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaCategory", _alwaysFetchCategoryCollectionViaCategory);
			info.AddValue("_alreadyFetchedCategoryCollectionViaCategory", _alreadyFetchedCategoryCollectionViaCategory);
			info.AddValue("_companyCollectionViaCategory", (!this.MarkedForDeletion?_companyCollectionViaCategory:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaCategory", _alwaysFetchCompanyCollectionViaCategory);
			info.AddValue("_alreadyFetchedCompanyCollectionViaCategory", _alreadyFetchedCompanyCollectionViaCategory);
			info.AddValue("_companyCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup", _alwaysFetchCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup", _alreadyFetchedCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_companyCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup_", _alwaysFetchCompanyCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup_", _alreadyFetchedCompanyCollectionViaDeliverypointgroup_);
			info.AddValue("_companyCollectionViaProduct", (!this.MarkedForDeletion?_companyCollectionViaProduct:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaProduct", _alwaysFetchCompanyCollectionViaProduct);
			info.AddValue("_alreadyFetchedCompanyCollectionViaProduct", _alreadyFetchedCompanyCollectionViaProduct);
			info.AddValue("_companyCollectionViaRoute", (!this.MarkedForDeletion?_companyCollectionViaRoute:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaRoute", _alwaysFetchCompanyCollectionViaRoute);
			info.AddValue("_alreadyFetchedCompanyCollectionViaRoute", _alreadyFetchedCompanyCollectionViaRoute);
			info.AddValue("_companyOwnerCollectionViaCompany", (!this.MarkedForDeletion?_companyOwnerCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCompanyOwnerCollectionViaCompany", _alwaysFetchCompanyOwnerCollectionViaCompany);
			info.AddValue("_alreadyFetchedCompanyOwnerCollectionViaCompany", _alreadyFetchedCompanyOwnerCollectionViaCompany);
			info.AddValue("_countryCollectionViaCompany", (!this.MarkedForDeletion?_countryCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCountryCollectionViaCompany", _alwaysFetchCountryCollectionViaCompany);
			info.AddValue("_alreadyFetchedCountryCollectionViaCompany", _alreadyFetchedCountryCollectionViaCompany);
			info.AddValue("_currencyCollectionViaCompany", (!this.MarkedForDeletion?_currencyCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCurrencyCollectionViaCompany", _alwaysFetchCurrencyCollectionViaCompany);
			info.AddValue("_alreadyFetchedCurrencyCollectionViaCompany", _alreadyFetchedCurrencyCollectionViaCompany);
			info.AddValue("_genericcategoryCollectionViaCategory", (!this.MarkedForDeletion?_genericcategoryCollectionViaCategory:null));
			info.AddValue("_alwaysFetchGenericcategoryCollectionViaCategory", _alwaysFetchGenericcategoryCollectionViaCategory);
			info.AddValue("_alreadyFetchedGenericcategoryCollectionViaCategory", _alreadyFetchedGenericcategoryCollectionViaCategory);
			info.AddValue("_genericproductCollectionViaProduct", (!this.MarkedForDeletion?_genericproductCollectionViaProduct:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaProduct", _alwaysFetchGenericproductCollectionViaProduct);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaProduct", _alreadyFetchedGenericproductCollectionViaProduct);
			info.AddValue("_languageCollectionViaCompany", (!this.MarkedForDeletion?_languageCollectionViaCompany:null));
			info.AddValue("_alwaysFetchLanguageCollectionViaCompany", _alwaysFetchLanguageCollectionViaCompany);
			info.AddValue("_alreadyFetchedLanguageCollectionViaCompany", _alreadyFetchedLanguageCollectionViaCompany);
			info.AddValue("_menuCollectionViaCategory", (!this.MarkedForDeletion?_menuCollectionViaCategory:null));
			info.AddValue("_alwaysFetchMenuCollectionViaCategory", _alwaysFetchMenuCollectionViaCategory);
			info.AddValue("_alreadyFetchedMenuCollectionViaCategory", _alreadyFetchedMenuCollectionViaCategory);
			info.AddValue("_menuCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_menuCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchMenuCollectionViaDeliverypointgroup", _alwaysFetchMenuCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedMenuCollectionViaDeliverypointgroup", _alreadyFetchedMenuCollectionViaDeliverypointgroup);
			info.AddValue("_menuCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_menuCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchMenuCollectionViaDeliverypointgroup_", _alwaysFetchMenuCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedMenuCollectionViaDeliverypointgroup_", _alreadyFetchedMenuCollectionViaDeliverypointgroup_);
			info.AddValue("_poscategoryCollectionViaCategory", (!this.MarkedForDeletion?_poscategoryCollectionViaCategory:null));
			info.AddValue("_alwaysFetchPoscategoryCollectionViaCategory", _alwaysFetchPoscategoryCollectionViaCategory);
			info.AddValue("_alreadyFetchedPoscategoryCollectionViaCategory", _alreadyFetchedPoscategoryCollectionViaCategory);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_);
			info.AddValue("_posproductCollectionViaProduct", (!this.MarkedForDeletion?_posproductCollectionViaProduct:null));
			info.AddValue("_alwaysFetchPosproductCollectionViaProduct", _alwaysFetchPosproductCollectionViaProduct);
			info.AddValue("_alreadyFetchedPosproductCollectionViaProduct", _alreadyFetchedPosproductCollectionViaProduct);
			info.AddValue("_productCollectionViaCategory", (!this.MarkedForDeletion?_productCollectionViaCategory:null));
			info.AddValue("_alwaysFetchProductCollectionViaCategory", _alwaysFetchProductCollectionViaCategory);
			info.AddValue("_alreadyFetchedProductCollectionViaCategory", _alreadyFetchedProductCollectionViaCategory);
			info.AddValue("_routeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup", _alwaysFetchRouteCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup", _alreadyFetchedRouteCollectionViaDeliverypointgroup);
			info.AddValue("_routeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup_", _alwaysFetchRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup_", _alreadyFetchedRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_supportpoolCollectionViaCompany", (!this.MarkedForDeletion?_supportpoolCollectionViaCompany:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaCompany", _alwaysFetchSupportpoolCollectionViaCompany);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaCompany", _alreadyFetchedSupportpoolCollectionViaCompany);
			info.AddValue("_terminalCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_terminalCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaDeliverypointgroup", _alwaysFetchTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedTerminalCollectionViaDeliverypointgroup", _alreadyFetchedTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_terminalCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_terminalCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaDeliverypointgroup_", _alwaysFetchTerminalCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedTerminalCollectionViaDeliverypointgroup_", _alreadyFetchedTerminalCollectionViaDeliverypointgroup_);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup__", _alwaysFetchUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__", _alreadyFetchedUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup___", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup___:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup___", _alwaysFetchUIModeCollectionViaDeliverypointgroup___);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup___", _alreadyFetchedUIModeCollectionViaDeliverypointgroup___);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup____", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup____:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup____", _alwaysFetchUIModeCollectionViaDeliverypointgroup____);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup____", _alreadyFetchedUIModeCollectionViaDeliverypointgroup____);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup_____", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup_____:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup_____", _alwaysFetchUIModeCollectionViaDeliverypointgroup_____);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____", _alreadyFetchedUIModeCollectionViaDeliverypointgroup_____);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup", _alwaysFetchUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup", _alreadyFetchedUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup_", _alwaysFetchUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_", _alreadyFetchedUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_vattariffCollectionViaProduct", (!this.MarkedForDeletion?_vattariffCollectionViaProduct:null));
			info.AddValue("_alwaysFetchVattariffCollectionViaProduct", _alwaysFetchVattariffCollectionViaProduct);
			info.AddValue("_alreadyFetchedVattariffCollectionViaProduct", _alreadyFetchedVattariffCollectionViaProduct);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_escalationRouteEntity", (!this.MarkedForDeletion?_escalationRouteEntity:null));
			info.AddValue("_escalationRouteEntityReturnsNewIfNotFound", _escalationRouteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEscalationRouteEntity", _alwaysFetchEscalationRouteEntity);
			info.AddValue("_alreadyFetchedEscalationRouteEntity", _alreadyFetchedEscalationRouteEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "EscalationRouteEntity":
					_alreadyFetchedEscalationRouteEntity = true;
					this.EscalationRouteEntity = (RouteEntity)entity;
					break;
				case "CategoryCollection":
					_alreadyFetchedCategoryCollection = true;
					if(entity!=null)
					{
						this.CategoryCollection.Add((CategoryEntity)entity);
					}
					break;
				case "ClientConfigurationRouteCollection":
					_alreadyFetchedClientConfigurationRouteCollection = true;
					if(entity!=null)
					{
						this.ClientConfigurationRouteCollection.Add((ClientConfigurationRouteEntity)entity);
					}
					break;
				case "CompanyCollection":
					_alreadyFetchedCompanyCollection = true;
					if(entity!=null)
					{
						this.CompanyCollection.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection__":
					_alreadyFetchedDeliverypointgroupCollection__ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection__.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection___":
					_alreadyFetchedDeliverypointgroupCollection___ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection___.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection_":
					_alreadyFetchedDeliverypointgroupCollection_ = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection_.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "ProductCollection":
					_alreadyFetchedProductCollection = true;
					if(entity!=null)
					{
						this.ProductCollection.Add((ProductEntity)entity);
					}
					break;
				case "EscalationFromRouteCollection":
					_alreadyFetchedEscalationFromRouteCollection = true;
					if(entity!=null)
					{
						this.EscalationFromRouteCollection.Add((RouteEntity)entity);
					}
					break;
				case "RoutestepCollection":
					_alreadyFetchedRoutestepCollection = true;
					if(entity!=null)
					{
						this.RoutestepCollection.Add((RoutestepEntity)entity);
					}
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.AnnouncementCollectionViaDeliverypointgroup.Add((AnnouncementEntity)entity);
					}
					break;
				case "AnnouncementCollectionViaDeliverypointgroup_":
					_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.AnnouncementCollectionViaDeliverypointgroup_.Add((AnnouncementEntity)entity);
					}
					break;
				case "CategoryCollectionViaCategory":
					_alreadyFetchedCategoryCollectionViaCategory = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaCategory.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaCategory":
					_alreadyFetchedCompanyCollectionViaCategory = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaCategory.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup_":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup_.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaProduct":
					_alreadyFetchedCompanyCollectionViaProduct = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaProduct.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaRoute":
					_alreadyFetchedCompanyCollectionViaRoute = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaRoute.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyOwnerCollectionViaCompany":
					_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CompanyOwnerCollectionViaCompany.Add((CompanyOwnerEntity)entity);
					}
					break;
				case "CountryCollectionViaCompany":
					_alreadyFetchedCountryCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CountryCollectionViaCompany.Add((CountryEntity)entity);
					}
					break;
				case "CurrencyCollectionViaCompany":
					_alreadyFetchedCurrencyCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CurrencyCollectionViaCompany.Add((CurrencyEntity)entity);
					}
					break;
				case "GenericcategoryCollectionViaCategory":
					_alreadyFetchedGenericcategoryCollectionViaCategory = true;
					if(entity!=null)
					{
						this.GenericcategoryCollectionViaCategory.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericproductCollectionViaProduct":
					_alreadyFetchedGenericproductCollectionViaProduct = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaProduct.Add((GenericproductEntity)entity);
					}
					break;
				case "LanguageCollectionViaCompany":
					_alreadyFetchedLanguageCollectionViaCompany = true;
					if(entity!=null)
					{
						this.LanguageCollectionViaCompany.Add((LanguageEntity)entity);
					}
					break;
				case "MenuCollectionViaCategory":
					_alreadyFetchedMenuCollectionViaCategory = true;
					if(entity!=null)
					{
						this.MenuCollectionViaCategory.Add((MenuEntity)entity);
					}
					break;
				case "MenuCollectionViaDeliverypointgroup":
					_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.MenuCollectionViaDeliverypointgroup.Add((MenuEntity)entity);
					}
					break;
				case "MenuCollectionViaDeliverypointgroup_":
					_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.MenuCollectionViaDeliverypointgroup_.Add((MenuEntity)entity);
					}
					break;
				case "PoscategoryCollectionViaCategory":
					_alreadyFetchedPoscategoryCollectionViaCategory = true;
					if(entity!=null)
					{
						this.PoscategoryCollectionViaCategory.Add((PoscategoryEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup_":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup_.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "PosproductCollectionViaProduct":
					_alreadyFetchedPosproductCollectionViaProduct = true;
					if(entity!=null)
					{
						this.PosproductCollectionViaProduct.Add((PosproductEntity)entity);
					}
					break;
				case "ProductCollectionViaCategory":
					_alreadyFetchedProductCollectionViaCategory = true;
					if(entity!=null)
					{
						this.ProductCollectionViaCategory.Add((ProductEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup_.Add((RouteEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaCompany":
					_alreadyFetchedSupportpoolCollectionViaCompany = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaCompany.Add((SupportpoolEntity)entity);
					}
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaDeliverypointgroup.Add((TerminalEntity)entity);
					}
					break;
				case "TerminalCollectionViaDeliverypointgroup_":
					_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaDeliverypointgroup_.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup__.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup___":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup___.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup____":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup____.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup_____":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup_____.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup_.Add((UIModeEntity)entity);
					}
					break;
				case "VattariffCollectionViaProduct":
					_alreadyFetchedVattariffCollectionViaProduct = true;
					if(entity!=null)
					{
						this.VattariffCollectionViaProduct.Add((VattariffEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "EscalationRouteEntity":
					SetupSyncEscalationRouteEntity(relatedEntity);
					break;
				case "CategoryCollection":
					_categoryCollection.Add((CategoryEntity)relatedEntity);
					break;
				case "ClientConfigurationRouteCollection":
					_clientConfigurationRouteCollection.Add((ClientConfigurationRouteEntity)relatedEntity);
					break;
				case "CompanyCollection":
					_companyCollection.Add((CompanyEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection__":
					_deliverypointgroupCollection__.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection___":
					_deliverypointgroupCollection___.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection_":
					_deliverypointgroupCollection_.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "ProductCollection":
					_productCollection.Add((ProductEntity)relatedEntity);
					break;
				case "EscalationFromRouteCollection":
					_escalationFromRouteCollection.Add((RouteEntity)relatedEntity);
					break;
				case "RoutestepCollection":
					_routestepCollection.Add((RoutestepEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "EscalationRouteEntity":
					DesetupSyncEscalationRouteEntity(false, true);
					break;
				case "CategoryCollection":
					this.PerformRelatedEntityRemoval(_categoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientConfigurationRouteCollection":
					this.PerformRelatedEntityRemoval(_clientConfigurationRouteCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CompanyCollection":
					this.PerformRelatedEntityRemoval(_companyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection__":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection__, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection___":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection___, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection_":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCollection":
					this.PerformRelatedEntityRemoval(_productCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EscalationFromRouteCollection":
					this.PerformRelatedEntityRemoval(_escalationFromRouteCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoutestepCollection":
					this.PerformRelatedEntityRemoval(_routestepCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_escalationRouteEntity!=null)
			{
				toReturn.Add(_escalationRouteEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_categoryCollection);
			toReturn.Add(_clientConfigurationRouteCollection);
			toReturn.Add(_companyCollection);
			toReturn.Add(_deliverypointgroupCollection__);
			toReturn.Add(_deliverypointgroupCollection);
			toReturn.Add(_deliverypointgroupCollection___);
			toReturn.Add(_deliverypointgroupCollection_);
			toReturn.Add(_productCollection);
			toReturn.Add(_escalationFromRouteCollection);
			toReturn.Add(_routestepCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routeId)
		{
			return FetchUsingPK(routeId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routeId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(routeId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routeId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(routeId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 routeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(routeId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RouteId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RouteRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryCollection || forceFetch || _alwaysFetchCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollection);
				_categoryCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, filter);
				_categoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollection = true;
			}
			return _categoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollection'. These settings will be taken into account
		/// when the property CategoryCollection is requested or GetMultiCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollection.SortClauses=sortClauses;
			_categoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationRouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch)
		{
			return GetMultiClientConfigurationRouteCollection(forceFetch, _clientConfigurationRouteCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationRouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientConfigurationRouteCollection(forceFetch, _clientConfigurationRouteCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientConfigurationRouteCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection GetMultiClientConfigurationRouteCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientConfigurationRouteCollection || forceFetch || _alwaysFetchClientConfigurationRouteCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientConfigurationRouteCollection);
				_clientConfigurationRouteCollection.SuppressClearInGetMulti=!forceFetch;
				_clientConfigurationRouteCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientConfigurationRouteCollection.GetMultiManyToOne(null, this, filter);
				_clientConfigurationRouteCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientConfigurationRouteCollection = true;
			}
			return _clientConfigurationRouteCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientConfigurationRouteCollection'. These settings will be taken into account
		/// when the property ClientConfigurationRouteCollection is requested or GetMultiClientConfigurationRouteCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientConfigurationRouteCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientConfigurationRouteCollection.SortClauses=sortClauses;
			_clientConfigurationRouteCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyCollection || forceFetch || _alwaysFetchCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollection);
				_companyCollection.SuppressClearInGetMulti=!forceFetch;
				_companyCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, filter);
				_companyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollection = true;
			}
			return _companyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollection'. These settings will be taken into account
		/// when the property CompanyCollection is requested or GetMultiCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollection.SortClauses=sortClauses;
			_companyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection__(forceFetch, _deliverypointgroupCollection__.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection__(forceFetch, _deliverypointgroupCollection__.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection__(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection__(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection__ || forceFetch || _alwaysFetchDeliverypointgroupCollection__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection__);
				_deliverypointgroupCollection__.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection__.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection__.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_deliverypointgroupCollection__.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection__ = true;
			}
			return _deliverypointgroupCollection__;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection__'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection__ is requested or GetMultiDeliverypointgroupCollection__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection__.SortClauses=sortClauses;
			_deliverypointgroupCollection__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection___(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection___(forceFetch, _deliverypointgroupCollection___.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection___(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection___(forceFetch, _deliverypointgroupCollection___.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection___(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection___(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection___ || forceFetch || _alwaysFetchDeliverypointgroupCollection___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection___);
				_deliverypointgroupCollection___.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection___.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection___.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_deliverypointgroupCollection___.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection___ = true;
			}
			return _deliverypointgroupCollection___;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection___'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection___ is requested or GetMultiDeliverypointgroupCollection___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection___.SortClauses=sortClauses;
			_deliverypointgroupCollection___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection_(forceFetch, _deliverypointgroupCollection_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection_(forceFetch, _deliverypointgroupCollection_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection_ || forceFetch || _alwaysFetchDeliverypointgroupCollection_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection_);
				_deliverypointgroupCollection_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection_.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_deliverypointgroupCollection_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection_ = true;
			}
			return _deliverypointgroupCollection_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection_'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection_ is requested or GetMultiDeliverypointgroupCollection_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection_.SortClauses=sortClauses;
			_deliverypointgroupCollection_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCollection || forceFetch || _alwaysFetchProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollection);
				_productCollection.SuppressClearInGetMulti=!forceFetch;
				_productCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_productCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollection = true;
			}
			return _productCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollection'. These settings will be taken into account
		/// when the property ProductCollection is requested or GetMultiProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollection.SortClauses=sortClauses;
			_productCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiEscalationFromRouteCollection(bool forceFetch)
		{
			return GetMultiEscalationFromRouteCollection(forceFetch, _escalationFromRouteCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiEscalationFromRouteCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEscalationFromRouteCollection(forceFetch, _escalationFromRouteCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiEscalationFromRouteCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEscalationFromRouteCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection GetMultiEscalationFromRouteCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEscalationFromRouteCollection || forceFetch || _alwaysFetchEscalationFromRouteCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_escalationFromRouteCollection);
				_escalationFromRouteCollection.SuppressClearInGetMulti=!forceFetch;
				_escalationFromRouteCollection.EntityFactoryToUse = entityFactoryToUse;
				_escalationFromRouteCollection.GetMultiManyToOne(null, this, filter);
				_escalationFromRouteCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedEscalationFromRouteCollection = true;
			}
			return _escalationFromRouteCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'EscalationFromRouteCollection'. These settings will be taken into account
		/// when the property EscalationFromRouteCollection is requested or GetMultiEscalationFromRouteCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEscalationFromRouteCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_escalationFromRouteCollection.SortClauses=sortClauses;
			_escalationFromRouteCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoutestepEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollection(bool forceFetch)
		{
			return GetMultiRoutestepCollection(forceFetch, _routestepCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoutestepEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutestepCollection(forceFetch, _routestepCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutestepCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoutestepCollection GetMultiRoutestepCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutestepCollection || forceFetch || _alwaysFetchRoutestepCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routestepCollection);
				_routestepCollection.SuppressClearInGetMulti=!forceFetch;
				_routestepCollection.EntityFactoryToUse = entityFactoryToUse;
				_routestepCollection.GetMultiManyToOne(this, filter);
				_routestepCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutestepCollection = true;
			}
			return _routestepCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutestepCollection'. These settings will be taken into account
		/// when the property RoutestepCollection is requested or GetMultiRoutestepCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutestepCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routestepCollection.SortClauses=sortClauses;
			_routestepCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiAnnouncementCollectionViaDeliverypointgroup(forceFetch, _announcementCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchAnnouncementCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_announcementCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
			}
			return _announcementCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property AnnouncementCollectionViaDeliverypointgroup is requested or GetMultiAnnouncementCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_announcementCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiAnnouncementCollectionViaDeliverypointgroup_(forceFetch, _announcementCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_announcementCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_announcementCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup_"));
				_announcementCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = true;
			}
			return _announcementCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property AnnouncementCollectionViaDeliverypointgroup_ is requested or GetMultiAnnouncementCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_announcementCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaCategory(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaCategory(forceFetch, _categoryCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaCategory || forceFetch || _alwaysFetchCategoryCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_categoryCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaCategory.GetMulti(filter, GetRelationsForField("CategoryCollectionViaCategory"));
				_categoryCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaCategory = true;
			}
			return _categoryCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaCategory'. These settings will be taken into account
		/// when the property CategoryCollectionViaCategory is requested or GetMultiCategoryCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaCategory.SortClauses=sortClauses;
			_categoryCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaCategory(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaCategory(forceFetch, _companyCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaCategory || forceFetch || _alwaysFetchCompanyCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_companyCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaCategory.GetMulti(filter, GetRelationsForField("CompanyCollectionViaCategory"));
				_companyCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaCategory = true;
			}
			return _companyCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaCategory'. These settings will be taken into account
		/// when the property CompanyCollectionViaCategory is requested or GetMultiCompanyCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaCategory.SortClauses=sortClauses;
			_companyCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup(forceFetch, _companyCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
			}
			return _companyCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup is requested or GetMultiCompanyCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup_(forceFetch, _companyCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_companyCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup_"));
				_companyCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = true;
			}
			return _companyCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup_ is requested or GetMultiCompanyCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaProduct(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaProduct(forceFetch, _companyCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaProduct || forceFetch || _alwaysFetchCompanyCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_companyCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaProduct.GetMulti(filter, GetRelationsForField("CompanyCollectionViaProduct"));
				_companyCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaProduct = true;
			}
			return _companyCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaProduct'. These settings will be taken into account
		/// when the property CompanyCollectionViaProduct is requested or GetMultiCompanyCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaProduct.SortClauses=sortClauses;
			_companyCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaRoute(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaRoute(forceFetch, _companyCollectionViaRoute.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaRoute(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaRoute || forceFetch || _alwaysFetchCompanyCollectionViaRoute) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaRoute);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_companyCollectionViaRoute.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaRoute.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaRoute.GetMulti(filter, GetRelationsForField("CompanyCollectionViaRoute"));
				_companyCollectionViaRoute.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaRoute = true;
			}
			return _companyCollectionViaRoute;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaRoute'. These settings will be taken into account
		/// when the property CompanyCollectionViaRoute is requested or GetMultiCompanyCollectionViaRoute is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaRoute(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaRoute.SortClauses=sortClauses;
			_companyCollectionViaRoute.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyOwnerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCompanyOwnerCollectionViaCompany(forceFetch, _companyOwnerCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyOwnerCollectionViaCompany || forceFetch || _alwaysFetchCompanyOwnerCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyOwnerCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_companyOwnerCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_companyOwnerCollectionViaCompany.GetMulti(filter, GetRelationsForField("CompanyOwnerCollectionViaCompany"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
			}
			return _companyOwnerCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyOwnerCollectionViaCompany'. These settings will be taken into account
		/// when the property CompanyOwnerCollectionViaCompany is requested or GetMultiCompanyOwnerCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyOwnerCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyOwnerCollectionViaCompany.SortClauses=sortClauses;
			_companyOwnerCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CountryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCountryCollectionViaCompany(forceFetch, _countryCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCountryCollectionViaCompany || forceFetch || _alwaysFetchCountryCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_countryCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_countryCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_countryCollectionViaCompany.GetMulti(filter, GetRelationsForField("CountryCollectionViaCompany"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCountryCollectionViaCompany = true;
			}
			return _countryCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CountryCollectionViaCompany'. These settings will be taken into account
		/// when the property CountryCollectionViaCompany is requested or GetMultiCountryCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCountryCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_countryCollectionViaCompany.SortClauses=sortClauses;
			_countryCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CurrencyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCurrencyCollectionViaCompany(forceFetch, _currencyCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCurrencyCollectionViaCompany || forceFetch || _alwaysFetchCurrencyCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_currencyCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_currencyCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_currencyCollectionViaCompany.GetMulti(filter, GetRelationsForField("CurrencyCollectionViaCompany"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCurrencyCollectionViaCompany = true;
			}
			return _currencyCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CurrencyCollectionViaCompany'. These settings will be taken into account
		/// when the property CurrencyCollectionViaCompany is requested or GetMultiCurrencyCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCurrencyCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_currencyCollectionViaCompany.SortClauses=sortClauses;
			_currencyCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaCategory(bool forceFetch)
		{
			return GetMultiGenericcategoryCollectionViaCategory(forceFetch, _genericcategoryCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollectionViaCategory || forceFetch || _alwaysFetchGenericcategoryCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_genericcategoryCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollectionViaCategory.GetMulti(filter, GetRelationsForField("GenericcategoryCollectionViaCategory"));
				_genericcategoryCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollectionViaCategory = true;
			}
			return _genericcategoryCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollectionViaCategory'. These settings will be taken into account
		/// when the property GenericcategoryCollectionViaCategory is requested or GetMultiGenericcategoryCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollectionViaCategory.SortClauses=sortClauses;
			_genericcategoryCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaProduct(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaProduct(forceFetch, _genericproductCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaProduct || forceFetch || _alwaysFetchGenericproductCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_genericproductCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaProduct.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaProduct"));
				_genericproductCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaProduct = true;
			}
			return _genericproductCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaProduct'. These settings will be taken into account
		/// when the property GenericproductCollectionViaProduct is requested or GetMultiGenericproductCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaProduct.SortClauses=sortClauses;
			_genericproductCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaCompany(bool forceFetch)
		{
			return GetMultiLanguageCollectionViaCompany(forceFetch, _languageCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedLanguageCollectionViaCompany || forceFetch || _alwaysFetchLanguageCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_languageCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_languageCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_languageCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_languageCollectionViaCompany.GetMulti(filter, GetRelationsForField("LanguageCollectionViaCompany"));
				_languageCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedLanguageCollectionViaCompany = true;
			}
			return _languageCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'LanguageCollectionViaCompany'. These settings will be taken into account
		/// when the property LanguageCollectionViaCompany is requested or GetMultiLanguageCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLanguageCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_languageCollectionViaCompany.SortClauses=sortClauses;
			_languageCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch)
		{
			return GetMultiMenuCollectionViaCategory(forceFetch, _menuCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaCategory || forceFetch || _alwaysFetchMenuCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaCategory.GetMulti(filter, GetRelationsForField("MenuCollectionViaCategory"));
				_menuCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaCategory = true;
			}
			return _menuCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaCategory'. These settings will be taken into account
		/// when the property MenuCollectionViaCategory is requested or GetMultiMenuCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaCategory.SortClauses=sortClauses;
			_menuCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiMenuCollectionViaDeliverypointgroup(forceFetch, _menuCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchMenuCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("MenuCollectionViaDeliverypointgroup"));
				_menuCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = true;
			}
			return _menuCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property MenuCollectionViaDeliverypointgroup is requested or GetMultiMenuCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_menuCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MenuEntity'</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiMenuCollectionViaDeliverypointgroup_(forceFetch, _menuCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MenuCollection GetMultiMenuCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedMenuCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchMenuCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_menuCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_menuCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_menuCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_menuCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("MenuCollectionViaDeliverypointgroup_"));
				_menuCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = true;
			}
			return _menuCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'MenuCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property MenuCollectionViaDeliverypointgroup_ is requested or GetMultiMenuCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMenuCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_menuCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_menuCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PoscategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PoscategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.PoscategoryCollection GetMultiPoscategoryCollectionViaCategory(bool forceFetch)
		{
			return GetMultiPoscategoryCollectionViaCategory(forceFetch, _poscategoryCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PoscategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PoscategoryCollection GetMultiPoscategoryCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPoscategoryCollectionViaCategory || forceFetch || _alwaysFetchPoscategoryCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_poscategoryCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_poscategoryCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_poscategoryCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_poscategoryCollectionViaCategory.GetMulti(filter, GetRelationsForField("PoscategoryCollectionViaCategory"));
				_poscategoryCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedPoscategoryCollectionViaCategory = true;
			}
			return _poscategoryCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'PoscategoryCollectionViaCategory'. These settings will be taken into account
		/// when the property PoscategoryCollectionViaCategory is requested or GetMultiPoscategoryCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPoscategoryCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_poscategoryCollectionViaCategory.SortClauses=sortClauses;
			_poscategoryCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup_"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup_ is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosproductCollection GetMultiPosproductCollectionViaProduct(bool forceFetch)
		{
			return GetMultiPosproductCollectionViaProduct(forceFetch, _posproductCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosproductCollection GetMultiPosproductCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosproductCollectionViaProduct || forceFetch || _alwaysFetchPosproductCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posproductCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_posproductCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_posproductCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_posproductCollectionViaProduct.GetMulti(filter, GetRelationsForField("PosproductCollectionViaProduct"));
				_posproductCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedPosproductCollectionViaProduct = true;
			}
			return _posproductCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosproductCollectionViaProduct'. These settings will be taken into account
		/// when the property PosproductCollectionViaProduct is requested or GetMultiPosproductCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosproductCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posproductCollectionViaProduct.SortClauses=sortClauses;
			_posproductCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaCategory(bool forceFetch)
		{
			return GetMultiProductCollectionViaCategory(forceFetch, _productCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaCategory || forceFetch || _alwaysFetchProductCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_productCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaCategory.GetMulti(filter, GetRelationsForField("ProductCollectionViaCategory"));
				_productCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaCategory = true;
			}
			return _productCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaCategory'. These settings will be taken into account
		/// when the property ProductCollectionViaCategory is requested or GetMultiProductCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaCategory.SortClauses=sortClauses;
			_productCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup(forceFetch, _routeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
			}
			return _routeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup is requested or GetMultiRouteCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup_(forceFetch, _routeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
			}
			return _routeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup_ is requested or GetMultiRouteCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaCompany(forceFetch, _supportpoolCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaCompany || forceFetch || _alwaysFetchSupportpoolCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaCompany.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaCompany"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaCompany = true;
			}
			return _supportpoolCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaCompany'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaCompany is requested or GetMultiSupportpoolCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaCompany.SortClauses=sortClauses;
			_supportpoolCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaDeliverypointgroup(forceFetch, _terminalCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchTerminalCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
			}
			return _terminalCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property TerminalCollectionViaDeliverypointgroup is requested or GetMultiTerminalCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_terminalCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaDeliverypointgroup_(forceFetch, _terminalCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchTerminalCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_terminalCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("TerminalCollectionViaDeliverypointgroup_"));
				_terminalCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = true;
			}
			return _terminalCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property TerminalCollectionViaDeliverypointgroup_ is requested or GetMultiTerminalCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_terminalCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup__(forceFetch, _uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup__ is requested or GetMultiUIModeCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup___(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup___(forceFetch, _uIModeCollectionViaDeliverypointgroup___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_uIModeCollectionViaDeliverypointgroup___.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup___.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup___.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup___"));
				_uIModeCollectionViaDeliverypointgroup___.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup___;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup___'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup___ is requested or GetMultiUIModeCollectionViaDeliverypointgroup___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup___.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup____(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup____(forceFetch, _uIModeCollectionViaDeliverypointgroup____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_uIModeCollectionViaDeliverypointgroup____.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup____.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup____.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup____"));
				_uIModeCollectionViaDeliverypointgroup____.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup____;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup____'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup____ is requested or GetMultiUIModeCollectionViaDeliverypointgroup____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup____.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_____(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup_____(forceFetch, _uIModeCollectionViaDeliverypointgroup_____.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_____(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup_____) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup_____);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_uIModeCollectionViaDeliverypointgroup_____.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup_____.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup_____.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_____"));
				_uIModeCollectionViaDeliverypointgroup_____.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup_____;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup_____'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup_____ is requested or GetMultiUIModeCollectionViaDeliverypointgroup_____ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup_____(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup_____.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup_____.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup(forceFetch, _uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
			}
			return _uIModeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup is requested or GetMultiUIModeCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup_(forceFetch, _uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup_ is requested or GetMultiUIModeCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VattariffEntity'</returns>
		public Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollectionViaProduct(bool forceFetch)
		{
			return GetMultiVattariffCollectionViaProduct(forceFetch, _vattariffCollectionViaProduct.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollectionViaProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedVattariffCollectionViaProduct || forceFetch || _alwaysFetchVattariffCollectionViaProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vattariffCollectionViaProduct);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(RouteFields.RouteId, ComparisonOperator.Equal, this.RouteId, "RouteEntity__"));
				_vattariffCollectionViaProduct.SuppressClearInGetMulti=!forceFetch;
				_vattariffCollectionViaProduct.EntityFactoryToUse = entityFactoryToUse;
				_vattariffCollectionViaProduct.GetMulti(filter, GetRelationsForField("VattariffCollectionViaProduct"));
				_vattariffCollectionViaProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedVattariffCollectionViaProduct = true;
			}
			return _vattariffCollectionViaProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'VattariffCollectionViaProduct'. These settings will be taken into account
		/// when the property VattariffCollectionViaProduct is requested or GetMultiVattariffCollectionViaProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVattariffCollectionViaProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vattariffCollectionViaProduct.SortClauses=sortClauses;
			_vattariffCollectionViaProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public RouteEntity GetSingleEscalationRouteEntity()
		{
			return GetSingleEscalationRouteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteEntity' which is related to this entity.</returns>
		public virtual RouteEntity GetSingleEscalationRouteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEscalationRouteEntity || forceFetch || _alwaysFetchEscalationRouteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteEntityUsingRouteIdEscalationRouteId);
				RouteEntity newEntity = new RouteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EscalationRouteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_escalationRouteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EscalationRouteEntity = newEntity;
				_alreadyFetchedEscalationRouteEntity = fetchResult;
			}
			return _escalationRouteEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("EscalationRouteEntity", _escalationRouteEntity);
			toReturn.Add("CategoryCollection", _categoryCollection);
			toReturn.Add("ClientConfigurationRouteCollection", _clientConfigurationRouteCollection);
			toReturn.Add("CompanyCollection", _companyCollection);
			toReturn.Add("DeliverypointgroupCollection__", _deliverypointgroupCollection__);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("DeliverypointgroupCollection___", _deliverypointgroupCollection___);
			toReturn.Add("DeliverypointgroupCollection_", _deliverypointgroupCollection_);
			toReturn.Add("ProductCollection", _productCollection);
			toReturn.Add("EscalationFromRouteCollection", _escalationFromRouteCollection);
			toReturn.Add("RoutestepCollection", _routestepCollection);
			toReturn.Add("AnnouncementCollectionViaDeliverypointgroup", _announcementCollectionViaDeliverypointgroup);
			toReturn.Add("AnnouncementCollectionViaDeliverypointgroup_", _announcementCollectionViaDeliverypointgroup_);
			toReturn.Add("CategoryCollectionViaCategory", _categoryCollectionViaCategory);
			toReturn.Add("CompanyCollectionViaCategory", _companyCollectionViaCategory);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup", _companyCollectionViaDeliverypointgroup);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup_", _companyCollectionViaDeliverypointgroup_);
			toReturn.Add("CompanyCollectionViaProduct", _companyCollectionViaProduct);
			toReturn.Add("CompanyCollectionViaRoute", _companyCollectionViaRoute);
			toReturn.Add("CompanyOwnerCollectionViaCompany", _companyOwnerCollectionViaCompany);
			toReturn.Add("CountryCollectionViaCompany", _countryCollectionViaCompany);
			toReturn.Add("CurrencyCollectionViaCompany", _currencyCollectionViaCompany);
			toReturn.Add("GenericcategoryCollectionViaCategory", _genericcategoryCollectionViaCategory);
			toReturn.Add("GenericproductCollectionViaProduct", _genericproductCollectionViaProduct);
			toReturn.Add("LanguageCollectionViaCompany", _languageCollectionViaCompany);
			toReturn.Add("MenuCollectionViaCategory", _menuCollectionViaCategory);
			toReturn.Add("MenuCollectionViaDeliverypointgroup", _menuCollectionViaDeliverypointgroup);
			toReturn.Add("MenuCollectionViaDeliverypointgroup_", _menuCollectionViaDeliverypointgroup_);
			toReturn.Add("PoscategoryCollectionViaCategory", _poscategoryCollectionViaCategory);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup", _posdeliverypointgroupCollectionViaDeliverypointgroup);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup_", _posdeliverypointgroupCollectionViaDeliverypointgroup_);
			toReturn.Add("PosproductCollectionViaProduct", _posproductCollectionViaProduct);
			toReturn.Add("ProductCollectionViaCategory", _productCollectionViaCategory);
			toReturn.Add("RouteCollectionViaDeliverypointgroup", _routeCollectionViaDeliverypointgroup);
			toReturn.Add("RouteCollectionViaDeliverypointgroup_", _routeCollectionViaDeliverypointgroup_);
			toReturn.Add("SupportpoolCollectionViaCompany", _supportpoolCollectionViaCompany);
			toReturn.Add("TerminalCollectionViaDeliverypointgroup", _terminalCollectionViaDeliverypointgroup);
			toReturn.Add("TerminalCollectionViaDeliverypointgroup_", _terminalCollectionViaDeliverypointgroup_);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup__", _uIModeCollectionViaDeliverypointgroup__);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup___", _uIModeCollectionViaDeliverypointgroup___);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup____", _uIModeCollectionViaDeliverypointgroup____);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup_____", _uIModeCollectionViaDeliverypointgroup_____);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup", _uIModeCollectionViaDeliverypointgroup);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup_", _uIModeCollectionViaDeliverypointgroup_);
			toReturn.Add("VattariffCollectionViaProduct", _vattariffCollectionViaProduct);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="validator">The validator object for this RouteEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 routeId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(routeId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_categoryCollection = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollection.SetContainingEntityInfo(this, "RouteEntity");

			_clientConfigurationRouteCollection = new Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection();
			_clientConfigurationRouteCollection.SetContainingEntityInfo(this, "RouteEntity");

			_companyCollection = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollection.SetContainingEntityInfo(this, "RouteEntity");

			_deliverypointgroupCollection__ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection__.SetContainingEntityInfo(this, "EmailDocumentRouteEntity");

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "RouteEntity");

			_deliverypointgroupCollection___ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection___.SetContainingEntityInfo(this, "RouteEntity_");

			_deliverypointgroupCollection_ = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection_.SetContainingEntityInfo(this, "SystemMessageRouteEntity");

			_productCollection = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollection.SetContainingEntityInfo(this, "RouteEntity");

			_escalationFromRouteCollection = new Obymobi.Data.CollectionClasses.RouteCollection();
			_escalationFromRouteCollection.SetContainingEntityInfo(this, "EscalationRouteEntity");

			_routestepCollection = new Obymobi.Data.CollectionClasses.RoutestepCollection();
			_routestepCollection.SetContainingEntityInfo(this, "RouteEntity");
			_announcementCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_announcementCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_categoryCollectionViaCategory = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaCategory = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaProduct = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaRoute = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyOwnerCollectionViaCompany = new Obymobi.Data.CollectionClasses.CompanyOwnerCollection();
			_countryCollectionViaCompany = new Obymobi.Data.CollectionClasses.CountryCollection();
			_currencyCollectionViaCompany = new Obymobi.Data.CollectionClasses.CurrencyCollection();
			_genericcategoryCollectionViaCategory = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericproductCollectionViaProduct = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_languageCollectionViaCompany = new Obymobi.Data.CollectionClasses.LanguageCollection();
			_menuCollectionViaCategory = new Obymobi.Data.CollectionClasses.MenuCollection();
			_menuCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.MenuCollection();
			_menuCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.MenuCollection();
			_poscategoryCollectionViaCategory = new Obymobi.Data.CollectionClasses.PoscategoryCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_posproductCollectionViaProduct = new Obymobi.Data.CollectionClasses.PosproductCollection();
			_productCollectionViaCategory = new Obymobi.Data.CollectionClasses.ProductCollection();
			_routeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_supportpoolCollectionViaCompany = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_terminalCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup___ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup____ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup_____ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_vattariffCollectionViaProduct = new Obymobi.Data.CollectionClasses.VattariffCollection();
			_companyEntityReturnsNewIfNotFound = true;
			_escalationRouteEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogAlways", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StepTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EscalationRouteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BeingHandledSupportNotificationTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RetrievalSupportNotificationTimeoutMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticRouteRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "RouteCollection", resetFKFields, new int[] { (int)RouteFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticRouteRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _escalationRouteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEscalationRouteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _escalationRouteEntity, new PropertyChangedEventHandler( OnEscalationRouteEntityPropertyChanged ), "EscalationRouteEntity", Obymobi.Data.RelationClasses.StaticRouteRelations.RouteEntityUsingRouteIdEscalationRouteIdStatic, true, signalRelatedEntity, "EscalationFromRouteCollection", resetFKFields, new int[] { (int)RouteFieldIndex.EscalationRouteId } );		
			_escalationRouteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _escalationRouteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEscalationRouteEntity(IEntityCore relatedEntity)
		{
			if(_escalationRouteEntity!=relatedEntity)
			{		
				DesetupSyncEscalationRouteEntity(true, true);
				_escalationRouteEntity = (RouteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _escalationRouteEntity, new PropertyChangedEventHandler( OnEscalationRouteEntityPropertyChanged ), "EscalationRouteEntity", Obymobi.Data.RelationClasses.StaticRouteRelations.RouteEntityUsingRouteIdEscalationRouteIdStatic, true, ref _alreadyFetchedEscalationRouteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEscalationRouteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="routeId">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 routeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RouteFieldIndex.RouteId].ForcedCurrentValueWrite(routeId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRouteDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RouteEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RouteRelations Relations
		{
			get	{ return new RouteRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryCollection")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfigurationRoute' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationRouteCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationRouteCollection")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.ClientConfigurationRouteEntity, 0, null, null, null, "ClientConfigurationRouteCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyCollection")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection__
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection__")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection___
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection___")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection_
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection_")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductCollection")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEscalationFromRouteCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("EscalationFromRouteCollection")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "EscalationFromRouteCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Routestep' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutestepCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoutestepCollection(), (IEntityRelation)GetRelationsForField("RoutestepCollection")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.RoutestepEntity, 0, null, null, null, "RoutestepCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"), "AnnouncementCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup_"), "AnnouncementCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaCategory"), "CategoryCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaCategory"), "CompanyCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"), "CompanyCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup_"), "CompanyCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaProduct"), "CompanyCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaRoute
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.RouteEntityUsingEscalationRouteId;
				intermediateRelation.SetAliases(string.Empty, "Route_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaRoute"), "CompanyCollectionViaRoute", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyOwner'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyOwnerCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyOwnerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyOwnerEntity, 0, null, null, GetRelationsForField("CompanyOwnerCollectionViaCompany"), "CompanyOwnerCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Country'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCountryCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CountryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CountryEntity, 0, null, null, GetRelationsForField("CountryCollectionViaCompany"), "CountryCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, GetRelationsForField("CurrencyCollectionViaCompany"), "CurrencyCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, GetRelationsForField("GenericcategoryCollectionViaCategory"), "GenericcategoryCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaProduct"), "GenericproductCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, GetRelationsForField("LanguageCollectionViaCompany"), "LanguageCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaCategory"), "MenuCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaDeliverypointgroup"), "MenuCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Menu'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMenuCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MenuCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.MenuEntity, 0, null, null, GetRelationsForField("MenuCollectionViaDeliverypointgroup_"), "MenuCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Poscategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPoscategoryCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PoscategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.PoscategoryEntity, 0, null, null, GetRelationsForField("PoscategoryCollectionViaCategory"), "PoscategoryCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"), "PosdeliverypointgroupCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup_"), "PosdeliverypointgroupCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosproductCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.PosproductEntity, 0, null, null, GetRelationsForField("PosproductCollectionViaProduct"), "PosproductCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaCategory"), "ProductCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup"), "RouteCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"), "RouteCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaCompany"), "SupportpoolCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"), "TerminalCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaDeliverypointgroup_"), "TerminalCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"), "UIModeCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup___"), "UIModeCollectionViaDeliverypointgroup___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup____"), "UIModeCollectionViaDeliverypointgroup____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup_____
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_____"), "UIModeCollectionViaDeliverypointgroup_____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"), "UIModeCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingSystemMessageRouteId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"), "UIModeCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Vattariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVattariffCollectionViaProduct
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ProductEntityUsingRouteId;
				intermediateRelation.SetAliases(string.Empty, "Product_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VattariffCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.VattariffEntity, 0, null, null, GetRelationsForField("VattariffCollectionViaProduct"), "VattariffCollectionViaProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEscalationRouteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("EscalationRouteEntity")[0], (int)Obymobi.Data.EntityType.RouteEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, null, "EscalationRouteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RouteId property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."RouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RouteId
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.RouteId, true); }
			set	{ SetValue((int)RouteFieldIndex.RouteId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.CompanyId, true); }
			set	{ SetValue((int)RouteFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Name property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)RouteFieldIndex.Name, true); }
			set	{ SetValue((int)RouteFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)RouteFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)RouteFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The LogAlways property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."LogAlways"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LogAlways
		{
			get { return (System.Boolean)GetValue((int)RouteFieldIndex.LogAlways, true); }
			set	{ SetValue((int)RouteFieldIndex.LogAlways, value, true); }
		}

		/// <summary> The StepTimeoutMinutes property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."StepTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 StepTimeoutMinutes
		{
			get { return (System.Int32)GetValue((int)RouteFieldIndex.StepTimeoutMinutes, true); }
			set	{ SetValue((int)RouteFieldIndex.StepTimeoutMinutes, value, true); }
		}

		/// <summary> The EscalationRouteId property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."EscalationRouteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EscalationRouteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RouteFieldIndex.EscalationRouteId, false); }
			set	{ SetValue((int)RouteFieldIndex.EscalationRouteId, value, true); }
		}

		/// <summary> The BeingHandledSupportNotificationTimeoutMinutes property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."BeingHandledSupportNotificationTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BeingHandledSupportNotificationTimeoutMinutes
		{
			get { return (Nullable<System.Int32>)GetValue((int)RouteFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, false); }
			set	{ SetValue((int)RouteFieldIndex.BeingHandledSupportNotificationTimeoutMinutes, value, true); }
		}

		/// <summary> The RetrievalSupportNotificationTimeoutMinutes property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."RetrievalSupportNotificationTimeoutMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RetrievalSupportNotificationTimeoutMinutes
		{
			get { return (Nullable<System.Int32>)GetValue((int)RouteFieldIndex.RetrievalSupportNotificationTimeoutMinutes, false); }
			set	{ SetValue((int)RouteFieldIndex.RetrievalSupportNotificationTimeoutMinutes, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RouteFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RouteFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Route"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RouteFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RouteFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollection
		{
			get	{ return GetMultiCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollection. When set to true, CategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollection
		{
			get	{ return _alwaysFetchCategoryCollection; }
			set	{ _alwaysFetchCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollection already has been fetched. Setting this property to false when CategoryCollection has been fetched
		/// will clear the CategoryCollection collection well. Setting this property to true while CategoryCollection hasn't been fetched disables lazy loading for CategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollection
		{
			get { return _alreadyFetchedCategoryCollection;}
			set 
			{
				if(_alreadyFetchedCategoryCollection && !value && (_categoryCollection != null))
				{
					_categoryCollection.Clear();
				}
				_alreadyFetchedCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientConfigurationRouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientConfigurationRouteCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationRouteCollection ClientConfigurationRouteCollection
		{
			get	{ return GetMultiClientConfigurationRouteCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationRouteCollection. When set to true, ClientConfigurationRouteCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationRouteCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientConfigurationRouteCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationRouteCollection
		{
			get	{ return _alwaysFetchClientConfigurationRouteCollection; }
			set	{ _alwaysFetchClientConfigurationRouteCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationRouteCollection already has been fetched. Setting this property to false when ClientConfigurationRouteCollection has been fetched
		/// will clear the ClientConfigurationRouteCollection collection well. Setting this property to true while ClientConfigurationRouteCollection hasn't been fetched disables lazy loading for ClientConfigurationRouteCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationRouteCollection
		{
			get { return _alreadyFetchedClientConfigurationRouteCollection;}
			set 
			{
				if(_alreadyFetchedClientConfigurationRouteCollection && !value && (_clientConfigurationRouteCollection != null))
				{
					_clientConfigurationRouteCollection.Clear();
				}
				_alreadyFetchedClientConfigurationRouteCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollection
		{
			get	{ return GetMultiCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollection. When set to true, CompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollection
		{
			get	{ return _alwaysFetchCompanyCollection; }
			set	{ _alwaysFetchCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollection already has been fetched. Setting this property to false when CompanyCollection has been fetched
		/// will clear the CompanyCollection collection well. Setting this property to true while CompanyCollection hasn't been fetched disables lazy loading for CompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollection
		{
			get { return _alreadyFetchedCompanyCollection;}
			set 
			{
				if(_alreadyFetchedCompanyCollection && !value && (_companyCollection != null))
				{
					_companyCollection.Clear();
				}
				_alreadyFetchedCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection__
		{
			get	{ return GetMultiDeliverypointgroupCollection__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection__. When set to true, DeliverypointgroupCollection__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection__ is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection__
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection__; }
			set	{ _alwaysFetchDeliverypointgroupCollection__ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection__ already has been fetched. Setting this property to false when DeliverypointgroupCollection__ has been fetched
		/// will clear the DeliverypointgroupCollection__ collection well. Setting this property to true while DeliverypointgroupCollection__ hasn't been fetched disables lazy loading for DeliverypointgroupCollection__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection__
		{
			get { return _alreadyFetchedDeliverypointgroupCollection__;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection__ && !value && (_deliverypointgroupCollection__ != null))
				{
					_deliverypointgroupCollection__.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection__ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection___
		{
			get	{ return GetMultiDeliverypointgroupCollection___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection___. When set to true, DeliverypointgroupCollection___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection___ is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection___
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection___; }
			set	{ _alwaysFetchDeliverypointgroupCollection___ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection___ already has been fetched. Setting this property to false when DeliverypointgroupCollection___ has been fetched
		/// will clear the DeliverypointgroupCollection___ collection well. Setting this property to true while DeliverypointgroupCollection___ hasn't been fetched disables lazy loading for DeliverypointgroupCollection___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection___
		{
			get { return _alreadyFetchedDeliverypointgroupCollection___;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection___ && !value && (_deliverypointgroupCollection___ != null))
				{
					_deliverypointgroupCollection___.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection___ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection_
		{
			get	{ return GetMultiDeliverypointgroupCollection_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection_. When set to true, DeliverypointgroupCollection_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection_ is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection_
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection_; }
			set	{ _alwaysFetchDeliverypointgroupCollection_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection_ already has been fetched. Setting this property to false when DeliverypointgroupCollection_ has been fetched
		/// will clear the DeliverypointgroupCollection_ collection well. Setting this property to true while DeliverypointgroupCollection_ hasn't been fetched disables lazy loading for DeliverypointgroupCollection_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection_
		{
			get { return _alreadyFetchedDeliverypointgroupCollection_;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection_ && !value && (_deliverypointgroupCollection_ != null))
				{
					_deliverypointgroupCollection_.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollection
		{
			get	{ return GetMultiProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollection. When set to true, ProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollection
		{
			get	{ return _alwaysFetchProductCollection; }
			set	{ _alwaysFetchProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollection already has been fetched. Setting this property to false when ProductCollection has been fetched
		/// will clear the ProductCollection collection well. Setting this property to true while ProductCollection hasn't been fetched disables lazy loading for ProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollection
		{
			get { return _alreadyFetchedProductCollection;}
			set 
			{
				if(_alreadyFetchedProductCollection && !value && (_productCollection != null))
				{
					_productCollection.Clear();
				}
				_alreadyFetchedProductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEscalationFromRouteCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection EscalationFromRouteCollection
		{
			get	{ return GetMultiEscalationFromRouteCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EscalationFromRouteCollection. When set to true, EscalationFromRouteCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EscalationFromRouteCollection is accessed. You can always execute/ a forced fetch by calling GetMultiEscalationFromRouteCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEscalationFromRouteCollection
		{
			get	{ return _alwaysFetchEscalationFromRouteCollection; }
			set	{ _alwaysFetchEscalationFromRouteCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EscalationFromRouteCollection already has been fetched. Setting this property to false when EscalationFromRouteCollection has been fetched
		/// will clear the EscalationFromRouteCollection collection well. Setting this property to true while EscalationFromRouteCollection hasn't been fetched disables lazy loading for EscalationFromRouteCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEscalationFromRouteCollection
		{
			get { return _alreadyFetchedEscalationFromRouteCollection;}
			set 
			{
				if(_alreadyFetchedEscalationFromRouteCollection && !value && (_escalationFromRouteCollection != null))
				{
					_escalationFromRouteCollection.Clear();
				}
				_alreadyFetchedEscalationFromRouteCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoutestepEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutestepCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoutestepCollection RoutestepCollection
		{
			get	{ return GetMultiRoutestepCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutestepCollection. When set to true, RoutestepCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutestepCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoutestepCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutestepCollection
		{
			get	{ return _alwaysFetchRoutestepCollection; }
			set	{ _alwaysFetchRoutestepCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutestepCollection already has been fetched. Setting this property to false when RoutestepCollection has been fetched
		/// will clear the RoutestepCollection collection well. Setting this property to true while RoutestepCollection hasn't been fetched disables lazy loading for RoutestepCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutestepCollection
		{
			get { return _alreadyFetchedRoutestepCollection;}
			set 
			{
				if(_alreadyFetchedRoutestepCollection && !value && (_routestepCollection != null))
				{
					_routestepCollection.Clear();
				}
				_alreadyFetchedRoutestepCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollectionViaDeliverypointgroup
		{
			get { return GetMultiAnnouncementCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollectionViaDeliverypointgroup. When set to true, AnnouncementCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiAnnouncementCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchAnnouncementCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchAnnouncementCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when AnnouncementCollectionViaDeliverypointgroup has been fetched
		/// will clear the AnnouncementCollectionViaDeliverypointgroup collection well. Setting this property to true while AnnouncementCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for AnnouncementCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup && !value && (_announcementCollectionViaDeliverypointgroup != null))
				{
					_announcementCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollectionViaDeliverypointgroup_
		{
			get { return GetMultiAnnouncementCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollectionViaDeliverypointgroup_. When set to true, AnnouncementCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiAnnouncementCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchAnnouncementCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when AnnouncementCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the AnnouncementCollectionViaDeliverypointgroup_ collection well. Setting this property to true while AnnouncementCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for AnnouncementCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ && !value && (_announcementCollectionViaDeliverypointgroup_ != null))
				{
					_announcementCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaCategory
		{
			get { return GetMultiCategoryCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaCategory. When set to true, CategoryCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaCategory
		{
			get	{ return _alwaysFetchCategoryCollectionViaCategory; }
			set	{ _alwaysFetchCategoryCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaCategory already has been fetched. Setting this property to false when CategoryCollectionViaCategory has been fetched
		/// will clear the CategoryCollectionViaCategory collection well. Setting this property to true while CategoryCollectionViaCategory hasn't been fetched disables lazy loading for CategoryCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaCategory
		{
			get { return _alreadyFetchedCategoryCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaCategory && !value && (_categoryCollectionViaCategory != null))
				{
					_categoryCollectionViaCategory.Clear();
				}
				_alreadyFetchedCategoryCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaCategory
		{
			get { return GetMultiCompanyCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaCategory. When set to true, CompanyCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaCategory
		{
			get	{ return _alwaysFetchCompanyCollectionViaCategory; }
			set	{ _alwaysFetchCompanyCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaCategory already has been fetched. Setting this property to false when CompanyCollectionViaCategory has been fetched
		/// will clear the CompanyCollectionViaCategory collection well. Setting this property to true while CompanyCollectionViaCategory hasn't been fetched disables lazy loading for CompanyCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaCategory
		{
			get { return _alreadyFetchedCompanyCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaCategory && !value && (_companyCollectionViaCategory != null))
				{
					_companyCollectionViaCategory.Clear();
				}
				_alreadyFetchedCompanyCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup. When set to true, CompanyCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup && !value && (_companyCollectionViaDeliverypointgroup != null))
				{
					_companyCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup_
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup_. When set to true, CompanyCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup_ collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ && !value && (_companyCollectionViaDeliverypointgroup_ != null))
				{
					_companyCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaProduct
		{
			get { return GetMultiCompanyCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaProduct. When set to true, CompanyCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaProduct
		{
			get	{ return _alwaysFetchCompanyCollectionViaProduct; }
			set	{ _alwaysFetchCompanyCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaProduct already has been fetched. Setting this property to false when CompanyCollectionViaProduct has been fetched
		/// will clear the CompanyCollectionViaProduct collection well. Setting this property to true while CompanyCollectionViaProduct hasn't been fetched disables lazy loading for CompanyCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaProduct
		{
			get { return _alreadyFetchedCompanyCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaProduct && !value && (_companyCollectionViaProduct != null))
				{
					_companyCollectionViaProduct.Clear();
				}
				_alreadyFetchedCompanyCollectionViaProduct = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaRoute()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaRoute
		{
			get { return GetMultiCompanyCollectionViaRoute(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaRoute. When set to true, CompanyCollectionViaRoute is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaRoute is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaRoute(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaRoute
		{
			get	{ return _alwaysFetchCompanyCollectionViaRoute; }
			set	{ _alwaysFetchCompanyCollectionViaRoute = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaRoute already has been fetched. Setting this property to false when CompanyCollectionViaRoute has been fetched
		/// will clear the CompanyCollectionViaRoute collection well. Setting this property to true while CompanyCollectionViaRoute hasn't been fetched disables lazy loading for CompanyCollectionViaRoute</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaRoute
		{
			get { return _alreadyFetchedCompanyCollectionViaRoute;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaRoute && !value && (_companyCollectionViaRoute != null))
				{
					_companyCollectionViaRoute.Clear();
				}
				_alreadyFetchedCompanyCollectionViaRoute = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyOwnerCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyOwnerCollection CompanyOwnerCollectionViaCompany
		{
			get { return GetMultiCompanyOwnerCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyOwnerCollectionViaCompany. When set to true, CompanyOwnerCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyOwnerCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCompanyOwnerCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyOwnerCollectionViaCompany
		{
			get	{ return _alwaysFetchCompanyOwnerCollectionViaCompany; }
			set	{ _alwaysFetchCompanyOwnerCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyOwnerCollectionViaCompany already has been fetched. Setting this property to false when CompanyOwnerCollectionViaCompany has been fetched
		/// will clear the CompanyOwnerCollectionViaCompany collection well. Setting this property to true while CompanyOwnerCollectionViaCompany hasn't been fetched disables lazy loading for CompanyOwnerCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyOwnerCollectionViaCompany
		{
			get { return _alreadyFetchedCompanyOwnerCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCompanyOwnerCollectionViaCompany && !value && (_companyOwnerCollectionViaCompany != null))
				{
					_companyOwnerCollectionViaCompany.Clear();
				}
				_alreadyFetchedCompanyOwnerCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCountryCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CountryCollection CountryCollectionViaCompany
		{
			get { return GetMultiCountryCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CountryCollectionViaCompany. When set to true, CountryCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CountryCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCountryCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCountryCollectionViaCompany
		{
			get	{ return _alwaysFetchCountryCollectionViaCompany; }
			set	{ _alwaysFetchCountryCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CountryCollectionViaCompany already has been fetched. Setting this property to false when CountryCollectionViaCompany has been fetched
		/// will clear the CountryCollectionViaCompany collection well. Setting this property to true while CountryCollectionViaCompany hasn't been fetched disables lazy loading for CountryCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCountryCollectionViaCompany
		{
			get { return _alreadyFetchedCountryCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCountryCollectionViaCompany && !value && (_countryCollectionViaCompany != null))
				{
					_countryCollectionViaCompany.Clear();
				}
				_alreadyFetchedCountryCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCurrencyCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CurrencyCollection CurrencyCollectionViaCompany
		{
			get { return GetMultiCurrencyCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyCollectionViaCompany. When set to true, CurrencyCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCurrencyCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyCollectionViaCompany
		{
			get	{ return _alwaysFetchCurrencyCollectionViaCompany; }
			set	{ _alwaysFetchCurrencyCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyCollectionViaCompany already has been fetched. Setting this property to false when CurrencyCollectionViaCompany has been fetched
		/// will clear the CurrencyCollectionViaCompany collection well. Setting this property to true while CurrencyCollectionViaCompany hasn't been fetched disables lazy loading for CurrencyCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyCollectionViaCompany
		{
			get { return _alreadyFetchedCurrencyCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCurrencyCollectionViaCompany && !value && (_currencyCollectionViaCompany != null))
				{
					_currencyCollectionViaCompany.Clear();
				}
				_alreadyFetchedCurrencyCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollectionViaCategory
		{
			get { return GetMultiGenericcategoryCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollectionViaCategory. When set to true, GenericcategoryCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiGenericcategoryCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollectionViaCategory
		{
			get	{ return _alwaysFetchGenericcategoryCollectionViaCategory; }
			set	{ _alwaysFetchGenericcategoryCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollectionViaCategory already has been fetched. Setting this property to false when GenericcategoryCollectionViaCategory has been fetched
		/// will clear the GenericcategoryCollectionViaCategory collection well. Setting this property to true while GenericcategoryCollectionViaCategory hasn't been fetched disables lazy loading for GenericcategoryCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollectionViaCategory
		{
			get { return _alreadyFetchedGenericcategoryCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollectionViaCategory && !value && (_genericcategoryCollectionViaCategory != null))
				{
					_genericcategoryCollectionViaCategory.Clear();
				}
				_alreadyFetchedGenericcategoryCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaProduct
		{
			get { return GetMultiGenericproductCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaProduct. When set to true, GenericproductCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaProduct
		{
			get	{ return _alwaysFetchGenericproductCollectionViaProduct; }
			set	{ _alwaysFetchGenericproductCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaProduct already has been fetched. Setting this property to false when GenericproductCollectionViaProduct has been fetched
		/// will clear the GenericproductCollectionViaProduct collection well. Setting this property to true while GenericproductCollectionViaProduct hasn't been fetched disables lazy loading for GenericproductCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaProduct
		{
			get { return _alreadyFetchedGenericproductCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaProduct && !value && (_genericproductCollectionViaProduct != null))
				{
					_genericproductCollectionViaProduct.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaProduct = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLanguageCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LanguageCollection LanguageCollectionViaCompany
		{
			get { return GetMultiLanguageCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageCollectionViaCompany. When set to true, LanguageCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiLanguageCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageCollectionViaCompany
		{
			get	{ return _alwaysFetchLanguageCollectionViaCompany; }
			set	{ _alwaysFetchLanguageCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageCollectionViaCompany already has been fetched. Setting this property to false when LanguageCollectionViaCompany has been fetched
		/// will clear the LanguageCollectionViaCompany collection well. Setting this property to true while LanguageCollectionViaCompany hasn't been fetched disables lazy loading for LanguageCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageCollectionViaCompany
		{
			get { return _alreadyFetchedLanguageCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedLanguageCollectionViaCompany && !value && (_languageCollectionViaCompany != null))
				{
					_languageCollectionViaCompany.Clear();
				}
				_alreadyFetchedLanguageCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaCategory
		{
			get { return GetMultiMenuCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaCategory. When set to true, MenuCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaCategory
		{
			get	{ return _alwaysFetchMenuCollectionViaCategory; }
			set	{ _alwaysFetchMenuCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaCategory already has been fetched. Setting this property to false when MenuCollectionViaCategory has been fetched
		/// will clear the MenuCollectionViaCategory collection well. Setting this property to true while MenuCollectionViaCategory hasn't been fetched disables lazy loading for MenuCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaCategory
		{
			get { return _alreadyFetchedMenuCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaCategory && !value && (_menuCollectionViaCategory != null))
				{
					_menuCollectionViaCategory.Clear();
				}
				_alreadyFetchedMenuCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaDeliverypointgroup
		{
			get { return GetMultiMenuCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaDeliverypointgroup. When set to true, MenuCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchMenuCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchMenuCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when MenuCollectionViaDeliverypointgroup has been fetched
		/// will clear the MenuCollectionViaDeliverypointgroup collection well. Setting this property to true while MenuCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for MenuCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedMenuCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaDeliverypointgroup && !value && (_menuCollectionViaDeliverypointgroup != null))
				{
					_menuCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedMenuCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'MenuEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMenuCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MenuCollection MenuCollectionViaDeliverypointgroup_
		{
			get { return GetMultiMenuCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MenuCollectionViaDeliverypointgroup_. When set to true, MenuCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MenuCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiMenuCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMenuCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchMenuCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchMenuCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MenuCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when MenuCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the MenuCollectionViaDeliverypointgroup_ collection well. Setting this property to true while MenuCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for MenuCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMenuCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedMenuCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedMenuCollectionViaDeliverypointgroup_ && !value && (_menuCollectionViaDeliverypointgroup_ != null))
				{
					_menuCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedMenuCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PoscategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPoscategoryCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PoscategoryCollection PoscategoryCollectionViaCategory
		{
			get { return GetMultiPoscategoryCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PoscategoryCollectionViaCategory. When set to true, PoscategoryCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PoscategoryCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiPoscategoryCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPoscategoryCollectionViaCategory
		{
			get	{ return _alwaysFetchPoscategoryCollectionViaCategory; }
			set	{ _alwaysFetchPoscategoryCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PoscategoryCollectionViaCategory already has been fetched. Setting this property to false when PoscategoryCollectionViaCategory has been fetched
		/// will clear the PoscategoryCollectionViaCategory collection well. Setting this property to true while PoscategoryCollectionViaCategory hasn't been fetched disables lazy loading for PoscategoryCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPoscategoryCollectionViaCategory
		{
			get { return _alreadyFetchedPoscategoryCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedPoscategoryCollectionViaCategory && !value && (_poscategoryCollectionViaCategory != null))
				{
					_poscategoryCollectionViaCategory.Clear();
				}
				_alreadyFetchedPoscategoryCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup_. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup_ collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup_ != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosproductCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosproductCollection PosproductCollectionViaProduct
		{
			get { return GetMultiPosproductCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosproductCollectionViaProduct. When set to true, PosproductCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosproductCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiPosproductCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosproductCollectionViaProduct
		{
			get	{ return _alwaysFetchPosproductCollectionViaProduct; }
			set	{ _alwaysFetchPosproductCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosproductCollectionViaProduct already has been fetched. Setting this property to false when PosproductCollectionViaProduct has been fetched
		/// will clear the PosproductCollectionViaProduct collection well. Setting this property to true while PosproductCollectionViaProduct hasn't been fetched disables lazy loading for PosproductCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosproductCollectionViaProduct
		{
			get { return _alreadyFetchedPosproductCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedPosproductCollectionViaProduct && !value && (_posproductCollectionViaProduct != null))
				{
					_posproductCollectionViaProduct.Clear();
				}
				_alreadyFetchedPosproductCollectionViaProduct = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaCategory
		{
			get { return GetMultiProductCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaCategory. When set to true, ProductCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaCategory
		{
			get	{ return _alwaysFetchProductCollectionViaCategory; }
			set	{ _alwaysFetchProductCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaCategory already has been fetched. Setting this property to false when ProductCollectionViaCategory has been fetched
		/// will clear the ProductCollectionViaCategory collection well. Setting this property to true while ProductCollectionViaCategory hasn't been fetched disables lazy loading for ProductCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaCategory
		{
			get { return _alreadyFetchedProductCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaCategory && !value && (_productCollectionViaCategory != null))
				{
					_productCollectionViaCategory.Clear();
				}
				_alreadyFetchedProductCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup. When set to true, RouteCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup && !value && (_routeCollectionViaDeliverypointgroup != null))
				{
					_routeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup_
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup_. When set to true, RouteCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup_ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup_ && !value && (_routeCollectionViaDeliverypointgroup_ != null))
				{
					_routeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaCompany
		{
			get { return GetMultiSupportpoolCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaCompany. When set to true, SupportpoolCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaCompany
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaCompany; }
			set	{ _alwaysFetchSupportpoolCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaCompany already has been fetched. Setting this property to false when SupportpoolCollectionViaCompany has been fetched
		/// will clear the SupportpoolCollectionViaCompany collection well. Setting this property to true while SupportpoolCollectionViaCompany hasn't been fetched disables lazy loading for SupportpoolCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaCompany
		{
			get { return _alreadyFetchedSupportpoolCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaCompany && !value && (_supportpoolCollectionViaCompany != null))
				{
					_supportpoolCollectionViaCompany.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaDeliverypointgroup
		{
			get { return GetMultiTerminalCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaDeliverypointgroup. When set to true, TerminalCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchTerminalCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchTerminalCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when TerminalCollectionViaDeliverypointgroup has been fetched
		/// will clear the TerminalCollectionViaDeliverypointgroup collection well. Setting this property to true while TerminalCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for TerminalCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedTerminalCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaDeliverypointgroup && !value && (_terminalCollectionViaDeliverypointgroup != null))
				{
					_terminalCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaDeliverypointgroup_
		{
			get { return GetMultiTerminalCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaDeliverypointgroup_. When set to true, TerminalCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchTerminalCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchTerminalCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when TerminalCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the TerminalCollectionViaDeliverypointgroup_ collection well. Setting this property to true while TerminalCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for TerminalCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedTerminalCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ && !value && (_terminalCollectionViaDeliverypointgroup_ != null))
				{
					_terminalCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup__
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup__. When set to true, UIModeCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup__ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ && !value && (_uIModeCollectionViaDeliverypointgroup__ != null))
				{
					_uIModeCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup___
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup___. When set to true, UIModeCollectionViaDeliverypointgroup___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup___ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup___
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup___; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup___ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup___ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup___ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup___ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup___
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup___;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ && !value && (_uIModeCollectionViaDeliverypointgroup___ != null))
				{
					_uIModeCollectionViaDeliverypointgroup___.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup____
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup____. When set to true, UIModeCollectionViaDeliverypointgroup____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup____ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup____
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup____; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup____ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup____ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup____ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup____ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup____
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup____;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ && !value && (_uIModeCollectionViaDeliverypointgroup____ != null))
				{
					_uIModeCollectionViaDeliverypointgroup____.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup_____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup_____
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup_____(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup_____. When set to true, UIModeCollectionViaDeliverypointgroup_____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup_____ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup_____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup_____
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup_____; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup_____ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup_____ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup_____ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup_____ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup_____ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup_____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup_____
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup_____;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ && !value && (_uIModeCollectionViaDeliverypointgroup_____ != null))
				{
					_uIModeCollectionViaDeliverypointgroup_____.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_____ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup. When set to true, UIModeCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup && !value && (_uIModeCollectionViaDeliverypointgroup != null))
				{
					_uIModeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup_
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup_. When set to true, UIModeCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup_ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ && !value && (_uIModeCollectionViaDeliverypointgroup_ != null))
				{
					_uIModeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVattariffCollectionViaProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.VattariffCollection VattariffCollectionViaProduct
		{
			get { return GetMultiVattariffCollectionViaProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VattariffCollectionViaProduct. When set to true, VattariffCollectionViaProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VattariffCollectionViaProduct is accessed. You can always execute a forced fetch by calling GetMultiVattariffCollectionViaProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVattariffCollectionViaProduct
		{
			get	{ return _alwaysFetchVattariffCollectionViaProduct; }
			set	{ _alwaysFetchVattariffCollectionViaProduct = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VattariffCollectionViaProduct already has been fetched. Setting this property to false when VattariffCollectionViaProduct has been fetched
		/// will clear the VattariffCollectionViaProduct collection well. Setting this property to true while VattariffCollectionViaProduct hasn't been fetched disables lazy loading for VattariffCollectionViaProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVattariffCollectionViaProduct
		{
			get { return _alreadyFetchedVattariffCollectionViaProduct;}
			set 
			{
				if(_alreadyFetchedVattariffCollectionViaProduct && !value && (_vattariffCollectionViaProduct != null))
				{
					_vattariffCollectionViaProduct.Clear();
				}
				_alreadyFetchedVattariffCollectionViaProduct = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RouteCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEscalationRouteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RouteEntity EscalationRouteEntity
		{
			get	{ return GetSingleEscalationRouteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEscalationRouteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EscalationFromRouteCollection", "EscalationRouteEntity", _escalationRouteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EscalationRouteEntity. When set to true, EscalationRouteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EscalationRouteEntity is accessed. You can always execute a forced fetch by calling GetSingleEscalationRouteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEscalationRouteEntity
		{
			get	{ return _alwaysFetchEscalationRouteEntity; }
			set	{ _alwaysFetchEscalationRouteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EscalationRouteEntity already has been fetched. Setting this property to false when EscalationRouteEntity has been fetched
		/// will set EscalationRouteEntity to null as well. Setting this property to true while EscalationRouteEntity hasn't been fetched disables lazy loading for EscalationRouteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEscalationRouteEntity
		{
			get { return _alreadyFetchedEscalationRouteEntity;}
			set 
			{
				if(_alreadyFetchedEscalationRouteEntity && !value)
				{
					this.EscalationRouteEntity = null;
				}
				_alreadyFetchedEscalationRouteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EscalationRouteEntity is not found
		/// in the database. When set to true, EscalationRouteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EscalationRouteEntityReturnsNewIfNotFound
		{
			get	{ return _escalationRouteEntityReturnsNewIfNotFound; }
			set { _escalationRouteEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RouteEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
