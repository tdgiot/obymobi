﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Alteration'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AlterationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AlterationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AlterationCollection	_alterationCollection;
		private bool	_alwaysFetchAlterationCollection, _alreadyFetchedAlterationCollection;
		private Obymobi.Data.CollectionClasses.AlterationitemCollection	_alterationitemCollection;
		private bool	_alwaysFetchAlterationitemCollection, _alreadyFetchedAlterationitemCollection;
		private Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection	_alterationitemAlterationCollection;
		private bool	_alwaysFetchAlterationitemAlterationCollection, _alreadyFetchedAlterationitemAlterationCollection;
		private Obymobi.Data.CollectionClasses.AlterationLanguageCollection	_alterationLanguageCollection;
		private bool	_alwaysFetchAlterationLanguageCollection, _alreadyFetchedAlterationLanguageCollection;
		private Obymobi.Data.CollectionClasses.AlterationProductCollection	_alterationProductCollection;
		private bool	_alwaysFetchAlterationProductCollection, _alreadyFetchedAlterationProductCollection;
		private Obymobi.Data.CollectionClasses.CategoryAlterationCollection	_categoryAlterationCollection;
		private bool	_alwaysFetchCategoryAlterationCollection, _alreadyFetchedCategoryAlterationCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection	_orderitemAlterationitemCollection;
		private bool	_alwaysFetchOrderitemAlterationitemCollection, _alreadyFetchedOrderitemAlterationitemCollection;
		private Obymobi.Data.CollectionClasses.PriceLevelItemCollection	_priceLevelItemCollection;
		private bool	_alwaysFetchPriceLevelItemCollection, _alreadyFetchedPriceLevelItemCollection;
		private Obymobi.Data.CollectionClasses.ProductAlterationCollection	_productAlterationCollection;
		private bool	_alwaysFetchProductAlterationCollection, _alreadyFetchedProductAlterationCollection;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;

		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection _pointOfInterestCollectionViaMedium;
		private bool	_alwaysFetchPointOfInterestCollectionViaMedium, _alreadyFetchedPointOfInterestCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;
		private AlterationEntity _parentAlterationEntity;
		private bool	_alwaysFetchParentAlterationEntity, _alreadyFetchedParentAlterationEntity, _parentAlterationEntityReturnsNewIfNotFound;
		private BrandEntity _brandEntity;
		private bool	_alwaysFetchBrandEntity, _alreadyFetchedBrandEntity, _brandEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private ExternalProductEntity _externalProductEntity;
		private bool	_alwaysFetchExternalProductEntity, _alreadyFetchedExternalProductEntity, _externalProductEntityReturnsNewIfNotFound;
		private GenericalterationEntity _genericalterationEntity;
		private bool	_alwaysFetchGenericalterationEntity, _alreadyFetchedGenericalterationEntity, _genericalterationEntityReturnsNewIfNotFound;
		private PosalterationEntity _posAlterationEntity;
		private bool	_alwaysFetchPosAlterationEntity, _alreadyFetchedPosAlterationEntity, _posAlterationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentAlterationEntity</summary>
			public static readonly string ParentAlterationEntity = "ParentAlterationEntity";
			/// <summary>Member name BrandEntity</summary>
			public static readonly string BrandEntity = "BrandEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name ExternalProductEntity</summary>
			public static readonly string ExternalProductEntity = "ExternalProductEntity";
			/// <summary>Member name GenericalterationEntity</summary>
			public static readonly string GenericalterationEntity = "GenericalterationEntity";
			/// <summary>Member name PosAlterationEntity</summary>
			public static readonly string PosAlterationEntity = "PosAlterationEntity";
			/// <summary>Member name AlterationCollection</summary>
			public static readonly string AlterationCollection = "AlterationCollection";
			/// <summary>Member name AlterationitemCollection</summary>
			public static readonly string AlterationitemCollection = "AlterationitemCollection";
			/// <summary>Member name AlterationitemAlterationCollection</summary>
			public static readonly string AlterationitemAlterationCollection = "AlterationitemAlterationCollection";
			/// <summary>Member name AlterationLanguageCollection</summary>
			public static readonly string AlterationLanguageCollection = "AlterationLanguageCollection";
			/// <summary>Member name AlterationProductCollection</summary>
			public static readonly string AlterationProductCollection = "AlterationProductCollection";
			/// <summary>Member name CategoryAlterationCollection</summary>
			public static readonly string CategoryAlterationCollection = "CategoryAlterationCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
			/// <summary>Member name PriceLevelItemCollection</summary>
			public static readonly string PriceLevelItemCollection = "PriceLevelItemCollection";
			/// <summary>Member name ProductAlterationCollection</summary>
			public static readonly string ProductAlterationCollection = "ProductAlterationCollection";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";

			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name PointOfInterestCollectionViaMedium</summary>
			public static readonly string PointOfInterestCollectionViaMedium = "PointOfInterestCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AlterationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AlterationEntityBase() :base("AlterationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		protected AlterationEntityBase(System.Int32 alterationId):base("AlterationEntity")
		{
			InitClassFetch(alterationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AlterationEntityBase(System.Int32 alterationId, IPrefetchPath prefetchPathToUse): base("AlterationEntity")
		{
			InitClassFetch(alterationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="validator">The custom validator object for this AlterationEntity</param>
		protected AlterationEntityBase(System.Int32 alterationId, IValidator validator):base("AlterationEntity")
		{
			InitClassFetch(alterationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationCollection = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollection", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollection = info.GetBoolean("_alwaysFetchAlterationCollection");
			_alreadyFetchedAlterationCollection = info.GetBoolean("_alreadyFetchedAlterationCollection");

			_alterationitemCollection = (Obymobi.Data.CollectionClasses.AlterationitemCollection)info.GetValue("_alterationitemCollection", typeof(Obymobi.Data.CollectionClasses.AlterationitemCollection));
			_alwaysFetchAlterationitemCollection = info.GetBoolean("_alwaysFetchAlterationitemCollection");
			_alreadyFetchedAlterationitemCollection = info.GetBoolean("_alreadyFetchedAlterationitemCollection");

			_alterationitemAlterationCollection = (Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection)info.GetValue("_alterationitemAlterationCollection", typeof(Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection));
			_alwaysFetchAlterationitemAlterationCollection = info.GetBoolean("_alwaysFetchAlterationitemAlterationCollection");
			_alreadyFetchedAlterationitemAlterationCollection = info.GetBoolean("_alreadyFetchedAlterationitemAlterationCollection");

			_alterationLanguageCollection = (Obymobi.Data.CollectionClasses.AlterationLanguageCollection)info.GetValue("_alterationLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AlterationLanguageCollection));
			_alwaysFetchAlterationLanguageCollection = info.GetBoolean("_alwaysFetchAlterationLanguageCollection");
			_alreadyFetchedAlterationLanguageCollection = info.GetBoolean("_alreadyFetchedAlterationLanguageCollection");

			_alterationProductCollection = (Obymobi.Data.CollectionClasses.AlterationProductCollection)info.GetValue("_alterationProductCollection", typeof(Obymobi.Data.CollectionClasses.AlterationProductCollection));
			_alwaysFetchAlterationProductCollection = info.GetBoolean("_alwaysFetchAlterationProductCollection");
			_alreadyFetchedAlterationProductCollection = info.GetBoolean("_alreadyFetchedAlterationProductCollection");

			_categoryAlterationCollection = (Obymobi.Data.CollectionClasses.CategoryAlterationCollection)info.GetValue("_categoryAlterationCollection", typeof(Obymobi.Data.CollectionClasses.CategoryAlterationCollection));
			_alwaysFetchCategoryAlterationCollection = info.GetBoolean("_alwaysFetchCategoryAlterationCollection");
			_alreadyFetchedCategoryAlterationCollection = info.GetBoolean("_alreadyFetchedCategoryAlterationCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_orderitemAlterationitemCollection = (Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection)info.GetValue("_orderitemAlterationitemCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection));
			_alwaysFetchOrderitemAlterationitemCollection = info.GetBoolean("_alwaysFetchOrderitemAlterationitemCollection");
			_alreadyFetchedOrderitemAlterationitemCollection = info.GetBoolean("_alreadyFetchedOrderitemAlterationitemCollection");

			_priceLevelItemCollection = (Obymobi.Data.CollectionClasses.PriceLevelItemCollection)info.GetValue("_priceLevelItemCollection", typeof(Obymobi.Data.CollectionClasses.PriceLevelItemCollection));
			_alwaysFetchPriceLevelItemCollection = info.GetBoolean("_alwaysFetchPriceLevelItemCollection");
			_alreadyFetchedPriceLevelItemCollection = info.GetBoolean("_alreadyFetchedPriceLevelItemCollection");

			_productAlterationCollection = (Obymobi.Data.CollectionClasses.ProductAlterationCollection)info.GetValue("_productAlterationCollection", typeof(Obymobi.Data.CollectionClasses.ProductAlterationCollection));
			_alwaysFetchProductAlterationCollection = info.GetBoolean("_alwaysFetchProductAlterationCollection");
			_alreadyFetchedProductAlterationCollection = info.GetBoolean("_alreadyFetchedProductAlterationCollection");
			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");


			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_pointOfInterestCollectionViaMedium = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollectionViaMedium = info.GetBoolean("_alwaysFetchPointOfInterestCollectionViaMedium");
			_alreadyFetchedPointOfInterestCollectionViaMedium = info.GetBoolean("_alreadyFetchedPointOfInterestCollectionViaMedium");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");
			_parentAlterationEntity = (AlterationEntity)info.GetValue("_parentAlterationEntity", typeof(AlterationEntity));
			if(_parentAlterationEntity!=null)
			{
				_parentAlterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentAlterationEntityReturnsNewIfNotFound = info.GetBoolean("_parentAlterationEntityReturnsNewIfNotFound");
			_alwaysFetchParentAlterationEntity = info.GetBoolean("_alwaysFetchParentAlterationEntity");
			_alreadyFetchedParentAlterationEntity = info.GetBoolean("_alreadyFetchedParentAlterationEntity");

			_brandEntity = (BrandEntity)info.GetValue("_brandEntity", typeof(BrandEntity));
			if(_brandEntity!=null)
			{
				_brandEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_brandEntityReturnsNewIfNotFound = info.GetBoolean("_brandEntityReturnsNewIfNotFound");
			_alwaysFetchBrandEntity = info.GetBoolean("_alwaysFetchBrandEntity");
			_alreadyFetchedBrandEntity = info.GetBoolean("_alreadyFetchedBrandEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_externalProductEntity = (ExternalProductEntity)info.GetValue("_externalProductEntity", typeof(ExternalProductEntity));
			if(_externalProductEntity!=null)
			{
				_externalProductEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalProductEntityReturnsNewIfNotFound = info.GetBoolean("_externalProductEntityReturnsNewIfNotFound");
			_alwaysFetchExternalProductEntity = info.GetBoolean("_alwaysFetchExternalProductEntity");
			_alreadyFetchedExternalProductEntity = info.GetBoolean("_alreadyFetchedExternalProductEntity");

			_genericalterationEntity = (GenericalterationEntity)info.GetValue("_genericalterationEntity", typeof(GenericalterationEntity));
			if(_genericalterationEntity!=null)
			{
				_genericalterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_genericalterationEntityReturnsNewIfNotFound = info.GetBoolean("_genericalterationEntityReturnsNewIfNotFound");
			_alwaysFetchGenericalterationEntity = info.GetBoolean("_alwaysFetchGenericalterationEntity");
			_alreadyFetchedGenericalterationEntity = info.GetBoolean("_alreadyFetchedGenericalterationEntity");

			_posAlterationEntity = (PosalterationEntity)info.GetValue("_posAlterationEntity", typeof(PosalterationEntity));
			if(_posAlterationEntity!=null)
			{
				_posAlterationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_posAlterationEntityReturnsNewIfNotFound = info.GetBoolean("_posAlterationEntityReturnsNewIfNotFound");
			_alwaysFetchPosAlterationEntity = info.GetBoolean("_alwaysFetchPosAlterationEntity");
			_alreadyFetchedPosAlterationEntity = info.GetBoolean("_alreadyFetchedPosAlterationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AlterationFieldIndex)fieldIndex)
			{
				case AlterationFieldIndex.PosalterationId:
					DesetupSyncPosAlterationEntity(true, false);
					_alreadyFetchedPosAlterationEntity = false;
					break;
				case AlterationFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case AlterationFieldIndex.GenericalterationId:
					DesetupSyncGenericalterationEntity(true, false);
					_alreadyFetchedGenericalterationEntity = false;
					break;
				case AlterationFieldIndex.ParentAlterationId:
					DesetupSyncParentAlterationEntity(true, false);
					_alreadyFetchedParentAlterationEntity = false;
					break;
				case AlterationFieldIndex.BrandId:
					DesetupSyncBrandEntity(true, false);
					_alreadyFetchedBrandEntity = false;
					break;
				case AlterationFieldIndex.ExternalProductId:
					DesetupSyncExternalProductEntity(true, false);
					_alreadyFetchedExternalProductEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationCollection = (_alterationCollection.Count > 0);
			_alreadyFetchedAlterationitemCollection = (_alterationitemCollection.Count > 0);
			_alreadyFetchedAlterationitemAlterationCollection = (_alterationitemAlterationCollection.Count > 0);
			_alreadyFetchedAlterationLanguageCollection = (_alterationLanguageCollection.Count > 0);
			_alreadyFetchedAlterationProductCollection = (_alterationProductCollection.Count > 0);
			_alreadyFetchedCategoryAlterationCollection = (_categoryAlterationCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedOrderitemAlterationitemCollection = (_orderitemAlterationitemCollection.Count > 0);
			_alreadyFetchedPriceLevelItemCollection = (_priceLevelItemCollection.Count > 0);
			_alreadyFetchedProductAlterationCollection = (_productAlterationCollection.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedPointOfInterestCollectionViaMedium = (_pointOfInterestCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
			_alreadyFetchedParentAlterationEntity = (_parentAlterationEntity != null);
			_alreadyFetchedBrandEntity = (_brandEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedExternalProductEntity = (_externalProductEntity != null);
			_alreadyFetchedGenericalterationEntity = (_genericalterationEntity != null);
			_alreadyFetchedPosAlterationEntity = (_posAlterationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParentAlterationEntity":
					toReturn.Add(Relations.AlterationEntityUsingAlterationIdParentAlterationId);
					break;
				case "BrandEntity":
					toReturn.Add(Relations.BrandEntityUsingBrandId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "ExternalProductEntity":
					toReturn.Add(Relations.ExternalProductEntityUsingExternalProductId);
					break;
				case "GenericalterationEntity":
					toReturn.Add(Relations.GenericalterationEntityUsingGenericalterationId);
					break;
				case "PosAlterationEntity":
					toReturn.Add(Relations.PosalterationEntityUsingPosalterationId);
					break;
				case "AlterationCollection":
					toReturn.Add(Relations.AlterationEntityUsingParentAlterationId);
					break;
				case "AlterationitemCollection":
					toReturn.Add(Relations.AlterationitemEntityUsingAlterationId);
					break;
				case "AlterationitemAlterationCollection":
					toReturn.Add(Relations.AlterationitemAlterationEntityUsingAlterationId);
					break;
				case "AlterationLanguageCollection":
					toReturn.Add(Relations.AlterationLanguageEntityUsingAlterationId);
					break;
				case "AlterationProductCollection":
					toReturn.Add(Relations.AlterationProductEntityUsingAlterationId);
					break;
				case "CategoryAlterationCollection":
					toReturn.Add(Relations.CategoryAlterationEntityUsingAlterationId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingAlterationId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingAlterationId);
					break;
				case "OrderitemAlterationitemCollection":
					toReturn.Add(Relations.OrderitemAlterationitemEntityUsingAlterationId);
					break;
				case "PriceLevelItemCollection":
					toReturn.Add(Relations.PriceLevelItemEntityUsingAlterationId);
					break;
				case "ProductAlterationCollection":
					toReturn.Add(Relations.ProductAlterationEntityUsingAlterationId);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationId, "AlterationEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationId, "AlterationEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationId, "AlterationEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationId, "AlterationEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "PointOfInterestCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationId, "AlterationEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.PointOfInterestEntityUsingPointOfInterestId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationId, "AlterationEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingAlterationId, "AlterationEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationCollection", (!this.MarkedForDeletion?_alterationCollection:null));
			info.AddValue("_alwaysFetchAlterationCollection", _alwaysFetchAlterationCollection);
			info.AddValue("_alreadyFetchedAlterationCollection", _alreadyFetchedAlterationCollection);
			info.AddValue("_alterationitemCollection", (!this.MarkedForDeletion?_alterationitemCollection:null));
			info.AddValue("_alwaysFetchAlterationitemCollection", _alwaysFetchAlterationitemCollection);
			info.AddValue("_alreadyFetchedAlterationitemCollection", _alreadyFetchedAlterationitemCollection);
			info.AddValue("_alterationitemAlterationCollection", (!this.MarkedForDeletion?_alterationitemAlterationCollection:null));
			info.AddValue("_alwaysFetchAlterationitemAlterationCollection", _alwaysFetchAlterationitemAlterationCollection);
			info.AddValue("_alreadyFetchedAlterationitemAlterationCollection", _alreadyFetchedAlterationitemAlterationCollection);
			info.AddValue("_alterationLanguageCollection", (!this.MarkedForDeletion?_alterationLanguageCollection:null));
			info.AddValue("_alwaysFetchAlterationLanguageCollection", _alwaysFetchAlterationLanguageCollection);
			info.AddValue("_alreadyFetchedAlterationLanguageCollection", _alreadyFetchedAlterationLanguageCollection);
			info.AddValue("_alterationProductCollection", (!this.MarkedForDeletion?_alterationProductCollection:null));
			info.AddValue("_alwaysFetchAlterationProductCollection", _alwaysFetchAlterationProductCollection);
			info.AddValue("_alreadyFetchedAlterationProductCollection", _alreadyFetchedAlterationProductCollection);
			info.AddValue("_categoryAlterationCollection", (!this.MarkedForDeletion?_categoryAlterationCollection:null));
			info.AddValue("_alwaysFetchCategoryAlterationCollection", _alwaysFetchCategoryAlterationCollection);
			info.AddValue("_alreadyFetchedCategoryAlterationCollection", _alreadyFetchedCategoryAlterationCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_orderitemAlterationitemCollection", (!this.MarkedForDeletion?_orderitemAlterationitemCollection:null));
			info.AddValue("_alwaysFetchOrderitemAlterationitemCollection", _alwaysFetchOrderitemAlterationitemCollection);
			info.AddValue("_alreadyFetchedOrderitemAlterationitemCollection", _alreadyFetchedOrderitemAlterationitemCollection);
			info.AddValue("_priceLevelItemCollection", (!this.MarkedForDeletion?_priceLevelItemCollection:null));
			info.AddValue("_alwaysFetchPriceLevelItemCollection", _alwaysFetchPriceLevelItemCollection);
			info.AddValue("_alreadyFetchedPriceLevelItemCollection", _alreadyFetchedPriceLevelItemCollection);
			info.AddValue("_productAlterationCollection", (!this.MarkedForDeletion?_productAlterationCollection:null));
			info.AddValue("_alwaysFetchProductAlterationCollection", _alwaysFetchProductAlterationCollection);
			info.AddValue("_alreadyFetchedProductAlterationCollection", _alreadyFetchedProductAlterationCollection);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_pointOfInterestCollectionViaMedium", (!this.MarkedForDeletion?_pointOfInterestCollectionViaMedium:null));
			info.AddValue("_alwaysFetchPointOfInterestCollectionViaMedium", _alwaysFetchPointOfInterestCollectionViaMedium);
			info.AddValue("_alreadyFetchedPointOfInterestCollectionViaMedium", _alreadyFetchedPointOfInterestCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);
			info.AddValue("_parentAlterationEntity", (!this.MarkedForDeletion?_parentAlterationEntity:null));
			info.AddValue("_parentAlterationEntityReturnsNewIfNotFound", _parentAlterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentAlterationEntity", _alwaysFetchParentAlterationEntity);
			info.AddValue("_alreadyFetchedParentAlterationEntity", _alreadyFetchedParentAlterationEntity);
			info.AddValue("_brandEntity", (!this.MarkedForDeletion?_brandEntity:null));
			info.AddValue("_brandEntityReturnsNewIfNotFound", _brandEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBrandEntity", _alwaysFetchBrandEntity);
			info.AddValue("_alreadyFetchedBrandEntity", _alreadyFetchedBrandEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_externalProductEntity", (!this.MarkedForDeletion?_externalProductEntity:null));
			info.AddValue("_externalProductEntityReturnsNewIfNotFound", _externalProductEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalProductEntity", _alwaysFetchExternalProductEntity);
			info.AddValue("_alreadyFetchedExternalProductEntity", _alreadyFetchedExternalProductEntity);
			info.AddValue("_genericalterationEntity", (!this.MarkedForDeletion?_genericalterationEntity:null));
			info.AddValue("_genericalterationEntityReturnsNewIfNotFound", _genericalterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGenericalterationEntity", _alwaysFetchGenericalterationEntity);
			info.AddValue("_alreadyFetchedGenericalterationEntity", _alreadyFetchedGenericalterationEntity);
			info.AddValue("_posAlterationEntity", (!this.MarkedForDeletion?_posAlterationEntity:null));
			info.AddValue("_posAlterationEntityReturnsNewIfNotFound", _posAlterationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPosAlterationEntity", _alwaysFetchPosAlterationEntity);
			info.AddValue("_alreadyFetchedPosAlterationEntity", _alreadyFetchedPosAlterationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParentAlterationEntity":
					_alreadyFetchedParentAlterationEntity = true;
					this.ParentAlterationEntity = (AlterationEntity)entity;
					break;
				case "BrandEntity":
					_alreadyFetchedBrandEntity = true;
					this.BrandEntity = (BrandEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "ExternalProductEntity":
					_alreadyFetchedExternalProductEntity = true;
					this.ExternalProductEntity = (ExternalProductEntity)entity;
					break;
				case "GenericalterationEntity":
					_alreadyFetchedGenericalterationEntity = true;
					this.GenericalterationEntity = (GenericalterationEntity)entity;
					break;
				case "PosAlterationEntity":
					_alreadyFetchedPosAlterationEntity = true;
					this.PosAlterationEntity = (PosalterationEntity)entity;
					break;
				case "AlterationCollection":
					_alreadyFetchedAlterationCollection = true;
					if(entity!=null)
					{
						this.AlterationCollection.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationitemCollection":
					_alreadyFetchedAlterationitemCollection = true;
					if(entity!=null)
					{
						this.AlterationitemCollection.Add((AlterationitemEntity)entity);
					}
					break;
				case "AlterationitemAlterationCollection":
					_alreadyFetchedAlterationitemAlterationCollection = true;
					if(entity!=null)
					{
						this.AlterationitemAlterationCollection.Add((AlterationitemAlterationEntity)entity);
					}
					break;
				case "AlterationLanguageCollection":
					_alreadyFetchedAlterationLanguageCollection = true;
					if(entity!=null)
					{
						this.AlterationLanguageCollection.Add((AlterationLanguageEntity)entity);
					}
					break;
				case "AlterationProductCollection":
					_alreadyFetchedAlterationProductCollection = true;
					if(entity!=null)
					{
						this.AlterationProductCollection.Add((AlterationProductEntity)entity);
					}
					break;
				case "CategoryAlterationCollection":
					_alreadyFetchedCategoryAlterationCollection = true;
					if(entity!=null)
					{
						this.CategoryAlterationCollection.Add((CategoryAlterationEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "OrderitemAlterationitemCollection":
					_alreadyFetchedOrderitemAlterationitemCollection = true;
					if(entity!=null)
					{
						this.OrderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)entity);
					}
					break;
				case "PriceLevelItemCollection":
					_alreadyFetchedPriceLevelItemCollection = true;
					if(entity!=null)
					{
						this.PriceLevelItemCollection.Add((PriceLevelItemEntity)entity);
					}
					break;
				case "ProductAlterationCollection":
					_alreadyFetchedProductAlterationCollection = true;
					if(entity!=null)
					{
						this.ProductAlterationCollection.Add((ProductAlterationEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "PointOfInterestCollectionViaMedium":
					_alreadyFetchedPointOfInterestCollectionViaMedium = true;
					if(entity!=null)
					{
						this.PointOfInterestCollectionViaMedium.Add((PointOfInterestEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParentAlterationEntity":
					SetupSyncParentAlterationEntity(relatedEntity);
					break;
				case "BrandEntity":
					SetupSyncBrandEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "ExternalProductEntity":
					SetupSyncExternalProductEntity(relatedEntity);
					break;
				case "GenericalterationEntity":
					SetupSyncGenericalterationEntity(relatedEntity);
					break;
				case "PosAlterationEntity":
					SetupSyncPosAlterationEntity(relatedEntity);
					break;
				case "AlterationCollection":
					_alterationCollection.Add((AlterationEntity)relatedEntity);
					break;
				case "AlterationitemCollection":
					_alterationitemCollection.Add((AlterationitemEntity)relatedEntity);
					break;
				case "AlterationitemAlterationCollection":
					_alterationitemAlterationCollection.Add((AlterationitemAlterationEntity)relatedEntity);
					break;
				case "AlterationLanguageCollection":
					_alterationLanguageCollection.Add((AlterationLanguageEntity)relatedEntity);
					break;
				case "AlterationProductCollection":
					_alterationProductCollection.Add((AlterationProductEntity)relatedEntity);
					break;
				case "CategoryAlterationCollection":
					_categoryAlterationCollection.Add((CategoryAlterationEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "OrderitemAlterationitemCollection":
					_orderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)relatedEntity);
					break;
				case "PriceLevelItemCollection":
					_priceLevelItemCollection.Add((PriceLevelItemEntity)relatedEntity);
					break;
				case "ProductAlterationCollection":
					_productAlterationCollection.Add((ProductAlterationEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParentAlterationEntity":
					DesetupSyncParentAlterationEntity(false, true);
					break;
				case "BrandEntity":
					DesetupSyncBrandEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "ExternalProductEntity":
					DesetupSyncExternalProductEntity(false, true);
					break;
				case "GenericalterationEntity":
					DesetupSyncGenericalterationEntity(false, true);
					break;
				case "PosAlterationEntity":
					DesetupSyncPosAlterationEntity(false, true);
					break;
				case "AlterationCollection":
					this.PerformRelatedEntityRemoval(_alterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationitemCollection":
					this.PerformRelatedEntityRemoval(_alterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationitemAlterationCollection":
					this.PerformRelatedEntityRemoval(_alterationitemAlterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationLanguageCollection":
					this.PerformRelatedEntityRemoval(_alterationLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlterationProductCollection":
					this.PerformRelatedEntityRemoval(_alterationProductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategoryAlterationCollection":
					this.PerformRelatedEntityRemoval(_categoryAlterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemAlterationitemCollection":
					this.PerformRelatedEntityRemoval(_orderitemAlterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PriceLevelItemCollection":
					this.PerformRelatedEntityRemoval(_priceLevelItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductAlterationCollection":
					this.PerformRelatedEntityRemoval(_productAlterationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_parentAlterationEntity!=null)
			{
				toReturn.Add(_parentAlterationEntity);
			}
			if(_brandEntity!=null)
			{
				toReturn.Add(_brandEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_externalProductEntity!=null)
			{
				toReturn.Add(_externalProductEntity);
			}
			if(_genericalterationEntity!=null)
			{
				toReturn.Add(_genericalterationEntity);
			}
			if(_posAlterationEntity!=null)
			{
				toReturn.Add(_posAlterationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_alterationCollection);
			toReturn.Add(_alterationitemCollection);
			toReturn.Add(_alterationitemAlterationCollection);
			toReturn.Add(_alterationLanguageCollection);
			toReturn.Add(_alterationProductCollection);
			toReturn.Add(_categoryAlterationCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_orderitemAlterationitemCollection);
			toReturn.Add(_priceLevelItemCollection);
			toReturn.Add(_productAlterationCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationId)
		{
			return FetchUsingPK(alterationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(alterationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(alterationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(alterationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AlterationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AlterationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationCollection(forceFetch, _alterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationCollection || forceFetch || _alwaysFetchAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollection);
				_alterationCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollection.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_alterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollection = true;
			}
			return _alterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollection'. These settings will be taken into account
		/// when the property AlterationCollection is requested or GetMultiAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollection.SortClauses=sortClauses;
			_alterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch)
		{
			return GetMultiAlterationitemCollection(forceFetch, _alterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationitemCollection(forceFetch, _alterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemCollection GetMultiAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationitemCollection || forceFetch || _alwaysFetchAlterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationitemCollection);
				_alterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationitemCollection.GetMultiManyToOne(this, null, null, filter);
				_alterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationitemCollection = true;
			}
			return _alterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationitemCollection'. These settings will be taken into account
		/// when the property AlterationitemCollection is requested or GetMultiAlterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationitemCollection.SortClauses=sortClauses;
			_alterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch)
		{
			return GetMultiAlterationitemAlterationCollection(forceFetch, _alterationitemAlterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationitemAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationitemAlterationCollection(forceFetch, _alterationitemAlterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationitemAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection GetMultiAlterationitemAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationitemAlterationCollection || forceFetch || _alwaysFetchAlterationitemAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationitemAlterationCollection);
				_alterationitemAlterationCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationitemAlterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationitemAlterationCollection.GetMultiManyToOne(this, null, filter);
				_alterationitemAlterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationitemAlterationCollection = true;
			}
			return _alterationitemAlterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationitemAlterationCollection'. These settings will be taken into account
		/// when the property AlterationitemAlterationCollection is requested or GetMultiAlterationitemAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationitemAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationitemAlterationCollection.SortClauses=sortClauses;
			_alterationitemAlterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch)
		{
			return GetMultiAlterationLanguageCollection(forceFetch, _alterationLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationLanguageCollection(forceFetch, _alterationLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationLanguageCollection GetMultiAlterationLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationLanguageCollection || forceFetch || _alwaysFetchAlterationLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationLanguageCollection);
				_alterationLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationLanguageCollection.GetMultiManyToOne(this, null, filter);
				_alterationLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationLanguageCollection = true;
			}
			return _alterationLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationLanguageCollection'. These settings will be taken into account
		/// when the property AlterationLanguageCollection is requested or GetMultiAlterationLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationLanguageCollection.SortClauses=sortClauses;
			_alterationLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationProductCollection GetMultiAlterationProductCollection(bool forceFetch)
		{
			return GetMultiAlterationProductCollection(forceFetch, _alterationProductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AlterationProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationProductCollection GetMultiAlterationProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAlterationProductCollection(forceFetch, _alterationProductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AlterationProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationProductCollection GetMultiAlterationProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAlterationProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AlterationProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AlterationProductCollection GetMultiAlterationProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAlterationProductCollection || forceFetch || _alwaysFetchAlterationProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationProductCollection);
				_alterationProductCollection.SuppressClearInGetMulti=!forceFetch;
				_alterationProductCollection.EntityFactoryToUse = entityFactoryToUse;
				_alterationProductCollection.GetMultiManyToOne(this, null, filter);
				_alterationProductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationProductCollection = true;
			}
			return _alterationProductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationProductCollection'. These settings will be taken into account
		/// when the property AlterationProductCollection is requested or GetMultiAlterationProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationProductCollection.SortClauses=sortClauses;
			_alterationProductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch)
		{
			return GetMultiCategoryAlterationCollection(forceFetch, _categoryAlterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryAlterationCollection(forceFetch, _categoryAlterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryAlterationCollection GetMultiCategoryAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryAlterationCollection || forceFetch || _alwaysFetchCategoryAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryAlterationCollection);
				_categoryAlterationCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryAlterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryAlterationCollection.GetMultiManyToOne(this, null, filter);
				_categoryAlterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryAlterationCollection = true;
			}
			return _categoryAlterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryAlterationCollection'. These settings will be taken into account
		/// when the property CategoryAlterationCollection is requested or GetMultiCategoryAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryAlterationCollection.SortClauses=sortClauses;
			_categoryAlterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemAlterationitemCollection || forceFetch || _alwaysFetchOrderitemAlterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemAlterationitemCollection);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemAlterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemAlterationitemCollection.GetMultiManyToOne(this, null, null, null, null, null, filter);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemAlterationitemCollection = true;
			}
			return _orderitemAlterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemAlterationitemCollection'. These settings will be taken into account
		/// when the property OrderitemAlterationitemCollection is requested or GetMultiOrderitemAlterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemAlterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemAlterationitemCollection.SortClauses=sortClauses;
			_orderitemAlterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PriceLevelItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, _priceLevelItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PriceLevelItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, _priceLevelItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPriceLevelItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PriceLevelItemCollection GetMultiPriceLevelItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPriceLevelItemCollection || forceFetch || _alwaysFetchPriceLevelItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_priceLevelItemCollection);
				_priceLevelItemCollection.SuppressClearInGetMulti=!forceFetch;
				_priceLevelItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_priceLevelItemCollection.GetMultiManyToOne(this, null, null, null, filter);
				_priceLevelItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPriceLevelItemCollection = true;
			}
			return _priceLevelItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PriceLevelItemCollection'. These settings will be taken into account
		/// when the property PriceLevelItemCollection is requested or GetMultiPriceLevelItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPriceLevelItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_priceLevelItemCollection.SortClauses=sortClauses;
			_priceLevelItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductAlterationCollection GetMultiProductAlterationCollection(bool forceFetch)
		{
			return GetMultiProductAlterationCollection(forceFetch, _productAlterationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductAlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductAlterationCollection GetMultiProductAlterationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductAlterationCollection(forceFetch, _productAlterationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductAlterationCollection GetMultiProductAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductAlterationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductAlterationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductAlterationCollection GetMultiProductAlterationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductAlterationCollection || forceFetch || _alwaysFetchProductAlterationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productAlterationCollection);
				_productAlterationCollection.SuppressClearInGetMulti=!forceFetch;
				_productAlterationCollection.EntityFactoryToUse = entityFactoryToUse;
				_productAlterationCollection.GetMultiManyToOne(this, null, null, filter);
				_productAlterationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductAlterationCollection = true;
			}
			return _productAlterationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductAlterationCollection'. These settings will be taken into account
		/// when the property ProductAlterationCollection is requested or GetMultiProductAlterationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductAlterationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productAlterationCollection.SortClauses=sortClauses;
			_productAlterationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationFields.AlterationId, ComparisonOperator.Equal, this.AlterationId, "AlterationEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationFields.AlterationId, ComparisonOperator.Equal, this.AlterationId, "AlterationEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationFields.AlterationId, ComparisonOperator.Equal, this.AlterationId, "AlterationEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationFields.AlterationId, ComparisonOperator.Equal, this.AlterationId, "AlterationEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch)
		{
			return GetMultiPointOfInterestCollectionViaMedium(forceFetch, _pointOfInterestCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollectionViaMedium || forceFetch || _alwaysFetchPointOfInterestCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationFields.AlterationId, ComparisonOperator.Equal, this.AlterationId, "AlterationEntity__"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollectionViaMedium.GetMulti(filter, GetRelationsForField("PointOfInterestCollectionViaMedium"));
				_pointOfInterestCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollectionViaMedium = true;
			}
			return _pointOfInterestCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollectionViaMedium'. These settings will be taken into account
		/// when the property PointOfInterestCollectionViaMedium is requested or GetMultiPointOfInterestCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollectionViaMedium.SortClauses=sortClauses;
			_pointOfInterestCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationFields.AlterationId, ComparisonOperator.Equal, this.AlterationId, "AlterationEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AlterationFields.AlterationId, ComparisonOperator.Equal, this.AlterationId, "AlterationEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public AlterationEntity GetSingleParentAlterationEntity()
		{
			return GetSingleParentAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationEntity' which is related to this entity.</returns>
		public virtual AlterationEntity GetSingleParentAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentAlterationEntity || forceFetch || _alwaysFetchParentAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationEntityUsingAlterationIdParentAlterationId);
				AlterationEntity newEntity = new AlterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentAlterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AlterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentAlterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentAlterationEntity = newEntity;
				_alreadyFetchedParentAlterationEntity = fetchResult;
			}
			return _parentAlterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public BrandEntity GetSingleBrandEntity()
		{
			return GetSingleBrandEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'BrandEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BrandEntity' which is related to this entity.</returns>
		public virtual BrandEntity GetSingleBrandEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedBrandEntity || forceFetch || _alwaysFetchBrandEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BrandEntityUsingBrandId);
				BrandEntity newEntity = new BrandEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BrandId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BrandEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_brandEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BrandEntity = newEntity;
				_alreadyFetchedBrandEntity = fetchResult;
			}
			return _brandEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public ExternalProductEntity GetSingleExternalProductEntity()
		{
			return GetSingleExternalProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalProductEntity' which is related to this entity.</returns>
		public virtual ExternalProductEntity GetSingleExternalProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalProductEntity || forceFetch || _alwaysFetchExternalProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalProductEntityUsingExternalProductId);
				ExternalProductEntity newEntity = new ExternalProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ExternalProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalProductEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalProductEntity = newEntity;
				_alreadyFetchedExternalProductEntity = fetchResult;
			}
			return _externalProductEntity;
		}


		/// <summary> Retrieves the related entity of type 'GenericalterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'GenericalterationEntity' which is related to this entity.</returns>
		public GenericalterationEntity GetSingleGenericalterationEntity()
		{
			return GetSingleGenericalterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'GenericalterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'GenericalterationEntity' which is related to this entity.</returns>
		public virtual GenericalterationEntity GetSingleGenericalterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedGenericalterationEntity || forceFetch || _alwaysFetchGenericalterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.GenericalterationEntityUsingGenericalterationId);
				GenericalterationEntity newEntity = new GenericalterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GenericalterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (GenericalterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_genericalterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GenericalterationEntity = newEntity;
				_alreadyFetchedGenericalterationEntity = fetchResult;
			}
			return _genericalterationEntity;
		}


		/// <summary> Retrieves the related entity of type 'PosalterationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PosalterationEntity' which is related to this entity.</returns>
		public PosalterationEntity GetSinglePosAlterationEntity()
		{
			return GetSinglePosAlterationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PosalterationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PosalterationEntity' which is related to this entity.</returns>
		public virtual PosalterationEntity GetSinglePosAlterationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPosAlterationEntity || forceFetch || _alwaysFetchPosAlterationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PosalterationEntityUsingPosalterationId);
				PosalterationEntity newEntity = new PosalterationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PosalterationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PosalterationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_posAlterationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PosAlterationEntity = newEntity;
				_alreadyFetchedPosAlterationEntity = fetchResult;
			}
			return _posAlterationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParentAlterationEntity", _parentAlterationEntity);
			toReturn.Add("BrandEntity", _brandEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("ExternalProductEntity", _externalProductEntity);
			toReturn.Add("GenericalterationEntity", _genericalterationEntity);
			toReturn.Add("PosAlterationEntity", _posAlterationEntity);
			toReturn.Add("AlterationCollection", _alterationCollection);
			toReturn.Add("AlterationitemCollection", _alterationitemCollection);
			toReturn.Add("AlterationitemAlterationCollection", _alterationitemAlterationCollection);
			toReturn.Add("AlterationLanguageCollection", _alterationLanguageCollection);
			toReturn.Add("AlterationProductCollection", _alterationProductCollection);
			toReturn.Add("CategoryAlterationCollection", _categoryAlterationCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("OrderitemAlterationitemCollection", _orderitemAlterationitemCollection);
			toReturn.Add("PriceLevelItemCollection", _priceLevelItemCollection);
			toReturn.Add("ProductAlterationCollection", _productAlterationCollection);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("PointOfInterestCollectionViaMedium", _pointOfInterestCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="validator">The validator object for this AlterationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 alterationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(alterationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_alterationCollection = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationCollection.SetContainingEntityInfo(this, "ParentAlterationEntity");

			_alterationitemCollection = new Obymobi.Data.CollectionClasses.AlterationitemCollection();
			_alterationitemCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_alterationitemAlterationCollection = new Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection();
			_alterationitemAlterationCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_alterationLanguageCollection = new Obymobi.Data.CollectionClasses.AlterationLanguageCollection();
			_alterationLanguageCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_alterationProductCollection = new Obymobi.Data.CollectionClasses.AlterationProductCollection();
			_alterationProductCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_categoryAlterationCollection = new Obymobi.Data.CollectionClasses.CategoryAlterationCollection();
			_categoryAlterationCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_orderitemAlterationitemCollection = new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection();
			_orderitemAlterationitemCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_priceLevelItemCollection = new Obymobi.Data.CollectionClasses.PriceLevelItemCollection();
			_priceLevelItemCollection.SetContainingEntityInfo(this, "AlterationEntity");

			_productAlterationCollection = new Obymobi.Data.CollectionClasses.ProductAlterationCollection();
			_productAlterationCollection.SetContainingEntityInfo(this, "AlterationEntity");
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_pointOfInterestCollectionViaMedium = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_parentAlterationEntityReturnsNewIfNotFound = true;
			_brandEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_externalProductEntityReturnsNewIfNotFound = true;
			_genericalterationEntityReturnsNewIfNotFound = true;
			_posAlterationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxOptions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosalterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailableOnOtoucho", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AvailableOnObymobi", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinLeadMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxLeadHours", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IntervalMinutes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowDatePicker", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GenericalterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Value", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderLevelEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentAlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceAddition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginalAlterationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowDuplicates", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTimeUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BrandId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAvailable", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _parentAlterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentAlterationEntity, new PropertyChangedEventHandler( OnParentAlterationEntityPropertyChanged ), "ParentAlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.AlterationEntityUsingAlterationIdParentAlterationIdStatic, true, signalRelatedEntity, "AlterationCollection", resetFKFields, new int[] { (int)AlterationFieldIndex.ParentAlterationId } );		
			_parentAlterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _parentAlterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentAlterationEntity(IEntityCore relatedEntity)
		{
			if(_parentAlterationEntity!=relatedEntity)
			{		
				DesetupSyncParentAlterationEntity(true, true);
				_parentAlterationEntity = (AlterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentAlterationEntity, new PropertyChangedEventHandler( OnParentAlterationEntityPropertyChanged ), "ParentAlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.AlterationEntityUsingAlterationIdParentAlterationIdStatic, true, ref _alreadyFetchedParentAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _brandEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBrandEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.BrandEntityUsingBrandIdStatic, true, signalRelatedEntity, "AlterationCollection", resetFKFields, new int[] { (int)AlterationFieldIndex.BrandId } );		
			_brandEntity = null;
		}
		
		/// <summary> setups the sync logic for member _brandEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBrandEntity(IEntityCore relatedEntity)
		{
			if(_brandEntity!=relatedEntity)
			{		
				DesetupSyncBrandEntity(true, true);
				_brandEntity = (BrandEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _brandEntity, new PropertyChangedEventHandler( OnBrandEntityPropertyChanged ), "BrandEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.BrandEntityUsingBrandIdStatic, true, ref _alreadyFetchedBrandEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBrandEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "AlterationCollection", resetFKFields, new int[] { (int)AlterationFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalProductEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalProductEntity, new PropertyChangedEventHandler( OnExternalProductEntityPropertyChanged ), "ExternalProductEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.ExternalProductEntityUsingExternalProductIdStatic, true, signalRelatedEntity, "AlterationCollection", resetFKFields, new int[] { (int)AlterationFieldIndex.ExternalProductId } );		
			_externalProductEntity = null;
		}
		
		/// <summary> setups the sync logic for member _externalProductEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalProductEntity(IEntityCore relatedEntity)
		{
			if(_externalProductEntity!=relatedEntity)
			{		
				DesetupSyncExternalProductEntity(true, true);
				_externalProductEntity = (ExternalProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalProductEntity, new PropertyChangedEventHandler( OnExternalProductEntityPropertyChanged ), "ExternalProductEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.ExternalProductEntityUsingExternalProductIdStatic, true, ref _alreadyFetchedExternalProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _genericalterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGenericalterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _genericalterationEntity, new PropertyChangedEventHandler( OnGenericalterationEntityPropertyChanged ), "GenericalterationEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.GenericalterationEntityUsingGenericalterationIdStatic, true, signalRelatedEntity, "AlterationCollection", resetFKFields, new int[] { (int)AlterationFieldIndex.GenericalterationId } );		
			_genericalterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _genericalterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGenericalterationEntity(IEntityCore relatedEntity)
		{
			if(_genericalterationEntity!=relatedEntity)
			{		
				DesetupSyncGenericalterationEntity(true, true);
				_genericalterationEntity = (GenericalterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _genericalterationEntity, new PropertyChangedEventHandler( OnGenericalterationEntityPropertyChanged ), "GenericalterationEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.GenericalterationEntityUsingGenericalterationIdStatic, true, ref _alreadyFetchedGenericalterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGenericalterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _posAlterationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPosAlterationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _posAlterationEntity, new PropertyChangedEventHandler( OnPosAlterationEntityPropertyChanged ), "PosAlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.PosalterationEntityUsingPosalterationIdStatic, true, signalRelatedEntity, "AlterationCollection", resetFKFields, new int[] { (int)AlterationFieldIndex.PosalterationId } );		
			_posAlterationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _posAlterationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPosAlterationEntity(IEntityCore relatedEntity)
		{
			if(_posAlterationEntity!=relatedEntity)
			{		
				DesetupSyncPosAlterationEntity(true, true);
				_posAlterationEntity = (PosalterationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _posAlterationEntity, new PropertyChangedEventHandler( OnPosAlterationEntityPropertyChanged ), "PosAlterationEntity", Obymobi.Data.RelationClasses.StaticAlterationRelations.PosalterationEntityUsingPosalterationIdStatic, true, ref _alreadyFetchedPosAlterationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPosAlterationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 alterationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AlterationFieldIndex.AlterationId].ForcedCurrentValueWrite(alterationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAlterationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AlterationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AlterationRelations Relations
		{
			get	{ return new AlterationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "AlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationitemCollection(), (IEntityRelation)GetRelationsForField("AlterationitemCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.AlterationitemEntity, 0, null, null, null, "AlterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationitemAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationitemAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection(), (IEntityRelation)GetRelationsForField("AlterationitemAlterationCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.AlterationitemAlterationEntity, 0, null, null, null, "AlterationitemAlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationLanguageCollection(), (IEntityRelation)GetRelationsForField("AlterationLanguageCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.AlterationLanguageEntity, 0, null, null, null, "AlterationLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AlterationProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationProductCollection(), (IEntityRelation)GetRelationsForField("AlterationProductCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.AlterationProductEntity, 0, null, null, null, "AlterationProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategoryAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryAlterationCollection(), (IEntityRelation)GetRelationsForField("CategoryAlterationCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.CategoryAlterationEntity, 0, null, null, null, "CategoryAlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemAlterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemAlterationitemCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, 0, null, null, null, "OrderitemAlterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceLevelItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceLevelItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceLevelItemCollection(), (IEntityRelation)GetRelationsForField("PriceLevelItemCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.PriceLevelItemEntity, 0, null, null, null, "PriceLevelItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductAlteration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductAlterationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductAlterationCollection(), (IEntityRelation)GetRelationsForField("ProductAlterationCollection")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.ProductAlterationEntity, 0, null, null, null, "ProductAlterationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, GetRelationsForField("PointOfInterestCollectionViaMedium"), "PointOfInterestCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingAlterationId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), (IEntityRelation)GetRelationsForField("ParentAlterationEntity")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, null, "ParentAlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Brand'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBrandEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BrandCollection(), (IEntityRelation)GetRelationsForField("BrandEntity")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.BrandEntity, 0, null, null, null, "BrandEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalProduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ExternalProductCollection(), (IEntityRelation)GetRelationsForField("ExternalProductEntity")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.ExternalProductEntity, 0, null, null, null, "ExternalProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericalterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericalterationCollection(), (IEntityRelation)GetRelationsForField("GenericalterationEntity")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.GenericalterationEntity, 0, null, null, null, "GenericalterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posalteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosAlterationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosalterationCollection(), (IEntityRelation)GetRelationsForField("PosAlterationEntity")[0], (int)Obymobi.Data.EntityType.AlterationEntity, (int)Obymobi.Data.EntityType.PosalterationEntity, 0, null, null, null, "PosAlterationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationId
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.AlterationId, true); }
			set	{ SetValue((int)AlterationFieldIndex.AlterationId, value, true); }
		}

		/// <summary> The Name property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.Name, true); }
			set	{ SetValue((int)AlterationFieldIndex.Name, value, true); }
		}

		/// <summary> The MinOptions property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MinOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinOptions
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MinOptions, true); }
			set	{ SetValue((int)AlterationFieldIndex.MinOptions, value, true); }
		}

		/// <summary> The MaxOptions property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MaxOptions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxOptions
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MaxOptions, true); }
			set	{ SetValue((int)AlterationFieldIndex.MaxOptions, value, true); }
		}

		/// <summary> The PosalterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."PosalterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PosalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.PosalterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.PosalterationId, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.CompanyId, false); }
			set	{ SetValue((int)AlterationFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The AvailableOnOtoucho property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AvailableOnOtoucho"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnOtoucho
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.AvailableOnOtoucho, true); }
			set	{ SetValue((int)AlterationFieldIndex.AvailableOnOtoucho, value, true); }
		}

		/// <summary> The AvailableOnObymobi property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AvailableOnObymobi"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AvailableOnObymobi
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.AvailableOnObymobi, true); }
			set	{ SetValue((int)AlterationFieldIndex.AvailableOnObymobi, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AlterationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AlterationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The MinLeadMinutes property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MinLeadMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinLeadMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MinLeadMinutes, true); }
			set	{ SetValue((int)AlterationFieldIndex.MinLeadMinutes, value, true); }
		}

		/// <summary> The MaxLeadHours property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."MaxLeadHours"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxLeadHours
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.MaxLeadHours, true); }
			set	{ SetValue((int)AlterationFieldIndex.MaxLeadHours, value, true); }
		}

		/// <summary> The IntervalMinutes property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."IntervalMinutes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 IntervalMinutes
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.IntervalMinutes, true); }
			set	{ SetValue((int)AlterationFieldIndex.IntervalMinutes, value, true); }
		}

		/// <summary> The ShowDatePicker property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."ShowDatePicker"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowDatePicker
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.ShowDatePicker, true); }
			set	{ SetValue((int)AlterationFieldIndex.ShowDatePicker, value, true); }
		}

		/// <summary> The Type property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AlterationType Type
		{
			get { return (Obymobi.Enums.AlterationType)GetValue((int)AlterationFieldIndex.Type, true); }
			set	{ SetValue((int)AlterationFieldIndex.Type, value, true); }
		}

		/// <summary> The GenericalterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."GenericalterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GenericalterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.GenericalterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.GenericalterationId, value, true); }
		}

		/// <summary> The Value property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Value"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Value
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.Value, true); }
			set	{ SetValue((int)AlterationFieldIndex.Value, value, true); }
		}

		/// <summary> The OrderLevelEnabled property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."OrderLevelEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OrderLevelEnabled
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.OrderLevelEnabled, true); }
			set	{ SetValue((int)AlterationFieldIndex.OrderLevelEnabled, value, true); }
		}

		/// <summary> The Description property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.Description, true); }
			set	{ SetValue((int)AlterationFieldIndex.Description, value, true); }
		}

		/// <summary> The LayoutType property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."LayoutType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AlterationLayoutType LayoutType
		{
			get { return (Obymobi.Enums.AlterationLayoutType)GetValue((int)AlterationFieldIndex.LayoutType, true); }
			set	{ SetValue((int)AlterationFieldIndex.LayoutType, value, true); }
		}

		/// <summary> The ParentAlterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."ParentAlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentAlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.ParentAlterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.ParentAlterationId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.SortOrder, true); }
			set	{ SetValue((int)AlterationFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The Visible property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.Visible, true); }
			set	{ SetValue((int)AlterationFieldIndex.Visible, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The FriendlyName property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."FriendlyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)AlterationFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)AlterationFieldIndex.FriendlyName, value, true); }
		}

		/// <summary> The PriceAddition property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."PriceAddition"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceAddition
		{
			get { return (System.Decimal)GetValue((int)AlterationFieldIndex.PriceAddition, true); }
			set	{ SetValue((int)AlterationFieldIndex.PriceAddition, value, true); }
		}

		/// <summary> The Version property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)AlterationFieldIndex.Version, true); }
			set	{ SetValue((int)AlterationFieldIndex.Version, value, true); }
		}

		/// <summary> The OriginalAlterationId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."OriginalAlterationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OriginalAlterationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.OriginalAlterationId, false); }
			set	{ SetValue((int)AlterationFieldIndex.OriginalAlterationId, value, true); }
		}

		/// <summary> The AllowDuplicates property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."AllowDuplicates"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowDuplicates
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.AllowDuplicates, true); }
			set	{ SetValue((int)AlterationFieldIndex.AllowDuplicates, value, true); }
		}

		/// <summary> The StartTime property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."StartTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.StartTime, false); }
			set	{ SetValue((int)AlterationFieldIndex.StartTime, value, true); }
		}

		/// <summary> The EndTime property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."EndTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.EndTime, false); }
			set	{ SetValue((int)AlterationFieldIndex.EndTime, value, true); }
		}

		/// <summary> The StartTimeUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."StartTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.StartTimeUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.StartTimeUTC, value, true); }
		}

		/// <summary> The EndTimeUTC property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."EndTimeUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndTimeUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationFieldIndex.EndTimeUTC, false); }
			set	{ SetValue((int)AlterationFieldIndex.EndTimeUTC, value, true); }
		}

		/// <summary> The BrandId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."BrandId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BrandId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.BrandId, false); }
			set	{ SetValue((int)AlterationFieldIndex.BrandId, value, true); }
		}

		/// <summary> The ExternalProductId property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."ExternalProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationFieldIndex.ExternalProductId, false); }
			set	{ SetValue((int)AlterationFieldIndex.ExternalProductId, value, true); }
		}

		/// <summary> The IsAvailable property of the Entity Alteration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Alteration"."IsAvailable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAvailable
		{
			get { return (System.Boolean)GetValue((int)AlterationFieldIndex.IsAvailable, true); }
			set	{ SetValue((int)AlterationFieldIndex.IsAvailable, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollection
		{
			get	{ return GetMultiAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollection. When set to true, AlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollection
		{
			get	{ return _alwaysFetchAlterationCollection; }
			set	{ _alwaysFetchAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollection already has been fetched. Setting this property to false when AlterationCollection has been fetched
		/// will clear the AlterationCollection collection well. Setting this property to true while AlterationCollection hasn't been fetched disables lazy loading for AlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollection
		{
			get { return _alreadyFetchedAlterationCollection;}
			set 
			{
				if(_alreadyFetchedAlterationCollection && !value && (_alterationCollection != null))
				{
					_alterationCollection.Clear();
				}
				_alreadyFetchedAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemCollection AlterationitemCollection
		{
			get	{ return GetMultiAlterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationitemCollection. When set to true, AlterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationitemCollection
		{
			get	{ return _alwaysFetchAlterationitemCollection; }
			set	{ _alwaysFetchAlterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationitemCollection already has been fetched. Setting this property to false when AlterationitemCollection has been fetched
		/// will clear the AlterationitemCollection collection well. Setting this property to true while AlterationitemCollection hasn't been fetched disables lazy loading for AlterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationitemCollection
		{
			get { return _alreadyFetchedAlterationitemCollection;}
			set 
			{
				if(_alreadyFetchedAlterationitemCollection && !value && (_alterationitemCollection != null))
				{
					_alterationitemCollection.Clear();
				}
				_alreadyFetchedAlterationitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationitemAlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationitemAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationitemAlterationCollection AlterationitemAlterationCollection
		{
			get	{ return GetMultiAlterationitemAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationitemAlterationCollection. When set to true, AlterationitemAlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationitemAlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationitemAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationitemAlterationCollection
		{
			get	{ return _alwaysFetchAlterationitemAlterationCollection; }
			set	{ _alwaysFetchAlterationitemAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationitemAlterationCollection already has been fetched. Setting this property to false when AlterationitemAlterationCollection has been fetched
		/// will clear the AlterationitemAlterationCollection collection well. Setting this property to true while AlterationitemAlterationCollection hasn't been fetched disables lazy loading for AlterationitemAlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationitemAlterationCollection
		{
			get { return _alreadyFetchedAlterationitemAlterationCollection;}
			set 
			{
				if(_alreadyFetchedAlterationitemAlterationCollection && !value && (_alterationitemAlterationCollection != null))
				{
					_alterationitemAlterationCollection.Clear();
				}
				_alreadyFetchedAlterationitemAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationLanguageCollection AlterationLanguageCollection
		{
			get	{ return GetMultiAlterationLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationLanguageCollection. When set to true, AlterationLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationLanguageCollection
		{
			get	{ return _alwaysFetchAlterationLanguageCollection; }
			set	{ _alwaysFetchAlterationLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationLanguageCollection already has been fetched. Setting this property to false when AlterationLanguageCollection has been fetched
		/// will clear the AlterationLanguageCollection collection well. Setting this property to true while AlterationLanguageCollection hasn't been fetched disables lazy loading for AlterationLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationLanguageCollection
		{
			get { return _alreadyFetchedAlterationLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAlterationLanguageCollection && !value && (_alterationLanguageCollection != null))
				{
					_alterationLanguageCollection.Clear();
				}
				_alreadyFetchedAlterationLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AlterationProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationProductCollection AlterationProductCollection
		{
			get	{ return GetMultiAlterationProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationProductCollection. When set to true, AlterationProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAlterationProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationProductCollection
		{
			get	{ return _alwaysFetchAlterationProductCollection; }
			set	{ _alwaysFetchAlterationProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationProductCollection already has been fetched. Setting this property to false when AlterationProductCollection has been fetched
		/// will clear the AlterationProductCollection collection well. Setting this property to true while AlterationProductCollection hasn't been fetched disables lazy loading for AlterationProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationProductCollection
		{
			get { return _alreadyFetchedAlterationProductCollection;}
			set 
			{
				if(_alreadyFetchedAlterationProductCollection && !value && (_alterationProductCollection != null))
				{
					_alterationProductCollection.Clear();
				}
				_alreadyFetchedAlterationProductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategoryAlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryAlterationCollection CategoryAlterationCollection
		{
			get	{ return GetMultiCategoryAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryAlterationCollection. When set to true, CategoryAlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryAlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryAlterationCollection
		{
			get	{ return _alwaysFetchCategoryAlterationCollection; }
			set	{ _alwaysFetchCategoryAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryAlterationCollection already has been fetched. Setting this property to false when CategoryAlterationCollection has been fetched
		/// will clear the CategoryAlterationCollection collection well. Setting this property to true while CategoryAlterationCollection hasn't been fetched disables lazy loading for CategoryAlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryAlterationCollection
		{
			get { return _alreadyFetchedCategoryAlterationCollection;}
			set 
			{
				if(_alreadyFetchedCategoryAlterationCollection && !value && (_categoryAlterationCollection != null))
				{
					_categoryAlterationCollection.Clear();
				}
				_alreadyFetchedCategoryAlterationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemAlterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection OrderitemAlterationitemCollection
		{
			get	{ return GetMultiOrderitemAlterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemAlterationitemCollection. When set to true, OrderitemAlterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemAlterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemAlterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemAlterationitemCollection
		{
			get	{ return _alwaysFetchOrderitemAlterationitemCollection; }
			set	{ _alwaysFetchOrderitemAlterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemAlterationitemCollection already has been fetched. Setting this property to false when OrderitemAlterationitemCollection has been fetched
		/// will clear the OrderitemAlterationitemCollection collection well. Setting this property to true while OrderitemAlterationitemCollection hasn't been fetched disables lazy loading for OrderitemAlterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemAlterationitemCollection
		{
			get { return _alreadyFetchedOrderitemAlterationitemCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemAlterationitemCollection && !value && (_orderitemAlterationitemCollection != null))
				{
					_orderitemAlterationitemCollection.Clear();
				}
				_alreadyFetchedOrderitemAlterationitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PriceLevelItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPriceLevelItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PriceLevelItemCollection PriceLevelItemCollection
		{
			get	{ return GetMultiPriceLevelItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PriceLevelItemCollection. When set to true, PriceLevelItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceLevelItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPriceLevelItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceLevelItemCollection
		{
			get	{ return _alwaysFetchPriceLevelItemCollection; }
			set	{ _alwaysFetchPriceLevelItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceLevelItemCollection already has been fetched. Setting this property to false when PriceLevelItemCollection has been fetched
		/// will clear the PriceLevelItemCollection collection well. Setting this property to true while PriceLevelItemCollection hasn't been fetched disables lazy loading for PriceLevelItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceLevelItemCollection
		{
			get { return _alreadyFetchedPriceLevelItemCollection;}
			set 
			{
				if(_alreadyFetchedPriceLevelItemCollection && !value && (_priceLevelItemCollection != null))
				{
					_priceLevelItemCollection.Clear();
				}
				_alreadyFetchedPriceLevelItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductAlterationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductAlterationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductAlterationCollection ProductAlterationCollection
		{
			get	{ return GetMultiProductAlterationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductAlterationCollection. When set to true, ProductAlterationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductAlterationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductAlterationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductAlterationCollection
		{
			get	{ return _alwaysFetchProductAlterationCollection; }
			set	{ _alwaysFetchProductAlterationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductAlterationCollection already has been fetched. Setting this property to false when ProductAlterationCollection has been fetched
		/// will clear the ProductAlterationCollection collection well. Setting this property to true while ProductAlterationCollection hasn't been fetched disables lazy loading for ProductAlterationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductAlterationCollection
		{
			get { return _alreadyFetchedProductAlterationCollection;}
			set 
			{
				if(_alreadyFetchedProductAlterationCollection && !value && (_productAlterationCollection != null))
				{
					_productAlterationCollection.Clear();
				}
				_alreadyFetchedProductAlterationCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollectionViaMedium
		{
			get { return GetMultiPointOfInterestCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollectionViaMedium. When set to true, PointOfInterestCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiPointOfInterestCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollectionViaMedium
		{
			get	{ return _alwaysFetchPointOfInterestCollectionViaMedium; }
			set	{ _alwaysFetchPointOfInterestCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollectionViaMedium already has been fetched. Setting this property to false when PointOfInterestCollectionViaMedium has been fetched
		/// will clear the PointOfInterestCollectionViaMedium collection well. Setting this property to true while PointOfInterestCollectionViaMedium hasn't been fetched disables lazy loading for PointOfInterestCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollectionViaMedium
		{
			get { return _alreadyFetchedPointOfInterestCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollectionViaMedium && !value && (_pointOfInterestCollectionViaMedium != null))
				{
					_pointOfInterestCollectionViaMedium.Clear();
				}
				_alreadyFetchedPointOfInterestCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AlterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationEntity ParentAlterationEntity
		{
			get	{ return GetSingleParentAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationCollection", "ParentAlterationEntity", _parentAlterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentAlterationEntity. When set to true, ParentAlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentAlterationEntity is accessed. You can always execute a forced fetch by calling GetSingleParentAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentAlterationEntity
		{
			get	{ return _alwaysFetchParentAlterationEntity; }
			set	{ _alwaysFetchParentAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentAlterationEntity already has been fetched. Setting this property to false when ParentAlterationEntity has been fetched
		/// will set ParentAlterationEntity to null as well. Setting this property to true while ParentAlterationEntity hasn't been fetched disables lazy loading for ParentAlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentAlterationEntity
		{
			get { return _alreadyFetchedParentAlterationEntity;}
			set 
			{
				if(_alreadyFetchedParentAlterationEntity && !value)
				{
					this.ParentAlterationEntity = null;
				}
				_alreadyFetchedParentAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentAlterationEntity is not found
		/// in the database. When set to true, ParentAlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ParentAlterationEntityReturnsNewIfNotFound
		{
			get	{ return _parentAlterationEntityReturnsNewIfNotFound; }
			set { _parentAlterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BrandEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBrandEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual BrandEntity BrandEntity
		{
			get	{ return GetSingleBrandEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBrandEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationCollection", "BrandEntity", _brandEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BrandEntity. When set to true, BrandEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BrandEntity is accessed. You can always execute a forced fetch by calling GetSingleBrandEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBrandEntity
		{
			get	{ return _alwaysFetchBrandEntity; }
			set	{ _alwaysFetchBrandEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BrandEntity already has been fetched. Setting this property to false when BrandEntity has been fetched
		/// will set BrandEntity to null as well. Setting this property to true while BrandEntity hasn't been fetched disables lazy loading for BrandEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBrandEntity
		{
			get { return _alreadyFetchedBrandEntity;}
			set 
			{
				if(_alreadyFetchedBrandEntity && !value)
				{
					this.BrandEntity = null;
				}
				_alreadyFetchedBrandEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BrandEntity is not found
		/// in the database. When set to true, BrandEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool BrandEntityReturnsNewIfNotFound
		{
			get	{ return _brandEntityReturnsNewIfNotFound; }
			set { _brandEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ExternalProductEntity ExternalProductEntity
		{
			get	{ return GetSingleExternalProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationCollection", "ExternalProductEntity", _externalProductEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalProductEntity. When set to true, ExternalProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalProductEntity is accessed. You can always execute a forced fetch by calling GetSingleExternalProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalProductEntity
		{
			get	{ return _alwaysFetchExternalProductEntity; }
			set	{ _alwaysFetchExternalProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalProductEntity already has been fetched. Setting this property to false when ExternalProductEntity has been fetched
		/// will set ExternalProductEntity to null as well. Setting this property to true while ExternalProductEntity hasn't been fetched disables lazy loading for ExternalProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalProductEntity
		{
			get { return _alreadyFetchedExternalProductEntity;}
			set 
			{
				if(_alreadyFetchedExternalProductEntity && !value)
				{
					this.ExternalProductEntity = null;
				}
				_alreadyFetchedExternalProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalProductEntity is not found
		/// in the database. When set to true, ExternalProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ExternalProductEntityReturnsNewIfNotFound
		{
			get	{ return _externalProductEntityReturnsNewIfNotFound; }
			set { _externalProductEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'GenericalterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGenericalterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual GenericalterationEntity GenericalterationEntity
		{
			get	{ return GetSingleGenericalterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGenericalterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationCollection", "GenericalterationEntity", _genericalterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GenericalterationEntity. When set to true, GenericalterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericalterationEntity is accessed. You can always execute a forced fetch by calling GetSingleGenericalterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericalterationEntity
		{
			get	{ return _alwaysFetchGenericalterationEntity; }
			set	{ _alwaysFetchGenericalterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericalterationEntity already has been fetched. Setting this property to false when GenericalterationEntity has been fetched
		/// will set GenericalterationEntity to null as well. Setting this property to true while GenericalterationEntity hasn't been fetched disables lazy loading for GenericalterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericalterationEntity
		{
			get { return _alreadyFetchedGenericalterationEntity;}
			set 
			{
				if(_alreadyFetchedGenericalterationEntity && !value)
				{
					this.GenericalterationEntity = null;
				}
				_alreadyFetchedGenericalterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GenericalterationEntity is not found
		/// in the database. When set to true, GenericalterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool GenericalterationEntityReturnsNewIfNotFound
		{
			get	{ return _genericalterationEntityReturnsNewIfNotFound; }
			set { _genericalterationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PosalterationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePosAlterationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PosalterationEntity PosAlterationEntity
		{
			get	{ return GetSinglePosAlterationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPosAlterationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationCollection", "PosAlterationEntity", _posAlterationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PosAlterationEntity. When set to true, PosAlterationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosAlterationEntity is accessed. You can always execute a forced fetch by calling GetSinglePosAlterationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosAlterationEntity
		{
			get	{ return _alwaysFetchPosAlterationEntity; }
			set	{ _alwaysFetchPosAlterationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosAlterationEntity already has been fetched. Setting this property to false when PosAlterationEntity has been fetched
		/// will set PosAlterationEntity to null as well. Setting this property to true while PosAlterationEntity hasn't been fetched disables lazy loading for PosAlterationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosAlterationEntity
		{
			get { return _alreadyFetchedPosAlterationEntity;}
			set 
			{
				if(_alreadyFetchedPosAlterationEntity && !value)
				{
					this.PosAlterationEntity = null;
				}
				_alreadyFetchedPosAlterationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PosAlterationEntity is not found
		/// in the database. When set to true, PosAlterationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PosAlterationEntityReturnsNewIfNotFound
		{
			get	{ return _posAlterationEntityReturnsNewIfNotFound; }
			set { _posAlterationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AlterationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
