﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PointOfInterest'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PointOfInterestEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PointOfInterestEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection	_accessCodePointOfInterestCollection;
		private bool	_alwaysFetchAccessCodePointOfInterestCollection, _alreadyFetchedAccessCodePointOfInterestCollection;
		private Obymobi.Data.CollectionClasses.BusinesshoursCollection	_businesshoursCollection;
		private bool	_alwaysFetchBusinesshoursCollection, _alreadyFetchedBusinesshoursCollection;
		private Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection	_companyManagementTaskCollection;
		private bool	_alwaysFetchCompanyManagementTaskCollection, _alreadyFetchedCompanyManagementTaskCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.MapPointOfInterestCollection	_mapPointOfInterestCollection;
		private bool	_alwaysFetchMapPointOfInterestCollection, _alreadyFetchedMapPointOfInterestCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection	_pointOfInterestAmenityCollection;
		private bool	_alwaysFetchPointOfInterestAmenityCollection, _alreadyFetchedPointOfInterestAmenityCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection	_pointOfInterestLanguageCollection;
		private bool	_alwaysFetchPointOfInterestLanguageCollection, _alreadyFetchedPointOfInterestLanguageCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection	_pointOfInterestVenueCategoryCollection;
		private bool	_alwaysFetchPointOfInterestVenueCategoryCollection, _alreadyFetchedPointOfInterestVenueCategoryCollection;
		private Obymobi.Data.CollectionClasses.ProductCollection	_productCollection;
		private bool	_alwaysFetchProductCollection, _alreadyFetchedProductCollection;
		private Obymobi.Data.CollectionClasses.UIModeCollection	_uIModeCollection;
		private bool	_alwaysFetchUIModeCollection, _alreadyFetchedUIModeCollection;
		private Obymobi.Data.CollectionClasses.AdvertisementCollection _advertisementCollectionViaMedium;
		private bool	_alwaysFetchAdvertisementCollectionViaMedium, _alreadyFetchedAdvertisementCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationCollection _alterationCollectionViaMedium;
		private bool	_alwaysFetchAlterationCollectionViaMedium, _alreadyFetchedAlterationCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.AlterationoptionCollection _alterationoptionCollectionViaMedium;
		private bool	_alwaysFetchAlterationoptionCollectionViaMedium, _alreadyFetchedAlterationoptionCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium;
		private bool	_alwaysFetchCategoryCollectionViaMedium, _alreadyFetchedCategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaMedium_;
		private bool	_alwaysFetchCategoryCollectionViaMedium_, _alreadyFetchedCategoryCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaMedium;
		private bool	_alwaysFetchCompanyCollectionViaMedium, _alreadyFetchedCompanyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaMedium;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaMedium, _alreadyFetchedDeliverypointgroupCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium, _alreadyFetchedEntertainmentCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaMedium_;
		private bool	_alwaysFetchEntertainmentCollectionViaMedium_, _alreadyFetchedEntertainmentCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaMedium;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaMedium, _alreadyFetchedEntertainmentcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection _genericcategoryCollectionViaMedium;
		private bool	_alwaysFetchGenericcategoryCollectionViaMedium, _alreadyFetchedGenericcategoryCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.GenericproductCollection _genericproductCollectionViaMedium;
		private bool	_alwaysFetchGenericproductCollectionViaMedium, _alreadyFetchedGenericproductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium;
		private bool	_alwaysFetchProductCollectionViaMedium, _alreadyFetchedProductCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaMedium_;
		private bool	_alwaysFetchProductCollectionViaMedium_, _alreadyFetchedProductCollectionViaMedium_;
		private Obymobi.Data.CollectionClasses.SurveyCollection _surveyCollectionViaMedium;
		private bool	_alwaysFetchSurveyCollectionViaMedium, _alreadyFetchedSurveyCollectionViaMedium;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaMedium;
		private bool	_alwaysFetchSurveyPageCollectionViaMedium, _alreadyFetchedSurveyPageCollectionViaMedium;
		private ActionButtonEntity _actionButtonEntity;
		private bool	_alwaysFetchActionButtonEntity, _alreadyFetchedActionButtonEntity, _actionButtonEntityReturnsNewIfNotFound;
		private CountryEntity _countryEntity;
		private bool	_alwaysFetchCountryEntity, _alreadyFetchedCountryEntity, _countryEntityReturnsNewIfNotFound;
		private CurrencyEntity _currencyEntity;
		private bool	_alwaysFetchCurrencyEntity, _alreadyFetchedCurrencyEntity, _currencyEntityReturnsNewIfNotFound;
		private TimeZoneEntity _timeZoneEntity;
		private bool	_alwaysFetchTimeZoneEntity, _alreadyFetchedTimeZoneEntity, _timeZoneEntityReturnsNewIfNotFound;
		private TimestampEntity _timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection, _timestampCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ActionButtonEntity</summary>
			public static readonly string ActionButtonEntity = "ActionButtonEntity";
			/// <summary>Member name CountryEntity</summary>
			public static readonly string CountryEntity = "CountryEntity";
			/// <summary>Member name CurrencyEntity</summary>
			public static readonly string CurrencyEntity = "CurrencyEntity";
			/// <summary>Member name TimeZoneEntity</summary>
			public static readonly string TimeZoneEntity = "TimeZoneEntity";
			/// <summary>Member name AccessCodePointOfInterestCollection</summary>
			public static readonly string AccessCodePointOfInterestCollection = "AccessCodePointOfInterestCollection";
			/// <summary>Member name BusinesshoursCollection</summary>
			public static readonly string BusinesshoursCollection = "BusinesshoursCollection";
			/// <summary>Member name CompanyManagementTaskCollection</summary>
			public static readonly string CompanyManagementTaskCollection = "CompanyManagementTaskCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name MapPointOfInterestCollection</summary>
			public static readonly string MapPointOfInterestCollection = "MapPointOfInterestCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name PointOfInterestAmenityCollection</summary>
			public static readonly string PointOfInterestAmenityCollection = "PointOfInterestAmenityCollection";
			/// <summary>Member name PointOfInterestLanguageCollection</summary>
			public static readonly string PointOfInterestLanguageCollection = "PointOfInterestLanguageCollection";
			/// <summary>Member name PointOfInterestVenueCategoryCollection</summary>
			public static readonly string PointOfInterestVenueCategoryCollection = "PointOfInterestVenueCategoryCollection";
			/// <summary>Member name ProductCollection</summary>
			public static readonly string ProductCollection = "ProductCollection";
			/// <summary>Member name UIModeCollection</summary>
			public static readonly string UIModeCollection = "UIModeCollection";
			/// <summary>Member name AdvertisementCollectionViaMedium</summary>
			public static readonly string AdvertisementCollectionViaMedium = "AdvertisementCollectionViaMedium";
			/// <summary>Member name AlterationCollectionViaMedium</summary>
			public static readonly string AlterationCollectionViaMedium = "AlterationCollectionViaMedium";
			/// <summary>Member name AlterationoptionCollectionViaMedium</summary>
			public static readonly string AlterationoptionCollectionViaMedium = "AlterationoptionCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium</summary>
			public static readonly string CategoryCollectionViaMedium = "CategoryCollectionViaMedium";
			/// <summary>Member name CategoryCollectionViaMedium_</summary>
			public static readonly string CategoryCollectionViaMedium_ = "CategoryCollectionViaMedium_";
			/// <summary>Member name CompanyCollectionViaMedium</summary>
			public static readonly string CompanyCollectionViaMedium = "CompanyCollectionViaMedium";
			/// <summary>Member name DeliverypointgroupCollectionViaMedium</summary>
			public static readonly string DeliverypointgroupCollectionViaMedium = "DeliverypointgroupCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium</summary>
			public static readonly string EntertainmentCollectionViaMedium = "EntertainmentCollectionViaMedium";
			/// <summary>Member name EntertainmentCollectionViaMedium_</summary>
			public static readonly string EntertainmentCollectionViaMedium_ = "EntertainmentCollectionViaMedium_";
			/// <summary>Member name EntertainmentcategoryCollectionViaMedium</summary>
			public static readonly string EntertainmentcategoryCollectionViaMedium = "EntertainmentcategoryCollectionViaMedium";
			/// <summary>Member name GenericcategoryCollectionViaMedium</summary>
			public static readonly string GenericcategoryCollectionViaMedium = "GenericcategoryCollectionViaMedium";
			/// <summary>Member name GenericproductCollectionViaMedium</summary>
			public static readonly string GenericproductCollectionViaMedium = "GenericproductCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium</summary>
			public static readonly string ProductCollectionViaMedium = "ProductCollectionViaMedium";
			/// <summary>Member name ProductCollectionViaMedium_</summary>
			public static readonly string ProductCollectionViaMedium_ = "ProductCollectionViaMedium_";
			/// <summary>Member name SurveyCollectionViaMedium</summary>
			public static readonly string SurveyCollectionViaMedium = "SurveyCollectionViaMedium";
			/// <summary>Member name SurveyPageCollectionViaMedium</summary>
			public static readonly string SurveyPageCollectionViaMedium = "SurveyPageCollectionViaMedium";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PointOfInterestEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PointOfInterestEntityBase() :base("PointOfInterestEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		protected PointOfInterestEntityBase(System.Int32 pointOfInterestId):base("PointOfInterestEntity")
		{
			InitClassFetch(pointOfInterestId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PointOfInterestEntityBase(System.Int32 pointOfInterestId, IPrefetchPath prefetchPathToUse): base("PointOfInterestEntity")
		{
			InitClassFetch(pointOfInterestId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="validator">The custom validator object for this PointOfInterestEntity</param>
		protected PointOfInterestEntityBase(System.Int32 pointOfInterestId, IValidator validator):base("PointOfInterestEntity")
		{
			InitClassFetch(pointOfInterestId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PointOfInterestEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accessCodePointOfInterestCollection = (Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection)info.GetValue("_accessCodePointOfInterestCollection", typeof(Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection));
			_alwaysFetchAccessCodePointOfInterestCollection = info.GetBoolean("_alwaysFetchAccessCodePointOfInterestCollection");
			_alreadyFetchedAccessCodePointOfInterestCollection = info.GetBoolean("_alreadyFetchedAccessCodePointOfInterestCollection");

			_businesshoursCollection = (Obymobi.Data.CollectionClasses.BusinesshoursCollection)info.GetValue("_businesshoursCollection", typeof(Obymobi.Data.CollectionClasses.BusinesshoursCollection));
			_alwaysFetchBusinesshoursCollection = info.GetBoolean("_alwaysFetchBusinesshoursCollection");
			_alreadyFetchedBusinesshoursCollection = info.GetBoolean("_alreadyFetchedBusinesshoursCollection");

			_companyManagementTaskCollection = (Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection)info.GetValue("_companyManagementTaskCollection", typeof(Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection));
			_alwaysFetchCompanyManagementTaskCollection = info.GetBoolean("_alwaysFetchCompanyManagementTaskCollection");
			_alreadyFetchedCompanyManagementTaskCollection = info.GetBoolean("_alreadyFetchedCompanyManagementTaskCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_mapPointOfInterestCollection = (Obymobi.Data.CollectionClasses.MapPointOfInterestCollection)info.GetValue("_mapPointOfInterestCollection", typeof(Obymobi.Data.CollectionClasses.MapPointOfInterestCollection));
			_alwaysFetchMapPointOfInterestCollection = info.GetBoolean("_alwaysFetchMapPointOfInterestCollection");
			_alreadyFetchedMapPointOfInterestCollection = info.GetBoolean("_alreadyFetchedMapPointOfInterestCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_pointOfInterestAmenityCollection = (Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection)info.GetValue("_pointOfInterestAmenityCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection));
			_alwaysFetchPointOfInterestAmenityCollection = info.GetBoolean("_alwaysFetchPointOfInterestAmenityCollection");
			_alreadyFetchedPointOfInterestAmenityCollection = info.GetBoolean("_alreadyFetchedPointOfInterestAmenityCollection");

			_pointOfInterestLanguageCollection = (Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection)info.GetValue("_pointOfInterestLanguageCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection));
			_alwaysFetchPointOfInterestLanguageCollection = info.GetBoolean("_alwaysFetchPointOfInterestLanguageCollection");
			_alreadyFetchedPointOfInterestLanguageCollection = info.GetBoolean("_alreadyFetchedPointOfInterestLanguageCollection");

			_pointOfInterestVenueCategoryCollection = (Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection)info.GetValue("_pointOfInterestVenueCategoryCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection));
			_alwaysFetchPointOfInterestVenueCategoryCollection = info.GetBoolean("_alwaysFetchPointOfInterestVenueCategoryCollection");
			_alreadyFetchedPointOfInterestVenueCategoryCollection = info.GetBoolean("_alreadyFetchedPointOfInterestVenueCategoryCollection");

			_productCollection = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollection", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollection = info.GetBoolean("_alwaysFetchProductCollection");
			_alreadyFetchedProductCollection = info.GetBoolean("_alreadyFetchedProductCollection");

			_uIModeCollection = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollection", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollection = info.GetBoolean("_alwaysFetchUIModeCollection");
			_alreadyFetchedUIModeCollection = info.GetBoolean("_alreadyFetchedUIModeCollection");
			_advertisementCollectionViaMedium = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollectionViaMedium = info.GetBoolean("_alwaysFetchAdvertisementCollectionViaMedium");
			_alreadyFetchedAdvertisementCollectionViaMedium = info.GetBoolean("_alreadyFetchedAdvertisementCollectionViaMedium");

			_alterationCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationCollection)info.GetValue("_alterationCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationCollection));
			_alwaysFetchAlterationCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationCollectionViaMedium");
			_alreadyFetchedAlterationCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationCollectionViaMedium");

			_alterationoptionCollectionViaMedium = (Obymobi.Data.CollectionClasses.AlterationoptionCollection)info.GetValue("_alterationoptionCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.AlterationoptionCollection));
			_alwaysFetchAlterationoptionCollectionViaMedium = info.GetBoolean("_alwaysFetchAlterationoptionCollectionViaMedium");
			_alreadyFetchedAlterationoptionCollectionViaMedium = info.GetBoolean("_alreadyFetchedAlterationoptionCollectionViaMedium");

			_categoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium");
			_alreadyFetchedCategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium");

			_categoryCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaMedium_ = info.GetBoolean("_alwaysFetchCategoryCollectionViaMedium_");
			_alreadyFetchedCategoryCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedCategoryCollectionViaMedium_");

			_companyCollectionViaMedium = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaMedium = info.GetBoolean("_alwaysFetchCompanyCollectionViaMedium");
			_alreadyFetchedCompanyCollectionViaMedium = info.GetBoolean("_alreadyFetchedCompanyCollectionViaMedium");

			_deliverypointgroupCollectionViaMedium = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaMedium");
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaMedium");

			_entertainmentCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium");
			_alreadyFetchedEntertainmentCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium");

			_entertainmentCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaMedium_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaMedium_");
			_alreadyFetchedEntertainmentCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaMedium_");

			_entertainmentcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaMedium");
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaMedium");

			_genericcategoryCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericcategoryCollectionViaMedium");
			_alreadyFetchedGenericcategoryCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericcategoryCollectionViaMedium");

			_genericproductCollectionViaMedium = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollectionViaMedium = info.GetBoolean("_alwaysFetchGenericproductCollectionViaMedium");
			_alreadyFetchedGenericproductCollectionViaMedium = info.GetBoolean("_alreadyFetchedGenericproductCollectionViaMedium");

			_productCollectionViaMedium = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium = info.GetBoolean("_alwaysFetchProductCollectionViaMedium");
			_alreadyFetchedProductCollectionViaMedium = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium");

			_productCollectionViaMedium_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaMedium_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaMedium_ = info.GetBoolean("_alwaysFetchProductCollectionViaMedium_");
			_alreadyFetchedProductCollectionViaMedium_ = info.GetBoolean("_alreadyFetchedProductCollectionViaMedium_");

			_surveyCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyCollection)info.GetValue("_surveyCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyCollection));
			_alwaysFetchSurveyCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyCollectionViaMedium");
			_alreadyFetchedSurveyCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyCollectionViaMedium");

			_surveyPageCollectionViaMedium = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaMedium", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaMedium = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaMedium");
			_alreadyFetchedSurveyPageCollectionViaMedium = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaMedium");
			_actionButtonEntity = (ActionButtonEntity)info.GetValue("_actionButtonEntity", typeof(ActionButtonEntity));
			if(_actionButtonEntity!=null)
			{
				_actionButtonEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_actionButtonEntityReturnsNewIfNotFound = info.GetBoolean("_actionButtonEntityReturnsNewIfNotFound");
			_alwaysFetchActionButtonEntity = info.GetBoolean("_alwaysFetchActionButtonEntity");
			_alreadyFetchedActionButtonEntity = info.GetBoolean("_alreadyFetchedActionButtonEntity");

			_countryEntity = (CountryEntity)info.GetValue("_countryEntity", typeof(CountryEntity));
			if(_countryEntity!=null)
			{
				_countryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_countryEntityReturnsNewIfNotFound = info.GetBoolean("_countryEntityReturnsNewIfNotFound");
			_alwaysFetchCountryEntity = info.GetBoolean("_alwaysFetchCountryEntity");
			_alreadyFetchedCountryEntity = info.GetBoolean("_alreadyFetchedCountryEntity");

			_currencyEntity = (CurrencyEntity)info.GetValue("_currencyEntity", typeof(CurrencyEntity));
			if(_currencyEntity!=null)
			{
				_currencyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_currencyEntityReturnsNewIfNotFound = info.GetBoolean("_currencyEntityReturnsNewIfNotFound");
			_alwaysFetchCurrencyEntity = info.GetBoolean("_alwaysFetchCurrencyEntity");
			_alreadyFetchedCurrencyEntity = info.GetBoolean("_alreadyFetchedCurrencyEntity");

			_timeZoneEntity = (TimeZoneEntity)info.GetValue("_timeZoneEntity", typeof(TimeZoneEntity));
			if(_timeZoneEntity!=null)
			{
				_timeZoneEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timeZoneEntityReturnsNewIfNotFound = info.GetBoolean("_timeZoneEntityReturnsNewIfNotFound");
			_alwaysFetchTimeZoneEntity = info.GetBoolean("_alwaysFetchTimeZoneEntity");
			_alreadyFetchedTimeZoneEntity = info.GetBoolean("_alreadyFetchedTimeZoneEntity");
			_timestampCollection = (TimestampEntity)info.GetValue("_timestampCollection", typeof(TimestampEntity));
			if(_timestampCollection!=null)
			{
				_timestampCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timestampCollectionReturnsNewIfNotFound = info.GetBoolean("_timestampCollectionReturnsNewIfNotFound");
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PointOfInterestFieldIndex)fieldIndex)
			{
				case PointOfInterestFieldIndex.CountryId:
					DesetupSyncCountryEntity(true, false);
					_alreadyFetchedCountryEntity = false;
					break;
				case PointOfInterestFieldIndex.ActionButtonId:
					DesetupSyncActionButtonEntity(true, false);
					_alreadyFetchedActionButtonEntity = false;
					break;
				case PointOfInterestFieldIndex.CurrencyId:
					DesetupSyncCurrencyEntity(true, false);
					_alreadyFetchedCurrencyEntity = false;
					break;
				case PointOfInterestFieldIndex.TimeZoneId:
					DesetupSyncTimeZoneEntity(true, false);
					_alreadyFetchedTimeZoneEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccessCodePointOfInterestCollection = (_accessCodePointOfInterestCollection.Count > 0);
			_alreadyFetchedBusinesshoursCollection = (_businesshoursCollection.Count > 0);
			_alreadyFetchedCompanyManagementTaskCollection = (_companyManagementTaskCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedMapPointOfInterestCollection = (_mapPointOfInterestCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedPointOfInterestAmenityCollection = (_pointOfInterestAmenityCollection.Count > 0);
			_alreadyFetchedPointOfInterestLanguageCollection = (_pointOfInterestLanguageCollection.Count > 0);
			_alreadyFetchedPointOfInterestVenueCategoryCollection = (_pointOfInterestVenueCategoryCollection.Count > 0);
			_alreadyFetchedProductCollection = (_productCollection.Count > 0);
			_alreadyFetchedUIModeCollection = (_uIModeCollection.Count > 0);
			_alreadyFetchedAdvertisementCollectionViaMedium = (_advertisementCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationCollectionViaMedium = (_alterationCollectionViaMedium.Count > 0);
			_alreadyFetchedAlterationoptionCollectionViaMedium = (_alterationoptionCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium = (_categoryCollectionViaMedium.Count > 0);
			_alreadyFetchedCategoryCollectionViaMedium_ = (_categoryCollectionViaMedium_.Count > 0);
			_alreadyFetchedCompanyCollectionViaMedium = (_companyCollectionViaMedium.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaMedium = (_deliverypointgroupCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium = (_entertainmentCollectionViaMedium.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaMedium_ = (_entertainmentCollectionViaMedium_.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaMedium = (_entertainmentcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericcategoryCollectionViaMedium = (_genericcategoryCollectionViaMedium.Count > 0);
			_alreadyFetchedGenericproductCollectionViaMedium = (_genericproductCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium = (_productCollectionViaMedium.Count > 0);
			_alreadyFetchedProductCollectionViaMedium_ = (_productCollectionViaMedium_.Count > 0);
			_alreadyFetchedSurveyCollectionViaMedium = (_surveyCollectionViaMedium.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaMedium = (_surveyPageCollectionViaMedium.Count > 0);
			_alreadyFetchedActionButtonEntity = (_actionButtonEntity != null);
			_alreadyFetchedCountryEntity = (_countryEntity != null);
			_alreadyFetchedCurrencyEntity = (_currencyEntity != null);
			_alreadyFetchedTimeZoneEntity = (_timeZoneEntity != null);
			_alreadyFetchedTimestampCollection = (_timestampCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ActionButtonEntity":
					toReturn.Add(Relations.ActionButtonEntityUsingActionButtonId);
					break;
				case "CountryEntity":
					toReturn.Add(Relations.CountryEntityUsingCountryId);
					break;
				case "CurrencyEntity":
					toReturn.Add(Relations.CurrencyEntityUsingCurrencyId);
					break;
				case "TimeZoneEntity":
					toReturn.Add(Relations.TimeZoneEntityUsingTimeZoneId);
					break;
				case "AccessCodePointOfInterestCollection":
					toReturn.Add(Relations.AccessCodePointOfInterestEntityUsingPointOfInterestId);
					break;
				case "BusinesshoursCollection":
					toReturn.Add(Relations.BusinesshoursEntityUsingPointOfInterestId);
					break;
				case "CompanyManagementTaskCollection":
					toReturn.Add(Relations.CompanyManagementTaskEntityUsingPointOfInterestId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingPointOfInterestId);
					break;
				case "MapPointOfInterestCollection":
					toReturn.Add(Relations.MapPointOfInterestEntityUsingPointOfInterestId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId);
					break;
				case "PointOfInterestAmenityCollection":
					toReturn.Add(Relations.PointOfInterestAmenityEntityUsingPointOfInterestId);
					break;
				case "PointOfInterestLanguageCollection":
					toReturn.Add(Relations.PointOfInterestLanguageEntityUsingPointOfInterestId);
					break;
				case "PointOfInterestVenueCategoryCollection":
					toReturn.Add(Relations.PointOfInterestVenueCategoryEntityUsingPointOfInterestId);
					break;
				case "ProductCollection":
					toReturn.Add(Relations.ProductEntityUsingPointOfInterestId);
					break;
				case "UIModeCollection":
					toReturn.Add(Relations.UIModeEntityUsingPointOfInterestId);
					break;
				case "AdvertisementCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, "Media_", string.Empty, JoinHint.None);
					break;
				case "AlterationoptionCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingActionCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CategoryEntityUsingCategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.CompanyEntityUsingCompanyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, "Media_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericcategoryCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericcategoryEntityUsingGenericcategoryId, "Media_", string.Empty, JoinHint.None);
					break;
				case "GenericproductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.GenericproductEntityUsingGenericproductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingActionProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaMedium_":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.ProductEntityUsingProductId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, "Media_", string.Empty, JoinHint.None);
					break;
				case "SurveyPageCollectionViaMedium":
					toReturn.Add(Relations.MediaEntityUsingPointOfInterestId, "PointOfInterestEntity__", "Media_", JoinHint.None);
					toReturn.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, "Media_", string.Empty, JoinHint.None);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingPointOfInterestId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accessCodePointOfInterestCollection", (!this.MarkedForDeletion?_accessCodePointOfInterestCollection:null));
			info.AddValue("_alwaysFetchAccessCodePointOfInterestCollection", _alwaysFetchAccessCodePointOfInterestCollection);
			info.AddValue("_alreadyFetchedAccessCodePointOfInterestCollection", _alreadyFetchedAccessCodePointOfInterestCollection);
			info.AddValue("_businesshoursCollection", (!this.MarkedForDeletion?_businesshoursCollection:null));
			info.AddValue("_alwaysFetchBusinesshoursCollection", _alwaysFetchBusinesshoursCollection);
			info.AddValue("_alreadyFetchedBusinesshoursCollection", _alreadyFetchedBusinesshoursCollection);
			info.AddValue("_companyManagementTaskCollection", (!this.MarkedForDeletion?_companyManagementTaskCollection:null));
			info.AddValue("_alwaysFetchCompanyManagementTaskCollection", _alwaysFetchCompanyManagementTaskCollection);
			info.AddValue("_alreadyFetchedCompanyManagementTaskCollection", _alreadyFetchedCompanyManagementTaskCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_mapPointOfInterestCollection", (!this.MarkedForDeletion?_mapPointOfInterestCollection:null));
			info.AddValue("_alwaysFetchMapPointOfInterestCollection", _alwaysFetchMapPointOfInterestCollection);
			info.AddValue("_alreadyFetchedMapPointOfInterestCollection", _alreadyFetchedMapPointOfInterestCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_pointOfInterestAmenityCollection", (!this.MarkedForDeletion?_pointOfInterestAmenityCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestAmenityCollection", _alwaysFetchPointOfInterestAmenityCollection);
			info.AddValue("_alreadyFetchedPointOfInterestAmenityCollection", _alreadyFetchedPointOfInterestAmenityCollection);
			info.AddValue("_pointOfInterestLanguageCollection", (!this.MarkedForDeletion?_pointOfInterestLanguageCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestLanguageCollection", _alwaysFetchPointOfInterestLanguageCollection);
			info.AddValue("_alreadyFetchedPointOfInterestLanguageCollection", _alreadyFetchedPointOfInterestLanguageCollection);
			info.AddValue("_pointOfInterestVenueCategoryCollection", (!this.MarkedForDeletion?_pointOfInterestVenueCategoryCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestVenueCategoryCollection", _alwaysFetchPointOfInterestVenueCategoryCollection);
			info.AddValue("_alreadyFetchedPointOfInterestVenueCategoryCollection", _alreadyFetchedPointOfInterestVenueCategoryCollection);
			info.AddValue("_productCollection", (!this.MarkedForDeletion?_productCollection:null));
			info.AddValue("_alwaysFetchProductCollection", _alwaysFetchProductCollection);
			info.AddValue("_alreadyFetchedProductCollection", _alreadyFetchedProductCollection);
			info.AddValue("_uIModeCollection", (!this.MarkedForDeletion?_uIModeCollection:null));
			info.AddValue("_alwaysFetchUIModeCollection", _alwaysFetchUIModeCollection);
			info.AddValue("_alreadyFetchedUIModeCollection", _alreadyFetchedUIModeCollection);
			info.AddValue("_advertisementCollectionViaMedium", (!this.MarkedForDeletion?_advertisementCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAdvertisementCollectionViaMedium", _alwaysFetchAdvertisementCollectionViaMedium);
			info.AddValue("_alreadyFetchedAdvertisementCollectionViaMedium", _alreadyFetchedAdvertisementCollectionViaMedium);
			info.AddValue("_alterationCollectionViaMedium", (!this.MarkedForDeletion?_alterationCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationCollectionViaMedium", _alwaysFetchAlterationCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationCollectionViaMedium", _alreadyFetchedAlterationCollectionViaMedium);
			info.AddValue("_alterationoptionCollectionViaMedium", (!this.MarkedForDeletion?_alterationoptionCollectionViaMedium:null));
			info.AddValue("_alwaysFetchAlterationoptionCollectionViaMedium", _alwaysFetchAlterationoptionCollectionViaMedium);
			info.AddValue("_alreadyFetchedAlterationoptionCollectionViaMedium", _alreadyFetchedAlterationoptionCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium", (!this.MarkedForDeletion?_categoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium", _alwaysFetchCategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium", _alreadyFetchedCategoryCollectionViaMedium);
			info.AddValue("_categoryCollectionViaMedium_", (!this.MarkedForDeletion?_categoryCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaMedium_", _alwaysFetchCategoryCollectionViaMedium_);
			info.AddValue("_alreadyFetchedCategoryCollectionViaMedium_", _alreadyFetchedCategoryCollectionViaMedium_);
			info.AddValue("_companyCollectionViaMedium", (!this.MarkedForDeletion?_companyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaMedium", _alwaysFetchCompanyCollectionViaMedium);
			info.AddValue("_alreadyFetchedCompanyCollectionViaMedium", _alreadyFetchedCompanyCollectionViaMedium);
			info.AddValue("_deliverypointgroupCollectionViaMedium", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaMedium:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaMedium", _alwaysFetchDeliverypointgroupCollectionViaMedium);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaMedium", _alreadyFetchedDeliverypointgroupCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium", _alwaysFetchEntertainmentCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium", _alreadyFetchedEntertainmentCollectionViaMedium);
			info.AddValue("_entertainmentCollectionViaMedium_", (!this.MarkedForDeletion?_entertainmentCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaMedium_", _alwaysFetchEntertainmentCollectionViaMedium_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaMedium_", _alreadyFetchedEntertainmentCollectionViaMedium_);
			info.AddValue("_entertainmentcategoryCollectionViaMedium", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaMedium", _alwaysFetchEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaMedium", _alreadyFetchedEntertainmentcategoryCollectionViaMedium);
			info.AddValue("_genericcategoryCollectionViaMedium", (!this.MarkedForDeletion?_genericcategoryCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericcategoryCollectionViaMedium", _alwaysFetchGenericcategoryCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericcategoryCollectionViaMedium", _alreadyFetchedGenericcategoryCollectionViaMedium);
			info.AddValue("_genericproductCollectionViaMedium", (!this.MarkedForDeletion?_genericproductCollectionViaMedium:null));
			info.AddValue("_alwaysFetchGenericproductCollectionViaMedium", _alwaysFetchGenericproductCollectionViaMedium);
			info.AddValue("_alreadyFetchedGenericproductCollectionViaMedium", _alreadyFetchedGenericproductCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium", (!this.MarkedForDeletion?_productCollectionViaMedium:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium", _alwaysFetchProductCollectionViaMedium);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium", _alreadyFetchedProductCollectionViaMedium);
			info.AddValue("_productCollectionViaMedium_", (!this.MarkedForDeletion?_productCollectionViaMedium_:null));
			info.AddValue("_alwaysFetchProductCollectionViaMedium_", _alwaysFetchProductCollectionViaMedium_);
			info.AddValue("_alreadyFetchedProductCollectionViaMedium_", _alreadyFetchedProductCollectionViaMedium_);
			info.AddValue("_surveyCollectionViaMedium", (!this.MarkedForDeletion?_surveyCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyCollectionViaMedium", _alwaysFetchSurveyCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyCollectionViaMedium", _alreadyFetchedSurveyCollectionViaMedium);
			info.AddValue("_surveyPageCollectionViaMedium", (!this.MarkedForDeletion?_surveyPageCollectionViaMedium:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaMedium", _alwaysFetchSurveyPageCollectionViaMedium);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaMedium", _alreadyFetchedSurveyPageCollectionViaMedium);
			info.AddValue("_actionButtonEntity", (!this.MarkedForDeletion?_actionButtonEntity:null));
			info.AddValue("_actionButtonEntityReturnsNewIfNotFound", _actionButtonEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchActionButtonEntity", _alwaysFetchActionButtonEntity);
			info.AddValue("_alreadyFetchedActionButtonEntity", _alreadyFetchedActionButtonEntity);
			info.AddValue("_countryEntity", (!this.MarkedForDeletion?_countryEntity:null));
			info.AddValue("_countryEntityReturnsNewIfNotFound", _countryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCountryEntity", _alwaysFetchCountryEntity);
			info.AddValue("_alreadyFetchedCountryEntity", _alreadyFetchedCountryEntity);
			info.AddValue("_currencyEntity", (!this.MarkedForDeletion?_currencyEntity:null));
			info.AddValue("_currencyEntityReturnsNewIfNotFound", _currencyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCurrencyEntity", _alwaysFetchCurrencyEntity);
			info.AddValue("_alreadyFetchedCurrencyEntity", _alreadyFetchedCurrencyEntity);
			info.AddValue("_timeZoneEntity", (!this.MarkedForDeletion?_timeZoneEntity:null));
			info.AddValue("_timeZoneEntityReturnsNewIfNotFound", _timeZoneEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimeZoneEntity", _alwaysFetchTimeZoneEntity);
			info.AddValue("_alreadyFetchedTimeZoneEntity", _alreadyFetchedTimeZoneEntity);

			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_timestampCollectionReturnsNewIfNotFound", _timestampCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ActionButtonEntity":
					_alreadyFetchedActionButtonEntity = true;
					this.ActionButtonEntity = (ActionButtonEntity)entity;
					break;
				case "CountryEntity":
					_alreadyFetchedCountryEntity = true;
					this.CountryEntity = (CountryEntity)entity;
					break;
				case "CurrencyEntity":
					_alreadyFetchedCurrencyEntity = true;
					this.CurrencyEntity = (CurrencyEntity)entity;
					break;
				case "TimeZoneEntity":
					_alreadyFetchedTimeZoneEntity = true;
					this.TimeZoneEntity = (TimeZoneEntity)entity;
					break;
				case "AccessCodePointOfInterestCollection":
					_alreadyFetchedAccessCodePointOfInterestCollection = true;
					if(entity!=null)
					{
						this.AccessCodePointOfInterestCollection.Add((AccessCodePointOfInterestEntity)entity);
					}
					break;
				case "BusinesshoursCollection":
					_alreadyFetchedBusinesshoursCollection = true;
					if(entity!=null)
					{
						this.BusinesshoursCollection.Add((BusinesshoursEntity)entity);
					}
					break;
				case "CompanyManagementTaskCollection":
					_alreadyFetchedCompanyManagementTaskCollection = true;
					if(entity!=null)
					{
						this.CompanyManagementTaskCollection.Add((CompanyManagementTaskEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "MapPointOfInterestCollection":
					_alreadyFetchedMapPointOfInterestCollection = true;
					if(entity!=null)
					{
						this.MapPointOfInterestCollection.Add((MapPointOfInterestEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "PointOfInterestAmenityCollection":
					_alreadyFetchedPointOfInterestAmenityCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestAmenityCollection.Add((PointOfInterestAmenityEntity)entity);
					}
					break;
				case "PointOfInterestLanguageCollection":
					_alreadyFetchedPointOfInterestLanguageCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestLanguageCollection.Add((PointOfInterestLanguageEntity)entity);
					}
					break;
				case "PointOfInterestVenueCategoryCollection":
					_alreadyFetchedPointOfInterestVenueCategoryCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestVenueCategoryCollection.Add((PointOfInterestVenueCategoryEntity)entity);
					}
					break;
				case "ProductCollection":
					_alreadyFetchedProductCollection = true;
					if(entity!=null)
					{
						this.ProductCollection.Add((ProductEntity)entity);
					}
					break;
				case "UIModeCollection":
					_alreadyFetchedUIModeCollection = true;
					if(entity!=null)
					{
						this.UIModeCollection.Add((UIModeEntity)entity);
					}
					break;
				case "AdvertisementCollectionViaMedium":
					_alreadyFetchedAdvertisementCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AdvertisementCollectionViaMedium.Add((AdvertisementEntity)entity);
					}
					break;
				case "AlterationCollectionViaMedium":
					_alreadyFetchedAlterationCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationCollectionViaMedium.Add((AlterationEntity)entity);
					}
					break;
				case "AlterationoptionCollectionViaMedium":
					_alreadyFetchedAlterationoptionCollectionViaMedium = true;
					if(entity!=null)
					{
						this.AlterationoptionCollectionViaMedium.Add((AlterationoptionEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium":
					_alreadyFetchedCategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium.Add((CategoryEntity)entity);
					}
					break;
				case "CategoryCollectionViaMedium_":
					_alreadyFetchedCategoryCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaMedium_.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaMedium":
					_alreadyFetchedCompanyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaMedium.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaMedium":
					_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaMedium.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium":
					_alreadyFetchedEntertainmentCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaMedium_":
					_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaMedium_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaMedium":
					_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaMedium.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				case "GenericcategoryCollectionViaMedium":
					_alreadyFetchedGenericcategoryCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericcategoryCollectionViaMedium.Add((GenericcategoryEntity)entity);
					}
					break;
				case "GenericproductCollectionViaMedium":
					_alreadyFetchedGenericproductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.GenericproductCollectionViaMedium.Add((GenericproductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium":
					_alreadyFetchedProductCollectionViaMedium = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaMedium_":
					_alreadyFetchedProductCollectionViaMedium_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaMedium_.Add((ProductEntity)entity);
					}
					break;
				case "SurveyCollectionViaMedium":
					_alreadyFetchedSurveyCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyCollectionViaMedium.Add((SurveyEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaMedium":
					_alreadyFetchedSurveyPageCollectionViaMedium = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaMedium.Add((SurveyPageEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					this.TimestampCollection = (TimestampEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ActionButtonEntity":
					SetupSyncActionButtonEntity(relatedEntity);
					break;
				case "CountryEntity":
					SetupSyncCountryEntity(relatedEntity);
					break;
				case "CurrencyEntity":
					SetupSyncCurrencyEntity(relatedEntity);
					break;
				case "TimeZoneEntity":
					SetupSyncTimeZoneEntity(relatedEntity);
					break;
				case "AccessCodePointOfInterestCollection":
					_accessCodePointOfInterestCollection.Add((AccessCodePointOfInterestEntity)relatedEntity);
					break;
				case "BusinesshoursCollection":
					_businesshoursCollection.Add((BusinesshoursEntity)relatedEntity);
					break;
				case "CompanyManagementTaskCollection":
					_companyManagementTaskCollection.Add((CompanyManagementTaskEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "MapPointOfInterestCollection":
					_mapPointOfInterestCollection.Add((MapPointOfInterestEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "PointOfInterestAmenityCollection":
					_pointOfInterestAmenityCollection.Add((PointOfInterestAmenityEntity)relatedEntity);
					break;
				case "PointOfInterestLanguageCollection":
					_pointOfInterestLanguageCollection.Add((PointOfInterestLanguageEntity)relatedEntity);
					break;
				case "PointOfInterestVenueCategoryCollection":
					_pointOfInterestVenueCategoryCollection.Add((PointOfInterestVenueCategoryEntity)relatedEntity);
					break;
				case "ProductCollection":
					_productCollection.Add((ProductEntity)relatedEntity);
					break;
				case "UIModeCollection":
					_uIModeCollection.Add((UIModeEntity)relatedEntity);
					break;
				case "TimestampCollection":
					SetupSyncTimestampCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ActionButtonEntity":
					DesetupSyncActionButtonEntity(false, true);
					break;
				case "CountryEntity":
					DesetupSyncCountryEntity(false, true);
					break;
				case "CurrencyEntity":
					DesetupSyncCurrencyEntity(false, true);
					break;
				case "TimeZoneEntity":
					DesetupSyncTimeZoneEntity(false, true);
					break;
				case "AccessCodePointOfInterestCollection":
					this.PerformRelatedEntityRemoval(_accessCodePointOfInterestCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinesshoursCollection":
					this.PerformRelatedEntityRemoval(_businesshoursCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CompanyManagementTaskCollection":
					this.PerformRelatedEntityRemoval(_companyManagementTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MapPointOfInterestCollection":
					this.PerformRelatedEntityRemoval(_mapPointOfInterestCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestAmenityCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestAmenityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestLanguageCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestVenueCategoryCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestVenueCategoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCollection":
					this.PerformRelatedEntityRemoval(_productCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIModeCollection":
					this.PerformRelatedEntityRemoval(_uIModeCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					DesetupSyncTimestampCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_timestampCollection!=null)
			{
				toReturn.Add(_timestampCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_actionButtonEntity!=null)
			{
				toReturn.Add(_actionButtonEntity);
			}
			if(_countryEntity!=null)
			{
				toReturn.Add(_countryEntity);
			}
			if(_currencyEntity!=null)
			{
				toReturn.Add(_currencyEntity);
			}
			if(_timeZoneEntity!=null)
			{
				toReturn.Add(_timeZoneEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accessCodePointOfInterestCollection);
			toReturn.Add(_businesshoursCollection);
			toReturn.Add(_companyManagementTaskCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_mapPointOfInterestCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_pointOfInterestAmenityCollection);
			toReturn.Add(_pointOfInterestLanguageCollection);
			toReturn.Add(_pointOfInterestVenueCategoryCollection);
			toReturn.Add(_productCollection);
			toReturn.Add(_uIModeCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestId)
		{
			return FetchUsingPK(pointOfInterestId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pointOfInterestId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pointOfInterestId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 pointOfInterestId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pointOfInterestId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PointOfInterestId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PointOfInterestRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccessCodePointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch)
		{
			return GetMultiAccessCodePointOfInterestCollection(forceFetch, _accessCodePointOfInterestCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccessCodePointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccessCodePointOfInterestCollection(forceFetch, _accessCodePointOfInterestCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccessCodePointOfInterestCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection GetMultiAccessCodePointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccessCodePointOfInterestCollection || forceFetch || _alwaysFetchAccessCodePointOfInterestCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accessCodePointOfInterestCollection);
				_accessCodePointOfInterestCollection.SuppressClearInGetMulti=!forceFetch;
				_accessCodePointOfInterestCollection.EntityFactoryToUse = entityFactoryToUse;
				_accessCodePointOfInterestCollection.GetMultiManyToOne(null, this, filter);
				_accessCodePointOfInterestCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAccessCodePointOfInterestCollection = true;
			}
			return _accessCodePointOfInterestCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccessCodePointOfInterestCollection'. These settings will be taken into account
		/// when the property AccessCodePointOfInterestCollection is requested or GetMultiAccessCodePointOfInterestCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccessCodePointOfInterestCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accessCodePointOfInterestCollection.SortClauses=sortClauses;
			_accessCodePointOfInterestCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinesshoursEntity'</returns>
		public Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch)
		{
			return GetMultiBusinesshoursCollection(forceFetch, _businesshoursCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinesshoursEntity'</returns>
		public Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinesshoursCollection(forceFetch, _businesshoursCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinesshoursCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.BusinesshoursCollection GetMultiBusinesshoursCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinesshoursCollection || forceFetch || _alwaysFetchBusinesshoursCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businesshoursCollection);
				_businesshoursCollection.SuppressClearInGetMulti=!forceFetch;
				_businesshoursCollection.EntityFactoryToUse = entityFactoryToUse;
				_businesshoursCollection.GetMultiManyToOne(null, null, this, filter);
				_businesshoursCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinesshoursCollection = true;
			}
			return _businesshoursCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinesshoursCollection'. These settings will be taken into account
		/// when the property BusinesshoursCollection is requested or GetMultiBusinesshoursCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinesshoursCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businesshoursCollection.SortClauses=sortClauses;
			_businesshoursCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyManagementTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyManagementTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection GetMultiCompanyManagementTaskCollection(bool forceFetch)
		{
			return GetMultiCompanyManagementTaskCollection(forceFetch, _companyManagementTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyManagementTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyManagementTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection GetMultiCompanyManagementTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyManagementTaskCollection(forceFetch, _companyManagementTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyManagementTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection GetMultiCompanyManagementTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyManagementTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyManagementTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection GetMultiCompanyManagementTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyManagementTaskCollection || forceFetch || _alwaysFetchCompanyManagementTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyManagementTaskCollection);
				_companyManagementTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_companyManagementTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyManagementTaskCollection.GetMultiManyToOne(null, this, filter);
				_companyManagementTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyManagementTaskCollection = true;
			}
			return _companyManagementTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyManagementTaskCollection'. These settings will be taken into account
		/// when the property CompanyManagementTaskCollection is requested or GetMultiCompanyManagementTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyManagementTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyManagementTaskCollection.SortClauses=sortClauses;
			_companyManagementTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MapPointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MapPointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.MapPointOfInterestCollection GetMultiMapPointOfInterestCollection(bool forceFetch)
		{
			return GetMultiMapPointOfInterestCollection(forceFetch, _mapPointOfInterestCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MapPointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MapPointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.MapPointOfInterestCollection GetMultiMapPointOfInterestCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMapPointOfInterestCollection(forceFetch, _mapPointOfInterestCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MapPointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MapPointOfInterestCollection GetMultiMapPointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMapPointOfInterestCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MapPointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MapPointOfInterestCollection GetMultiMapPointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMapPointOfInterestCollection || forceFetch || _alwaysFetchMapPointOfInterestCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mapPointOfInterestCollection);
				_mapPointOfInterestCollection.SuppressClearInGetMulti=!forceFetch;
				_mapPointOfInterestCollection.EntityFactoryToUse = entityFactoryToUse;
				_mapPointOfInterestCollection.GetMultiManyToOne(null, this, filter);
				_mapPointOfInterestCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMapPointOfInterestCollection = true;
			}
			return _mapPointOfInterestCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MapPointOfInterestCollection'. These settings will be taken into account
		/// when the property MapPointOfInterestCollection is requested or GetMultiMapPointOfInterestCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMapPointOfInterestCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mapPointOfInterestCollection.SortClauses=sortClauses;
			_mapPointOfInterestCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestAmenityEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestAmenityCollection(forceFetch, _pointOfInterestAmenityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestAmenityEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestAmenityCollection(forceFetch, _pointOfInterestAmenityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestAmenityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestAmenityCollection || forceFetch || _alwaysFetchPointOfInterestAmenityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestAmenityCollection);
				_pointOfInterestAmenityCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestAmenityCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestAmenityCollection.GetMultiManyToOne(null, this, filter);
				_pointOfInterestAmenityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestAmenityCollection = true;
			}
			return _pointOfInterestAmenityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestAmenityCollection'. These settings will be taken into account
		/// when the property PointOfInterestAmenityCollection is requested or GetMultiPointOfInterestAmenityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestAmenityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestAmenityCollection.SortClauses=sortClauses;
			_pointOfInterestAmenityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestLanguageCollection(forceFetch, _pointOfInterestLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestLanguageCollection(forceFetch, _pointOfInterestLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection GetMultiPointOfInterestLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestLanguageCollection || forceFetch || _alwaysFetchPointOfInterestLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestLanguageCollection);
				_pointOfInterestLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestLanguageCollection.GetMultiManyToOne(null, this, filter);
				_pointOfInterestLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestLanguageCollection = true;
			}
			return _pointOfInterestLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestLanguageCollection'. These settings will be taken into account
		/// when the property PointOfInterestLanguageCollection is requested or GetMultiPointOfInterestLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestLanguageCollection.SortClauses=sortClauses;
			_pointOfInterestLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestVenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestVenueCategoryCollection(forceFetch, _pointOfInterestVenueCategoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestVenueCategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestVenueCategoryCollection(forceFetch, _pointOfInterestVenueCategoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestVenueCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection GetMultiPointOfInterestVenueCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestVenueCategoryCollection || forceFetch || _alwaysFetchPointOfInterestVenueCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestVenueCategoryCollection);
				_pointOfInterestVenueCategoryCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestVenueCategoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestVenueCategoryCollection.GetMultiManyToOne(this, null, filter);
				_pointOfInterestVenueCategoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestVenueCategoryCollection = true;
			}
			return _pointOfInterestVenueCategoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestVenueCategoryCollection'. These settings will be taken into account
		/// when the property PointOfInterestVenueCategoryCollection is requested or GetMultiPointOfInterestVenueCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestVenueCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestVenueCategoryCollection.SortClauses=sortClauses;
			_pointOfInterestVenueCategoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCollection(forceFetch, _productCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCollection || forceFetch || _alwaysFetchProductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollection);
				_productCollection.SuppressClearInGetMulti=!forceFetch;
				_productCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_productCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollection = true;
			}
			return _productCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollection'. These settings will be taken into account
		/// when the property ProductCollection is requested or GetMultiProductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollection.SortClauses=sortClauses;
			_productCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch)
		{
			return GetMultiUIModeCollection(forceFetch, _uIModeCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIModeCollection(forceFetch, _uIModeCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIModeCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIModeCollection || forceFetch || _alwaysFetchUIModeCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollection);
				_uIModeCollection.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollection.GetMultiManyToOne(null, this, null, filter);
				_uIModeCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollection = true;
			}
			return _uIModeCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollection'. These settings will be taken into account
		/// when the property UIModeCollection is requested or GetMultiUIModeCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollection.SortClauses=sortClauses;
			_uIModeCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAdvertisementCollectionViaMedium(forceFetch, _advertisementCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAdvertisementCollectionViaMedium || forceFetch || _alwaysFetchAdvertisementCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollectionViaMedium.GetMulti(filter, GetRelationsForField("AdvertisementCollectionViaMedium"));
				_advertisementCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollectionViaMedium = true;
			}
			return _advertisementCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollectionViaMedium'. These settings will be taken into account
		/// when the property AdvertisementCollectionViaMedium is requested or GetMultiAdvertisementCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollectionViaMedium.SortClauses=sortClauses;
			_advertisementCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationCollectionViaMedium(forceFetch, _alterationCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationCollection GetMultiAlterationCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationCollectionViaMedium || forceFetch || _alwaysFetchAlterationCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationCollectionViaMedium"));
				_alterationCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationCollectionViaMedium = true;
			}
			return _alterationCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationCollectionViaMedium is requested or GetMultiAlterationCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationCollectionViaMedium.SortClauses=sortClauses;
			_alterationCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AlterationoptionEntity'</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch)
		{
			return GetMultiAlterationoptionCollectionViaMedium(forceFetch, _alterationoptionCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AlterationoptionCollection GetMultiAlterationoptionCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAlterationoptionCollectionViaMedium || forceFetch || _alwaysFetchAlterationoptionCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_alterationoptionCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_alterationoptionCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_alterationoptionCollectionViaMedium.GetMulti(filter, GetRelationsForField("AlterationoptionCollectionViaMedium"));
				_alterationoptionCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedAlterationoptionCollectionViaMedium = true;
			}
			return _alterationoptionCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'AlterationoptionCollectionViaMedium'. These settings will be taken into account
		/// when the property AlterationoptionCollectionViaMedium is requested or GetMultiAlterationoptionCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAlterationoptionCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_alterationoptionCollectionViaMedium.SortClauses=sortClauses;
			_alterationoptionCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium(forceFetch, _categoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium || forceFetch || _alwaysFetchCategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium"));
				_categoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium = true;
			}
			return _categoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium is requested or GetMultiCategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium.SortClauses=sortClauses;
			_categoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaMedium_(forceFetch, _categoryCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaMedium_ || forceFetch || _alwaysFetchCategoryCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaMedium_.GetMulti(filter, GetRelationsForField("CategoryCollectionViaMedium_"));
				_categoryCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaMedium_ = true;
			}
			return _categoryCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaMedium_'. These settings will be taken into account
		/// when the property CategoryCollectionViaMedium_ is requested or GetMultiCategoryCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaMedium_.SortClauses=sortClauses;
			_categoryCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaMedium(forceFetch, _companyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaMedium || forceFetch || _alwaysFetchCompanyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaMedium.GetMulti(filter, GetRelationsForField("CompanyCollectionViaMedium"));
				_companyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaMedium = true;
			}
			return _companyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaMedium'. These settings will be taken into account
		/// when the property CompanyCollectionViaMedium is requested or GetMultiCompanyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaMedium.SortClauses=sortClauses;
			_companyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaMedium(forceFetch, _deliverypointgroupCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaMedium || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaMedium.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaMedium"));
				_deliverypointgroupCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = true;
			}
			return _deliverypointgroupCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaMedium'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaMedium is requested or GetMultiDeliverypointgroupCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaMedium.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium(forceFetch, _entertainmentCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium"));
				_entertainmentCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium = true;
			}
			return _entertainmentCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium is requested or GetMultiEntertainmentCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaMedium_(forceFetch, _entertainmentCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaMedium_ || forceFetch || _alwaysFetchEntertainmentCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaMedium_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaMedium_"));
				_entertainmentCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaMedium_ = true;
			}
			return _entertainmentCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaMedium_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaMedium_ is requested or GetMultiEntertainmentCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaMedium_.SortClauses=sortClauses;
			_entertainmentCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaMedium(forceFetch, _entertainmentcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaMedium || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"));
				_entertainmentcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = true;
			}
			return _entertainmentcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaMedium is requested or GetMultiEntertainmentcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaMedium.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericcategoryCollectionViaMedium(forceFetch, _genericcategoryCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollectionViaMedium || forceFetch || _alwaysFetchGenericcategoryCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericcategoryCollectionViaMedium"));
				_genericcategoryCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollectionViaMedium = true;
			}
			return _genericcategoryCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericcategoryCollectionViaMedium is requested or GetMultiGenericcategoryCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollectionViaMedium.SortClauses=sortClauses;
			_genericcategoryCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiGenericproductCollectionViaMedium(forceFetch, _genericproductCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericproductCollectionViaMedium || forceFetch || _alwaysFetchGenericproductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollectionViaMedium.GetMulti(filter, GetRelationsForField("GenericproductCollectionViaMedium"));
				_genericproductCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollectionViaMedium = true;
			}
			return _genericproductCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollectionViaMedium'. These settings will be taken into account
		/// when the property GenericproductCollectionViaMedium is requested or GetMultiGenericproductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollectionViaMedium.SortClauses=sortClauses;
			_genericproductCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium(forceFetch, _productCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium || forceFetch || _alwaysFetchProductCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_productCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium"));
				_productCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium = true;
			}
			return _productCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium is requested or GetMultiProductCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium.SortClauses=sortClauses;
			_productCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch)
		{
			return GetMultiProductCollectionViaMedium_(forceFetch, _productCollectionViaMedium_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaMedium_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaMedium_ || forceFetch || _alwaysFetchProductCollectionViaMedium_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaMedium_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaMedium_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaMedium_.GetMulti(filter, GetRelationsForField("ProductCollectionViaMedium_"));
				_productCollectionViaMedium_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaMedium_ = true;
			}
			return _productCollectionViaMedium_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaMedium_'. These settings will be taken into account
		/// when the property ProductCollectionViaMedium_ is requested or GetMultiProductCollectionViaMedium_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaMedium_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaMedium_.SortClauses=sortClauses;
			_productCollectionViaMedium_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyCollectionViaMedium(forceFetch, _surveyCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyCollection GetMultiSurveyCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyCollectionViaMedium || forceFetch || _alwaysFetchSurveyCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyCollectionViaMedium"));
				_surveyCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyCollectionViaMedium = true;
			}
			return _surveyCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyCollectionViaMedium is requested or GetMultiSurveyCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyCollectionViaMedium.SortClauses=sortClauses;
			_surveyCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaMedium(forceFetch, _surveyPageCollectionViaMedium.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaMedium(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaMedium || forceFetch || _alwaysFetchSurveyPageCollectionViaMedium) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaMedium);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PointOfInterestFields.PointOfInterestId, ComparisonOperator.Equal, this.PointOfInterestId, "PointOfInterestEntity__"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaMedium.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaMedium.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaMedium"));
				_surveyPageCollectionViaMedium.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaMedium = true;
			}
			return _surveyPageCollectionViaMedium;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaMedium'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaMedium is requested or GetMultiSurveyPageCollectionViaMedium is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaMedium(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaMedium.SortClauses=sortClauses;
			_surveyPageCollectionViaMedium.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ActionButtonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ActionButtonEntity' which is related to this entity.</returns>
		public ActionButtonEntity GetSingleActionButtonEntity()
		{
			return GetSingleActionButtonEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ActionButtonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ActionButtonEntity' which is related to this entity.</returns>
		public virtual ActionButtonEntity GetSingleActionButtonEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedActionButtonEntity || forceFetch || _alwaysFetchActionButtonEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ActionButtonEntityUsingActionButtonId);
				ActionButtonEntity newEntity = new ActionButtonEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionButtonId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ActionButtonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_actionButtonEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ActionButtonEntity = newEntity;
				_alreadyFetchedActionButtonEntity = fetchResult;
			}
			return _actionButtonEntity;
		}


		/// <summary> Retrieves the related entity of type 'CountryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CountryEntity' which is related to this entity.</returns>
		public CountryEntity GetSingleCountryEntity()
		{
			return GetSingleCountryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CountryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CountryEntity' which is related to this entity.</returns>
		public virtual CountryEntity GetSingleCountryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCountryEntity || forceFetch || _alwaysFetchCountryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CountryEntityUsingCountryId);
				CountryEntity newEntity = new CountryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CountryId);
				}
				if(fetchResult)
				{
					newEntity = (CountryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_countryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CountryEntity = newEntity;
				_alreadyFetchedCountryEntity = fetchResult;
			}
			return _countryEntity;
		}


		/// <summary> Retrieves the related entity of type 'CurrencyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CurrencyEntity' which is related to this entity.</returns>
		public CurrencyEntity GetSingleCurrencyEntity()
		{
			return GetSingleCurrencyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CurrencyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CurrencyEntity' which is related to this entity.</returns>
		public virtual CurrencyEntity GetSingleCurrencyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCurrencyEntity || forceFetch || _alwaysFetchCurrencyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CurrencyEntityUsingCurrencyId);
				CurrencyEntity newEntity = new CurrencyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CurrencyId);
				}
				if(fetchResult)
				{
					newEntity = (CurrencyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_currencyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CurrencyEntity = newEntity;
				_alreadyFetchedCurrencyEntity = fetchResult;
			}
			return _currencyEntity;
		}


		/// <summary> Retrieves the related entity of type 'TimeZoneEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TimeZoneEntity' which is related to this entity.</returns>
		public TimeZoneEntity GetSingleTimeZoneEntity()
		{
			return GetSingleTimeZoneEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TimeZoneEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimeZoneEntity' which is related to this entity.</returns>
		public virtual TimeZoneEntity GetSingleTimeZoneEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimeZoneEntity || forceFetch || _alwaysFetchTimeZoneEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimeZoneEntityUsingTimeZoneId);
				TimeZoneEntity newEntity = new TimeZoneEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TimeZoneId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TimeZoneEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timeZoneEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimeZoneEntity = newEntity;
				_alreadyFetchedTimeZoneEntity = fetchResult;
			}
			return _timeZoneEntity;
		}

		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public TimestampEntity GetSingleTimestampCollection()
		{
			return GetSingleTimestampCollection(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public virtual TimestampEntity GetSingleTimestampCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimestampEntityUsingPointOfInterestId);
				TimestampEntity newEntity = new TimestampEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCPointOfInterestId(this.PointOfInterestId);
				}
				if(fetchResult)
				{
					newEntity = (TimestampEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timestampCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimestampCollection = newEntity;
				_alreadyFetchedTimestampCollection = fetchResult;
			}
			return _timestampCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ActionButtonEntity", _actionButtonEntity);
			toReturn.Add("CountryEntity", _countryEntity);
			toReturn.Add("CurrencyEntity", _currencyEntity);
			toReturn.Add("TimeZoneEntity", _timeZoneEntity);
			toReturn.Add("AccessCodePointOfInterestCollection", _accessCodePointOfInterestCollection);
			toReturn.Add("BusinesshoursCollection", _businesshoursCollection);
			toReturn.Add("CompanyManagementTaskCollection", _companyManagementTaskCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("MapPointOfInterestCollection", _mapPointOfInterestCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("PointOfInterestAmenityCollection", _pointOfInterestAmenityCollection);
			toReturn.Add("PointOfInterestLanguageCollection", _pointOfInterestLanguageCollection);
			toReturn.Add("PointOfInterestVenueCategoryCollection", _pointOfInterestVenueCategoryCollection);
			toReturn.Add("ProductCollection", _productCollection);
			toReturn.Add("UIModeCollection", _uIModeCollection);
			toReturn.Add("AdvertisementCollectionViaMedium", _advertisementCollectionViaMedium);
			toReturn.Add("AlterationCollectionViaMedium", _alterationCollectionViaMedium);
			toReturn.Add("AlterationoptionCollectionViaMedium", _alterationoptionCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium", _categoryCollectionViaMedium);
			toReturn.Add("CategoryCollectionViaMedium_", _categoryCollectionViaMedium_);
			toReturn.Add("CompanyCollectionViaMedium", _companyCollectionViaMedium);
			toReturn.Add("DeliverypointgroupCollectionViaMedium", _deliverypointgroupCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium", _entertainmentCollectionViaMedium);
			toReturn.Add("EntertainmentCollectionViaMedium_", _entertainmentCollectionViaMedium_);
			toReturn.Add("EntertainmentcategoryCollectionViaMedium", _entertainmentcategoryCollectionViaMedium);
			toReturn.Add("GenericcategoryCollectionViaMedium", _genericcategoryCollectionViaMedium);
			toReturn.Add("GenericproductCollectionViaMedium", _genericproductCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium", _productCollectionViaMedium);
			toReturn.Add("ProductCollectionViaMedium_", _productCollectionViaMedium_);
			toReturn.Add("SurveyCollectionViaMedium", _surveyCollectionViaMedium);
			toReturn.Add("SurveyPageCollectionViaMedium", _surveyPageCollectionViaMedium);
			toReturn.Add("TimestampCollection", _timestampCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="validator">The validator object for this PointOfInterestEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 pointOfInterestId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pointOfInterestId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accessCodePointOfInterestCollection = new Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection();
			_accessCodePointOfInterestCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_businesshoursCollection = new Obymobi.Data.CollectionClasses.BusinesshoursCollection();
			_businesshoursCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_companyManagementTaskCollection = new Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection();
			_companyManagementTaskCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_mapPointOfInterestCollection = new Obymobi.Data.CollectionClasses.MapPointOfInterestCollection();
			_mapPointOfInterestCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_pointOfInterestAmenityCollection = new Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection();
			_pointOfInterestAmenityCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_pointOfInterestLanguageCollection = new Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection();
			_pointOfInterestLanguageCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_pointOfInterestVenueCategoryCollection = new Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection();
			_pointOfInterestVenueCategoryCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_productCollection = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");

			_uIModeCollection = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollection.SetContainingEntityInfo(this, "PointOfInterestEntity");
			_advertisementCollectionViaMedium = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_alterationCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationCollection();
			_alterationoptionCollectionViaMedium = new Obymobi.Data.CollectionClasses.AlterationoptionCollection();
			_categoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaMedium = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointgroupCollectionViaMedium = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			_genericcategoryCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_genericproductCollectionViaMedium = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_productCollectionViaMedium = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaMedium_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_surveyCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyCollection();
			_surveyPageCollectionViaMedium = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_actionButtonEntityReturnsNewIfNotFound = true;
			_countryEntityReturnsNewIfNotFound = true;
			_currencyEntityReturnsNewIfNotFound = true;
			_timeZoneEntityReturnsNewIfNotFound = true;
			_timestampCollectionReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfInterestId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Zipcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Latitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Longitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Telephone", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Website", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionButtonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostIndicationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostIndicationValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DescriptionSingleLine", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionButtonUrlMobile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionButtonUrlTablet", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NameOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DescriptionOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DescriptionSingleLineOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline1Overridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline2Overridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline3Overridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ZipcodeOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CityOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryIdOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyIdOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LatitudeOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LongitudeOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TelephoneOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FaxOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WebsiteOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostIndicationTypeOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostIndicationValueOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionButtonIdOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionButtonUrlTabletOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionButtonUrlMobileOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinesshoursOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModifiedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneIdOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryCodeOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyCodeOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureCodeOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneOlsonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeZoneOlsonIdOverridden", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Floor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayDistance", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _actionButtonEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncActionButtonEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _actionButtonEntity, new PropertyChangedEventHandler( OnActionButtonEntityPropertyChanged ), "ActionButtonEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.ActionButtonEntityUsingActionButtonIdStatic, true, signalRelatedEntity, "PointOfInterestCollection", resetFKFields, new int[] { (int)PointOfInterestFieldIndex.ActionButtonId } );		
			_actionButtonEntity = null;
		}
		
		/// <summary> setups the sync logic for member _actionButtonEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncActionButtonEntity(IEntityCore relatedEntity)
		{
			if(_actionButtonEntity!=relatedEntity)
			{		
				DesetupSyncActionButtonEntity(true, true);
				_actionButtonEntity = (ActionButtonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _actionButtonEntity, new PropertyChangedEventHandler( OnActionButtonEntityPropertyChanged ), "ActionButtonEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.ActionButtonEntityUsingActionButtonIdStatic, true, ref _alreadyFetchedActionButtonEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnActionButtonEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _countryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCountryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _countryEntity, new PropertyChangedEventHandler( OnCountryEntityPropertyChanged ), "CountryEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.CountryEntityUsingCountryIdStatic, true, signalRelatedEntity, "PointOfInterestCollection", resetFKFields, new int[] { (int)PointOfInterestFieldIndex.CountryId } );		
			_countryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _countryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCountryEntity(IEntityCore relatedEntity)
		{
			if(_countryEntity!=relatedEntity)
			{		
				DesetupSyncCountryEntity(true, true);
				_countryEntity = (CountryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _countryEntity, new PropertyChangedEventHandler( OnCountryEntityPropertyChanged ), "CountryEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.CountryEntityUsingCountryIdStatic, true, ref _alreadyFetchedCountryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCountryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _currencyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCurrencyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _currencyEntity, new PropertyChangedEventHandler( OnCurrencyEntityPropertyChanged ), "CurrencyEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.CurrencyEntityUsingCurrencyIdStatic, true, signalRelatedEntity, "PointOfInterestCollection", resetFKFields, new int[] { (int)PointOfInterestFieldIndex.CurrencyId } );		
			_currencyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _currencyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCurrencyEntity(IEntityCore relatedEntity)
		{
			if(_currencyEntity!=relatedEntity)
			{		
				DesetupSyncCurrencyEntity(true, true);
				_currencyEntity = (CurrencyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _currencyEntity, new PropertyChangedEventHandler( OnCurrencyEntityPropertyChanged ), "CurrencyEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.CurrencyEntityUsingCurrencyIdStatic, true, ref _alreadyFetchedCurrencyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCurrencyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timeZoneEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimeZoneEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timeZoneEntity, new PropertyChangedEventHandler( OnTimeZoneEntityPropertyChanged ), "TimeZoneEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.TimeZoneEntityUsingTimeZoneIdStatic, true, signalRelatedEntity, "PointOfInterestCollection", resetFKFields, new int[] { (int)PointOfInterestFieldIndex.TimeZoneId } );		
			_timeZoneEntity = null;
		}
		
		/// <summary> setups the sync logic for member _timeZoneEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimeZoneEntity(IEntityCore relatedEntity)
		{
			if(_timeZoneEntity!=relatedEntity)
			{		
				DesetupSyncTimeZoneEntity(true, true);
				_timeZoneEntity = (TimeZoneEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timeZoneEntity, new PropertyChangedEventHandler( OnTimeZoneEntityPropertyChanged ), "TimeZoneEntity", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.TimeZoneEntityUsingTimeZoneIdStatic, true, ref _alreadyFetchedTimeZoneEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimeZoneEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timestampCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimestampCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.TimestampEntityUsingPointOfInterestIdStatic, false, signalRelatedEntity, "PointOfInterestEntity", false, new int[] { (int)PointOfInterestFieldIndex.PointOfInterestId } );
			_timestampCollection = null;
		}
	
		/// <summary> setups the sync logic for member _timestampCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimestampCollection(IEntityCore relatedEntity)
		{
			if(_timestampCollection!=relatedEntity)
			{
				DesetupSyncTimestampCollection(true, true);
				_timestampCollection = (TimestampEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticPointOfInterestRelations.TimestampEntityUsingPointOfInterestIdStatic, false, ref _alreadyFetchedTimestampCollection, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimestampCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 pointOfInterestId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PointOfInterestFieldIndex.PointOfInterestId].ForcedCurrentValueWrite(pointOfInterestId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePointOfInterestDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PointOfInterestEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PointOfInterestRelations Relations
		{
			get	{ return new PointOfInterestRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccessCodePointOfInterest' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccessCodePointOfInterestCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection(), (IEntityRelation)GetRelationsForField("AccessCodePointOfInterestCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.AccessCodePointOfInterestEntity, 0, null, null, null, "AccessCodePointOfInterestCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Businesshours' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinesshoursCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.BusinesshoursCollection(), (IEntityRelation)GetRelationsForField("BusinesshoursCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.BusinesshoursEntity, 0, null, null, null, "BusinesshoursCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyManagementTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyManagementTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection(), (IEntityRelation)GetRelationsForField("CompanyManagementTaskCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.CompanyManagementTaskEntity, 0, null, null, null, "CompanyManagementTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MapPointOfInterest' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMapPointOfInterestCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MapPointOfInterestCollection(), (IEntityRelation)GetRelationsForField("MapPointOfInterestCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.MapPointOfInterestEntity, 0, null, null, null, "MapPointOfInterestCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterestAmenity' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestAmenityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestAmenityCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.PointOfInterestAmenityEntity, 0, null, null, null, "PointOfInterestAmenityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterestLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestLanguageCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.PointOfInterestLanguageEntity, 0, null, null, null, "PointOfInterestLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterestVenueCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestVenueCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestVenueCategoryCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity, 0, null, null, null, "PointOfInterestVenueCategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("UIModeCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "UIModeCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, GetRelationsForField("AdvertisementCollectionViaMedium"), "AdvertisementCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alteration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.AlterationEntity, 0, null, null, GetRelationsForField("AlterationCollectionViaMedium"), "AlterationCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, GetRelationsForField("AlterationoptionCollectionViaMedium"), "AlterationoptionCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium"), "CategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaMedium_"), "CategoryCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaMedium"), "CompanyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaMedium"), "DeliverypointgroupCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium"), "EntertainmentCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaMedium_"), "EntertainmentCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaMedium"), "EntertainmentcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, GetRelationsForField("GenericcategoryCollectionViaMedium"), "GenericcategoryCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, GetRelationsForField("GenericproductCollectionViaMedium"), "GenericproductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium"), "ProductCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaMedium_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaMedium_"), "ProductCollectionViaMedium_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.SurveyEntity, 0, null, null, GetRelationsForField("SurveyCollectionViaMedium"), "SurveyCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaMedium
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.MediaEntityUsingPointOfInterestId;
				intermediateRelation.SetAliases(string.Empty, "Media_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaMedium"), "SurveyPageCollectionViaMedium", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ActionButton'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionButtonEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionButtonCollection(), (IEntityRelation)GetRelationsForField("ActionButtonEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.ActionButtonEntity, 0, null, null, null, "ActionButtonEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Country'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCountryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CountryCollection(), (IEntityRelation)GetRelationsForField("CountryEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.CountryEntity, 0, null, null, null, "CountryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), (IEntityRelation)GetRelationsForField("CurrencyEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, null, "CurrencyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TimeZone'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimeZoneEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimeZoneCollection(), (IEntityRelation)GetRelationsForField("TimeZoneEntity")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.TimeZoneEntity, 0, null, null, null, "TimeZoneEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.PointOfInterestEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PointOfInterestId property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."PointOfInterestId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PointOfInterestId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestFieldIndex.PointOfInterestId, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.PointOfInterestId, value, true); }
		}

		/// <summary> The Name property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Name, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Name, value, true); }
		}

		/// <summary> The Addressline1 property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Addressline1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline1
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Addressline1, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Addressline1, value, true); }
		}

		/// <summary> The Addressline2 property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Addressline2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline2
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Addressline2, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Addressline2, value, true); }
		}

		/// <summary> The Addressline3 property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Addressline3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline3
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Addressline3, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Addressline3, value, true); }
		}

		/// <summary> The Zipcode property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Zipcode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Zipcode
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Zipcode, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Zipcode, value, true); }
		}

		/// <summary> The City property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."City"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.City, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.City, value, true); }
		}

		/// <summary> The CountryId property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CountryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CountryId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestFieldIndex.CountryId, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CountryId, value, true); }
		}

		/// <summary> The Latitude property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Latitude"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Latitude
		{
			get { return (Nullable<System.Double>)GetValue((int)PointOfInterestFieldIndex.Latitude, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Latitude, value, true); }
		}

		/// <summary> The Longitude property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Longitude"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Longitude
		{
			get { return (Nullable<System.Double>)GetValue((int)PointOfInterestFieldIndex.Longitude, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Longitude, value, true); }
		}

		/// <summary> The Telephone property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Telephone"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Telephone
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Telephone, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Telephone, value, true); }
		}

		/// <summary> The Fax property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Fax"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Fax
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Fax, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Fax, value, true); }
		}

		/// <summary> The Website property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Website"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Website
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Website, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Website, value, true); }
		}

		/// <summary> The Email property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Email, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Email, value, true); }
		}

		/// <summary> The Visible property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.Visible, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Visible, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)PointOfInterestFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)PointOfInterestFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ActionButtonId property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ActionButtonId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ActionButtonId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PointOfInterestFieldIndex.ActionButtonId, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ActionButtonId, value, true); }
		}

		/// <summary> The CostIndicationType property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CostIndicationType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CostIndicationType
		{
			get { return (System.Int32)GetValue((int)PointOfInterestFieldIndex.CostIndicationType, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CostIndicationType, value, true); }
		}

		/// <summary> The CostIndicationValue property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CostIndicationValue"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> CostIndicationValue
		{
			get { return (Nullable<System.Decimal>)GetValue((int)PointOfInterestFieldIndex.CostIndicationValue, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CostIndicationValue, value, true); }
		}

		/// <summary> The Description property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Description"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.Description, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Description, value, true); }
		}

		/// <summary> The DescriptionSingleLine property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."DescriptionSingleLine"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DescriptionSingleLine
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.DescriptionSingleLine, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.DescriptionSingleLine, value, true); }
		}

		/// <summary> The ActionButtonUrlMobile property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ActionButtonUrlMobile"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionButtonUrlMobile
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.ActionButtonUrlMobile, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ActionButtonUrlMobile, value, true); }
		}

		/// <summary> The ActionButtonUrlTablet property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ActionButtonUrlTablet"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionButtonUrlTablet
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.ActionButtonUrlTablet, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ActionButtonUrlTablet, value, true); }
		}

		/// <summary> The CurrencyId property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CurrencyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CurrencyId
		{
			get { return (System.Int32)GetValue((int)PointOfInterestFieldIndex.CurrencyId, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CurrencyId, value, true); }
		}

		/// <summary> The ExternalId property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ExternalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ExternalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PointOfInterestFieldIndex.ExternalId, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ExternalId, value, true); }
		}

		/// <summary> The NameOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."NameOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NameOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.NameOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.NameOverridden, value, true); }
		}

		/// <summary> The DescriptionOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."DescriptionOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DescriptionOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.DescriptionOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.DescriptionOverridden, value, true); }
		}

		/// <summary> The DescriptionSingleLineOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."DescriptionSingleLineOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DescriptionSingleLineOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.DescriptionSingleLineOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.DescriptionSingleLineOverridden, value, true); }
		}

		/// <summary> The Addressline1Overridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Addressline1Overridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Addressline1Overridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.Addressline1Overridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Addressline1Overridden, value, true); }
		}

		/// <summary> The Addressline2Overridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Addressline2Overridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Addressline2Overridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.Addressline2Overridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Addressline2Overridden, value, true); }
		}

		/// <summary> The Addressline3Overridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Addressline3Overridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Addressline3Overridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.Addressline3Overridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Addressline3Overridden, value, true); }
		}

		/// <summary> The ZipcodeOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ZipcodeOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ZipcodeOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.ZipcodeOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ZipcodeOverridden, value, true); }
		}

		/// <summary> The CityOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CityOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CityOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CityOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CityOverridden, value, true); }
		}

		/// <summary> The CountryIdOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CountryIdOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CountryIdOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CountryIdOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CountryIdOverridden, value, true); }
		}

		/// <summary> The CurrencyIdOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CurrencyIdOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CurrencyIdOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CurrencyIdOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CurrencyIdOverridden, value, true); }
		}

		/// <summary> The LatitudeOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."LatitudeOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LatitudeOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.LatitudeOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.LatitudeOverridden, value, true); }
		}

		/// <summary> The LongitudeOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."LongitudeOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LongitudeOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.LongitudeOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.LongitudeOverridden, value, true); }
		}

		/// <summary> The TelephoneOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."TelephoneOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TelephoneOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.TelephoneOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.TelephoneOverridden, value, true); }
		}

		/// <summary> The FaxOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."FaxOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean FaxOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.FaxOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.FaxOverridden, value, true); }
		}

		/// <summary> The WebsiteOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."WebsiteOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean WebsiteOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.WebsiteOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.WebsiteOverridden, value, true); }
		}

		/// <summary> The EmailOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."EmailOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EmailOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.EmailOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.EmailOverridden, value, true); }
		}

		/// <summary> The CostIndicationTypeOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CostIndicationTypeOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CostIndicationTypeOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CostIndicationTypeOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CostIndicationTypeOverridden, value, true); }
		}

		/// <summary> The CostIndicationValueOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CostIndicationValueOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CostIndicationValueOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CostIndicationValueOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CostIndicationValueOverridden, value, true); }
		}

		/// <summary> The ActionButtonIdOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ActionButtonIdOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ActionButtonIdOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.ActionButtonIdOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ActionButtonIdOverridden, value, true); }
		}

		/// <summary> The ActionButtonUrlTabletOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ActionButtonUrlTabletOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ActionButtonUrlTabletOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.ActionButtonUrlTabletOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ActionButtonUrlTabletOverridden, value, true); }
		}

		/// <summary> The ActionButtonUrlMobileOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."ActionButtonUrlMobileOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ActionButtonUrlMobileOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.ActionButtonUrlMobileOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.ActionButtonUrlMobileOverridden, value, true); }
		}

		/// <summary> The BusinesshoursOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."BusinesshoursOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BusinesshoursOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.BusinesshoursOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.BusinesshoursOverridden, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfInterestFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfInterestFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LastModifiedUTC property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."LastModifiedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)PointOfInterestFieldIndex.LastModifiedUTC, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.LastModifiedUTC, value, true); }
		}

		/// <summary> The TimeZoneIdOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."TimeZoneIdOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TimeZoneIdOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.TimeZoneIdOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.TimeZoneIdOverridden, value, true); }
		}

		/// <summary> The TimeZoneId property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."TimeZoneId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TimeZoneId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PointOfInterestFieldIndex.TimeZoneId, false); }
			set	{ SetValue((int)PointOfInterestFieldIndex.TimeZoneId, value, true); }
		}

		/// <summary> The CountryCode property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CountryCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.CountryCode, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CountryCode, value, true); }
		}

		/// <summary> The CountryCodeOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CountryCodeOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CountryCodeOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CountryCodeOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CountryCodeOverridden, value, true); }
		}

		/// <summary> The CurrencyCode property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CurrencyCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrencyCode
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.CurrencyCode, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CurrencyCode, value, true); }
		}

		/// <summary> The CurrencyCodeOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CurrencyCodeOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CurrencyCodeOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CurrencyCodeOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CurrencyCodeOverridden, value, true); }
		}

		/// <summary> The CultureCode property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CultureCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureCode
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.CultureCode, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CultureCode, value, true); }
		}

		/// <summary> The CultureCodeOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."CultureCodeOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CultureCodeOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.CultureCodeOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.CultureCodeOverridden, value, true); }
		}

		/// <summary> The TimeZoneOlsonId property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."TimeZoneOlsonId"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeZoneOlsonId
		{
			get { return (System.String)GetValue((int)PointOfInterestFieldIndex.TimeZoneOlsonId, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.TimeZoneOlsonId, value, true); }
		}

		/// <summary> The TimeZoneOlsonIdOverridden property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."TimeZoneOlsonIdOverridden"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TimeZoneOlsonIdOverridden
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.TimeZoneOlsonIdOverridden, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.TimeZoneOlsonIdOverridden, value, true); }
		}

		/// <summary> The Floor property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."Floor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Floor
		{
			get { return (System.Int32)GetValue((int)PointOfInterestFieldIndex.Floor, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.Floor, value, true); }
		}

		/// <summary> The DisplayDistance property of the Entity PointOfInterest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PointOfInterest"."DisplayDistance"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DisplayDistance
		{
			get { return (System.Boolean)GetValue((int)PointOfInterestFieldIndex.DisplayDistance, true); }
			set	{ SetValue((int)PointOfInterestFieldIndex.DisplayDistance, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccessCodePointOfInterestEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccessCodePointOfInterestCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AccessCodePointOfInterestCollection AccessCodePointOfInterestCollection
		{
			get	{ return GetMultiAccessCodePointOfInterestCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccessCodePointOfInterestCollection. When set to true, AccessCodePointOfInterestCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccessCodePointOfInterestCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAccessCodePointOfInterestCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccessCodePointOfInterestCollection
		{
			get	{ return _alwaysFetchAccessCodePointOfInterestCollection; }
			set	{ _alwaysFetchAccessCodePointOfInterestCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccessCodePointOfInterestCollection already has been fetched. Setting this property to false when AccessCodePointOfInterestCollection has been fetched
		/// will clear the AccessCodePointOfInterestCollection collection well. Setting this property to true while AccessCodePointOfInterestCollection hasn't been fetched disables lazy loading for AccessCodePointOfInterestCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccessCodePointOfInterestCollection
		{
			get { return _alreadyFetchedAccessCodePointOfInterestCollection;}
			set 
			{
				if(_alreadyFetchedAccessCodePointOfInterestCollection && !value && (_accessCodePointOfInterestCollection != null))
				{
					_accessCodePointOfInterestCollection.Clear();
				}
				_alreadyFetchedAccessCodePointOfInterestCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinesshoursEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinesshoursCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.BusinesshoursCollection BusinesshoursCollection
		{
			get	{ return GetMultiBusinesshoursCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinesshoursCollection. When set to true, BusinesshoursCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinesshoursCollection is accessed. You can always execute/ a forced fetch by calling GetMultiBusinesshoursCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinesshoursCollection
		{
			get	{ return _alwaysFetchBusinesshoursCollection; }
			set	{ _alwaysFetchBusinesshoursCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinesshoursCollection already has been fetched. Setting this property to false when BusinesshoursCollection has been fetched
		/// will clear the BusinesshoursCollection collection well. Setting this property to true while BusinesshoursCollection hasn't been fetched disables lazy loading for BusinesshoursCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinesshoursCollection
		{
			get { return _alreadyFetchedBusinesshoursCollection;}
			set 
			{
				if(_alreadyFetchedBusinesshoursCollection && !value && (_businesshoursCollection != null))
				{
					_businesshoursCollection.Clear();
				}
				_alreadyFetchedBusinesshoursCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CompanyManagementTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyManagementTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyManagementTaskCollection CompanyManagementTaskCollection
		{
			get	{ return GetMultiCompanyManagementTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyManagementTaskCollection. When set to true, CompanyManagementTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyManagementTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyManagementTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyManagementTaskCollection
		{
			get	{ return _alwaysFetchCompanyManagementTaskCollection; }
			set	{ _alwaysFetchCompanyManagementTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyManagementTaskCollection already has been fetched. Setting this property to false when CompanyManagementTaskCollection has been fetched
		/// will clear the CompanyManagementTaskCollection collection well. Setting this property to true while CompanyManagementTaskCollection hasn't been fetched disables lazy loading for CompanyManagementTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyManagementTaskCollection
		{
			get { return _alreadyFetchedCompanyManagementTaskCollection;}
			set 
			{
				if(_alreadyFetchedCompanyManagementTaskCollection && !value && (_companyManagementTaskCollection != null))
				{
					_companyManagementTaskCollection.Clear();
				}
				_alreadyFetchedCompanyManagementTaskCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MapPointOfInterestEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMapPointOfInterestCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MapPointOfInterestCollection MapPointOfInterestCollection
		{
			get	{ return GetMultiMapPointOfInterestCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MapPointOfInterestCollection. When set to true, MapPointOfInterestCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MapPointOfInterestCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMapPointOfInterestCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMapPointOfInterestCollection
		{
			get	{ return _alwaysFetchMapPointOfInterestCollection; }
			set	{ _alwaysFetchMapPointOfInterestCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MapPointOfInterestCollection already has been fetched. Setting this property to false when MapPointOfInterestCollection has been fetched
		/// will clear the MapPointOfInterestCollection collection well. Setting this property to true while MapPointOfInterestCollection hasn't been fetched disables lazy loading for MapPointOfInterestCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMapPointOfInterestCollection
		{
			get { return _alreadyFetchedMapPointOfInterestCollection;}
			set 
			{
				if(_alreadyFetchedMapPointOfInterestCollection && !value && (_mapPointOfInterestCollection != null))
				{
					_mapPointOfInterestCollection.Clear();
				}
				_alreadyFetchedMapPointOfInterestCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestAmenityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection PointOfInterestAmenityCollection
		{
			get	{ return GetMultiPointOfInterestAmenityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestAmenityCollection. When set to true, PointOfInterestAmenityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestAmenityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestAmenityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestAmenityCollection
		{
			get	{ return _alwaysFetchPointOfInterestAmenityCollection; }
			set	{ _alwaysFetchPointOfInterestAmenityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestAmenityCollection already has been fetched. Setting this property to false when PointOfInterestAmenityCollection has been fetched
		/// will clear the PointOfInterestAmenityCollection collection well. Setting this property to true while PointOfInterestAmenityCollection hasn't been fetched disables lazy loading for PointOfInterestAmenityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestAmenityCollection
		{
			get { return _alreadyFetchedPointOfInterestAmenityCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestAmenityCollection && !value && (_pointOfInterestAmenityCollection != null))
				{
					_pointOfInterestAmenityCollection.Clear();
				}
				_alreadyFetchedPointOfInterestAmenityCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestLanguageCollection PointOfInterestLanguageCollection
		{
			get	{ return GetMultiPointOfInterestLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestLanguageCollection. When set to true, PointOfInterestLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestLanguageCollection
		{
			get	{ return _alwaysFetchPointOfInterestLanguageCollection; }
			set	{ _alwaysFetchPointOfInterestLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestLanguageCollection already has been fetched. Setting this property to false when PointOfInterestLanguageCollection has been fetched
		/// will clear the PointOfInterestLanguageCollection collection well. Setting this property to true while PointOfInterestLanguageCollection hasn't been fetched disables lazy loading for PointOfInterestLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestLanguageCollection
		{
			get { return _alreadyFetchedPointOfInterestLanguageCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestLanguageCollection && !value && (_pointOfInterestLanguageCollection != null))
				{
					_pointOfInterestLanguageCollection.Clear();
				}
				_alreadyFetchedPointOfInterestLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestVenueCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestVenueCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestVenueCategoryCollection PointOfInterestVenueCategoryCollection
		{
			get	{ return GetMultiPointOfInterestVenueCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestVenueCategoryCollection. When set to true, PointOfInterestVenueCategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestVenueCategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestVenueCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestVenueCategoryCollection
		{
			get	{ return _alwaysFetchPointOfInterestVenueCategoryCollection; }
			set	{ _alwaysFetchPointOfInterestVenueCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestVenueCategoryCollection already has been fetched. Setting this property to false when PointOfInterestVenueCategoryCollection has been fetched
		/// will clear the PointOfInterestVenueCategoryCollection collection well. Setting this property to true while PointOfInterestVenueCategoryCollection hasn't been fetched disables lazy loading for PointOfInterestVenueCategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestVenueCategoryCollection
		{
			get { return _alreadyFetchedPointOfInterestVenueCategoryCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestVenueCategoryCollection && !value && (_pointOfInterestVenueCategoryCollection != null))
				{
					_pointOfInterestVenueCategoryCollection.Clear();
				}
				_alreadyFetchedPointOfInterestVenueCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollection
		{
			get	{ return GetMultiProductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollection. When set to true, ProductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollection
		{
			get	{ return _alwaysFetchProductCollection; }
			set	{ _alwaysFetchProductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollection already has been fetched. Setting this property to false when ProductCollection has been fetched
		/// will clear the ProductCollection collection well. Setting this property to true while ProductCollection hasn't been fetched disables lazy loading for ProductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollection
		{
			get { return _alreadyFetchedProductCollection;}
			set 
			{
				if(_alreadyFetchedProductCollection && !value && (_productCollection != null))
				{
					_productCollection.Clear();
				}
				_alreadyFetchedProductCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollection
		{
			get	{ return GetMultiUIModeCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollection. When set to true, UIModeCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIModeCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollection
		{
			get	{ return _alwaysFetchUIModeCollection; }
			set	{ _alwaysFetchUIModeCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollection already has been fetched. Setting this property to false when UIModeCollection has been fetched
		/// will clear the UIModeCollection collection well. Setting this property to true while UIModeCollection hasn't been fetched disables lazy loading for UIModeCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollection
		{
			get { return _alreadyFetchedUIModeCollection;}
			set 
			{
				if(_alreadyFetchedUIModeCollection && !value && (_uIModeCollection != null))
				{
					_uIModeCollection.Clear();
				}
				_alreadyFetchedUIModeCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollectionViaMedium
		{
			get { return GetMultiAdvertisementCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollectionViaMedium. When set to true, AdvertisementCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAdvertisementCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollectionViaMedium
		{
			get	{ return _alwaysFetchAdvertisementCollectionViaMedium; }
			set	{ _alwaysFetchAdvertisementCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollectionViaMedium already has been fetched. Setting this property to false when AdvertisementCollectionViaMedium has been fetched
		/// will clear the AdvertisementCollectionViaMedium collection well. Setting this property to true while AdvertisementCollectionViaMedium hasn't been fetched disables lazy loading for AdvertisementCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollectionViaMedium
		{
			get { return _alreadyFetchedAdvertisementCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollectionViaMedium && !value && (_advertisementCollectionViaMedium != null))
				{
					_advertisementCollectionViaMedium.Clear();
				}
				_alreadyFetchedAdvertisementCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationCollection AlterationCollectionViaMedium
		{
			get { return GetMultiAlterationCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationCollectionViaMedium. When set to true, AlterationCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationCollectionViaMedium; }
			set	{ _alwaysFetchAlterationCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationCollectionViaMedium already has been fetched. Setting this property to false when AlterationCollectionViaMedium has been fetched
		/// will clear the AlterationCollectionViaMedium collection well. Setting this property to true while AlterationCollectionViaMedium hasn't been fetched disables lazy loading for AlterationCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationCollectionViaMedium && !value && (_alterationCollectionViaMedium != null))
				{
					_alterationCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AlterationoptionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAlterationoptionCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AlterationoptionCollection AlterationoptionCollectionViaMedium
		{
			get { return GetMultiAlterationoptionCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionCollectionViaMedium. When set to true, AlterationoptionCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiAlterationoptionCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionCollectionViaMedium
		{
			get	{ return _alwaysFetchAlterationoptionCollectionViaMedium; }
			set	{ _alwaysFetchAlterationoptionCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionCollectionViaMedium already has been fetched. Setting this property to false when AlterationoptionCollectionViaMedium has been fetched
		/// will clear the AlterationoptionCollectionViaMedium collection well. Setting this property to true while AlterationoptionCollectionViaMedium hasn't been fetched disables lazy loading for AlterationoptionCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionCollectionViaMedium
		{
			get { return _alreadyFetchedAlterationoptionCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedAlterationoptionCollectionViaMedium && !value && (_alterationoptionCollectionViaMedium != null))
				{
					_alterationoptionCollectionViaMedium.Clear();
				}
				_alreadyFetchedAlterationoptionCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium
		{
			get { return GetMultiCategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium. When set to true, CategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium; }
			set	{ _alwaysFetchCategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium already has been fetched. Setting this property to false when CategoryCollectionViaMedium has been fetched
		/// will clear the CategoryCollectionViaMedium collection well. Setting this property to true while CategoryCollectionViaMedium hasn't been fetched disables lazy loading for CategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium && !value && (_categoryCollectionViaMedium != null))
				{
					_categoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaMedium_
		{
			get { return GetMultiCategoryCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaMedium_. When set to true, CategoryCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaMedium_
		{
			get	{ return _alwaysFetchCategoryCollectionViaMedium_; }
			set	{ _alwaysFetchCategoryCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaMedium_ already has been fetched. Setting this property to false when CategoryCollectionViaMedium_ has been fetched
		/// will clear the CategoryCollectionViaMedium_ collection well. Setting this property to true while CategoryCollectionViaMedium_ hasn't been fetched disables lazy loading for CategoryCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaMedium_
		{
			get { return _alreadyFetchedCategoryCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaMedium_ && !value && (_categoryCollectionViaMedium_ != null))
				{
					_categoryCollectionViaMedium_.Clear();
				}
				_alreadyFetchedCategoryCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaMedium
		{
			get { return GetMultiCompanyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaMedium. When set to true, CompanyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaMedium
		{
			get	{ return _alwaysFetchCompanyCollectionViaMedium; }
			set	{ _alwaysFetchCompanyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaMedium already has been fetched. Setting this property to false when CompanyCollectionViaMedium has been fetched
		/// will clear the CompanyCollectionViaMedium collection well. Setting this property to true while CompanyCollectionViaMedium hasn't been fetched disables lazy loading for CompanyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaMedium
		{
			get { return _alreadyFetchedCompanyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaMedium && !value && (_companyCollectionViaMedium != null))
				{
					_companyCollectionViaMedium.Clear();
				}
				_alreadyFetchedCompanyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaMedium
		{
			get { return GetMultiDeliverypointgroupCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaMedium. When set to true, DeliverypointgroupCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaMedium
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaMedium; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaMedium already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaMedium has been fetched
		/// will clear the DeliverypointgroupCollectionViaMedium collection well. Setting this property to true while DeliverypointgroupCollectionViaMedium hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaMedium
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaMedium && !value && (_deliverypointgroupCollectionViaMedium != null))
				{
					_deliverypointgroupCollectionViaMedium.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium
		{
			get { return GetMultiEntertainmentCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium. When set to true, EntertainmentCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium has been fetched
		/// will clear the EntertainmentCollectionViaMedium collection well. Setting this property to true while EntertainmentCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium && !value && (_entertainmentCollectionViaMedium != null))
				{
					_entertainmentCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaMedium_
		{
			get { return GetMultiEntertainmentCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaMedium_. When set to true, EntertainmentCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaMedium_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaMedium_; }
			set	{ _alwaysFetchEntertainmentCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaMedium_ already has been fetched. Setting this property to false when EntertainmentCollectionViaMedium_ has been fetched
		/// will clear the EntertainmentCollectionViaMedium_ collection well. Setting this property to true while EntertainmentCollectionViaMedium_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaMedium_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaMedium_ && !value && (_entertainmentCollectionViaMedium_ != null))
				{
					_entertainmentCollectionViaMedium_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaMedium
		{
			get { return GetMultiEntertainmentcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaMedium. When set to true, EntertainmentcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaMedium; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaMedium already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaMedium has been fetched
		/// will clear the EntertainmentcategoryCollectionViaMedium collection well. Setting this property to true while EntertainmentcategoryCollectionViaMedium hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaMedium && !value && (_entertainmentcategoryCollectionViaMedium != null))
				{
					_entertainmentcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollectionViaMedium
		{
			get { return GetMultiGenericcategoryCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollectionViaMedium. When set to true, GenericcategoryCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericcategoryCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericcategoryCollectionViaMedium; }
			set	{ _alwaysFetchGenericcategoryCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollectionViaMedium already has been fetched. Setting this property to false when GenericcategoryCollectionViaMedium has been fetched
		/// will clear the GenericcategoryCollectionViaMedium collection well. Setting this property to true while GenericcategoryCollectionViaMedium hasn't been fetched disables lazy loading for GenericcategoryCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollectionViaMedium
		{
			get { return _alreadyFetchedGenericcategoryCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollectionViaMedium && !value && (_genericcategoryCollectionViaMedium != null))
				{
					_genericcategoryCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericcategoryCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollectionViaMedium
		{
			get { return GetMultiGenericproductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollectionViaMedium. When set to true, GenericproductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiGenericproductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollectionViaMedium
		{
			get	{ return _alwaysFetchGenericproductCollectionViaMedium; }
			set	{ _alwaysFetchGenericproductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollectionViaMedium already has been fetched. Setting this property to false when GenericproductCollectionViaMedium has been fetched
		/// will clear the GenericproductCollectionViaMedium collection well. Setting this property to true while GenericproductCollectionViaMedium hasn't been fetched disables lazy loading for GenericproductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollectionViaMedium
		{
			get { return _alreadyFetchedGenericproductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedGenericproductCollectionViaMedium && !value && (_genericproductCollectionViaMedium != null))
				{
					_genericproductCollectionViaMedium.Clear();
				}
				_alreadyFetchedGenericproductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium
		{
			get { return GetMultiProductCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium. When set to true, ProductCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium
		{
			get	{ return _alwaysFetchProductCollectionViaMedium; }
			set	{ _alwaysFetchProductCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium already has been fetched. Setting this property to false when ProductCollectionViaMedium has been fetched
		/// will clear the ProductCollectionViaMedium collection well. Setting this property to true while ProductCollectionViaMedium hasn't been fetched disables lazy loading for ProductCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium
		{
			get { return _alreadyFetchedProductCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium && !value && (_productCollectionViaMedium != null))
				{
					_productCollectionViaMedium.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaMedium_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaMedium_
		{
			get { return GetMultiProductCollectionViaMedium_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaMedium_. When set to true, ProductCollectionViaMedium_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaMedium_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaMedium_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaMedium_
		{
			get	{ return _alwaysFetchProductCollectionViaMedium_; }
			set	{ _alwaysFetchProductCollectionViaMedium_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaMedium_ already has been fetched. Setting this property to false when ProductCollectionViaMedium_ has been fetched
		/// will clear the ProductCollectionViaMedium_ collection well. Setting this property to true while ProductCollectionViaMedium_ hasn't been fetched disables lazy loading for ProductCollectionViaMedium_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaMedium_
		{
			get { return _alreadyFetchedProductCollectionViaMedium_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaMedium_ && !value && (_productCollectionViaMedium_ != null))
				{
					_productCollectionViaMedium_.Clear();
				}
				_alreadyFetchedProductCollectionViaMedium_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyCollection SurveyCollectionViaMedium
		{
			get { return GetMultiSurveyCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyCollectionViaMedium. When set to true, SurveyCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyCollectionViaMedium; }
			set	{ _alwaysFetchSurveyCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyCollectionViaMedium already has been fetched. Setting this property to false when SurveyCollectionViaMedium has been fetched
		/// will clear the SurveyCollectionViaMedium collection well. Setting this property to true while SurveyCollectionViaMedium hasn't been fetched disables lazy loading for SurveyCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyCollectionViaMedium && !value && (_surveyCollectionViaMedium != null))
				{
					_surveyCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyCollectionViaMedium = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaMedium()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaMedium
		{
			get { return GetMultiSurveyPageCollectionViaMedium(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaMedium. When set to true, SurveyPageCollectionViaMedium is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaMedium is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaMedium(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaMedium
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaMedium; }
			set	{ _alwaysFetchSurveyPageCollectionViaMedium = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaMedium already has been fetched. Setting this property to false when SurveyPageCollectionViaMedium has been fetched
		/// will clear the SurveyPageCollectionViaMedium collection well. Setting this property to true while SurveyPageCollectionViaMedium hasn't been fetched disables lazy loading for SurveyPageCollectionViaMedium</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaMedium
		{
			get { return _alreadyFetchedSurveyPageCollectionViaMedium;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaMedium && !value && (_surveyPageCollectionViaMedium != null))
				{
					_surveyPageCollectionViaMedium.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaMedium = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ActionButtonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleActionButtonEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ActionButtonEntity ActionButtonEntity
		{
			get	{ return GetSingleActionButtonEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncActionButtonEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestCollection", "ActionButtonEntity", _actionButtonEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ActionButtonEntity. When set to true, ActionButtonEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionButtonEntity is accessed. You can always execute a forced fetch by calling GetSingleActionButtonEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionButtonEntity
		{
			get	{ return _alwaysFetchActionButtonEntity; }
			set	{ _alwaysFetchActionButtonEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionButtonEntity already has been fetched. Setting this property to false when ActionButtonEntity has been fetched
		/// will set ActionButtonEntity to null as well. Setting this property to true while ActionButtonEntity hasn't been fetched disables lazy loading for ActionButtonEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionButtonEntity
		{
			get { return _alreadyFetchedActionButtonEntity;}
			set 
			{
				if(_alreadyFetchedActionButtonEntity && !value)
				{
					this.ActionButtonEntity = null;
				}
				_alreadyFetchedActionButtonEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ActionButtonEntity is not found
		/// in the database. When set to true, ActionButtonEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ActionButtonEntityReturnsNewIfNotFound
		{
			get	{ return _actionButtonEntityReturnsNewIfNotFound; }
			set { _actionButtonEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CountryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCountryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CountryEntity CountryEntity
		{
			get	{ return GetSingleCountryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCountryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestCollection", "CountryEntity", _countryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CountryEntity. When set to true, CountryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CountryEntity is accessed. You can always execute a forced fetch by calling GetSingleCountryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCountryEntity
		{
			get	{ return _alwaysFetchCountryEntity; }
			set	{ _alwaysFetchCountryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CountryEntity already has been fetched. Setting this property to false when CountryEntity has been fetched
		/// will set CountryEntity to null as well. Setting this property to true while CountryEntity hasn't been fetched disables lazy loading for CountryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCountryEntity
		{
			get { return _alreadyFetchedCountryEntity;}
			set 
			{
				if(_alreadyFetchedCountryEntity && !value)
				{
					this.CountryEntity = null;
				}
				_alreadyFetchedCountryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CountryEntity is not found
		/// in the database. When set to true, CountryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CountryEntityReturnsNewIfNotFound
		{
			get	{ return _countryEntityReturnsNewIfNotFound; }
			set { _countryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CurrencyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCurrencyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CurrencyEntity CurrencyEntity
		{
			get	{ return GetSingleCurrencyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCurrencyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestCollection", "CurrencyEntity", _currencyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyEntity. When set to true, CurrencyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyEntity is accessed. You can always execute a forced fetch by calling GetSingleCurrencyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyEntity
		{
			get	{ return _alwaysFetchCurrencyEntity; }
			set	{ _alwaysFetchCurrencyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyEntity already has been fetched. Setting this property to false when CurrencyEntity has been fetched
		/// will set CurrencyEntity to null as well. Setting this property to true while CurrencyEntity hasn't been fetched disables lazy loading for CurrencyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyEntity
		{
			get { return _alreadyFetchedCurrencyEntity;}
			set 
			{
				if(_alreadyFetchedCurrencyEntity && !value)
				{
					this.CurrencyEntity = null;
				}
				_alreadyFetchedCurrencyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CurrencyEntity is not found
		/// in the database. When set to true, CurrencyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CurrencyEntityReturnsNewIfNotFound
		{
			get	{ return _currencyEntityReturnsNewIfNotFound; }
			set { _currencyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimeZoneEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimeZoneEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimeZoneEntity TimeZoneEntity
		{
			get	{ return GetSingleTimeZoneEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTimeZoneEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PointOfInterestCollection", "TimeZoneEntity", _timeZoneEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimeZoneEntity. When set to true, TimeZoneEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimeZoneEntity is accessed. You can always execute a forced fetch by calling GetSingleTimeZoneEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimeZoneEntity
		{
			get	{ return _alwaysFetchTimeZoneEntity; }
			set	{ _alwaysFetchTimeZoneEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TimeZoneEntity already has been fetched. Setting this property to false when TimeZoneEntity has been fetched
		/// will set TimeZoneEntity to null as well. Setting this property to true while TimeZoneEntity hasn't been fetched disables lazy loading for TimeZoneEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimeZoneEntity
		{
			get { return _alreadyFetchedTimeZoneEntity;}
			set 
			{
				if(_alreadyFetchedTimeZoneEntity && !value)
				{
					this.TimeZoneEntity = null;
				}
				_alreadyFetchedTimeZoneEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimeZoneEntity is not found
		/// in the database. When set to true, TimeZoneEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimeZoneEntityReturnsNewIfNotFound
		{
			get	{ return _timeZoneEntityReturnsNewIfNotFound; }
			set { _timeZoneEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimestampEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimestampEntity TimestampCollection
		{
			get	{ return GetSingleTimestampCollection(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTimestampCollection(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_timestampCollection !=null);
						DesetupSyncTimestampCollection(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TimestampCollection");
						}
					}
					else
					{
						if(_timestampCollection!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PointOfInterestEntity");
							SetupSyncTimestampCollection(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute a forced fetch by calling GetSingleTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will set TimestampCollection to null as well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value)
				{
					this.TimestampCollection = null;
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimestampCollection is not found
		/// in the database. When set to true, TimestampCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimestampCollectionReturnsNewIfNotFound
		{
			get	{ return _timestampCollectionReturnsNewIfNotFound; }
			set	{ _timestampCollectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PointOfInterestEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
