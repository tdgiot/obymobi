﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'PaymentTransaction'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class PaymentTransactionEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "PaymentTransactionEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection	_paymentTransactionLogCollection;
		private bool	_alwaysFetchPaymentTransactionLogCollection, _alreadyFetchedPaymentTransactionLogCollection;
		private Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection	_paymentTransactionSplitCollection;
		private bool	_alwaysFetchPaymentTransactionSplitCollection, _alreadyFetchedPaymentTransactionSplitCollection;
		private OrderEntity _orderEntity;
		private bool	_alwaysFetchOrderEntity, _alreadyFetchedOrderEntity, _orderEntityReturnsNewIfNotFound;
		private PaymentIntegrationConfigurationEntity _paymentIntegrationConfigurationEntity;
		private bool	_alwaysFetchPaymentIntegrationConfigurationEntity, _alreadyFetchedPaymentIntegrationConfigurationEntity, _paymentIntegrationConfigurationEntityReturnsNewIfNotFound;
		private PaymentProviderEntity _paymentProviderEntity;
		private bool	_alwaysFetchPaymentProviderEntity, _alreadyFetchedPaymentProviderEntity, _paymentProviderEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name OrderEntity</summary>
			public static readonly string OrderEntity = "OrderEntity";
			/// <summary>Member name PaymentIntegrationConfigurationEntity</summary>
			public static readonly string PaymentIntegrationConfigurationEntity = "PaymentIntegrationConfigurationEntity";
			/// <summary>Member name PaymentProviderEntity</summary>
			public static readonly string PaymentProviderEntity = "PaymentProviderEntity";
			/// <summary>Member name PaymentTransactionLogCollection</summary>
			public static readonly string PaymentTransactionLogCollection = "PaymentTransactionLogCollection";
			/// <summary>Member name PaymentTransactionSplitCollection</summary>
			public static readonly string PaymentTransactionSplitCollection = "PaymentTransactionSplitCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentTransactionEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected PaymentTransactionEntityBase() :base("PaymentTransactionEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		protected PaymentTransactionEntityBase(System.Int32 paymentTransactionId):base("PaymentTransactionEntity")
		{
			InitClassFetch(paymentTransactionId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected PaymentTransactionEntityBase(System.Int32 paymentTransactionId, IPrefetchPath prefetchPathToUse): base("PaymentTransactionEntity")
		{
			InitClassFetch(paymentTransactionId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="validator">The custom validator object for this PaymentTransactionEntity</param>
		protected PaymentTransactionEntityBase(System.Int32 paymentTransactionId, IValidator validator):base("PaymentTransactionEntity")
		{
			InitClassFetch(paymentTransactionId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentTransactionEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_paymentTransactionLogCollection = (Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection)info.GetValue("_paymentTransactionLogCollection", typeof(Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection));
			_alwaysFetchPaymentTransactionLogCollection = info.GetBoolean("_alwaysFetchPaymentTransactionLogCollection");
			_alreadyFetchedPaymentTransactionLogCollection = info.GetBoolean("_alreadyFetchedPaymentTransactionLogCollection");

			_paymentTransactionSplitCollection = (Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection)info.GetValue("_paymentTransactionSplitCollection", typeof(Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection));
			_alwaysFetchPaymentTransactionSplitCollection = info.GetBoolean("_alwaysFetchPaymentTransactionSplitCollection");
			_alreadyFetchedPaymentTransactionSplitCollection = info.GetBoolean("_alreadyFetchedPaymentTransactionSplitCollection");
			_orderEntity = (OrderEntity)info.GetValue("_orderEntity", typeof(OrderEntity));
			if(_orderEntity!=null)
			{
				_orderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderEntityReturnsNewIfNotFound = info.GetBoolean("_orderEntityReturnsNewIfNotFound");
			_alwaysFetchOrderEntity = info.GetBoolean("_alwaysFetchOrderEntity");
			_alreadyFetchedOrderEntity = info.GetBoolean("_alreadyFetchedOrderEntity");

			_paymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)info.GetValue("_paymentIntegrationConfigurationEntity", typeof(PaymentIntegrationConfigurationEntity));
			if(_paymentIntegrationConfigurationEntity!=null)
			{
				_paymentIntegrationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentIntegrationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_paymentIntegrationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchPaymentIntegrationConfigurationEntity = info.GetBoolean("_alwaysFetchPaymentIntegrationConfigurationEntity");
			_alreadyFetchedPaymentIntegrationConfigurationEntity = info.GetBoolean("_alreadyFetchedPaymentIntegrationConfigurationEntity");

			_paymentProviderEntity = (PaymentProviderEntity)info.GetValue("_paymentProviderEntity", typeof(PaymentProviderEntity));
			if(_paymentProviderEntity!=null)
			{
				_paymentProviderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentProviderEntityReturnsNewIfNotFound = info.GetBoolean("_paymentProviderEntityReturnsNewIfNotFound");
			_alwaysFetchPaymentProviderEntity = info.GetBoolean("_alwaysFetchPaymentProviderEntity");
			_alreadyFetchedPaymentProviderEntity = info.GetBoolean("_alreadyFetchedPaymentProviderEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentTransactionFieldIndex)fieldIndex)
			{
				case PaymentTransactionFieldIndex.OrderId:
					DesetupSyncOrderEntity(true, false);
					_alreadyFetchedOrderEntity = false;
					break;
				case PaymentTransactionFieldIndex.PaymentProviderId:
					DesetupSyncPaymentProviderEntity(true, false);
					_alreadyFetchedPaymentProviderEntity = false;
					break;
				case PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId:
					DesetupSyncPaymentIntegrationConfigurationEntity(true, false);
					_alreadyFetchedPaymentIntegrationConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPaymentTransactionLogCollection = (_paymentTransactionLogCollection.Count > 0);
			_alreadyFetchedPaymentTransactionSplitCollection = (_paymentTransactionSplitCollection.Count > 0);
			_alreadyFetchedOrderEntity = (_orderEntity != null);
			_alreadyFetchedPaymentIntegrationConfigurationEntity = (_paymentIntegrationConfigurationEntity != null);
			_alreadyFetchedPaymentProviderEntity = (_paymentProviderEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "OrderEntity":
					toReturn.Add(Relations.OrderEntityUsingOrderId);
					break;
				case "PaymentIntegrationConfigurationEntity":
					toReturn.Add(Relations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
					break;
				case "PaymentProviderEntity":
					toReturn.Add(Relations.PaymentProviderEntityUsingPaymentProviderId);
					break;
				case "PaymentTransactionLogCollection":
					toReturn.Add(Relations.PaymentTransactionLogEntityUsingPaymentTransactionId);
					break;
				case "PaymentTransactionSplitCollection":
					toReturn.Add(Relations.PaymentTransactionSplitEntityUsingPaymentTransactionId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_paymentTransactionLogCollection", (!this.MarkedForDeletion?_paymentTransactionLogCollection:null));
			info.AddValue("_alwaysFetchPaymentTransactionLogCollection", _alwaysFetchPaymentTransactionLogCollection);
			info.AddValue("_alreadyFetchedPaymentTransactionLogCollection", _alreadyFetchedPaymentTransactionLogCollection);
			info.AddValue("_paymentTransactionSplitCollection", (!this.MarkedForDeletion?_paymentTransactionSplitCollection:null));
			info.AddValue("_alwaysFetchPaymentTransactionSplitCollection", _alwaysFetchPaymentTransactionSplitCollection);
			info.AddValue("_alreadyFetchedPaymentTransactionSplitCollection", _alreadyFetchedPaymentTransactionSplitCollection);
			info.AddValue("_orderEntity", (!this.MarkedForDeletion?_orderEntity:null));
			info.AddValue("_orderEntityReturnsNewIfNotFound", _orderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderEntity", _alwaysFetchOrderEntity);
			info.AddValue("_alreadyFetchedOrderEntity", _alreadyFetchedOrderEntity);
			info.AddValue("_paymentIntegrationConfigurationEntity", (!this.MarkedForDeletion?_paymentIntegrationConfigurationEntity:null));
			info.AddValue("_paymentIntegrationConfigurationEntityReturnsNewIfNotFound", _paymentIntegrationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentIntegrationConfigurationEntity", _alwaysFetchPaymentIntegrationConfigurationEntity);
			info.AddValue("_alreadyFetchedPaymentIntegrationConfigurationEntity", _alreadyFetchedPaymentIntegrationConfigurationEntity);
			info.AddValue("_paymentProviderEntity", (!this.MarkedForDeletion?_paymentProviderEntity:null));
			info.AddValue("_paymentProviderEntityReturnsNewIfNotFound", _paymentProviderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentProviderEntity", _alwaysFetchPaymentProviderEntity);
			info.AddValue("_alreadyFetchedPaymentProviderEntity", _alreadyFetchedPaymentProviderEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "OrderEntity":
					_alreadyFetchedOrderEntity = true;
					this.OrderEntity = (OrderEntity)entity;
					break;
				case "PaymentIntegrationConfigurationEntity":
					_alreadyFetchedPaymentIntegrationConfigurationEntity = true;
					this.PaymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)entity;
					break;
				case "PaymentProviderEntity":
					_alreadyFetchedPaymentProviderEntity = true;
					this.PaymentProviderEntity = (PaymentProviderEntity)entity;
					break;
				case "PaymentTransactionLogCollection":
					_alreadyFetchedPaymentTransactionLogCollection = true;
					if(entity!=null)
					{
						this.PaymentTransactionLogCollection.Add((PaymentTransactionLogEntity)entity);
					}
					break;
				case "PaymentTransactionSplitCollection":
					_alreadyFetchedPaymentTransactionSplitCollection = true;
					if(entity!=null)
					{
						this.PaymentTransactionSplitCollection.Add((PaymentTransactionSplitEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "OrderEntity":
					SetupSyncOrderEntity(relatedEntity);
					break;
				case "PaymentIntegrationConfigurationEntity":
					SetupSyncPaymentIntegrationConfigurationEntity(relatedEntity);
					break;
				case "PaymentProviderEntity":
					SetupSyncPaymentProviderEntity(relatedEntity);
					break;
				case "PaymentTransactionLogCollection":
					_paymentTransactionLogCollection.Add((PaymentTransactionLogEntity)relatedEntity);
					break;
				case "PaymentTransactionSplitCollection":
					_paymentTransactionSplitCollection.Add((PaymentTransactionSplitEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "OrderEntity":
					DesetupSyncOrderEntity(false, true);
					break;
				case "PaymentIntegrationConfigurationEntity":
					DesetupSyncPaymentIntegrationConfigurationEntity(false, true);
					break;
				case "PaymentProviderEntity":
					DesetupSyncPaymentProviderEntity(false, true);
					break;
				case "PaymentTransactionLogCollection":
					this.PerformRelatedEntityRemoval(_paymentTransactionLogCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentTransactionSplitCollection":
					this.PerformRelatedEntityRemoval(_paymentTransactionSplitCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_orderEntity!=null)
			{
				toReturn.Add(_orderEntity);
			}
			if(_paymentIntegrationConfigurationEntity!=null)
			{
				toReturn.Add(_paymentIntegrationConfigurationEntity);
			}
			if(_paymentProviderEntity!=null)
			{
				toReturn.Add(_paymentProviderEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_paymentTransactionLogCollection);
			toReturn.Add(_paymentTransactionSplitCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionId)
		{
			return FetchUsingPK(paymentTransactionId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentTransactionId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentTransactionId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentTransactionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentTransactionId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentTransactionId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentTransactionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection GetMultiPaymentTransactionLogCollection(bool forceFetch)
		{
			return GetMultiPaymentTransactionLogCollection(forceFetch, _paymentTransactionLogCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionLogEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection GetMultiPaymentTransactionLogCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentTransactionLogCollection(forceFetch, _paymentTransactionLogCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection GetMultiPaymentTransactionLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentTransactionLogCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection GetMultiPaymentTransactionLogCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentTransactionLogCollection || forceFetch || _alwaysFetchPaymentTransactionLogCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentTransactionLogCollection);
				_paymentTransactionLogCollection.SuppressClearInGetMulti=!forceFetch;
				_paymentTransactionLogCollection.EntityFactoryToUse = entityFactoryToUse;
				_paymentTransactionLogCollection.GetMultiManyToOne(this, filter);
				_paymentTransactionLogCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentTransactionLogCollection = true;
			}
			return _paymentTransactionLogCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentTransactionLogCollection'. These settings will be taken into account
		/// when the property PaymentTransactionLogCollection is requested or GetMultiPaymentTransactionLogCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentTransactionLogCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentTransactionLogCollection.SortClauses=sortClauses;
			_paymentTransactionLogCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionSplitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionSplitEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection GetMultiPaymentTransactionSplitCollection(bool forceFetch)
		{
			return GetMultiPaymentTransactionSplitCollection(forceFetch, _paymentTransactionSplitCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionSplitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentTransactionSplitEntity'</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection GetMultiPaymentTransactionSplitCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentTransactionSplitCollection(forceFetch, _paymentTransactionSplitCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionSplitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection GetMultiPaymentTransactionSplitCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentTransactionSplitCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionSplitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection GetMultiPaymentTransactionSplitCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentTransactionSplitCollection || forceFetch || _alwaysFetchPaymentTransactionSplitCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentTransactionSplitCollection);
				_paymentTransactionSplitCollection.SuppressClearInGetMulti=!forceFetch;
				_paymentTransactionSplitCollection.EntityFactoryToUse = entityFactoryToUse;
				_paymentTransactionSplitCollection.GetMultiManyToOne(this, filter);
				_paymentTransactionSplitCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentTransactionSplitCollection = true;
			}
			return _paymentTransactionSplitCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentTransactionSplitCollection'. These settings will be taken into account
		/// when the property PaymentTransactionSplitCollection is requested or GetMultiPaymentTransactionSplitCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentTransactionSplitCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentTransactionSplitCollection.SortClauses=sortClauses;
			_paymentTransactionSplitCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrderEntity()
		{
			return GetSingleOrderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderEntity || forceFetch || _alwaysFetchOrderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderId);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderId);
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderEntity = newEntity;
				_alreadyFetchedOrderEntity = fetchResult;
			}
			return _orderEntity;
		}


		/// <summary> Retrieves the related entity of type 'PaymentIntegrationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentIntegrationConfigurationEntity' which is related to this entity.</returns>
		public PaymentIntegrationConfigurationEntity GetSinglePaymentIntegrationConfigurationEntity()
		{
			return GetSinglePaymentIntegrationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentIntegrationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentIntegrationConfigurationEntity' which is related to this entity.</returns>
		public virtual PaymentIntegrationConfigurationEntity GetSinglePaymentIntegrationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentIntegrationConfigurationEntity || forceFetch || _alwaysFetchPaymentIntegrationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
				PaymentIntegrationConfigurationEntity newEntity = new PaymentIntegrationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentIntegrationConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentIntegrationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentIntegrationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentIntegrationConfigurationEntity = newEntity;
				_alreadyFetchedPaymentIntegrationConfigurationEntity = fetchResult;
			}
			return _paymentIntegrationConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'PaymentProviderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentProviderEntity' which is related to this entity.</returns>
		public PaymentProviderEntity GetSinglePaymentProviderEntity()
		{
			return GetSinglePaymentProviderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentProviderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentProviderEntity' which is related to this entity.</returns>
		public virtual PaymentProviderEntity GetSinglePaymentProviderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentProviderEntity || forceFetch || _alwaysFetchPaymentProviderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentProviderEntityUsingPaymentProviderId);
				PaymentProviderEntity newEntity = new PaymentProviderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentProviderId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentProviderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentProviderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentProviderEntity = newEntity;
				_alreadyFetchedPaymentProviderEntity = fetchResult;
			}
			return _paymentProviderEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("OrderEntity", _orderEntity);
			toReturn.Add("PaymentIntegrationConfigurationEntity", _paymentIntegrationConfigurationEntity);
			toReturn.Add("PaymentProviderEntity", _paymentProviderEntity);
			toReturn.Add("PaymentTransactionLogCollection", _paymentTransactionLogCollection);
			toReturn.Add("PaymentTransactionSplitCollection", _paymentTransactionSplitCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="validator">The validator object for this PaymentTransactionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 paymentTransactionId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentTransactionId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_paymentTransactionLogCollection = new Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection();
			_paymentTransactionLogCollection.SetContainingEntityInfo(this, "PaymentTransactionEntity");

			_paymentTransactionSplitCollection = new Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection();
			_paymentTransactionSplitCollection.SetContainingEntityInfo(this, "PaymentTransactionEntity");
			_orderEntityReturnsNewIfNotFound = true;
			_paymentIntegrationConfigurationEntityReturnsNewIfNotFound = true;
			_paymentProviderEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentTransactionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReferenceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MerchantReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MerchantAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardSummary", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuthCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentEnvironment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProviderType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProviderName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProviderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentIntegrationConfigurationId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _orderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionRelations.OrderEntityUsingOrderIdStatic, true, signalRelatedEntity, "PaymentTransactionCollection", resetFKFields, new int[] { (int)PaymentTransactionFieldIndex.OrderId } );		
			_orderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderEntity(IEntityCore relatedEntity)
		{
			if(_orderEntity!=relatedEntity)
			{		
				DesetupSyncOrderEntity(true, true);
				_orderEntity = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionRelations.OrderEntityUsingOrderIdStatic, true, ref _alreadyFetchedOrderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentIntegrationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentIntegrationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentIntegrationConfigurationEntity, new PropertyChangedEventHandler( OnPaymentIntegrationConfigurationEntityPropertyChanged ), "PaymentIntegrationConfigurationEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, true, signalRelatedEntity, "PaymentTransactionCollection", resetFKFields, new int[] { (int)PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId } );		
			_paymentIntegrationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _paymentIntegrationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentIntegrationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_paymentIntegrationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncPaymentIntegrationConfigurationEntity(true, true);
				_paymentIntegrationConfigurationEntity = (PaymentIntegrationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentIntegrationConfigurationEntity, new PropertyChangedEventHandler( OnPaymentIntegrationConfigurationEntityPropertyChanged ), "PaymentIntegrationConfigurationEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionRelations.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic, true, ref _alreadyFetchedPaymentIntegrationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentIntegrationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentProviderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentProviderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentProviderEntity, new PropertyChangedEventHandler( OnPaymentProviderEntityPropertyChanged ), "PaymentProviderEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionRelations.PaymentProviderEntityUsingPaymentProviderIdStatic, true, signalRelatedEntity, "PaymentTransactionCollection", resetFKFields, new int[] { (int)PaymentTransactionFieldIndex.PaymentProviderId } );		
			_paymentProviderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _paymentProviderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentProviderEntity(IEntityCore relatedEntity)
		{
			if(_paymentProviderEntity!=relatedEntity)
			{		
				DesetupSyncPaymentProviderEntity(true, true);
				_paymentProviderEntity = (PaymentProviderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentProviderEntity, new PropertyChangedEventHandler( OnPaymentProviderEntityPropertyChanged ), "PaymentProviderEntity", Obymobi.Data.RelationClasses.StaticPaymentTransactionRelations.PaymentProviderEntityUsingPaymentProviderIdStatic, true, ref _alreadyFetchedPaymentProviderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentProviderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 paymentTransactionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentTransactionFieldIndex.PaymentTransactionId].ForcedCurrentValueWrite(paymentTransactionId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentTransactionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentTransactionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentTransactionRelations Relations
		{
			get	{ return new PaymentTransactionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentTransactionLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentTransactionLogCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection(), (IEntityRelation)GetRelationsForField("PaymentTransactionLogCollection")[0], (int)Obymobi.Data.EntityType.PaymentTransactionEntity, (int)Obymobi.Data.EntityType.PaymentTransactionLogEntity, 0, null, null, null, "PaymentTransactionLogCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentTransactionSplit' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentTransactionSplitCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection(), (IEntityRelation)GetRelationsForField("PaymentTransactionSplitCollection")[0], (int)Obymobi.Data.EntityType.PaymentTransactionEntity, (int)Obymobi.Data.EntityType.PaymentTransactionSplitEntity, 0, null, null, null, "PaymentTransactionSplitCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderEntity")[0], (int)Obymobi.Data.EntityType.PaymentTransactionEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentIntegrationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentIntegrationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentIntegrationConfigurationCollection(), (IEntityRelation)GetRelationsForField("PaymentIntegrationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.PaymentTransactionEntity, (int)Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity, 0, null, null, null, "PaymentIntegrationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentProvider'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentProviderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PaymentProviderCollection(), (IEntityRelation)GetRelationsForField("PaymentProviderEntity")[0], (int)Obymobi.Data.EntityType.PaymentTransactionEntity, (int)Obymobi.Data.EntityType.PaymentProviderEntity, 0, null, null, null, "PaymentProviderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PaymentTransactionId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentTransactionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 PaymentTransactionId
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionFieldIndex.PaymentTransactionId, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentTransactionId, value, true); }
		}

		/// <summary> The OrderId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)PaymentTransactionFieldIndex.OrderId, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.OrderId, value, true); }
		}

		/// <summary> The ReferenceId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."ReferenceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReferenceId
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.ReferenceId, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.ReferenceId, value, true); }
		}

		/// <summary> The MerchantReference property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."MerchantReference"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MerchantReference
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.MerchantReference, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.MerchantReference, value, true); }
		}

		/// <summary> The PaymentMethod property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PaymentMethod
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.PaymentMethod, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentMethod, value, true); }
		}

		/// <summary> The Status property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."Status"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PaymentTransactionStatus Status
		{
			get { return (Obymobi.Enums.PaymentTransactionStatus)GetValue((int)PaymentTransactionFieldIndex.Status, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.Status, value, true); }
		}

		/// <summary> The PaymentData property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentData"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentData
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.PaymentData, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentData, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)PaymentTransactionFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentTransactionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The MerchantAccount property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."MerchantAccount"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MerchantAccount
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.MerchantAccount, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.MerchantAccount, value, true); }
		}

		/// <summary> The CardSummary property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."CardSummary"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardSummary
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.CardSummary, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.CardSummary, value, true); }
		}

		/// <summary> The AuthCode property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."AuthCode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AuthCode
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.AuthCode, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.AuthCode, value, true); }
		}

		/// <summary> The PaymentEnvironment property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentEnvironment"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PaymentProviderEnvironment PaymentEnvironment
		{
			get { return (Obymobi.Enums.PaymentProviderEnvironment)GetValue((int)PaymentTransactionFieldIndex.PaymentEnvironment, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentEnvironment, value, true); }
		}

		/// <summary> The PaymentProviderType property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentProviderType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.PaymentProviderType PaymentProviderType
		{
			get { return (Obymobi.Enums.PaymentProviderType)GetValue((int)PaymentTransactionFieldIndex.PaymentProviderType, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentProviderType, value, true); }
		}

		/// <summary> The PaymentProviderName property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentProviderName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProviderName
		{
			get { return (System.String)GetValue((int)PaymentTransactionFieldIndex.PaymentProviderName, true); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentProviderName, value, true); }
		}

		/// <summary> The PaymentProviderId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentProviderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PaymentProviderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentTransactionFieldIndex.PaymentProviderId, false); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentProviderId, value, true); }
		}

		/// <summary> The PaymentIntegrationConfigurationId property of the Entity PaymentTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PaymentTransaction"."PaymentIntegrationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PaymentIntegrationConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId, false); }
			set	{ SetValue((int)PaymentTransactionFieldIndex.PaymentIntegrationConfigurationId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PaymentTransactionLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentTransactionLogCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionLogCollection PaymentTransactionLogCollection
		{
			get	{ return GetMultiPaymentTransactionLogCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentTransactionLogCollection. When set to true, PaymentTransactionLogCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentTransactionLogCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentTransactionLogCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentTransactionLogCollection
		{
			get	{ return _alwaysFetchPaymentTransactionLogCollection; }
			set	{ _alwaysFetchPaymentTransactionLogCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentTransactionLogCollection already has been fetched. Setting this property to false when PaymentTransactionLogCollection has been fetched
		/// will clear the PaymentTransactionLogCollection collection well. Setting this property to true while PaymentTransactionLogCollection hasn't been fetched disables lazy loading for PaymentTransactionLogCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentTransactionLogCollection
		{
			get { return _alreadyFetchedPaymentTransactionLogCollection;}
			set 
			{
				if(_alreadyFetchedPaymentTransactionLogCollection && !value && (_paymentTransactionLogCollection != null))
				{
					_paymentTransactionLogCollection.Clear();
				}
				_alreadyFetchedPaymentTransactionLogCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentTransactionSplitEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentTransactionSplitCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PaymentTransactionSplitCollection PaymentTransactionSplitCollection
		{
			get	{ return GetMultiPaymentTransactionSplitCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentTransactionSplitCollection. When set to true, PaymentTransactionSplitCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentTransactionSplitCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentTransactionSplitCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentTransactionSplitCollection
		{
			get	{ return _alwaysFetchPaymentTransactionSplitCollection; }
			set	{ _alwaysFetchPaymentTransactionSplitCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentTransactionSplitCollection already has been fetched. Setting this property to false when PaymentTransactionSplitCollection has been fetched
		/// will clear the PaymentTransactionSplitCollection collection well. Setting this property to true while PaymentTransactionSplitCollection hasn't been fetched disables lazy loading for PaymentTransactionSplitCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentTransactionSplitCollection
		{
			get { return _alreadyFetchedPaymentTransactionSplitCollection;}
			set 
			{
				if(_alreadyFetchedPaymentTransactionSplitCollection && !value && (_paymentTransactionSplitCollection != null))
				{
					_paymentTransactionSplitCollection.Clear();
				}
				_alreadyFetchedPaymentTransactionSplitCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderEntity OrderEntity
		{
			get	{ return GetSingleOrderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentTransactionCollection", "OrderEntity", _orderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderEntity. When set to true, OrderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderEntity
		{
			get	{ return _alwaysFetchOrderEntity; }
			set	{ _alwaysFetchOrderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderEntity already has been fetched. Setting this property to false when OrderEntity has been fetched
		/// will set OrderEntity to null as well. Setting this property to true while OrderEntity hasn't been fetched disables lazy loading for OrderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderEntity
		{
			get { return _alreadyFetchedOrderEntity;}
			set 
			{
				if(_alreadyFetchedOrderEntity && !value)
				{
					this.OrderEntity = null;
				}
				_alreadyFetchedOrderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderEntity is not found
		/// in the database. When set to true, OrderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderEntityReturnsNewIfNotFound
		{
			get	{ return _orderEntityReturnsNewIfNotFound; }
			set { _orderEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentIntegrationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentIntegrationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PaymentIntegrationConfigurationEntity PaymentIntegrationConfigurationEntity
		{
			get	{ return GetSinglePaymentIntegrationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentIntegrationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentTransactionCollection", "PaymentIntegrationConfigurationEntity", _paymentIntegrationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentIntegrationConfigurationEntity. When set to true, PaymentIntegrationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentIntegrationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSinglePaymentIntegrationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentIntegrationConfigurationEntity
		{
			get	{ return _alwaysFetchPaymentIntegrationConfigurationEntity; }
			set	{ _alwaysFetchPaymentIntegrationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentIntegrationConfigurationEntity already has been fetched. Setting this property to false when PaymentIntegrationConfigurationEntity has been fetched
		/// will set PaymentIntegrationConfigurationEntity to null as well. Setting this property to true while PaymentIntegrationConfigurationEntity hasn't been fetched disables lazy loading for PaymentIntegrationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentIntegrationConfigurationEntity
		{
			get { return _alreadyFetchedPaymentIntegrationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedPaymentIntegrationConfigurationEntity && !value)
				{
					this.PaymentIntegrationConfigurationEntity = null;
				}
				_alreadyFetchedPaymentIntegrationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentIntegrationConfigurationEntity is not found
		/// in the database. When set to true, PaymentIntegrationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PaymentIntegrationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _paymentIntegrationConfigurationEntityReturnsNewIfNotFound; }
			set { _paymentIntegrationConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentProviderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentProviderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PaymentProviderEntity PaymentProviderEntity
		{
			get	{ return GetSinglePaymentProviderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentProviderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentTransactionCollection", "PaymentProviderEntity", _paymentProviderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentProviderEntity. When set to true, PaymentProviderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentProviderEntity is accessed. You can always execute a forced fetch by calling GetSinglePaymentProviderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentProviderEntity
		{
			get	{ return _alwaysFetchPaymentProviderEntity; }
			set	{ _alwaysFetchPaymentProviderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentProviderEntity already has been fetched. Setting this property to false when PaymentProviderEntity has been fetched
		/// will set PaymentProviderEntity to null as well. Setting this property to true while PaymentProviderEntity hasn't been fetched disables lazy loading for PaymentProviderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentProviderEntity
		{
			get { return _alreadyFetchedPaymentProviderEntity;}
			set 
			{
				if(_alreadyFetchedPaymentProviderEntity && !value)
				{
					this.PaymentProviderEntity = null;
				}
				_alreadyFetchedPaymentProviderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentProviderEntity is not found
		/// in the database. When set to true, PaymentProviderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PaymentProviderEntityReturnsNewIfNotFound
		{
			get	{ return _paymentProviderEntityReturnsNewIfNotFound; }
			set { _paymentProviderEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.PaymentTransactionEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
