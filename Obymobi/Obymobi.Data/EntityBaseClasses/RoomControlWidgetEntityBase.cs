﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'RoomControlWidget'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class RoomControlWidgetEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "RoomControlWidgetEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection	_roomControlWidgetLanguageCollection;
		private bool	_alwaysFetchRoomControlWidgetLanguageCollection, _alreadyFetchedRoomControlWidgetLanguageCollection;
		private InfraredConfigurationEntity _infraredConfigurationEntity;
		private bool	_alwaysFetchInfraredConfigurationEntity, _alreadyFetchedInfraredConfigurationEntity, _infraredConfigurationEntityReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity;
		private bool	_alwaysFetchRoomControlComponentEntity, _alreadyFetchedRoomControlComponentEntity, _roomControlComponentEntityReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity_;
		private bool	_alwaysFetchRoomControlComponentEntity_, _alreadyFetchedRoomControlComponentEntity_, _roomControlComponentEntity_ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity__;
		private bool	_alwaysFetchRoomControlComponentEntity__, _alreadyFetchedRoomControlComponentEntity__, _roomControlComponentEntity__ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity___;
		private bool	_alwaysFetchRoomControlComponentEntity___, _alreadyFetchedRoomControlComponentEntity___, _roomControlComponentEntity___ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity____;
		private bool	_alwaysFetchRoomControlComponentEntity____, _alreadyFetchedRoomControlComponentEntity____, _roomControlComponentEntity____ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity_____;
		private bool	_alwaysFetchRoomControlComponentEntity_____, _alreadyFetchedRoomControlComponentEntity_____, _roomControlComponentEntity_____ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity______;
		private bool	_alwaysFetchRoomControlComponentEntity______, _alreadyFetchedRoomControlComponentEntity______, _roomControlComponentEntity______ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity_______;
		private bool	_alwaysFetchRoomControlComponentEntity_______, _alreadyFetchedRoomControlComponentEntity_______, _roomControlComponentEntity_______ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity________;
		private bool	_alwaysFetchRoomControlComponentEntity________, _alreadyFetchedRoomControlComponentEntity________, _roomControlComponentEntity________ReturnsNewIfNotFound;
		private RoomControlComponentEntity _roomControlComponentEntity_________;
		private bool	_alwaysFetchRoomControlComponentEntity_________, _alreadyFetchedRoomControlComponentEntity_________, _roomControlComponentEntity_________ReturnsNewIfNotFound;
		private RoomControlSectionEntity _roomControlSectionEntity;
		private bool	_alwaysFetchRoomControlSectionEntity, _alreadyFetchedRoomControlSectionEntity, _roomControlSectionEntityReturnsNewIfNotFound;
		private RoomControlSectionItemEntity _roomControlSectionItemEntity;
		private bool	_alwaysFetchRoomControlSectionItemEntity, _alreadyFetchedRoomControlSectionItemEntity, _roomControlSectionItemEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name InfraredConfigurationEntity</summary>
			public static readonly string InfraredConfigurationEntity = "InfraredConfigurationEntity";
			/// <summary>Member name RoomControlComponentEntity</summary>
			public static readonly string RoomControlComponentEntity = "RoomControlComponentEntity";
			/// <summary>Member name RoomControlComponentEntity_</summary>
			public static readonly string RoomControlComponentEntity_ = "RoomControlComponentEntity_";
			/// <summary>Member name RoomControlComponentEntity__</summary>
			public static readonly string RoomControlComponentEntity__ = "RoomControlComponentEntity__";
			/// <summary>Member name RoomControlComponentEntity___</summary>
			public static readonly string RoomControlComponentEntity___ = "RoomControlComponentEntity___";
			/// <summary>Member name RoomControlComponentEntity____</summary>
			public static readonly string RoomControlComponentEntity____ = "RoomControlComponentEntity____";
			/// <summary>Member name RoomControlComponentEntity_____</summary>
			public static readonly string RoomControlComponentEntity_____ = "RoomControlComponentEntity_____";
			/// <summary>Member name RoomControlComponentEntity______</summary>
			public static readonly string RoomControlComponentEntity______ = "RoomControlComponentEntity______";
			/// <summary>Member name RoomControlComponentEntity_______</summary>
			public static readonly string RoomControlComponentEntity_______ = "RoomControlComponentEntity_______";
			/// <summary>Member name RoomControlComponentEntity________</summary>
			public static readonly string RoomControlComponentEntity________ = "RoomControlComponentEntity________";
			/// <summary>Member name RoomControlComponentEntity_________</summary>
			public static readonly string RoomControlComponentEntity_________ = "RoomControlComponentEntity_________";
			/// <summary>Member name RoomControlSectionEntity</summary>
			public static readonly string RoomControlSectionEntity = "RoomControlSectionEntity";
			/// <summary>Member name RoomControlSectionItemEntity</summary>
			public static readonly string RoomControlSectionItemEntity = "RoomControlSectionItemEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name RoomControlWidgetLanguageCollection</summary>
			public static readonly string RoomControlWidgetLanguageCollection = "RoomControlWidgetLanguageCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RoomControlWidgetEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected RoomControlWidgetEntityBase() :base("RoomControlWidgetEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		protected RoomControlWidgetEntityBase(System.Int32 roomControlWidgetId):base("RoomControlWidgetEntity")
		{
			InitClassFetch(roomControlWidgetId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected RoomControlWidgetEntityBase(System.Int32 roomControlWidgetId, IPrefetchPath prefetchPathToUse): base("RoomControlWidgetEntity")
		{
			InitClassFetch(roomControlWidgetId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="validator">The custom validator object for this RoomControlWidgetEntity</param>
		protected RoomControlWidgetEntityBase(System.Int32 roomControlWidgetId, IValidator validator):base("RoomControlWidgetEntity")
		{
			InitClassFetch(roomControlWidgetId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlWidgetEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_roomControlWidgetLanguageCollection = (Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection)info.GetValue("_roomControlWidgetLanguageCollection", typeof(Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection));
			_alwaysFetchRoomControlWidgetLanguageCollection = info.GetBoolean("_alwaysFetchRoomControlWidgetLanguageCollection");
			_alreadyFetchedRoomControlWidgetLanguageCollection = info.GetBoolean("_alreadyFetchedRoomControlWidgetLanguageCollection");
			_infraredConfigurationEntity = (InfraredConfigurationEntity)info.GetValue("_infraredConfigurationEntity", typeof(InfraredConfigurationEntity));
			if(_infraredConfigurationEntity!=null)
			{
				_infraredConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_infraredConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_infraredConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchInfraredConfigurationEntity = info.GetBoolean("_alwaysFetchInfraredConfigurationEntity");
			_alreadyFetchedInfraredConfigurationEntity = info.GetBoolean("_alreadyFetchedInfraredConfigurationEntity");

			_roomControlComponentEntity = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity!=null)
			{
				_roomControlComponentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity = info.GetBoolean("_alwaysFetchRoomControlComponentEntity");
			_alreadyFetchedRoomControlComponentEntity = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity");

			_roomControlComponentEntity_ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity_", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity_!=null)
			{
				_roomControlComponentEntity_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity_ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity_ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity_ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity_");
			_alreadyFetchedRoomControlComponentEntity_ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity_");

			_roomControlComponentEntity__ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity__", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity__!=null)
			{
				_roomControlComponentEntity__.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity__ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity__ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity__ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity__");
			_alreadyFetchedRoomControlComponentEntity__ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity__");

			_roomControlComponentEntity___ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity___", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity___!=null)
			{
				_roomControlComponentEntity___.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity___ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity___ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity___ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity___");
			_alreadyFetchedRoomControlComponentEntity___ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity___");

			_roomControlComponentEntity____ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity____", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity____!=null)
			{
				_roomControlComponentEntity____.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity____ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity____ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity____ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity____");
			_alreadyFetchedRoomControlComponentEntity____ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity____");

			_roomControlComponentEntity_____ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity_____", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity_____!=null)
			{
				_roomControlComponentEntity_____.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity_____ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity_____ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity_____ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity_____");
			_alreadyFetchedRoomControlComponentEntity_____ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity_____");

			_roomControlComponentEntity______ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity______", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity______!=null)
			{
				_roomControlComponentEntity______.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity______ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity______ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity______ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity______");
			_alreadyFetchedRoomControlComponentEntity______ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity______");

			_roomControlComponentEntity_______ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity_______", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity_______!=null)
			{
				_roomControlComponentEntity_______.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity_______ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity_______ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity_______ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity_______");
			_alreadyFetchedRoomControlComponentEntity_______ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity_______");

			_roomControlComponentEntity________ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity________", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity________!=null)
			{
				_roomControlComponentEntity________.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity________ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity________ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity________ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity________");
			_alreadyFetchedRoomControlComponentEntity________ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity________");

			_roomControlComponentEntity_________ = (RoomControlComponentEntity)info.GetValue("_roomControlComponentEntity_________", typeof(RoomControlComponentEntity));
			if(_roomControlComponentEntity_________!=null)
			{
				_roomControlComponentEntity_________.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlComponentEntity_________ReturnsNewIfNotFound = info.GetBoolean("_roomControlComponentEntity_________ReturnsNewIfNotFound");
			_alwaysFetchRoomControlComponentEntity_________ = info.GetBoolean("_alwaysFetchRoomControlComponentEntity_________");
			_alreadyFetchedRoomControlComponentEntity_________ = info.GetBoolean("_alreadyFetchedRoomControlComponentEntity_________");

			_roomControlSectionEntity = (RoomControlSectionEntity)info.GetValue("_roomControlSectionEntity", typeof(RoomControlSectionEntity));
			if(_roomControlSectionEntity!=null)
			{
				_roomControlSectionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionEntity = info.GetBoolean("_alwaysFetchRoomControlSectionEntity");
			_alreadyFetchedRoomControlSectionEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionEntity");

			_roomControlSectionItemEntity = (RoomControlSectionItemEntity)info.GetValue("_roomControlSectionItemEntity", typeof(RoomControlSectionItemEntity));
			if(_roomControlSectionItemEntity!=null)
			{
				_roomControlSectionItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_roomControlSectionItemEntityReturnsNewIfNotFound = info.GetBoolean("_roomControlSectionItemEntityReturnsNewIfNotFound");
			_alwaysFetchRoomControlSectionItemEntity = info.GetBoolean("_alwaysFetchRoomControlSectionItemEntity");
			_alreadyFetchedRoomControlSectionItemEntity = info.GetBoolean("_alreadyFetchedRoomControlSectionItemEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RoomControlWidgetFieldIndex)fieldIndex)
			{
				case RoomControlWidgetFieldIndex.RoomControlSectionId:
					DesetupSyncRoomControlSectionEntity(true, false);
					_alreadyFetchedRoomControlSectionEntity = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId1:
					DesetupSyncRoomControlComponentEntity(true, false);
					_alreadyFetchedRoomControlComponentEntity = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlSectionItemId:
					DesetupSyncRoomControlSectionItemEntity(true, false);
					_alreadyFetchedRoomControlSectionItemEntity = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId10:
					DesetupSyncRoomControlComponentEntity_(true, false);
					_alreadyFetchedRoomControlComponentEntity_ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId2:
					DesetupSyncRoomControlComponentEntity__(true, false);
					_alreadyFetchedRoomControlComponentEntity__ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId3:
					DesetupSyncRoomControlComponentEntity___(true, false);
					_alreadyFetchedRoomControlComponentEntity___ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId4:
					DesetupSyncRoomControlComponentEntity____(true, false);
					_alreadyFetchedRoomControlComponentEntity____ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId5:
					DesetupSyncRoomControlComponentEntity_____(true, false);
					_alreadyFetchedRoomControlComponentEntity_____ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId6:
					DesetupSyncRoomControlComponentEntity______(true, false);
					_alreadyFetchedRoomControlComponentEntity______ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId7:
					DesetupSyncRoomControlComponentEntity_______(true, false);
					_alreadyFetchedRoomControlComponentEntity_______ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId8:
					DesetupSyncRoomControlComponentEntity________(true, false);
					_alreadyFetchedRoomControlComponentEntity________ = false;
					break;
				case RoomControlWidgetFieldIndex.RoomControlComponentId9:
					DesetupSyncRoomControlComponentEntity_________(true, false);
					_alreadyFetchedRoomControlComponentEntity_________ = false;
					break;
				case RoomControlWidgetFieldIndex.InfraredConfigurationId:
					DesetupSyncInfraredConfigurationEntity(true, false);
					_alreadyFetchedInfraredConfigurationEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedRoomControlWidgetLanguageCollection = (_roomControlWidgetLanguageCollection.Count > 0);
			_alreadyFetchedInfraredConfigurationEntity = (_infraredConfigurationEntity != null);
			_alreadyFetchedRoomControlComponentEntity = (_roomControlComponentEntity != null);
			_alreadyFetchedRoomControlComponentEntity_ = (_roomControlComponentEntity_ != null);
			_alreadyFetchedRoomControlComponentEntity__ = (_roomControlComponentEntity__ != null);
			_alreadyFetchedRoomControlComponentEntity___ = (_roomControlComponentEntity___ != null);
			_alreadyFetchedRoomControlComponentEntity____ = (_roomControlComponentEntity____ != null);
			_alreadyFetchedRoomControlComponentEntity_____ = (_roomControlComponentEntity_____ != null);
			_alreadyFetchedRoomControlComponentEntity______ = (_roomControlComponentEntity______ != null);
			_alreadyFetchedRoomControlComponentEntity_______ = (_roomControlComponentEntity_______ != null);
			_alreadyFetchedRoomControlComponentEntity________ = (_roomControlComponentEntity________ != null);
			_alreadyFetchedRoomControlComponentEntity_________ = (_roomControlComponentEntity_________ != null);
			_alreadyFetchedRoomControlSectionEntity = (_roomControlSectionEntity != null);
			_alreadyFetchedRoomControlSectionItemEntity = (_roomControlSectionItemEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					toReturn.Add(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
					break;
				case "RoomControlComponentEntity":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId1);
					break;
				case "RoomControlComponentEntity_":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId10);
					break;
				case "RoomControlComponentEntity__":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId2);
					break;
				case "RoomControlComponentEntity___":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId3);
					break;
				case "RoomControlComponentEntity____":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId4);
					break;
				case "RoomControlComponentEntity_____":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId5);
					break;
				case "RoomControlComponentEntity______":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId6);
					break;
				case "RoomControlComponentEntity_______":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId7);
					break;
				case "RoomControlComponentEntity________":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId8);
					break;
				case "RoomControlComponentEntity_________":
					toReturn.Add(Relations.RoomControlComponentEntityUsingRoomControlComponentId9);
					break;
				case "RoomControlSectionEntity":
					toReturn.Add(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
					break;
				case "RoomControlSectionItemEntity":
					toReturn.Add(Relations.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingRoomControlWidgetId);
					break;
				case "RoomControlWidgetLanguageCollection":
					toReturn.Add(Relations.RoomControlWidgetLanguageEntityUsingRoomControlWidgetId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_roomControlWidgetLanguageCollection", (!this.MarkedForDeletion?_roomControlWidgetLanguageCollection:null));
			info.AddValue("_alwaysFetchRoomControlWidgetLanguageCollection", _alwaysFetchRoomControlWidgetLanguageCollection);
			info.AddValue("_alreadyFetchedRoomControlWidgetLanguageCollection", _alreadyFetchedRoomControlWidgetLanguageCollection);
			info.AddValue("_infraredConfigurationEntity", (!this.MarkedForDeletion?_infraredConfigurationEntity:null));
			info.AddValue("_infraredConfigurationEntityReturnsNewIfNotFound", _infraredConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInfraredConfigurationEntity", _alwaysFetchInfraredConfigurationEntity);
			info.AddValue("_alreadyFetchedInfraredConfigurationEntity", _alreadyFetchedInfraredConfigurationEntity);
			info.AddValue("_roomControlComponentEntity", (!this.MarkedForDeletion?_roomControlComponentEntity:null));
			info.AddValue("_roomControlComponentEntityReturnsNewIfNotFound", _roomControlComponentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity", _alwaysFetchRoomControlComponentEntity);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity", _alreadyFetchedRoomControlComponentEntity);
			info.AddValue("_roomControlComponentEntity_", (!this.MarkedForDeletion?_roomControlComponentEntity_:null));
			info.AddValue("_roomControlComponentEntity_ReturnsNewIfNotFound", _roomControlComponentEntity_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity_", _alwaysFetchRoomControlComponentEntity_);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity_", _alreadyFetchedRoomControlComponentEntity_);
			info.AddValue("_roomControlComponentEntity__", (!this.MarkedForDeletion?_roomControlComponentEntity__:null));
			info.AddValue("_roomControlComponentEntity__ReturnsNewIfNotFound", _roomControlComponentEntity__ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity__", _alwaysFetchRoomControlComponentEntity__);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity__", _alreadyFetchedRoomControlComponentEntity__);
			info.AddValue("_roomControlComponentEntity___", (!this.MarkedForDeletion?_roomControlComponentEntity___:null));
			info.AddValue("_roomControlComponentEntity___ReturnsNewIfNotFound", _roomControlComponentEntity___ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity___", _alwaysFetchRoomControlComponentEntity___);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity___", _alreadyFetchedRoomControlComponentEntity___);
			info.AddValue("_roomControlComponentEntity____", (!this.MarkedForDeletion?_roomControlComponentEntity____:null));
			info.AddValue("_roomControlComponentEntity____ReturnsNewIfNotFound", _roomControlComponentEntity____ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity____", _alwaysFetchRoomControlComponentEntity____);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity____", _alreadyFetchedRoomControlComponentEntity____);
			info.AddValue("_roomControlComponentEntity_____", (!this.MarkedForDeletion?_roomControlComponentEntity_____:null));
			info.AddValue("_roomControlComponentEntity_____ReturnsNewIfNotFound", _roomControlComponentEntity_____ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity_____", _alwaysFetchRoomControlComponentEntity_____);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity_____", _alreadyFetchedRoomControlComponentEntity_____);
			info.AddValue("_roomControlComponentEntity______", (!this.MarkedForDeletion?_roomControlComponentEntity______:null));
			info.AddValue("_roomControlComponentEntity______ReturnsNewIfNotFound", _roomControlComponentEntity______ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity______", _alwaysFetchRoomControlComponentEntity______);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity______", _alreadyFetchedRoomControlComponentEntity______);
			info.AddValue("_roomControlComponentEntity_______", (!this.MarkedForDeletion?_roomControlComponentEntity_______:null));
			info.AddValue("_roomControlComponentEntity_______ReturnsNewIfNotFound", _roomControlComponentEntity_______ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity_______", _alwaysFetchRoomControlComponentEntity_______);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity_______", _alreadyFetchedRoomControlComponentEntity_______);
			info.AddValue("_roomControlComponentEntity________", (!this.MarkedForDeletion?_roomControlComponentEntity________:null));
			info.AddValue("_roomControlComponentEntity________ReturnsNewIfNotFound", _roomControlComponentEntity________ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity________", _alwaysFetchRoomControlComponentEntity________);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity________", _alreadyFetchedRoomControlComponentEntity________);
			info.AddValue("_roomControlComponentEntity_________", (!this.MarkedForDeletion?_roomControlComponentEntity_________:null));
			info.AddValue("_roomControlComponentEntity_________ReturnsNewIfNotFound", _roomControlComponentEntity_________ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlComponentEntity_________", _alwaysFetchRoomControlComponentEntity_________);
			info.AddValue("_alreadyFetchedRoomControlComponentEntity_________", _alreadyFetchedRoomControlComponentEntity_________);
			info.AddValue("_roomControlSectionEntity", (!this.MarkedForDeletion?_roomControlSectionEntity:null));
			info.AddValue("_roomControlSectionEntityReturnsNewIfNotFound", _roomControlSectionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionEntity", _alwaysFetchRoomControlSectionEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionEntity", _alreadyFetchedRoomControlSectionEntity);
			info.AddValue("_roomControlSectionItemEntity", (!this.MarkedForDeletion?_roomControlSectionItemEntity:null));
			info.AddValue("_roomControlSectionItemEntityReturnsNewIfNotFound", _roomControlSectionItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRoomControlSectionItemEntity", _alwaysFetchRoomControlSectionItemEntity);
			info.AddValue("_alreadyFetchedRoomControlSectionItemEntity", _alreadyFetchedRoomControlSectionItemEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "InfraredConfigurationEntity":
					_alreadyFetchedInfraredConfigurationEntity = true;
					this.InfraredConfigurationEntity = (InfraredConfigurationEntity)entity;
					break;
				case "RoomControlComponentEntity":
					_alreadyFetchedRoomControlComponentEntity = true;
					this.RoomControlComponentEntity = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity_":
					_alreadyFetchedRoomControlComponentEntity_ = true;
					this.RoomControlComponentEntity_ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity__":
					_alreadyFetchedRoomControlComponentEntity__ = true;
					this.RoomControlComponentEntity__ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity___":
					_alreadyFetchedRoomControlComponentEntity___ = true;
					this.RoomControlComponentEntity___ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity____":
					_alreadyFetchedRoomControlComponentEntity____ = true;
					this.RoomControlComponentEntity____ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity_____":
					_alreadyFetchedRoomControlComponentEntity_____ = true;
					this.RoomControlComponentEntity_____ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity______":
					_alreadyFetchedRoomControlComponentEntity______ = true;
					this.RoomControlComponentEntity______ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity_______":
					_alreadyFetchedRoomControlComponentEntity_______ = true;
					this.RoomControlComponentEntity_______ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity________":
					_alreadyFetchedRoomControlComponentEntity________ = true;
					this.RoomControlComponentEntity________ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlComponentEntity_________":
					_alreadyFetchedRoomControlComponentEntity_________ = true;
					this.RoomControlComponentEntity_________ = (RoomControlComponentEntity)entity;
					break;
				case "RoomControlSectionEntity":
					_alreadyFetchedRoomControlSectionEntity = true;
					this.RoomControlSectionEntity = (RoomControlSectionEntity)entity;
					break;
				case "RoomControlSectionItemEntity":
					_alreadyFetchedRoomControlSectionItemEntity = true;
					this.RoomControlSectionItemEntity = (RoomControlSectionItemEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "RoomControlWidgetLanguageCollection":
					_alreadyFetchedRoomControlWidgetLanguageCollection = true;
					if(entity!=null)
					{
						this.RoomControlWidgetLanguageCollection.Add((RoomControlWidgetLanguageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					SetupSyncInfraredConfigurationEntity(relatedEntity);
					break;
				case "RoomControlComponentEntity":
					SetupSyncRoomControlComponentEntity(relatedEntity);
					break;
				case "RoomControlComponentEntity_":
					SetupSyncRoomControlComponentEntity_(relatedEntity);
					break;
				case "RoomControlComponentEntity__":
					SetupSyncRoomControlComponentEntity__(relatedEntity);
					break;
				case "RoomControlComponentEntity___":
					SetupSyncRoomControlComponentEntity___(relatedEntity);
					break;
				case "RoomControlComponentEntity____":
					SetupSyncRoomControlComponentEntity____(relatedEntity);
					break;
				case "RoomControlComponentEntity_____":
					SetupSyncRoomControlComponentEntity_____(relatedEntity);
					break;
				case "RoomControlComponentEntity______":
					SetupSyncRoomControlComponentEntity______(relatedEntity);
					break;
				case "RoomControlComponentEntity_______":
					SetupSyncRoomControlComponentEntity_______(relatedEntity);
					break;
				case "RoomControlComponentEntity________":
					SetupSyncRoomControlComponentEntity________(relatedEntity);
					break;
				case "RoomControlComponentEntity_________":
					SetupSyncRoomControlComponentEntity_________(relatedEntity);
					break;
				case "RoomControlSectionEntity":
					SetupSyncRoomControlSectionEntity(relatedEntity);
					break;
				case "RoomControlSectionItemEntity":
					SetupSyncRoomControlSectionItemEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "RoomControlWidgetLanguageCollection":
					_roomControlWidgetLanguageCollection.Add((RoomControlWidgetLanguageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "InfraredConfigurationEntity":
					DesetupSyncInfraredConfigurationEntity(false, true);
					break;
				case "RoomControlComponentEntity":
					DesetupSyncRoomControlComponentEntity(false, true);
					break;
				case "RoomControlComponentEntity_":
					DesetupSyncRoomControlComponentEntity_(false, true);
					break;
				case "RoomControlComponentEntity__":
					DesetupSyncRoomControlComponentEntity__(false, true);
					break;
				case "RoomControlComponentEntity___":
					DesetupSyncRoomControlComponentEntity___(false, true);
					break;
				case "RoomControlComponentEntity____":
					DesetupSyncRoomControlComponentEntity____(false, true);
					break;
				case "RoomControlComponentEntity_____":
					DesetupSyncRoomControlComponentEntity_____(false, true);
					break;
				case "RoomControlComponentEntity______":
					DesetupSyncRoomControlComponentEntity______(false, true);
					break;
				case "RoomControlComponentEntity_______":
					DesetupSyncRoomControlComponentEntity_______(false, true);
					break;
				case "RoomControlComponentEntity________":
					DesetupSyncRoomControlComponentEntity________(false, true);
					break;
				case "RoomControlComponentEntity_________":
					DesetupSyncRoomControlComponentEntity_________(false, true);
					break;
				case "RoomControlSectionEntity":
					DesetupSyncRoomControlSectionEntity(false, true);
					break;
				case "RoomControlSectionItemEntity":
					DesetupSyncRoomControlSectionItemEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoomControlWidgetLanguageCollection":
					this.PerformRelatedEntityRemoval(_roomControlWidgetLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_infraredConfigurationEntity!=null)
			{
				toReturn.Add(_infraredConfigurationEntity);
			}
			if(_roomControlComponentEntity!=null)
			{
				toReturn.Add(_roomControlComponentEntity);
			}
			if(_roomControlComponentEntity_!=null)
			{
				toReturn.Add(_roomControlComponentEntity_);
			}
			if(_roomControlComponentEntity__!=null)
			{
				toReturn.Add(_roomControlComponentEntity__);
			}
			if(_roomControlComponentEntity___!=null)
			{
				toReturn.Add(_roomControlComponentEntity___);
			}
			if(_roomControlComponentEntity____!=null)
			{
				toReturn.Add(_roomControlComponentEntity____);
			}
			if(_roomControlComponentEntity_____!=null)
			{
				toReturn.Add(_roomControlComponentEntity_____);
			}
			if(_roomControlComponentEntity______!=null)
			{
				toReturn.Add(_roomControlComponentEntity______);
			}
			if(_roomControlComponentEntity_______!=null)
			{
				toReturn.Add(_roomControlComponentEntity_______);
			}
			if(_roomControlComponentEntity________!=null)
			{
				toReturn.Add(_roomControlComponentEntity________);
			}
			if(_roomControlComponentEntity_________!=null)
			{
				toReturn.Add(_roomControlComponentEntity_________);
			}
			if(_roomControlSectionEntity!=null)
			{
				toReturn.Add(_roomControlSectionEntity);
			}
			if(_roomControlSectionItemEntity!=null)
			{
				toReturn.Add(_roomControlSectionItemEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_roomControlWidgetLanguageCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlWidgetId)
		{
			return FetchUsingPK(roomControlWidgetId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlWidgetId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(roomControlWidgetId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlWidgetId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(roomControlWidgetId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 roomControlWidgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(roomControlWidgetId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RoomControlWidgetId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RoomControlWidgetRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch)
		{
			return GetMultiRoomControlWidgetLanguageCollection(forceFetch, _roomControlWidgetLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoomControlWidgetLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoomControlWidgetLanguageCollection(forceFetch, _roomControlWidgetLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoomControlWidgetLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection GetMultiRoomControlWidgetLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoomControlWidgetLanguageCollection || forceFetch || _alwaysFetchRoomControlWidgetLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roomControlWidgetLanguageCollection);
				_roomControlWidgetLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_roomControlWidgetLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_roomControlWidgetLanguageCollection.GetMultiManyToOne(null, this, filter);
				_roomControlWidgetLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedRoomControlWidgetLanguageCollection = true;
			}
			return _roomControlWidgetLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoomControlWidgetLanguageCollection'. These settings will be taken into account
		/// when the property RoomControlWidgetLanguageCollection is requested or GetMultiRoomControlWidgetLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoomControlWidgetLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roomControlWidgetLanguageCollection.SortClauses=sortClauses;
			_roomControlWidgetLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public InfraredConfigurationEntity GetSingleInfraredConfigurationEntity()
		{
			return GetSingleInfraredConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'InfraredConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InfraredConfigurationEntity' which is related to this entity.</returns>
		public virtual InfraredConfigurationEntity GetSingleInfraredConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedInfraredConfigurationEntity || forceFetch || _alwaysFetchInfraredConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);
				InfraredConfigurationEntity newEntity = new InfraredConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InfraredConfigurationId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InfraredConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_infraredConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InfraredConfigurationEntity = newEntity;
				_alreadyFetchedInfraredConfigurationEntity = fetchResult;
			}
			return _infraredConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity()
		{
			return GetSingleRoomControlComponentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity || forceFetch || _alwaysFetchRoomControlComponentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId1);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId1.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity = newEntity;
				_alreadyFetchedRoomControlComponentEntity = fetchResult;
			}
			return _roomControlComponentEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity_()
		{
			return GetSingleRoomControlComponentEntity_(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity_(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity_ || forceFetch || _alwaysFetchRoomControlComponentEntity_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId10);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId10.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity_ = newEntity;
				_alreadyFetchedRoomControlComponentEntity_ = fetchResult;
			}
			return _roomControlComponentEntity_;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity__()
		{
			return GetSingleRoomControlComponentEntity__(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity__(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity__ || forceFetch || _alwaysFetchRoomControlComponentEntity__) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId2);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId2.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity__ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity__ = newEntity;
				_alreadyFetchedRoomControlComponentEntity__ = fetchResult;
			}
			return _roomControlComponentEntity__;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity___()
		{
			return GetSingleRoomControlComponentEntity___(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity___(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity___ || forceFetch || _alwaysFetchRoomControlComponentEntity___) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId3);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId3.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity___ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity___ = newEntity;
				_alreadyFetchedRoomControlComponentEntity___ = fetchResult;
			}
			return _roomControlComponentEntity___;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity____()
		{
			return GetSingleRoomControlComponentEntity____(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity____(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity____ || forceFetch || _alwaysFetchRoomControlComponentEntity____) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId4);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId4.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity____ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity____ = newEntity;
				_alreadyFetchedRoomControlComponentEntity____ = fetchResult;
			}
			return _roomControlComponentEntity____;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity_____()
		{
			return GetSingleRoomControlComponentEntity_____(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity_____(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity_____ || forceFetch || _alwaysFetchRoomControlComponentEntity_____) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId5);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId5.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity_____ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity_____ = newEntity;
				_alreadyFetchedRoomControlComponentEntity_____ = fetchResult;
			}
			return _roomControlComponentEntity_____;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity______()
		{
			return GetSingleRoomControlComponentEntity______(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity______(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity______ || forceFetch || _alwaysFetchRoomControlComponentEntity______) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId6);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId6.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity______ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity______ = newEntity;
				_alreadyFetchedRoomControlComponentEntity______ = fetchResult;
			}
			return _roomControlComponentEntity______;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity_______()
		{
			return GetSingleRoomControlComponentEntity_______(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity_______(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity_______ || forceFetch || _alwaysFetchRoomControlComponentEntity_______) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId7);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId7.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity_______ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity_______ = newEntity;
				_alreadyFetchedRoomControlComponentEntity_______ = fetchResult;
			}
			return _roomControlComponentEntity_______;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity________()
		{
			return GetSingleRoomControlComponentEntity________(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity________(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity________ || forceFetch || _alwaysFetchRoomControlComponentEntity________) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId8);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId8.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity________ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity________ = newEntity;
				_alreadyFetchedRoomControlComponentEntity________ = fetchResult;
			}
			return _roomControlComponentEntity________;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public RoomControlComponentEntity GetSingleRoomControlComponentEntity_________()
		{
			return GetSingleRoomControlComponentEntity_________(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlComponentEntity' which is related to this entity.</returns>
		public virtual RoomControlComponentEntity GetSingleRoomControlComponentEntity_________(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlComponentEntity_________ || forceFetch || _alwaysFetchRoomControlComponentEntity_________) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlComponentEntityUsingRoomControlComponentId9);
				RoomControlComponentEntity newEntity = new RoomControlComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlComponentId9.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlComponentEntity_________ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlComponentEntity_________ = newEntity;
				_alreadyFetchedRoomControlComponentEntity_________ = fetchResult;
			}
			return _roomControlComponentEntity_________;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public RoomControlSectionEntity GetSingleRoomControlSectionEntity()
		{
			return GetSingleRoomControlSectionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionEntity GetSingleRoomControlSectionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionEntity || forceFetch || _alwaysFetchRoomControlSectionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionEntityUsingRoomControlSectionId);
				RoomControlSectionEntity newEntity = new RoomControlSectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionEntity = newEntity;
				_alreadyFetchedRoomControlSectionEntity = fetchResult;
			}
			return _roomControlSectionEntity;
		}


		/// <summary> Retrieves the related entity of type 'RoomControlSectionItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RoomControlSectionItemEntity' which is related to this entity.</returns>
		public RoomControlSectionItemEntity GetSingleRoomControlSectionItemEntity()
		{
			return GetSingleRoomControlSectionItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'RoomControlSectionItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RoomControlSectionItemEntity' which is related to this entity.</returns>
		public virtual RoomControlSectionItemEntity GetSingleRoomControlSectionItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRoomControlSectionItemEntity || forceFetch || _alwaysFetchRoomControlSectionItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
				RoomControlSectionItemEntity newEntity = new RoomControlSectionItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RoomControlSectionItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RoomControlSectionItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_roomControlSectionItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RoomControlSectionItemEntity = newEntity;
				_alreadyFetchedRoomControlSectionItemEntity = fetchResult;
			}
			return _roomControlSectionItemEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("InfraredConfigurationEntity", _infraredConfigurationEntity);
			toReturn.Add("RoomControlComponentEntity", _roomControlComponentEntity);
			toReturn.Add("RoomControlComponentEntity_", _roomControlComponentEntity_);
			toReturn.Add("RoomControlComponentEntity__", _roomControlComponentEntity__);
			toReturn.Add("RoomControlComponentEntity___", _roomControlComponentEntity___);
			toReturn.Add("RoomControlComponentEntity____", _roomControlComponentEntity____);
			toReturn.Add("RoomControlComponentEntity_____", _roomControlComponentEntity_____);
			toReturn.Add("RoomControlComponentEntity______", _roomControlComponentEntity______);
			toReturn.Add("RoomControlComponentEntity_______", _roomControlComponentEntity_______);
			toReturn.Add("RoomControlComponentEntity________", _roomControlComponentEntity________);
			toReturn.Add("RoomControlComponentEntity_________", _roomControlComponentEntity_________);
			toReturn.Add("RoomControlSectionEntity", _roomControlSectionEntity);
			toReturn.Add("RoomControlSectionItemEntity", _roomControlSectionItemEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("RoomControlWidgetLanguageCollection", _roomControlWidgetLanguageCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="validator">The validator object for this RoomControlWidgetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 roomControlWidgetId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(roomControlWidgetId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "RoomControlWidgetEntity");

			_roomControlWidgetLanguageCollection = new Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection();
			_roomControlWidgetLanguageCollection.SetContainingEntityInfo(this, "RoomControlWidgetEntity");
			_infraredConfigurationEntityReturnsNewIfNotFound = true;
			_roomControlComponentEntityReturnsNewIfNotFound = true;
			_roomControlComponentEntity_ReturnsNewIfNotFound = true;
			_roomControlComponentEntity__ReturnsNewIfNotFound = true;
			_roomControlComponentEntity___ReturnsNewIfNotFound = true;
			_roomControlComponentEntity____ReturnsNewIfNotFound = true;
			_roomControlComponentEntity_____ReturnsNewIfNotFound = true;
			_roomControlComponentEntity______ReturnsNewIfNotFound = true;
			_roomControlComponentEntity_______ReturnsNewIfNotFound = true;
			_roomControlComponentEntity________ReturnsNewIfNotFound = true;
			_roomControlComponentEntity_________ReturnsNewIfNotFound = true;
			_roomControlSectionEntityReturnsNewIfNotFound = true;
			_roomControlSectionItemEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlWidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSectionItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfraredConfigurationId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInfraredConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, signalRelatedEntity, "RoomControlWidgetCollection", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.InfraredConfigurationId } );		
			_infraredConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _infraredConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInfraredConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_infraredConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncInfraredConfigurationEntity(true, true);
				_infraredConfigurationEntity = (InfraredConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _infraredConfigurationEntity, new PropertyChangedEventHandler( OnInfraredConfigurationEntityPropertyChanged ), "InfraredConfigurationEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.InfraredConfigurationEntityUsingInfraredConfigurationIdStatic, true, ref _alreadyFetchedInfraredConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInfraredConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity, new PropertyChangedEventHandler( OnRoomControlComponentEntityPropertyChanged ), "RoomControlComponentEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId1Static, true, signalRelatedEntity, "RoomControlWidgetCollection", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId1 } );		
			_roomControlComponentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity(true, true);
				_roomControlComponentEntity = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity, new PropertyChangedEventHandler( OnRoomControlComponentEntityPropertyChanged ), "RoomControlComponentEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId1Static, true, ref _alreadyFetchedRoomControlComponentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity_, new PropertyChangedEventHandler( OnRoomControlComponentEntity_PropertyChanged ), "RoomControlComponentEntity_", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId10Static, true, signalRelatedEntity, "RoomControlWidgetCollection_", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId10 } );		
			_roomControlComponentEntity_ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity_(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity_!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity_(true, true);
				_roomControlComponentEntity_ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity_, new PropertyChangedEventHandler( OnRoomControlComponentEntity_PropertyChanged ), "RoomControlComponentEntity_", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId10Static, true, ref _alreadyFetchedRoomControlComponentEntity_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity__</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity__(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity__, new PropertyChangedEventHandler( OnRoomControlComponentEntity__PropertyChanged ), "RoomControlComponentEntity__", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId2Static, true, signalRelatedEntity, "RoomControlWidgetCollection__", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId2 } );		
			_roomControlComponentEntity__ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity__</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity__(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity__!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity__(true, true);
				_roomControlComponentEntity__ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity__, new PropertyChangedEventHandler( OnRoomControlComponentEntity__PropertyChanged ), "RoomControlComponentEntity__", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId2Static, true, ref _alreadyFetchedRoomControlComponentEntity__, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity__PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity___</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity___(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity___, new PropertyChangedEventHandler( OnRoomControlComponentEntity___PropertyChanged ), "RoomControlComponentEntity___", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId3Static, true, signalRelatedEntity, "RoomControlWidgetCollection___", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId3 } );		
			_roomControlComponentEntity___ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity___</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity___(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity___!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity___(true, true);
				_roomControlComponentEntity___ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity___, new PropertyChangedEventHandler( OnRoomControlComponentEntity___PropertyChanged ), "RoomControlComponentEntity___", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId3Static, true, ref _alreadyFetchedRoomControlComponentEntity___, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity___PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity____</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity____(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity____, new PropertyChangedEventHandler( OnRoomControlComponentEntity____PropertyChanged ), "RoomControlComponentEntity____", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId4Static, true, signalRelatedEntity, "RoomControlWidgetCollection____", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId4 } );		
			_roomControlComponentEntity____ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity____</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity____(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity____!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity____(true, true);
				_roomControlComponentEntity____ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity____, new PropertyChangedEventHandler( OnRoomControlComponentEntity____PropertyChanged ), "RoomControlComponentEntity____", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId4Static, true, ref _alreadyFetchedRoomControlComponentEntity____, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity____PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity_____</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity_____(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity_____, new PropertyChangedEventHandler( OnRoomControlComponentEntity_____PropertyChanged ), "RoomControlComponentEntity_____", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId5Static, true, signalRelatedEntity, "RoomControlWidgetCollection_____", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId5 } );		
			_roomControlComponentEntity_____ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity_____</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity_____(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity_____!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity_____(true, true);
				_roomControlComponentEntity_____ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity_____, new PropertyChangedEventHandler( OnRoomControlComponentEntity_____PropertyChanged ), "RoomControlComponentEntity_____", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId5Static, true, ref _alreadyFetchedRoomControlComponentEntity_____, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity_____PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity______</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity______(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity______, new PropertyChangedEventHandler( OnRoomControlComponentEntity______PropertyChanged ), "RoomControlComponentEntity______", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId6Static, true, signalRelatedEntity, "RoomControlWidgetCollection______", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId6 } );		
			_roomControlComponentEntity______ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity______</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity______(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity______!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity______(true, true);
				_roomControlComponentEntity______ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity______, new PropertyChangedEventHandler( OnRoomControlComponentEntity______PropertyChanged ), "RoomControlComponentEntity______", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId6Static, true, ref _alreadyFetchedRoomControlComponentEntity______, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity______PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity_______</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity_______(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity_______, new PropertyChangedEventHandler( OnRoomControlComponentEntity_______PropertyChanged ), "RoomControlComponentEntity_______", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId7Static, true, signalRelatedEntity, "RoomControlWidgetCollection_______", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId7 } );		
			_roomControlComponentEntity_______ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity_______</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity_______(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity_______!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity_______(true, true);
				_roomControlComponentEntity_______ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity_______, new PropertyChangedEventHandler( OnRoomControlComponentEntity_______PropertyChanged ), "RoomControlComponentEntity_______", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId7Static, true, ref _alreadyFetchedRoomControlComponentEntity_______, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity_______PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity________</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity________(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity________, new PropertyChangedEventHandler( OnRoomControlComponentEntity________PropertyChanged ), "RoomControlComponentEntity________", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId8Static, true, signalRelatedEntity, "RoomControlWidgetCollection________", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId8 } );		
			_roomControlComponentEntity________ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity________</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity________(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity________!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity________(true, true);
				_roomControlComponentEntity________ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity________, new PropertyChangedEventHandler( OnRoomControlComponentEntity________PropertyChanged ), "RoomControlComponentEntity________", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId8Static, true, ref _alreadyFetchedRoomControlComponentEntity________, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity________PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlComponentEntity_________</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlComponentEntity_________(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlComponentEntity_________, new PropertyChangedEventHandler( OnRoomControlComponentEntity_________PropertyChanged ), "RoomControlComponentEntity_________", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId9Static, true, signalRelatedEntity, "RoomControlWidgetCollection_________", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlComponentId9 } );		
			_roomControlComponentEntity_________ = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlComponentEntity_________</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlComponentEntity_________(IEntityCore relatedEntity)
		{
			if(_roomControlComponentEntity_________!=relatedEntity)
			{		
				DesetupSyncRoomControlComponentEntity_________(true, true);
				_roomControlComponentEntity_________ = (RoomControlComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlComponentEntity_________, new PropertyChangedEventHandler( OnRoomControlComponentEntity_________PropertyChanged ), "RoomControlComponentEntity_________", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlComponentEntityUsingRoomControlComponentId9Static, true, ref _alreadyFetchedRoomControlComponentEntity_________, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlComponentEntity_________PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, signalRelatedEntity, "RoomControlWidgetCollection", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlSectionId } );		
			_roomControlSectionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionEntity(true, true);
				_roomControlSectionEntity = (RoomControlSectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionEntity, new PropertyChangedEventHandler( OnRoomControlSectionEntityPropertyChanged ), "RoomControlSectionEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlSectionEntityUsingRoomControlSectionIdStatic, true, ref _alreadyFetchedRoomControlSectionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _roomControlSectionItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRoomControlSectionItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _roomControlSectionItemEntity, new PropertyChangedEventHandler( OnRoomControlSectionItemEntityPropertyChanged ), "RoomControlSectionItemEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic, true, signalRelatedEntity, "RoomControlWidgetCollection", resetFKFields, new int[] { (int)RoomControlWidgetFieldIndex.RoomControlSectionItemId } );		
			_roomControlSectionItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _roomControlSectionItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRoomControlSectionItemEntity(IEntityCore relatedEntity)
		{
			if(_roomControlSectionItemEntity!=relatedEntity)
			{		
				DesetupSyncRoomControlSectionItemEntity(true, true);
				_roomControlSectionItemEntity = (RoomControlSectionItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _roomControlSectionItemEntity, new PropertyChangedEventHandler( OnRoomControlSectionItemEntityPropertyChanged ), "RoomControlSectionItemEntity", Obymobi.Data.RelationClasses.StaticRoomControlWidgetRelations.RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic, true, ref _alreadyFetchedRoomControlSectionItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRoomControlSectionItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 roomControlWidgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RoomControlWidgetFieldIndex.RoomControlWidgetId].ForcedCurrentValueWrite(roomControlWidgetId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoomControlWidgetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RoomControlWidgetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RoomControlWidgetRelations Relations
		{
			get	{ return new RoomControlWidgetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlWidgetLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlWidgetLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection(), (IEntityRelation)GetRelationsForField("RoomControlWidgetLanguageCollection")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlWidgetLanguageEntity, 0, null, null, null, "RoomControlWidgetLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InfraredConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInfraredConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.InfraredConfigurationCollection(), (IEntityRelation)GetRelationsForField("InfraredConfigurationEntity")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.InfraredConfigurationEntity, 0, null, null, null, "InfraredConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity_
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity_")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity__
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity__")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity___
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity___")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity____
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity____")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity_____
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity_____")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity_____", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity______
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity______")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity______", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity_______
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity_______")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity_______", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity________
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity________")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity________", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlComponentEntity_________
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlComponentCollection(), (IEntityRelation)GetRelationsForField("RoomControlComponentEntity_________")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlComponentEntity, 0, null, null, null, "RoomControlComponentEntity_________", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionEntity")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlSectionEntity, 0, null, null, null, "RoomControlSectionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoomControlSectionItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoomControlSectionItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RoomControlSectionItemCollection(), (IEntityRelation)GetRelationsForField("RoomControlSectionItemEntity")[0], (int)Obymobi.Data.EntityType.RoomControlWidgetEntity, (int)Obymobi.Data.EntityType.RoomControlSectionItemEntity, 0, null, null, null, "RoomControlSectionItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RoomControlWidgetId property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlWidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 RoomControlWidgetId
		{
			get { return (System.Int32)GetValue((int)RoomControlWidgetFieldIndex.RoomControlWidgetId, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlWidgetId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The RoomControlSectionId property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlSectionId, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlSectionId, value, true); }
		}

		/// <summary> The RoomControlComponentId1 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId1
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId1, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId1, value, true); }
		}

		/// <summary> The Caption property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Caption
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.Caption, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.Caption, value, true); }
		}

		/// <summary> The Type property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.RoomControlWidgetType Type
		{
			get { return (Obymobi.Enums.RoomControlWidgetType)GetValue((int)RoomControlWidgetFieldIndex.Type, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.Type, value, true); }
		}

		/// <summary> The SortOrder property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)RoomControlWidgetFieldIndex.SortOrder, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The Visible property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)RoomControlWidgetFieldIndex.Visible, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.Visible, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlWidgetFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RoomControlWidgetFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)RoomControlWidgetFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The RoomControlSectionItemId property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlSectionItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlSectionItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlSectionItemId, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlSectionItemId, value, true); }
		}

		/// <summary> The RoomControlComponentId10 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId10"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId10
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId10, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId10, value, true); }
		}

		/// <summary> The RoomControlComponentId2 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId2
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId2, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId2, value, true); }
		}

		/// <summary> The RoomControlComponentId3 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId3
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId3, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId3, value, true); }
		}

		/// <summary> The RoomControlComponentId4 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId4
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId4, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId4, value, true); }
		}

		/// <summary> The RoomControlComponentId5 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId5"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId5
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId5, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId5, value, true); }
		}

		/// <summary> The RoomControlComponentId6 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId6"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId6
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId6, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId6, value, true); }
		}

		/// <summary> The RoomControlComponentId7 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId7"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId7
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId7, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId7, value, true); }
		}

		/// <summary> The RoomControlComponentId8 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId8"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId8
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId8, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId8, value, true); }
		}

		/// <summary> The RoomControlComponentId9 property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."RoomControlComponentId9"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId9
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId9, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.RoomControlComponentId9, value, true); }
		}

		/// <summary> The InfraredConfigurationId property of the Entity RoomControlWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RoomControlWidget"."InfraredConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InfraredConfigurationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)RoomControlWidgetFieldIndex.InfraredConfigurationId, false); }
			set	{ SetValue((int)RoomControlWidgetFieldIndex.InfraredConfigurationId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RoomControlWidgetLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoomControlWidgetLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RoomControlWidgetLanguageCollection RoomControlWidgetLanguageCollection
		{
			get	{ return GetMultiRoomControlWidgetLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlWidgetLanguageCollection. When set to true, RoomControlWidgetLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlWidgetLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiRoomControlWidgetLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlWidgetLanguageCollection
		{
			get	{ return _alwaysFetchRoomControlWidgetLanguageCollection; }
			set	{ _alwaysFetchRoomControlWidgetLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlWidgetLanguageCollection already has been fetched. Setting this property to false when RoomControlWidgetLanguageCollection has been fetched
		/// will clear the RoomControlWidgetLanguageCollection collection well. Setting this property to true while RoomControlWidgetLanguageCollection hasn't been fetched disables lazy loading for RoomControlWidgetLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlWidgetLanguageCollection
		{
			get { return _alreadyFetchedRoomControlWidgetLanguageCollection;}
			set 
			{
				if(_alreadyFetchedRoomControlWidgetLanguageCollection && !value && (_roomControlWidgetLanguageCollection != null))
				{
					_roomControlWidgetLanguageCollection.Clear();
				}
				_alreadyFetchedRoomControlWidgetLanguageCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'InfraredConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInfraredConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual InfraredConfigurationEntity InfraredConfigurationEntity
		{
			get	{ return GetSingleInfraredConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInfraredConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection", "InfraredConfigurationEntity", _infraredConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InfraredConfigurationEntity. When set to true, InfraredConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InfraredConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleInfraredConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInfraredConfigurationEntity
		{
			get	{ return _alwaysFetchInfraredConfigurationEntity; }
			set	{ _alwaysFetchInfraredConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InfraredConfigurationEntity already has been fetched. Setting this property to false when InfraredConfigurationEntity has been fetched
		/// will set InfraredConfigurationEntity to null as well. Setting this property to true while InfraredConfigurationEntity hasn't been fetched disables lazy loading for InfraredConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInfraredConfigurationEntity
		{
			get { return _alreadyFetchedInfraredConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedInfraredConfigurationEntity && !value)
				{
					this.InfraredConfigurationEntity = null;
				}
				_alreadyFetchedInfraredConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InfraredConfigurationEntity is not found
		/// in the database. When set to true, InfraredConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool InfraredConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _infraredConfigurationEntityReturnsNewIfNotFound; }
			set { _infraredConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity
		{
			get	{ return GetSingleRoomControlComponentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection", "RoomControlComponentEntity", _roomControlComponentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity. When set to true, RoomControlComponentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity
		{
			get	{ return _alwaysFetchRoomControlComponentEntity; }
			set	{ _alwaysFetchRoomControlComponentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity already has been fetched. Setting this property to false when RoomControlComponentEntity has been fetched
		/// will set RoomControlComponentEntity to null as well. Setting this property to true while RoomControlComponentEntity hasn't been fetched disables lazy loading for RoomControlComponentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity
		{
			get { return _alreadyFetchedRoomControlComponentEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity && !value)
				{
					this.RoomControlComponentEntity = null;
				}
				_alreadyFetchedRoomControlComponentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity is not found
		/// in the database. When set to true, RoomControlComponentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntityReturnsNewIfNotFound; }
			set { _roomControlComponentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity_
		{
			get	{ return GetSingleRoomControlComponentEntity_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection_", "RoomControlComponentEntity_", _roomControlComponentEntity_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity_. When set to true, RoomControlComponentEntity_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity_ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity_
		{
			get	{ return _alwaysFetchRoomControlComponentEntity_; }
			set	{ _alwaysFetchRoomControlComponentEntity_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity_ already has been fetched. Setting this property to false when RoomControlComponentEntity_ has been fetched
		/// will set RoomControlComponentEntity_ to null as well. Setting this property to true while RoomControlComponentEntity_ hasn't been fetched disables lazy loading for RoomControlComponentEntity_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity_
		{
			get { return _alreadyFetchedRoomControlComponentEntity_;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity_ && !value)
				{
					this.RoomControlComponentEntity_ = null;
				}
				_alreadyFetchedRoomControlComponentEntity_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity_ is not found
		/// in the database. When set to true, RoomControlComponentEntity_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity_ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity_ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity__
		{
			get	{ return GetSingleRoomControlComponentEntity__(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity__(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection__", "RoomControlComponentEntity__", _roomControlComponentEntity__, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity__. When set to true, RoomControlComponentEntity__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity__ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity__
		{
			get	{ return _alwaysFetchRoomControlComponentEntity__; }
			set	{ _alwaysFetchRoomControlComponentEntity__ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity__ already has been fetched. Setting this property to false when RoomControlComponentEntity__ has been fetched
		/// will set RoomControlComponentEntity__ to null as well. Setting this property to true while RoomControlComponentEntity__ hasn't been fetched disables lazy loading for RoomControlComponentEntity__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity__
		{
			get { return _alreadyFetchedRoomControlComponentEntity__;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity__ && !value)
				{
					this.RoomControlComponentEntity__ = null;
				}
				_alreadyFetchedRoomControlComponentEntity__ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity__ is not found
		/// in the database. When set to true, RoomControlComponentEntity__ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity__ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity__ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity__ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity___
		{
			get	{ return GetSingleRoomControlComponentEntity___(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity___(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection___", "RoomControlComponentEntity___", _roomControlComponentEntity___, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity___. When set to true, RoomControlComponentEntity___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity___ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity___
		{
			get	{ return _alwaysFetchRoomControlComponentEntity___; }
			set	{ _alwaysFetchRoomControlComponentEntity___ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity___ already has been fetched. Setting this property to false when RoomControlComponentEntity___ has been fetched
		/// will set RoomControlComponentEntity___ to null as well. Setting this property to true while RoomControlComponentEntity___ hasn't been fetched disables lazy loading for RoomControlComponentEntity___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity___
		{
			get { return _alreadyFetchedRoomControlComponentEntity___;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity___ && !value)
				{
					this.RoomControlComponentEntity___ = null;
				}
				_alreadyFetchedRoomControlComponentEntity___ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity___ is not found
		/// in the database. When set to true, RoomControlComponentEntity___ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity___ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity___ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity___ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity____
		{
			get	{ return GetSingleRoomControlComponentEntity____(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity____(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection____", "RoomControlComponentEntity____", _roomControlComponentEntity____, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity____. When set to true, RoomControlComponentEntity____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity____ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity____
		{
			get	{ return _alwaysFetchRoomControlComponentEntity____; }
			set	{ _alwaysFetchRoomControlComponentEntity____ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity____ already has been fetched. Setting this property to false when RoomControlComponentEntity____ has been fetched
		/// will set RoomControlComponentEntity____ to null as well. Setting this property to true while RoomControlComponentEntity____ hasn't been fetched disables lazy loading for RoomControlComponentEntity____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity____
		{
			get { return _alreadyFetchedRoomControlComponentEntity____;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity____ && !value)
				{
					this.RoomControlComponentEntity____ = null;
				}
				_alreadyFetchedRoomControlComponentEntity____ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity____ is not found
		/// in the database. When set to true, RoomControlComponentEntity____ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity____ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity____ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity____ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity_____()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity_____
		{
			get	{ return GetSingleRoomControlComponentEntity_____(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity_____(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection_____", "RoomControlComponentEntity_____", _roomControlComponentEntity_____, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity_____. When set to true, RoomControlComponentEntity_____ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity_____ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity_____(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity_____
		{
			get	{ return _alwaysFetchRoomControlComponentEntity_____; }
			set	{ _alwaysFetchRoomControlComponentEntity_____ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity_____ already has been fetched. Setting this property to false when RoomControlComponentEntity_____ has been fetched
		/// will set RoomControlComponentEntity_____ to null as well. Setting this property to true while RoomControlComponentEntity_____ hasn't been fetched disables lazy loading for RoomControlComponentEntity_____</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity_____
		{
			get { return _alreadyFetchedRoomControlComponentEntity_____;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity_____ && !value)
				{
					this.RoomControlComponentEntity_____ = null;
				}
				_alreadyFetchedRoomControlComponentEntity_____ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity_____ is not found
		/// in the database. When set to true, RoomControlComponentEntity_____ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity_____ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity_____ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity_____ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity______()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity______
		{
			get	{ return GetSingleRoomControlComponentEntity______(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity______(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection______", "RoomControlComponentEntity______", _roomControlComponentEntity______, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity______. When set to true, RoomControlComponentEntity______ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity______ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity______(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity______
		{
			get	{ return _alwaysFetchRoomControlComponentEntity______; }
			set	{ _alwaysFetchRoomControlComponentEntity______ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity______ already has been fetched. Setting this property to false when RoomControlComponentEntity______ has been fetched
		/// will set RoomControlComponentEntity______ to null as well. Setting this property to true while RoomControlComponentEntity______ hasn't been fetched disables lazy loading for RoomControlComponentEntity______</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity______
		{
			get { return _alreadyFetchedRoomControlComponentEntity______;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity______ && !value)
				{
					this.RoomControlComponentEntity______ = null;
				}
				_alreadyFetchedRoomControlComponentEntity______ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity______ is not found
		/// in the database. When set to true, RoomControlComponentEntity______ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity______ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity______ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity______ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity_______()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity_______
		{
			get	{ return GetSingleRoomControlComponentEntity_______(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity_______(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection_______", "RoomControlComponentEntity_______", _roomControlComponentEntity_______, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity_______. When set to true, RoomControlComponentEntity_______ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity_______ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity_______(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity_______
		{
			get	{ return _alwaysFetchRoomControlComponentEntity_______; }
			set	{ _alwaysFetchRoomControlComponentEntity_______ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity_______ already has been fetched. Setting this property to false when RoomControlComponentEntity_______ has been fetched
		/// will set RoomControlComponentEntity_______ to null as well. Setting this property to true while RoomControlComponentEntity_______ hasn't been fetched disables lazy loading for RoomControlComponentEntity_______</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity_______
		{
			get { return _alreadyFetchedRoomControlComponentEntity_______;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity_______ && !value)
				{
					this.RoomControlComponentEntity_______ = null;
				}
				_alreadyFetchedRoomControlComponentEntity_______ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity_______ is not found
		/// in the database. When set to true, RoomControlComponentEntity_______ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity_______ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity_______ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity_______ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity________()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity________
		{
			get	{ return GetSingleRoomControlComponentEntity________(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity________(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection________", "RoomControlComponentEntity________", _roomControlComponentEntity________, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity________. When set to true, RoomControlComponentEntity________ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity________ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity________(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity________
		{
			get	{ return _alwaysFetchRoomControlComponentEntity________; }
			set	{ _alwaysFetchRoomControlComponentEntity________ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity________ already has been fetched. Setting this property to false when RoomControlComponentEntity________ has been fetched
		/// will set RoomControlComponentEntity________ to null as well. Setting this property to true while RoomControlComponentEntity________ hasn't been fetched disables lazy loading for RoomControlComponentEntity________</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity________
		{
			get { return _alreadyFetchedRoomControlComponentEntity________;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity________ && !value)
				{
					this.RoomControlComponentEntity________ = null;
				}
				_alreadyFetchedRoomControlComponentEntity________ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity________ is not found
		/// in the database. When set to true, RoomControlComponentEntity________ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity________ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity________ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity________ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlComponentEntity_________()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlComponentEntity RoomControlComponentEntity_________
		{
			get	{ return GetSingleRoomControlComponentEntity_________(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlComponentEntity_________(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection_________", "RoomControlComponentEntity_________", _roomControlComponentEntity_________, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlComponentEntity_________. When set to true, RoomControlComponentEntity_________ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlComponentEntity_________ is accessed. You can always execute a forced fetch by calling GetSingleRoomControlComponentEntity_________(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlComponentEntity_________
		{
			get	{ return _alwaysFetchRoomControlComponentEntity_________; }
			set	{ _alwaysFetchRoomControlComponentEntity_________ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlComponentEntity_________ already has been fetched. Setting this property to false when RoomControlComponentEntity_________ has been fetched
		/// will set RoomControlComponentEntity_________ to null as well. Setting this property to true while RoomControlComponentEntity_________ hasn't been fetched disables lazy loading for RoomControlComponentEntity_________</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlComponentEntity_________
		{
			get { return _alreadyFetchedRoomControlComponentEntity_________;}
			set 
			{
				if(_alreadyFetchedRoomControlComponentEntity_________ && !value)
				{
					this.RoomControlComponentEntity_________ = null;
				}
				_alreadyFetchedRoomControlComponentEntity_________ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlComponentEntity_________ is not found
		/// in the database. When set to true, RoomControlComponentEntity_________ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlComponentEntity_________ReturnsNewIfNotFound
		{
			get	{ return _roomControlComponentEntity_________ReturnsNewIfNotFound; }
			set { _roomControlComponentEntity_________ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionEntity RoomControlSectionEntity
		{
			get	{ return GetSingleRoomControlSectionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection", "RoomControlSectionEntity", _roomControlSectionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionEntity. When set to true, RoomControlSectionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionEntity
		{
			get	{ return _alwaysFetchRoomControlSectionEntity; }
			set	{ _alwaysFetchRoomControlSectionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionEntity already has been fetched. Setting this property to false when RoomControlSectionEntity has been fetched
		/// will set RoomControlSectionEntity to null as well. Setting this property to true while RoomControlSectionEntity hasn't been fetched disables lazy loading for RoomControlSectionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionEntity
		{
			get { return _alreadyFetchedRoomControlSectionEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionEntity && !value)
				{
					this.RoomControlSectionEntity = null;
				}
				_alreadyFetchedRoomControlSectionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionEntity is not found
		/// in the database. When set to true, RoomControlSectionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionEntityReturnsNewIfNotFound; }
			set { _roomControlSectionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RoomControlSectionItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRoomControlSectionItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual RoomControlSectionItemEntity RoomControlSectionItemEntity
		{
			get	{ return GetSingleRoomControlSectionItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRoomControlSectionItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoomControlWidgetCollection", "RoomControlSectionItemEntity", _roomControlSectionItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RoomControlSectionItemEntity. When set to true, RoomControlSectionItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoomControlSectionItemEntity is accessed. You can always execute a forced fetch by calling GetSingleRoomControlSectionItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoomControlSectionItemEntity
		{
			get	{ return _alwaysFetchRoomControlSectionItemEntity; }
			set	{ _alwaysFetchRoomControlSectionItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoomControlSectionItemEntity already has been fetched. Setting this property to false when RoomControlSectionItemEntity has been fetched
		/// will set RoomControlSectionItemEntity to null as well. Setting this property to true while RoomControlSectionItemEntity hasn't been fetched disables lazy loading for RoomControlSectionItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoomControlSectionItemEntity
		{
			get { return _alreadyFetchedRoomControlSectionItemEntity;}
			set 
			{
				if(_alreadyFetchedRoomControlSectionItemEntity && !value)
				{
					this.RoomControlSectionItemEntity = null;
				}
				_alreadyFetchedRoomControlSectionItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RoomControlSectionItemEntity is not found
		/// in the database. When set to true, RoomControlSectionItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RoomControlSectionItemEntityReturnsNewIfNotFound
		{
			get	{ return _roomControlSectionItemEntityReturnsNewIfNotFound; }
			set { _roomControlSectionItemEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.RoomControlWidgetEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
