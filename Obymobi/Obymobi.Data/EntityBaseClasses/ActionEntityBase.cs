﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Action'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ActionEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ActionEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.NavigationMenuItemCollection	_navigationMenuItemCollection;
		private bool	_alwaysFetchNavigationMenuItemCollection, _alreadyFetchedNavigationMenuItemCollection;
		private Obymobi.Data.CollectionClasses.WidgetCollection	_widgetCollection;
		private bool	_alwaysFetchWidgetCollection, _alreadyFetchedWidgetCollection;
		private ApplicationConfigurationEntity _applicationConfigurationEntity;
		private bool	_alwaysFetchApplicationConfigurationEntity, _alreadyFetchedApplicationConfigurationEntity, _applicationConfigurationEntityReturnsNewIfNotFound;
		private LandingPageEntity _landingPageEntity;
		private bool	_alwaysFetchLandingPageEntity, _alreadyFetchedLandingPageEntity, _landingPageEntityReturnsNewIfNotFound;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private ProductCategoryEntity _productCategoryEntity;
		private bool	_alwaysFetchProductCategoryEntity, _alreadyFetchedProductCategoryEntity, _productCategoryEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ApplicationConfigurationEntity</summary>
			public static readonly string ApplicationConfigurationEntity = "ApplicationConfigurationEntity";
			/// <summary>Member name LandingPageEntity</summary>
			public static readonly string LandingPageEntity = "LandingPageEntity";
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name ProductCategoryEntity</summary>
			public static readonly string ProductCategoryEntity = "ProductCategoryEntity";
			/// <summary>Member name NavigationMenuItemCollection</summary>
			public static readonly string NavigationMenuItemCollection = "NavigationMenuItemCollection";
			/// <summary>Member name WidgetCollection</summary>
			public static readonly string WidgetCollection = "WidgetCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ActionEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ActionEntityBase() :base("ActionEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		protected ActionEntityBase(System.Int32 actionId):base("ActionEntity")
		{
			InitClassFetch(actionId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ActionEntityBase(System.Int32 actionId, IPrefetchPath prefetchPathToUse): base("ActionEntity")
		{
			InitClassFetch(actionId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <param name="validator">The custom validator object for this ActionEntity</param>
		protected ActionEntityBase(System.Int32 actionId, IValidator validator):base("ActionEntity")
		{
			InitClassFetch(actionId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ActionEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_navigationMenuItemCollection = (Obymobi.Data.CollectionClasses.NavigationMenuItemCollection)info.GetValue("_navigationMenuItemCollection", typeof(Obymobi.Data.CollectionClasses.NavigationMenuItemCollection));
			_alwaysFetchNavigationMenuItemCollection = info.GetBoolean("_alwaysFetchNavigationMenuItemCollection");
			_alreadyFetchedNavigationMenuItemCollection = info.GetBoolean("_alreadyFetchedNavigationMenuItemCollection");

			_widgetCollection = (Obymobi.Data.CollectionClasses.WidgetCollection)info.GetValue("_widgetCollection", typeof(Obymobi.Data.CollectionClasses.WidgetCollection));
			_alwaysFetchWidgetCollection = info.GetBoolean("_alwaysFetchWidgetCollection");
			_alreadyFetchedWidgetCollection = info.GetBoolean("_alreadyFetchedWidgetCollection");
			_applicationConfigurationEntity = (ApplicationConfigurationEntity)info.GetValue("_applicationConfigurationEntity", typeof(ApplicationConfigurationEntity));
			if(_applicationConfigurationEntity!=null)
			{
				_applicationConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_applicationConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_applicationConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchApplicationConfigurationEntity = info.GetBoolean("_alwaysFetchApplicationConfigurationEntity");
			_alreadyFetchedApplicationConfigurationEntity = info.GetBoolean("_alreadyFetchedApplicationConfigurationEntity");

			_landingPageEntity = (LandingPageEntity)info.GetValue("_landingPageEntity", typeof(LandingPageEntity));
			if(_landingPageEntity!=null)
			{
				_landingPageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_landingPageEntityReturnsNewIfNotFound = info.GetBoolean("_landingPageEntityReturnsNewIfNotFound");
			_alwaysFetchLandingPageEntity = info.GetBoolean("_alwaysFetchLandingPageEntity");
			_alreadyFetchedLandingPageEntity = info.GetBoolean("_alreadyFetchedLandingPageEntity");

			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_productCategoryEntity = (ProductCategoryEntity)info.GetValue("_productCategoryEntity", typeof(ProductCategoryEntity));
			if(_productCategoryEntity!=null)
			{
				_productCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_productCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchProductCategoryEntity = info.GetBoolean("_alwaysFetchProductCategoryEntity");
			_alreadyFetchedProductCategoryEntity = info.GetBoolean("_alreadyFetchedProductCategoryEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ActionFieldIndex)fieldIndex)
			{
				case ActionFieldIndex.LandingPageId:
					DesetupSyncLandingPageEntity(true, false);
					_alreadyFetchedLandingPageEntity = false;
					break;
				case ActionFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case ActionFieldIndex.ProductCategoryId:
					DesetupSyncProductCategoryEntity(true, false);
					_alreadyFetchedProductCategoryEntity = false;
					break;
				case ActionFieldIndex.ApplicationConfigurationId:
					DesetupSyncApplicationConfigurationEntity(true, false);
					_alreadyFetchedApplicationConfigurationEntity = false;
					break;
				case ActionFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedNavigationMenuItemCollection = (_navigationMenuItemCollection.Count > 0);
			_alreadyFetchedWidgetCollection = (_widgetCollection.Count > 0);
			_alreadyFetchedApplicationConfigurationEntity = (_applicationConfigurationEntity != null);
			_alreadyFetchedLandingPageEntity = (_landingPageEntity != null);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedProductCategoryEntity = (_productCategoryEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					toReturn.Add(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
					break;
				case "LandingPageEntity":
					toReturn.Add(Relations.LandingPageEntityUsingLandingPageId);
					break;
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "ProductCategoryEntity":
					toReturn.Add(Relations.ProductCategoryEntityUsingProductCategoryId);
					break;
				case "NavigationMenuItemCollection":
					toReturn.Add(Relations.NavigationMenuItemEntityUsingActionId);
					break;
				case "WidgetCollection":
					toReturn.Add(Relations.WidgetEntityUsingActionId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_navigationMenuItemCollection", (!this.MarkedForDeletion?_navigationMenuItemCollection:null));
			info.AddValue("_alwaysFetchNavigationMenuItemCollection", _alwaysFetchNavigationMenuItemCollection);
			info.AddValue("_alreadyFetchedNavigationMenuItemCollection", _alreadyFetchedNavigationMenuItemCollection);
			info.AddValue("_widgetCollection", (!this.MarkedForDeletion?_widgetCollection:null));
			info.AddValue("_alwaysFetchWidgetCollection", _alwaysFetchWidgetCollection);
			info.AddValue("_alreadyFetchedWidgetCollection", _alreadyFetchedWidgetCollection);
			info.AddValue("_applicationConfigurationEntity", (!this.MarkedForDeletion?_applicationConfigurationEntity:null));
			info.AddValue("_applicationConfigurationEntityReturnsNewIfNotFound", _applicationConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApplicationConfigurationEntity", _alwaysFetchApplicationConfigurationEntity);
			info.AddValue("_alreadyFetchedApplicationConfigurationEntity", _alreadyFetchedApplicationConfigurationEntity);
			info.AddValue("_landingPageEntity", (!this.MarkedForDeletion?_landingPageEntity:null));
			info.AddValue("_landingPageEntityReturnsNewIfNotFound", _landingPageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLandingPageEntity", _alwaysFetchLandingPageEntity);
			info.AddValue("_alreadyFetchedLandingPageEntity", _alreadyFetchedLandingPageEntity);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_productCategoryEntity", (!this.MarkedForDeletion?_productCategoryEntity:null));
			info.AddValue("_productCategoryEntityReturnsNewIfNotFound", _productCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductCategoryEntity", _alwaysFetchProductCategoryEntity);
			info.AddValue("_alreadyFetchedProductCategoryEntity", _alreadyFetchedProductCategoryEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ApplicationConfigurationEntity":
					_alreadyFetchedApplicationConfigurationEntity = true;
					this.ApplicationConfigurationEntity = (ApplicationConfigurationEntity)entity;
					break;
				case "LandingPageEntity":
					_alreadyFetchedLandingPageEntity = true;
					this.LandingPageEntity = (LandingPageEntity)entity;
					break;
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "ProductCategoryEntity":
					_alreadyFetchedProductCategoryEntity = true;
					this.ProductCategoryEntity = (ProductCategoryEntity)entity;
					break;
				case "NavigationMenuItemCollection":
					_alreadyFetchedNavigationMenuItemCollection = true;
					if(entity!=null)
					{
						this.NavigationMenuItemCollection.Add((NavigationMenuItemEntity)entity);
					}
					break;
				case "WidgetCollection":
					_alreadyFetchedWidgetCollection = true;
					if(entity!=null)
					{
						this.WidgetCollection.Add((WidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					SetupSyncApplicationConfigurationEntity(relatedEntity);
					break;
				case "LandingPageEntity":
					SetupSyncLandingPageEntity(relatedEntity);
					break;
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "ProductCategoryEntity":
					SetupSyncProductCategoryEntity(relatedEntity);
					break;
				case "NavigationMenuItemCollection":
					_navigationMenuItemCollection.Add((NavigationMenuItemEntity)relatedEntity);
					break;
				case "WidgetCollection":
					_widgetCollection.Add((WidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ApplicationConfigurationEntity":
					DesetupSyncApplicationConfigurationEntity(false, true);
					break;
				case "LandingPageEntity":
					DesetupSyncLandingPageEntity(false, true);
					break;
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "ProductCategoryEntity":
					DesetupSyncProductCategoryEntity(false, true);
					break;
				case "NavigationMenuItemCollection":
					this.PerformRelatedEntityRemoval(_navigationMenuItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WidgetCollection":
					this.PerformRelatedEntityRemoval(_widgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_applicationConfigurationEntity!=null)
			{
				toReturn.Add(_applicationConfigurationEntity);
			}
			if(_landingPageEntity!=null)
			{
				toReturn.Add(_landingPageEntity);
			}
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_productCategoryEntity!=null)
			{
				toReturn.Add(_productCategoryEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_navigationMenuItemCollection);
			toReturn.Add(_widgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 actionId)
		{
			return FetchUsingPK(actionId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 actionId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(actionId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 actionId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(actionId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 actionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(actionId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ActionId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ActionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch)
		{
			return GetMultiNavigationMenuItemCollection(forceFetch, _navigationMenuItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NavigationMenuItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNavigationMenuItemCollection(forceFetch, _navigationMenuItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNavigationMenuItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuItemCollection GetMultiNavigationMenuItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNavigationMenuItemCollection || forceFetch || _alwaysFetchNavigationMenuItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_navigationMenuItemCollection);
				_navigationMenuItemCollection.SuppressClearInGetMulti=!forceFetch;
				_navigationMenuItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_navigationMenuItemCollection.GetMultiManyToOne(this, null, filter);
				_navigationMenuItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedNavigationMenuItemCollection = true;
			}
			return _navigationMenuItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'NavigationMenuItemCollection'. These settings will be taken into account
		/// when the property NavigationMenuItemCollection is requested or GetMultiNavigationMenuItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNavigationMenuItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_navigationMenuItemCollection.SortClauses=sortClauses;
			_navigationMenuItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch)
		{
			return GetMultiWidgetCollection(forceFetch, _widgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWidgetCollection(forceFetch, _widgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.WidgetCollection GetMultiWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWidgetCollection || forceFetch || _alwaysFetchWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_widgetCollection);
				_widgetCollection.SuppressClearInGetMulti=!forceFetch;
				_widgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_widgetCollection.GetMultiManyToOne(this, null, filter);
				_widgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedWidgetCollection = true;
			}
			return _widgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'WidgetCollection'. These settings will be taken into account
		/// when the property WidgetCollection is requested or GetMultiWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_widgetCollection.SortClauses=sortClauses;
			_widgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity()
		{
			return GetSingleApplicationConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ApplicationConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApplicationConfigurationEntity' which is related to this entity.</returns>
		public virtual ApplicationConfigurationEntity GetSingleApplicationConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedApplicationConfigurationEntity || forceFetch || _alwaysFetchApplicationConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApplicationConfigurationEntityUsingApplicationConfigurationId);
				ApplicationConfigurationEntity newEntity = new ApplicationConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApplicationConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (ApplicationConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_applicationConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ApplicationConfigurationEntity = newEntity;
				_alreadyFetchedApplicationConfigurationEntity = fetchResult;
			}
			return _applicationConfigurationEntity;
		}


		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public LandingPageEntity GetSingleLandingPageEntity()
		{
			return GetSingleLandingPageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public virtual LandingPageEntity GetSingleLandingPageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLandingPageEntity || forceFetch || _alwaysFetchLandingPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LandingPageEntityUsingLandingPageId);
				LandingPageEntity newEntity = new LandingPageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LandingPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LandingPageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_landingPageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LandingPageEntity = newEntity;
				_alreadyFetchedLandingPageEntity = fetchResult;
			}
			return _landingPageEntity;
		}


		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public ProductCategoryEntity GetSingleProductCategoryEntity()
		{
			return GetSingleProductCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public virtual ProductCategoryEntity GetSingleProductCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductCategoryEntity || forceFetch || _alwaysFetchProductCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductCategoryEntityUsingProductCategoryId);
				ProductCategoryEntity newEntity = new ProductCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductCategoryEntity = newEntity;
				_alreadyFetchedProductCategoryEntity = fetchResult;
			}
			return _productCategoryEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ApplicationConfigurationEntity", _applicationConfigurationEntity);
			toReturn.Add("LandingPageEntity", _landingPageEntity);
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("ProductCategoryEntity", _productCategoryEntity);
			toReturn.Add("NavigationMenuItemCollection", _navigationMenuItemCollection);
			toReturn.Add("WidgetCollection", _widgetCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <param name="validator">The validator object for this ActionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 actionId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(actionId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_navigationMenuItemCollection = new Obymobi.Data.CollectionClasses.NavigationMenuItemCollection();
			_navigationMenuItemCollection.SetContainingEntityInfo(this, "ActionEntity");

			_widgetCollection = new Obymobi.Data.CollectionClasses.WidgetCollection();
			_widgetCollection.SetContainingEntityInfo(this, "ActionEntity");
			_applicationConfigurationEntityReturnsNewIfNotFound = true;
			_landingPageEntityReturnsNewIfNotFound = true;
			_categoryEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_productCategoryEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LandingPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Resource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApplicationConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticActionRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, signalRelatedEntity, "ActionCollection", resetFKFields, new int[] { (int)ActionFieldIndex.ApplicationConfigurationId } );		
			_applicationConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _applicationConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApplicationConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_applicationConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncApplicationConfigurationEntity(true, true);
				_applicationConfigurationEntity = (ApplicationConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _applicationConfigurationEntity, new PropertyChangedEventHandler( OnApplicationConfigurationEntityPropertyChanged ), "ApplicationConfigurationEntity", Obymobi.Data.RelationClasses.StaticActionRelations.ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic, true, ref _alreadyFetchedApplicationConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApplicationConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _landingPageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLandingPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticActionRelations.LandingPageEntityUsingLandingPageIdStatic, true, signalRelatedEntity, "ActionCollection", resetFKFields, new int[] { (int)ActionFieldIndex.LandingPageId } );		
			_landingPageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _landingPageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLandingPageEntity(IEntityCore relatedEntity)
		{
			if(_landingPageEntity!=relatedEntity)
			{		
				DesetupSyncLandingPageEntity(true, true);
				_landingPageEntity = (LandingPageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticActionRelations.LandingPageEntityUsingLandingPageIdStatic, true, ref _alreadyFetchedLandingPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLandingPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticActionRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "ActionCollection", resetFKFields, new int[] { (int)ActionFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticActionRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticActionRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "ActionCollection", resetFKFields, new int[] { (int)ActionFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticActionRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticActionRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, signalRelatedEntity, "ActionCollection", resetFKFields, new int[] { (int)ActionFieldIndex.ProductCategoryId } );		
			_productCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductCategoryEntity(IEntityCore relatedEntity)
		{
			if(_productCategoryEntity!=relatedEntity)
			{		
				DesetupSyncProductCategoryEntity(true, true);
				_productCategoryEntity = (ProductCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticActionRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, ref _alreadyFetchedProductCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="actionId">PK value for Action which data should be fetched into this Action object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 actionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ActionFieldIndex.ActionId].ForcedCurrentValueWrite(actionId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateActionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ActionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ActionRelations Relations
		{
			get	{ return new ActionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NavigationMenuItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNavigationMenuItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.NavigationMenuItemCollection(), (IEntityRelation)GetRelationsForField("NavigationMenuItemCollection")[0], (int)Obymobi.Data.EntityType.ActionEntity, (int)Obymobi.Data.EntityType.NavigationMenuItemEntity, 0, null, null, null, "NavigationMenuItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Widget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetCollection(), (IEntityRelation)GetRelationsForField("WidgetCollection")[0], (int)Obymobi.Data.EntityType.ActionEntity, (int)Obymobi.Data.EntityType.WidgetEntity, 0, null, null, null, "WidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ApplicationConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApplicationConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ApplicationConfigurationCollection(), (IEntityRelation)GetRelationsForField("ApplicationConfigurationEntity")[0], (int)Obymobi.Data.EntityType.ActionEntity, (int)Obymobi.Data.EntityType.ApplicationConfigurationEntity, 0, null, null, null, "ApplicationConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("LandingPageEntity")[0], (int)Obymobi.Data.EntityType.ActionEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "LandingPageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.ActionEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.ActionEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryEntity")[0], (int)Obymobi.Data.EntityType.ActionEntity, (int)Obymobi.Data.EntityType.ProductCategoryEntity, 0, null, null, null, "ProductCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ActionId property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."ActionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ActionId
		{
			get { return (System.Int32)GetValue((int)ActionFieldIndex.ActionId, true); }
			set	{ SetValue((int)ActionFieldIndex.ActionId, value, true); }
		}

		/// <summary> The ActionType property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."ActionType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ActionType ActionType
		{
			get { return (Obymobi.Enums.ActionType)GetValue((int)ActionFieldIndex.ActionType, true); }
			set	{ SetValue((int)ActionFieldIndex.ActionType, value, true); }
		}

		/// <summary> The LandingPageId property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."LandingPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LandingPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ActionFieldIndex.LandingPageId, false); }
			set	{ SetValue((int)ActionFieldIndex.LandingPageId, value, true); }
		}

		/// <summary> The CategoryId property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ActionFieldIndex.CategoryId, false); }
			set	{ SetValue((int)ActionFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The ProductCategoryId property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."ProductCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ActionFieldIndex.ProductCategoryId, false); }
			set	{ SetValue((int)ActionFieldIndex.ProductCategoryId, value, true); }
		}

		/// <summary> The Resource property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."Resource"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Resource
		{
			get { return (System.String)GetValue((int)ActionFieldIndex.Resource, true); }
			set	{ SetValue((int)ActionFieldIndex.Resource, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)ActionFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)ActionFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)ActionFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)ActionFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ActionFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)ActionFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ActionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ActionFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)ActionFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)ActionFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ApplicationConfigurationId property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."ApplicationConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ApplicationConfigurationId
		{
			get { return (System.Int32)GetValue((int)ActionFieldIndex.ApplicationConfigurationId, true); }
			set	{ SetValue((int)ActionFieldIndex.ApplicationConfigurationId, value, true); }
		}

		/// <summary> The ProductId property of the Entity Action<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Action"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ActionFieldIndex.ProductId, false); }
			set	{ SetValue((int)ActionFieldIndex.ProductId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'NavigationMenuItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNavigationMenuItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.NavigationMenuItemCollection NavigationMenuItemCollection
		{
			get	{ return GetMultiNavigationMenuItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NavigationMenuItemCollection. When set to true, NavigationMenuItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NavigationMenuItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiNavigationMenuItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNavigationMenuItemCollection
		{
			get	{ return _alwaysFetchNavigationMenuItemCollection; }
			set	{ _alwaysFetchNavigationMenuItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NavigationMenuItemCollection already has been fetched. Setting this property to false when NavigationMenuItemCollection has been fetched
		/// will clear the NavigationMenuItemCollection collection well. Setting this property to true while NavigationMenuItemCollection hasn't been fetched disables lazy loading for NavigationMenuItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNavigationMenuItemCollection
		{
			get { return _alreadyFetchedNavigationMenuItemCollection;}
			set 
			{
				if(_alreadyFetchedNavigationMenuItemCollection && !value && (_navigationMenuItemCollection != null))
				{
					_navigationMenuItemCollection.Clear();
				}
				_alreadyFetchedNavigationMenuItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'WidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.WidgetCollection WidgetCollection
		{
			get	{ return GetMultiWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetCollection. When set to true, WidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetCollection
		{
			get	{ return _alwaysFetchWidgetCollection; }
			set	{ _alwaysFetchWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetCollection already has been fetched. Setting this property to false when WidgetCollection has been fetched
		/// will clear the WidgetCollection collection well. Setting this property to true while WidgetCollection hasn't been fetched disables lazy loading for WidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetCollection
		{
			get { return _alreadyFetchedWidgetCollection;}
			set 
			{
				if(_alreadyFetchedWidgetCollection && !value && (_widgetCollection != null))
				{
					_widgetCollection.Clear();
				}
				_alreadyFetchedWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ApplicationConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApplicationConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ApplicationConfigurationEntity ApplicationConfigurationEntity
		{
			get	{ return GetSingleApplicationConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApplicationConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionCollection", "ApplicationConfigurationEntity", _applicationConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ApplicationConfigurationEntity. When set to true, ApplicationConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ApplicationConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleApplicationConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApplicationConfigurationEntity
		{
			get	{ return _alwaysFetchApplicationConfigurationEntity; }
			set	{ _alwaysFetchApplicationConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ApplicationConfigurationEntity already has been fetched. Setting this property to false when ApplicationConfigurationEntity has been fetched
		/// will set ApplicationConfigurationEntity to null as well. Setting this property to true while ApplicationConfigurationEntity hasn't been fetched disables lazy loading for ApplicationConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApplicationConfigurationEntity
		{
			get { return _alreadyFetchedApplicationConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedApplicationConfigurationEntity && !value)
				{
					this.ApplicationConfigurationEntity = null;
				}
				_alreadyFetchedApplicationConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ApplicationConfigurationEntity is not found
		/// in the database. When set to true, ApplicationConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ApplicationConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _applicationConfigurationEntityReturnsNewIfNotFound; }
			set { _applicationConfigurationEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LandingPageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLandingPageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LandingPageEntity LandingPageEntity
		{
			get	{ return GetSingleLandingPageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLandingPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionCollection", "LandingPageEntity", _landingPageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageEntity. When set to true, LandingPageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageEntity is accessed. You can always execute a forced fetch by calling GetSingleLandingPageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageEntity
		{
			get	{ return _alwaysFetchLandingPageEntity; }
			set	{ _alwaysFetchLandingPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageEntity already has been fetched. Setting this property to false when LandingPageEntity has been fetched
		/// will set LandingPageEntity to null as well. Setting this property to true while LandingPageEntity hasn't been fetched disables lazy loading for LandingPageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageEntity
		{
			get { return _alreadyFetchedLandingPageEntity;}
			set 
			{
				if(_alreadyFetchedLandingPageEntity && !value)
				{
					this.LandingPageEntity = null;
				}
				_alreadyFetchedLandingPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LandingPageEntity is not found
		/// in the database. When set to true, LandingPageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LandingPageEntityReturnsNewIfNotFound
		{
			get	{ return _landingPageEntityReturnsNewIfNotFound; }
			set { _landingPageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductCategoryEntity ProductCategoryEntity
		{
			get	{ return GetSingleProductCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ActionCollection", "ProductCategoryEntity", _productCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryEntity. When set to true, ProductCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleProductCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryEntity
		{
			get	{ return _alwaysFetchProductCategoryEntity; }
			set	{ _alwaysFetchProductCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryEntity already has been fetched. Setting this property to false when ProductCategoryEntity has been fetched
		/// will set ProductCategoryEntity to null as well. Setting this property to true while ProductCategoryEntity hasn't been fetched disables lazy loading for ProductCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryEntity
		{
			get { return _alreadyFetchedProductCategoryEntity;}
			set 
			{
				if(_alreadyFetchedProductCategoryEntity && !value)
				{
					this.ProductCategoryEntity = null;
				}
				_alreadyFetchedProductCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductCategoryEntity is not found
		/// in the database. When set to true, ProductCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _productCategoryEntityReturnsNewIfNotFound; }
			set { _productCategoryEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ActionEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
