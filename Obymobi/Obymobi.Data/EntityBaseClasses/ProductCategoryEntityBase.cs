﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ProductCategory'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ProductCategoryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ProductCategoryEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.ActionCollection	_actionCollection;
		private bool	_alwaysFetchActionCollection, _alreadyFetchedActionCollection;
		private Obymobi.Data.CollectionClasses.AvailabilityCollection	_availabilityCollection;
		private bool	_alwaysFetchAvailabilityCollection, _alreadyFetchedAvailabilityCollection;
		private Obymobi.Data.CollectionClasses.CategorySuggestionCollection	_categorySuggestionCollection;
		private bool	_alwaysFetchCategorySuggestionCollection, _alreadyFetchedCategorySuggestionCollection;
		private Obymobi.Data.CollectionClasses.MessageCollection	_messageCollection;
		private bool	_alwaysFetchMessageCollection, _alreadyFetchedMessageCollection;
		private Obymobi.Data.CollectionClasses.MessageTemplateCollection	_messageTemplateCollection;
		private bool	_alwaysFetchMessageTemplateCollection, _alreadyFetchedMessageTemplateCollection;
		private Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection	_productCategorySuggestionCollection;
		private bool	_alwaysFetchProductCategorySuggestionCollection, _alreadyFetchedProductCategorySuggestionCollection;
		private Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection	_suggestedProductCategorySuggestionCollection;
		private bool	_alwaysFetchSuggestedProductCategorySuggestionCollection, _alreadyFetchedSuggestedProductCategorySuggestionCollection;
		private Obymobi.Data.CollectionClasses.ProductCategoryTagCollection	_productCategoryTagCollection;
		private bool	_alwaysFetchProductCategoryTagCollection, _alreadyFetchedProductCategoryTagCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageCollection	_scheduledMessageCollection;
		private bool	_alwaysFetchScheduledMessageCollection, _alreadyFetchedScheduledMessageCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name ActionCollection</summary>
			public static readonly string ActionCollection = "ActionCollection";
			/// <summary>Member name AvailabilityCollection</summary>
			public static readonly string AvailabilityCollection = "AvailabilityCollection";
			/// <summary>Member name CategorySuggestionCollection</summary>
			public static readonly string CategorySuggestionCollection = "CategorySuggestionCollection";
			/// <summary>Member name MessageCollection</summary>
			public static readonly string MessageCollection = "MessageCollection";
			/// <summary>Member name MessageTemplateCollection</summary>
			public static readonly string MessageTemplateCollection = "MessageTemplateCollection";
			/// <summary>Member name ProductCategorySuggestionCollection</summary>
			public static readonly string ProductCategorySuggestionCollection = "ProductCategorySuggestionCollection";
			/// <summary>Member name SuggestedProductCategorySuggestionCollection</summary>
			public static readonly string SuggestedProductCategorySuggestionCollection = "SuggestedProductCategorySuggestionCollection";
			/// <summary>Member name ProductCategoryTagCollection</summary>
			public static readonly string ProductCategoryTagCollection = "ProductCategoryTagCollection";
			/// <summary>Member name ScheduledMessageCollection</summary>
			public static readonly string ScheduledMessageCollection = "ScheduledMessageCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProductCategoryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ProductCategoryEntityBase() :base("ProductCategoryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		protected ProductCategoryEntityBase(System.Int32 productCategoryId):base("ProductCategoryEntity")
		{
			InitClassFetch(productCategoryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ProductCategoryEntityBase(System.Int32 productCategoryId, IPrefetchPath prefetchPathToUse): base("ProductCategoryEntity")
		{
			InitClassFetch(productCategoryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="validator">The custom validator object for this ProductCategoryEntity</param>
		protected ProductCategoryEntityBase(System.Int32 productCategoryId, IValidator validator):base("ProductCategoryEntity")
		{
			InitClassFetch(productCategoryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductCategoryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_actionCollection = (Obymobi.Data.CollectionClasses.ActionCollection)info.GetValue("_actionCollection", typeof(Obymobi.Data.CollectionClasses.ActionCollection));
			_alwaysFetchActionCollection = info.GetBoolean("_alwaysFetchActionCollection");
			_alreadyFetchedActionCollection = info.GetBoolean("_alreadyFetchedActionCollection");

			_availabilityCollection = (Obymobi.Data.CollectionClasses.AvailabilityCollection)info.GetValue("_availabilityCollection", typeof(Obymobi.Data.CollectionClasses.AvailabilityCollection));
			_alwaysFetchAvailabilityCollection = info.GetBoolean("_alwaysFetchAvailabilityCollection");
			_alreadyFetchedAvailabilityCollection = info.GetBoolean("_alreadyFetchedAvailabilityCollection");

			_categorySuggestionCollection = (Obymobi.Data.CollectionClasses.CategorySuggestionCollection)info.GetValue("_categorySuggestionCollection", typeof(Obymobi.Data.CollectionClasses.CategorySuggestionCollection));
			_alwaysFetchCategorySuggestionCollection = info.GetBoolean("_alwaysFetchCategorySuggestionCollection");
			_alreadyFetchedCategorySuggestionCollection = info.GetBoolean("_alreadyFetchedCategorySuggestionCollection");

			_messageCollection = (Obymobi.Data.CollectionClasses.MessageCollection)info.GetValue("_messageCollection", typeof(Obymobi.Data.CollectionClasses.MessageCollection));
			_alwaysFetchMessageCollection = info.GetBoolean("_alwaysFetchMessageCollection");
			_alreadyFetchedMessageCollection = info.GetBoolean("_alreadyFetchedMessageCollection");

			_messageTemplateCollection = (Obymobi.Data.CollectionClasses.MessageTemplateCollection)info.GetValue("_messageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.MessageTemplateCollection));
			_alwaysFetchMessageTemplateCollection = info.GetBoolean("_alwaysFetchMessageTemplateCollection");
			_alreadyFetchedMessageTemplateCollection = info.GetBoolean("_alreadyFetchedMessageTemplateCollection");

			_productCategorySuggestionCollection = (Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection)info.GetValue("_productCategorySuggestionCollection", typeof(Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection));
			_alwaysFetchProductCategorySuggestionCollection = info.GetBoolean("_alwaysFetchProductCategorySuggestionCollection");
			_alreadyFetchedProductCategorySuggestionCollection = info.GetBoolean("_alreadyFetchedProductCategorySuggestionCollection");

			_suggestedProductCategorySuggestionCollection = (Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection)info.GetValue("_suggestedProductCategorySuggestionCollection", typeof(Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection));
			_alwaysFetchSuggestedProductCategorySuggestionCollection = info.GetBoolean("_alwaysFetchSuggestedProductCategorySuggestionCollection");
			_alreadyFetchedSuggestedProductCategorySuggestionCollection = info.GetBoolean("_alreadyFetchedSuggestedProductCategorySuggestionCollection");

			_productCategoryTagCollection = (Obymobi.Data.CollectionClasses.ProductCategoryTagCollection)info.GetValue("_productCategoryTagCollection", typeof(Obymobi.Data.CollectionClasses.ProductCategoryTagCollection));
			_alwaysFetchProductCategoryTagCollection = info.GetBoolean("_alwaysFetchProductCategoryTagCollection");
			_alreadyFetchedProductCategoryTagCollection = info.GetBoolean("_alreadyFetchedProductCategoryTagCollection");

			_scheduledMessageCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageCollection)info.GetValue("_scheduledMessageCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageCollection));
			_alwaysFetchScheduledMessageCollection = info.GetBoolean("_alwaysFetchScheduledMessageCollection");
			_alreadyFetchedScheduledMessageCollection = info.GetBoolean("_alreadyFetchedScheduledMessageCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProductCategoryFieldIndex)fieldIndex)
			{
				case ProductCategoryFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case ProductCategoryFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedActionCollection = (_actionCollection.Count > 0);
			_alreadyFetchedAvailabilityCollection = (_availabilityCollection.Count > 0);
			_alreadyFetchedCategorySuggestionCollection = (_categorySuggestionCollection.Count > 0);
			_alreadyFetchedMessageCollection = (_messageCollection.Count > 0);
			_alreadyFetchedMessageTemplateCollection = (_messageTemplateCollection.Count > 0);
			_alreadyFetchedProductCategorySuggestionCollection = (_productCategorySuggestionCollection.Count > 0);
			_alreadyFetchedSuggestedProductCategorySuggestionCollection = (_suggestedProductCategorySuggestionCollection.Count > 0);
			_alreadyFetchedProductCategoryTagCollection = (_productCategoryTagCollection.Count > 0);
			_alreadyFetchedScheduledMessageCollection = (_scheduledMessageCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingProductCategoryId);
					break;
				case "ActionCollection":
					toReturn.Add(Relations.ActionEntityUsingProductCategoryId);
					break;
				case "AvailabilityCollection":
					toReturn.Add(Relations.AvailabilityEntityUsingActionProductCategoryId);
					break;
				case "CategorySuggestionCollection":
					toReturn.Add(Relations.CategorySuggestionEntityUsingProductCategoryId);
					break;
				case "MessageCollection":
					toReturn.Add(Relations.MessageEntityUsingProductCategoryId);
					break;
				case "MessageTemplateCollection":
					toReturn.Add(Relations.MessageTemplateEntityUsingProductCategoryId);
					break;
				case "ProductCategorySuggestionCollection":
					toReturn.Add(Relations.ProductCategorySuggestionEntityUsingProductCategoryId);
					break;
				case "SuggestedProductCategorySuggestionCollection":
					toReturn.Add(Relations.ProductCategorySuggestionEntityUsingSuggestedProductCategoryId);
					break;
				case "ProductCategoryTagCollection":
					toReturn.Add(Relations.ProductCategoryTagEntityUsingProductCategoryId);
					break;
				case "ScheduledMessageCollection":
					toReturn.Add(Relations.ScheduledMessageEntityUsingProductCategoryId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingProductCategoryId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_actionCollection", (!this.MarkedForDeletion?_actionCollection:null));
			info.AddValue("_alwaysFetchActionCollection", _alwaysFetchActionCollection);
			info.AddValue("_alreadyFetchedActionCollection", _alreadyFetchedActionCollection);
			info.AddValue("_availabilityCollection", (!this.MarkedForDeletion?_availabilityCollection:null));
			info.AddValue("_alwaysFetchAvailabilityCollection", _alwaysFetchAvailabilityCollection);
			info.AddValue("_alreadyFetchedAvailabilityCollection", _alreadyFetchedAvailabilityCollection);
			info.AddValue("_categorySuggestionCollection", (!this.MarkedForDeletion?_categorySuggestionCollection:null));
			info.AddValue("_alwaysFetchCategorySuggestionCollection", _alwaysFetchCategorySuggestionCollection);
			info.AddValue("_alreadyFetchedCategorySuggestionCollection", _alreadyFetchedCategorySuggestionCollection);
			info.AddValue("_messageCollection", (!this.MarkedForDeletion?_messageCollection:null));
			info.AddValue("_alwaysFetchMessageCollection", _alwaysFetchMessageCollection);
			info.AddValue("_alreadyFetchedMessageCollection", _alreadyFetchedMessageCollection);
			info.AddValue("_messageTemplateCollection", (!this.MarkedForDeletion?_messageTemplateCollection:null));
			info.AddValue("_alwaysFetchMessageTemplateCollection", _alwaysFetchMessageTemplateCollection);
			info.AddValue("_alreadyFetchedMessageTemplateCollection", _alreadyFetchedMessageTemplateCollection);
			info.AddValue("_productCategorySuggestionCollection", (!this.MarkedForDeletion?_productCategorySuggestionCollection:null));
			info.AddValue("_alwaysFetchProductCategorySuggestionCollection", _alwaysFetchProductCategorySuggestionCollection);
			info.AddValue("_alreadyFetchedProductCategorySuggestionCollection", _alreadyFetchedProductCategorySuggestionCollection);
			info.AddValue("_suggestedProductCategorySuggestionCollection", (!this.MarkedForDeletion?_suggestedProductCategorySuggestionCollection:null));
			info.AddValue("_alwaysFetchSuggestedProductCategorySuggestionCollection", _alwaysFetchSuggestedProductCategorySuggestionCollection);
			info.AddValue("_alreadyFetchedSuggestedProductCategorySuggestionCollection", _alreadyFetchedSuggestedProductCategorySuggestionCollection);
			info.AddValue("_productCategoryTagCollection", (!this.MarkedForDeletion?_productCategoryTagCollection:null));
			info.AddValue("_alwaysFetchProductCategoryTagCollection", _alwaysFetchProductCategoryTagCollection);
			info.AddValue("_alreadyFetchedProductCategoryTagCollection", _alreadyFetchedProductCategoryTagCollection);
			info.AddValue("_scheduledMessageCollection", (!this.MarkedForDeletion?_scheduledMessageCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageCollection", _alwaysFetchScheduledMessageCollection);
			info.AddValue("_alreadyFetchedScheduledMessageCollection", _alreadyFetchedScheduledMessageCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "ActionCollection":
					_alreadyFetchedActionCollection = true;
					if(entity!=null)
					{
						this.ActionCollection.Add((ActionEntity)entity);
					}
					break;
				case "AvailabilityCollection":
					_alreadyFetchedAvailabilityCollection = true;
					if(entity!=null)
					{
						this.AvailabilityCollection.Add((AvailabilityEntity)entity);
					}
					break;
				case "CategorySuggestionCollection":
					_alreadyFetchedCategorySuggestionCollection = true;
					if(entity!=null)
					{
						this.CategorySuggestionCollection.Add((CategorySuggestionEntity)entity);
					}
					break;
				case "MessageCollection":
					_alreadyFetchedMessageCollection = true;
					if(entity!=null)
					{
						this.MessageCollection.Add((MessageEntity)entity);
					}
					break;
				case "MessageTemplateCollection":
					_alreadyFetchedMessageTemplateCollection = true;
					if(entity!=null)
					{
						this.MessageTemplateCollection.Add((MessageTemplateEntity)entity);
					}
					break;
				case "ProductCategorySuggestionCollection":
					_alreadyFetchedProductCategorySuggestionCollection = true;
					if(entity!=null)
					{
						this.ProductCategorySuggestionCollection.Add((ProductCategorySuggestionEntity)entity);
					}
					break;
				case "SuggestedProductCategorySuggestionCollection":
					_alreadyFetchedSuggestedProductCategorySuggestionCollection = true;
					if(entity!=null)
					{
						this.SuggestedProductCategorySuggestionCollection.Add((ProductCategorySuggestionEntity)entity);
					}
					break;
				case "ProductCategoryTagCollection":
					_alreadyFetchedProductCategoryTagCollection = true;
					if(entity!=null)
					{
						this.ProductCategoryTagCollection.Add((ProductCategoryTagEntity)entity);
					}
					break;
				case "ScheduledMessageCollection":
					_alreadyFetchedScheduledMessageCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageCollection.Add((ScheduledMessageEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "ActionCollection":
					_actionCollection.Add((ActionEntity)relatedEntity);
					break;
				case "AvailabilityCollection":
					_availabilityCollection.Add((AvailabilityEntity)relatedEntity);
					break;
				case "CategorySuggestionCollection":
					_categorySuggestionCollection.Add((CategorySuggestionEntity)relatedEntity);
					break;
				case "MessageCollection":
					_messageCollection.Add((MessageEntity)relatedEntity);
					break;
				case "MessageTemplateCollection":
					_messageTemplateCollection.Add((MessageTemplateEntity)relatedEntity);
					break;
				case "ProductCategorySuggestionCollection":
					_productCategorySuggestionCollection.Add((ProductCategorySuggestionEntity)relatedEntity);
					break;
				case "SuggestedProductCategorySuggestionCollection":
					_suggestedProductCategorySuggestionCollection.Add((ProductCategorySuggestionEntity)relatedEntity);
					break;
				case "ProductCategoryTagCollection":
					_productCategoryTagCollection.Add((ProductCategoryTagEntity)relatedEntity);
					break;
				case "ScheduledMessageCollection":
					_scheduledMessageCollection.Add((ScheduledMessageEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ActionCollection":
					this.PerformRelatedEntityRemoval(_actionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AvailabilityCollection":
					this.PerformRelatedEntityRemoval(_availabilityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CategorySuggestionCollection":
					this.PerformRelatedEntityRemoval(_categorySuggestionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageCollection":
					this.PerformRelatedEntityRemoval(_messageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MessageTemplateCollection":
					this.PerformRelatedEntityRemoval(_messageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCategorySuggestionCollection":
					this.PerformRelatedEntityRemoval(_productCategorySuggestionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SuggestedProductCategorySuggestionCollection":
					this.PerformRelatedEntityRemoval(_suggestedProductCategorySuggestionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductCategoryTagCollection":
					this.PerformRelatedEntityRemoval(_productCategoryTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_actionCollection);
			toReturn.Add(_availabilityCollection);
			toReturn.Add(_categorySuggestionCollection);
			toReturn.Add(_messageCollection);
			toReturn.Add(_messageTemplateCollection);
			toReturn.Add(_productCategorySuggestionCollection);
			toReturn.Add(_suggestedProductCategorySuggestionCollection);
			toReturn.Add(_productCategoryTagCollection);
			toReturn.Add(_scheduledMessageCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="categoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductIdCategoryId(System.Int32 productId, System.Int32 categoryId)
		{
			return FetchUsingUCProductIdCategoryId( productId,  categoryId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="categoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductIdCategoryId(System.Int32 productId, System.Int32 categoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCProductIdCategoryId( productId,  categoryId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="categoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductIdCategoryId(System.Int32 productId, System.Int32 categoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCProductIdCategoryId( productId,  categoryId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="productId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="categoryId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCProductIdCategoryId(System.Int32 productId, System.Int32 categoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((ProductCategoryDAO)CreateDAOInstance()).FetchProductCategoryUsingUCProductIdCategoryId(this, this.Transaction, productId, categoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productCategoryId)
		{
			return FetchUsingPK(productCategoryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productCategoryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(productCategoryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(productCategoryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 productCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(productCategoryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProductCategoryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProductCategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ActionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch)
		{
			return GetMultiActionCollection(forceFetch, _actionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ActionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiActionCollection(forceFetch, _actionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiActionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ActionCollection GetMultiActionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedActionCollection || forceFetch || _alwaysFetchActionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_actionCollection);
				_actionCollection.SuppressClearInGetMulti=!forceFetch;
				_actionCollection.EntityFactoryToUse = entityFactoryToUse;
				_actionCollection.GetMultiManyToOne(null, null, null, null, this, filter);
				_actionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedActionCollection = true;
			}
			return _actionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ActionCollection'. These settings will be taken into account
		/// when the property ActionCollection is requested or GetMultiActionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersActionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_actionCollection.SortClauses=sortClauses;
			_actionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AvailabilityEntity'</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAvailabilityCollection(forceFetch, _availabilityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAvailabilityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection GetMultiAvailabilityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAvailabilityCollection || forceFetch || _alwaysFetchAvailabilityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_availabilityCollection);
				_availabilityCollection.SuppressClearInGetMulti=!forceFetch;
				_availabilityCollection.EntityFactoryToUse = entityFactoryToUse;
				_availabilityCollection.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_availabilityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAvailabilityCollection = true;
			}
			return _availabilityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AvailabilityCollection'. These settings will be taken into account
		/// when the property AvailabilityCollection is requested or GetMultiAvailabilityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAvailabilityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_availabilityCollection.SortClauses=sortClauses;
			_availabilityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch)
		{
			return GetMultiCategorySuggestionCollection(forceFetch, _categorySuggestionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategorySuggestionCollection(forceFetch, _categorySuggestionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategorySuggestionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategorySuggestionCollection GetMultiCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategorySuggestionCollection || forceFetch || _alwaysFetchCategorySuggestionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categorySuggestionCollection);
				_categorySuggestionCollection.SuppressClearInGetMulti=!forceFetch;
				_categorySuggestionCollection.EntityFactoryToUse = entityFactoryToUse;
				_categorySuggestionCollection.GetMultiManyToOne(null, null, this, filter);
				_categorySuggestionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategorySuggestionCollection = true;
			}
			return _categorySuggestionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategorySuggestionCollection'. These settings will be taken into account
		/// when the property CategorySuggestionCollection is requested or GetMultiCategorySuggestionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategorySuggestionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categorySuggestionCollection.SortClauses=sortClauses;
			_categorySuggestionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageCollection(forceFetch, _messageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection GetMultiMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageCollection || forceFetch || _alwaysFetchMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageCollection);
				_messageCollection.SuppressClearInGetMulti=!forceFetch;
				_messageCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, filter);
				_messageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageCollection = true;
			}
			return _messageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageCollection'. These settings will be taken into account
		/// when the property MessageCollection is requested or GetMultiMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageCollection.SortClauses=sortClauses;
			_messageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageTemplateCollection(forceFetch, _messageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection GetMultiMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageTemplateCollection || forceFetch || _alwaysFetchMessageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageTemplateCollection);
				_messageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_messageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageTemplateCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, filter);
				_messageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageTemplateCollection = true;
			}
			return _messageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageTemplateCollection'. These settings will be taken into account
		/// when the property MessageTemplateCollection is requested or GetMultiMessageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageTemplateCollection.SortClauses=sortClauses;
			_messageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiProductCategorySuggestionCollection(bool forceFetch)
		{
			return GetMultiProductCategorySuggestionCollection(forceFetch, _productCategorySuggestionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiProductCategorySuggestionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCategorySuggestionCollection(forceFetch, _productCategorySuggestionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiProductCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCategorySuggestionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiProductCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCategorySuggestionCollection || forceFetch || _alwaysFetchProductCategorySuggestionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCategorySuggestionCollection);
				_productCategorySuggestionCollection.SuppressClearInGetMulti=!forceFetch;
				_productCategorySuggestionCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCategorySuggestionCollection.GetMultiManyToOne(this, null, filter);
				_productCategorySuggestionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCategorySuggestionCollection = true;
			}
			return _productCategorySuggestionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCategorySuggestionCollection'. These settings will be taken into account
		/// when the property ProductCategorySuggestionCollection is requested or GetMultiProductCategorySuggestionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCategorySuggestionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCategorySuggestionCollection.SortClauses=sortClauses;
			_productCategorySuggestionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiSuggestedProductCategorySuggestionCollection(bool forceFetch)
		{
			return GetMultiSuggestedProductCategorySuggestionCollection(forceFetch, _suggestedProductCategorySuggestionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategorySuggestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiSuggestedProductCategorySuggestionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSuggestedProductCategorySuggestionCollection(forceFetch, _suggestedProductCategorySuggestionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiSuggestedProductCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSuggestedProductCategorySuggestionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection GetMultiSuggestedProductCategorySuggestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSuggestedProductCategorySuggestionCollection || forceFetch || _alwaysFetchSuggestedProductCategorySuggestionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_suggestedProductCategorySuggestionCollection);
				_suggestedProductCategorySuggestionCollection.SuppressClearInGetMulti=!forceFetch;
				_suggestedProductCategorySuggestionCollection.EntityFactoryToUse = entityFactoryToUse;
				_suggestedProductCategorySuggestionCollection.GetMultiManyToOne(null, this, filter);
				_suggestedProductCategorySuggestionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSuggestedProductCategorySuggestionCollection = true;
			}
			return _suggestedProductCategorySuggestionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SuggestedProductCategorySuggestionCollection'. These settings will be taken into account
		/// when the property SuggestedProductCategorySuggestionCollection is requested or GetMultiSuggestedProductCategorySuggestionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSuggestedProductCategorySuggestionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_suggestedProductCategorySuggestionCollection.SortClauses=sortClauses;
			_suggestedProductCategorySuggestionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, _productCategoryTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductCategoryTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, _productCategoryTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductCategoryTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryTagCollection GetMultiProductCategoryTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductCategoryTagCollection || forceFetch || _alwaysFetchProductCategoryTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCategoryTagCollection);
				_productCategoryTagCollection.SuppressClearInGetMulti=!forceFetch;
				_productCategoryTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_productCategoryTagCollection.GetMultiManyToOne(null, this, null, filter);
				_productCategoryTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCategoryTagCollection = true;
			}
			return _productCategoryTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCategoryTagCollection'. These settings will be taken into account
		/// when the property ProductCategoryTagCollection is requested or GetMultiProductCategoryTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCategoryTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCategoryTagCollection.SortClauses=sortClauses;
			_productCategoryTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageCollection || forceFetch || _alwaysFetchScheduledMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageCollection);
				_scheduledMessageCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageCollection.GetMultiManyToOne(null, null, null, null, null, null, this, null, filter);
				_scheduledMessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageCollection = true;
			}
			return _scheduledMessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageCollection'. These settings will be taken into account
		/// when the property ScheduledMessageCollection is requested or GetMultiScheduledMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageCollection.SortClauses=sortClauses;
			_scheduledMessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId);
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId);
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("ActionCollection", _actionCollection);
			toReturn.Add("AvailabilityCollection", _availabilityCollection);
			toReturn.Add("CategorySuggestionCollection", _categorySuggestionCollection);
			toReturn.Add("MessageCollection", _messageCollection);
			toReturn.Add("MessageTemplateCollection", _messageTemplateCollection);
			toReturn.Add("ProductCategorySuggestionCollection", _productCategorySuggestionCollection);
			toReturn.Add("SuggestedProductCategorySuggestionCollection", _suggestedProductCategorySuggestionCollection);
			toReturn.Add("ProductCategoryTagCollection", _productCategoryTagCollection);
			toReturn.Add("ScheduledMessageCollection", _scheduledMessageCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="validator">The validator object for this ProductCategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 productCategoryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(productCategoryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_actionCollection = new Obymobi.Data.CollectionClasses.ActionCollection();
			_actionCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_availabilityCollection = new Obymobi.Data.CollectionClasses.AvailabilityCollection();
			_availabilityCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_categorySuggestionCollection = new Obymobi.Data.CollectionClasses.CategorySuggestionCollection();
			_categorySuggestionCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_messageCollection = new Obymobi.Data.CollectionClasses.MessageCollection();
			_messageCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_messageTemplateCollection = new Obymobi.Data.CollectionClasses.MessageTemplateCollection();
			_messageTemplateCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_productCategorySuggestionCollection = new Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection();
			_productCategorySuggestionCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_suggestedProductCategorySuggestionCollection = new Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection();
			_suggestedProductCategorySuggestionCollection.SetContainingEntityInfo(this, "SuggestedProductCategoryEntity");

			_productCategoryTagCollection = new Obymobi.Data.CollectionClasses.ProductCategoryTagCollection();
			_productCategoryTagCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_scheduledMessageCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageCollection();
			_scheduledMessageCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "ProductCategoryEntity");
			_categoryEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InheritSuggestions", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticProductCategoryRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "ProductCategoryCollection", resetFKFields, new int[] { (int)ProductCategoryFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticProductCategoryRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticProductCategoryRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "ProductCategoryCollection", resetFKFields, new int[] { (int)ProductCategoryFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticProductCategoryRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 productCategoryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProductCategoryFieldIndex.ProductCategoryId].ForcedCurrentValueWrite(productCategoryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProductCategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProductCategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProductCategoryRelations Relations
		{
			get	{ return new ProductCategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Action' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathActionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ActionCollection(), (IEntityRelation)GetRelationsForField("ActionCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.ActionEntity, 0, null, null, null, "ActionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Availability' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAvailabilityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AvailabilityCollection(), (IEntityRelation)GetRelationsForField("AvailabilityCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.AvailabilityEntity, 0, null, null, null, "AvailabilityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CategorySuggestion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategorySuggestionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategorySuggestionCollection(), (IEntityRelation)GetRelationsForField("CategorySuggestionCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.CategorySuggestionEntity, 0, null, null, null, "CategorySuggestionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Message' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageCollection(), (IEntityRelation)GetRelationsForField("MessageCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.MessageEntity, 0, null, null, null, "MessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageTemplateCollection(), (IEntityRelation)GetRelationsForField("MessageTemplateCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.MessageTemplateEntity, 0, null, null, null, "MessageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategorySuggestion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategorySuggestionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection(), (IEntityRelation)GetRelationsForField("ProductCategorySuggestionCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.ProductCategorySuggestionEntity, 0, null, null, null, "ProductCategorySuggestionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategorySuggestion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSuggestedProductCategorySuggestionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection(), (IEntityRelation)GetRelationsForField("SuggestedProductCategorySuggestionCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.ProductCategorySuggestionEntity, 0, null, null, null, "SuggestedProductCategorySuggestionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategoryTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryTagCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryTagCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.ProductCategoryTagEntity, 0, null, null, null, "ProductCategoryTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.ProductCategoryEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ProductCategoryId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."ProductCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ProductCategoryId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.ProductCategoryId, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.ProductCategoryId, value, true); }
		}

		/// <summary> The ProductId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProductId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.ProductId, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.ProductId, value, true); }
		}

		/// <summary> The CategoryId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CategoryId
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.CategoryId, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.SortOrder, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ProductCategoryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductCategoryFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)ProductCategoryFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductCategoryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ProductCategoryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductCategoryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ProductCategoryFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The InheritSuggestions property of the Entity ProductCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ProductCategory"."InheritSuggestions"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean InheritSuggestions
		{
			get { return (System.Boolean)GetValue((int)ProductCategoryFieldIndex.InheritSuggestions, true); }
			set	{ SetValue((int)ProductCategoryFieldIndex.InheritSuggestions, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ActionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiActionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ActionCollection ActionCollection
		{
			get	{ return GetMultiActionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ActionCollection. When set to true, ActionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ActionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiActionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchActionCollection
		{
			get	{ return _alwaysFetchActionCollection; }
			set	{ _alwaysFetchActionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ActionCollection already has been fetched. Setting this property to false when ActionCollection has been fetched
		/// will clear the ActionCollection collection well. Setting this property to true while ActionCollection hasn't been fetched disables lazy loading for ActionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedActionCollection
		{
			get { return _alreadyFetchedActionCollection;}
			set 
			{
				if(_alreadyFetchedActionCollection && !value && (_actionCollection != null))
				{
					_actionCollection.Clear();
				}
				_alreadyFetchedActionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AvailabilityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAvailabilityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AvailabilityCollection AvailabilityCollection
		{
			get	{ return GetMultiAvailabilityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AvailabilityCollection. When set to true, AvailabilityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AvailabilityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAvailabilityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAvailabilityCollection
		{
			get	{ return _alwaysFetchAvailabilityCollection; }
			set	{ _alwaysFetchAvailabilityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AvailabilityCollection already has been fetched. Setting this property to false when AvailabilityCollection has been fetched
		/// will clear the AvailabilityCollection collection well. Setting this property to true while AvailabilityCollection hasn't been fetched disables lazy loading for AvailabilityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAvailabilityCollection
		{
			get { return _alreadyFetchedAvailabilityCollection;}
			set 
			{
				if(_alreadyFetchedAvailabilityCollection && !value && (_availabilityCollection != null))
				{
					_availabilityCollection.Clear();
				}
				_alreadyFetchedAvailabilityCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CategorySuggestionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategorySuggestionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategorySuggestionCollection CategorySuggestionCollection
		{
			get	{ return GetMultiCategorySuggestionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategorySuggestionCollection. When set to true, CategorySuggestionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategorySuggestionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategorySuggestionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategorySuggestionCollection
		{
			get	{ return _alwaysFetchCategorySuggestionCollection; }
			set	{ _alwaysFetchCategorySuggestionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategorySuggestionCollection already has been fetched. Setting this property to false when CategorySuggestionCollection has been fetched
		/// will clear the CategorySuggestionCollection collection well. Setting this property to true while CategorySuggestionCollection hasn't been fetched disables lazy loading for CategorySuggestionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategorySuggestionCollection
		{
			get { return _alreadyFetchedCategorySuggestionCollection;}
			set 
			{
				if(_alreadyFetchedCategorySuggestionCollection && !value && (_categorySuggestionCollection != null))
				{
					_categorySuggestionCollection.Clear();
				}
				_alreadyFetchedCategorySuggestionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageCollection MessageCollection
		{
			get	{ return GetMultiMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageCollection. When set to true, MessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageCollection
		{
			get	{ return _alwaysFetchMessageCollection; }
			set	{ _alwaysFetchMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageCollection already has been fetched. Setting this property to false when MessageCollection has been fetched
		/// will clear the MessageCollection collection well. Setting this property to true while MessageCollection hasn't been fetched disables lazy loading for MessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageCollection
		{
			get { return _alreadyFetchedMessageCollection;}
			set 
			{
				if(_alreadyFetchedMessageCollection && !value && (_messageCollection != null))
				{
					_messageCollection.Clear();
				}
				_alreadyFetchedMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MessageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCollection MessageTemplateCollection
		{
			get	{ return GetMultiMessageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageTemplateCollection. When set to true, MessageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageTemplateCollection
		{
			get	{ return _alwaysFetchMessageTemplateCollection; }
			set	{ _alwaysFetchMessageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageTemplateCollection already has been fetched. Setting this property to false when MessageTemplateCollection has been fetched
		/// will clear the MessageTemplateCollection collection well. Setting this property to true while MessageTemplateCollection hasn't been fetched disables lazy loading for MessageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageTemplateCollection
		{
			get { return _alreadyFetchedMessageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedMessageTemplateCollection && !value && (_messageTemplateCollection != null))
				{
					_messageTemplateCollection.Clear();
				}
				_alreadyFetchedMessageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCategorySuggestionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection ProductCategorySuggestionCollection
		{
			get	{ return GetMultiProductCategorySuggestionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategorySuggestionCollection. When set to true, ProductCategorySuggestionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategorySuggestionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCategorySuggestionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategorySuggestionCollection
		{
			get	{ return _alwaysFetchProductCategorySuggestionCollection; }
			set	{ _alwaysFetchProductCategorySuggestionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategorySuggestionCollection already has been fetched. Setting this property to false when ProductCategorySuggestionCollection has been fetched
		/// will clear the ProductCategorySuggestionCollection collection well. Setting this property to true while ProductCategorySuggestionCollection hasn't been fetched disables lazy loading for ProductCategorySuggestionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategorySuggestionCollection
		{
			get { return _alreadyFetchedProductCategorySuggestionCollection;}
			set 
			{
				if(_alreadyFetchedProductCategorySuggestionCollection && !value && (_productCategorySuggestionCollection != null))
				{
					_productCategorySuggestionCollection.Clear();
				}
				_alreadyFetchedProductCategorySuggestionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductCategorySuggestionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSuggestedProductCategorySuggestionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCategorySuggestionCollection SuggestedProductCategorySuggestionCollection
		{
			get	{ return GetMultiSuggestedProductCategorySuggestionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SuggestedProductCategorySuggestionCollection. When set to true, SuggestedProductCategorySuggestionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SuggestedProductCategorySuggestionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSuggestedProductCategorySuggestionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSuggestedProductCategorySuggestionCollection
		{
			get	{ return _alwaysFetchSuggestedProductCategorySuggestionCollection; }
			set	{ _alwaysFetchSuggestedProductCategorySuggestionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SuggestedProductCategorySuggestionCollection already has been fetched. Setting this property to false when SuggestedProductCategorySuggestionCollection has been fetched
		/// will clear the SuggestedProductCategorySuggestionCollection collection well. Setting this property to true while SuggestedProductCategorySuggestionCollection hasn't been fetched disables lazy loading for SuggestedProductCategorySuggestionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSuggestedProductCategorySuggestionCollection
		{
			get { return _alreadyFetchedSuggestedProductCategorySuggestionCollection;}
			set 
			{
				if(_alreadyFetchedSuggestedProductCategorySuggestionCollection && !value && (_suggestedProductCategorySuggestionCollection != null))
				{
					_suggestedProductCategorySuggestionCollection.Clear();
				}
				_alreadyFetchedSuggestedProductCategorySuggestionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductCategoryTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCategoryTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCategoryTagCollection ProductCategoryTagCollection
		{
			get	{ return GetMultiProductCategoryTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryTagCollection. When set to true, ProductCategoryTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiProductCategoryTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryTagCollection
		{
			get	{ return _alwaysFetchProductCategoryTagCollection; }
			set	{ _alwaysFetchProductCategoryTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryTagCollection already has been fetched. Setting this property to false when ProductCategoryTagCollection has been fetched
		/// will clear the ProductCategoryTagCollection collection well. Setting this property to true while ProductCategoryTagCollection hasn't been fetched disables lazy loading for ProductCategoryTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryTagCollection
		{
			get { return _alreadyFetchedProductCategoryTagCollection;}
			set 
			{
				if(_alreadyFetchedProductCategoryTagCollection && !value && (_productCategoryTagCollection != null))
				{
					_productCategoryTagCollection.Clear();
				}
				_alreadyFetchedProductCategoryTagCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection ScheduledMessageCollection
		{
			get	{ return GetMultiScheduledMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageCollection. When set to true, ScheduledMessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageCollection
		{
			get	{ return _alwaysFetchScheduledMessageCollection; }
			set	{ _alwaysFetchScheduledMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageCollection already has been fetched. Setting this property to false when ScheduledMessageCollection has been fetched
		/// will clear the ScheduledMessageCollection collection well. Setting this property to true while ScheduledMessageCollection hasn't been fetched disables lazy loading for ScheduledMessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageCollection
		{
			get { return _alreadyFetchedScheduledMessageCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageCollection && !value && (_scheduledMessageCollection != null))
				{
					_scheduledMessageCollection.Clear();
				}
				_alreadyFetchedScheduledMessageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductCategoryCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProductCategoryCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ProductCategoryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
