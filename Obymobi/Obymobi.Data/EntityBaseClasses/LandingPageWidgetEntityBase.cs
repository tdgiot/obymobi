﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'LandingPageWidget'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class LandingPageWidgetEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "LandingPageWidgetEntity"; }
		}
	
		#region Class Member Declarations
		private LandingPageEntity _landingPageEntity;
		private bool	_alwaysFetchLandingPageEntity, _alreadyFetchedLandingPageEntity, _landingPageEntityReturnsNewIfNotFound;
		private WidgetEntity _widgetEntity;
		private bool	_alwaysFetchWidgetEntity, _alreadyFetchedWidgetEntity, _widgetEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name LandingPageEntity</summary>
			public static readonly string LandingPageEntity = "LandingPageEntity";
			/// <summary>Member name WidgetEntity</summary>
			public static readonly string WidgetEntity = "WidgetEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static LandingPageWidgetEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected LandingPageWidgetEntityBase() :base("LandingPageWidgetEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		protected LandingPageWidgetEntityBase(System.Int32 landingPageId, System.Int32 widgetId):base("LandingPageWidgetEntity")
		{
			InitClassFetch(landingPageId, widgetId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected LandingPageWidgetEntityBase(System.Int32 landingPageId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse): base("LandingPageWidgetEntity")
		{
			InitClassFetch(landingPageId, widgetId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="validator">The custom validator object for this LandingPageWidgetEntity</param>
		protected LandingPageWidgetEntityBase(System.Int32 landingPageId, System.Int32 widgetId, IValidator validator):base("LandingPageWidgetEntity")
		{
			InitClassFetch(landingPageId, widgetId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LandingPageWidgetEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_landingPageEntity = (LandingPageEntity)info.GetValue("_landingPageEntity", typeof(LandingPageEntity));
			if(_landingPageEntity!=null)
			{
				_landingPageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_landingPageEntityReturnsNewIfNotFound = info.GetBoolean("_landingPageEntityReturnsNewIfNotFound");
			_alwaysFetchLandingPageEntity = info.GetBoolean("_alwaysFetchLandingPageEntity");
			_alreadyFetchedLandingPageEntity = info.GetBoolean("_alreadyFetchedLandingPageEntity");

			_widgetEntity = (WidgetEntity)info.GetValue("_widgetEntity", typeof(WidgetEntity));
			if(_widgetEntity!=null)
			{
				_widgetEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_widgetEntityReturnsNewIfNotFound = info.GetBoolean("_widgetEntityReturnsNewIfNotFound");
			_alwaysFetchWidgetEntity = info.GetBoolean("_alwaysFetchWidgetEntity");
			_alreadyFetchedWidgetEntity = info.GetBoolean("_alreadyFetchedWidgetEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((LandingPageWidgetFieldIndex)fieldIndex)
			{
				case LandingPageWidgetFieldIndex.LandingPageId:
					DesetupSyncLandingPageEntity(true, false);
					_alreadyFetchedLandingPageEntity = false;
					break;
				case LandingPageWidgetFieldIndex.WidgetId:
					DesetupSyncWidgetEntity(true, false);
					_alreadyFetchedWidgetEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLandingPageEntity = (_landingPageEntity != null);
			_alreadyFetchedWidgetEntity = (_widgetEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "LandingPageEntity":
					toReturn.Add(Relations.LandingPageEntityUsingLandingPageId);
					break;
				case "WidgetEntity":
					toReturn.Add(Relations.WidgetEntityUsingWidgetId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_landingPageEntity", (!this.MarkedForDeletion?_landingPageEntity:null));
			info.AddValue("_landingPageEntityReturnsNewIfNotFound", _landingPageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLandingPageEntity", _alwaysFetchLandingPageEntity);
			info.AddValue("_alreadyFetchedLandingPageEntity", _alreadyFetchedLandingPageEntity);
			info.AddValue("_widgetEntity", (!this.MarkedForDeletion?_widgetEntity:null));
			info.AddValue("_widgetEntityReturnsNewIfNotFound", _widgetEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWidgetEntity", _alwaysFetchWidgetEntity);
			info.AddValue("_alreadyFetchedWidgetEntity", _alreadyFetchedWidgetEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "LandingPageEntity":
					_alreadyFetchedLandingPageEntity = true;
					this.LandingPageEntity = (LandingPageEntity)entity;
					break;
				case "WidgetEntity":
					_alreadyFetchedWidgetEntity = true;
					this.WidgetEntity = (WidgetEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "LandingPageEntity":
					SetupSyncLandingPageEntity(relatedEntity);
					break;
				case "WidgetEntity":
					SetupSyncWidgetEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "LandingPageEntity":
					DesetupSyncLandingPageEntity(false, true);
					break;
				case "WidgetEntity":
					DesetupSyncWidgetEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_landingPageEntity!=null)
			{
				toReturn.Add(_landingPageEntity);
			}
			if(_widgetEntity!=null)
			{
				toReturn.Add(_widgetEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 landingPageId, System.Int32 widgetId)
		{
			return FetchUsingPK(landingPageId, widgetId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 landingPageId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(landingPageId, widgetId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 landingPageId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(landingPageId, widgetId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 landingPageId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(landingPageId, widgetId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LandingPageId, this.WidgetId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new LandingPageWidgetRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public LandingPageEntity GetSingleLandingPageEntity()
		{
			return GetSingleLandingPageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LandingPageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LandingPageEntity' which is related to this entity.</returns>
		public virtual LandingPageEntity GetSingleLandingPageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLandingPageEntity || forceFetch || _alwaysFetchLandingPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LandingPageEntityUsingLandingPageId);
				LandingPageEntity newEntity = new LandingPageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LandingPageId);
				}
				if(fetchResult)
				{
					newEntity = (LandingPageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_landingPageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LandingPageEntity = newEntity;
				_alreadyFetchedLandingPageEntity = fetchResult;
			}
			return _landingPageEntity;
		}


		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public WidgetEntity GetSingleWidgetEntity()
		{
			return GetSingleWidgetEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'WidgetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WidgetEntity' which is related to this entity.</returns>
		public virtual WidgetEntity GetSingleWidgetEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedWidgetEntity || forceFetch || _alwaysFetchWidgetEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WidgetEntityUsingWidgetId);
				WidgetEntity newEntity = (WidgetEntity)GeneralEntityFactory.Create(Obymobi.Data.EntityType.WidgetEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WidgetEntity.FetchPolymorphic(this.Transaction, this.WidgetId, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WidgetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_widgetEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WidgetEntity = newEntity;
				_alreadyFetchedWidgetEntity = fetchResult;
			}
			return _widgetEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("LandingPageEntity", _landingPageEntity);
			toReturn.Add("WidgetEntity", _widgetEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="validator">The validator object for this LandingPageWidgetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 landingPageId, System.Int32 widgetId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(landingPageId, widgetId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_landingPageEntityReturnsNewIfNotFound = true;
			_widgetEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LandingPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _landingPageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLandingPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticLandingPageWidgetRelations.LandingPageEntityUsingLandingPageIdStatic, true, signalRelatedEntity, "LandingPageWidgetCollection", resetFKFields, new int[] { (int)LandingPageWidgetFieldIndex.LandingPageId } );		
			_landingPageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _landingPageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLandingPageEntity(IEntityCore relatedEntity)
		{
			if(_landingPageEntity!=relatedEntity)
			{		
				DesetupSyncLandingPageEntity(true, true);
				_landingPageEntity = (LandingPageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _landingPageEntity, new PropertyChangedEventHandler( OnLandingPageEntityPropertyChanged ), "LandingPageEntity", Obymobi.Data.RelationClasses.StaticLandingPageWidgetRelations.LandingPageEntityUsingLandingPageIdStatic, true, ref _alreadyFetchedLandingPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLandingPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _widgetEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWidgetEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _widgetEntity, new PropertyChangedEventHandler( OnWidgetEntityPropertyChanged ), "WidgetEntity", Obymobi.Data.RelationClasses.StaticLandingPageWidgetRelations.WidgetEntityUsingWidgetIdStatic, true, signalRelatedEntity, "LandingPageWidgetCollection", resetFKFields, new int[] { (int)LandingPageWidgetFieldIndex.WidgetId } );		
			_widgetEntity = null;
		}
		
		/// <summary> setups the sync logic for member _widgetEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWidgetEntity(IEntityCore relatedEntity)
		{
			if(_widgetEntity!=relatedEntity)
			{		
				DesetupSyncWidgetEntity(true, true);
				_widgetEntity = (WidgetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _widgetEntity, new PropertyChangedEventHandler( OnWidgetEntityPropertyChanged ), "WidgetEntity", Obymobi.Data.RelationClasses.StaticLandingPageWidgetRelations.WidgetEntityUsingWidgetIdStatic, true, ref _alreadyFetchedWidgetEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWidgetEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 landingPageId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)LandingPageWidgetFieldIndex.LandingPageId].ForcedCurrentValueWrite(landingPageId);
				this.Fields[(int)LandingPageWidgetFieldIndex.WidgetId].ForcedCurrentValueWrite(widgetId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateLandingPageWidgetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new LandingPageWidgetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static LandingPageWidgetRelations Relations
		{
			get	{ return new LandingPageWidgetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LandingPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLandingPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LandingPageCollection(), (IEntityRelation)GetRelationsForField("LandingPageEntity")[0], (int)Obymobi.Data.EntityType.LandingPageWidgetEntity, (int)Obymobi.Data.EntityType.LandingPageEntity, 0, null, null, null, "LandingPageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Widget'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWidgetEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.WidgetCollection(), (IEntityRelation)GetRelationsForField("WidgetEntity")[0], (int)Obymobi.Data.EntityType.LandingPageWidgetEntity, (int)Obymobi.Data.EntityType.WidgetEntity, 0, null, null, null, "WidgetEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LandingPageId property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."LandingPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 LandingPageId
		{
			get { return (System.Int32)GetValue((int)LandingPageWidgetFieldIndex.LandingPageId, true); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.LandingPageId, value, true); }
		}

		/// <summary> The WidgetId property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."WidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 WidgetId
		{
			get { return (System.Int32)GetValue((int)LandingPageWidgetFieldIndex.WidgetId, true); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.WidgetId, value, true); }
		}

		/// <summary> The SortOrder property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)LandingPageWidgetFieldIndex.SortOrder, true); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)LandingPageWidgetFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)LandingPageWidgetFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)LandingPageWidgetFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)LandingPageWidgetFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity LandingPageWidget<br/><br/></summary>
		/// <remarks>Mapped on  table field: "LandingPageWidget"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)LandingPageWidgetFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)LandingPageWidgetFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'LandingPageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLandingPageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LandingPageEntity LandingPageEntity
		{
			get	{ return GetSingleLandingPageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLandingPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "LandingPageWidgetCollection", "LandingPageEntity", _landingPageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LandingPageEntity. When set to true, LandingPageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LandingPageEntity is accessed. You can always execute a forced fetch by calling GetSingleLandingPageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLandingPageEntity
		{
			get	{ return _alwaysFetchLandingPageEntity; }
			set	{ _alwaysFetchLandingPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LandingPageEntity already has been fetched. Setting this property to false when LandingPageEntity has been fetched
		/// will set LandingPageEntity to null as well. Setting this property to true while LandingPageEntity hasn't been fetched disables lazy loading for LandingPageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLandingPageEntity
		{
			get { return _alreadyFetchedLandingPageEntity;}
			set 
			{
				if(_alreadyFetchedLandingPageEntity && !value)
				{
					this.LandingPageEntity = null;
				}
				_alreadyFetchedLandingPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LandingPageEntity is not found
		/// in the database. When set to true, LandingPageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LandingPageEntityReturnsNewIfNotFound
		{
			get	{ return _landingPageEntityReturnsNewIfNotFound; }
			set { _landingPageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WidgetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWidgetEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual WidgetEntity WidgetEntity
		{
			get	{ return GetSingleWidgetEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWidgetEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "LandingPageWidgetCollection", "WidgetEntity", _widgetEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WidgetEntity. When set to true, WidgetEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WidgetEntity is accessed. You can always execute a forced fetch by calling GetSingleWidgetEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWidgetEntity
		{
			get	{ return _alwaysFetchWidgetEntity; }
			set	{ _alwaysFetchWidgetEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WidgetEntity already has been fetched. Setting this property to false when WidgetEntity has been fetched
		/// will set WidgetEntity to null as well. Setting this property to true while WidgetEntity hasn't been fetched disables lazy loading for WidgetEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWidgetEntity
		{
			get { return _alreadyFetchedWidgetEntity;}
			set 
			{
				if(_alreadyFetchedWidgetEntity && !value)
				{
					this.WidgetEntity = null;
				}
				_alreadyFetchedWidgetEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WidgetEntity is not found
		/// in the database. When set to true, WidgetEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool WidgetEntityReturnsNewIfNotFound
		{
			get	{ return _widgetEntityReturnsNewIfNotFound; }
			set { _widgetEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.LandingPageWidgetEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
