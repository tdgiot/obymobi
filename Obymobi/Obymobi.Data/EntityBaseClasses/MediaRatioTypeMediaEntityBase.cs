﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'MediaRatioTypeMedia'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MediaRatioTypeMediaEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MediaRatioTypeMediaEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection	_cloudProcessingTaskCollection;
		private bool	_alwaysFetchCloudProcessingTaskCollection, _alreadyFetchedCloudProcessingTaskCollection;
		private Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection	_mediaProcessingTaskCollection;
		private bool	_alwaysFetchMediaProcessingTaskCollection, _alreadyFetchedMediaProcessingTaskCollection;
		private MediaEntity _mediaEntity;
		private bool	_alwaysFetchMediaEntity, _alreadyFetchedMediaEntity, _mediaEntityReturnsNewIfNotFound;
		private MediaRatioTypeMediaFileEntity _mediaRatioTypeMediaFileEntity;
		private bool	_alwaysFetchMediaRatioTypeMediaFileEntity, _alreadyFetchedMediaRatioTypeMediaFileEntity, _mediaRatioTypeMediaFileEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name MediaEntity</summary>
			public static readonly string MediaEntity = "MediaEntity";
			/// <summary>Member name CloudProcessingTaskCollection</summary>
			public static readonly string CloudProcessingTaskCollection = "CloudProcessingTaskCollection";
			/// <summary>Member name MediaProcessingTaskCollection</summary>
			public static readonly string MediaProcessingTaskCollection = "MediaProcessingTaskCollection";
			/// <summary>Member name MediaRatioTypeMediaFileEntity</summary>
			public static readonly string MediaRatioTypeMediaFileEntity = "MediaRatioTypeMediaFileEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MediaRatioTypeMediaEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MediaRatioTypeMediaEntityBase() :base("MediaRatioTypeMediaEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		protected MediaRatioTypeMediaEntityBase(System.Int32 mediaRatioTypeMediaId):base("MediaRatioTypeMediaEntity")
		{
			InitClassFetch(mediaRatioTypeMediaId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MediaRatioTypeMediaEntityBase(System.Int32 mediaRatioTypeMediaId, IPrefetchPath prefetchPathToUse): base("MediaRatioTypeMediaEntity")
		{
			InitClassFetch(mediaRatioTypeMediaId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="validator">The custom validator object for this MediaRatioTypeMediaEntity</param>
		protected MediaRatioTypeMediaEntityBase(System.Int32 mediaRatioTypeMediaId, IValidator validator):base("MediaRatioTypeMediaEntity")
		{
			InitClassFetch(mediaRatioTypeMediaId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaRatioTypeMediaEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cloudProcessingTaskCollection = (Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection)info.GetValue("_cloudProcessingTaskCollection", typeof(Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection));
			_alwaysFetchCloudProcessingTaskCollection = info.GetBoolean("_alwaysFetchCloudProcessingTaskCollection");
			_alreadyFetchedCloudProcessingTaskCollection = info.GetBoolean("_alreadyFetchedCloudProcessingTaskCollection");

			_mediaProcessingTaskCollection = (Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection)info.GetValue("_mediaProcessingTaskCollection", typeof(Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection));
			_alwaysFetchMediaProcessingTaskCollection = info.GetBoolean("_alwaysFetchMediaProcessingTaskCollection");
			_alreadyFetchedMediaProcessingTaskCollection = info.GetBoolean("_alreadyFetchedMediaProcessingTaskCollection");
			_mediaEntity = (MediaEntity)info.GetValue("_mediaEntity", typeof(MediaEntity));
			if(_mediaEntity!=null)
			{
				_mediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaEntity = info.GetBoolean("_alwaysFetchMediaEntity");
			_alreadyFetchedMediaEntity = info.GetBoolean("_alreadyFetchedMediaEntity");
			_mediaRatioTypeMediaFileEntity = (MediaRatioTypeMediaFileEntity)info.GetValue("_mediaRatioTypeMediaFileEntity", typeof(MediaRatioTypeMediaFileEntity));
			if(_mediaRatioTypeMediaFileEntity!=null)
			{
				_mediaRatioTypeMediaFileEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaRatioTypeMediaFileEntityReturnsNewIfNotFound = info.GetBoolean("_mediaRatioTypeMediaFileEntityReturnsNewIfNotFound");
			_alwaysFetchMediaRatioTypeMediaFileEntity = info.GetBoolean("_alwaysFetchMediaRatioTypeMediaFileEntity");
			_alreadyFetchedMediaRatioTypeMediaFileEntity = info.GetBoolean("_alreadyFetchedMediaRatioTypeMediaFileEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MediaRatioTypeMediaFieldIndex)fieldIndex)
			{
				case MediaRatioTypeMediaFieldIndex.MediaId:
					DesetupSyncMediaEntity(true, false);
					_alreadyFetchedMediaEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCloudProcessingTaskCollection = (_cloudProcessingTaskCollection.Count > 0);
			_alreadyFetchedMediaProcessingTaskCollection = (_mediaProcessingTaskCollection.Count > 0);
			_alreadyFetchedMediaEntity = (_mediaEntity != null);
			_alreadyFetchedMediaRatioTypeMediaFileEntity = (_mediaRatioTypeMediaFileEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "MediaEntity":
					toReturn.Add(Relations.MediaEntityUsingMediaId);
					break;
				case "CloudProcessingTaskCollection":
					toReturn.Add(Relations.CloudProcessingTaskEntityUsingMediaRatioTypeMediaId);
					break;
				case "MediaProcessingTaskCollection":
					toReturn.Add(Relations.MediaProcessingTaskEntityUsingMediaRatioTypeMediaId);
					break;
				case "MediaRatioTypeMediaFileEntity":
					toReturn.Add(Relations.MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cloudProcessingTaskCollection", (!this.MarkedForDeletion?_cloudProcessingTaskCollection:null));
			info.AddValue("_alwaysFetchCloudProcessingTaskCollection", _alwaysFetchCloudProcessingTaskCollection);
			info.AddValue("_alreadyFetchedCloudProcessingTaskCollection", _alreadyFetchedCloudProcessingTaskCollection);
			info.AddValue("_mediaProcessingTaskCollection", (!this.MarkedForDeletion?_mediaProcessingTaskCollection:null));
			info.AddValue("_alwaysFetchMediaProcessingTaskCollection", _alwaysFetchMediaProcessingTaskCollection);
			info.AddValue("_alreadyFetchedMediaProcessingTaskCollection", _alreadyFetchedMediaProcessingTaskCollection);
			info.AddValue("_mediaEntity", (!this.MarkedForDeletion?_mediaEntity:null));
			info.AddValue("_mediaEntityReturnsNewIfNotFound", _mediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaEntity", _alwaysFetchMediaEntity);
			info.AddValue("_alreadyFetchedMediaEntity", _alreadyFetchedMediaEntity);

			info.AddValue("_mediaRatioTypeMediaFileEntity", (!this.MarkedForDeletion?_mediaRatioTypeMediaFileEntity:null));
			info.AddValue("_mediaRatioTypeMediaFileEntityReturnsNewIfNotFound", _mediaRatioTypeMediaFileEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaRatioTypeMediaFileEntity", _alwaysFetchMediaRatioTypeMediaFileEntity);
			info.AddValue("_alreadyFetchedMediaRatioTypeMediaFileEntity", _alreadyFetchedMediaRatioTypeMediaFileEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "MediaEntity":
					_alreadyFetchedMediaEntity = true;
					this.MediaEntity = (MediaEntity)entity;
					break;
				case "CloudProcessingTaskCollection":
					_alreadyFetchedCloudProcessingTaskCollection = true;
					if(entity!=null)
					{
						this.CloudProcessingTaskCollection.Add((CloudProcessingTaskEntity)entity);
					}
					break;
				case "MediaProcessingTaskCollection":
					_alreadyFetchedMediaProcessingTaskCollection = true;
					if(entity!=null)
					{
						this.MediaProcessingTaskCollection.Add((MediaProcessingTaskEntity)entity);
					}
					break;
				case "MediaRatioTypeMediaFileEntity":
					_alreadyFetchedMediaRatioTypeMediaFileEntity = true;
					this.MediaRatioTypeMediaFileEntity = (MediaRatioTypeMediaFileEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "MediaEntity":
					SetupSyncMediaEntity(relatedEntity);
					break;
				case "CloudProcessingTaskCollection":
					_cloudProcessingTaskCollection.Add((CloudProcessingTaskEntity)relatedEntity);
					break;
				case "MediaProcessingTaskCollection":
					_mediaProcessingTaskCollection.Add((MediaProcessingTaskEntity)relatedEntity);
					break;
				case "MediaRatioTypeMediaFileEntity":
					SetupSyncMediaRatioTypeMediaFileEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "MediaEntity":
					DesetupSyncMediaEntity(false, true);
					break;
				case "CloudProcessingTaskCollection":
					this.PerformRelatedEntityRemoval(_cloudProcessingTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaProcessingTaskCollection":
					this.PerformRelatedEntityRemoval(_mediaProcessingTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaRatioTypeMediaFileEntity":
					DesetupSyncMediaRatioTypeMediaFileEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_mediaRatioTypeMediaFileEntity!=null)
			{
				toReturn.Add(_mediaRatioTypeMediaFileEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_mediaEntity!=null)
			{
				toReturn.Add(_mediaEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cloudProcessingTaskCollection);
			toReturn.Add(_mediaProcessingTaskCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRatioTypeMediaId)
		{
			return FetchUsingPK(mediaRatioTypeMediaId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRatioTypeMediaId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(mediaRatioTypeMediaId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRatioTypeMediaId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(mediaRatioTypeMediaId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 mediaRatioTypeMediaId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(mediaRatioTypeMediaId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MediaRatioTypeMediaId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MediaRatioTypeMediaRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CloudProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, _cloudProcessingTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CloudProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, _cloudProcessingTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCloudProcessingTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection GetMultiCloudProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCloudProcessingTaskCollection || forceFetch || _alwaysFetchCloudProcessingTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cloudProcessingTaskCollection);
				_cloudProcessingTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_cloudProcessingTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_cloudProcessingTaskCollection.GetMultiManyToOne(null, null, this, null, null, filter);
				_cloudProcessingTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCloudProcessingTaskCollection = true;
			}
			return _cloudProcessingTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CloudProcessingTaskCollection'. These settings will be taken into account
		/// when the property CloudProcessingTaskCollection is requested or GetMultiCloudProcessingTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCloudProcessingTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cloudProcessingTaskCollection.SortClauses=sortClauses;
			_cloudProcessingTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection GetMultiMediaProcessingTaskCollection(bool forceFetch)
		{
			return GetMultiMediaProcessingTaskCollection(forceFetch, _mediaProcessingTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaProcessingTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection GetMultiMediaProcessingTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaProcessingTaskCollection(forceFetch, _mediaProcessingTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection GetMultiMediaProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaProcessingTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaProcessingTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection GetMultiMediaProcessingTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaProcessingTaskCollection || forceFetch || _alwaysFetchMediaProcessingTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaProcessingTaskCollection);
				_mediaProcessingTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaProcessingTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaProcessingTaskCollection.GetMultiManyToOne(this, filter);
				_mediaProcessingTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaProcessingTaskCollection = true;
			}
			return _mediaProcessingTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaProcessingTaskCollection'. These settings will be taken into account
		/// when the property MediaProcessingTaskCollection is requested or GetMultiMediaProcessingTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaProcessingTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaProcessingTaskCollection.SortClauses=sortClauses;
			_mediaProcessingTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public MediaEntity GetSingleMediaEntity()
		{
			return GetSingleMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public virtual MediaEntity GetSingleMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaEntity || forceFetch || _alwaysFetchMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaEntityUsingMediaId);
				MediaEntity newEntity = new MediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MediaId);
				}
				if(fetchResult)
				{
					newEntity = (MediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaEntity = newEntity;
				_alreadyFetchedMediaEntity = fetchResult;
			}
			return _mediaEntity;
		}

		/// <summary> Retrieves the related entity of type 'MediaRatioTypeMediaFileEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'MediaRatioTypeMediaFileEntity' which is related to this entity.</returns>
		public MediaRatioTypeMediaFileEntity GetSingleMediaRatioTypeMediaFileEntity()
		{
			return GetSingleMediaRatioTypeMediaFileEntity(false);
		}
		
		/// <summary> Retrieves the related entity of type 'MediaRatioTypeMediaFileEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaRatioTypeMediaFileEntity' which is related to this entity.</returns>
		public virtual MediaRatioTypeMediaFileEntity GetSingleMediaRatioTypeMediaFileEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaRatioTypeMediaFileEntity || forceFetch || _alwaysFetchMediaRatioTypeMediaFileEntity) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaId);
				MediaRatioTypeMediaFileEntity newEntity = new MediaRatioTypeMediaFileEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCMediaRatioTypeMediaId(this.MediaRatioTypeMediaId);
				}
				if(fetchResult)
				{
					newEntity = (MediaRatioTypeMediaFileEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaRatioTypeMediaFileEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaRatioTypeMediaFileEntity = newEntity;
				_alreadyFetchedMediaRatioTypeMediaFileEntity = fetchResult;
			}
			return _mediaRatioTypeMediaFileEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("MediaEntity", _mediaEntity);
			toReturn.Add("CloudProcessingTaskCollection", _cloudProcessingTaskCollection);
			toReturn.Add("MediaProcessingTaskCollection", _mediaProcessingTaskCollection);
			toReturn.Add("MediaRatioTypeMediaFileEntity", _mediaRatioTypeMediaFileEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="validator">The validator object for this MediaRatioTypeMediaEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 mediaRatioTypeMediaId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(mediaRatioTypeMediaId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cloudProcessingTaskCollection = new Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection();
			_cloudProcessingTaskCollection.SetContainingEntityInfo(this, "MediaRatioTypeMediaEntity");

			_mediaProcessingTaskCollection = new Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection();
			_mediaProcessingTaskCollection.SetContainingEntityInfo(this, "MediaRatioTypeMediaEntity");
			_mediaEntityReturnsNewIfNotFound = true;
			_mediaRatioTypeMediaFileEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaRatioTypeMediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Top", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Left", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Width", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Height", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastDistributedVersionTicks", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastDistributedVersionTicksAmazon", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ManuallyVerified", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _mediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMediaRatioTypeMediaRelations.MediaEntityUsingMediaIdStatic, true, signalRelatedEntity, "MediaRatioTypeMediaCollection", resetFKFields, new int[] { (int)MediaRatioTypeMediaFieldIndex.MediaId } );		
			_mediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaEntity(true, true);
				_mediaEntity = (MediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMediaRatioTypeMediaRelations.MediaEntityUsingMediaIdStatic, true, ref _alreadyFetchedMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mediaRatioTypeMediaFileEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaRatioTypeMediaFileEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaRatioTypeMediaFileEntity, new PropertyChangedEventHandler( OnMediaRatioTypeMediaFileEntityPropertyChanged ), "MediaRatioTypeMediaFileEntity", Obymobi.Data.RelationClasses.StaticMediaRatioTypeMediaRelations.MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaIdStatic, false, signalRelatedEntity, "MediaRatioTypeMediaEntity", false, new int[] { (int)MediaRatioTypeMediaFieldIndex.MediaRatioTypeMediaId } );
			_mediaRatioTypeMediaFileEntity = null;
		}
	
		/// <summary> setups the sync logic for member _mediaRatioTypeMediaFileEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaRatioTypeMediaFileEntity(IEntityCore relatedEntity)
		{
			if(_mediaRatioTypeMediaFileEntity!=relatedEntity)
			{
				DesetupSyncMediaRatioTypeMediaFileEntity(true, true);
				_mediaRatioTypeMediaFileEntity = (MediaRatioTypeMediaFileEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaRatioTypeMediaFileEntity, new PropertyChangedEventHandler( OnMediaRatioTypeMediaFileEntityPropertyChanged ), "MediaRatioTypeMediaFileEntity", Obymobi.Data.RelationClasses.StaticMediaRatioTypeMediaRelations.MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaIdStatic, false, ref _alreadyFetchedMediaRatioTypeMediaFileEntity, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaRatioTypeMediaFileEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 mediaRatioTypeMediaId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MediaRatioTypeMediaFieldIndex.MediaRatioTypeMediaId].ForcedCurrentValueWrite(mediaRatioTypeMediaId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMediaRatioTypeMediaDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MediaRatioTypeMediaEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MediaRatioTypeMediaRelations Relations
		{
			get	{ return new MediaRatioTypeMediaRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CloudProcessingTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCloudProcessingTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection(), (IEntityRelation)GetRelationsForField("CloudProcessingTaskCollection")[0], (int)Obymobi.Data.EntityType.MediaRatioTypeMediaEntity, (int)Obymobi.Data.EntityType.CloudProcessingTaskEntity, 0, null, null, null, "CloudProcessingTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaProcessingTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaProcessingTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection(), (IEntityRelation)GetRelationsForField("MediaProcessingTaskCollection")[0], (int)Obymobi.Data.EntityType.MediaRatioTypeMediaEntity, (int)Obymobi.Data.EntityType.MediaProcessingTaskEntity, 0, null, null, null, "MediaProcessingTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaEntity")[0], (int)Obymobi.Data.EntityType.MediaRatioTypeMediaEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MediaRatioTypeMediaFile'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaRatioTypeMediaFileEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaRatioTypeMediaFileCollection(), (IEntityRelation)GetRelationsForField("MediaRatioTypeMediaFileEntity")[0], (int)Obymobi.Data.EntityType.MediaRatioTypeMediaEntity, (int)Obymobi.Data.EntityType.MediaRatioTypeMediaFileEntity, 0, null, null, null, "MediaRatioTypeMediaFileEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MediaRatioTypeMediaId property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."MediaRatioTypeMediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MediaRatioTypeMediaId
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.MediaRatioTypeMediaId, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.MediaRatioTypeMediaId, value, true); }
		}

		/// <summary> The MediaId property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MediaId
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.MediaId, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.MediaId, value, true); }
		}

		/// <summary> The Top property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Top"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Top
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Top, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Top, value, true); }
		}

		/// <summary> The Left property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Left"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Left
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Left, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Left, value, true); }
		}

		/// <summary> The Width property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Width"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Width
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Width, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Width, value, true); }
		}

		/// <summary> The Height property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."Height"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Height
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.Height, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.Height, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The MediaType property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."MediaType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaType
		{
			get { return (Nullable<System.Int32>)GetValue((int)MediaRatioTypeMediaFieldIndex.MediaType, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.MediaType, value, true); }
		}

		/// <summary> The LastDistributedVersionTicks property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."LastDistributedVersionTicks"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicks
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicks, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicks, value, true); }
		}

		/// <summary> The LastDistributedVersionTicksAmazon property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."LastDistributedVersionTicksAmazon"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastDistributedVersionTicksAmazon
		{
			get { return (Nullable<System.Int64>)GetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAmazon, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.LastDistributedVersionTicksAmazon, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaRatioTypeMediaFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ManuallyVerified property of the Entity MediaRatioTypeMedia<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MediaRatioTypeMedia"."ManuallyVerified"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ManuallyVerified
		{
			get { return (System.Boolean)GetValue((int)MediaRatioTypeMediaFieldIndex.ManuallyVerified, true); }
			set	{ SetValue((int)MediaRatioTypeMediaFieldIndex.ManuallyVerified, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CloudProcessingTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCloudProcessingTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CloudProcessingTaskCollection CloudProcessingTaskCollection
		{
			get	{ return GetMultiCloudProcessingTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CloudProcessingTaskCollection. When set to true, CloudProcessingTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CloudProcessingTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCloudProcessingTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCloudProcessingTaskCollection
		{
			get	{ return _alwaysFetchCloudProcessingTaskCollection; }
			set	{ _alwaysFetchCloudProcessingTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CloudProcessingTaskCollection already has been fetched. Setting this property to false when CloudProcessingTaskCollection has been fetched
		/// will clear the CloudProcessingTaskCollection collection well. Setting this property to true while CloudProcessingTaskCollection hasn't been fetched disables lazy loading for CloudProcessingTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCloudProcessingTaskCollection
		{
			get { return _alreadyFetchedCloudProcessingTaskCollection;}
			set 
			{
				if(_alreadyFetchedCloudProcessingTaskCollection && !value && (_cloudProcessingTaskCollection != null))
				{
					_cloudProcessingTaskCollection.Clear();
				}
				_alreadyFetchedCloudProcessingTaskCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaProcessingTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaProcessingTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaProcessingTaskCollection MediaProcessingTaskCollection
		{
			get	{ return GetMultiMediaProcessingTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaProcessingTaskCollection. When set to true, MediaProcessingTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaProcessingTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaProcessingTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaProcessingTaskCollection
		{
			get	{ return _alwaysFetchMediaProcessingTaskCollection; }
			set	{ _alwaysFetchMediaProcessingTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaProcessingTaskCollection already has been fetched. Setting this property to false when MediaProcessingTaskCollection has been fetched
		/// will clear the MediaProcessingTaskCollection collection well. Setting this property to true while MediaProcessingTaskCollection hasn't been fetched disables lazy loading for MediaProcessingTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaProcessingTaskCollection
		{
			get { return _alreadyFetchedMediaProcessingTaskCollection;}
			set 
			{
				if(_alreadyFetchedMediaProcessingTaskCollection && !value && (_mediaProcessingTaskCollection != null))
				{
					_mediaProcessingTaskCollection.Clear();
				}
				_alreadyFetchedMediaProcessingTaskCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'MediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaEntity MediaEntity
		{
			get	{ return GetSingleMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MediaRatioTypeMediaCollection", "MediaEntity", _mediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaEntity. When set to true, MediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaEntity
		{
			get	{ return _alwaysFetchMediaEntity; }
			set	{ _alwaysFetchMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaEntity already has been fetched. Setting this property to false when MediaEntity has been fetched
		/// will set MediaEntity to null as well. Setting this property to true while MediaEntity hasn't been fetched disables lazy loading for MediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaEntity
		{
			get { return _alreadyFetchedMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaEntity && !value)
				{
					this.MediaEntity = null;
				}
				_alreadyFetchedMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaEntity is not found
		/// in the database. When set to true, MediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaEntityReturnsNewIfNotFound; }
			set { _mediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MediaRatioTypeMediaFileEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaRatioTypeMediaFileEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaRatioTypeMediaFileEntity MediaRatioTypeMediaFileEntity
		{
			get	{ return GetSingleMediaRatioTypeMediaFileEntity(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncMediaRatioTypeMediaFileEntity(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_mediaRatioTypeMediaFileEntity !=null);
						DesetupSyncMediaRatioTypeMediaFileEntity(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("MediaRatioTypeMediaFileEntity");
						}
					}
					else
					{
						if(_mediaRatioTypeMediaFileEntity!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "MediaRatioTypeMediaEntity");
							SetupSyncMediaRatioTypeMediaFileEntity(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaRatioTypeMediaFileEntity. When set to true, MediaRatioTypeMediaFileEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaRatioTypeMediaFileEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaRatioTypeMediaFileEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaRatioTypeMediaFileEntity
		{
			get	{ return _alwaysFetchMediaRatioTypeMediaFileEntity; }
			set	{ _alwaysFetchMediaRatioTypeMediaFileEntity = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property MediaRatioTypeMediaFileEntity already has been fetched. Setting this property to false when MediaRatioTypeMediaFileEntity has been fetched
		/// will set MediaRatioTypeMediaFileEntity to null as well. Setting this property to true while MediaRatioTypeMediaFileEntity hasn't been fetched disables lazy loading for MediaRatioTypeMediaFileEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaRatioTypeMediaFileEntity
		{
			get { return _alreadyFetchedMediaRatioTypeMediaFileEntity;}
			set 
			{
				if(_alreadyFetchedMediaRatioTypeMediaFileEntity && !value)
				{
					this.MediaRatioTypeMediaFileEntity = null;
				}
				_alreadyFetchedMediaRatioTypeMediaFileEntity = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaRatioTypeMediaFileEntity is not found
		/// in the database. When set to true, MediaRatioTypeMediaFileEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaRatioTypeMediaFileEntityReturnsNewIfNotFound
		{
			get	{ return _mediaRatioTypeMediaFileEntityReturnsNewIfNotFound; }
			set	{ _mediaRatioTypeMediaFileEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MediaRatioTypeMediaEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
