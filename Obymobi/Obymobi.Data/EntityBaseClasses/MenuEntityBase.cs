﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Menu'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MenuEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MenuEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CategoryCollection	_categoryCollection;
		private bool	_alwaysFetchCategoryCollection, _alreadyFetchedCategoryCollection;
		private Obymobi.Data.CollectionClasses.ClientConfigurationCollection	_clientConfigurationCollection;
		private bool	_alwaysFetchClientConfigurationCollection, _alreadyFetchedClientConfigurationCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.AnnouncementCollection _announcementCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchAnnouncementCollectionViaDeliverypointgroup, _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaCategory;
		private bool	_alwaysFetchCategoryCollectionViaCategory, _alreadyFetchedCategoryCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaCategory;
		private bool	_alwaysFetchCompanyCollectionViaCategory, _alreadyFetchedCompanyCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchCompanyCollectionViaDeliverypointgroup, _alreadyFetchedCompanyCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.GenericcategoryCollection _genericcategoryCollectionViaCategory;
		private bool	_alwaysFetchGenericcategoryCollectionViaCategory, _alreadyFetchedGenericcategoryCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.PoscategoryCollection _poscategoryCollectionViaCategory;
		private bool	_alwaysFetchPoscategoryCollectionViaCategory, _alreadyFetchedPoscategoryCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection _posdeliverypointgroupCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup, _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaCategory;
		private bool	_alwaysFetchProductCollectionViaCategory, _alreadyFetchedProductCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCategory;
		private bool	_alwaysFetchRouteCollectionViaCategory, _alreadyFetchedRouteCollectionViaCategory;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup, _alreadyFetchedRouteCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchRouteCollectionViaDeliverypointgroup_, _alreadyFetchedRouteCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchTerminalCollectionViaDeliverypointgroup, _alreadyFetchedTerminalCollectionViaDeliverypointgroup;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup_;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup_, _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup__;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup__, _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaDeliverypointgroup;
		private bool	_alwaysFetchUIModeCollectionViaDeliverypointgroup, _alreadyFetchedUIModeCollectionViaDeliverypointgroup;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private TimestampEntity _timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection, _timestampCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name CategoryCollection</summary>
			public static readonly string CategoryCollection = "CategoryCollection";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name AnnouncementCollectionViaDeliverypointgroup</summary>
			public static readonly string AnnouncementCollectionViaDeliverypointgroup = "AnnouncementCollectionViaDeliverypointgroup";
			/// <summary>Member name CategoryCollectionViaCategory</summary>
			public static readonly string CategoryCollectionViaCategory = "CategoryCollectionViaCategory";
			/// <summary>Member name CompanyCollectionViaCategory</summary>
			public static readonly string CompanyCollectionViaCategory = "CompanyCollectionViaCategory";
			/// <summary>Member name CompanyCollectionViaDeliverypointgroup</summary>
			public static readonly string CompanyCollectionViaDeliverypointgroup = "CompanyCollectionViaDeliverypointgroup";
			/// <summary>Member name GenericcategoryCollectionViaCategory</summary>
			public static readonly string GenericcategoryCollectionViaCategory = "GenericcategoryCollectionViaCategory";
			/// <summary>Member name PoscategoryCollectionViaCategory</summary>
			public static readonly string PoscategoryCollectionViaCategory = "PoscategoryCollectionViaCategory";
			/// <summary>Member name PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
			public static readonly string PosdeliverypointgroupCollectionViaDeliverypointgroup = "PosdeliverypointgroupCollectionViaDeliverypointgroup";
			/// <summary>Member name ProductCollectionViaCategory</summary>
			public static readonly string ProductCollectionViaCategory = "ProductCollectionViaCategory";
			/// <summary>Member name RouteCollectionViaCategory</summary>
			public static readonly string RouteCollectionViaCategory = "RouteCollectionViaCategory";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup = "RouteCollectionViaDeliverypointgroup";
			/// <summary>Member name RouteCollectionViaDeliverypointgroup_</summary>
			public static readonly string RouteCollectionViaDeliverypointgroup_ = "RouteCollectionViaDeliverypointgroup_";
			/// <summary>Member name TerminalCollectionViaDeliverypointgroup</summary>
			public static readonly string TerminalCollectionViaDeliverypointgroup = "TerminalCollectionViaDeliverypointgroup";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup_</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup_ = "UIModeCollectionViaDeliverypointgroup_";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup__</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup__ = "UIModeCollectionViaDeliverypointgroup__";
			/// <summary>Member name UIModeCollectionViaDeliverypointgroup</summary>
			public static readonly string UIModeCollectionViaDeliverypointgroup = "UIModeCollectionViaDeliverypointgroup";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MenuEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MenuEntityBase() :base("MenuEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		protected MenuEntityBase(System.Int32 menuId):base("MenuEntity")
		{
			InitClassFetch(menuId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MenuEntityBase(System.Int32 menuId, IPrefetchPath prefetchPathToUse): base("MenuEntity")
		{
			InitClassFetch(menuId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="validator">The custom validator object for this MenuEntity</param>
		protected MenuEntityBase(System.Int32 menuId, IValidator validator):base("MenuEntity")
		{
			InitClassFetch(menuId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MenuEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_categoryCollection = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollection", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollection = info.GetBoolean("_alwaysFetchCategoryCollection");
			_alreadyFetchedCategoryCollection = info.GetBoolean("_alreadyFetchedCategoryCollection");

			_clientConfigurationCollection = (Obymobi.Data.CollectionClasses.ClientConfigurationCollection)info.GetValue("_clientConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.ClientConfigurationCollection));
			_alwaysFetchClientConfigurationCollection = info.GetBoolean("_alwaysFetchClientConfigurationCollection");
			_alreadyFetchedClientConfigurationCollection = info.GetBoolean("_alreadyFetchedClientConfigurationCollection");

			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");
			_announcementCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.AnnouncementCollection)info.GetValue("_announcementCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.AnnouncementCollection));
			_alwaysFetchAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup");
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup");

			_categoryCollectionViaCategory = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaCategory = info.GetBoolean("_alwaysFetchCategoryCollectionViaCategory");
			_alreadyFetchedCategoryCollectionViaCategory = info.GetBoolean("_alreadyFetchedCategoryCollectionViaCategory");

			_companyCollectionViaCategory = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaCategory = info.GetBoolean("_alwaysFetchCompanyCollectionViaCategory");
			_alreadyFetchedCompanyCollectionViaCategory = info.GetBoolean("_alreadyFetchedCompanyCollectionViaCategory");

			_companyCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchCompanyCollectionViaDeliverypointgroup");
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedCompanyCollectionViaDeliverypointgroup");

			_genericcategoryCollectionViaCategory = (Obymobi.Data.CollectionClasses.GenericcategoryCollection)info.GetValue("_genericcategoryCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.GenericcategoryCollection));
			_alwaysFetchGenericcategoryCollectionViaCategory = info.GetBoolean("_alwaysFetchGenericcategoryCollectionViaCategory");
			_alreadyFetchedGenericcategoryCollectionViaCategory = info.GetBoolean("_alreadyFetchedGenericcategoryCollectionViaCategory");

			_poscategoryCollectionViaCategory = (Obymobi.Data.CollectionClasses.PoscategoryCollection)info.GetValue("_poscategoryCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.PoscategoryCollection));
			_alwaysFetchPoscategoryCollectionViaCategory = info.GetBoolean("_alwaysFetchPoscategoryCollectionViaCategory");
			_alreadyFetchedPoscategoryCollectionViaCategory = info.GetBoolean("_alreadyFetchedPoscategoryCollectionViaCategory");

			_posdeliverypointgroupCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection)info.GetValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection));
			_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup");
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup");

			_productCollectionViaCategory = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaCategory = info.GetBoolean("_alwaysFetchProductCollectionViaCategory");
			_alreadyFetchedProductCollectionViaCategory = info.GetBoolean("_alreadyFetchedProductCollectionViaCategory");

			_routeCollectionViaCategory = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCategory", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCategory = info.GetBoolean("_alwaysFetchRouteCollectionViaCategory");
			_alreadyFetchedRouteCollectionViaCategory = info.GetBoolean("_alreadyFetchedRouteCollectionViaCategory");

			_routeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup");

			_routeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchRouteCollectionViaDeliverypointgroup_");
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedRouteCollectionViaDeliverypointgroup_");

			_terminalCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchTerminalCollectionViaDeliverypointgroup");
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedTerminalCollectionViaDeliverypointgroup");

			_uIModeCollectionViaDeliverypointgroup_ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup_", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup_");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_");

			_uIModeCollectionViaDeliverypointgroup__ = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup__", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup__");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__");

			_uIModeCollectionViaDeliverypointgroup = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaDeliverypointgroup", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alwaysFetchUIModeCollectionViaDeliverypointgroup");
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = info.GetBoolean("_alreadyFetchedUIModeCollectionViaDeliverypointgroup");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			_timestampCollection = (TimestampEntity)info.GetValue("_timestampCollection", typeof(TimestampEntity));
			if(_timestampCollection!=null)
			{
				_timestampCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_timestampCollectionReturnsNewIfNotFound = info.GetBoolean("_timestampCollectionReturnsNewIfNotFound");
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MenuFieldIndex)fieldIndex)
			{
				case MenuFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCategoryCollection = (_categoryCollection.Count > 0);
			_alreadyFetchedClientConfigurationCollection = (_clientConfigurationCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = (_announcementCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedCategoryCollectionViaCategory = (_categoryCollectionViaCategory.Count > 0);
			_alreadyFetchedCompanyCollectionViaCategory = (_companyCollectionViaCategory.Count > 0);
			_alreadyFetchedCompanyCollectionViaDeliverypointgroup = (_companyCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedGenericcategoryCollectionViaCategory = (_genericcategoryCollectionViaCategory.Count > 0);
			_alreadyFetchedPoscategoryCollectionViaCategory = (_poscategoryCollectionViaCategory.Count > 0);
			_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = (_posdeliverypointgroupCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedProductCollectionViaCategory = (_productCollectionViaCategory.Count > 0);
			_alreadyFetchedRouteCollectionViaCategory = (_routeCollectionViaCategory.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup = (_routeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = (_routeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedTerminalCollectionViaDeliverypointgroup = (_terminalCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = (_uIModeCollectionViaDeliverypointgroup_.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = (_uIModeCollectionViaDeliverypointgroup__.Count > 0);
			_alreadyFetchedUIModeCollectionViaDeliverypointgroup = (_uIModeCollectionViaDeliverypointgroup.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedTimestampCollection = (_timestampCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "CategoryCollection":
					toReturn.Add(Relations.CategoryEntityUsingMenuId);
					break;
				case "ClientConfigurationCollection":
					toReturn.Add(Relations.ClientConfigurationEntityUsingMenuId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId);
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.AnnouncementEntityUsingReorderNotificationAnnouncementId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "CategoryCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingMenuId, "MenuEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.CategoryEntityUsingParentCategoryId, "Category_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingMenuId, "MenuEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.CompanyEntityUsingCompanyId, "Category_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.CompanyEntityUsingCompanyId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "GenericcategoryCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingMenuId, "MenuEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.GenericcategoryEntityUsingGenericcategoryId, "Category_", string.Empty, JoinHint.None);
					break;
				case "PoscategoryCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingMenuId, "MenuEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.PoscategoryEntityUsingPoscategoryId, "Category_", string.Empty, JoinHint.None);
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingMenuId, "MenuEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.ProductEntityUsingProductId, "Category_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCategory":
					toReturn.Add(Relations.CategoryEntityUsingMenuId, "MenuEntity__", "Category_", JoinHint.None);
					toReturn.Add(CategoryEntity.Relations.RouteEntityUsingRouteId, "Category_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingSystemMessageRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.RouteEntityUsingRouteId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.TerminalEntityUsingXTerminalId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingMobileUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingTabletUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingMenuId, "MenuEntity__", "Deliverypointgroup_", JoinHint.None);
					toReturn.Add(DeliverypointgroupEntity.Relations.UIModeEntityUsingUIModeId, "Deliverypointgroup_", string.Empty, JoinHint.None);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingMenuId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_categoryCollection", (!this.MarkedForDeletion?_categoryCollection:null));
			info.AddValue("_alwaysFetchCategoryCollection", _alwaysFetchCategoryCollection);
			info.AddValue("_alreadyFetchedCategoryCollection", _alreadyFetchedCategoryCollection);
			info.AddValue("_clientConfigurationCollection", (!this.MarkedForDeletion?_clientConfigurationCollection:null));
			info.AddValue("_alwaysFetchClientConfigurationCollection", _alwaysFetchClientConfigurationCollection);
			info.AddValue("_alreadyFetchedClientConfigurationCollection", _alreadyFetchedClientConfigurationCollection);
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_announcementCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_announcementCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchAnnouncementCollectionViaDeliverypointgroup", _alwaysFetchAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup", _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup);
			info.AddValue("_categoryCollectionViaCategory", (!this.MarkedForDeletion?_categoryCollectionViaCategory:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaCategory", _alwaysFetchCategoryCollectionViaCategory);
			info.AddValue("_alreadyFetchedCategoryCollectionViaCategory", _alreadyFetchedCategoryCollectionViaCategory);
			info.AddValue("_companyCollectionViaCategory", (!this.MarkedForDeletion?_companyCollectionViaCategory:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaCategory", _alwaysFetchCompanyCollectionViaCategory);
			info.AddValue("_alreadyFetchedCompanyCollectionViaCategory", _alreadyFetchedCompanyCollectionViaCategory);
			info.AddValue("_companyCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_companyCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaDeliverypointgroup", _alwaysFetchCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedCompanyCollectionViaDeliverypointgroup", _alreadyFetchedCompanyCollectionViaDeliverypointgroup);
			info.AddValue("_genericcategoryCollectionViaCategory", (!this.MarkedForDeletion?_genericcategoryCollectionViaCategory:null));
			info.AddValue("_alwaysFetchGenericcategoryCollectionViaCategory", _alwaysFetchGenericcategoryCollectionViaCategory);
			info.AddValue("_alreadyFetchedGenericcategoryCollectionViaCategory", _alreadyFetchedGenericcategoryCollectionViaCategory);
			info.AddValue("_poscategoryCollectionViaCategory", (!this.MarkedForDeletion?_poscategoryCollectionViaCategory:null));
			info.AddValue("_alwaysFetchPoscategoryCollectionViaCategory", _alwaysFetchPoscategoryCollectionViaCategory);
			info.AddValue("_alreadyFetchedPoscategoryCollectionViaCategory", _alreadyFetchedPoscategoryCollectionViaCategory);
			info.AddValue("_posdeliverypointgroupCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_posdeliverypointgroupCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup", _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup", _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup);
			info.AddValue("_productCollectionViaCategory", (!this.MarkedForDeletion?_productCollectionViaCategory:null));
			info.AddValue("_alwaysFetchProductCollectionViaCategory", _alwaysFetchProductCollectionViaCategory);
			info.AddValue("_alreadyFetchedProductCollectionViaCategory", _alreadyFetchedProductCollectionViaCategory);
			info.AddValue("_routeCollectionViaCategory", (!this.MarkedForDeletion?_routeCollectionViaCategory:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCategory", _alwaysFetchRouteCollectionViaCategory);
			info.AddValue("_alreadyFetchedRouteCollectionViaCategory", _alreadyFetchedRouteCollectionViaCategory);
			info.AddValue("_routeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup", _alwaysFetchRouteCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup", _alreadyFetchedRouteCollectionViaDeliverypointgroup);
			info.AddValue("_routeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_routeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchRouteCollectionViaDeliverypointgroup_", _alwaysFetchRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedRouteCollectionViaDeliverypointgroup_", _alreadyFetchedRouteCollectionViaDeliverypointgroup_);
			info.AddValue("_terminalCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_terminalCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaDeliverypointgroup", _alwaysFetchTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedTerminalCollectionViaDeliverypointgroup", _alreadyFetchedTerminalCollectionViaDeliverypointgroup);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup_", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup_:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup_", _alwaysFetchUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup_", _alreadyFetchedUIModeCollectionViaDeliverypointgroup_);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup__", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup__:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup__", _alwaysFetchUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup__", _alreadyFetchedUIModeCollectionViaDeliverypointgroup__);
			info.AddValue("_uIModeCollectionViaDeliverypointgroup", (!this.MarkedForDeletion?_uIModeCollectionViaDeliverypointgroup:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaDeliverypointgroup", _alwaysFetchUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_alreadyFetchedUIModeCollectionViaDeliverypointgroup", _alreadyFetchedUIModeCollectionViaDeliverypointgroup);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_timestampCollectionReturnsNewIfNotFound", _timestampCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "CategoryCollection":
					_alreadyFetchedCategoryCollection = true;
					if(entity!=null)
					{
						this.CategoryCollection.Add((CategoryEntity)entity);
					}
					break;
				case "ClientConfigurationCollection":
					_alreadyFetchedClientConfigurationCollection = true;
					if(entity!=null)
					{
						this.ClientConfigurationCollection.Add((ClientConfigurationEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "AnnouncementCollectionViaDeliverypointgroup":
					_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.AnnouncementCollectionViaDeliverypointgroup.Add((AnnouncementEntity)entity);
					}
					break;
				case "CategoryCollectionViaCategory":
					_alreadyFetchedCategoryCollectionViaCategory = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaCategory.Add((CategoryEntity)entity);
					}
					break;
				case "CompanyCollectionViaCategory":
					_alreadyFetchedCompanyCollectionViaCategory = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaCategory.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaDeliverypointgroup":
					_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaDeliverypointgroup.Add((CompanyEntity)entity);
					}
					break;
				case "GenericcategoryCollectionViaCategory":
					_alreadyFetchedGenericcategoryCollectionViaCategory = true;
					if(entity!=null)
					{
						this.GenericcategoryCollectionViaCategory.Add((GenericcategoryEntity)entity);
					}
					break;
				case "PoscategoryCollectionViaCategory":
					_alreadyFetchedPoscategoryCollectionViaCategory = true;
					if(entity!=null)
					{
						this.PoscategoryCollectionViaCategory.Add((PoscategoryEntity)entity);
					}
					break;
				case "PosdeliverypointgroupCollectionViaDeliverypointgroup":
					_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.PosdeliverypointgroupCollectionViaDeliverypointgroup.Add((PosdeliverypointgroupEntity)entity);
					}
					break;
				case "ProductCollectionViaCategory":
					_alreadyFetchedProductCollectionViaCategory = true;
					if(entity!=null)
					{
						this.ProductCollectionViaCategory.Add((ProductEntity)entity);
					}
					break;
				case "RouteCollectionViaCategory":
					_alreadyFetchedRouteCollectionViaCategory = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCategory.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup.Add((RouteEntity)entity);
					}
					break;
				case "RouteCollectionViaDeliverypointgroup_":
					_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.RouteCollectionViaDeliverypointgroup_.Add((RouteEntity)entity);
					}
					break;
				case "TerminalCollectionViaDeliverypointgroup":
					_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaDeliverypointgroup.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup_":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup_.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup__":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup__.Add((UIModeEntity)entity);
					}
					break;
				case "UIModeCollectionViaDeliverypointgroup":
					_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaDeliverypointgroup.Add((UIModeEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					this.TimestampCollection = (TimestampEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "CategoryCollection":
					_categoryCollection.Add((CategoryEntity)relatedEntity);
					break;
				case "ClientConfigurationCollection":
					_clientConfigurationCollection.Add((ClientConfigurationEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "TimestampCollection":
					SetupSyncTimestampCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "CategoryCollection":
					this.PerformRelatedEntityRemoval(_categoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientConfigurationCollection":
					this.PerformRelatedEntityRemoval(_clientConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					DesetupSyncTimestampCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_timestampCollection!=null)
			{
				toReturn.Add(_timestampCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_categoryCollection);
			toReturn.Add(_clientConfigurationCollection);
			toReturn.Add(_deliverypointgroupCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 menuId)
		{
			return FetchUsingPK(menuId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 menuId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(menuId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 menuId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(menuId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 menuId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(menuId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MenuId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MenuRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCategoryCollection(forceFetch, _categoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCategoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCategoryCollection || forceFetch || _alwaysFetchCategoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollection);
				_categoryCollection.SuppressClearInGetMulti=!forceFetch;
				_categoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_categoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollection = true;
			}
			return _categoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollection'. These settings will be taken into account
		/// when the property CategoryCollection is requested or GetMultiCategoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollection.SortClauses=sortClauses;
			_categoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientConfigurationCollection || forceFetch || _alwaysFetchClientConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientConfigurationCollection);
				_clientConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_clientConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientConfigurationCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, filter);
				_clientConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientConfigurationCollection = true;
			}
			return _clientConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientConfigurationCollection'. These settings will be taken into account
		/// when the property ClientConfigurationCollection is requested or GetMultiClientConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientConfigurationCollection.SortClauses=sortClauses;
			_clientConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AnnouncementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiAnnouncementCollectionViaDeliverypointgroup(forceFetch, _announcementCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AnnouncementCollection GetMultiAnnouncementCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchAnnouncementCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_announcementCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_announcementCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_announcementCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"));
				_announcementCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = true;
			}
			return _announcementCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'AnnouncementCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property AnnouncementCollectionViaDeliverypointgroup is requested or GetMultiAnnouncementCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAnnouncementCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_announcementCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_announcementCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaCategory(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaCategory(forceFetch, _categoryCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaCategory || forceFetch || _alwaysFetchCategoryCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_categoryCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaCategory.GetMulti(filter, GetRelationsForField("CategoryCollectionViaCategory"));
				_categoryCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaCategory = true;
			}
			return _categoryCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaCategory'. These settings will be taken into account
		/// when the property CategoryCollectionViaCategory is requested or GetMultiCategoryCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaCategory.SortClauses=sortClauses;
			_categoryCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaCategory(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaCategory(forceFetch, _companyCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaCategory || forceFetch || _alwaysFetchCompanyCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_companyCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaCategory.GetMulti(filter, GetRelationsForField("CompanyCollectionViaCategory"));
				_companyCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaCategory = true;
			}
			return _companyCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaCategory'. These settings will be taken into account
		/// when the property CompanyCollectionViaCategory is requested or GetMultiCompanyCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaCategory.SortClauses=sortClauses;
			_companyCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaDeliverypointgroup(forceFetch, _companyCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchCompanyCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"));
				_companyCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = true;
			}
			return _companyCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property CompanyCollectionViaDeliverypointgroup is requested or GetMultiCompanyCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_companyCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaCategory(bool forceFetch)
		{
			return GetMultiGenericcategoryCollectionViaCategory(forceFetch, _genericcategoryCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericcategoryCollection GetMultiGenericcategoryCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedGenericcategoryCollectionViaCategory || forceFetch || _alwaysFetchGenericcategoryCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericcategoryCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_genericcategoryCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_genericcategoryCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_genericcategoryCollectionViaCategory.GetMulti(filter, GetRelationsForField("GenericcategoryCollectionViaCategory"));
				_genericcategoryCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericcategoryCollectionViaCategory = true;
			}
			return _genericcategoryCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericcategoryCollectionViaCategory'. These settings will be taken into account
		/// when the property GenericcategoryCollectionViaCategory is requested or GetMultiGenericcategoryCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericcategoryCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericcategoryCollectionViaCategory.SortClauses=sortClauses;
			_genericcategoryCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PoscategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PoscategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.PoscategoryCollection GetMultiPoscategoryCollectionViaCategory(bool forceFetch)
		{
			return GetMultiPoscategoryCollectionViaCategory(forceFetch, _poscategoryCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PoscategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PoscategoryCollection GetMultiPoscategoryCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPoscategoryCollectionViaCategory || forceFetch || _alwaysFetchPoscategoryCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_poscategoryCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_poscategoryCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_poscategoryCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_poscategoryCollectionViaCategory.GetMulti(filter, GetRelationsForField("PoscategoryCollectionViaCategory"));
				_poscategoryCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedPoscategoryCollectionViaCategory = true;
			}
			return _poscategoryCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'PoscategoryCollectionViaCategory'. These settings will be taken into account
		/// when the property PoscategoryCollectionViaCategory is requested or GetMultiPoscategoryCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPoscategoryCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_poscategoryCollectionViaCategory.SortClauses=sortClauses;
			_poscategoryCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PosdeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(forceFetch, _posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_posdeliverypointgroupCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_posdeliverypointgroupCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"));
				_posdeliverypointgroupCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = true;
			}
			return _posdeliverypointgroupCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'PosdeliverypointgroupCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property PosdeliverypointgroupCollectionViaDeliverypointgroup is requested or GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPosdeliverypointgroupCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_posdeliverypointgroupCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_posdeliverypointgroupCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaCategory(bool forceFetch)
		{
			return GetMultiProductCollectionViaCategory(forceFetch, _productCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaCategory || forceFetch || _alwaysFetchProductCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_productCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaCategory.GetMulti(filter, GetRelationsForField("ProductCollectionViaCategory"));
				_productCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaCategory = true;
			}
			return _productCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaCategory'. These settings will be taken into account
		/// when the property ProductCollectionViaCategory is requested or GetMultiProductCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaCategory.SortClauses=sortClauses;
			_productCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCategory(forceFetch, _routeCollectionViaCategory.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCategory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCategory || forceFetch || _alwaysFetchRouteCollectionViaCategory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCategory);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCategory.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCategory.GetMulti(filter, GetRelationsForField("RouteCollectionViaCategory"));
				_routeCollectionViaCategory.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCategory = true;
			}
			return _routeCollectionViaCategory;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCategory'. These settings will be taken into account
		/// when the property RouteCollectionViaCategory is requested or GetMultiRouteCollectionViaCategory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCategory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCategory.SortClauses=sortClauses;
			_routeCollectionViaCategory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup(forceFetch, _routeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup"));
				_routeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = true;
			}
			return _routeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup is requested or GetMultiRouteCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiRouteCollectionViaDeliverypointgroup_(forceFetch, _routeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchRouteCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"));
				_routeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = true;
			}
			return _routeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property RouteCollectionViaDeliverypointgroup_ is requested or GetMultiRouteCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_routeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaDeliverypointgroup(forceFetch, _terminalCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchTerminalCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"));
				_terminalCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = true;
			}
			return _terminalCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property TerminalCollectionViaDeliverypointgroup is requested or GetMultiTerminalCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_terminalCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup_(forceFetch, _uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup_.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup_.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"));
				_uIModeCollectionViaDeliverypointgroup_.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup_;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup_'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup_ is requested or GetMultiUIModeCollectionViaDeliverypointgroup_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup_.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup__(forceFetch, _uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup__.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup__.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"));
				_uIModeCollectionViaDeliverypointgroup__.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = true;
			}
			return _uIModeCollectionViaDeliverypointgroup__;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup__'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup__ is requested or GetMultiUIModeCollectionViaDeliverypointgroup__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup__.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaDeliverypointgroup(forceFetch, _uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaDeliverypointgroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaDeliverypointgroup || forceFetch || _alwaysFetchUIModeCollectionViaDeliverypointgroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaDeliverypointgroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(MenuFields.MenuId, ComparisonOperator.Equal, this.MenuId, "MenuEntity__"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaDeliverypointgroup.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaDeliverypointgroup.GetMulti(filter, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"));
				_uIModeCollectionViaDeliverypointgroup.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = true;
			}
			return _uIModeCollectionViaDeliverypointgroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaDeliverypointgroup'. These settings will be taken into account
		/// when the property UIModeCollectionViaDeliverypointgroup is requested or GetMultiUIModeCollectionViaDeliverypointgroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaDeliverypointgroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaDeliverypointgroup.SortClauses=sortClauses;
			_uIModeCollectionViaDeliverypointgroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}

		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public TimestampEntity GetSingleTimestampCollection()
		{
			return GetSingleTimestampCollection(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TimestampEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TimestampEntity' which is related to this entity.</returns>
		public virtual TimestampEntity GetSingleTimestampCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TimestampEntityUsingMenuId);
				TimestampEntity newEntity = new TimestampEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCMenuId(this.MenuId);
				}
				if(fetchResult)
				{
					newEntity = (TimestampEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_timestampCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TimestampCollection = newEntity;
				_alreadyFetchedTimestampCollection = fetchResult;
			}
			return _timestampCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("CategoryCollection", _categoryCollection);
			toReturn.Add("ClientConfigurationCollection", _clientConfigurationCollection);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("AnnouncementCollectionViaDeliverypointgroup", _announcementCollectionViaDeliverypointgroup);
			toReturn.Add("CategoryCollectionViaCategory", _categoryCollectionViaCategory);
			toReturn.Add("CompanyCollectionViaCategory", _companyCollectionViaCategory);
			toReturn.Add("CompanyCollectionViaDeliverypointgroup", _companyCollectionViaDeliverypointgroup);
			toReturn.Add("GenericcategoryCollectionViaCategory", _genericcategoryCollectionViaCategory);
			toReturn.Add("PoscategoryCollectionViaCategory", _poscategoryCollectionViaCategory);
			toReturn.Add("PosdeliverypointgroupCollectionViaDeliverypointgroup", _posdeliverypointgroupCollectionViaDeliverypointgroup);
			toReturn.Add("ProductCollectionViaCategory", _productCollectionViaCategory);
			toReturn.Add("RouteCollectionViaCategory", _routeCollectionViaCategory);
			toReturn.Add("RouteCollectionViaDeliverypointgroup", _routeCollectionViaDeliverypointgroup);
			toReturn.Add("RouteCollectionViaDeliverypointgroup_", _routeCollectionViaDeliverypointgroup_);
			toReturn.Add("TerminalCollectionViaDeliverypointgroup", _terminalCollectionViaDeliverypointgroup);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup_", _uIModeCollectionViaDeliverypointgroup_);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup__", _uIModeCollectionViaDeliverypointgroup__);
			toReturn.Add("UIModeCollectionViaDeliverypointgroup", _uIModeCollectionViaDeliverypointgroup);
			toReturn.Add("TimestampCollection", _timestampCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="validator">The validator object for this MenuEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 menuId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(menuId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_categoryCollection = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_categoryCollection.SetContainingEntityInfo(this, "MenuEntity");

			_clientConfigurationCollection = new Obymobi.Data.CollectionClasses.ClientConfigurationCollection();
			_clientConfigurationCollection.SetContainingEntityInfo(this, "MenuEntity");

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "MenuEntity");
			_announcementCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
			_categoryCollectionViaCategory = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_companyCollectionViaCategory = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_genericcategoryCollectionViaCategory = new Obymobi.Data.CollectionClasses.GenericcategoryCollection();
			_poscategoryCollectionViaCategory = new Obymobi.Data.CollectionClasses.PoscategoryCollection();
			_posdeliverypointgroupCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection();
			_productCollectionViaCategory = new Obymobi.Data.CollectionClasses.ProductCollection();
			_routeCollectionViaCategory = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.RouteCollection();
			_routeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.RouteCollection();
			_terminalCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaDeliverypointgroup_ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup__ = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollectionViaDeliverypointgroup = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_companyEntityReturnsNewIfNotFound = true;
			_timestampCollectionReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMenuRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "MenuCollection", resetFKFields, new int[] { (int)MenuFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMenuRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _timestampCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTimestampCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticMenuRelations.TimestampEntityUsingMenuIdStatic, false, signalRelatedEntity, "MenuEntity", false, new int[] { (int)MenuFieldIndex.MenuId } );
			_timestampCollection = null;
		}
	
		/// <summary> setups the sync logic for member _timestampCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTimestampCollection(IEntityCore relatedEntity)
		{
			if(_timestampCollection!=relatedEntity)
			{
				DesetupSyncTimestampCollection(true, true);
				_timestampCollection = (TimestampEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _timestampCollection, new PropertyChangedEventHandler( OnTimestampCollectionPropertyChanged ), "TimestampCollection", Obymobi.Data.RelationClasses.StaticMenuRelations.TimestampEntityUsingMenuIdStatic, false, ref _alreadyFetchedTimestampCollection, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTimestampCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 menuId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MenuFieldIndex.MenuId].ForcedCurrentValueWrite(menuId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMenuDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MenuEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MenuRelations Relations
		{
			get	{ return new MenuRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryCollection")[0], (int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationCollection")[0], (int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Announcement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAnnouncementCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AnnouncementCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.AnnouncementEntity, 0, null, null, GetRelationsForField("AnnouncementCollectionViaDeliverypointgroup"), "AnnouncementCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaCategory"), "CategoryCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaCategory"), "CompanyCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaDeliverypointgroup"), "CompanyCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericcategoryCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.GenericcategoryEntity, 0, null, null, GetRelationsForField("GenericcategoryCollectionViaCategory"), "GenericcategoryCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Poscategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPoscategoryCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PoscategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.PoscategoryEntity, 0, null, null, GetRelationsForField("PoscategoryCollectionViaCategory"), "PoscategoryCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posdeliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.PosdeliverypointgroupEntity, 0, null, null, GetRelationsForField("PosdeliverypointgroupCollectionViaDeliverypointgroup"), "PosdeliverypointgroupCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaCategory"), "ProductCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCategory
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CategoryEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Category_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCategory"), "RouteCollectionViaCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup"), "RouteCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaDeliverypointgroup_"), "RouteCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaDeliverypointgroup"), "TerminalCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup_"), "UIModeCollectionViaDeliverypointgroup_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup__"), "UIModeCollectionViaDeliverypointgroup__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaDeliverypointgroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DeliverypointgroupEntityUsingMenuId;
				intermediateRelation.SetAliases(string.Empty, "Deliverypointgroup_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaDeliverypointgroup"), "UIModeCollectionViaDeliverypointgroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.MenuEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MenuId property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."MenuId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MenuId
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.MenuId, true); }
			set	{ SetValue((int)MenuFieldIndex.MenuId, value, true); }
		}

		/// <summary> The Name property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)MenuFieldIndex.Name, true); }
			set	{ SetValue((int)MenuFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MenuFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MenuFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)MenuFieldIndex.CompanyId, true); }
			set	{ SetValue((int)MenuFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MenuFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MenuFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Menu<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MenuFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MenuFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollection
		{
			get	{ return GetMultiCategoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollection. When set to true, CategoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCategoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollection
		{
			get	{ return _alwaysFetchCategoryCollection; }
			set	{ _alwaysFetchCategoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollection already has been fetched. Setting this property to false when CategoryCollection has been fetched
		/// will clear the CategoryCollection collection well. Setting this property to true while CategoryCollection hasn't been fetched disables lazy loading for CategoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollection
		{
			get { return _alreadyFetchedCategoryCollection;}
			set 
			{
				if(_alreadyFetchedCategoryCollection && !value && (_categoryCollection != null))
				{
					_categoryCollection.Clear();
				}
				_alreadyFetchedCategoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection ClientConfigurationCollection
		{
			get	{ return GetMultiClientConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationCollection. When set to true, ClientConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationCollection
		{
			get	{ return _alwaysFetchClientConfigurationCollection; }
			set	{ _alwaysFetchClientConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationCollection already has been fetched. Setting this property to false when ClientConfigurationCollection has been fetched
		/// will clear the ClientConfigurationCollection collection well. Setting this property to true while ClientConfigurationCollection hasn't been fetched disables lazy loading for ClientConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationCollection
		{
			get { return _alreadyFetchedClientConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedClientConfigurationCollection && !value && (_clientConfigurationCollection != null))
				{
					_clientConfigurationCollection.Clear();
				}
				_alreadyFetchedClientConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AnnouncementEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAnnouncementCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AnnouncementCollection AnnouncementCollectionViaDeliverypointgroup
		{
			get { return GetMultiAnnouncementCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AnnouncementCollectionViaDeliverypointgroup. When set to true, AnnouncementCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AnnouncementCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiAnnouncementCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAnnouncementCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchAnnouncementCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchAnnouncementCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AnnouncementCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when AnnouncementCollectionViaDeliverypointgroup has been fetched
		/// will clear the AnnouncementCollectionViaDeliverypointgroup collection well. Setting this property to true while AnnouncementCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for AnnouncementCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAnnouncementCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedAnnouncementCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup && !value && (_announcementCollectionViaDeliverypointgroup != null))
				{
					_announcementCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedAnnouncementCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaCategory
		{
			get { return GetMultiCategoryCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaCategory. When set to true, CategoryCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaCategory
		{
			get	{ return _alwaysFetchCategoryCollectionViaCategory; }
			set	{ _alwaysFetchCategoryCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaCategory already has been fetched. Setting this property to false when CategoryCollectionViaCategory has been fetched
		/// will clear the CategoryCollectionViaCategory collection well. Setting this property to true while CategoryCollectionViaCategory hasn't been fetched disables lazy loading for CategoryCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaCategory
		{
			get { return _alreadyFetchedCategoryCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaCategory && !value && (_categoryCollectionViaCategory != null))
				{
					_categoryCollectionViaCategory.Clear();
				}
				_alreadyFetchedCategoryCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaCategory
		{
			get { return GetMultiCompanyCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaCategory. When set to true, CompanyCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaCategory
		{
			get	{ return _alwaysFetchCompanyCollectionViaCategory; }
			set	{ _alwaysFetchCompanyCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaCategory already has been fetched. Setting this property to false when CompanyCollectionViaCategory has been fetched
		/// will clear the CompanyCollectionViaCategory collection well. Setting this property to true while CompanyCollectionViaCategory hasn't been fetched disables lazy loading for CompanyCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaCategory
		{
			get { return _alreadyFetchedCompanyCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaCategory && !value && (_companyCollectionViaCategory != null))
				{
					_companyCollectionViaCategory.Clear();
				}
				_alreadyFetchedCompanyCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaDeliverypointgroup
		{
			get { return GetMultiCompanyCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaDeliverypointgroup. When set to true, CompanyCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchCompanyCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchCompanyCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when CompanyCollectionViaDeliverypointgroup has been fetched
		/// will clear the CompanyCollectionViaDeliverypointgroup collection well. Setting this property to true while CompanyCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for CompanyCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedCompanyCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaDeliverypointgroup && !value && (_companyCollectionViaDeliverypointgroup != null))
				{
					_companyCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedCompanyCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'GenericcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericcategoryCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericcategoryCollection GenericcategoryCollectionViaCategory
		{
			get { return GetMultiGenericcategoryCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericcategoryCollectionViaCategory. When set to true, GenericcategoryCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericcategoryCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiGenericcategoryCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericcategoryCollectionViaCategory
		{
			get	{ return _alwaysFetchGenericcategoryCollectionViaCategory; }
			set	{ _alwaysFetchGenericcategoryCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericcategoryCollectionViaCategory already has been fetched. Setting this property to false when GenericcategoryCollectionViaCategory has been fetched
		/// will clear the GenericcategoryCollectionViaCategory collection well. Setting this property to true while GenericcategoryCollectionViaCategory hasn't been fetched disables lazy loading for GenericcategoryCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericcategoryCollectionViaCategory
		{
			get { return _alreadyFetchedGenericcategoryCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedGenericcategoryCollectionViaCategory && !value && (_genericcategoryCollectionViaCategory != null))
				{
					_genericcategoryCollectionViaCategory.Clear();
				}
				_alreadyFetchedGenericcategoryCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PoscategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPoscategoryCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PoscategoryCollection PoscategoryCollectionViaCategory
		{
			get { return GetMultiPoscategoryCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PoscategoryCollectionViaCategory. When set to true, PoscategoryCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PoscategoryCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiPoscategoryCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPoscategoryCollectionViaCategory
		{
			get	{ return _alwaysFetchPoscategoryCollectionViaCategory; }
			set	{ _alwaysFetchPoscategoryCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PoscategoryCollectionViaCategory already has been fetched. Setting this property to false when PoscategoryCollectionViaCategory has been fetched
		/// will clear the PoscategoryCollectionViaCategory collection well. Setting this property to true while PoscategoryCollectionViaCategory hasn't been fetched disables lazy loading for PoscategoryCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPoscategoryCollectionViaCategory
		{
			get { return _alreadyFetchedPoscategoryCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedPoscategoryCollectionViaCategory && !value && (_poscategoryCollectionViaCategory != null))
				{
					_poscategoryCollectionViaCategory.Clear();
				}
				_alreadyFetchedPoscategoryCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PosdeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PosdeliverypointgroupCollection PosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PosdeliverypointgroupCollectionViaDeliverypointgroup. When set to true, PosdeliverypointgroupCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PosdeliverypointgroupCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiPosdeliverypointgroupCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchPosdeliverypointgroupCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PosdeliverypointgroupCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when PosdeliverypointgroupCollectionViaDeliverypointgroup has been fetched
		/// will clear the PosdeliverypointgroupCollectionViaDeliverypointgroup collection well. Setting this property to true while PosdeliverypointgroupCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for PosdeliverypointgroupCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup && !value && (_posdeliverypointgroupCollectionViaDeliverypointgroup != null))
				{
					_posdeliverypointgroupCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedPosdeliverypointgroupCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaCategory
		{
			get { return GetMultiProductCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaCategory. When set to true, ProductCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaCategory
		{
			get	{ return _alwaysFetchProductCollectionViaCategory; }
			set	{ _alwaysFetchProductCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaCategory already has been fetched. Setting this property to false when ProductCollectionViaCategory has been fetched
		/// will clear the ProductCollectionViaCategory collection well. Setting this property to true while ProductCollectionViaCategory hasn't been fetched disables lazy loading for ProductCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaCategory
		{
			get { return _alreadyFetchedProductCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaCategory && !value && (_productCollectionViaCategory != null))
				{
					_productCollectionViaCategory.Clear();
				}
				_alreadyFetchedProductCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCategory
		{
			get { return GetMultiRouteCollectionViaCategory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCategory. When set to true, RouteCollectionViaCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCategory is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCategory
		{
			get	{ return _alwaysFetchRouteCollectionViaCategory; }
			set	{ _alwaysFetchRouteCollectionViaCategory = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCategory already has been fetched. Setting this property to false when RouteCollectionViaCategory has been fetched
		/// will clear the RouteCollectionViaCategory collection well. Setting this property to true while RouteCollectionViaCategory hasn't been fetched disables lazy loading for RouteCollectionViaCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCategory
		{
			get { return _alreadyFetchedRouteCollectionViaCategory;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCategory && !value && (_routeCollectionViaCategory != null))
				{
					_routeCollectionViaCategory.Clear();
				}
				_alreadyFetchedRouteCollectionViaCategory = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup. When set to true, RouteCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup && !value && (_routeCollectionViaDeliverypointgroup != null))
				{
					_routeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaDeliverypointgroup_
		{
			get { return GetMultiRouteCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaDeliverypointgroup_. When set to true, RouteCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchRouteCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchRouteCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when RouteCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the RouteCollectionViaDeliverypointgroup_ collection well. Setting this property to true while RouteCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for RouteCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedRouteCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaDeliverypointgroup_ && !value && (_routeCollectionViaDeliverypointgroup_ != null))
				{
					_routeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedRouteCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaDeliverypointgroup
		{
			get { return GetMultiTerminalCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaDeliverypointgroup. When set to true, TerminalCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchTerminalCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchTerminalCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when TerminalCollectionViaDeliverypointgroup has been fetched
		/// will clear the TerminalCollectionViaDeliverypointgroup collection well. Setting this property to true while TerminalCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for TerminalCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedTerminalCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaDeliverypointgroup && !value && (_terminalCollectionViaDeliverypointgroup != null))
				{
					_terminalCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedTerminalCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup_
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup_. When set to true, UIModeCollectionViaDeliverypointgroup_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup_ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup_
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup_; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup_ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup_ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup_ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup_ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup_
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup_;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ && !value && (_uIModeCollectionViaDeliverypointgroup_ != null))
				{
					_uIModeCollectionViaDeliverypointgroup_.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup__
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup__. When set to true, UIModeCollectionViaDeliverypointgroup__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup__ is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup__
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup__; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup__ already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup__ has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup__ collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup__ hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup__
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup__;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ && !value && (_uIModeCollectionViaDeliverypointgroup__ != null))
				{
					_uIModeCollectionViaDeliverypointgroup__.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaDeliverypointgroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaDeliverypointgroup
		{
			get { return GetMultiUIModeCollectionViaDeliverypointgroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaDeliverypointgroup. When set to true, UIModeCollectionViaDeliverypointgroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaDeliverypointgroup is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaDeliverypointgroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaDeliverypointgroup
		{
			get	{ return _alwaysFetchUIModeCollectionViaDeliverypointgroup; }
			set	{ _alwaysFetchUIModeCollectionViaDeliverypointgroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaDeliverypointgroup already has been fetched. Setting this property to false when UIModeCollectionViaDeliverypointgroup has been fetched
		/// will clear the UIModeCollectionViaDeliverypointgroup collection well. Setting this property to true while UIModeCollectionViaDeliverypointgroup hasn't been fetched disables lazy loading for UIModeCollectionViaDeliverypointgroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaDeliverypointgroup
		{
			get { return _alreadyFetchedUIModeCollectionViaDeliverypointgroup;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaDeliverypointgroup && !value && (_uIModeCollectionViaDeliverypointgroup != null))
				{
					_uIModeCollectionViaDeliverypointgroup.Clear();
				}
				_alreadyFetchedUIModeCollectionViaDeliverypointgroup = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MenuCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TimestampEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TimestampEntity TimestampCollection
		{
			get	{ return GetSingleTimestampCollection(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTimestampCollection(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_timestampCollection !=null);
						DesetupSyncTimestampCollection(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TimestampCollection");
						}
					}
					else
					{
						if(_timestampCollection!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "MenuEntity");
							SetupSyncTimestampCollection(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute a forced fetch by calling GetSingleTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will set TimestampCollection to null as well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value)
				{
					this.TimestampCollection = null;
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TimestampCollection is not found
		/// in the database. When set to true, TimestampCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TimestampCollectionReturnsNewIfNotFound
		{
			get	{ return _timestampCollectionReturnsNewIfNotFound; }
			set	{ _timestampCollectionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MenuEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
