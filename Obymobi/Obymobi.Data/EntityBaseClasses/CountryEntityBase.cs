﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Country'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CountryEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CountryEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CompanyCollection	_companyCollection;
		private bool	_alwaysFetchCompanyCollection, _alreadyFetchedCompanyCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection	_pointOfInterestCollection;
		private bool	_alwaysFetchPointOfInterestCollection, _alreadyFetchedPointOfInterestCollection;
		private Obymobi.Data.CollectionClasses.VattariffCollection	_vattariffCollection;
		private bool	_alwaysFetchVattariffCollection, _alreadyFetchedVattariffCollection;
		private Obymobi.Data.CollectionClasses.CompanyOwnerCollection _companyOwnerCollectionViaCompany;
		private bool	_alwaysFetchCompanyOwnerCollectionViaCompany, _alreadyFetchedCompanyOwnerCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.CurrencyCollection _currencyCollectionViaCompany;
		private bool	_alwaysFetchCurrencyCollectionViaCompany, _alreadyFetchedCurrencyCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCompany;
		private bool	_alwaysFetchRouteCollectionViaCompany, _alreadyFetchedRouteCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaCompany;
		private bool	_alwaysFetchSupportpoolCollectionViaCompany, _alreadyFetchedSupportpoolCollectionViaCompany;
		private CurrencyEntity _currencyEntity;
		private bool	_alwaysFetchCurrencyEntity, _alreadyFetchedCurrencyEntity, _currencyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CurrencyEntity</summary>
			public static readonly string CurrencyEntity = "CurrencyEntity";
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name PointOfInterestCollection</summary>
			public static readonly string PointOfInterestCollection = "PointOfInterestCollection";
			/// <summary>Member name VattariffCollection</summary>
			public static readonly string VattariffCollection = "VattariffCollection";
			/// <summary>Member name CompanyOwnerCollectionViaCompany</summary>
			public static readonly string CompanyOwnerCollectionViaCompany = "CompanyOwnerCollectionViaCompany";
			/// <summary>Member name CurrencyCollectionViaCompany</summary>
			public static readonly string CurrencyCollectionViaCompany = "CurrencyCollectionViaCompany";
			/// <summary>Member name RouteCollectionViaCompany</summary>
			public static readonly string RouteCollectionViaCompany = "RouteCollectionViaCompany";
			/// <summary>Member name SupportpoolCollectionViaCompany</summary>
			public static readonly string SupportpoolCollectionViaCompany = "SupportpoolCollectionViaCompany";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CountryEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CountryEntityBase() :base("CountryEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		protected CountryEntityBase(System.Int32 countryId):base("CountryEntity")
		{
			InitClassFetch(countryId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CountryEntityBase(System.Int32 countryId, IPrefetchPath prefetchPathToUse): base("CountryEntity")
		{
			InitClassFetch(countryId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <param name="validator">The custom validator object for this CountryEntity</param>
		protected CountryEntityBase(System.Int32 countryId, IValidator validator):base("CountryEntity")
		{
			InitClassFetch(countryId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CountryEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_companyCollection = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollection", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollection = info.GetBoolean("_alwaysFetchCompanyCollection");
			_alreadyFetchedCompanyCollection = info.GetBoolean("_alreadyFetchedCompanyCollection");

			_pointOfInterestCollection = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollection = info.GetBoolean("_alwaysFetchPointOfInterestCollection");
			_alreadyFetchedPointOfInterestCollection = info.GetBoolean("_alreadyFetchedPointOfInterestCollection");

			_vattariffCollection = (Obymobi.Data.CollectionClasses.VattariffCollection)info.GetValue("_vattariffCollection", typeof(Obymobi.Data.CollectionClasses.VattariffCollection));
			_alwaysFetchVattariffCollection = info.GetBoolean("_alwaysFetchVattariffCollection");
			_alreadyFetchedVattariffCollection = info.GetBoolean("_alreadyFetchedVattariffCollection");
			_companyOwnerCollectionViaCompany = (Obymobi.Data.CollectionClasses.CompanyOwnerCollection)info.GetValue("_companyOwnerCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CompanyOwnerCollection));
			_alwaysFetchCompanyOwnerCollectionViaCompany = info.GetBoolean("_alwaysFetchCompanyOwnerCollectionViaCompany");
			_alreadyFetchedCompanyOwnerCollectionViaCompany = info.GetBoolean("_alreadyFetchedCompanyOwnerCollectionViaCompany");

			_currencyCollectionViaCompany = (Obymobi.Data.CollectionClasses.CurrencyCollection)info.GetValue("_currencyCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CurrencyCollection));
			_alwaysFetchCurrencyCollectionViaCompany = info.GetBoolean("_alwaysFetchCurrencyCollectionViaCompany");
			_alreadyFetchedCurrencyCollectionViaCompany = info.GetBoolean("_alreadyFetchedCurrencyCollectionViaCompany");

			_routeCollectionViaCompany = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCompany = info.GetBoolean("_alwaysFetchRouteCollectionViaCompany");
			_alreadyFetchedRouteCollectionViaCompany = info.GetBoolean("_alreadyFetchedRouteCollectionViaCompany");

			_supportpoolCollectionViaCompany = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaCompany = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaCompany");
			_alreadyFetchedSupportpoolCollectionViaCompany = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaCompany");
			_currencyEntity = (CurrencyEntity)info.GetValue("_currencyEntity", typeof(CurrencyEntity));
			if(_currencyEntity!=null)
			{
				_currencyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_currencyEntityReturnsNewIfNotFound = info.GetBoolean("_currencyEntityReturnsNewIfNotFound");
			_alwaysFetchCurrencyEntity = info.GetBoolean("_alwaysFetchCurrencyEntity");
			_alreadyFetchedCurrencyEntity = info.GetBoolean("_alreadyFetchedCurrencyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CountryFieldIndex)fieldIndex)
			{
				case CountryFieldIndex.CurrencyId:
					DesetupSyncCurrencyEntity(true, false);
					_alreadyFetchedCurrencyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCompanyCollection = (_companyCollection.Count > 0);
			_alreadyFetchedPointOfInterestCollection = (_pointOfInterestCollection.Count > 0);
			_alreadyFetchedVattariffCollection = (_vattariffCollection.Count > 0);
			_alreadyFetchedCompanyOwnerCollectionViaCompany = (_companyOwnerCollectionViaCompany.Count > 0);
			_alreadyFetchedCurrencyCollectionViaCompany = (_currencyCollectionViaCompany.Count > 0);
			_alreadyFetchedRouteCollectionViaCompany = (_routeCollectionViaCompany.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaCompany = (_supportpoolCollectionViaCompany.Count > 0);
			_alreadyFetchedCurrencyEntity = (_currencyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CurrencyEntity":
					toReturn.Add(Relations.CurrencyEntityUsingCurrencyId);
					break;
				case "CompanyCollection":
					toReturn.Add(Relations.CompanyEntityUsingCountryId);
					break;
				case "PointOfInterestCollection":
					toReturn.Add(Relations.PointOfInterestEntityUsingCountryId);
					break;
				case "VattariffCollection":
					toReturn.Add(Relations.VattariffEntityUsingCountryId);
					break;
				case "CompanyOwnerCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCountryId, "CountryEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CompanyOwnerEntityUsingCompanyOwnerId, "Company_", string.Empty, JoinHint.None);
					break;
				case "CurrencyCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCountryId, "CountryEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CurrencyEntityUsingCurrencyId, "Company_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCountryId, "CountryEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.RouteEntityUsingRouteId, "Company_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCountryId, "CountryEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.SupportpoolEntityUsingSupportpoolId, "Company_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_companyCollection", (!this.MarkedForDeletion?_companyCollection:null));
			info.AddValue("_alwaysFetchCompanyCollection", _alwaysFetchCompanyCollection);
			info.AddValue("_alreadyFetchedCompanyCollection", _alreadyFetchedCompanyCollection);
			info.AddValue("_pointOfInterestCollection", (!this.MarkedForDeletion?_pointOfInterestCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestCollection", _alwaysFetchPointOfInterestCollection);
			info.AddValue("_alreadyFetchedPointOfInterestCollection", _alreadyFetchedPointOfInterestCollection);
			info.AddValue("_vattariffCollection", (!this.MarkedForDeletion?_vattariffCollection:null));
			info.AddValue("_alwaysFetchVattariffCollection", _alwaysFetchVattariffCollection);
			info.AddValue("_alreadyFetchedVattariffCollection", _alreadyFetchedVattariffCollection);
			info.AddValue("_companyOwnerCollectionViaCompany", (!this.MarkedForDeletion?_companyOwnerCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCompanyOwnerCollectionViaCompany", _alwaysFetchCompanyOwnerCollectionViaCompany);
			info.AddValue("_alreadyFetchedCompanyOwnerCollectionViaCompany", _alreadyFetchedCompanyOwnerCollectionViaCompany);
			info.AddValue("_currencyCollectionViaCompany", (!this.MarkedForDeletion?_currencyCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCurrencyCollectionViaCompany", _alwaysFetchCurrencyCollectionViaCompany);
			info.AddValue("_alreadyFetchedCurrencyCollectionViaCompany", _alreadyFetchedCurrencyCollectionViaCompany);
			info.AddValue("_routeCollectionViaCompany", (!this.MarkedForDeletion?_routeCollectionViaCompany:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCompany", _alwaysFetchRouteCollectionViaCompany);
			info.AddValue("_alreadyFetchedRouteCollectionViaCompany", _alreadyFetchedRouteCollectionViaCompany);
			info.AddValue("_supportpoolCollectionViaCompany", (!this.MarkedForDeletion?_supportpoolCollectionViaCompany:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaCompany", _alwaysFetchSupportpoolCollectionViaCompany);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaCompany", _alreadyFetchedSupportpoolCollectionViaCompany);
			info.AddValue("_currencyEntity", (!this.MarkedForDeletion?_currencyEntity:null));
			info.AddValue("_currencyEntityReturnsNewIfNotFound", _currencyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCurrencyEntity", _alwaysFetchCurrencyEntity);
			info.AddValue("_alreadyFetchedCurrencyEntity", _alreadyFetchedCurrencyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CurrencyEntity":
					_alreadyFetchedCurrencyEntity = true;
					this.CurrencyEntity = (CurrencyEntity)entity;
					break;
				case "CompanyCollection":
					_alreadyFetchedCompanyCollection = true;
					if(entity!=null)
					{
						this.CompanyCollection.Add((CompanyEntity)entity);
					}
					break;
				case "PointOfInterestCollection":
					_alreadyFetchedPointOfInterestCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestCollection.Add((PointOfInterestEntity)entity);
					}
					break;
				case "VattariffCollection":
					_alreadyFetchedVattariffCollection = true;
					if(entity!=null)
					{
						this.VattariffCollection.Add((VattariffEntity)entity);
					}
					break;
				case "CompanyOwnerCollectionViaCompany":
					_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CompanyOwnerCollectionViaCompany.Add((CompanyOwnerEntity)entity);
					}
					break;
				case "CurrencyCollectionViaCompany":
					_alreadyFetchedCurrencyCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CurrencyCollectionViaCompany.Add((CurrencyEntity)entity);
					}
					break;
				case "RouteCollectionViaCompany":
					_alreadyFetchedRouteCollectionViaCompany = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCompany.Add((RouteEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaCompany":
					_alreadyFetchedSupportpoolCollectionViaCompany = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaCompany.Add((SupportpoolEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CurrencyEntity":
					SetupSyncCurrencyEntity(relatedEntity);
					break;
				case "CompanyCollection":
					_companyCollection.Add((CompanyEntity)relatedEntity);
					break;
				case "PointOfInterestCollection":
					_pointOfInterestCollection.Add((PointOfInterestEntity)relatedEntity);
					break;
				case "VattariffCollection":
					_vattariffCollection.Add((VattariffEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CurrencyEntity":
					DesetupSyncCurrencyEntity(false, true);
					break;
				case "CompanyCollection":
					this.PerformRelatedEntityRemoval(_companyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VattariffCollection":
					this.PerformRelatedEntityRemoval(_vattariffCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_currencyEntity!=null)
			{
				toReturn.Add(_currencyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_companyCollection);
			toReturn.Add(_pointOfInterestCollection);
			toReturn.Add(_vattariffCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 countryId)
		{
			return FetchUsingPK(countryId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 countryId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(countryId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 countryId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(countryId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 countryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(countryId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CountryId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CountryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyCollection || forceFetch || _alwaysFetchCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollection);
				_companyCollection.SuppressClearInGetMulti=!forceFetch;
				_companyCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, filter);
				_companyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollection = true;
			}
			return _companyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollection'. These settings will be taken into account
		/// when the property CompanyCollection is requested or GetMultiCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollection.SortClauses=sortClauses;
			_companyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestCollection(forceFetch, _pointOfInterestCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestCollection(forceFetch, _pointOfInterestCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollection || forceFetch || _alwaysFetchPointOfInterestCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollection);
				_pointOfInterestCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollection.GetMultiManyToOne(null, this, null, null, filter);
				_pointOfInterestCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollection = true;
			}
			return _pointOfInterestCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollection'. These settings will be taken into account
		/// when the property PointOfInterestCollection is requested or GetMultiPointOfInterestCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollection.SortClauses=sortClauses;
			_pointOfInterestCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VattariffEntity'</returns>
		public Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollection(bool forceFetch)
		{
			return GetMultiVattariffCollection(forceFetch, _vattariffCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VattariffEntity'</returns>
		public Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVattariffCollection(forceFetch, _vattariffCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVattariffCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.VattariffCollection GetMultiVattariffCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVattariffCollection || forceFetch || _alwaysFetchVattariffCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vattariffCollection);
				_vattariffCollection.SuppressClearInGetMulti=!forceFetch;
				_vattariffCollection.EntityFactoryToUse = entityFactoryToUse;
				_vattariffCollection.GetMultiManyToOne(this, filter);
				_vattariffCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedVattariffCollection = true;
			}
			return _vattariffCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'VattariffCollection'. These settings will be taken into account
		/// when the property VattariffCollection is requested or GetMultiVattariffCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVattariffCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vattariffCollection.SortClauses=sortClauses;
			_vattariffCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyOwnerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCompanyOwnerCollectionViaCompany(forceFetch, _companyOwnerCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyOwnerCollectionViaCompany || forceFetch || _alwaysFetchCompanyOwnerCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyOwnerCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CountryFields.CountryId, ComparisonOperator.Equal, this.CountryId, "CountryEntity__"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_companyOwnerCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_companyOwnerCollectionViaCompany.GetMulti(filter, GetRelationsForField("CompanyOwnerCollectionViaCompany"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
			}
			return _companyOwnerCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyOwnerCollectionViaCompany'. These settings will be taken into account
		/// when the property CompanyOwnerCollectionViaCompany is requested or GetMultiCompanyOwnerCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyOwnerCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyOwnerCollectionViaCompany.SortClauses=sortClauses;
			_companyOwnerCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CurrencyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCurrencyCollectionViaCompany(forceFetch, _currencyCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CurrencyCollection GetMultiCurrencyCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCurrencyCollectionViaCompany || forceFetch || _alwaysFetchCurrencyCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_currencyCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CountryFields.CountryId, ComparisonOperator.Equal, this.CountryId, "CountryEntity__"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_currencyCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_currencyCollectionViaCompany.GetMulti(filter, GetRelationsForField("CurrencyCollectionViaCompany"));
				_currencyCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCurrencyCollectionViaCompany = true;
			}
			return _currencyCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CurrencyCollectionViaCompany'. These settings will be taken into account
		/// when the property CurrencyCollectionViaCompany is requested or GetMultiCurrencyCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCurrencyCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_currencyCollectionViaCompany.SortClauses=sortClauses;
			_currencyCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCompany(forceFetch, _routeCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCompany || forceFetch || _alwaysFetchRouteCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CountryFields.CountryId, ComparisonOperator.Equal, this.CountryId, "CountryEntity__"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCompany.GetMulti(filter, GetRelationsForField("RouteCollectionViaCompany"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCompany = true;
			}
			return _routeCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCompany'. These settings will be taken into account
		/// when the property RouteCollectionViaCompany is requested or GetMultiRouteCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCompany.SortClauses=sortClauses;
			_routeCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaCompany(forceFetch, _supportpoolCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaCompany || forceFetch || _alwaysFetchSupportpoolCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CountryFields.CountryId, ComparisonOperator.Equal, this.CountryId, "CountryEntity__"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaCompany.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaCompany"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaCompany = true;
			}
			return _supportpoolCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaCompany'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaCompany is requested or GetMultiSupportpoolCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaCompany.SortClauses=sortClauses;
			_supportpoolCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CurrencyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CurrencyEntity' which is related to this entity.</returns>
		public CurrencyEntity GetSingleCurrencyEntity()
		{
			return GetSingleCurrencyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CurrencyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CurrencyEntity' which is related to this entity.</returns>
		public virtual CurrencyEntity GetSingleCurrencyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCurrencyEntity || forceFetch || _alwaysFetchCurrencyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CurrencyEntityUsingCurrencyId);
				CurrencyEntity newEntity = new CurrencyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CurrencyId);
				}
				if(fetchResult)
				{
					newEntity = (CurrencyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_currencyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CurrencyEntity = newEntity;
				_alreadyFetchedCurrencyEntity = fetchResult;
			}
			return _currencyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CurrencyEntity", _currencyEntity);
			toReturn.Add("CompanyCollection", _companyCollection);
			toReturn.Add("PointOfInterestCollection", _pointOfInterestCollection);
			toReturn.Add("VattariffCollection", _vattariffCollection);
			toReturn.Add("CompanyOwnerCollectionViaCompany", _companyOwnerCollectionViaCompany);
			toReturn.Add("CurrencyCollectionViaCompany", _currencyCollectionViaCompany);
			toReturn.Add("RouteCollectionViaCompany", _routeCollectionViaCompany);
			toReturn.Add("SupportpoolCollectionViaCompany", _supportpoolCollectionViaCompany);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <param name="validator">The validator object for this CountryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 countryId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(countryId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_companyCollection = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollection.SetContainingEntityInfo(this, "CountryEntity");

			_pointOfInterestCollection = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_pointOfInterestCollection.SetContainingEntityInfo(this, "CountryEntity");

			_vattariffCollection = new Obymobi.Data.CollectionClasses.VattariffCollection();
			_vattariffCollection.SetContainingEntityInfo(this, "CountryEntity");
			_companyOwnerCollectionViaCompany = new Obymobi.Data.CollectionClasses.CompanyOwnerCollection();
			_currencyCollectionViaCompany = new Obymobi.Data.CollectionClasses.CurrencyCollection();
			_routeCollectionViaCompany = new Obymobi.Data.CollectionClasses.RouteCollection();
			_supportpoolCollectionViaCompany = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			_currencyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CultureName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodeAlpha3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _currencyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCurrencyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _currencyEntity, new PropertyChangedEventHandler( OnCurrencyEntityPropertyChanged ), "CurrencyEntity", Obymobi.Data.RelationClasses.StaticCountryRelations.CurrencyEntityUsingCurrencyIdStatic, true, signalRelatedEntity, "CountryCollection", resetFKFields, new int[] { (int)CountryFieldIndex.CurrencyId } );		
			_currencyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _currencyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCurrencyEntity(IEntityCore relatedEntity)
		{
			if(_currencyEntity!=relatedEntity)
			{		
				DesetupSyncCurrencyEntity(true, true);
				_currencyEntity = (CurrencyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _currencyEntity, new PropertyChangedEventHandler( OnCurrencyEntityPropertyChanged ), "CurrencyEntity", Obymobi.Data.RelationClasses.StaticCountryRelations.CurrencyEntityUsingCurrencyIdStatic, true, ref _alreadyFetchedCurrencyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCurrencyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="countryId">PK value for Country which data should be fetched into this Country object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 countryId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CountryFieldIndex.CountryId].ForcedCurrentValueWrite(countryId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCountryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CountryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CountryRelations Relations
		{
			get	{ return new CountryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyCollection")[0], (int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestCollection")[0], (int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Vattariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVattariffCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.VattariffCollection(), (IEntityRelation)GetRelationsForField("VattariffCollection")[0], (int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.VattariffEntity, 0, null, null, null, "VattariffCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyOwner'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyOwnerCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCountryId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyOwnerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.CompanyOwnerEntity, 0, null, null, GetRelationsForField("CompanyOwnerCollectionViaCompany"), "CompanyOwnerCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCountryId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, GetRelationsForField("CurrencyCollectionViaCompany"), "CurrencyCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCountryId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCompany"), "RouteCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCountryId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaCompany"), "SupportpoolCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Currency'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCurrencyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CurrencyCollection(), (IEntityRelation)GetRelationsForField("CurrencyEntity")[0], (int)Obymobi.Data.EntityType.CountryEntity, (int)Obymobi.Data.EntityType.CurrencyEntity, 0, null, null, null, "CurrencyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CountryId property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."CountryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CountryId
		{
			get { return (System.Int32)GetValue((int)CountryFieldIndex.CountryId, true); }
			set	{ SetValue((int)CountryFieldIndex.CountryId, value, true); }
		}

		/// <summary> The Name property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CountryFieldIndex.Name, true); }
			set	{ SetValue((int)CountryFieldIndex.Name, value, true); }
		}

		/// <summary> The Code property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."Code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)CountryFieldIndex.Code, true); }
			set	{ SetValue((int)CountryFieldIndex.Code, value, true); }
		}

		/// <summary> The CultureName property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."CultureName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CultureName
		{
			get { return (System.String)GetValue((int)CountryFieldIndex.CultureName, true); }
			set	{ SetValue((int)CountryFieldIndex.CultureName, value, true); }
		}

		/// <summary> The CurrencyId property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."CurrencyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CurrencyId
		{
			get { return (System.Int32)GetValue((int)CountryFieldIndex.CurrencyId, true); }
			set	{ SetValue((int)CountryFieldIndex.CurrencyId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CountryFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CountryFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CountryFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CountryFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CodeAlpha3 property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."CodeAlpha3"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CodeAlpha3
		{
			get { return (System.String)GetValue((int)CountryFieldIndex.CodeAlpha3, true); }
			set	{ SetValue((int)CountryFieldIndex.CodeAlpha3, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CountryFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CountryFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Country<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Country"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CountryFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CountryFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollection
		{
			get	{ return GetMultiCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollection. When set to true, CompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollection
		{
			get	{ return _alwaysFetchCompanyCollection; }
			set	{ _alwaysFetchCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollection already has been fetched. Setting this property to false when CompanyCollection has been fetched
		/// will clear the CompanyCollection collection well. Setting this property to true while CompanyCollection hasn't been fetched disables lazy loading for CompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollection
		{
			get { return _alreadyFetchedCompanyCollection;}
			set 
			{
				if(_alreadyFetchedCompanyCollection && !value && (_companyCollection != null))
				{
					_companyCollection.Clear();
				}
				_alreadyFetchedCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollection
		{
			get	{ return GetMultiPointOfInterestCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollection. When set to true, PointOfInterestCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollection
		{
			get	{ return _alwaysFetchPointOfInterestCollection; }
			set	{ _alwaysFetchPointOfInterestCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollection already has been fetched. Setting this property to false when PointOfInterestCollection has been fetched
		/// will clear the PointOfInterestCollection collection well. Setting this property to true while PointOfInterestCollection hasn't been fetched disables lazy loading for PointOfInterestCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollection
		{
			get { return _alreadyFetchedPointOfInterestCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollection && !value && (_pointOfInterestCollection != null))
				{
					_pointOfInterestCollection.Clear();
				}
				_alreadyFetchedPointOfInterestCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VattariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVattariffCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.VattariffCollection VattariffCollection
		{
			get	{ return GetMultiVattariffCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VattariffCollection. When set to true, VattariffCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VattariffCollection is accessed. You can always execute/ a forced fetch by calling GetMultiVattariffCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVattariffCollection
		{
			get	{ return _alwaysFetchVattariffCollection; }
			set	{ _alwaysFetchVattariffCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VattariffCollection already has been fetched. Setting this property to false when VattariffCollection has been fetched
		/// will clear the VattariffCollection collection well. Setting this property to true while VattariffCollection hasn't been fetched disables lazy loading for VattariffCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVattariffCollection
		{
			get { return _alreadyFetchedVattariffCollection;}
			set 
			{
				if(_alreadyFetchedVattariffCollection && !value && (_vattariffCollection != null))
				{
					_vattariffCollection.Clear();
				}
				_alreadyFetchedVattariffCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyOwnerCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyOwnerCollection CompanyOwnerCollectionViaCompany
		{
			get { return GetMultiCompanyOwnerCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyOwnerCollectionViaCompany. When set to true, CompanyOwnerCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyOwnerCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCompanyOwnerCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyOwnerCollectionViaCompany
		{
			get	{ return _alwaysFetchCompanyOwnerCollectionViaCompany; }
			set	{ _alwaysFetchCompanyOwnerCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyOwnerCollectionViaCompany already has been fetched. Setting this property to false when CompanyOwnerCollectionViaCompany has been fetched
		/// will clear the CompanyOwnerCollectionViaCompany collection well. Setting this property to true while CompanyOwnerCollectionViaCompany hasn't been fetched disables lazy loading for CompanyOwnerCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyOwnerCollectionViaCompany
		{
			get { return _alreadyFetchedCompanyOwnerCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCompanyOwnerCollectionViaCompany && !value && (_companyOwnerCollectionViaCompany != null))
				{
					_companyOwnerCollectionViaCompany.Clear();
				}
				_alreadyFetchedCompanyOwnerCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CurrencyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCurrencyCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CurrencyCollection CurrencyCollectionViaCompany
		{
			get { return GetMultiCurrencyCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyCollectionViaCompany. When set to true, CurrencyCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCurrencyCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyCollectionViaCompany
		{
			get	{ return _alwaysFetchCurrencyCollectionViaCompany; }
			set	{ _alwaysFetchCurrencyCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyCollectionViaCompany already has been fetched. Setting this property to false when CurrencyCollectionViaCompany has been fetched
		/// will clear the CurrencyCollectionViaCompany collection well. Setting this property to true while CurrencyCollectionViaCompany hasn't been fetched disables lazy loading for CurrencyCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyCollectionViaCompany
		{
			get { return _alreadyFetchedCurrencyCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCurrencyCollectionViaCompany && !value && (_currencyCollectionViaCompany != null))
				{
					_currencyCollectionViaCompany.Clear();
				}
				_alreadyFetchedCurrencyCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCompany
		{
			get { return GetMultiRouteCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCompany. When set to true, RouteCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCompany
		{
			get	{ return _alwaysFetchRouteCollectionViaCompany; }
			set	{ _alwaysFetchRouteCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCompany already has been fetched. Setting this property to false when RouteCollectionViaCompany has been fetched
		/// will clear the RouteCollectionViaCompany collection well. Setting this property to true while RouteCollectionViaCompany hasn't been fetched disables lazy loading for RouteCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCompany
		{
			get { return _alreadyFetchedRouteCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCompany && !value && (_routeCollectionViaCompany != null))
				{
					_routeCollectionViaCompany.Clear();
				}
				_alreadyFetchedRouteCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaCompany
		{
			get { return GetMultiSupportpoolCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaCompany. When set to true, SupportpoolCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaCompany
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaCompany; }
			set	{ _alwaysFetchSupportpoolCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaCompany already has been fetched. Setting this property to false when SupportpoolCollectionViaCompany has been fetched
		/// will clear the SupportpoolCollectionViaCompany collection well. Setting this property to true while SupportpoolCollectionViaCompany hasn't been fetched disables lazy loading for SupportpoolCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaCompany
		{
			get { return _alreadyFetchedSupportpoolCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaCompany && !value && (_supportpoolCollectionViaCompany != null))
				{
					_supportpoolCollectionViaCompany.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaCompany = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CurrencyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCurrencyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CurrencyEntity CurrencyEntity
		{
			get	{ return GetSingleCurrencyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCurrencyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CountryCollection", "CurrencyEntity", _currencyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CurrencyEntity. When set to true, CurrencyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CurrencyEntity is accessed. You can always execute a forced fetch by calling GetSingleCurrencyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCurrencyEntity
		{
			get	{ return _alwaysFetchCurrencyEntity; }
			set	{ _alwaysFetchCurrencyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CurrencyEntity already has been fetched. Setting this property to false when CurrencyEntity has been fetched
		/// will set CurrencyEntity to null as well. Setting this property to true while CurrencyEntity hasn't been fetched disables lazy loading for CurrencyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCurrencyEntity
		{
			get { return _alreadyFetchedCurrencyEntity;}
			set 
			{
				if(_alreadyFetchedCurrencyEntity && !value)
				{
					this.CurrencyEntity = null;
				}
				_alreadyFetchedCurrencyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CurrencyEntity is not found
		/// in the database. When set to true, CurrencyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CurrencyEntityReturnsNewIfNotFound
		{
			get	{ return _currencyEntityReturnsNewIfNotFound; }
			set { _currencyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CountryEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
