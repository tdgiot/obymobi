﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Netmessage'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class NetmessageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "NetmessageEntity"; }
		}
	
		#region Class Member Declarations
		private ClientEntity _receiverClientEntity;
		private bool	_alwaysFetchReceiverClientEntity, _alreadyFetchedReceiverClientEntity, _receiverClientEntityReturnsNewIfNotFound;
		private ClientEntity _senderClientEntity;
		private bool	_alwaysFetchSenderClientEntity, _alreadyFetchedSenderClientEntity, _senderClientEntityReturnsNewIfNotFound;
		private CompanyEntity _receiverCompanyEntity;
		private bool	_alwaysFetchReceiverCompanyEntity, _alreadyFetchedReceiverCompanyEntity, _receiverCompanyEntityReturnsNewIfNotFound;
		private CompanyEntity _senderCompanyEntity;
		private bool	_alwaysFetchSenderCompanyEntity, _alreadyFetchedSenderCompanyEntity, _senderCompanyEntityReturnsNewIfNotFound;
		private CustomerEntity _recieverCustomerEntity;
		private bool	_alwaysFetchRecieverCustomerEntity, _alreadyFetchedRecieverCustomerEntity, _recieverCustomerEntityReturnsNewIfNotFound;
		private CustomerEntity _senderCustomerEntity;
		private bool	_alwaysFetchSenderCustomerEntity, _alreadyFetchedSenderCustomerEntity, _senderCustomerEntityReturnsNewIfNotFound;
		private DeliverypointEntity _receiverDeliverypointEntity;
		private bool	_alwaysFetchReceiverDeliverypointEntity, _alreadyFetchedReceiverDeliverypointEntity, _receiverDeliverypointEntityReturnsNewIfNotFound;
		private DeliverypointEntity _senderDeliverypointEntity;
		private bool	_alwaysFetchSenderDeliverypointEntity, _alreadyFetchedSenderDeliverypointEntity, _senderDeliverypointEntityReturnsNewIfNotFound;
		private DeliverypointgroupEntity _deliverypointgroupEntity;
		private bool	_alwaysFetchDeliverypointgroupEntity, _alreadyFetchedDeliverypointgroupEntity, _deliverypointgroupEntityReturnsNewIfNotFound;
		private TerminalEntity _receiverTerminalEntity;
		private bool	_alwaysFetchReceiverTerminalEntity, _alreadyFetchedReceiverTerminalEntity, _receiverTerminalEntityReturnsNewIfNotFound;
		private TerminalEntity _senderTerminalEntity;
		private bool	_alwaysFetchSenderTerminalEntity, _alreadyFetchedSenderTerminalEntity, _senderTerminalEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ReceiverClientEntity</summary>
			public static readonly string ReceiverClientEntity = "ReceiverClientEntity";
			/// <summary>Member name SenderClientEntity</summary>
			public static readonly string SenderClientEntity = "SenderClientEntity";
			/// <summary>Member name ReceiverCompanyEntity</summary>
			public static readonly string ReceiverCompanyEntity = "ReceiverCompanyEntity";
			/// <summary>Member name SenderCompanyEntity</summary>
			public static readonly string SenderCompanyEntity = "SenderCompanyEntity";
			/// <summary>Member name RecieverCustomerEntity</summary>
			public static readonly string RecieverCustomerEntity = "RecieverCustomerEntity";
			/// <summary>Member name SenderCustomerEntity</summary>
			public static readonly string SenderCustomerEntity = "SenderCustomerEntity";
			/// <summary>Member name ReceiverDeliverypointEntity</summary>
			public static readonly string ReceiverDeliverypointEntity = "ReceiverDeliverypointEntity";
			/// <summary>Member name SenderDeliverypointEntity</summary>
			public static readonly string SenderDeliverypointEntity = "SenderDeliverypointEntity";
			/// <summary>Member name DeliverypointgroupEntity</summary>
			public static readonly string DeliverypointgroupEntity = "DeliverypointgroupEntity";
			/// <summary>Member name ReceiverTerminalEntity</summary>
			public static readonly string ReceiverTerminalEntity = "ReceiverTerminalEntity";
			/// <summary>Member name SenderTerminalEntity</summary>
			public static readonly string SenderTerminalEntity = "SenderTerminalEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NetmessageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected NetmessageEntityBase() :base("NetmessageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		protected NetmessageEntityBase(System.Int32 netmessageId):base("NetmessageEntity")
		{
			InitClassFetch(netmessageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected NetmessageEntityBase(System.Int32 netmessageId, IPrefetchPath prefetchPathToUse): base("NetmessageEntity")
		{
			InitClassFetch(netmessageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="validator">The custom validator object for this NetmessageEntity</param>
		protected NetmessageEntityBase(System.Int32 netmessageId, IValidator validator):base("NetmessageEntity")
		{
			InitClassFetch(netmessageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NetmessageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_receiverClientEntity = (ClientEntity)info.GetValue("_receiverClientEntity", typeof(ClientEntity));
			if(_receiverClientEntity!=null)
			{
				_receiverClientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiverClientEntityReturnsNewIfNotFound = info.GetBoolean("_receiverClientEntityReturnsNewIfNotFound");
			_alwaysFetchReceiverClientEntity = info.GetBoolean("_alwaysFetchReceiverClientEntity");
			_alreadyFetchedReceiverClientEntity = info.GetBoolean("_alreadyFetchedReceiverClientEntity");

			_senderClientEntity = (ClientEntity)info.GetValue("_senderClientEntity", typeof(ClientEntity));
			if(_senderClientEntity!=null)
			{
				_senderClientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_senderClientEntityReturnsNewIfNotFound = info.GetBoolean("_senderClientEntityReturnsNewIfNotFound");
			_alwaysFetchSenderClientEntity = info.GetBoolean("_alwaysFetchSenderClientEntity");
			_alreadyFetchedSenderClientEntity = info.GetBoolean("_alreadyFetchedSenderClientEntity");

			_receiverCompanyEntity = (CompanyEntity)info.GetValue("_receiverCompanyEntity", typeof(CompanyEntity));
			if(_receiverCompanyEntity!=null)
			{
				_receiverCompanyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiverCompanyEntityReturnsNewIfNotFound = info.GetBoolean("_receiverCompanyEntityReturnsNewIfNotFound");
			_alwaysFetchReceiverCompanyEntity = info.GetBoolean("_alwaysFetchReceiverCompanyEntity");
			_alreadyFetchedReceiverCompanyEntity = info.GetBoolean("_alreadyFetchedReceiverCompanyEntity");

			_senderCompanyEntity = (CompanyEntity)info.GetValue("_senderCompanyEntity", typeof(CompanyEntity));
			if(_senderCompanyEntity!=null)
			{
				_senderCompanyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_senderCompanyEntityReturnsNewIfNotFound = info.GetBoolean("_senderCompanyEntityReturnsNewIfNotFound");
			_alwaysFetchSenderCompanyEntity = info.GetBoolean("_alwaysFetchSenderCompanyEntity");
			_alreadyFetchedSenderCompanyEntity = info.GetBoolean("_alreadyFetchedSenderCompanyEntity");

			_recieverCustomerEntity = (CustomerEntity)info.GetValue("_recieverCustomerEntity", typeof(CustomerEntity));
			if(_recieverCustomerEntity!=null)
			{
				_recieverCustomerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_recieverCustomerEntityReturnsNewIfNotFound = info.GetBoolean("_recieverCustomerEntityReturnsNewIfNotFound");
			_alwaysFetchRecieverCustomerEntity = info.GetBoolean("_alwaysFetchRecieverCustomerEntity");
			_alreadyFetchedRecieverCustomerEntity = info.GetBoolean("_alreadyFetchedRecieverCustomerEntity");

			_senderCustomerEntity = (CustomerEntity)info.GetValue("_senderCustomerEntity", typeof(CustomerEntity));
			if(_senderCustomerEntity!=null)
			{
				_senderCustomerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_senderCustomerEntityReturnsNewIfNotFound = info.GetBoolean("_senderCustomerEntityReturnsNewIfNotFound");
			_alwaysFetchSenderCustomerEntity = info.GetBoolean("_alwaysFetchSenderCustomerEntity");
			_alreadyFetchedSenderCustomerEntity = info.GetBoolean("_alreadyFetchedSenderCustomerEntity");

			_receiverDeliverypointEntity = (DeliverypointEntity)info.GetValue("_receiverDeliverypointEntity", typeof(DeliverypointEntity));
			if(_receiverDeliverypointEntity!=null)
			{
				_receiverDeliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiverDeliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_receiverDeliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchReceiverDeliverypointEntity = info.GetBoolean("_alwaysFetchReceiverDeliverypointEntity");
			_alreadyFetchedReceiverDeliverypointEntity = info.GetBoolean("_alreadyFetchedReceiverDeliverypointEntity");

			_senderDeliverypointEntity = (DeliverypointEntity)info.GetValue("_senderDeliverypointEntity", typeof(DeliverypointEntity));
			if(_senderDeliverypointEntity!=null)
			{
				_senderDeliverypointEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_senderDeliverypointEntityReturnsNewIfNotFound = info.GetBoolean("_senderDeliverypointEntityReturnsNewIfNotFound");
			_alwaysFetchSenderDeliverypointEntity = info.GetBoolean("_alwaysFetchSenderDeliverypointEntity");
			_alreadyFetchedSenderDeliverypointEntity = info.GetBoolean("_alreadyFetchedSenderDeliverypointEntity");

			_deliverypointgroupEntity = (DeliverypointgroupEntity)info.GetValue("_deliverypointgroupEntity", typeof(DeliverypointgroupEntity));
			if(_deliverypointgroupEntity!=null)
			{
				_deliverypointgroupEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deliverypointgroupEntityReturnsNewIfNotFound = info.GetBoolean("_deliverypointgroupEntityReturnsNewIfNotFound");
			_alwaysFetchDeliverypointgroupEntity = info.GetBoolean("_alwaysFetchDeliverypointgroupEntity");
			_alreadyFetchedDeliverypointgroupEntity = info.GetBoolean("_alreadyFetchedDeliverypointgroupEntity");

			_receiverTerminalEntity = (TerminalEntity)info.GetValue("_receiverTerminalEntity", typeof(TerminalEntity));
			if(_receiverTerminalEntity!=null)
			{
				_receiverTerminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiverTerminalEntityReturnsNewIfNotFound = info.GetBoolean("_receiverTerminalEntityReturnsNewIfNotFound");
			_alwaysFetchReceiverTerminalEntity = info.GetBoolean("_alwaysFetchReceiverTerminalEntity");
			_alreadyFetchedReceiverTerminalEntity = info.GetBoolean("_alreadyFetchedReceiverTerminalEntity");

			_senderTerminalEntity = (TerminalEntity)info.GetValue("_senderTerminalEntity", typeof(TerminalEntity));
			if(_senderTerminalEntity!=null)
			{
				_senderTerminalEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_senderTerminalEntityReturnsNewIfNotFound = info.GetBoolean("_senderTerminalEntityReturnsNewIfNotFound");
			_alwaysFetchSenderTerminalEntity = info.GetBoolean("_alwaysFetchSenderTerminalEntity");
			_alreadyFetchedSenderTerminalEntity = info.GetBoolean("_alreadyFetchedSenderTerminalEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((NetmessageFieldIndex)fieldIndex)
			{
				case NetmessageFieldIndex.SenderCustomerId:
					DesetupSyncSenderCustomerEntity(true, false);
					_alreadyFetchedSenderCustomerEntity = false;
					break;
				case NetmessageFieldIndex.SenderClientId:
					DesetupSyncSenderClientEntity(true, false);
					_alreadyFetchedSenderClientEntity = false;
					break;
				case NetmessageFieldIndex.SenderCompanyId:
					DesetupSyncSenderCompanyEntity(true, false);
					_alreadyFetchedSenderCompanyEntity = false;
					break;
				case NetmessageFieldIndex.SenderDeliverypointId:
					DesetupSyncSenderDeliverypointEntity(true, false);
					_alreadyFetchedSenderDeliverypointEntity = false;
					break;
				case NetmessageFieldIndex.ReceiverCustomerId:
					DesetupSyncRecieverCustomerEntity(true, false);
					_alreadyFetchedRecieverCustomerEntity = false;
					break;
				case NetmessageFieldIndex.ReceiverClientId:
					DesetupSyncReceiverClientEntity(true, false);
					_alreadyFetchedReceiverClientEntity = false;
					break;
				case NetmessageFieldIndex.ReceiverCompanyId:
					DesetupSyncReceiverCompanyEntity(true, false);
					_alreadyFetchedReceiverCompanyEntity = false;
					break;
				case NetmessageFieldIndex.ReceiverDeliverypointId:
					DesetupSyncReceiverDeliverypointEntity(true, false);
					_alreadyFetchedReceiverDeliverypointEntity = false;
					break;
				case NetmessageFieldIndex.SenderTerminalId:
					DesetupSyncSenderTerminalEntity(true, false);
					_alreadyFetchedSenderTerminalEntity = false;
					break;
				case NetmessageFieldIndex.ReceiverTerminalId:
					DesetupSyncReceiverTerminalEntity(true, false);
					_alreadyFetchedReceiverTerminalEntity = false;
					break;
				case NetmessageFieldIndex.ReceiverDeliverypointgroupId:
					DesetupSyncDeliverypointgroupEntity(true, false);
					_alreadyFetchedDeliverypointgroupEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedReceiverClientEntity = (_receiverClientEntity != null);
			_alreadyFetchedSenderClientEntity = (_senderClientEntity != null);
			_alreadyFetchedReceiverCompanyEntity = (_receiverCompanyEntity != null);
			_alreadyFetchedSenderCompanyEntity = (_senderCompanyEntity != null);
			_alreadyFetchedRecieverCustomerEntity = (_recieverCustomerEntity != null);
			_alreadyFetchedSenderCustomerEntity = (_senderCustomerEntity != null);
			_alreadyFetchedReceiverDeliverypointEntity = (_receiverDeliverypointEntity != null);
			_alreadyFetchedSenderDeliverypointEntity = (_senderDeliverypointEntity != null);
			_alreadyFetchedDeliverypointgroupEntity = (_deliverypointgroupEntity != null);
			_alreadyFetchedReceiverTerminalEntity = (_receiverTerminalEntity != null);
			_alreadyFetchedSenderTerminalEntity = (_senderTerminalEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ReceiverClientEntity":
					toReturn.Add(Relations.ClientEntityUsingReceiverClientId);
					break;
				case "SenderClientEntity":
					toReturn.Add(Relations.ClientEntityUsingSenderClientId);
					break;
				case "ReceiverCompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingReceiverCompanyId);
					break;
				case "SenderCompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingSenderCompanyId);
					break;
				case "RecieverCustomerEntity":
					toReturn.Add(Relations.CustomerEntityUsingReceiverCustomerId);
					break;
				case "SenderCustomerEntity":
					toReturn.Add(Relations.CustomerEntityUsingSenderCustomerId);
					break;
				case "ReceiverDeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingReceiverDeliverypointId);
					break;
				case "SenderDeliverypointEntity":
					toReturn.Add(Relations.DeliverypointEntityUsingSenderDeliverypointId);
					break;
				case "DeliverypointgroupEntity":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId);
					break;
				case "ReceiverTerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingReceiverTerminalId);
					break;
				case "SenderTerminalEntity":
					toReturn.Add(Relations.TerminalEntityUsingSenderTerminalId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_receiverClientEntity", (!this.MarkedForDeletion?_receiverClientEntity:null));
			info.AddValue("_receiverClientEntityReturnsNewIfNotFound", _receiverClientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiverClientEntity", _alwaysFetchReceiverClientEntity);
			info.AddValue("_alreadyFetchedReceiverClientEntity", _alreadyFetchedReceiverClientEntity);
			info.AddValue("_senderClientEntity", (!this.MarkedForDeletion?_senderClientEntity:null));
			info.AddValue("_senderClientEntityReturnsNewIfNotFound", _senderClientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSenderClientEntity", _alwaysFetchSenderClientEntity);
			info.AddValue("_alreadyFetchedSenderClientEntity", _alreadyFetchedSenderClientEntity);
			info.AddValue("_receiverCompanyEntity", (!this.MarkedForDeletion?_receiverCompanyEntity:null));
			info.AddValue("_receiverCompanyEntityReturnsNewIfNotFound", _receiverCompanyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiverCompanyEntity", _alwaysFetchReceiverCompanyEntity);
			info.AddValue("_alreadyFetchedReceiverCompanyEntity", _alreadyFetchedReceiverCompanyEntity);
			info.AddValue("_senderCompanyEntity", (!this.MarkedForDeletion?_senderCompanyEntity:null));
			info.AddValue("_senderCompanyEntityReturnsNewIfNotFound", _senderCompanyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSenderCompanyEntity", _alwaysFetchSenderCompanyEntity);
			info.AddValue("_alreadyFetchedSenderCompanyEntity", _alreadyFetchedSenderCompanyEntity);
			info.AddValue("_recieverCustomerEntity", (!this.MarkedForDeletion?_recieverCustomerEntity:null));
			info.AddValue("_recieverCustomerEntityReturnsNewIfNotFound", _recieverCustomerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRecieverCustomerEntity", _alwaysFetchRecieverCustomerEntity);
			info.AddValue("_alreadyFetchedRecieverCustomerEntity", _alreadyFetchedRecieverCustomerEntity);
			info.AddValue("_senderCustomerEntity", (!this.MarkedForDeletion?_senderCustomerEntity:null));
			info.AddValue("_senderCustomerEntityReturnsNewIfNotFound", _senderCustomerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSenderCustomerEntity", _alwaysFetchSenderCustomerEntity);
			info.AddValue("_alreadyFetchedSenderCustomerEntity", _alreadyFetchedSenderCustomerEntity);
			info.AddValue("_receiverDeliverypointEntity", (!this.MarkedForDeletion?_receiverDeliverypointEntity:null));
			info.AddValue("_receiverDeliverypointEntityReturnsNewIfNotFound", _receiverDeliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiverDeliverypointEntity", _alwaysFetchReceiverDeliverypointEntity);
			info.AddValue("_alreadyFetchedReceiverDeliverypointEntity", _alreadyFetchedReceiverDeliverypointEntity);
			info.AddValue("_senderDeliverypointEntity", (!this.MarkedForDeletion?_senderDeliverypointEntity:null));
			info.AddValue("_senderDeliverypointEntityReturnsNewIfNotFound", _senderDeliverypointEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSenderDeliverypointEntity", _alwaysFetchSenderDeliverypointEntity);
			info.AddValue("_alreadyFetchedSenderDeliverypointEntity", _alreadyFetchedSenderDeliverypointEntity);
			info.AddValue("_deliverypointgroupEntity", (!this.MarkedForDeletion?_deliverypointgroupEntity:null));
			info.AddValue("_deliverypointgroupEntityReturnsNewIfNotFound", _deliverypointgroupEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeliverypointgroupEntity", _alwaysFetchDeliverypointgroupEntity);
			info.AddValue("_alreadyFetchedDeliverypointgroupEntity", _alreadyFetchedDeliverypointgroupEntity);
			info.AddValue("_receiverTerminalEntity", (!this.MarkedForDeletion?_receiverTerminalEntity:null));
			info.AddValue("_receiverTerminalEntityReturnsNewIfNotFound", _receiverTerminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiverTerminalEntity", _alwaysFetchReceiverTerminalEntity);
			info.AddValue("_alreadyFetchedReceiverTerminalEntity", _alreadyFetchedReceiverTerminalEntity);
			info.AddValue("_senderTerminalEntity", (!this.MarkedForDeletion?_senderTerminalEntity:null));
			info.AddValue("_senderTerminalEntityReturnsNewIfNotFound", _senderTerminalEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSenderTerminalEntity", _alwaysFetchSenderTerminalEntity);
			info.AddValue("_alreadyFetchedSenderTerminalEntity", _alreadyFetchedSenderTerminalEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ReceiverClientEntity":
					_alreadyFetchedReceiverClientEntity = true;
					this.ReceiverClientEntity = (ClientEntity)entity;
					break;
				case "SenderClientEntity":
					_alreadyFetchedSenderClientEntity = true;
					this.SenderClientEntity = (ClientEntity)entity;
					break;
				case "ReceiverCompanyEntity":
					_alreadyFetchedReceiverCompanyEntity = true;
					this.ReceiverCompanyEntity = (CompanyEntity)entity;
					break;
				case "SenderCompanyEntity":
					_alreadyFetchedSenderCompanyEntity = true;
					this.SenderCompanyEntity = (CompanyEntity)entity;
					break;
				case "RecieverCustomerEntity":
					_alreadyFetchedRecieverCustomerEntity = true;
					this.RecieverCustomerEntity = (CustomerEntity)entity;
					break;
				case "SenderCustomerEntity":
					_alreadyFetchedSenderCustomerEntity = true;
					this.SenderCustomerEntity = (CustomerEntity)entity;
					break;
				case "ReceiverDeliverypointEntity":
					_alreadyFetchedReceiverDeliverypointEntity = true;
					this.ReceiverDeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "SenderDeliverypointEntity":
					_alreadyFetchedSenderDeliverypointEntity = true;
					this.SenderDeliverypointEntity = (DeliverypointEntity)entity;
					break;
				case "DeliverypointgroupEntity":
					_alreadyFetchedDeliverypointgroupEntity = true;
					this.DeliverypointgroupEntity = (DeliverypointgroupEntity)entity;
					break;
				case "ReceiverTerminalEntity":
					_alreadyFetchedReceiverTerminalEntity = true;
					this.ReceiverTerminalEntity = (TerminalEntity)entity;
					break;
				case "SenderTerminalEntity":
					_alreadyFetchedSenderTerminalEntity = true;
					this.SenderTerminalEntity = (TerminalEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ReceiverClientEntity":
					SetupSyncReceiverClientEntity(relatedEntity);
					break;
				case "SenderClientEntity":
					SetupSyncSenderClientEntity(relatedEntity);
					break;
				case "ReceiverCompanyEntity":
					SetupSyncReceiverCompanyEntity(relatedEntity);
					break;
				case "SenderCompanyEntity":
					SetupSyncSenderCompanyEntity(relatedEntity);
					break;
				case "RecieverCustomerEntity":
					SetupSyncRecieverCustomerEntity(relatedEntity);
					break;
				case "SenderCustomerEntity":
					SetupSyncSenderCustomerEntity(relatedEntity);
					break;
				case "ReceiverDeliverypointEntity":
					SetupSyncReceiverDeliverypointEntity(relatedEntity);
					break;
				case "SenderDeliverypointEntity":
					SetupSyncSenderDeliverypointEntity(relatedEntity);
					break;
				case "DeliverypointgroupEntity":
					SetupSyncDeliverypointgroupEntity(relatedEntity);
					break;
				case "ReceiverTerminalEntity":
					SetupSyncReceiverTerminalEntity(relatedEntity);
					break;
				case "SenderTerminalEntity":
					SetupSyncSenderTerminalEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ReceiverClientEntity":
					DesetupSyncReceiverClientEntity(false, true);
					break;
				case "SenderClientEntity":
					DesetupSyncSenderClientEntity(false, true);
					break;
				case "ReceiverCompanyEntity":
					DesetupSyncReceiverCompanyEntity(false, true);
					break;
				case "SenderCompanyEntity":
					DesetupSyncSenderCompanyEntity(false, true);
					break;
				case "RecieverCustomerEntity":
					DesetupSyncRecieverCustomerEntity(false, true);
					break;
				case "SenderCustomerEntity":
					DesetupSyncSenderCustomerEntity(false, true);
					break;
				case "ReceiverDeliverypointEntity":
					DesetupSyncReceiverDeliverypointEntity(false, true);
					break;
				case "SenderDeliverypointEntity":
					DesetupSyncSenderDeliverypointEntity(false, true);
					break;
				case "DeliverypointgroupEntity":
					DesetupSyncDeliverypointgroupEntity(false, true);
					break;
				case "ReceiverTerminalEntity":
					DesetupSyncReceiverTerminalEntity(false, true);
					break;
				case "SenderTerminalEntity":
					DesetupSyncSenderTerminalEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_receiverClientEntity!=null)
			{
				toReturn.Add(_receiverClientEntity);
			}
			if(_senderClientEntity!=null)
			{
				toReturn.Add(_senderClientEntity);
			}
			if(_receiverCompanyEntity!=null)
			{
				toReturn.Add(_receiverCompanyEntity);
			}
			if(_senderCompanyEntity!=null)
			{
				toReturn.Add(_senderCompanyEntity);
			}
			if(_recieverCustomerEntity!=null)
			{
				toReturn.Add(_recieverCustomerEntity);
			}
			if(_senderCustomerEntity!=null)
			{
				toReturn.Add(_senderCustomerEntity);
			}
			if(_receiverDeliverypointEntity!=null)
			{
				toReturn.Add(_receiverDeliverypointEntity);
			}
			if(_senderDeliverypointEntity!=null)
			{
				toReturn.Add(_senderDeliverypointEntity);
			}
			if(_deliverypointgroupEntity!=null)
			{
				toReturn.Add(_deliverypointgroupEntity);
			}
			if(_receiverTerminalEntity!=null)
			{
				toReturn.Add(_receiverTerminalEntity);
			}
			if(_senderTerminalEntity!=null)
			{
				toReturn.Add(_senderTerminalEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 netmessageId)
		{
			return FetchUsingPK(netmessageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 netmessageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(netmessageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 netmessageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(netmessageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 netmessageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(netmessageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.NetmessageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NetmessageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleReceiverClientEntity()
		{
			return GetSingleReceiverClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleReceiverClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiverClientEntity || forceFetch || _alwaysFetchReceiverClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingReceiverClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiverClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiverClientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiverClientEntity = newEntity;
				_alreadyFetchedReceiverClientEntity = fetchResult;
			}
			return _receiverClientEntity;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleSenderClientEntity()
		{
			return GetSingleSenderClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleSenderClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSenderClientEntity || forceFetch || _alwaysFetchSenderClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingSenderClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SenderClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_senderClientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SenderClientEntity = newEntity;
				_alreadyFetchedSenderClientEntity = fetchResult;
			}
			return _senderClientEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleReceiverCompanyEntity()
		{
			return GetSingleReceiverCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleReceiverCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiverCompanyEntity || forceFetch || _alwaysFetchReceiverCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingReceiverCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiverCompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiverCompanyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiverCompanyEntity = newEntity;
				_alreadyFetchedReceiverCompanyEntity = fetchResult;
			}
			return _receiverCompanyEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleSenderCompanyEntity()
		{
			return GetSingleSenderCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleSenderCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSenderCompanyEntity || forceFetch || _alwaysFetchSenderCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingSenderCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SenderCompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_senderCompanyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SenderCompanyEntity = newEntity;
				_alreadyFetchedSenderCompanyEntity = fetchResult;
			}
			return _senderCompanyEntity;
		}


		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public CustomerEntity GetSingleRecieverCustomerEntity()
		{
			return GetSingleRecieverCustomerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public virtual CustomerEntity GetSingleRecieverCustomerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedRecieverCustomerEntity || forceFetch || _alwaysFetchRecieverCustomerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerEntityUsingReceiverCustomerId);
				CustomerEntity newEntity = new CustomerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiverCustomerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_recieverCustomerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RecieverCustomerEntity = newEntity;
				_alreadyFetchedRecieverCustomerEntity = fetchResult;
			}
			return _recieverCustomerEntity;
		}


		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public CustomerEntity GetSingleSenderCustomerEntity()
		{
			return GetSingleSenderCustomerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public virtual CustomerEntity GetSingleSenderCustomerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSenderCustomerEntity || forceFetch || _alwaysFetchSenderCustomerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerEntityUsingSenderCustomerId);
				CustomerEntity newEntity = new CustomerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SenderCustomerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_senderCustomerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SenderCustomerEntity = newEntity;
				_alreadyFetchedSenderCustomerEntity = fetchResult;
			}
			return _senderCustomerEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleReceiverDeliverypointEntity()
		{
			return GetSingleReceiverDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleReceiverDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiverDeliverypointEntity || forceFetch || _alwaysFetchReceiverDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingReceiverDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiverDeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiverDeliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiverDeliverypointEntity = newEntity;
				_alreadyFetchedReceiverDeliverypointEntity = fetchResult;
			}
			return _receiverDeliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public DeliverypointEntity GetSingleSenderDeliverypointEntity()
		{
			return GetSingleSenderDeliverypointEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointEntity' which is related to this entity.</returns>
		public virtual DeliverypointEntity GetSingleSenderDeliverypointEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSenderDeliverypointEntity || forceFetch || _alwaysFetchSenderDeliverypointEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointEntityUsingSenderDeliverypointId);
				DeliverypointEntity newEntity = new DeliverypointEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SenderDeliverypointId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_senderDeliverypointEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SenderDeliverypointEntity = newEntity;
				_alreadyFetchedSenderDeliverypointEntity = fetchResult;
			}
			return _senderDeliverypointEntity;
		}


		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public DeliverypointgroupEntity GetSingleDeliverypointgroupEntity()
		{
			return GetSingleDeliverypointgroupEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'DeliverypointgroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeliverypointgroupEntity' which is related to this entity.</returns>
		public virtual DeliverypointgroupEntity GetSingleDeliverypointgroupEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeliverypointgroupEntity || forceFetch || _alwaysFetchDeliverypointgroupEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupId);
				DeliverypointgroupEntity newEntity = new DeliverypointgroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiverDeliverypointgroupId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeliverypointgroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deliverypointgroupEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeliverypointgroupEntity = newEntity;
				_alreadyFetchedDeliverypointgroupEntity = fetchResult;
			}
			return _deliverypointgroupEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleReceiverTerminalEntity()
		{
			return GetSingleReceiverTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleReceiverTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiverTerminalEntity || forceFetch || _alwaysFetchReceiverTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingReceiverTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiverTerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiverTerminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiverTerminalEntity = newEntity;
				_alreadyFetchedReceiverTerminalEntity = fetchResult;
			}
			return _receiverTerminalEntity;
		}


		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public TerminalEntity GetSingleSenderTerminalEntity()
		{
			return GetSingleSenderTerminalEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TerminalEntity' which is related to this entity.</returns>
		public virtual TerminalEntity GetSingleSenderTerminalEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSenderTerminalEntity || forceFetch || _alwaysFetchSenderTerminalEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TerminalEntityUsingSenderTerminalId);
				TerminalEntity newEntity = new TerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SenderTerminalId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_senderTerminalEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SenderTerminalEntity = newEntity;
				_alreadyFetchedSenderTerminalEntity = fetchResult;
			}
			return _senderTerminalEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ReceiverClientEntity", _receiverClientEntity);
			toReturn.Add("SenderClientEntity", _senderClientEntity);
			toReturn.Add("ReceiverCompanyEntity", _receiverCompanyEntity);
			toReturn.Add("SenderCompanyEntity", _senderCompanyEntity);
			toReturn.Add("RecieverCustomerEntity", _recieverCustomerEntity);
			toReturn.Add("SenderCustomerEntity", _senderCustomerEntity);
			toReturn.Add("ReceiverDeliverypointEntity", _receiverDeliverypointEntity);
			toReturn.Add("SenderDeliverypointEntity", _senderDeliverypointEntity);
			toReturn.Add("DeliverypointgroupEntity", _deliverypointgroupEntity);
			toReturn.Add("ReceiverTerminalEntity", _receiverTerminalEntity);
			toReturn.Add("SenderTerminalEntity", _senderTerminalEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="validator">The validator object for this NetmessageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 netmessageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(netmessageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_receiverClientEntityReturnsNewIfNotFound = true;
			_senderClientEntityReturnsNewIfNotFound = true;
			_receiverCompanyEntityReturnsNewIfNotFound = true;
			_senderCompanyEntityReturnsNewIfNotFound = true;
			_recieverCustomerEntityReturnsNewIfNotFound = true;
			_senderCustomerEntityReturnsNewIfNotFound = true;
			_receiverDeliverypointEntityReturnsNewIfNotFound = true;
			_senderDeliverypointEntityReturnsNewIfNotFound = true;
			_deliverypointgroupEntityReturnsNewIfNotFound = true;
			_receiverTerminalEntityReturnsNewIfNotFound = true;
			_senderTerminalEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetmessageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SenderCustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SenderClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SenderCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SenderDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiverCustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiverClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiverCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiverDeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SenderTerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiverTerminalId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Guid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiverDeliverypointgroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageTypeText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorMessage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageLog", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SenderIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiverIdentifier", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _receiverClientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiverClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiverClientEntity, new PropertyChangedEventHandler( OnReceiverClientEntityPropertyChanged ), "ReceiverClientEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.ClientEntityUsingReceiverClientIdStatic, true, signalRelatedEntity, "ReceivedNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.ReceiverClientId } );		
			_receiverClientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _receiverClientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiverClientEntity(IEntityCore relatedEntity)
		{
			if(_receiverClientEntity!=relatedEntity)
			{		
				DesetupSyncReceiverClientEntity(true, true);
				_receiverClientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiverClientEntity, new PropertyChangedEventHandler( OnReceiverClientEntityPropertyChanged ), "ReceiverClientEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.ClientEntityUsingReceiverClientIdStatic, true, ref _alreadyFetchedReceiverClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiverClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _senderClientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSenderClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _senderClientEntity, new PropertyChangedEventHandler( OnSenderClientEntityPropertyChanged ), "SenderClientEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.ClientEntityUsingSenderClientIdStatic, true, signalRelatedEntity, "SentNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.SenderClientId } );		
			_senderClientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _senderClientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSenderClientEntity(IEntityCore relatedEntity)
		{
			if(_senderClientEntity!=relatedEntity)
			{		
				DesetupSyncSenderClientEntity(true, true);
				_senderClientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _senderClientEntity, new PropertyChangedEventHandler( OnSenderClientEntityPropertyChanged ), "SenderClientEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.ClientEntityUsingSenderClientIdStatic, true, ref _alreadyFetchedSenderClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSenderClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _receiverCompanyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiverCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiverCompanyEntity, new PropertyChangedEventHandler( OnReceiverCompanyEntityPropertyChanged ), "ReceiverCompanyEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CompanyEntityUsingReceiverCompanyIdStatic, true, signalRelatedEntity, "ReceivedNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.ReceiverCompanyId } );		
			_receiverCompanyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _receiverCompanyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiverCompanyEntity(IEntityCore relatedEntity)
		{
			if(_receiverCompanyEntity!=relatedEntity)
			{		
				DesetupSyncReceiverCompanyEntity(true, true);
				_receiverCompanyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiverCompanyEntity, new PropertyChangedEventHandler( OnReceiverCompanyEntityPropertyChanged ), "ReceiverCompanyEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CompanyEntityUsingReceiverCompanyIdStatic, true, ref _alreadyFetchedReceiverCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiverCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _senderCompanyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSenderCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _senderCompanyEntity, new PropertyChangedEventHandler( OnSenderCompanyEntityPropertyChanged ), "SenderCompanyEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CompanyEntityUsingSenderCompanyIdStatic, true, signalRelatedEntity, "SentNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.SenderCompanyId } );		
			_senderCompanyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _senderCompanyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSenderCompanyEntity(IEntityCore relatedEntity)
		{
			if(_senderCompanyEntity!=relatedEntity)
			{		
				DesetupSyncSenderCompanyEntity(true, true);
				_senderCompanyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _senderCompanyEntity, new PropertyChangedEventHandler( OnSenderCompanyEntityPropertyChanged ), "SenderCompanyEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CompanyEntityUsingSenderCompanyIdStatic, true, ref _alreadyFetchedSenderCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSenderCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _recieverCustomerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRecieverCustomerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _recieverCustomerEntity, new PropertyChangedEventHandler( OnRecieverCustomerEntityPropertyChanged ), "RecieverCustomerEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CustomerEntityUsingReceiverCustomerIdStatic, true, signalRelatedEntity, "ReceivedNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.ReceiverCustomerId } );		
			_recieverCustomerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _recieverCustomerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRecieverCustomerEntity(IEntityCore relatedEntity)
		{
			if(_recieverCustomerEntity!=relatedEntity)
			{		
				DesetupSyncRecieverCustomerEntity(true, true);
				_recieverCustomerEntity = (CustomerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _recieverCustomerEntity, new PropertyChangedEventHandler( OnRecieverCustomerEntityPropertyChanged ), "RecieverCustomerEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CustomerEntityUsingReceiverCustomerIdStatic, true, ref _alreadyFetchedRecieverCustomerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRecieverCustomerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _senderCustomerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSenderCustomerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _senderCustomerEntity, new PropertyChangedEventHandler( OnSenderCustomerEntityPropertyChanged ), "SenderCustomerEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CustomerEntityUsingSenderCustomerIdStatic, true, signalRelatedEntity, "SentNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.SenderCustomerId } );		
			_senderCustomerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _senderCustomerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSenderCustomerEntity(IEntityCore relatedEntity)
		{
			if(_senderCustomerEntity!=relatedEntity)
			{		
				DesetupSyncSenderCustomerEntity(true, true);
				_senderCustomerEntity = (CustomerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _senderCustomerEntity, new PropertyChangedEventHandler( OnSenderCustomerEntityPropertyChanged ), "SenderCustomerEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.CustomerEntityUsingSenderCustomerIdStatic, true, ref _alreadyFetchedSenderCustomerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSenderCustomerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _receiverDeliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiverDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiverDeliverypointEntity, new PropertyChangedEventHandler( OnReceiverDeliverypointEntityPropertyChanged ), "ReceiverDeliverypointEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.DeliverypointEntityUsingReceiverDeliverypointIdStatic, true, signalRelatedEntity, "ReceivedNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.ReceiverDeliverypointId } );		
			_receiverDeliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _receiverDeliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiverDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_receiverDeliverypointEntity!=relatedEntity)
			{		
				DesetupSyncReceiverDeliverypointEntity(true, true);
				_receiverDeliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiverDeliverypointEntity, new PropertyChangedEventHandler( OnReceiverDeliverypointEntityPropertyChanged ), "ReceiverDeliverypointEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.DeliverypointEntityUsingReceiverDeliverypointIdStatic, true, ref _alreadyFetchedReceiverDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiverDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _senderDeliverypointEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSenderDeliverypointEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _senderDeliverypointEntity, new PropertyChangedEventHandler( OnSenderDeliverypointEntityPropertyChanged ), "SenderDeliverypointEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.DeliverypointEntityUsingSenderDeliverypointIdStatic, true, signalRelatedEntity, "SentNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.SenderDeliverypointId } );		
			_senderDeliverypointEntity = null;
		}
		
		/// <summary> setups the sync logic for member _senderDeliverypointEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSenderDeliverypointEntity(IEntityCore relatedEntity)
		{
			if(_senderDeliverypointEntity!=relatedEntity)
			{		
				DesetupSyncSenderDeliverypointEntity(true, true);
				_senderDeliverypointEntity = (DeliverypointEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _senderDeliverypointEntity, new PropertyChangedEventHandler( OnSenderDeliverypointEntityPropertyChanged ), "SenderDeliverypointEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.DeliverypointEntityUsingSenderDeliverypointIdStatic, true, ref _alreadyFetchedSenderDeliverypointEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSenderDeliverypointEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeliverypointgroupEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupIdStatic, true, signalRelatedEntity, "NetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.ReceiverDeliverypointgroupId } );		
			_deliverypointgroupEntity = null;
		}
		
		/// <summary> setups the sync logic for member _deliverypointgroupEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeliverypointgroupEntity(IEntityCore relatedEntity)
		{
			if(_deliverypointgroupEntity!=relatedEntity)
			{		
				DesetupSyncDeliverypointgroupEntity(true, true);
				_deliverypointgroupEntity = (DeliverypointgroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deliverypointgroupEntity, new PropertyChangedEventHandler( OnDeliverypointgroupEntityPropertyChanged ), "DeliverypointgroupEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.DeliverypointgroupEntityUsingReceiverDeliverypointgroupIdStatic, true, ref _alreadyFetchedDeliverypointgroupEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeliverypointgroupEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _receiverTerminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiverTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiverTerminalEntity, new PropertyChangedEventHandler( OnReceiverTerminalEntityPropertyChanged ), "ReceiverTerminalEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.TerminalEntityUsingReceiverTerminalIdStatic, true, signalRelatedEntity, "ReceivedNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.ReceiverTerminalId } );		
			_receiverTerminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _receiverTerminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiverTerminalEntity(IEntityCore relatedEntity)
		{
			if(_receiverTerminalEntity!=relatedEntity)
			{		
				DesetupSyncReceiverTerminalEntity(true, true);
				_receiverTerminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiverTerminalEntity, new PropertyChangedEventHandler( OnReceiverTerminalEntityPropertyChanged ), "ReceiverTerminalEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.TerminalEntityUsingReceiverTerminalIdStatic, true, ref _alreadyFetchedReceiverTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiverTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _senderTerminalEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSenderTerminalEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _senderTerminalEntity, new PropertyChangedEventHandler( OnSenderTerminalEntityPropertyChanged ), "SenderTerminalEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.TerminalEntityUsingSenderTerminalIdStatic, true, signalRelatedEntity, "SentNetmessageCollection", resetFKFields, new int[] { (int)NetmessageFieldIndex.SenderTerminalId } );		
			_senderTerminalEntity = null;
		}
		
		/// <summary> setups the sync logic for member _senderTerminalEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSenderTerminalEntity(IEntityCore relatedEntity)
		{
			if(_senderTerminalEntity!=relatedEntity)
			{		
				DesetupSyncSenderTerminalEntity(true, true);
				_senderTerminalEntity = (TerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _senderTerminalEntity, new PropertyChangedEventHandler( OnSenderTerminalEntityPropertyChanged ), "SenderTerminalEntity", Obymobi.Data.RelationClasses.StaticNetmessageRelations.TerminalEntityUsingSenderTerminalIdStatic, true, ref _alreadyFetchedSenderTerminalEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSenderTerminalEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 netmessageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)NetmessageFieldIndex.NetmessageId].ForcedCurrentValueWrite(netmessageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateNetmessageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new NetmessageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NetmessageRelations Relations
		{
			get	{ return new NetmessageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiverClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ReceiverClientEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ReceiverClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSenderClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("SenderClientEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "SenderClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiverCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("ReceiverCompanyEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "ReceiverCompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSenderCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("SenderCompanyEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "SenderCompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRecieverCustomerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), (IEntityRelation)GetRelationsForField("RecieverCustomerEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, null, "RecieverCustomerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSenderCustomerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), (IEntityRelation)GetRelationsForField("SenderCustomerEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, null, "SenderCustomerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiverDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("ReceiverDeliverypointEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "ReceiverDeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSenderDeliverypointEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("SenderDeliverypointEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "SenderDeliverypointEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiverTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("ReceiverTerminalEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "ReceiverTerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSenderTerminalEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("SenderTerminalEntity")[0], (int)Obymobi.Data.EntityType.NetmessageEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "SenderTerminalEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The NetmessageId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."NetmessageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 NetmessageId
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.NetmessageId, true); }
			set	{ SetValue((int)NetmessageFieldIndex.NetmessageId, value, true); }
		}

		/// <summary> The SenderCustomerId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderCustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderCustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderCustomerId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderCustomerId, value, true); }
		}

		/// <summary> The SenderClientId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderClientId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderClientId, value, true); }
		}

		/// <summary> The SenderCompanyId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderCompanyId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderCompanyId, value, true); }
		}

		/// <summary> The SenderDeliverypointId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderDeliverypointId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderDeliverypointId, value, true); }
		}

		/// <summary> The ReceiverCustomerId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverCustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverCustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverCustomerId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverCustomerId, value, true); }
		}

		/// <summary> The ReceiverClientId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverClientId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverClientId, value, true); }
		}

		/// <summary> The ReceiverCompanyId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverCompanyId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverCompanyId, value, true); }
		}

		/// <summary> The ReceiverDeliverypointId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverDeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverDeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverDeliverypointId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverDeliverypointId, value, true); }
		}

		/// <summary> The MessageType property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."MessageType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessageType
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.MessageType, true); }
			set	{ SetValue((int)NetmessageFieldIndex.MessageType, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)NetmessageFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)NetmessageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)NetmessageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The SenderTerminalId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderTerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SenderTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.SenderTerminalId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderTerminalId, value, true); }
		}

		/// <summary> The ReceiverTerminalId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverTerminalId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverTerminalId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverTerminalId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverTerminalId, value, true); }
		}

		/// <summary> The Guid property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."Guid"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.Guid, true); }
			set	{ SetValue((int)NetmessageFieldIndex.Guid, value, true); }
		}

		/// <summary> The ReceiverDeliverypointgroupId property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverDeliverypointgroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReceiverDeliverypointgroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)NetmessageFieldIndex.ReceiverDeliverypointgroupId, false); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverDeliverypointgroupId, value, true); }
		}

		/// <summary> The Status property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."Status"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Status
		{
			get { return (System.Int32)GetValue((int)NetmessageFieldIndex.Status, true); }
			set	{ SetValue((int)NetmessageFieldIndex.Status, value, true); }
		}

		/// <summary> The MessageTypeText property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."MessageTypeText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessageTypeText
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.MessageTypeText, true); }
			set	{ SetValue((int)NetmessageFieldIndex.MessageTypeText, value, true); }
		}

		/// <summary> The StatusText property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."StatusText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StatusText
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.StatusText, true); }
			set	{ SetValue((int)NetmessageFieldIndex.StatusText, value, true); }
		}

		/// <summary> The ErrorMessage property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ErrorMessage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ErrorMessage
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.ErrorMessage, true); }
			set	{ SetValue((int)NetmessageFieldIndex.ErrorMessage, value, true); }
		}

		/// <summary> The MessageLog property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."MessageLog"<br/>
		/// Table field type characteristics (type, precision, scale, length): Text, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessageLog
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.MessageLog, true); }
			set	{ SetValue((int)NetmessageFieldIndex.MessageLog, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NetmessageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)NetmessageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NetmessageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)NetmessageFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The SenderIdentifier property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."SenderIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SenderIdentifier
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.SenderIdentifier, true); }
			set	{ SetValue((int)NetmessageFieldIndex.SenderIdentifier, value, true); }
		}

		/// <summary> The ReceiverIdentifier property of the Entity Netmessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Netmessage"."ReceiverIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiverIdentifier
		{
			get { return (System.String)GetValue((int)NetmessageFieldIndex.ReceiverIdentifier, true); }
			set	{ SetValue((int)NetmessageFieldIndex.ReceiverIdentifier, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiverClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ReceiverClientEntity
		{
			get	{ return GetSingleReceiverClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiverClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceivedNetmessageCollection", "ReceiverClientEntity", _receiverClientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiverClientEntity. When set to true, ReceiverClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiverClientEntity is accessed. You can always execute a forced fetch by calling GetSingleReceiverClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiverClientEntity
		{
			get	{ return _alwaysFetchReceiverClientEntity; }
			set	{ _alwaysFetchReceiverClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiverClientEntity already has been fetched. Setting this property to false when ReceiverClientEntity has been fetched
		/// will set ReceiverClientEntity to null as well. Setting this property to true while ReceiverClientEntity hasn't been fetched disables lazy loading for ReceiverClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiverClientEntity
		{
			get { return _alreadyFetchedReceiverClientEntity;}
			set 
			{
				if(_alreadyFetchedReceiverClientEntity && !value)
				{
					this.ReceiverClientEntity = null;
				}
				_alreadyFetchedReceiverClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiverClientEntity is not found
		/// in the database. When set to true, ReceiverClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReceiverClientEntityReturnsNewIfNotFound
		{
			get	{ return _receiverClientEntityReturnsNewIfNotFound; }
			set { _receiverClientEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSenderClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity SenderClientEntity
		{
			get	{ return GetSingleSenderClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSenderClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SentNetmessageCollection", "SenderClientEntity", _senderClientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SenderClientEntity. When set to true, SenderClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SenderClientEntity is accessed. You can always execute a forced fetch by calling GetSingleSenderClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSenderClientEntity
		{
			get	{ return _alwaysFetchSenderClientEntity; }
			set	{ _alwaysFetchSenderClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SenderClientEntity already has been fetched. Setting this property to false when SenderClientEntity has been fetched
		/// will set SenderClientEntity to null as well. Setting this property to true while SenderClientEntity hasn't been fetched disables lazy loading for SenderClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSenderClientEntity
		{
			get { return _alreadyFetchedSenderClientEntity;}
			set 
			{
				if(_alreadyFetchedSenderClientEntity && !value)
				{
					this.SenderClientEntity = null;
				}
				_alreadyFetchedSenderClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SenderClientEntity is not found
		/// in the database. When set to true, SenderClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SenderClientEntityReturnsNewIfNotFound
		{
			get	{ return _senderClientEntityReturnsNewIfNotFound; }
			set { _senderClientEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiverCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity ReceiverCompanyEntity
		{
			get	{ return GetSingleReceiverCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiverCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceivedNetmessageCollection", "ReceiverCompanyEntity", _receiverCompanyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiverCompanyEntity. When set to true, ReceiverCompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiverCompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleReceiverCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiverCompanyEntity
		{
			get	{ return _alwaysFetchReceiverCompanyEntity; }
			set	{ _alwaysFetchReceiverCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiverCompanyEntity already has been fetched. Setting this property to false when ReceiverCompanyEntity has been fetched
		/// will set ReceiverCompanyEntity to null as well. Setting this property to true while ReceiverCompanyEntity hasn't been fetched disables lazy loading for ReceiverCompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiverCompanyEntity
		{
			get { return _alreadyFetchedReceiverCompanyEntity;}
			set 
			{
				if(_alreadyFetchedReceiverCompanyEntity && !value)
				{
					this.ReceiverCompanyEntity = null;
				}
				_alreadyFetchedReceiverCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiverCompanyEntity is not found
		/// in the database. When set to true, ReceiverCompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReceiverCompanyEntityReturnsNewIfNotFound
		{
			get	{ return _receiverCompanyEntityReturnsNewIfNotFound; }
			set { _receiverCompanyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSenderCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity SenderCompanyEntity
		{
			get	{ return GetSingleSenderCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSenderCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SentNetmessageCollection", "SenderCompanyEntity", _senderCompanyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SenderCompanyEntity. When set to true, SenderCompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SenderCompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleSenderCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSenderCompanyEntity
		{
			get	{ return _alwaysFetchSenderCompanyEntity; }
			set	{ _alwaysFetchSenderCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SenderCompanyEntity already has been fetched. Setting this property to false when SenderCompanyEntity has been fetched
		/// will set SenderCompanyEntity to null as well. Setting this property to true while SenderCompanyEntity hasn't been fetched disables lazy loading for SenderCompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSenderCompanyEntity
		{
			get { return _alreadyFetchedSenderCompanyEntity;}
			set 
			{
				if(_alreadyFetchedSenderCompanyEntity && !value)
				{
					this.SenderCompanyEntity = null;
				}
				_alreadyFetchedSenderCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SenderCompanyEntity is not found
		/// in the database. When set to true, SenderCompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SenderCompanyEntityReturnsNewIfNotFound
		{
			get	{ return _senderCompanyEntityReturnsNewIfNotFound; }
			set { _senderCompanyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CustomerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRecieverCustomerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CustomerEntity RecieverCustomerEntity
		{
			get	{ return GetSingleRecieverCustomerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRecieverCustomerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceivedNetmessageCollection", "RecieverCustomerEntity", _recieverCustomerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RecieverCustomerEntity. When set to true, RecieverCustomerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RecieverCustomerEntity is accessed. You can always execute a forced fetch by calling GetSingleRecieverCustomerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRecieverCustomerEntity
		{
			get	{ return _alwaysFetchRecieverCustomerEntity; }
			set	{ _alwaysFetchRecieverCustomerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RecieverCustomerEntity already has been fetched. Setting this property to false when RecieverCustomerEntity has been fetched
		/// will set RecieverCustomerEntity to null as well. Setting this property to true while RecieverCustomerEntity hasn't been fetched disables lazy loading for RecieverCustomerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRecieverCustomerEntity
		{
			get { return _alreadyFetchedRecieverCustomerEntity;}
			set 
			{
				if(_alreadyFetchedRecieverCustomerEntity && !value)
				{
					this.RecieverCustomerEntity = null;
				}
				_alreadyFetchedRecieverCustomerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RecieverCustomerEntity is not found
		/// in the database. When set to true, RecieverCustomerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool RecieverCustomerEntityReturnsNewIfNotFound
		{
			get	{ return _recieverCustomerEntityReturnsNewIfNotFound; }
			set { _recieverCustomerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CustomerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSenderCustomerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CustomerEntity SenderCustomerEntity
		{
			get	{ return GetSingleSenderCustomerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSenderCustomerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SentNetmessageCollection", "SenderCustomerEntity", _senderCustomerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SenderCustomerEntity. When set to true, SenderCustomerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SenderCustomerEntity is accessed. You can always execute a forced fetch by calling GetSingleSenderCustomerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSenderCustomerEntity
		{
			get	{ return _alwaysFetchSenderCustomerEntity; }
			set	{ _alwaysFetchSenderCustomerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SenderCustomerEntity already has been fetched. Setting this property to false when SenderCustomerEntity has been fetched
		/// will set SenderCustomerEntity to null as well. Setting this property to true while SenderCustomerEntity hasn't been fetched disables lazy loading for SenderCustomerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSenderCustomerEntity
		{
			get { return _alreadyFetchedSenderCustomerEntity;}
			set 
			{
				if(_alreadyFetchedSenderCustomerEntity && !value)
				{
					this.SenderCustomerEntity = null;
				}
				_alreadyFetchedSenderCustomerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SenderCustomerEntity is not found
		/// in the database. When set to true, SenderCustomerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SenderCustomerEntityReturnsNewIfNotFound
		{
			get	{ return _senderCustomerEntityReturnsNewIfNotFound; }
			set { _senderCustomerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiverDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity ReceiverDeliverypointEntity
		{
			get	{ return GetSingleReceiverDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiverDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceivedNetmessageCollection", "ReceiverDeliverypointEntity", _receiverDeliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiverDeliverypointEntity. When set to true, ReceiverDeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiverDeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleReceiverDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiverDeliverypointEntity
		{
			get	{ return _alwaysFetchReceiverDeliverypointEntity; }
			set	{ _alwaysFetchReceiverDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiverDeliverypointEntity already has been fetched. Setting this property to false when ReceiverDeliverypointEntity has been fetched
		/// will set ReceiverDeliverypointEntity to null as well. Setting this property to true while ReceiverDeliverypointEntity hasn't been fetched disables lazy loading for ReceiverDeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiverDeliverypointEntity
		{
			get { return _alreadyFetchedReceiverDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedReceiverDeliverypointEntity && !value)
				{
					this.ReceiverDeliverypointEntity = null;
				}
				_alreadyFetchedReceiverDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiverDeliverypointEntity is not found
		/// in the database. When set to true, ReceiverDeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReceiverDeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _receiverDeliverypointEntityReturnsNewIfNotFound; }
			set { _receiverDeliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSenderDeliverypointEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointEntity SenderDeliverypointEntity
		{
			get	{ return GetSingleSenderDeliverypointEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSenderDeliverypointEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SentNetmessageCollection", "SenderDeliverypointEntity", _senderDeliverypointEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SenderDeliverypointEntity. When set to true, SenderDeliverypointEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SenderDeliverypointEntity is accessed. You can always execute a forced fetch by calling GetSingleSenderDeliverypointEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSenderDeliverypointEntity
		{
			get	{ return _alwaysFetchSenderDeliverypointEntity; }
			set	{ _alwaysFetchSenderDeliverypointEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SenderDeliverypointEntity already has been fetched. Setting this property to false when SenderDeliverypointEntity has been fetched
		/// will set SenderDeliverypointEntity to null as well. Setting this property to true while SenderDeliverypointEntity hasn't been fetched disables lazy loading for SenderDeliverypointEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSenderDeliverypointEntity
		{
			get { return _alreadyFetchedSenderDeliverypointEntity;}
			set 
			{
				if(_alreadyFetchedSenderDeliverypointEntity && !value)
				{
					this.SenderDeliverypointEntity = null;
				}
				_alreadyFetchedSenderDeliverypointEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SenderDeliverypointEntity is not found
		/// in the database. When set to true, SenderDeliverypointEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SenderDeliverypointEntityReturnsNewIfNotFound
		{
			get	{ return _senderDeliverypointEntityReturnsNewIfNotFound; }
			set { _senderDeliverypointEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeliverypointgroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeliverypointgroupEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual DeliverypointgroupEntity DeliverypointgroupEntity
		{
			get	{ return GetSingleDeliverypointgroupEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeliverypointgroupEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NetmessageCollection", "DeliverypointgroupEntity", _deliverypointgroupEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupEntity. When set to true, DeliverypointgroupEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupEntity is accessed. You can always execute a forced fetch by calling GetSingleDeliverypointgroupEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupEntity
		{
			get	{ return _alwaysFetchDeliverypointgroupEntity; }
			set	{ _alwaysFetchDeliverypointgroupEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupEntity already has been fetched. Setting this property to false when DeliverypointgroupEntity has been fetched
		/// will set DeliverypointgroupEntity to null as well. Setting this property to true while DeliverypointgroupEntity hasn't been fetched disables lazy loading for DeliverypointgroupEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupEntity
		{
			get { return _alreadyFetchedDeliverypointgroupEntity;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupEntity && !value)
				{
					this.DeliverypointgroupEntity = null;
				}
				_alreadyFetchedDeliverypointgroupEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeliverypointgroupEntity is not found
		/// in the database. When set to true, DeliverypointgroupEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool DeliverypointgroupEntityReturnsNewIfNotFound
		{
			get	{ return _deliverypointgroupEntityReturnsNewIfNotFound; }
			set { _deliverypointgroupEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiverTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity ReceiverTerminalEntity
		{
			get	{ return GetSingleReceiverTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiverTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReceivedNetmessageCollection", "ReceiverTerminalEntity", _receiverTerminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiverTerminalEntity. When set to true, ReceiverTerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiverTerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleReceiverTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiverTerminalEntity
		{
			get	{ return _alwaysFetchReceiverTerminalEntity; }
			set	{ _alwaysFetchReceiverTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiverTerminalEntity already has been fetched. Setting this property to false when ReceiverTerminalEntity has been fetched
		/// will set ReceiverTerminalEntity to null as well. Setting this property to true while ReceiverTerminalEntity hasn't been fetched disables lazy loading for ReceiverTerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiverTerminalEntity
		{
			get { return _alreadyFetchedReceiverTerminalEntity;}
			set 
			{
				if(_alreadyFetchedReceiverTerminalEntity && !value)
				{
					this.ReceiverTerminalEntity = null;
				}
				_alreadyFetchedReceiverTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiverTerminalEntity is not found
		/// in the database. When set to true, ReceiverTerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReceiverTerminalEntityReturnsNewIfNotFound
		{
			get	{ return _receiverTerminalEntityReturnsNewIfNotFound; }
			set { _receiverTerminalEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSenderTerminalEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TerminalEntity SenderTerminalEntity
		{
			get	{ return GetSingleSenderTerminalEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSenderTerminalEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SentNetmessageCollection", "SenderTerminalEntity", _senderTerminalEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SenderTerminalEntity. When set to true, SenderTerminalEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SenderTerminalEntity is accessed. You can always execute a forced fetch by calling GetSingleSenderTerminalEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSenderTerminalEntity
		{
			get	{ return _alwaysFetchSenderTerminalEntity; }
			set	{ _alwaysFetchSenderTerminalEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SenderTerminalEntity already has been fetched. Setting this property to false when SenderTerminalEntity has been fetched
		/// will set SenderTerminalEntity to null as well. Setting this property to true while SenderTerminalEntity hasn't been fetched disables lazy loading for SenderTerminalEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSenderTerminalEntity
		{
			get { return _alreadyFetchedSenderTerminalEntity;}
			set 
			{
				if(_alreadyFetchedSenderTerminalEntity && !value)
				{
					this.SenderTerminalEntity = null;
				}
				_alreadyFetchedSenderTerminalEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SenderTerminalEntity is not found
		/// in the database. When set to true, SenderTerminalEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SenderTerminalEntityReturnsNewIfNotFound
		{
			get	{ return _senderTerminalEntityReturnsNewIfNotFound; }
			set { _senderTerminalEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.NetmessageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
