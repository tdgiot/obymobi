﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'AlterationoptionTag'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AlterationoptionTagEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AlterationoptionTagEntity"; }
		}
	
		#region Class Member Declarations
		private AlterationoptionEntity _alterationoptionEntity;
		private bool	_alwaysFetchAlterationoptionEntity, _alreadyFetchedAlterationoptionEntity, _alterationoptionEntityReturnsNewIfNotFound;
		private TagEntity _tagEntity;
		private bool	_alwaysFetchTagEntity, _alreadyFetchedTagEntity, _tagEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AlterationoptionEntity</summary>
			public static readonly string AlterationoptionEntity = "AlterationoptionEntity";
			/// <summary>Member name TagEntity</summary>
			public static readonly string TagEntity = "TagEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AlterationoptionTagEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AlterationoptionTagEntityBase() :base("AlterationoptionTagEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		protected AlterationoptionTagEntityBase(System.Int32 alterationoptionTagId):base("AlterationoptionTagEntity")
		{
			InitClassFetch(alterationoptionTagId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AlterationoptionTagEntityBase(System.Int32 alterationoptionTagId, IPrefetchPath prefetchPathToUse): base("AlterationoptionTagEntity")
		{
			InitClassFetch(alterationoptionTagId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="validator">The custom validator object for this AlterationoptionTagEntity</param>
		protected AlterationoptionTagEntityBase(System.Int32 alterationoptionTagId, IValidator validator):base("AlterationoptionTagEntity")
		{
			InitClassFetch(alterationoptionTagId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationoptionTagEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_alterationoptionEntity = (AlterationoptionEntity)info.GetValue("_alterationoptionEntity", typeof(AlterationoptionEntity));
			if(_alterationoptionEntity!=null)
			{
				_alterationoptionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alterationoptionEntityReturnsNewIfNotFound = info.GetBoolean("_alterationoptionEntityReturnsNewIfNotFound");
			_alwaysFetchAlterationoptionEntity = info.GetBoolean("_alwaysFetchAlterationoptionEntity");
			_alreadyFetchedAlterationoptionEntity = info.GetBoolean("_alreadyFetchedAlterationoptionEntity");

			_tagEntity = (TagEntity)info.GetValue("_tagEntity", typeof(TagEntity));
			if(_tagEntity!=null)
			{
				_tagEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tagEntityReturnsNewIfNotFound = info.GetBoolean("_tagEntityReturnsNewIfNotFound");
			_alwaysFetchTagEntity = info.GetBoolean("_alwaysFetchTagEntity");
			_alreadyFetchedTagEntity = info.GetBoolean("_alreadyFetchedTagEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AlterationoptionTagFieldIndex)fieldIndex)
			{
				case AlterationoptionTagFieldIndex.AlterationoptionId:
					DesetupSyncAlterationoptionEntity(true, false);
					_alreadyFetchedAlterationoptionEntity = false;
					break;
				case AlterationoptionTagFieldIndex.TagId:
					DesetupSyncTagEntity(true, false);
					_alreadyFetchedTagEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAlterationoptionEntity = (_alterationoptionEntity != null);
			_alreadyFetchedTagEntity = (_tagEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AlterationoptionEntity":
					toReturn.Add(Relations.AlterationoptionEntityUsingAlterationoptionId);
					break;
				case "TagEntity":
					toReturn.Add(Relations.TagEntityUsingTagId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_alterationoptionEntity", (!this.MarkedForDeletion?_alterationoptionEntity:null));
			info.AddValue("_alterationoptionEntityReturnsNewIfNotFound", _alterationoptionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlterationoptionEntity", _alwaysFetchAlterationoptionEntity);
			info.AddValue("_alreadyFetchedAlterationoptionEntity", _alreadyFetchedAlterationoptionEntity);
			info.AddValue("_tagEntity", (!this.MarkedForDeletion?_tagEntity:null));
			info.AddValue("_tagEntityReturnsNewIfNotFound", _tagEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTagEntity", _alwaysFetchTagEntity);
			info.AddValue("_alreadyFetchedTagEntity", _alreadyFetchedTagEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AlterationoptionEntity":
					_alreadyFetchedAlterationoptionEntity = true;
					this.AlterationoptionEntity = (AlterationoptionEntity)entity;
					break;
				case "TagEntity":
					_alreadyFetchedTagEntity = true;
					this.TagEntity = (TagEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AlterationoptionEntity":
					SetupSyncAlterationoptionEntity(relatedEntity);
					break;
				case "TagEntity":
					SetupSyncTagEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AlterationoptionEntity":
					DesetupSyncAlterationoptionEntity(false, true);
					break;
				case "TagEntity":
					DesetupSyncTagEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_alterationoptionEntity!=null)
			{
				toReturn.Add(_alterationoptionEntity);
			}
			if(_tagEntity!=null)
			{
				toReturn.Add(_tagEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionTagId)
		{
			return FetchUsingPK(alterationoptionTagId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionTagId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(alterationoptionTagId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionTagId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(alterationoptionTagId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 alterationoptionTagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(alterationoptionTagId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AlterationoptionTagId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AlterationoptionTagRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public AlterationoptionEntity GetSingleAlterationoptionEntity()
		{
			return GetSingleAlterationoptionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AlterationoptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AlterationoptionEntity' which is related to this entity.</returns>
		public virtual AlterationoptionEntity GetSingleAlterationoptionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlterationoptionEntity || forceFetch || _alwaysFetchAlterationoptionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AlterationoptionEntityUsingAlterationoptionId);
				AlterationoptionEntity newEntity = new AlterationoptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlterationoptionId);
				}
				if(fetchResult)
				{
					newEntity = (AlterationoptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alterationoptionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlterationoptionEntity = newEntity;
				_alreadyFetchedAlterationoptionEntity = fetchResult;
			}
			return _alterationoptionEntity;
		}


		/// <summary> Retrieves the related entity of type 'TagEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TagEntity' which is related to this entity.</returns>
		public TagEntity GetSingleTagEntity()
		{
			return GetSingleTagEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TagEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TagEntity' which is related to this entity.</returns>
		public virtual TagEntity GetSingleTagEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTagEntity || forceFetch || _alwaysFetchTagEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TagEntityUsingTagId);
				TagEntity newEntity = new TagEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TagId);
				}
				if(fetchResult)
				{
					newEntity = (TagEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tagEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TagEntity = newEntity;
				_alreadyFetchedTagEntity = fetchResult;
			}
			return _tagEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AlterationoptionEntity", _alterationoptionEntity);
			toReturn.Add("TagEntity", _tagEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="validator">The validator object for this AlterationoptionTagEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 alterationoptionTagId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(alterationoptionTagId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_alterationoptionEntityReturnsNewIfNotFound = true;
			_tagEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionTagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationoptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _alterationoptionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlterationoptionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionTagRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, signalRelatedEntity, "AlterationoptionTagCollection", resetFKFields, new int[] { (int)AlterationoptionTagFieldIndex.AlterationoptionId } );		
			_alterationoptionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _alterationoptionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlterationoptionEntity(IEntityCore relatedEntity)
		{
			if(_alterationoptionEntity!=relatedEntity)
			{		
				DesetupSyncAlterationoptionEntity(true, true);
				_alterationoptionEntity = (AlterationoptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alterationoptionEntity, new PropertyChangedEventHandler( OnAlterationoptionEntityPropertyChanged ), "AlterationoptionEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionTagRelations.AlterationoptionEntityUsingAlterationoptionIdStatic, true, ref _alreadyFetchedAlterationoptionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlterationoptionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tagEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTagEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tagEntity, new PropertyChangedEventHandler( OnTagEntityPropertyChanged ), "TagEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionTagRelations.TagEntityUsingTagIdStatic, true, signalRelatedEntity, "AlterationoptionTagCollection", resetFKFields, new int[] { (int)AlterationoptionTagFieldIndex.TagId } );		
			_tagEntity = null;
		}
		
		/// <summary> setups the sync logic for member _tagEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTagEntity(IEntityCore relatedEntity)
		{
			if(_tagEntity!=relatedEntity)
			{		
				DesetupSyncTagEntity(true, true);
				_tagEntity = (TagEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tagEntity, new PropertyChangedEventHandler( OnTagEntityPropertyChanged ), "TagEntity", Obymobi.Data.RelationClasses.StaticAlterationoptionTagRelations.TagEntityUsingTagIdStatic, true, ref _alreadyFetchedTagEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTagEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="alterationoptionTagId">PK value for AlterationoptionTag which data should be fetched into this AlterationoptionTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 alterationoptionTagId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AlterationoptionTagFieldIndex.AlterationoptionTagId].ForcedCurrentValueWrite(alterationoptionTagId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAlterationoptionTagDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AlterationoptionTagEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AlterationoptionTagRelations Relations
		{
			get	{ return new AlterationoptionTagRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Alterationoption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlterationoptionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AlterationoptionCollection(), (IEntityRelation)GetRelationsForField("AlterationoptionEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionTagEntity, (int)Obymobi.Data.EntityType.AlterationoptionEntity, 0, null, null, null, "AlterationoptionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tag'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTagEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TagCollection(), (IEntityRelation)GetRelationsForField("TagEntity")[0], (int)Obymobi.Data.EntityType.AlterationoptionTagEntity, (int)Obymobi.Data.EntityType.TagEntity, 0, null, null, null, "TagEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlterationoptionTagId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."AlterationoptionTagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AlterationoptionTagId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionTagFieldIndex.AlterationoptionTagId, true); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.AlterationoptionTagId, value, true); }
		}

		/// <summary> The AlterationoptionId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."AlterationoptionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationoptionId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionTagFieldIndex.AlterationoptionId, true); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.AlterationoptionId, value, true); }
		}

		/// <summary> The TagId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."TagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TagId
		{
			get { return (System.Int32)GetValue((int)AlterationoptionTagFieldIndex.TagId, true); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.TagId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedUTC
		{
			get { return (System.DateTime)GetValue((int)AlterationoptionTagFieldIndex.CreatedUTC, true); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AlterationoptionTagFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity AlterationoptionTag<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AlterationoptionTag"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AlterationoptionTagFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AlterationoptionTagFieldIndex.ParentCompanyId, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AlterationoptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlterationoptionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AlterationoptionEntity AlterationoptionEntity
		{
			get	{ return GetSingleAlterationoptionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAlterationoptionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionTagCollection", "AlterationoptionEntity", _alterationoptionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlterationoptionEntity. When set to true, AlterationoptionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlterationoptionEntity is accessed. You can always execute a forced fetch by calling GetSingleAlterationoptionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlterationoptionEntity
		{
			get	{ return _alwaysFetchAlterationoptionEntity; }
			set	{ _alwaysFetchAlterationoptionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AlterationoptionEntity already has been fetched. Setting this property to false when AlterationoptionEntity has been fetched
		/// will set AlterationoptionEntity to null as well. Setting this property to true while AlterationoptionEntity hasn't been fetched disables lazy loading for AlterationoptionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlterationoptionEntity
		{
			get { return _alreadyFetchedAlterationoptionEntity;}
			set 
			{
				if(_alreadyFetchedAlterationoptionEntity && !value)
				{
					this.AlterationoptionEntity = null;
				}
				_alreadyFetchedAlterationoptionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlterationoptionEntity is not found
		/// in the database. When set to true, AlterationoptionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AlterationoptionEntityReturnsNewIfNotFound
		{
			get	{ return _alterationoptionEntityReturnsNewIfNotFound; }
			set { _alterationoptionEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TagEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTagEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TagEntity TagEntity
		{
			get	{ return GetSingleTagEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTagEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AlterationoptionTagCollection", "TagEntity", _tagEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TagEntity. When set to true, TagEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TagEntity is accessed. You can always execute a forced fetch by calling GetSingleTagEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTagEntity
		{
			get	{ return _alwaysFetchTagEntity; }
			set	{ _alwaysFetchTagEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TagEntity already has been fetched. Setting this property to false when TagEntity has been fetched
		/// will set TagEntity to null as well. Setting this property to true while TagEntity hasn't been fetched disables lazy loading for TagEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTagEntity
		{
			get { return _alreadyFetchedTagEntity;}
			set 
			{
				if(_alreadyFetchedTagEntity && !value)
				{
					this.TagEntity = null;
				}
				_alreadyFetchedTagEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TagEntity is not found
		/// in the database. When set to true, TagEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TagEntityReturnsNewIfNotFound
		{
			get	{ return _tagEntityReturnsNewIfNotFound; }
			set { _tagEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AlterationoptionTagEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
