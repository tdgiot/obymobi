﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'EntertainmentcategoryLanguage'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class EntertainmentcategoryLanguageEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "EntertainmentcategoryLanguageEntity"; }
		}
	
		#region Class Member Declarations
		private EntertainmentcategoryEntity _entertainmentcategoryEntity;
		private bool	_alwaysFetchEntertainmentcategoryEntity, _alreadyFetchedEntertainmentcategoryEntity, _entertainmentcategoryEntityReturnsNewIfNotFound;
		private LanguageEntity _languageEntity;
		private bool	_alwaysFetchLanguageEntity, _alreadyFetchedLanguageEntity, _languageEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EntertainmentcategoryEntity</summary>
			public static readonly string EntertainmentcategoryEntity = "EntertainmentcategoryEntity";
			/// <summary>Member name LanguageEntity</summary>
			public static readonly string LanguageEntity = "LanguageEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EntertainmentcategoryLanguageEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected EntertainmentcategoryLanguageEntityBase() :base("EntertainmentcategoryLanguageEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		protected EntertainmentcategoryLanguageEntityBase(System.Int32 entertainmentcategoryLanguageId):base("EntertainmentcategoryLanguageEntity")
		{
			InitClassFetch(entertainmentcategoryLanguageId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected EntertainmentcategoryLanguageEntityBase(System.Int32 entertainmentcategoryLanguageId, IPrefetchPath prefetchPathToUse): base("EntertainmentcategoryLanguageEntity")
		{
			InitClassFetch(entertainmentcategoryLanguageId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <param name="validator">The custom validator object for this EntertainmentcategoryLanguageEntity</param>
		protected EntertainmentcategoryLanguageEntityBase(System.Int32 entertainmentcategoryLanguageId, IValidator validator):base("EntertainmentcategoryLanguageEntity")
		{
			InitClassFetch(entertainmentcategoryLanguageId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntertainmentcategoryLanguageEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_entertainmentcategoryEntity = (EntertainmentcategoryEntity)info.GetValue("_entertainmentcategoryEntity", typeof(EntertainmentcategoryEntity));
			if(_entertainmentcategoryEntity!=null)
			{
				_entertainmentcategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentcategoryEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentcategoryEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentcategoryEntity = info.GetBoolean("_alwaysFetchEntertainmentcategoryEntity");
			_alreadyFetchedEntertainmentcategoryEntity = info.GetBoolean("_alreadyFetchedEntertainmentcategoryEntity");

			_languageEntity = (LanguageEntity)info.GetValue("_languageEntity", typeof(LanguageEntity));
			if(_languageEntity!=null)
			{
				_languageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_languageEntityReturnsNewIfNotFound = info.GetBoolean("_languageEntityReturnsNewIfNotFound");
			_alwaysFetchLanguageEntity = info.GetBoolean("_alwaysFetchLanguageEntity");
			_alreadyFetchedLanguageEntity = info.GetBoolean("_alreadyFetchedLanguageEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((EntertainmentcategoryLanguageFieldIndex)fieldIndex)
			{
				case EntertainmentcategoryLanguageFieldIndex.EntertainmentcategoryId:
					DesetupSyncEntertainmentcategoryEntity(true, false);
					_alreadyFetchedEntertainmentcategoryEntity = false;
					break;
				case EntertainmentcategoryLanguageFieldIndex.LanguageId:
					DesetupSyncLanguageEntity(true, false);
					_alreadyFetchedLanguageEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedEntertainmentcategoryEntity = (_entertainmentcategoryEntity != null);
			_alreadyFetchedLanguageEntity = (_languageEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EntertainmentcategoryEntity":
					toReturn.Add(Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
					break;
				case "LanguageEntity":
					toReturn.Add(Relations.LanguageEntityUsingLanguageId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_entertainmentcategoryEntity", (!this.MarkedForDeletion?_entertainmentcategoryEntity:null));
			info.AddValue("_entertainmentcategoryEntityReturnsNewIfNotFound", _entertainmentcategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentcategoryEntity", _alwaysFetchEntertainmentcategoryEntity);
			info.AddValue("_alreadyFetchedEntertainmentcategoryEntity", _alreadyFetchedEntertainmentcategoryEntity);
			info.AddValue("_languageEntity", (!this.MarkedForDeletion?_languageEntity:null));
			info.AddValue("_languageEntityReturnsNewIfNotFound", _languageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLanguageEntity", _alwaysFetchLanguageEntity);
			info.AddValue("_alreadyFetchedLanguageEntity", _alreadyFetchedLanguageEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EntertainmentcategoryEntity":
					_alreadyFetchedEntertainmentcategoryEntity = true;
					this.EntertainmentcategoryEntity = (EntertainmentcategoryEntity)entity;
					break;
				case "LanguageEntity":
					_alreadyFetchedLanguageEntity = true;
					this.LanguageEntity = (LanguageEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EntertainmentcategoryEntity":
					SetupSyncEntertainmentcategoryEntity(relatedEntity);
					break;
				case "LanguageEntity":
					SetupSyncLanguageEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EntertainmentcategoryEntity":
					DesetupSyncEntertainmentcategoryEntity(false, true);
					break;
				case "LanguageEntity":
					DesetupSyncLanguageEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_entertainmentcategoryEntity!=null)
			{
				toReturn.Add(_entertainmentcategoryEntity);
			}
			if(_languageEntity!=null)
			{
				toReturn.Add(_languageEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryLanguageId)
		{
			return FetchUsingPK(entertainmentcategoryLanguageId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryLanguageId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entertainmentcategoryLanguageId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entertainmentcategoryLanguageId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentcategoryLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entertainmentcategoryLanguageId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntertainmentcategoryLanguageId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EntertainmentcategoryLanguageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity()
		{
			return GetSingleEntertainmentcategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentcategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentcategoryEntity' which is related to this entity.</returns>
		public virtual EntertainmentcategoryEntity GetSingleEntertainmentcategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentcategoryEntity || forceFetch || _alwaysFetchEntertainmentcategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
				EntertainmentcategoryEntity newEntity = new EntertainmentcategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentcategoryId);
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentcategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentcategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentcategoryEntity = newEntity;
				_alreadyFetchedEntertainmentcategoryEntity = fetchResult;
			}
			return _entertainmentcategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public LanguageEntity GetSingleLanguageEntity()
		{
			return GetSingleLanguageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'LanguageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LanguageEntity' which is related to this entity.</returns>
		public virtual LanguageEntity GetSingleLanguageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedLanguageEntity || forceFetch || _alwaysFetchLanguageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LanguageEntityUsingLanguageId);
				LanguageEntity newEntity = new LanguageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LanguageId);
				}
				if(fetchResult)
				{
					newEntity = (LanguageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_languageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.LanguageEntity = newEntity;
				_alreadyFetchedLanguageEntity = fetchResult;
			}
			return _languageEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EntertainmentcategoryEntity", _entertainmentcategoryEntity);
			toReturn.Add("LanguageEntity", _languageEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <param name="validator">The validator object for this EntertainmentcategoryLanguageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 entertainmentcategoryLanguageId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entertainmentcategoryLanguageId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_entertainmentcategoryEntityReturnsNewIfNotFound = true;
			_languageEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentcategoryLanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentcategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentcategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticEntertainmentcategoryLanguageRelations.EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic, true, signalRelatedEntity, "EntertainmentcategoryLanguageCollection", resetFKFields, new int[] { (int)EntertainmentcategoryLanguageFieldIndex.EntertainmentcategoryId } );		
			_entertainmentcategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentcategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentcategoryEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentcategoryEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentcategoryEntity(true, true);
				_entertainmentcategoryEntity = (EntertainmentcategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentcategoryEntity, new PropertyChangedEventHandler( OnEntertainmentcategoryEntityPropertyChanged ), "EntertainmentcategoryEntity", Obymobi.Data.RelationClasses.StaticEntertainmentcategoryLanguageRelations.EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic, true, ref _alreadyFetchedEntertainmentcategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentcategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _languageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLanguageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticEntertainmentcategoryLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, signalRelatedEntity, "EntertainmentcategoryLanguageCollection", resetFKFields, new int[] { (int)EntertainmentcategoryLanguageFieldIndex.LanguageId } );		
			_languageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _languageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLanguageEntity(IEntityCore relatedEntity)
		{
			if(_languageEntity!=relatedEntity)
			{		
				DesetupSyncLanguageEntity(true, true);
				_languageEntity = (LanguageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _languageEntity, new PropertyChangedEventHandler( OnLanguageEntityPropertyChanged ), "LanguageEntity", Obymobi.Data.RelationClasses.StaticEntertainmentcategoryLanguageRelations.LanguageEntityUsingLanguageIdStatic, true, ref _alreadyFetchedLanguageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLanguageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entertainmentcategoryLanguageId">PK value for EntertainmentcategoryLanguage which data should be fetched into this EntertainmentcategoryLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 entertainmentcategoryLanguageId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EntertainmentcategoryLanguageFieldIndex.EntertainmentcategoryLanguageId].ForcedCurrentValueWrite(entertainmentcategoryLanguageId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEntertainmentcategoryLanguageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EntertainmentcategoryLanguageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EntertainmentcategoryLanguageRelations Relations
		{
			get	{ return new EntertainmentcategoryLanguageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), (IEntityRelation)GetRelationsForField("EntertainmentcategoryEntity")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, null, "EntertainmentcategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), (IEntityRelation)GetRelationsForField("LanguageEntity")[0], (int)Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, null, "LanguageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EntertainmentcategoryLanguageId property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."EntertainmentcategoryLanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 EntertainmentcategoryLanguageId
		{
			get { return (System.Int32)GetValue((int)EntertainmentcategoryLanguageFieldIndex.EntertainmentcategoryLanguageId, true); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.EntertainmentcategoryLanguageId, value, true); }
		}

		/// <summary> The EntertainmentcategoryId property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."EntertainmentcategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EntertainmentcategoryId
		{
			get { return (System.Int32)GetValue((int)EntertainmentcategoryLanguageFieldIndex.EntertainmentcategoryId, true); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.EntertainmentcategoryId, value, true); }
		}

		/// <summary> The LanguageId property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."LanguageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LanguageId
		{
			get { return (System.Int32)GetValue((int)EntertainmentcategoryLanguageFieldIndex.LanguageId, true); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.LanguageId, value, true); }
		}

		/// <summary> The Name property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)EntertainmentcategoryLanguageFieldIndex.Name, true); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.Name, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)EntertainmentcategoryLanguageFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)EntertainmentcategoryLanguageFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentcategoryLanguageFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity EntertainmentcategoryLanguage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentcategoryLanguage"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentcategoryLanguageFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)EntertainmentcategoryLanguageFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'EntertainmentcategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentcategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentcategoryEntity EntertainmentcategoryEntity
		{
			get	{ return GetSingleEntertainmentcategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentcategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EntertainmentcategoryLanguageCollection", "EntertainmentcategoryEntity", _entertainmentcategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryEntity. When set to true, EntertainmentcategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentcategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryEntity
		{
			get	{ return _alwaysFetchEntertainmentcategoryEntity; }
			set	{ _alwaysFetchEntertainmentcategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryEntity already has been fetched. Setting this property to false when EntertainmentcategoryEntity has been fetched
		/// will set EntertainmentcategoryEntity to null as well. Setting this property to true while EntertainmentcategoryEntity hasn't been fetched disables lazy loading for EntertainmentcategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryEntity
		{
			get { return _alreadyFetchedEntertainmentcategoryEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryEntity && !value)
				{
					this.EntertainmentcategoryEntity = null;
				}
				_alreadyFetchedEntertainmentcategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentcategoryEntity is not found
		/// in the database. When set to true, EntertainmentcategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentcategoryEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentcategoryEntityReturnsNewIfNotFound; }
			set { _entertainmentcategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LanguageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLanguageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual LanguageEntity LanguageEntity
		{
			get	{ return GetSingleLanguageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLanguageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EntertainmentcategoryLanguageCollection", "LanguageEntity", _languageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageEntity. When set to true, LanguageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageEntity is accessed. You can always execute a forced fetch by calling GetSingleLanguageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageEntity
		{
			get	{ return _alwaysFetchLanguageEntity; }
			set	{ _alwaysFetchLanguageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageEntity already has been fetched. Setting this property to false when LanguageEntity has been fetched
		/// will set LanguageEntity to null as well. Setting this property to true while LanguageEntity hasn't been fetched disables lazy loading for LanguageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageEntity
		{
			get { return _alreadyFetchedLanguageEntity;}
			set 
			{
				if(_alreadyFetchedLanguageEntity && !value)
				{
					this.LanguageEntity = null;
				}
				_alreadyFetchedLanguageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property LanguageEntity is not found
		/// in the database. When set to true, LanguageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool LanguageEntityReturnsNewIfNotFound
		{
			get	{ return _languageEntityReturnsNewIfNotFound; }
			set { _languageEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
